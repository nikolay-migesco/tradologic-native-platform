<?php
/**
 * Created by PhpStorm.
 * User: SbWereWolf
 * Date: 2019-04-18
 * Time: 17:45
 */

/* This is unknown artifact of past days, may be change 'tradologic' for something else ? */
if ( !defined('TOLTEXTDOMAIN') ) {
    define('TOLTEXTDOMAIN', 'tradologic');
}

function defineMigescoLayout(){
    if (!defined('MIGESCO_LAYOUT')) {
        define('MIGESCO_LAYOUT', 'migesco');
    }
}

function isMigescoLayout(){
    return defined('MIGESCO_LAYOUT');
}

function getDefaultTraderoom(){
    return 'simplex';
}

class ResourceLoader
{
    private $webPrefix = '';
    private $directory = '';

    public function __construct(string $directory, string $webPrefix)
    {
        $this->directory = $directory;
        $this->webPrefix = $webPrefix;
    }

    private function getFileTime(string $filePath): string
    {
        return strval(filemtime($this->directory . $filePath));
    }

    public function withFileTime(string $filePath)
    {
        $result = $this->webPrefix . "$filePath?" . $this->getFileTime($filePath);

        return $result;
    }

    public function enqueueStyle(string $handle, string $filePath)
    {
        wp_enqueue_style($handle, $this->webPrefix . $filePath, false, null, false);
    }

    public function enqueueStyleWithFileTime(string $handle, string $filePath)
    {
        wp_enqueue_style($handle, $this->webPrefix . $filePath, false, $this->getFileTime($filePath), false);
    }

    public function registerScriptWithFileTime(string $handle, string $filePath, array $dependency = array(), bool $inFooter = true)
    {
        wp_register_script($handle, $this->webPrefix . $filePath, $dependency, $this->getFileTime($filePath), $inFooter);
        wp_enqueue_script($handle);
    }

    public function registerScript(string $handle, string $filePath, array $dependency = array(), bool $inFooter = true)
    {
        wp_register_script($handle, $this->webPrefix . $filePath, $dependency, null, $inFooter);
        wp_enqueue_script($handle);
    }
}
