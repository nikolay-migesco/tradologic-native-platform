var migesco = {
    wasSet: function (name) {
        const result = typeof name !== typeof undefined;
        return result;
    },
    hasValue: function (name) {
        const isSet = this.wasSet(name);
        let result = false;
        if (isSet) {
            result = name != null;
        }

        return result;
    },
    tradologicThemes: {
        cookieName: "theme",
        lightLayout: "light",
        darkLayout: "dark",
    },
    writeThemeCookie: function (pageTheme) {
        $.cookie(this.tradologicThemes.cookieName, pageTheme, {"path": "/"});
        return true;
    },
    readThemeCookie: function () {
        const result = $.cookie(this.tradologicThemes.cookieName);
        return result;
    },
    chartChangeTheme: function (chart, theme) {
        chart.changeTheme(theme);
    },
    tradingviewThemes: {
        lightLayout: "Light",
        darkLayout: "Dark",
    },
    becomeLight: function (chart) {
        const isExists = this.hasValue(chart);
        if (isExists) {
            this.chartChangeTheme(chart, this.tradingviewThemes.lightLayout);
            this.writeThemeCookie(this.tradologicThemes.lightLayout);
        }
    },
    becomeDark: function (chart) {
        const isExists = this.hasValue(chart);
        if (isExists) {
            this.chartChangeTheme(chart, this.tradingviewThemes.darkLayout);
            this.writeThemeCookie(this.tradologicThemes.darkLayout);
        }
    },
    chooseTheme: function () {
        const theme = this.readThemeCookie();
        let result = this.tradingviewThemes.darkLayout;
        switch (theme) {
            case this.tradologicThemes.darkLayout:
                result = this.tradingviewThemes.darkLayout;
                break;
            case this.tradologicThemes.lightLayout:
                result = this.tradingviewThemes.lightLayout;
                break;
        }
        return result;
    },
    event: {tradingviewIsReady: "tradingview-is-ready"},
    waitReadinessOfTradingview: function () {
        isTradingviewLibrary = typeof TradingView !== typeof undefined;
        if (isTradingviewLibrary) {
            const waitingTradingviewReady = 500;
            let waitForTradingView = setTimeout(function setLocale() {
                const isTradingViewDefined = typeof tvWidget !== typeof undefined;
                let isReady = false;
                if (isTradingViewDefined) {
                    isReady = typeof tvWidget._ready !== typeof undefined;
                }
                if (isReady) {
                    isReady = tvWidget._ready;
                }
                if (isReady) {
                    $(window).trigger(this.event.tradingviewIsReady);
                }
                if (!isReady) {
                    waitForTradingView = setTimeout(setLocale, waitingTradingviewReady);
                }
            }, waitingTradingviewReady);
        }
    },
    traderoom: null,
    setSimplexDesktop: function () {
        this.traderoom = traderoomLayout.simplexDesktop;
    },
    setSimplexPhone: function () {
        this.traderoom = traderoomLayout.simplexPhone;
    },
    setProfexDesktop: function () {
        this.traderoom = traderoomLayout.profexDesktop;
    },
    setProfexPhone: function () {
        this.traderoom = traderoomLayout.profexPhone;
    },
    setProfexTablet: function () {
        this.traderoom = traderoomLayout.profexTablet;
    },
    isSimplexDesktop: function () {
        return this.traderoom === traderoomLayout.simplexDesktop;
    },
    isSimplexPhone: function () {
        return this.traderoom === traderoomLayout.simplexPhone;
    },
    isProfexDesktop: function () {
        return this.traderoom === traderoomLayout.profexDesktop;
    },
    isProfexPhone: function () {
        return this.traderoom === traderoomLayout.profexPhone;
    },
    isProfexTablet: function () {
        return this.traderoom === traderoomLayout.profexTablet;
    },
    attachDraggableToNewNodes: function () {
        const bodyElement = $("body");
        const observer = new MutationObserver(function (mutations) {
            mutations.forEach(function (mutation) {
                mutation.addedNodes.forEach(function (newNode) {
                    const nodeClass = newNode.className;
                    let process = typeof nodeClass !== typeof undefined;
                    if(process){
                        process = nodeClass.includes("ui-dialog");
                    }
                    if(process){
                        $(newNode).draggable();
                    }
                });
            });
        });

        observer.observe(bodyElement[0], {
            childList: true
        });
    }
};

const defaultLocale = "ru";
const getPathLanguagePrefix = function () {
    const language = $.cookie("current_language");
    const path = language === defaultLocale ? "" : `/${language}`;
    return path;
};

const traderoomMode = {
    easyForex: "EasyForex",
    realForex: "RealForex",
};
const traderoomLayout = {
    simplexDesktop: "simplex",
    simplexPhone: "simplexm",
    profexDesktop: "forex",
    profexTablet: "realil",
    profexPhone: "realm"
};
const simplexMark = [
    traderoomMode.easyForex,
    traderoomLayout.simplexDesktop,
    traderoomLayout.simplexPhone,
    "",
    null,
];
const profexMark = [
    traderoomMode.realForex,
    traderoomLayout.profexDesktop,
    traderoomLayout.profexTablet,
    traderoomLayout.profexPhone,
];

const migescoEvents = {
    forexJsIsCompleteLoad: "forex-js-is-complete-load"
};
$(window).on(migescoEvents.forexJsIsCompleteLoad, function (event) {
    $(".separator.vertical-separator.side-separator .separator-collapse .separator-arrow").trigger("click");
});

function checkIsMobile() {
    let clientJs = null;

    const hasJsClient = migesco.wasSet(widgets.client);
    if (hasJsClient) {
        clientJs = widgets.client;
    }
    if (!hasJsClient) {
        clientJs = new ClientJS();
    }
    const isMobile = clientJs.isMobileMajor();
    return isMobile;
}

const getTraderoomLink = function (mode) {
    let tradeMode = "";
    const isMobile = checkIsMobile();

    const isSimplex = mode === traderoomMode.easyForex;
    if (isSimplex && !isMobile) {
        tradeMode = traderoomLayout.simplexDesktop;
    }
    if (isSimplex && isMobile) {
        tradeMode = traderoomLayout.simplexPhone;
    }

    if (!isSimplex && !isMobile) {
        tradeMode = traderoomLayout.profexDesktop;
    }

    let isTablet = false;
    if (!isSimplex && isMobile) {
        isTablet = $(document).width() >= 1024;
    }
    if (isTablet) {
        tradeMode = traderoomLayout.profexTablet;
    }
    if (!isSimplex && isMobile && !isTablet) {
        tradeMode = traderoomLayout.profexPhone;
    }

    const link = `/traderoom?product=${tradeMode}`;
    return link;
};

jQuery(function ($) {

    const composeLinkToHost = function (element) {
        element.href = "/";
    };
    $(".nav_logo a.navbar-brand").get().forEach(composeLinkToHost);

    const timeoutForWidgets = 2000;
    let waitingLoadOfWidgets = setTimeout(function isWidgetsFinishLoading() {
            if (widgets.isFirstStart) {
                waitingLoadOfWidgets = setTimeout(isWidgetsFinishLoading, timeoutForWidgets);
            }
            if (!widgets.isFirstStart) {
                const composeLinkToSimplex = function () {
                    const link = getTraderoomLink(traderoomMode.easyForex);
                    return link;
                };
                const composeLinkToProfex = function () {
                    const link = getTraderoomLink(traderoomMode.realForex);
                    return link;
                };

                const composeLinkToTraderoom = function (element) {
                    const href = element.href;
                    let isTraderoomLink = false;
                    if (href.includes("/traderoom")) {
                        isTraderoomLink = true;
                    }
                    let letRedefineLink = false;
                    if (isTraderoomLink) {
                        letRedefineLink = !href.includes("?") && !href.includes("&") && !href.includes("#");
                    }

                    let letSimplex = false;
                    let letForex = false;
                    if (letRedefineLink) {
                        letSimplex = simplexMark.includes(widgets.game);
                        letForex = profexMark.includes(widgets.game);
                    }

                    let link = "";
                    if (letSimplex) {
                        link = composeLinkToSimplex();
                    }
                    if (letForex) {
                        link = composeLinkToProfex();
                    }

                    if (letSimplex || letForex) {
                        element.href = link;
                    }
                };

                $("a").get().forEach(composeLinkToTraderoom);

                $("a.tol-forex-game").get().forEach(composeLinkToSimplex);

                $("a.tol-forex-pro").get().forEach(composeLinkToProfex);

                function setChartBeOpened() {
                    let isSuccess = false;
                    if(typeof easyForexWidget !== typeof undefined){
                        easyForexWidget.elementsCache.chartToggle.addClass("opened");
                        isSuccess = true;
                    }

                    return isSuccess;
                }

                let letSetResizeChartHandler = setChartBeOpened();

                if(letSetResizeChartHandler){
                    letSetResizeChartHandler = widgets.game === traderoomMode.easyForex;
                }
                if(letSetResizeChartHandler){
                    const composeChartPopup = function () {
                        const chartwrapper = $('.chartwrapper');
                        chartwrapper.css("left", "320px");

                        let proceed = true;
                        const documentWidth = $(document).width();
                        const toggleChart = $('.toggleChart');
                        const tolTradePopupModalDialog = $('#tol-trade-popup .modal-dialog');
                        if(documentWidth<766 && proceed){
                            chartwrapper.css("width","0");
                            toggleChart.css("right","-91px");
                            tolTradePopupModalDialog.css("left","0");
                            proceed = false;
                        }
                        if(documentWidth<960 && proceed){
                            chartwrapper.css("width","432px");
                            toggleChart.css("right","-521px");
                            tolTradePopupModalDialog.css("left","-240px");
                            proceed = false;
                        }
                        if(documentWidth<1200 && proceed){
                            chartwrapper.css("width","620px");
                            toggleChart.css("right","-709px");
                            tolTradePopupModalDialog.css("left","-330px");
                            proceed = false;
                        }
                        if(documentWidth<1366 && proceed){
                            chartwrapper.css("width","840px");
                            toggleChart.css("right","-929px");
                            tolTradePopupModalDialog.css("left","-435px");
                            proceed = false;
                        }
                        if(documentWidth<1440 && proceed){
                            chartwrapper.css("width","980px");
                            toggleChart.css("right","-1069px");
                            tolTradePopupModalDialog.css("left","-495px");
                            proceed = false;
                        }
                        if(documentWidth>=1440 && proceed){
                            chartwrapper.css("width","1050px");
                            toggleChart.css("right","-1139px");
                            tolTradePopupModalDialog.css("left","-545px");
                            proceed = false;
                        }
                    };
                    composeChartPopup();

                    $(window).resize(composeChartPopup);
                }

                const letRemoveStatusBar = migesco.isProfexTablet() || migesco.isProfexPhone()
                    || migesco.isSimplexPhone();

                if (letRemoveStatusBar) {
                    $('#statusbar').remove();
                }
            }
        }
        , timeoutForWidgets);

    function getLanguageIclCode(classesOfElement) {
        const iclMark = "icl-";
        const result = classesOfElement.split(" ").reduce(function (accumulator, currentValue, index, array) {
            if (accumulator == null) {
                if (currentValue.indexOf(iclMark) === 0) {
                    accumulator = currentValue.replace(iclMark, '')
                }
            }
            return accumulator;
        }, null);
        return result;
    }

    const classesOfElement = $("a.lang_sel_sel").attr("class");
    const isoCode = getLanguageIclCode(classesOfElement);

    const overrideLanguageCookie = function (isoCode) {

        const cookiesNames = {currentLanguage: "current_language"};
        const currentLanguage = $.cookie(cookiesNames.currentLanguage);
        const letOverrideLanguageCookie = currentLanguage !== isoCode;

        if (letOverrideLanguageCookie) {
            $.cookie(cookiesNames.currentLanguage, isoCode, {path: '/'});
        }
    };
    overrideLanguageCookie(isoCode);
});
