<?php

$configurationSource = 'configuration.php';
$isConfiguration = file_exists($configurationSource);
if($isConfiguration){
    include_once ($configurationSource);
}
unset($configurationSource);

/*
* IS the site Bitcoin
*/
define('IS_BITCOIN_SITE', false);


if(isset($_COOKIE["changeWidgets"]) && $_COOKIE["changeWidgets"]!=''){
    $env = $_COOKIE["changeWidgets"];
}else{
    if(preg_match("/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i", $_SERVER['SERVER_NAME'], $matches))
    {
        $domain = $matches['domain'];
    } else {
        $domain = $_SERVER['SERVER_NAME'];
    }
    $subdomain = rtrim(strstr($_SERVER['SERVER_NAME'], $domain, true), '.');
    if($subdomain=='stage' || $subdomain=='demotemplate-stage'){
        $env = 'stage';
    }elseif ($subdomain=='qa' || $subdomain=='demotemplate-qa') {
        $env = 'qa';
    }elseif ($subdomain=='www2' || $subdomain=='demotemplate') {
        $env = 'preview';
    }elseif ($subdomain=='www') {
        $env = 'prod';
    }else{
        if(strpos($_SERVER['SERVER_NAME'], '.local') !== FALSE){
            $env = 'localhost';
        }else{
            $env = 'prod';
        }
    }

    if(IS_BITCOIN_SITE){
        if($env=='preview' || $env=='prod'){
            $env = 'btc-prod';
        }else{
            $env = 'btc-stage';
        }
    }
};

define('ENV_NAME', $env);
$env_config_path = __DIR__.'/wp-config.json';
if (file_exists($env_config_path)) {
    $configs = json_decode(file_get_contents($env_config_path),true);
}

// Widgets and api configuration
foreach ($configs as $key=>$value){
    if($key==='env') continue;
    foreach ($value as $k=>$v){
        if($k==='configs') continue;
        if(!defined($k)) {
            define(strtoupper($k), $v);
        } else {
            die('Allready defined constant - '. $k);
        }
    }
}

// Create configs depending on ENV
foreach ($configs['env'][ENV_NAME] as $key=>$value){
    if(!defined($key)) {
        define(strtoupper($key), $value);
    } else {
        die('Allready defined constant - '. $key);
    }
}

// Create configs depending on ENV
foreach ($configs['widget_config'][ENV_NAME] as $key=>$value){
    if(!defined($key)) {
        define(strtoupper($key), $value);
    } else {
        die('Allready defined constant - '. $key);
    }
}
