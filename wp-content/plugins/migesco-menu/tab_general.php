<?php
if (!empty($_GET['drop'])) {
    $base = mmbv_get();
    foreach ($base AS $k => $row) {
        if ($row['id'] == $_GET['drop'])
            unset($base[$k]);
    }
    mmbv_update($base);
}

if (!empty($_GET['moveup'])) {
    $mmbv_mover = mmbv_get_by_id($_GET['moveup']);

    $replacing = -1;
    $base = mmbv_get();
    foreach ($base AS $k => $row) {
        if ($mmbv_mover['id'] == $row['id'] && $replacing > -1) {
            $base[$k] = $base[$replacing];
            $base[$replacing] = $row;
        } elseif ($mmbv_mover['group'] . $mmbv_mover['language'] . $mmbv_mover['access'] == $row['group'] . $row['language'] . $row['access']) {
            $replacing = $k;
        }
    }
    mmbv_update($base);
}

if (!empty($_GET['movedown'])) {
    $mmbv_mover = mmbv_get_by_id($_GET['movedown']);

    $replacing = -1;
    $base = mmbv_get();
    foreach ($base AS $k => $row) {
        if ($mmbv_mover['group'] . $mmbv_mover['language'] . $mmbv_mover['access'] == $row['group'] . $row['language'] . $row['access'] && $replacing > -1) {
            $base[$k] = $base[$replacing];
            $base[$replacing] = $row;
        } elseif ($mmbv_mover['id'] == $row['id']) {
            $replacing = $k;
        }
    }
    mmbv_update($base);
}

if (isset($_POST[MMBV_PREFIX . 'obj'])) {
    $base = mmbv_get();
    if ($_POST[MMBV_PREFIX . 'obj'] == 'new') {
        $new_sect = array(
            'id' => mmbv_get_last_id() + 1,
            'group' => $_POST[MMBV_PREFIX . 'group'],
            'name' => $_POST[MMBV_PREFIX . 'name'],
            'url' => $_POST[MMBV_PREFIX . 'url'],
            'language' => $_POST[MMBV_PREFIX . 'language'],
            'access' => $_POST[MMBV_PREFIX . 'access'],
        );
        $base[] = $new_sect;
    } elseif (intval($_POST[MMBV_PREFIX . 'obj']) > 0) {
        $edited_sect = array(
            'id' => $_POST[MMBV_PREFIX . 'obj'],
            'group' => $_POST[MMBV_PREFIX . 'group'],
            'name' => $_POST[MMBV_PREFIX . 'name'],
            'url' => $_POST[MMBV_PREFIX . 'url'],
            'language' => $_POST[MMBV_PREFIX . 'language'],
            'access' => $_POST[MMBV_PREFIX . 'access'],
        );
        foreach ($base AS $k => $row) {
            if ($row['id'] == $edited_sect['id'])
                $base[$k] = $edited_sect;
        }
    }
    mmbv_update($base);
}

if (!empty($_GET['edit'])) {
    $mmbv_edit_section = mmbv_get_by_id($_GET['edit']);
}
?>
<h3><?php echo $mmbv_edit_section ? 'Edit' : 'Add new'; ?> menu section</h3>
<form method="post" action="<?php echo MMBV_URL; ?>">
    <input type="hidden" name="<?php echo MMBV_PREFIX; ?>obj"
           value="<?php echo $mmbv_edit_section ? $mmbv_edit_section['id'] : 'new'; ?>">
    <input type="hidden" name="<?php echo MMBV_PREFIX; ?>group"
           value="<?php echo $mmbv_edit_section ? $mmbv_edit_section['group'] : 'main'; ?>">
    <div style="display:inline-block; padding-right:30px; float:left;">
        <p>
        <div style="display:inline-block; width:80px;">Name:</div>
        <input type="text" name="<?php echo MMBV_PREFIX; ?>name" size="20"
               value="<?php echo $mmbv_edit_section ? $mmbv_edit_section['name'] : ''; ?>"></p>
        <p>
        <div style="display:inline-block; width:80px;">Url:</div>
        <input type="text" name="<?php echo MMBV_PREFIX; ?>url" size="20"
               value="<?php echo $mmbv_edit_section ? $mmbv_edit_section['url'] : ''; ?>"></p>
    </div>
    <div style="display:inline-block; padding-right:30px;">
        <p>
        <div style="display:inline-block; width:80px;">Language:</div>
        <select name="<?php echo MMBV_PREFIX; ?>language"><?php foreach ($mmbv_language AS $k => $v) {
                echo '<option value="' . $k . '"' . ($k == $mmbv_edit_section['language'] ? ' selected' : '') . '>' . $v . '</option>';
            } ?></select></p>
        <p>
        <div style="display:inline-block; width:80px;">Access:</div>
        <select name="<?php echo MMBV_PREFIX; ?>access"><?php foreach ($mmbv_access AS $k => $v) {
                echo '<option value="' . $k . '"' . ($k == $mmbv_edit_section['access'] ? ' selected' : '') . '>' . $v . '</option>';
            } ?></select></p>
    </div>
    <div style="display:inline-block; padding-right:30px;">
        <input type="submit" class="button"/>
    </div>
</form>

<hr/><br/>

<h3>List of menu section</h3>

<table class="wp-list-table widefat fixed">
    <thead>
    <tr>
        <th scope="col" class="">Name</th>
        <th scope="col" class="">URL</th>
        <th scope="col" class="manage-column">Language</th>
        <th scope="col" class="manage-column">Access</th>
        <th scope="col" class="manage-column">Option</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $alternate = 0;
    $mmbv_base = array();
    foreach (mmbv_sorted() AS $k => $row) {
        $class = $alternate % 2 ? '' : 'alternate';
        $img_dir = MMBV_PLUGINDIR . 'img/';
        $href = MMBV_URL . '&';
        $url = home_url($row['url']);
        $drop = '<a href="' . $href . 'drop=' . $row['id'] . '" title="Delete this section" onclick="return confirm(\'Are you sure?\');"><img src="' . $img_dir . 'delete.gif" alt="" /></a>';
        if (mmbv_get_by_content($row['id'], 'content'))
            $drop = '<img src="' . $img_dir . 'delete.gif" alt="" onclick="alert(this.title);" title="You can\'t delete this section! First need remove contents!" />';

        echo <<<HTML
		<tr class="{$class}">
			<td><a href="{$href}tab=content&content={$row['id']}" title="{$row['id']}">{$row['name']}</a></td>
			<td><a href="{$url}" target="_blank">{$row['url']}</a></td>
			<td>{$mmbv_language[$row['language']]}</td>
			<td>{$mmbv_access[$row['access']]}</td>
			<td>
				<a href="{$href}moveup={$row['id']}" title="Move Up"><img src="{$img_dir}arrow-u.gif" alt="" /></a>
				<a href="{$href}movedown={$row['id']}" title="Move Down"><img src="{$img_dir}arrow-d.gif" alt="" /></a>
				<a href="{$href}tab=content&content={$row['id']}" title="Add or Edit Content menu"><img src="{$img_dir}attachment.gif" alt="" /></a>
				<a href="{$href}edit={$row['id']}" title="Edit this section"><img src="{$img_dir}edit.gif" alt="" /></a>
				{$drop}
			</td>
		</tr>
HTML;
        $alternate++;
    }
    ?>
    </tbody>
</table>
