<?php
if (isset($_POST[MMBV_PREFIX . 'obj'])) {
    $current_content = 0;
    $base = mmbv_get('content');
    if ($_POST[MMBV_PREFIX . 'obj'] == 'new') {
        $current_content = mmbv_get_last_id('content') + 1;
        $new_cont = array(
            'id' => $current_content,
            'menu' => $_POST[MMBV_PREFIX . 'menu'],
            'content' => $_POST[MMBV_PREFIX . 'content'],
        );
        $base[] = $new_cont;
    } elseif (intval($_POST[MMBV_PREFIX . 'obj']) > 0) {
        $current_content = $_POST[MMBV_PREFIX . 'obj'];
        $edited_cont = array(
            'id' => $current_content,
            'menu' => $_POST[MMBV_PREFIX . 'menu'],
            'content' => $_POST[MMBV_PREFIX . 'content'],
        );

        foreach ($edited_cont['content'] AS $k => $v) {
            if (empty($v))
                unset($edited_cont['content'][$k]);
        }

        foreach ($base AS $k => $row) {
            if ($row['id'] == $edited_cont['id']) {
                if (count($edited_cont[menu]) < 1 || count($edited_cont['content']) < 1) {
                    unset($base[$k]);
                } else
                    $base[$k] = $edited_cont;
            }
        }
    }
    $_GET['edit'] = $current_content;
    mmbv_update($base, 'content');
}


$mmbv_content_disabled = array();
$mmbv_content_menu = mmbv_get('content');
if (!empty($mmbv_content_menu)) {
    foreach ($mmbv_content_menu AS $k => $v) {
        if (!empty($v['menu']))
            $mmbv_content_disabled = array_unique(array_merge($mmbv_content_disabled, $v['menu']));
    }
}


if (!empty($_GET['content'])) {
    $content = mmbv_get_by_content($_GET['content'], 'content');
    $_GET['edit'] = $content['id'];
}
if (!empty($_GET['edit'])) {
    $mmbv_edit_content = mmbv_get_by_id($_GET['edit'], 'content');
}
?>
<h3><?php echo $mmbv_edit_content ? 'Edit' : 'Add new'; ?> menu content</h3>
<form method="post" name="<?php echo MMBV_PREFIX; ?>cont" action="<?php echo MMBV_URL; ?>&tab=content">
    <input type="hidden" name="<?php echo MMBV_PREFIX; ?>obj"
           value="<?php echo $mmbv_edit_content ? $mmbv_edit_content['id'] : 'new'; ?>">
    <p>
    <div style="display:inline-block; width:100px; float:left;">
        Menu section:<br/>
        <span style="font-size:70%;">Multi select with CTRL</span>
    </div>
    <select name="<?php echo MMBV_PREFIX; ?>menu[]"><!-- multiple style="height:120px;" -->
        <?php
        foreach (mmbv_sorted() AS $k => $v) {
            $selected = '';
            if (!empty($mmbv_edit_content['menu']) && in_array($v['id'], $mmbv_edit_content['menu']))
                $selected = ' selected';
            elseif ($v['id'] == $_GET['content'])
                $selected = ' selected';
            elseif (!empty($mmbv_content_disabled) && in_array($v['id'], $mmbv_content_disabled))
                $selected = ' disabled';

            echo '<option value="' . $v['id'] . '"' . $selected . '>' . $v['name'] . '</option>';
        }
        ?>
    </select>
    </p>

    <p>
    <div style="display:inline-block; width:100px; float:left;">
        Content:<br/><br/>
        <a href="#add"
           onclick="jQuery('#mmbv_content').append('<textarea name=<?php echo MMBV_PREFIX; ?>content[] wrap=off style=width:600px;height:320px;></textarea><br />'); return false;">Add
            content</a>
    </div>
    <div style="display:inline-block;" id="mmbv_content">
        <textarea name="<?php echo MMBV_PREFIX; ?>content[]" wrap="off"
                  style="width:600px; height:320px;"><?php echo $mmbv_edit_content ? $mmbv_edit_content['content'][0] : ''; ?></textarea><br/>
        <?php
        if (count($mmbv_edit_content['content']) > 1)
            foreach ($mmbv_edit_content['content'] AS $k => $v) {
                if ($k > 0)
                    echo '<textarea name="' . MMBV_PREFIX . 'content[]" wrap="off" style="width:600px; height:320px;">' . $v . '</textarea><br />';
            }
        ?>
    </div>
    </p>

    <input type="submit" class="button"/> <input type="button" class="button" value="Delete"
                                                 onclick="jQuery('form[name=mmbv_cont] textarea').val(''); jQuery('form[name=mmbv_cont]').submit();"/>
</form>
