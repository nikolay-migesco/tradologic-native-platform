<?php
define('MMBV_PLUGINDIR', (is_admin() ? '../' : '') . PLUGINDIR . '/' . dirname(plugin_basename(__FILE__)) . '/');
define('MMBV_URL', current(explode('&', str_replace('%7E', '~', $_SERVER['REQUEST_URI']))));
define('MMBV_PREFIX', 'mmbv_');


$mmbv_tabs = array('general' => 'General', 'content' => 'Contents', 'debug' => 'Debug');
$mmbv_language = array('ru' => 'Russian', 'en' => 'English');
$mmbv_access = array('User not Login', 'User Logged');
if (empty($mmbv_base)) $mmbv_base = array();


function mmbv_clean_param(&$params)
{
    if (is_array($params))
        foreach ($params AS $key => $value)
            $params[$key] = mmbv_clean_param($value);
    else
        $params = str_replace(array('\\\'', '\\"'), array('\'', '"'), $params);

    return $params;
}

function mmbv_update($array, $file = 'main')
{
    global $mmbv_base;
    $mmbv_base[$file] = $array = array_values(mmbv_clean_param($array));
    if ($fp = @fopen(MMBV_PLUGINDIR . $file . '.dat', 'w')) {
        fputs($fp, serialize($array));
        fclose($fp);
        return true;
    }
    return false;
}

function mmbv_get($file = 'main')
{
    global $mmbv_base;
    if (empty($mmbv_base[$file]))
        $mmbv_base[$file] = @unserialize(file_get_contents(MMBV_PLUGINDIR . $file . '.dat'));
    return $mmbv_base[$file];
}

function mmbv_get_by_id($id, $file = 'main')
{
    $base = mmbv_get($file);
    if (empty($base))
        return false;
    foreach ($base AS $row) {
        if ($row['id'] == $id)
            return $row;
    }
    return false;
}

function mmbv_get_by_content($id, $file = 'main')
{
    $base = mmbv_get($file);
    if (empty($base))
        return false;
    foreach ($base AS $row) {
        if (!empty($row['menu']) && in_array($id, $row['menu']))
            return $row;
    }
    return false;
}

function mmbv_get_last_id($file = 'main')
{
    $id = 1;
    $base = mmbv_get($file);
    if (empty($base))
        return $id;

    foreach ($base AS $row) {
        if ($id < $row['id'])
            $id = $row['id'];
    }
    return $id;
}

function mmbv_sorted($file = 'main')
{
    $base = mmbv_get($file);
    if (empty($base))
        return array();

    $base_sort = array();
    foreach ($base AS $k => $row) {
        $row['order'] = $k;
        $base_sort[$row['group'] . $row['language'] . $row['access']][] = $row;
    }

    $list = array();
    foreach ($base_sort AS $cell) {
        foreach ($cell AS $row)
            $list[] = $row;
    }
    return $list;
}
