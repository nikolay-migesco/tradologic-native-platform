<?php
header('Content-Type: text/html; charset=utf-8');

function isUserLogged()
{
    if (isset($_COOKIE['widgetSession'])) return true;    // standard widget-based login
    return false;
}

function getCurrentIso()
{
    return ($_COOKIE["_icl_current_language"] ? $_COOKIE["_icl_current_language"] : 'ru');
}

function mmbv_get($file = 'main')
{
    global $mmbv_base;
    if (empty($mmbv_base[$file]))
        $mmbv_base[$file] = @unserialize(file_get_contents($file . '.dat'));
    return $mmbv_base[$file];
}

function mmbv_get_by_content($id, $file = 'main')
{
    $base = mmbv_get($file);
    if (empty($base))
        return false;
    foreach ($base AS $row) {
        if (!empty($row['menu']) && in_array($id, $row['menu']))
            return $row;
    }
    return false;
}

$base = mmbv_get();
$language = getCurrentIso();
$access = (isUserLogged() ? '1' : '0');


$menu = array();
foreach ($base AS $row) {
    $contents = array();
    if ($row['group'] == 'main' && $row['language'] == $language && $row['access'] == $access) {
        if ($tmp = mmbv_get_by_content($row['id'], 'content')) {
            foreach ($tmp['content'] AS $cont) {
                if (!empty($cont))
                    $contents[] = $cont;
            }
            $row['contents'] = $contents;
        }
        $menu[] = $row;
    }
}

//echo '<pre>'.print_r($menu, 1).'</pre>';
//echo '<pre>'.print_r($menu[4]['contents'], 1).'</pre>';

?>


<?php if (count($menu[4]['contents']) > 1): ?>
    <?php foreach ($menu[4]['contents'] AS $node): ?>
        <div class="header_menu_table_ceil" style="width:<?php echo intval(100 / count($menu[4]['contents'])); ?>%;">
            <div class="header_menu_table_ceil_content">
                <?php echo $node; ?>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
