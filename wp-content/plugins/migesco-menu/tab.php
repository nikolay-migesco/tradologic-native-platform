<?php
/* Control Panel for Migesco Custom Menu */
if ($_SERVER['SCRIPT_FILENAME'] == __FILE__)
    die('Access denied!');

require_once(dirname(__FILE__) . '/engine.php');
$mmbv_tab_active = $_GET['tab'];
if (empty($mmbv_tab_active))
    $mmbv_tab_active = 'general';
?>
<div class="wrap">
    <h2 class="nav-tab-wrapper">
        <?php
        foreach ($mmbv_tabs AS $tab => $name) {
            echo '<a href="themes.php?page=migesco-menu&tab=' . $tab . '" class="nav-tab' . ($mmbv_tab_active == $tab ? ' nav-tab-active' : '') . '">' . $name . '</a>';
        }
        ?>
    </h2>
    <br/>
    <?php
    /* BODY PLUGIN */
    $mmbv_include = dirname(__FILE__) . '/tab_' . $mmbv_tab_active . '.php';
    if (is_file($mmbv_include)) {
        include($mmbv_include);
    }
    ?>
</div>
