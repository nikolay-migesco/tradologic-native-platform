<?php
/*
Plugin Name: Migesco Menu
Description: This plugin for Migesco. To create a custom menu
Version: 1.0
Author: Ruslans Jermakovics
Author URI: http://tfu.lv/
*/


add_action('admin_menu', 'migesco_admin_init');


function migesco_admin_init()
{
    add_theme_page('Migesco Menu', 'Migesco Menu', 8, 'migesco-menu', 'migesco_admin_plugin');
    add_filter('plugin_action_links_' . plugin_basename(__FILE__), 'migesco_admin_plugin_action_links');
}

function migesco_admin_plugin_action_links($_links)
{
    $links = array(
        '<a href="' . admin_url('themes.php?page=migesco-menu') . '">Make custom Menu</a>',
    );
    return array_merge($links, $_links);
}

function migesco_admin_plugin()
{
    include(dirname(__FILE__) . '/tab.php');
}


function migesco_menu($section = 'main')
{
    require_once(dirname(__FILE__) . '/engine.php');

    $base = mmbv_get();
    $language = ThemeHelper::getCurrentIso();
    $language = $language ? $language : 'ru';
    $access = isUserLogged() ? '1' : '0';

    $menu = array();
    foreach ($base AS $row) {
        $contents = array();
        if ($row['group'] == $section && $row['language'] == $language && $row['access'] == $access) {
            if ($tmp = mmbv_get_by_content($row['id'], 'content')) {
                foreach ($tmp['content'] AS $cont) {
                    if (!empty($cont))
                        $contents[] = $cont;
                }
                $row['contents'] = $contents;
            }
            $menu[] = $row;
        }
    }

    include('frontend_template.php');
}
