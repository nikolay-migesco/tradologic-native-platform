<div class="header_menu" style="width:980px;margin:34px auto 0;">
    <div style="position:relative;margin-top:-3px;" class="header_menu_body">
        <ul class="header_menu_list">
            <?php foreach ($menu AS $item): ?>
                <li>
                    <a href="<?php echo $item['url']; ?>"><?php echo $item['name']; ?></a>
                    <?php if (!empty($item['contents'])): ?>
                        <span class="header_submenu_connector"></span>
                        <div class="header_submenu">
                            <?php if (count($item['contents']) > 1): ?>
                                <?php foreach ($item['contents'] AS $node): ?>
                                    <div class="header_menu_table_ceil"
                                         style="width:<?php echo intval(100 / count($item['contents'])); ?>%;">
                                        <div class="header_menu_table_ceil_content">
                                            <?php echo $node; ?>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <div style="padding:0px 30px;">
                                    <?php echo $item['contents'][0]; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                </li>
            <?php endforeach; ?>
        </ul>
        <div id="dialog_ajax_box"><?php include(dirname(__FILE__) . '/../../themes/tol-parent/widget-userdata.php'); ?></div>
    </div>
</div>
