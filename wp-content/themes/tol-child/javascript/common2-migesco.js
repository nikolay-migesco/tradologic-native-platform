/* theme's common scripts */
/* theme's common scripts */
jQuery(function ($) {
    pageSwitch();
    /*
	 * Update clock
	 * To overwrite it, define another before it using the same name like this:
	 * updateClock = function() {...
	 */
    if (typeof (updateClock) !== "function") {
        updateClock = function () {
            var currentTime = new Date();
            var utcTime = (currentTime.getUTCHours() < 10 ? "0" + currentTime.getUTCHours() : currentTime.getUTCHours()) + ':' + (currentTime.getUTCMinutes() < 10 ? "0" + currentTime.getUTCMinutes() : currentTime.getUTCMinutes()) + ' UTC';
            var hours = currentTime.getHours();
            var minutes = currentTime.getMinutes();
            if (minutes < 10)
                minutes = "0" + minutes;

            var seconds = currentTime.getSeconds();
            if (seconds < 10)
                seconds = "0" + seconds;

            var month = new Array();
            month[0] = "Jan";
            month[1] = "Feb";
            month[2] = "Mar";
            month[3] = "Apr";
            month[4] = "May";
            month[5] = "Jun";
            month[6] = "Jul";
            month[7] = "Aug";
            month[8] = "Sep";
            month[9] = "Oct";
            month[10] = "Nov";
            month[11] = "Dec";

            var m = new Date();
            $("#header-clock-date").html(m.getDate() + " " + month[m.getMonth()]);

            $("#header-clock").html(hours + ":" + minutes + ":" + seconds);
            //$("#header-GMTclock").html('(' + (hours - 2) + ':' + minutes + ' UTC)');
            $("#header-GMTclock").html('(' + utcTime + ')');
        };
    }
    updateClock();
    setInterval(updateClock, 1000);

    // initialize jquery-placeholder
    if (typeof ($.fn.placeholder) === 'function') {
        $('input, textarea').placeholder();
    }
    ;

    // attach lpbutton to link with class "action-lpbutton"
    $('.action-lpbutton').click(function () {
        $('#lp-wrapper A').click();
    });

    $(".QuestionMark").hover(
        function () {
            var currentEl = this;
            $(".QuestionExpand").show();
        }, function () {
            $(".QuestionExpand").hide();
        }
    );
    // Home Page slider

    addPageAsClass();

    /************* ASSETS INDEX MENU *************/

    $('.assets-index-nav').click(function () {
        $('.assets-index-nav').removeClass('active');
        $(this).addClass('active');
        var catId = '#' + $(this).attr('id');
        $('.asset-col').hide();
        $('.assetindex-wrapper').find($('.asset-col' + catId)).show();
    });

    /************* END ASSETS INDEX MENU *************/
});

$(window).on("closedTradesIsReady", function () {
    try {
        $('#closeTradesOptionsWrap .customSelectInner').html($('#closeTradesOptionsWrap .myTradesOptionsAssetsDDL option:selected').text());
    } catch (e) {
    }
    ;
});


$(window).on('loadingIsCompleted', function () {

    //cashier
    $.removeCookie('step', {path: '/'});
    //


    if (widgets.isLogged() == true) {
        $('.traderoomWrap').removeClass("notlogged");
        $('.traderoomWrap').addClass("logged");
    } else {
        $('.traderoomWrap').addClass("notlogged");
    }
    ;

    if ((location.pathname == "/my-options") || (location.pathname == "/my-trades") || (location.pathname == "/past-expiries")) {
        setTimeout(function () {
            pageSwitch()
        }, 100);
    }

    if (widgets.game == "digital" && $.cookie('view') != 'multi') {
        printTraderInsightText();
    }
});

$(window).on('widgetsRegisterEvents', function () {
    $(window).on('linkOptionUpdated', function () {
        if (widgets.game == "digital" && $.cookie('view') != 'multi') {
            printTraderInsightText();
        }
    });
});

window.sliderSpeed = 550000;
window.chartMarkerUrl = '/wp-content/themes/xptheme/images/blink.gif';

$(document).on('click', '.login-BNT', function () {
    $('.login-popup-bg').show();
    $('.login-wrap').show();
});

$(document).on('click', '.login-close', function () {
    $('.login-popup-bg').hide();
    $('.login-wrap').hide();
});

$(document).on('click', '.reg-close', function () {
    $('.login-popup-bg').hide();
    $('.reg-wrap').hide();
});

$(document).on('click', '.resetpassword-close', function () {
    $('.login-popup-bg').hide();
    $('.resetpassword-wrap').hide();
});

$(document).on('click', '.profile-close', function () {
    $('.login-popup-bg').hide();
    $('.profile-wrap').hide();
});

$(document).on('click', '.change-close', function () {
    $('.login-popup-bg').hide();
    $('.change-wrap').hide();
});

$(document).on('click', '.how-to-trade-menu span', function () {
    $('.how-to-trade-menu span').removeClass('active');
    $(this).addClass('active');
});
$(document).ready(function () {
    try {
        $("#byROI").click(function () {
            $('#byHitRate').removeClass('active');
            $(this).addClass('active');
        });
        $("#byHitRate").click(function () {
            $('#byROI').removeClass('active');
            $(this).addClass('active');
        });
    } catch (e) {
    }
    ;

});
$(window).on("pastExpiriesTableProcessOptions", function () {
    pageSwitch();
});
$(window).on("myTradesTableIsLoaded", function () {
    pageSwitch();
});


pageSwitch = function () {
    $('.datagrid-paging .arrowButtons, .pastExpiriesPaging .arrowButtons, .closedTradesPaging .arrowButtons').remove();
    var prev = '<a class="arrowButtons prevPage" id="prevPage"><span class="pageBack">&nbsp;</a>',
        back = '<a class="arrowButtons nextPage" id="nextPage"><span class="pageFwd">&nbsp;</a>';
    if ($($('.paging a')[0]).hasClass('activePage')) {
        prev = '';
    }
    var paging = $('.paging a');
    if ($($('.paging a')[paging.length - 1]).hasClass('activePage')) {
        back = '';
    }
    if (paging.length == 0) {
        prev = '';
        back = '';
    }
    $('.datagrid-paging, .pastExpiriesPaging, .closedTradesPaging').prepend(prev);
    $('.datagrid-paging, .pastExpiriesPaging, .closedTradesPaging').append(back);
};

$(window).on("cashierPromocodePopupOpen", function () {
    $("div#loadingElementProcessing").hide();

});

addPageAsClass = function () {
    var x = location.pathname.split("/");
    var l = x.length;
    $("article").addClass(x[l - 1]);
};

addrows = function () {
    rows = $('.openTrades tbody tr');
    rowsLength = rows.length;
    if (rowsLength < 7) {
        rowsToAdd = 7 - rowsLength;
        rowsAdded = '';
        for (rowsToAdd; rowsAdded < rowsToAdd; rowsToAdd--) {
            $('.openTrades tbody').append("<tr><td>&nbsp;</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>");
        }
    } else if (rows == "[]") {
        rowsToAdd = 7;
        rowsAdded = '';
        for (rowsToAdd; rowsAdded < rowsToAdd; rowsToAdd--) {
            $('.openTrades tbody').append("<tr><td>&nbsp;</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>");
        }
    }

}


showSimple = function (isMulti, container) {
    if (isMulti) {
        $('.' + container + ' .autoTrade').removeClass('active');
        $('.' + container + ' .simpleTrade').addClass('active');
        $('.' + container + ' .contractsWrapper').hide();
        $('.' + container + ' .contractsDDL').val('1');
    } else {
        $('.autoTrade').removeClass('active');
        $('.simpleTrade').addClass('active');
        $('.backWrapper').removeClass('auto');
        $('.contractsWrapper').hide();
        $('.contractsDDL').val('1');
    }
    ;
};

showAuto = function (isMulti, container) {
    if (isMulti) {
        $('.' + container + ' .simpleTrade').removeClass('active');
        $('.' + container + ' .autoTrade').addClass('active');
        $('.' + container + ' .contractsWrapper').show();

        $('.' + container + ' .contractsDDL').val('2');
    } else {
        $('.simpleTrade').removeClass('active');
        $('.autoTrade').addClass('active');
        $('.backWrapper').addClass('auto');
        $('.contractsWrapper').show();
        $('.contractsDDL').val('2');
    }
    ;
};
$(document).ready(function () {
    $(document).on('click', '.promoCodeLink', function () {
        $('.cashierDepositMethod').show();
    });
    $(document).on('click', '.closedTradesBtn', function () {
        $('.chartClose').click()
    });
});

$(window).on("cashierPromocodeEnter", function () {
    $('.cashierFiedsets').show();
});

function printTraderInsightText() {
    var traderInsightData = '',
        leftPercent = '',
        rightPercent = '',
        selectedTradableAsset = widgets.links.tradologic.option.tradableAssetId;
    widgets.api.getTraderInsightItem(selectedTradableAsset, function (response) {
        traderInsightData = response,
            leftPercent = traderInsightData.data.CallTotalVolume + '%',
            rightPercent = traderInsightData.data.PutTotalVolume + '%';
        $('.traderInsightTitle').text(widgetMessage.trader_insight_chart.replace(/\{Asset}/g, widgets.links.tradologic.option.name).replace(/\{RateLeft}/g, leftPercent).replace(/\{RateRight}/g, rightPercent));
    });

}

