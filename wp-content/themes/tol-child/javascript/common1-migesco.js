$(window).on('loadingIsCompleted', function () {

    if (!('remove' in Element.prototype)) {
        Element.prototype.remove = function () {
            if (this.parentNode) {
                this.parentNode.removeChild(this);
            }
        };
    }

    if ($.cookie('current_language') != 'ru' && $.cookie('current_language') != 'it') {
        $('ul.navgame.dropdown-menu li.binary-game').remove();
        if (widgets.isMobileDevice()) {
            $('#menu-primary-not-logged li.mobile-binary').remove();
        }
    }
    //Load config for binary landscape
    if ($(window).width() == 1024) {
        $('.tol-wp-navigation li a').each(function (index) {
            if ($(this).attr('href').indexOf('config=newbinarym') > -1 && $(this).attr('href').indexOf('config=newbinaryil') == -1) {
                $(this).attr('href', $(this).attr('href').replace('?config=newbinarym', '?config=newbinaryil'))
            }
            ;
        });
    }
    ;
    //Start CustomFieldAssetFilter PopUp 
    $(window).on('assetFilterLoaded', function () {
        $('.assetFilterCustomPopup > .columnsContainer > .columns').on('click', function (e) {
            e.stopPropagation();
            if ($(this).hasClass('activeAssetField')) {
                $(this).removeClass('activeAssetField');
            } else {
                $(this).addClass('activeAssetField');
            }
        });
    });

    var config = helper.getParameterByName('config');
    if (window.location.href.indexOf('traderoom') > 0 && config == 'newbinarym' && ($.cookie('current_language') == 'ru' || $.cookie('current_language') == 'it')) {
        $('#lang_sel_click li.icl-it a,#lang_sel_click li.icl-ru a ').each(function (index) {
            if ($(this).attr('href').indexOf('config=newbinarym') == -1) {
                $(this).attr('href', $(this).attr('href').replace('traderoom/', 'traderoom?config=newbinarym'))
            }
            ;
        });
        $('#lang_sel_click li.icl-en a,#lang_sel_click li.icl-es a,#lang_sel_click li.icl-ar a ').each(function (index) {
            if ($(this).attr('href').indexOf('config=realm') == -1) {
                $(this).attr('href', $(this).attr('href').replace('traderoom/', 'traderoom?config=realm'))
            }
            ;
        });

    } else if (window.location.href.indexOf('traderoom') > 0 && config == 'newbinaryil' && ($.cookie('current_language') == 'ru' || $.cookie('current_language') == 'it')) {
        $('#lang_sel_click li.icl-it a,#lang_sel_click li.icl-ru a ').each(function (index) {
            if ($(this).attr('href').indexOf('config=newbinaryil') == -1) {
                $(this).attr('href', $(this).attr('href').replace('traderoom/', 'traderoom?config=newbinaryil'))
            }
            ;
        });
        $('#lang_sel_click li.icl-en a,#lang_sel_click li.icl-es a,#lang_sel_click li.icl-ar a ').each(function (index) {
            if ($(this).attr('href').indexOf('config=realm') == -1) {
                $(this).attr('href', $(this).attr('href').replace('traderoom/', 'traderoom?config=realm'))
            }
            ;
        });

    } else {
        if (window.location.href.indexOf('traderoom') > 0 && config == 'newbinary' && ($.cookie('current_language') == 'ru' || $.cookie('current_language') == 'it')) {
            $('#lang_sel_click li.icl-it a,#lang_sel_click li.icl-ru a ').each(function (index) {
                if ($(this).attr('href').indexOf('config=newbinary') == -1) {
                    $(this).attr('href', $(this).attr('href').replace('traderoom/', 'traderoom?config=newbinary'))
                }
                ;
            });
            $('#lang_sel_click li.icl-en a,#lang_sel_click li.icl-es a,#lang_sel_click li.icl-ar a ').each(function (index) {
                if ($(this).attr('href').indexOf('config=forex') == -1) {
                    $(this).attr('href', $(this).attr('href').replace('traderoom/', 'traderoom?config=forex'))
                }
                ;
            });
        }
    }

    $('.fb-social').click(function () {
        window.open('https://www.facebook.com/BNQCapital/');
        return false;
    });
    $('.tw-social').click(function () {
        window.open('https://twitter.com/bnqcapital');
        return false;
    });
    $('.li-social').click(function () {
        window.open('https://www.instagram.com/bnqcapital/');
        return false;
    });


});
