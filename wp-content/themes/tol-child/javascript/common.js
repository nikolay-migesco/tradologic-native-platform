$(document).ready(function () {
    $(window).on('loadingIsCompleted', function() {
        applyDemoUserFlow();
    });

    $(window).on('userDetailsUpdated', function () {
        if(!widgets.user.isReal && widgetsSettings.enable_demo_flow == "1"){
            widgets.api.getQuestionnaireAnswers(function (answers) {
                if(answers.data.QuestionnaireStatus != 2 && answers.data.QuestionnaireStatus != 1){
                    if($('#tol-myprofile-popup').is(':visible')){
                        $('#tol-myprofile-popup').modal('hide');
                        $('#tol-questionnaire-popup').modal('show');
                    }
                }
            });
        }
    });

    $(window).on('qestionarySubmittedSuccessfull', function(){
        if (widgetsSettings.enable_demo_flow == "1") {
            $('#tol-questionnaire-popup').modal('hide');
            $('#tol-upload-docs-popup').modal('show');
        };
    });
	$('.flex-market__item.indices > a').on("click", function () {
        window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'indices';
    });
	$('.flex-market__item.stocks > a').on("click", function () {
        window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'stocks';
    });
	$('.flex-market__item.commodities > a').on("click", function () {
        window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'commodities';
    });
	$('.flex-market__item.currencies > a').on("click", function () {
        window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'currencies';
    });
});

function applyDemoUserFlow() {
    if (widgetsSettings.enable_demo_flow != "1") {
        return;
    };

    $('#unverified-account-modal .tol-userbar-my-profile').unbind('click').bind('click', function () {
        if(!widgets.user.isReal) {
            widgets.api.getQuestionnaireAnswers(function (answers) {
                if(answers.data.QuestionnaireStatus != 2 && answers.data.QuestionnaireStatus != 1){
                    $('#unverified-account-modal').modal('hide');
                    $('#tol-myprofile-popup').modal('show');
                } else {
                    $('#unverified-account-modal').modal('hide');
                    $('#tol-questionnaire-popup').modal('hide');
                    $('#tol-upload-docs-popup').modal('show');
                }
            });
        }
        return;
    });

    $('.tol-userbar-transaction-history').unbind('click').bind('click', function () {
        window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'cashier?cpage=transactionHistory';
        return;
    });

    $('.nav-switch-account').on('click', function(event) {
        $('#tol-login-popup').modal('show');
        var switchType = ($(event.target).parent().data('switch') ? $(event.target).parent().data('switch') : $(event.target).parent().data('switch')).replace('trade', '').toLowerCase();
        $('#tol-login-popup').find('[data-translate="' + switchType + '"]').click();
    });

    $('.account-switch-toggle div').unbind('click').bind('click', function(event) {
        $('.account-switch-toggle div').removeClass('active');
        $(event.target).addClass('active');

        var switchType     = $(event.target).data('translate'),
            currentValue   = $('#tol-login-popup').find('#username').val(),
            demoEmailValue = widgets ? (widgets.user ? 'demo-' + widgets.user.email.replace('demo-', '') : 'demo-' + currentValue) : 'demo-' + currentValue;

        // add or remove prefix depending on tab selected
        if (switchType == 'demo')
            $('#tol-login-popup').find('#username').val(demoEmailValue);
        else {
            $('#tol-login-popup').find('#username').val(currentValue.replace('demo-', ''));
        }
    });

    var user = $.cookie('widgetUserData') ? JSON.parse($.cookie('widgetUserData')) : null;

    if (user) {
        var isVerified       = user.isVerified,
            hasParent        = user.ParentUserId ? user.id != user.ParentUserId : false,
            hasPackage       = user.Package > 0;

        $('.nav-switch-account').data('switch', (hasParent ? 'tradeReal' : 'tradeDemo'));
        $('.user-switch-acc').html(hasParent ? 'tradeReal' : 'tradeDemo');

        if (!isVerified && widgets.user.cySECRegulated == 2) {
            $('.tol-quick-deposit').unbind('click').bind('click', function(){
                $('#unverified-account-modal').modal('show');
            });

            if (!hasParent) {
                $(window).on('realForexAssetsDisplayed', function() {
                    $('[data-type="buyBox"], [data-type="sellBox"], [data-price="sell"], [data-price="buy"]').unbind('click').bind('click', function(event){
                        $('#unverified-account-modal').modal('show');
                        return false;
                    })
                });

                $(window).on('easyForexOptionsDisplayed', function() {
                    $('[data-type="invest-btn"]').unbind().on('click', function(event){
                        $('#unverified-account-modal').modal('show');
                        return false;
                    })
                });
                $('.widgetButtonAlpha, .widgetButtonBeta').unbind('click').bind('click', function(event){
                    $('#unverified-account-modal').modal('show');
                    return false;
                });

                // New Binary
                $(window).on('tableOptionsUpdated', function() { 
                    $('[data-type="call-btn"], [data-type="put-btn"]').unbind('click').bind('click', function(event){
                        $('#unverified-account-modal').modal('show');
                        return false;
                    });
                });

                // Exchange
                $(window).on('binaryExchangeTradeBoxUpdated', function() { 
                    $('[data-type="tradebutton"]').unbind('click').bind('click', function(event){
                        $('#unverified-account-modal').modal('show');
                        return false;
                    });
                });
            };

            // if user is unverified and without a package then show Packages link
            if (!hasPackage) {
                $('.packages-side').show();
                $('.market-research-side').hide();
            }
        };

        // add sidemenu link to market tools
        if (hasPackage) {
            $('.packages-side').hide();
            $('.market-research-side').show();

            if (user.Package >= 3000) {
                $('.userpack250, .userpack1200, .userpack3000').show();
            } else if (user.Package >= 1200) {
                $('.userpack250, .userpack1200').show();
                $('.userpack3000').hide();
            } else if (user.Package >= 250) {
                $('.userpack250').show();
                $('.userpack1200, .userpack3000').hide();
            };
        };

        if (hasParent) {
            $('.tol-quick-deposit').unbind('click').bind('click', function(){
                $('#unverified-account-modal').modal('show');
            });

            $('.tol-userbar-my-profile, .tol-quick-deposit').hide();
        };

        // add switch button
        if (hasPackage || hasParent) { 
            $('.li-switch-account').show();
        };

        // 3. (Demo1) Registered user, unverified with purchased package must have access to transaction History page
        if (hasPackage && !hasParent) {
            $('.tol-userbar-transaction-history').show();
        };

        var path = document.location.pathname.split('/')[1];

        $(window).on('loadingIsCompleted', function () {
            // if user is not logged don't show the traderoom
            if (window.location.pathname.indexOf('traderoom') > -1 && !widgets.user) {
                window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/');
            };

            if (hasParent) // Change account label
                $('.account-type:visible').html(helper.getTranslation('demoAccount', 'Demo Account'));
        });

        $(window).on('cashierLoaded', function () {
            if ( helper.getParameterByName('basicPrice') != '') {
                $('#depositAmountInput').val(helper.getParameterByName('basicPrice')).prop('readonly', true);
            };

            if (!hasPackage && helper.getParameterByName('basicPrice') != '') 
                $('#tol-cashier-main-nav a').addClass('disabled').click(function(e){e.preventDefault()});

            if (!isVerified) {
                $('.tol-quick-deposit, [data-type="buyBox"], [data-type="sellBox"]').unbind().on('click', function(event){
                    $('#unverified-account-modal').modal('show');
                });

                // disablle all fields if the user is not buying a package
                if(helper.getParameterByName('basicPrice') == '' && helper.getParameterByName('cpage') != 'ccdeposit') {
                    $('select, input').prop('disabled', true);
                    $('input[type="text"]').css('background-color', '#eee');
                };
            };
        });

        $(window).on('cashierDepositFieldsUpdated', function () {	
            if (!isVerified) {
                // disablle all fields if the user is not buying a package
                if(helper.getParameterByName('basicPrice') == '') {
                    $('select, input').prop('disabled', true);
                    $('input[type="text"]').css('background-color', '#eee');
                }
            }
        });
    } else {
        $('.packages-side').show();
        $('.market-research-side').hide();
    };
}