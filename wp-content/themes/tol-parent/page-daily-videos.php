<?php
/**
 * Template Name: MTE Daily Videos
 */
    // $userId = json_decode(stripslashes($_COOKIE['widgetUserData']), true)["id"];
    // $redirectDestination = $userId ? '/traderoom' : '';

    // if (json_decode(stripslashes($_COOKIE['widgetUserData']), true)["Package"] < 250) {
    //     $url         =  "//{$_SERVER['HTTP_HOST']}";
    //     $escaped_url = htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' );
    //     header("Location: " . $escaped_url . $redirectDestination);
    // }
 ?>
<script>
// Daily Video
Xclient.render({
    clientId: 'ab56181', // Client ID
    product: 'tecvid', // Product code (cal,news,sumr-d,sumr-w,sumr-m,tecan,tecvid,sgnl)
    lang: 'en',
    assetmode: 'forex', // binary or forex (sgnl only)
    asset: '$csib', //retreive data for specific assets ($csib)
    limit: 100, // data limit (news, sumr-d,sumr-w,sumr-m, tecan, tecvid only)

    getData: function(data) {
        var data = JSON.parse(data).data;
        if (data) {
            var content = '';
            for (var i = 0; i < data.length; i++) {
                var currentItem = data[i];
                if (i === 0) {
                    content +=  '<p class="top_news">Recently Uploaded</p>' +
                            '<div class="col-sm-12 tecvid-item">' +
                                '<div class="col-sm-12 tecvid-image">' +
                                    '<div class="play_img"></div>' + 
                                    '<img class="img-responsive" src="https://img.youtube.com/vi/'+currentItem.videoid+'/0.jpg">' + 
                                '</div>' +
                                '<span style="display: none;" class="videoid">' + currentItem.videoid + '</span>' +
                                '<div class="title-details">' +
                                    '<div class="title">' + currentItem.title + '</div>' +
                                    '<div class="date">' + currentItem.postedat + '</div>' +
                                '</div>' +
                            '</div>';
                } else if (i === 3 && $(window).width() > 1024) {
                    content +=  '<div class="col-sm-12 tecvid-item disabled">' +
                                '</div>';
                } else if (i === 4 && $(window).width() > 1024) {
                    content +=  '<p class="latest_news">All Activity</p>' +
                            '<div class="col-sm-12 tecvid-item">' +
                                '<div class="col-sm-12 tecvid-image">' +
                                    '<div class="play_img"></div>' + 
                                    '<img class="img-responsive" src="https://img.youtube.com/vi/'+currentItem.videoid+'/0.jpg">' + 
                                '</div>' +
                                '<span style="display: none;" class="videoid">' + currentItem.videoid + '</span>' +
                                '<div class="title-details">' +
                                    '<div class="title">' + currentItem.title + '</div>' +
                                    '<div class="date">' + currentItem.postedat + '</div>' +
                                '</div>' +
                            '</div>';
                } else if (i === 3 && $(window).width() == 1024) {
                    content += '<p class="row latest_news">All Activity</p>' +
                            '<div class="col-sm-12 tecvid-item">' +
                                '<div class="col-sm-12 tecvid-image">' +
                                    '<div class="play_img"></div>' + 
                                    '<img class="img-responsive" src="https://img.youtube.com/vi/'+currentItem.videoid+'/0.jpg">' + 
                                '</div>' +
                                '<span style="display: none;" class="videoid">' + currentItem.videoid + '</span>' +
                                '<div class="title-details">' +
                                    '<div class="title">' + currentItem.title + '</div>' +
                                    '<div class="date">' + currentItem.postedat + '</div>' +
                                '</div>' +
                            '</div>';
                            
                } else if (i === 2 && $(window).width() == 768) {
                    content += '<p class="row latest_news">All Activity</p>' +
                            '<div class="col-sm-12 tecvid-item">' +
                                '<div class="col-sm-12 tecvid-image">' +
                                    '<div class="play_img"></div>' + 
                                    '<img class="img-responsive" src="https://img.youtube.com/vi/'+currentItem.videoid+'/0.jpg">' + 
                                '</div>' +
                                '<span style="display: none;" class="videoid">' + currentItem.videoid + '</span>' +
                                '<div class="title-details">' +
                                    '<div class="title">' + currentItem.title + '</div>' +
                                    '<div class="date">' + currentItem.postedat + '</div>' +
                                '</div>' +
                            '</div>';
                } else if (i === 1 && $(window).width() < 768) {
                    content += '<p class="row latest_news">All Activity</p>' +
                            '<div class="col-sm-12 tecvid-item">' +
                                '<div class="col-sm-12 tecvid-image">' +
                                    '<div class="play_img"></div>' + 
                                    '<img class="img-responsive" src="https://img.youtube.com/vi/'+currentItem.videoid+'/0.jpg">' + 
                                '</div>' +
                                '<span style="display: none;" class="videoid">' + currentItem.videoid + '</span>' +
                                '<div class="title-details">' +
                                    '<div class="title">' + currentItem.title + '</div>' +
                                    '<div class="date">' + currentItem.postedat + '</div>' +
                                '</div>' +
                            '</div>';
                } else {
                    content += '<div class="col-sm-12 tecvid-item">' +
                                '<div class="col-sm-12 tecvid-image">' +
                                    '<div class="play_img"></div>' + 
                                    '<img class="img-responsive" src="https://img.youtube.com/vi/'+currentItem.videoid+'/0.jpg">' + 
                                '</div>' +
                                '<span style="display: none;" class="videoid">' + currentItem.videoid + '</span>' +
                                '<div class="title-details">' +
                                    '<div class="title">' + currentItem.title + '</div>' +
                                    '<div class="date">' + currentItem.postedat + '</div>' +
                                '</div>' +
                            '</div>';
                }
            }

            $("#mte-daily-video-container").append(content);
        }
    },
    added: function(message) {
        // console.log("added", message);
    },
    changed: function(message) {
        // console.log("changed", message);
    },
    removed: function(message) {
        // console.log("removed", message);
    }
}, '#mte-daily-video-container');

// Daily Video Popup
var videoId = $(this).find(".videoid").text();
$(document).on("click", ".tecvid-item", function() {
    var content = '';
   
    
     content += '<div class="full-video">' +
                    '<span class="close_btn">Close </span>' +
                    '<iframe id="youtube" src="https://www.youtube.com/embed/' + $(this).find(".videoid").text() + '?autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>' +
                    '<div class="col-sm-12">' +
                        '<div class="title">' + $(this).find(".title").text() + '</div>' +
                        '<div class="date">' + $(this).find(".date").text() + '</div>' +
                        '<div class="details">' + $(this).find(".details").text() + '</div>' +
                    '</div>' +
                    '<div class="trade_button_news">Trade</div>' +
                '</div>';
                
    $('#mte-daily-video-inner').html(content);
    if($(window).width() > 1024) {
        // Animation
        (function() {
            var appear, delay, disappear, i, j, len, offset, ref;
            ref = $(".tecvid-item");
            for (j = 0, len = ref.length; j < len; j++) {
                i = ref[j];
                offset = i.offsetLeft + i.offsetTop;
                delay = offset / 1000;
                // TODO: Calculate delay relative to first element offset, not absolute offset
                // BONUS: Map delay to an easing function
                $(i).css({
                'transition-delay': `${delay * 0.3}s`,
                'transition-duration': `${0.2}s`
                });
            }
            appear = function() {
                return setTimeout((function() {
                $('.tecvid-item').addClass('visible');
                }), 500);
            };
            appear();

        }).call(this);
        setTimeout(function() {
            $('#mte-daily-video-container').hide();
        }, 2000);
        setTimeout(function() {
            $('#mte-daily-video-inner').show();
        }, 2000);
    } else {
        $('#mte-daily-video-container').hide();
        $('#mte-daily-video-inner').show();
         $('html, body').animate({
        scrollTop: ($('#mte-daily-video-inner').offset().top - 100)
        },500);
    }
});
$(document).on("click", "#mte-daily-video-inner .close_btn", function() {
    $('.tecvid-item').removeClass('visible');
    $('#mte-daily-video-inner').hide();
    $('#mte-daily-video-container').show();
    event.preventDefault();
    $("#youtube").attr('src', '');
})
$(document).on("click", "#mte-daily-video-inner .trade_button_news", function() {
    if (widgets.isLogged()) {
        if (widgets.isMobileDevice()) {
            if($(window).width() == 1024) {
                window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'traderoom?product=realil';
            } else {
                window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'traderoom?product=realm';
            }
        } else {
            window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'traderoom';
        }
    } else {
        window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'registration';
    };
    $("#youtube").attr('src', '');
    return;
});


</script>

<style>
.tecvid-item {
    transform: scale(1);
    transition-timing-function: ease-out;
}
.visible {
    opacity: 1;
    transform: scale(0)
}
</style>