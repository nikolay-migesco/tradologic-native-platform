<?php
/**
 * Template Name: MTE news page
 */
    // $userId = json_decode(stripslashes($_COOKIE['widgetUserData']), true)["id"];
    // $redirectDestination = $userId ? '/traderoom' : '';

    // if (json_decode(stripslashes($_COOKIE['widgetUserData']), true)["Package"] < 250) {
    //     $url         =  "//{$_SERVER['HTTP_HOST']}";
    //     $escaped_url = htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' );
    //     header("Location: " . $escaped_url . $redirectDestination);
    // }
 ?>
<script>
Xclient.render({
    clientId: 'ab56181', // Client ID
    product: 'news', // Product code (cal,news,sumr-d,sumr-w,sumr-m,tecan,tecvid,sgnl)
    lang: 'en',
    assetmode: 'forex', // binary or forex (sgnl only)
    asset: '$csi', //retreive data for specific assets ($csib)
    limit: 100, // data limit (news, sumr-d,sumr-w,sumr-m, tecan, tecvid only)

    getData: function(data) {
        var data = JSON.parse(data).data;
        if (data) {
            var content = '';
            for (var i = 0; i < data.length; i++) {
                var currentItem = data[i];
                if (i === 0) {
                    content +=  '<p class="top_news">Today`s Top News</p>' +
                            '<div class="col-sm-12 news-item">' +
                                '<div>' +
                                    '<div class="col-sm-5 news-image">' +
                                        '<div class="image"><img class="img-responsive" src="' + currentItem.imageurl + '">' + '</div>' +
                                    '</div>' +
                                    '<div class="col-sm-7">' +
                                        '<div class="title">' + currentItem.title + '</div>' +
                                        '<div class="date">' + currentItem.postedat + '</div>' +
                                        '<div class="details">' + currentItem.description + '</div>' +
                                    '</div>' +
								'</div>' +
                            '</div>' +
                            '<p class="latest_news">Latest News</p>';
                } else {
                    content += '<div class="col-md-3 news-item">' +
                                '<div>' +
                                    '<div class="col-sm-12 news-image">' +
                                        '<div class="image"><img class="img-responsive" src="' + currentItem.imageurl + '">' + '</div>' +
                                    '</div>' +
                                    '<div class="title">' + currentItem.title + '</div>' +
                                    '<div class="date">' + currentItem.postedat + '</div>' +
									'<div class="details" >' + currentItem.description + '</div>' +
								'</div>' +
							'</div>';
                }
            }

            $("#mte-news-container").append(content);
        }
    },

    added: function(message) {
        // console.log("added", message);
    },
    changed: function(message) {
        //console.log("changed", message);
    },
    removed: function(message) {
        //console.log("removed", message);
    }
}, '#mte-news-container');

$(document).on("click", ".news-item", function() {
    var content = '';
    content += '<div class="full-article">' +
                    '<span class="close_btn">Close </span>' +
                    '<div class="col-sm-12 news-image">' +
                        '<div class="image"><img class="img-responsive" src="' + $(this).find("img").attr('src') + '">' + '</div>' +
                    '</div>' +
                    '<div class="col-sm-12">' +
                        '<div class="title">' + $(this).find(".title").text() + '</div>' +
                        '<div class="date">' + $(this).find(".date").text() + '</div>' +
                        '<div class="details">' + $(this).find(".details").text() + '</div>' +
                    '</div>' +
                    '<div class="trade_button_news">Trade</div>' +
				'</div>';

    $('#mte-new-inner').html(content);
    if($(window).width() > 1024) {
        // Animation
        (function() {
            var appear, delay, disappear, i, j, len, offset, ref;
            ref = $(".news-item");
            for (j = 0, len = ref.length; j < len; j++) {
                i = ref[j];
                offset = i.offsetLeft + i.offsetTop;
                delay = offset / 1000;
                // TODO: Calculate delay relative to first element offset, not absolute offset
                // BONUS: Map delay to an easing function
                $(i).css({
                'transition-delay': `${delay * 0.3}s`,
                'transition-duration': `${0.2}s`
                });
            }
            appear = function() {
                return setTimeout((function() {
                $('.news-item').addClass('visible');
                }), 500);
            };
            appear();

        }).call(this);
        setTimeout(function() {
            $('#mte-news-container').hide();
        }, 2000);
        setTimeout(function() {
            $('#mte-new-inner').show();
        }, 2000);
    } else {
        $('#mte-news-container').hide();
        $('#mte-new-inner').show();
        $('html, body').animate({
        scrollTop: ($('#mte-new-inner').offset().top - 100)
        },500);
    }
});
$(document).on("click", "#mte-new-inner .close_btn", function() {
    $('.news-item').removeClass('visible');
    $('#mte-new-inner').hide();
    $('#mte-news-container').show();
    
});
$(document).on("click", "#mte-new-inner .trade_button_news", function() {
    if (widgets.isLogged()) {
        if (widgets.isMobileDevice()) {
            if($(window).width() == 1024) {
                window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'traderoom?product=realil';
            } else {
                window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'traderoom?product=realm';
            }
        } else {
            window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'traderoom';
        }
    } else {
        window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'registration';
    };
    return;
});
</script>

<style>
.news-item {
    transform: scale(1);
    transition-timing-function: ease-out;
}
.visible {
    opacity: 1;
    transform: scale(0)
}
</style>