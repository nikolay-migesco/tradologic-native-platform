<?php
/**
 * Template Name: Education Custom
 */
get_header();

$hasPackage = isset(json_decode(stripslashes($_COOKIE['widgetUserData']), true)["Package"]);
$userId = json_decode(stripslashes($_COOKIE['widgetUserData']), true)["id"];
$parentId = json_decode(stripslashes($_COOKIE['widgetUserData']), true)["ParentUserId"];
$hasParent = ( isset(json_decode(stripslashes($_COOKIE['widgetUserData']), true)["ParentUserId"]) ? $userId != $parentId : false );
$isVerified = json_decode(stripslashes($_COOKIE['widgetUserData']), true)["isVerified"];

if ($hasPackage || $hasParent || $isVerified) {
    $url         =  "//{$_SERVER['HTTP_HOST']}";
    $escaped_url = htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' );
    header("Location: " . $escaped_url . '/traderoom');
}

switch (ICL_LANGUAGE_CODE) {

     case "de":
        $language = "de";
        break;
    case "ja":
        $language = "jp";
        break;
    case "ar":
        $language = "ar";
        break;
    case "zh-hans":
        $language = "cn";
        break;
    case "it":
        $language = "it";
        break;
    case "pl":
        $language = "pl";
        break;
    case "pt-pt":
        $language = "pt";
        break;
    case "ru":
        $language = "ru";
        break;
    case "tr":
        $language = "tr";
        break;
	case "fr":
        $language = "fr";
        break;
	case "es":
        $language = "es";
        break;
    default:
        $language = "en";
}
wp_enqueue_style( 'education');
?>
<canvas id="waves" height="450"></canvas>
<div class="container main">
<div class="container-fluid">
    <div class="row entry-content-row">
        <div class="col-sm-3 first-box">
            <div class="heading">
                <span class="box-title main-heading">Forex Trading</span>
                <span class="box-title second-heading"><h1>Basics</span>
            </div>
            <div class="box-title overview-box">
                <p> An overview of the forex market to help you determine if getting involved is for you.</p>
            </div> 
            <div class="box-body">
                <span class="price-circle">
                    <span id="price">250</span>
                    <p>EUR</p>
                </span>
                <span class="box-body-image basics"></span>
                <span class="overview-footer">By the end of this course you should be able to make a well-educated decision on whether or not to continue studying the Forex market.</span>
                <div class="box-buttons">
                     <div class="button buy-now" id="buyBasics" value="250">Buy Now</div>
                </div>
            </div>   
        </div>
        <div class="col-sm-3 second-box">
            <div class="heading">
                    <span class="box-title main-heading">Forex Trading</span>
                    <span class="box-title second-heading"><h1>A-Z</span>
                </div>
                <div class="box-title overview-box">
                    <p>Forex trading - beginners and experienced. Learn Trading by doing it! Live examples: forex with real money.</p>
                </div> 
                <div class="box-body">
                    <span class="price-circle">
                        <span id="price">1200</span>
                        <p>EUR</p>
                    </span>
                    <span class="box-body-image alphabet"></span>
                    <span class="overview-footer">
						Successfully analyse the Forex market using technical analysis<br>
						Understand Forex Terminology, like ask, bid, spread, margin etc.<br>  
						<div>
						 <span id="text">Be able to follow one of the successful strategies, or create one that works for you<br>
										You will know how to manage your risk when trading Forex<br>
										You will be able to select a reputable broker and understand the basics of regulations<br>
										A full and complete understanding of what Forex trading is, how it works, how to profit from it and how to manage your risk<br>
										You will learn how to use the fundamentals (forex news events) to decide when to trade and when not to.<br>
						</div>
						 </span>
					</span>
                    <div class="box-buttons">
                       <div class="button read-more" id="toggle">Read More</div>
                        <div class="button buy-now" id="buyAlphabet" value="1200">Buy Now</div>
                    </div>
                </div>   
        </div>
        <div class="col-sm-3 third-box">
            <div class="heading">
                    <span class="box-title main-heading">Forex Trading</span>
                    <span class="box-title second-heading"><h1>Complete Guide</span>
                </div>
                <div class="box-title overview-box">
                    <p>Forex Trading Program - Learn How to be a Successful Trader in the Currency Market - With Live Examples.</p>
                </div> 
                <div class="box-body">
                    <span class="price-circle">
                        <span id="price">3000</span>
                        <p>EUR</p>
                    </span>
                    <div class="mostpopular"><span class="best-seller">
                        <span>Most Popular</span>
                    </span></div>
                    <span class="box-body-image complete"></span>
                    <span class="overview-footer">
						Have a full understanding of how Forex Market works<br>
						Understand what leverage is and how to apply it<br>  
						<div>
						 <span id="text2">Understand the main concepts such as PIP, lot, margin, spread and much more<br>
							Know the main Forex Sessions and recognize the best time to trade<br>
							Use different types of orders according to the market scenario<br>
							Open an account with a reliable Broker<br>
							Read a Forex chart<br>
							Analyze price movements in a logical way<br>
							Analyze the market using technical and fundamental analysis<br>
							Use the most popular and effective tools of technical analysis<br>
							Understand the main Macroeconomic indicators<br>
							Read the Economic Calendar<br>
							Manage the risk to protect your money<br>
							Avoid mistakes that most traders make or have made in their career<br>
							Start trading in the Forex Market<br>
						</div>
						 </span>
					</span>
                    <div class="box-buttons">
                       <div class="button read-more" id="toggle2">Read More</div>
                        <div class="button buy-now" id="buyAdvanced" value="3000">Buy Now</div>
                    </div>
                </div>   
        </div>
        <!-- <div class="col-sm-3 fourth-box">
            <div class="heading">
                    <span class="box-title main-heading">Forex Trading</span>
                    <span class="box-title second-heading"><h1>ADVANCED Strategy</span>
                </div>
                <div class="box-title overview-box">
                    <p> A Complete - Profitable Forex Trading & Stock Trading Strategy. Master the Swings of the Trends & Earn Consistent Profit</p>
                </div> 
                <div class="box-body">
                    <span class="price-circle">
                        <span id="price">7500</span>
                        <p>EUR</p>
                    </span>
                    <span class="box-body-image advanced"></span>
                    <span class="overview-footer">
						Strong foundation and deep understanding about the upcoming, as well as ongoing trend of the market.<br>
						Time frame selection to trade the swings of a trend<br>  
						<div>
						 <span id="text3">Important price action basics, especially designed to read the market sentiment of a trend.<br>
											Advanced entry and exit strategy to trade the swings of a trend.<br>
											Strategy to trade the valid swing point of a trend.<br>
											Overall, very deep insight of the most reliable and profitable Swing Trading Strategy to make consistent profit from the market.<br>
											Strategy to trade the complex looking invalid swing trading setup on a trend.<br>
						</div>
						 </span>
					</span>
                    <div class="box-buttons">
                       <div class="button read-more" id="toggle3">Read More</div>
                        <div class="button buy-now" id="buyComplete" value="7500">Buy Now</div>
                    </div>
                </div>   
            </div>
        </div> -->
    </div>
</div>
</div></div>
<?php
get_footer();
?>


