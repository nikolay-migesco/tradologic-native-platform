<?php
/**
 * Static Home Page
 *
 */

defineMigescoLayout();

get_header();
?>
<link rel="stylesheet" href="/wp-content/themes/tol-parent/styles/animate.css" type="text/css" media="all"/>
<script type="text/javascript" src="/wp-content/themes/tol-parent/javascript/vendor.js"></script>
<script type="text/javascript" src="/wp-content/themes/tol-parent/javascript/chart.js"></script>
<script type="text/javascript" src="/wp-content/themes/tol-parent/javascript/jquery.parallax.min.js"></script>
<script type="text/javascript" src="/wp-content/themes/tol-parent/javascript/wow.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#tabs_link2 div').click(function () {
            var tab_id = $(this).attr('id');
            $('#tabs_link2 div').removeClass('active');
            $(this).addClass('active');
            $('#tabs_box .tabs_content').hide();
            $('#tabs_box .' + tab_id).show();
        });

        $('#next_tab').click(function () {
            var tab_id = $('#tabs_link2 div.active').attr('id');
            $('#tabs_link2 div').removeClass('active');
            if (tab_id === 'tabs_3') $('#tabs_1').addClass('active').click();
            else $('#' + tab_id).next().addClass('active').click();
        });

        $('#prev_tab').click(function () {
            var tab_id = $('#tabs_link2 div.active').attr('id');
            $('#tabs_link2 div').removeClass('active');
            if (tab_id === 'tabs_1') $('#tabs_3').addClass('active').click();
            else $('#' + tab_id).prev().addClass('active').click();
        });

        $('#chart_close').click(function () {
            if ($('#chart_left').css('display') === 'none') {
                $('#chart_left').animate({width: 'show'});
                $('#chart_close').animate({left: '299px', width: '29px'});
            } else {
                $('#chart_left').animate({width: 'hide'});
                $('#chart_close').animate({left: '0', width: '12px'});
            }
        });

        $('#chart_link a').click(function () {
            $('#chart_left').animate({width: 'hide'});
            $('#chart_close').animate({left: '0', width: '12px'});
        });

        $('#svece').parallax({
            mouseport: $("#header"),
            xparallax: true,
            yparallax: false
        });

        wow = new WOW();
        wow.init();
    });
</script>
<style type="text/css">
    .b_content {
        background-color: #edf1f5 !important;
    }

    .b_content .op-users-only, .width_th {
        width: 100% !important;
    }

    #news_block .news_box .news_name {
        min-height: 117px !important;
    }

    #main_header {
        overflow: hidden;
        margin-bottom: -176px;
        height: 706px;
    }

    #header .padding {
        z-index: 2;
    }

    #svece {
        position: absolute;
        bottom: 0;
        background: url('<?php echo get_template_directory_uri(); ?>/images/home/sveces.png') top center repeat-x;
        height: 360px;
        width: 110%;
        left: 0px;
        z-index: 1;
    }

    #uslovie {
        background: url('<?php echo get_template_directory_uri(); ?>/images/home/lines.jpg') top center repeat-x;
        height: 199px;
    }

    #uslovie .uslovie_box .uslovie_img {
        border-left: 0 !important;
    }

    #uslovie .uslovie_box:last-child .uslovie_img {
        border-right: 0 !important;
    }
</style>

<div id="main_header">
    <?php if (prior_lang() == 'ru'): //RUSSIAN SLIDER?>
        <div id="header"
             style="background:url('<?php echo get_template_directory_uri(); ?>/images/home/blue_mount.jpg') top center no-repeat;position:relative;">
            <div class="padding" style="position:relative;">
                <div id="flag_img"></div>
                <div id="h_slogan">
                    <div class="h_title">Современный брокер<br/>для успешных инвестиций</div>
                    <div class="h_strong">Зарабатывайте с нами:</div>
                    <div class="li_1">Продвинутая торговая платформа</div>
                    <div class="li_2">Выгодные условия торговли</div>
                    <div class="li_3">250+ активов для торговли</div>
                </div>
                <?php include(dirname(__FILE__) . '/widget-short-registration.php'); ?>
                <?php if (isUserLogged()) { ?>
                    <div style="float:right;">
                        <div class="op-real-account">
                            <a href="/traderoom"
                               style="margin-top:240px;display:block;height:50px;background-color:#fff;border:1px solid #244674;color:#244674;font: 17px/48px 'PT Sans' !important;text-decoration:none;padding-left:31px;padding-right:31px;">Начать
                                торговать</a>
                            <a href="/deposit-account"
                               style="margin-top:20px;display:block;height:50px;background-color:#69bb1b;border:1px solid white;color:#fff;font: 17px/48px 'PT Sans' !important;text-decoration:none;padding-left:31px;padding-right:31px;">Пополнить счет</a>
                        </div>
                        <div class="op-demo-account">
                            <a href="/traderoom"
                               style="margin-top:240px;display:block;height:50px;background-color:#244674;border:1px solid white;color:#fff;font: 17px/48px 'PT Sans' !important;text-decoration:none;padding-left:31px;padding-right:31px;">Начать торговать</a>
                            <a href="/poshagovoe-rukovodstvo"
                               style="margin-top:20px;display:block;height:50px;background-color:#244674;border:1px solid white;color:#fff;font: 17px 'PT Sans' !important;text-decoration:none;padding-left:31px;padding-right:31px;padding-top:2px;">Как торговать</a>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div id="svece"></div>
        </div>
    <?php elseif (prior_lang() == 'en'): //ENGLISH SLIDER?>
        <div id="header"
             style="background:url('<?php echo get_template_directory_uri(); ?>/images/home/blue_mount.jpg') top center no-repeat;position:relative;">
            <div class="padding" style="position:relative;">
                <div id="flag_img"></div>
                <div id="h_slogan">
                    <div class="h_title">Leading broker<br/>for successful investments</div>
                    <div class="h_strong">Earn with us:</div>
                    <div class="li_1">Advanced trading platform</div>
                    <div class="li_2">Profitable trading terms</div>
                    <div class="li_3">250+ assets for trading</div>
                </div>
                <?php include(dirname(__FILE__) . '/widget-short-registration-en.php'); ?>
                <?php if (isUserLogged()) { ?>
                    <div style="float:right;">
                        <div class="op-real-account">
                            <a href="/traderoom"
                               style="margin-top:240px;display:block;height:50px;background-color:#FFFFFF; border:1px solid #244674; color:#244674;font: 17px/48px 'PT Sans' !important;text-decoration:none;padding-left:31px;padding-right:31px;">Start trading</a>
                            <a href="/deposit-account"
                               style="margin-top:20px;display:block;height:50px;background-color:#69bb1b;border:1px solid white;color:#fff;font: 17px/48px 'PT Sans' !important;text-decoration:none;padding-left:31px;padding-right:31px;">Deposit account</a>
                        </div>
                        <div class="op-demo-account">
                            <a href="/traderoom"
                               style="margin-top:240px;display:block;height:50px;background-color:#244674;border:1px solid white;color:#fff;font: 17px/48px 'PT Sans' !important;text-decoration:none;padding-left:31px;padding-right:31px;">Start trading</a>
                            <a href="/poshagovoe-rukovodstvo"
                               style="margin-top:20px;display:block;height:50px;background-color:#244674;border:1px solid white;color:#fff;font: 17px 'PT Sans' !important;text-decoration:none;padding-left:31px;padding-right:31px;padding-top:2px;">How to trade</a>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div id="svece"></div>
        </div>
    <?php endif; ?>
</div>


<?php if (prior_lang() == 'ru'): //RUSSIAN HOME?>
    <div id="main_content">

        <div class="b_content">
            <div class="op-users-only">
                <div style="width: 1340px !important;margin: 40px auto 0 !important;">
                    <div class="blue_dotted"><span>ИНФОРМАЦИЯ ДЛЯ УСПЕШНОЙ ТОРГОВЛИ</span></div>
                    <?php
                    $args = array(
                        'posts_per_page' => 1,
                        'offset' => 0,
                        'category' => '',
                        'orderby' => 'date',
                        'order' => 'DESC',
                        'post_type' => 'post',
                        'post_status' => 'publish',
                        'suppress_filters' => true
                    );
                    ?>
                    <div id="news_block">
                        <?php
                        $args['category'] = '249';
                        $section2 = get_posts($args);
                        ?>
                        <div class="news_box">
                            <div class="news_img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/home/program_1.jpg"/>
                                <div><a href="/category/puls-rynka"><span>Пульс рынка</span></a></div>
                            </div>
                            <div class="news_name"><a
                                        href="/category/puls-rynka"><?php echo $section2[0]->post_title ?></a>
                            </div>
                        </div>
                        <?php
                        $args['category'] = '255';
                        $section3 = get_posts($args);
                        ?>
                        <div class="news_box">
                            <div class="news_img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/home/program_2.jpg"/>
                                <div><a href="/category/torgovie-idei"><span>Торговые идеи</span></a></div>
                            </div>
                            <div class="news_name"><a href="/category/torgovie-idei">На чем можно
                                    заработать <?php echo $section3[0]->post_title ?></a></div>
                        </div>
                        <?php
                        $args['category'] = '257';
                        $section4 = get_posts($args);
                        ?>
                        <div class="news_box">
                            <div class="news_img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/home/program_3.jpg"/>
                                <div><a href="/plan-torgovli"><span>План торговли</span></a></div>
                            </div>
                            <div class="news_name"><a
                                        href="/plan-torgovli"><?php echo $section4[0]->post_title ?></a>
                            </div>
                        </div>
                        <?php
                        $args['category'] = '250';
                        $section5 = get_posts($args);
                        ?>
                        <div class="news_box">
                            <div class="news_img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/home/program_5.jpg"/>
                                <div><a href="/ekonomicheskiy-calendar"><span>Экономический<br/>календарь</span></a>
                                </div>
                            </div>
                            <div class="news_name"><a href="/ekonomicheskiy-calendar">Главные финансовые события дня</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="width_th op-guests-only">
            <div id="notebook">
                <div style="padding-top:71px !important;">
                    <div class="chart">
                        <div class="chart__timer">
                            <div class="chart__timer__icon">
                                <div class="timer__circle animated">
                                    <div class="timer__line timer__line--hour"></div>
                                    <div class="timer__line timer__line--minutes"></div>
                                </div>
                            </div>
                            <div class="timer__text">00:<span id="time" class="timer__count">10</span></div>
                        </div>
                        <div class="chart__timer--big">
                            <div class="chart__timer--big__title">Ожидайте закрытия сделки:</div>
                            <div class="chart__timer__icon">
                                <div class="timer__circle animated">
                                    <div class="timer__line timer__line--hour"></div>
                                    <div class="timer__line timer__line--minutes"></div>
                                </div>
                            </div>
                            <div class="timer__text">00:<span id="time" class="timer__count">10</span></div>
                        </div>
                        <div class="chart__btn__box">
                            <div id="chart_up" class="chart__btn"><span class="digital-shadow">Выше</span><span
                                        class="call-arrow"></span></div>
                            <div id="chart_down" class="chart__btn"><span class="digital-shadow">Ниже</span><span
                                        class="put-arrow"></span></div>
                        </div>
                        <div id="chart_pin" class="chart__pin"></div>
                        <div id="rate_line" class="chart__rate_line"></div>
                        <div id="chart_loader"></div>
                        <svg id="chart_arr" width="41px" height="60px" viewbox="0 0 41 60" version="1.1"
                             xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                             class="chart__pin__arr">
                            <g id="arr">
                                <path id="arrdown"
                                      d="M22.2437026,20.4412156 L39.9575698,20.3875046 L39.9790542,38.1882859 L35.6382338,34.0437547 L10.5718276,59.4587938 L0.966358839,50.1453172 L26.2720229,24.8709031 L22.2437026,20.4412156 L22.2437026,20.4412156 Z"
                                      transform="translate(20.472707, 39.923149) rotate(90.000000) translate(-20.472707, -39.923149) "></path>
                                <path id="arrup"
                                      d="M22.078125,0.25976562 L39.7919922,0.20605469 L39.8134766,18.0068359 L35.4726562,13.8623047 L10.40625,39.2773438 L0.80078125,29.9638672 L26.1064453,4.68945312 L22.078125,0.25976562 L22.078125,0.25976562 Z"></path>
                            </g>
                        </svg>
                        <div id="chart_endline" class="chart__endline"></div>
                        <div id="chart_win" class="chart__block chart__block--win">
                            <div class="chart__block__h">Ваш прогноз верен</div>
                            <div class="chart__block__text">При инвестиции $100<br>ваш доход составил бы $180</div>
                            <div class="chart__block__btn__box">
                                <div class="chart__block__btn btn gray"><span>&larr; </span>Новый прогноз</div>
                                <a href="#register" class="chart__block__btn btn green">Начать зарабатывать</a>
                            </div>
                        </div>
                        <div id="chart_loss" class="chart__block chart__block--loss">
                            <div class="chart__block__h">Неверный прогноз</div>
                            <div class="chart__block__text">В случае верного прогноза при инвестиции $100<br>ваш доход
                                составил бы $180
                            </div>
                            <div class="chart__block__btn__box">
                                <div class="chart__block__btn btn gray"><span>&larr; </span>Новый прогноз</div>
                                <a href="#register" class="chart__block__btn btn green">Начать зарабатывать</a>
                            </div>
                        </div>
                        <div class="chart__text--top">Актив BTC/USD</div>
                        <div class="chart__text--bottom"><span>Инвестиция $100</span><span>Прибыль 80%</span><span>Доход $180</span>
                        </div>
                        <div class="loader_wrap">
                            <div class="loader">
                                <svg id="loader" version="1.1" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px"
                                     height="40px" viewbox="0 0 50 50">
                                    <path fill="#000"
                                          d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z"></path>
                                </svg>
                            </div>
                        </div>
                        <div class="chart__wrapper">
                            <svg id="chart"></svg>
                        </div>
                        <div id="chart_left">
                            <div class="title">Финансовые рынки - это просто, быстро и выгодно</div>
                            <div id="chart_link"><a>Попробуйте сейчас</a></div>
                        </div>
                        <div id="chart_close"></div>
                    </div>
                </div>
            </div>
        </div>

        <div id="torg">
            <div class="p_title">
                <p>Как торговать на SimpEx</p>
            </div>
            <div class="padding">
                <div class="shag_box">
                    <div class="shag_title"><span>1</span>Выбрать актив</div>
                    <div><img src="<?php echo get_template_directory_uri(); ?>/images/how-to-simplex/step-1.png"/></div>
                </div>
                <div class="shag_box">
                    <div class="shag_title"><span>2</span>Выбрать направление</div>
                    <div><img src="<?php echo get_template_directory_uri(); ?>/images/how-to-simplex/step-2.png"/></div>
                </div>
                <div class="shag_box">
                    <div class="shag_title"><span>3</span>Открыть сделку</div>
                    <div><img src="<?php echo get_template_directory_uri(); ?>/images/how-to-simplex/step-3.png"/></div>
                </div>
            </div>
            <div class="reg_link">
                <a href="#register" class="op-guests-only">Начать торговать</a>
                <a href="/deposit-account" class="op-users-only">Пополнить счет</a>
            </div>
        </div>

        <div id="blue_content">
            <div class="p_title">Почему Migesco?</div>
            <div class="blue_dotted"><span>Выгодные условия торговли</span></div>
            <div id="uslovie">
                <div class="uslovie_box wow animated zoomIn" style="animation-delay:.1s">
                    <div class="uslovie_img"></div>
                    <div class="uslovie_title">Минимальный депозит<br/>250 рублей</div>
                </div>
                <div class="uslovie_box wow animated zoomIn" style="animation-delay:.3s">
                    <div class="uslovie_img"></div>
                    <div class="uslovie_title">Фиксированные выплаты<br/>до 86%</div>
                </div>
                <div class="uslovie_box wow animated zoomIn" style="animation-delay:.5s;cursor:pointer;"
                     onclick="location.href='/akcii-i-bonusy';">
                    <div class="uslovie_img"></div>
                    <div class="uslovie_title">Бонусы и акции</div>
                </div>
                <div class="uslovie_box wow animated zoomIn" style="animation-delay:.7s;cursor:pointer;"
                     onclick="location.href='/urovni-servisa';">
                    <div class="uslovie_img"></div>
                    <div class="uslovie_title">Сервисы и привилегии</div>
                </div>
            </div>
            <div class="width_th op-guests-only">
                <div class="blue_dotted"><span>ИНФОРМАЦИЯ ДЛЯ УСПЕШНОЙ ТОРГОВЛИ</span></div>
                <?php
                $args = array(
                    'posts_per_page' => 1,
                    'offset' => 0,
                    'category' => '',
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'post_type' => 'post',
                    'post_status' => 'publish',
                    'suppress_filters' => true
                );
                ?>
                <div id="news_block">
                    <?php
                    $args['category'] = '249';
                    $section2 = get_posts($args);
                    ?>
                    <div class="news_box">
                        <div class="news_img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/home/program_1.jpg"/>
                            <div><a href="/category/puls-rynka"><span>Пульс рынка</span></a></div>
                        </div>
                        <div class="news_name"><a
                                    href="/category/puls-rynka"><?php echo $section2[0]->post_title ?></a></div>
                    </div>
                    <?php
                    $args['category'] = '255';
                    $section3 = get_posts($args);
                    ?>
                    <div class="news_box">
                        <div class="news_img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/home/program_2.jpg"/>
                            <div><a href="/category/torgovie-idei"><span>Торговые идеи</span></a></div>
                        </div>
                        <div class="news_name"><a href="/category/torgovie-idei">На чем можно
                                заработать <?php echo $section3[0]->post_title ?></a></div>
                    </div>
                    <?php
                    $args['category'] = '257';
                    $section4 = get_posts($args);
                    ?>
                    <div class="news_box">
                        <div class="news_img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/home/program_3.jpg"/>
                            <div><a href="/plan-torgovli"><span>План торговли</span></a></div>
                        </div>
                        <div class="news_name"><a
                                    href="/plan-torgovli"><?php echo $section4[0]->post_title ?></a>
                        </div>
                    </div>
                    <?php
                    $args['category'] = '250';
                    $section5 = get_posts($args);
                    ?>
                    <div class="news_box">
                        <div class="news_img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/home/program_5.jpg"/>
                            <div><a href="/ekonomicheskiy-calendar"><span>Экономический<br/>календарь</span></a></div>
                        </div>
                        <div class="news_name"><a href="/ekonomicheskiy-calendar">Главные финансовые события дня</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="blue_dotted"><span>Самый большой выбор сделок</span></div>
            <div id="tabs_box">
                <div id="tabs_link2">
                    <div id="tabs_1" class="active"><a>Более 250 активов<br/><span>Валюты, акции, индексы...</span></a>
                    </div>
                    <div id="tabs_2"><a>2 вида инвестирования</a></div>
                    <div id="tabs_3"><a>Время истечения</a></div>
                </div>
                <div class="tabs_content tabs_1" style="display:block;">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/home/aktivi.jpg"/>
                </div>
                <div class="tabs_content tabs_2">
                    <div id="game_desc">
                        <div class="game_1">
                            <p>
                                <strong>ProfEx</strong> - механизм торговли с использованием заемных средств,
                                позволяющий обменять один актив на доллар США или на другой актив. Для торговли на
                                ProfEx Migesco предлагает более 250 активов, среди которых все известные криптовалюты
                                (Bitcoin, Ethereum, Ripple и другие), акции крупных компаний (такие как Facebook,
                                Google, Apple), товары (WTI, Brent, золото).
                            </p>
                        </div>
                        <div class="game_2">
                            <p>
                                <strong>SimplEx</strong> - новая форма исполнения сделок, несколько отличающуюся от
                                традиционных бинарных опционов, но имеющую в основе именно этот механизм. SimplEx
                                отличается упрощенным механизмом заключения сделок, большим выбором активов для торговли
                                и возможностью самостоятельно определять соотношение потенциальных рисков и прибыли.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="tabs_content tabs_3">
                    <div class="tab_time"></div>
                    <div class="time_text">
                        <div>
                            <p>
                                Время истечения - это момент, когда проверяется оправдался ли ваш прогноз или нет. В
                                отличие от других инструментов финансового рынка, как на ProfEx, так и на SimplEx время
                                истечения по сделке устанавливается самим трейдером.
                            </p>
                        </div>
                        <div style="padding-top:55px;">
                            <p>
                                Выбор времени истечения зависит исключительно от торговой стратегии, которой
                                придерживается трейдер. В случае установки Stop Loss и Take Profit временем истечения
                                является момент достижения установленных целевых уровней.
                            </p>
                        </div>
                    </div>
                    <div style="clear:both;"></div>
                </div>
                <div id="prev_tab"></div>
                <div id="next_tab"></div>
            </div>
            <div class="reg_link">
                <a href="#register" class="op-guests-only">Начать торговать</a>
                <a href="/deposit-account" class="op-users-only">Пополнить счет</a>
            </div>
        </div>

        <div id="white_content">
            <div class="p_title">Платформа, заслужившая доверие</div>
            <div class="white_dotted"><span>На рынке с 2012 года</span></div>
            <div id="awards">
                <div class="award_box">
                    <div class="award_img">
                        <div><img src="<?php echo get_template_directory_uri(); ?>/images/home/award_1.png"/></div>
                    </div>
                    <div class="award_title">IAIR FOREX AWARDS</div>
                </div>
                <div class="award_box">
                    <div class="award_img">
                        <div><img src="<?php echo get_template_directory_uri(); ?>/images/home/award_2.png"/></div>
                    </div>
                    <div class="award_title">Forex Magnates Awards</div>
                </div>
                <div class="award_box">
                    <div class="award_img">
                        <div><img src="<?php echo get_template_directory_uri(); ?>/images/home/award_3.png"/></div>
                    </div>
                    <div class="award_title">Global Banking &amp;<br/>Finance Review AWARD</div>
                </div>
                <div class="award_box">
                    <div class="award_img">
                        <div><img src="<?php echo get_template_directory_uri(); ?>/images/home/award_4.png"/></div>
                    </div>
                    <div class="award_title">EGR AWARD 2015</div>
                </div>
            </div>
            <div style="clear:both;"></div>
            <div class="p_title">Выплаты удобным способом</div>
            <div id="pay">
                <div class="pay_box">
                    <div class="pay_img"></div>
                </div>
                <div class="pay_box">
                    <div class="pay_img"></div>
                </div>
                <div class="pay_box">
                    <div class="pay_img"></div>
                </div>
                <div class="pay_box">
                    <div class="pay_img"></div>
                </div>
                <div class="pay_box">
                    <div class="pay_img"></div>
                </div>
            </div>
            <div style="clear:both;"></div>
        </div>
    </div>
<?php elseif (prior_lang() == 'en'): //ENGLISH HOME?>
    <div id="main_content">

        <div class="b_content">
            <div class="op-users-only">
                <div style="width: 1340px !important;margin: 40px auto 0 !important;">
                    <div class="blue_dotted"><span>HOW TO MAKE YOUR TRADING PROFITABLE</span></div>
                    <?php
                    $args = array(
                        'posts_per_page' => 1,
                        'offset' => 0,
                        'category' => '',
                        'orderby' => 'date',
                        'order' => 'DESC',
                        'post_type' => 'post',
                        'post_status' => 'publish',
                        'suppress_filters' => true
                    );
                    ?>
                    <div id="news_block">
                        <?php
                        $args['category'] = '269';
                        $section2 = get_posts($args);
                        ?>
                        <div class="news_box">
                            <div class="news_img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/home/program_1.jpg"/>
                                <div><a href="/en/category/market-pulse"><span>Market pulse</span></a></div>
                            </div>
                            <div class="news_name"><a
                                        href="/en/category/market-pulse"><?php echo $section2[0]->post_title ?></a>
                            </div>
                        </div>
                        <?php
                        $args['category'] = '270';
                        $section3 = get_posts($args);
                        ?>
                        <div class="news_box">
                            <div class="news_img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/home/program_2.jpg"/>
                                <div><a href="/en/category/trading-signals"><span>Trading ideas</span></a></div>
                            </div>
                            <div class="news_name"><a href="/en/category/trading-signals">How you can
                                    earn <?php echo $section3[0]->post_title ?></a></div>
                        </div>
                        <?php
                        $args['category'] = '265';
                        $section4 = get_posts($args);
                        ?>
                        <div class="news_box">
                            <div class="news_img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/home/program_3.jpg"/>
                                <div><a href="/en/trading-plan"><span>Trading plan</span></a></div>
                            </div>
                            <div class="news_name"><a
                                        href="/en/trading-plan"><?php echo $section4[0]->post_title ?></a>
                            </div>
                        </div>
                        <div class="news_box">
                            <div class="news_img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/home/program_4.jpg"/>
                                <div><a href="/en/earnings-calendar"><span>Earning Calendar</span></a></div>
                            </div>
                            <div class="news_name"><a href="/en/earnings-calendar">Earning Calendar Learn more about
                                    world-famous companies stocks</a></div>
                        </div>
                        <div class="news_box">
                            <div class="news_img">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/home/program_5.jpg"/>
                                <div><a href="/en/economic-calendar"><span>Economic<br/>calendar</span></a></div>
                            </div>
                            <div class="news_name"><a href="/en/economic-calendar">Main financial events of the day</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="width_th op-guests-only">
            <div id="notebook">
                <div style="padding-top:71px !important;">
                    <div class="chart">
                        <div class="chart__timer">
                            <div class="chart__timer__icon">
                                <div class="timer__circle animated">
                                    <div class="timer__line timer__line--hour"></div>
                                    <div class="timer__line timer__line--minutes"></div>
                                </div>
                            </div>
                            <div class="timer__text">00:<span id="time" class="timer__count">10</span></div>
                        </div>
                        <div class="chart__timer--big">
                            <div class="chart__timer--big__title">Time remaining:</div>
                            <div class="chart__timer__icon">
                                <div class="timer__circle animated">
                                    <div class="timer__line timer__line--hour"></div>
                                    <div class="timer__line timer__line--minutes"></div>
                                </div>
                            </div>
                            <div class="timer__text">00:<span id="time" class="timer__count">10</span></div>
                        </div>
                        <div class="chart__btn__box">
                            <div id="chart_up" class="chart__btn"><span class="digital-shadow">Call</span><span
                                        class="call-arrow"></span></div>
                            <div id="chart_down" class="chart__btn"><span class="digital-shadow">Put</span><span
                                        class="put-arrow"></span></div>
                        </div>
                        <div id="chart_pin" class="chart__pin"></div>
                        <div id="rate_line" class="chart__rate_line"></div>
                        <div id="chart_loader"></div>
                        <svg id="chart_arr" width="41px" height="60px" viewbox="0 0 41 60" version="1.1"
                             xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                             class="chart__pin__arr">
                            <g id="arr">
                                <path id="arrdown"
                                      d="M22.2437026,20.4412156 L39.9575698,20.3875046 L39.9790542,38.1882859 L35.6382338,34.0437547 L10.5718276,59.4587938 L0.966358839,50.1453172 L26.2720229,24.8709031 L22.2437026,20.4412156 L22.2437026,20.4412156 Z"
                                      transform="translate(20.472707, 39.923149) rotate(90.000000) translate(-20.472707, -39.923149) "></path>
                                <path id="arrup"
                                      d="M22.078125,0.25976562 L39.7919922,0.20605469 L39.8134766,18.0068359 L35.4726562,13.8623047 L10.40625,39.2773438 L0.80078125,29.9638672 L26.1064453,4.68945312 L22.078125,0.25976562 L22.078125,0.25976562 Z"></path>
                            </g>
                        </svg>
                        <div id="chart_endline" class="chart__endline"></div>
                        <div id="chart_win" class="chart__block chart__block--win">
                            <div class="chart__block__h">Your forecast is correct</div>
                            <div class="chart__block__text">If you invest $100<br>you'll get profit $180</div>
                            <div class="chart__block__btn__box">
                                <div class="chart__block__btn btn gray"><span>&larr; </span>New forecast</div>
                                <a href="#register" class="chart__block__btn btn green">Start trading</a>
                            </div>
                        </div>
                        <div id="chart_loss" class="chart__block chart__block--loss">
                            <div class="chart__block__h">Wrong forecast</div>
                            <div class="chart__block__text">In case of correct forecast if you invest $100<br>you'll get
                                profit $180
                            </div>
                            <div class="chart__block__btn__box">
                                <div class="chart__block__btn btn gray"><span>&larr; </span>New forecast</div>
                                <a href="#register" class="chart__block__btn btn green">Start trading</a>
                            </div>
                        </div>
                        <div class="chart__text--top">Asset USD/RUB</div>
                        <div class="chart__text--bottom"><span>Investment $100</span><span>Profit 80%</span>
                            <span>Payout $180</span>
                        </div>
                        <div class="loader_wrap">
                            <div class="loader">
                                <svg id="loader" version="1.1" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px"
                                     height="40px" viewbox="0 0 50 50">
                                    <path fill="#000"
                                          d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z"></path>
                                </svg>
                            </div>
                        </div>
                        <div class="chart__wrapper">
                            <svg id="chart"></svg>
                        </div>
                        <div id="chart_left">
                            <div class="title">Financial markets - it is easy, fast and profitable</div>
                            <div id="chart_link"><a>Start now</a></div>
                        </div>
                        <div id="chart_close"></div>
                    </div>
                </div>
            </div>
        </div>

        <div id="torg">
            <div class="p_title">
                <p>How to trade SimpEx</p>
            </div>
            <div class="padding">
                <div class="shag_box">
                    <div class="shag_title"><span>1</span>Choose asset</div>
                    <div><img src="<?php echo get_template_directory_uri(); ?>/images/how-to-simplex/step-1.png"/></div>
                </div>
                <div class="shag_box">
                    <div class="shag_title"><span>2</span>Choose direction</div>
                    <div><img src="<?php echo get_template_directory_uri(); ?>/images/how-to-simplex/step-2.png"/></div>
                </div>
                <div class="shag_box">
                    <div class="shag_title"><span>3</span>Open trade</div>
                    <div><img src="<?php echo get_template_directory_uri(); ?>/images/how-to-simplex/step-3.png"/></div>
                </div>
            </div>
            <div class="reg_link">
                <a href="#register" class="op-guests-only">Start trading</a>
                <a href="/deposit-account" class="op-users-only">Deposit</a>
            </div>
        </div>

        <div id="blue_content">
            <div class="p_title">Why Migesco?</div>
            <div class="blue_dotted"><span>Profitable trading terms</span></div>
            <div id="uslovie">
                <div class="uslovie_box wow animated zoomIn" style="animation-delay:.1s">
                    <div class="uslovie_img"></div>
                    <div class="uslovie_title">Deposit amount<br/>$5</div>
                </div>
                <div class="uslovie_box wow animated zoomIn" style="animation-delay:.3s">
                    <div class="uslovie_img"></div>
                    <div class="uslovie_title">Fixed payout<br/>till 88%</div>
                </div>
                <div class="uslovie_box wow animated zoomIn" style="animation-delay:.5s;cursor:pointer;"
                     onclick="location.href='/akcii-i-bonusy';">
                    <div class="uslovie_img"></div>
                    <div class="uslovie_title">Bonuses and Promo</div>
                </div>
                <div class="uslovie_box wow animated zoomIn" style="animation-delay:.7s;cursor:pointer;"
                     onclick="location.href='/urovni-servisa';">
                    <div class="uslovie_img"></div>
                    <div class="uslovie_title">Services and privileges</div>
                </div>
            </div>
            <div class="width_th op-guests-only">
                <div class="blue_dotted"><span>HOW TO MAKE YOUR TRADING PROFITABLE</span></div>
                <?php
                $args = array(
                    'posts_per_page' => 1,
                    'offset' => 0,
                    'category' => '',
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'post_type' => 'post',
                    'post_status' => 'publish',
                    'suppress_filters' => true
                );
                ?>
                <div id="news_block">
                    <?php
                    $args['category'] = '269';
                    $section2 = get_posts($args);
                    ?>
                    <div class="news_box">
                        <div class="news_img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/home/program_1.jpg"/>
                            <div><a href="/en/category/market-pulse"><span>Market pulse</span></a></div>
                        </div>
                        <div class="news_name"><a
                                    href="/en/category/market-pulse"><?php echo $section2[0]->post_title ?></a></div>
                    </div>
                    <?php
                    $args['category'] = '270';
                    $section3 = get_posts($args);
                    ?>
                    <div class="news_box">
                        <div class="news_img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/home/program_2.jpg"/>
                            <div><a href="/en/category/trading-signals"><span>Trading ideas</span></a></div>
                        </div>
                        <div class="news_name"><a href="/en/category/trading-signals">How you can
                                earn <?php echo $section3[0]->post_title ?></a></div>
                    </div>
                    <?php
                    $args['category'] = '265';
                    $section4 = get_posts($args);
                    ?>
                    <div class="news_box">
                        <div class="news_img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/home/program_3.jpg"/>
                            <div><a href="/en/trading-plan"><span>Trading plan</span></a></div>
                        </div>
                        <div class="news_name"><a
                                    href="/en/trading-plan"><?php echo $section4[0]->post_title ?></a></div>
                    </div>
                    <div class="news_box">
                        <div class="news_img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/home/program_4.jpg"/>
                            <div><a href="/en/earnings-calendar"><span>Earning Calendar</span></a></div>
                        </div>
                        <div class="news_name"><a href="/en/earnings-calendar">Earning Calendar Learn more about
                                world-famous companies stocks</a></div>
                    </div>
                    <div class="news_box">
                        <div class="news_img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/home/program_5.jpg"/>
                            <div><a href="/en/economic-calendar"><span>Economic<br/>calendar</span></a></div>
                        </div>
                        <div class="news_name"><a href="/en/economic-calendar">Main financial events of the day</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="blue_dotted"><span>The largest selection of deals</span></div>
            <div id="tabs_box">
                <div id="tabs_link2">
                    <div id="tabs_1" class="active"><a>250+ assets for trading<br/><span>Currencies, stocks,
                                indicies...</span></a>
                    </div>
                    <div id="tabs_2"><a>2 types of  investment</a></div>
                    <div id="tabs_3"><a>Expiration time</a></div>
                </div>
                <div class="tabs_content tabs_1" style="display:block;">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/home/aktivi.jpg"/>
                </div>
                <div class="tabs_content tabs_2">
                    <div id="game_desc">
                        <div class="game_1">
                            <p>
                                <strong>ProfEx</strong> - is a is a leveraged trading mechanism that allows you to
                                exchange one asset for the US dollar or another asset. Migesco offers over 250 assets
                                for trading, that list includes all known cryptocurrencies (Bitcoin, Ethereum, Ripple
                                and others), large companies' stocks (Facebook, Google, Apple), commodities (WTI, Brent,
                                gold).
                            </p>
                        </div>
                        <div class="game_2">
                            <p>
                                <strong>SimplEx</strong> - is a new form of trades execution, it differs from the
                                traditional binary options, but it is based on same mechanism. SimplEx offers simplified
                                mechanism to open trades, a large choice of assets for trading. and the ability to
                                determine the ratio of potential risks and profits.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="tabs_content tabs_3">
                    <div class="tab_time"></div>
                    <div class="time_text">
                        <div>
                            <p>
                                The expiration time is the moment when it is checked whether your forecast was true or
                                not. Unlike the other financial market instruments, both at ProfEx and SimplEx trade
                                expiration time is set by the trader.
                            </p>
                        </div>
                        <div style="padding-top:55px;">
                            <p>
                                The expiration time depends solely on the trading strategy, which trader follows. In
                                case of setting Stop Loss and Take Profit expiration is the moment of reaching the
                                established target levels.
                            </p>
                        </div>
                    </div>
                    <div style="clear:both;"></div>
                </div>
                <div id="prev_tab"></div>
                <div id="next_tab"></div>
            </div>
            <div class="reg_link">
                <a href="#register" class="op-guests-only">Start trading</a>
                <a href="/deposit-account" class="op-users-only">Deposit account</a>
            </div>
        </div>

        <div id="white_content">
            <div class="p_title">The most trusted platform</div>
            <div class="white_dotted"><span>Works from 2012</span></div>
            <div id="awards">
                <div class="award_box">
                    <div class="award_img">
                        <div><img src="<?php echo get_template_directory_uri(); ?>/images/home/award_1.png"/></div>
                    </div>
                    <div class="award_title">IAIR FOREX AWARDS</div>
                </div>
                <div class="award_box">
                    <div class="award_img">
                        <div><img src="<?php echo get_template_directory_uri(); ?>/images/home/award_2.png"/></div>
                    </div>
                    <div class="award_title">Forex Magnates Awards</div>
                </div>
                <div class="award_box">
                    <div class="award_img">
                        <div><img src="<?php echo get_template_directory_uri(); ?>/images/home/award_3.png"/></div>
                    </div>
                    <div class="award_title">Global Banking &amp;<br/>Finance Review AWARD</div>
                </div>
                <div class="award_box">
                    <div class="award_img">
                        <div><img src="<?php echo get_template_directory_uri(); ?>/images/home/award_4.png"/></div>
                    </div>
                    <div class="award_title">EGR AWARD 2015</div>
                </div>
            </div>
            <div style="clear:both;"></div>
            <div class="p_title">Easy to withdraw</div>
            <div id="pay">
                <div class="pay_box">
                    <div class="pay_img"></div>
                </div>
                <div class="pay_box">
                    <div class="pay_img"></div>
                </div>
                <div class="pay_box">
                    <div class="pay_img"></div>
                </div>
                <div class="pay_box">
                    <div class="pay_img"></div>
                </div>
                <div class="pay_box">
                    <div class="pay_img"></div>
                </div>
            </div>
            <div style="clear:both;"></div>
        </div>
    </div>
<?php endif; ?>

<?php get_footer(); ?>
