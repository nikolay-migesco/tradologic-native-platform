<?php
/**
 * Template Name: MTE Market Research
 */
get_header();
switch (ICL_LANGUAGE_CODE) {

    case "de":
        $language = "de";
        break;
    case "ja":
        $language = "jp";
        break;
    case "ar":
        $language = "ar";
        break;
    case "zh-hans":
        $language = "cn";
        break;
    case "it":
        $language = "it";
        break;
    case "pl":
        $language = "pl";
        break;
    case "pt-pt":
        $language = "pt";
        break;
    case "ru":
        $language = "ru";
        break;
    case "tr":
        $language = "tr";
        break;
    default:
        $language = "en";
}
?>
<div id="content" role="main">
    <div class="container-fluid">
        <h1 class="main_header"><?php the_title(); ?></h1>
        <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-justified" id="market-research-tabs" role="tablist">
                <li class="nav-item active">
                    <a class="nav-link active" id="news-tab" data-toggle="tab" href="#news" role="tab" aria-controls="news" aria-selected="true">News</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="daily-videos-tab" data-toggle="tab" href="#daily-videos" role="tab" aria-controls="daily-videos" aria-selected="false">Daily Videos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="calculator-tab" data-toggle="tab" href="#calculator" role="tab" aria-controls="calculator" aria-selected="false">Calculator</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="calendar-tab" data-toggle="tab" href="#calendar" role="tab" aria-controls="calendar" aria-selected="false">Economic Calendar</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="chart-analysis-tab" data-toggle="tab" href="#chart-analysis" role="tab" aria-controls="chart-analysis" aria-selected="false">Chart Analysis</a>
                </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="news" role="tabpanel" aria-labelledby="news-tab">
                    <div id="mte-news-container">
                        <?php include( ABSPATH . 'wp-content/themes/tol-parent/page-news.php' ); ?>
                    </div>
                    <div id="mte-new-inner" style="display: none;"></div>
                </div>
                <div class="tab-pane" id="daily-videos" role="tabpanel" aria-labelledby="daily-videos-tab">
                    <div id="mte-daily-video-container">
                        <?php include( ABSPATH . 'wp-content/themes/tol-parent/page-daily-videos.php' ); ?>
                    </div>
                    <div id="mte-daily-video-inner" style="display: none;"></div>
                </div>
                <div class="tab-pane" id="calculator" role="tabpanel" aria-labelledby="calculator-tab">
                    <?php include( ABSPATH . 'wp-content/themes/tol-parent/page-calculator.php' ); ?>
                </div>
                <div class="tab-pane" id="calendar" role="tabpanel" aria-labelledby="calendar-tab">
                    <!-- Day tabs -->
                    <ul class="nav nav-tabs nav-justified" id="sortingByDays" role="tablist">
                        <li class="nav-item all active">
                            <a class="nav-link active" id="all-tab" data-toggle="tab" href="#all" role="tab" aria-controls="all" aria-selected="true">all</a>
                        </li>
                        <li class="nav-item monday">
                            <a class="nav-link" id="monday-tab" data-toggle="tab" href="#monday" role="tab" aria-controls="monday" aria-selected="false">mon</a>
                        </li>
                        <li class="nav-item tuesday">
                            <a class="nav-link" id="tuesday-tab" data-toggle="tab" href="#tuesday" role="tab" aria-controls="tuesday" aria-selected="false">tue</a>
                        </li>
                        <li class="nav-item wednesday">
                            <a class="nav-link" id="wednesday-tab" data-toggle="tab" href="#wednesday" role="tab" aria-controls="wednesday" aria-selected="false">wed</a>
                        </li>
                        <li class="nav-item thursday">
                            <a class="nav-link" id="thursday-tab" data-toggle="tab" href="#thursday" role="tab" aria-controls="thursday" aria-selected="false">thu</a>
                        </li>
                        <li class="nav-item friday">
                            <a class="nav-link" id="friday-tab" data-toggle="tab" href="#friday" role="tab" aria-controls="friday" aria-selected="false">fri</a>
                        </li>
                    </ul>
                    <span class="calendar_btn" type="button" data-toggle="collapse" data-target="#infoCalendar"></span>
                    <div id="infoCalendar" class="collapse">
                        <div class="impact-container">
                            <div id="impact3" class="choose-importance marked col-importace"><iron-icon class="importance-icon style-scope mte-calendar-event x-scope iron-icon-1" id="importanceIcon"><svg viewBox="0 0 24 24" preserveAspectRatio="xMidYMid meet" class="style-scope iron-icon" style="pointer-events: none; display: block; width: 100%; height: 100%;"><g class="style-scope iron-icon"><path d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z" class="style-scope iron-icon"></path></g></svg></iron-icon></div>
                            <div id="impact2" class="choose-importance marked col-importace"><iron-icon class="importance-icon style-scope mte-calendar-event x-scope iron-icon-1" id="importanceIcon"><svg viewBox="0 0 24 24" preserveAspectRatio="xMidYMid meet" class="style-scope iron-icon" style="pointer-events: none; display: block; width: 100%; height: 100%;"><g class="style-scope iron-icon"><path d="M22 9.74l-7.19-.62L12 2.5 9.19 9.13 2 9.74l5.46 4.73-1.64 7.03L12 17.77l6.18 3.73-1.63-7.03L22 9.74zM12 15.9V6.6l1.71 4.04 4.38.38-3.32 2.88 1 4.28L12 15.9z" class="style-scope iron-icon"></path></g></svg></iron-icon></div>
                            <div id="impact1" class="choose-importance marked col-importace"><iron-icon class="importance-icon style-scope mte-calendar-event x-scope iron-icon-1" id="importanceIcon"><svg viewBox="0 0 24 24" preserveAspectRatio="xMidYMid meet" class="style-scope iron-icon" style="pointer-events: none; display: block; width: 100%; height: 100%;"><g class="style-scope iron-icon"><path d="M22 9.24l-7.19-.62L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21 12 17.27 18.18 21l-1.63-7.03L22 9.24zM12 15.4l-3.76 2.27 1-4.28-3.32-2.88 4.38-.38L12 6.1l1.71 4.04 4.38.38-3.32 2.88 1 4.28L12 15.4z" class="style-scope iron-icon"></path></g></svg></iron-icon></div>
                        </div>
                        <div class="flag-row">
                            <div class="flag-item">
                                <label class="container-checkbox">
                                    <input type="checkbox" name="chooseFlagAud" id="flag-icon-aud" class="flag_checkbox" checked="checked"><span class="checkmark"></span>
                                </label>
                                <div class="flag-image aud"></div>
                                <span class="currency-name">Aud</span>
                            </div>
                            <div class="flag-item">
                                <label class="container-checkbox">
                                    <input type="checkbox" name="chooseFlagCad" id="flag-icon-cad" class="flag_checkbox" checked="checked"><span class="checkmark"></span>
                                </label>
                                <div class="flag-image cad"></div>
                                <span class="currency-name">Cad</span>
                            </div>
                            <div class="flag-item">
                                <label class="container-checkbox">
                                    <input type="checkbox" name="chooseFlagCny" id="flag-icon-cny" class="flag_checkbox" checked="checked"><span class="checkmark"></span>
                                </label>
                                <div class="flag-image cny"></div>
                                <span class="currency-name">CNY</span>
                            </div>
                            <div class="flag-item">
                                <label class="container-checkbox">
                                    <input type="checkbox" name="chooseFlagJpy" id="flag-icon-jpy" class="flag_checkbox" checked="checked"><span class="checkmark"></span>
                                </label>
                                <div class="flag-image jpy"></div>
                                <span class="currency-name">JPY</span>
                            </div>
                            <div class="flag-item">
                                <label class="container-checkbox">
                                    <input type="checkbox" name="chooseFlagNzd" id="flag-icon-nzd" class="flag_checkbox" checked="checked"><span class="checkmark"></span>
                                </label>
                                <div class="flag-image nzd"></div>
                                <span class="currency-name">NZD</span>
                            </div>
                            <div class="flag-item">
                                <label class="container-checkbox">
                                    <input type="checkbox" name="chooseFlagChf" id="flag-icon-chf" class="flag_checkbox" checked="checked"><span class="checkmark"></span>
                                </label>
                                <div class="flag-image chf"></div>
                                <span class="currency-name">CHF</span>
                            </div>
                            <div class="flag-item">
                                <label class="container-checkbox">
                                    <input type="checkbox" name="chooseFlagEur" id="flag-icon-eur" class="flag_checkbox" checked="checked"><span class="checkmark"></span>
                                </label>
                                <div class="flag-image eur"></div>
                                <span class="currency-name">EUR</span>
                            </div>
                            <div class="flag-item">
                                <label class="container-checkbox">
                                    <input type="checkbox" name="chooseFlagGbp" id="flag-icon-gbp" class="flag_checkbox" checked="checked"><span class="checkmark"></span>
                                </label>
                                <div class="flag-image gbp"></div>
                                <span class="currency-name">GBP</span>
                            </div>
                            <div class="flag-item">
                                <label class="container-checkbox">
                                    <input type="checkbox" name="chooseFlagUsd" id="flag-icon-usd" class="flag_checkbox" checked="checked"><span class="checkmark"></span>
                                </label>
                                <div class="flag-image usd"></div>
                                <span class="currency-name">USD</span>
                            </div>
                        </div>
                       
                    </div>
                    <!-- Day Tab panes -->
                    <div class="tab-content">
                    <div class="tab-pane active" id="all" role="tabpanel" aria-labelledby="all-tab">
                        <div class="row calendar-header">
                            <div class="col mte-calendar-events col-date">Date</div>
                            <div class="col mte-calendar-events col-flag-currency">Currency</div>
                            <div class="col mte-calendar-events col-event">Message</div>
                            <div class="col mte-calendar-events col-importace">Impact</div>
                            <div class="col mte-calendar-events col-actual">Actual</div>
                            <div class="col mte-calendar-events col-forcast">Forecast</div>
                            <div class="col mte-calendar-events col-previous">Previous</div>
                        </div>
                        <div id="mte-calendar-container">
                            <?php include( ABSPATH . 'wp-content/themes/tol-parent/page-calendar.php' ); ?>
                        </div>
                    </div>
                </div>
                </div>
                <div class="tab-pane" id="chart-analysis" role="tabpanel" aria-labelledby="chart-analysis-tab">
                    <div id="mte-chart-analysis-container">
                        <?php include( ABSPATH . 'wp-content/themes/tol-parent/page-chart-analysis.php' ); ?>
                    </div>
                    <div id="mte-chart-analysis-inner" style="display: none;"></div>
                </div>

            </div>
    </div>
</div>

<?php
    get_footer();
?>