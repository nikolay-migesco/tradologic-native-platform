﻿/// <reference path="../lib/engine.js" />
var loginWidget = {

    isPending: false,
    isUserProcessed: false,
    socialSettings: {
        loginFacebook: 'EnableFacebook',
        loginGoogle: 'EnableGoogle',
    },
    elementsCache: {},
    init: function () {
        loginWidget.isProcessed = true;
        var widget = this,
            $widgetElements = widget.elements,
            $submitElement = $widgetElements.filter('[data-type=submit]'),
            $containerElement = $widgetElements.filter('[data-type=container]');
        widget.elementsCache.container = $containerElement;

        $submitElement.click(function (item) {
            widget.submit();
        });

        $containerElement.show();

        $containerElement.each(function () {
            widget.elements.find('input').keypress(function (e) {
                if (e.which == 10 || e.which == 13) {
                    if ($(e.target).data('widget').toLowerCase() == 'login' || $(e.srcElement).data('widget').toLowerCase() == 'login') { //if the event is originated in the login widget
                        widget.submit();
                    }
                }
            });
        });

        if (typeof widgets.initLoginWidget == 'function') {
            widgets.initLoginWidget();
        }

        $(window).on(widgets.events.userLogin, function (event) {
            if (widgets.user != null && widgets.settings != null && widgets.settings.environment.TermsAndConditionsEnabled) {
                if (widgets.user.tos == false) {
                    $(window).trigger(widgets.events.tosRequired, true);
                }
            }
            if (widgets.isLogged() && !widget.isUserProcessed) {
                widget.isUserProcessed = true;
                widget.processDocsSentLogin();
            }
        });

        $(window).on(widgets.events.settingsLoaded, function () {
            widget.checkBackOfficeSettings();
        });

        $(window).on(widgets.events.docsNotSentPanelDismissed, function (event) {
            var field = 'DocSent';
            var oldValue = 'false';
            var newValue = 'false';
            widgets.api.insertAuditLog(field, oldValue, newValue, function (response) {
            })
        });

    },
    checkBackOfficeSettings: function () {
        var widget = this;
        if (widget.elementsCache.container.data('quick') == true) {
            for (var key in widget.socialSettings) {
                if (!helper.getGlobalSetting(widget.socialSettings[key])) {
                    $('[data-widget=' + key + ']').remove();

                }
            }
        }

    },

    processDocsSentLogin: function () {
        if (widgets.user != null && helper.getGlobalSetting('Documents sent pop up')) {
            if (widgets.user.docsSent == false && widgets.user.isReal == true) {
                $(window).trigger(widgets.events.docsNotSent, true);
            }
        }
    },


    submit: function () {
        var widget = loginWidget;
        //widget.validate();

        if (loginWidget.isPending === true) {
            return true;
        }
        loginWidget.isPending = true;

        var $error = widget.elements.filter('[data-type=error]');
        var $username = widget.elements.filter('[data-type=username]');
        var $password = widget.elements.filter('[data-type=password]');

        var useResourseV4 = widget.elements.filter('[data-useresoursev4]').data('useresoursev4');


        var facebookID = (widget.elements.filter('[data-type=facebookID]').length != 0) ? widget.elements.filter('[data-type=facebookID]').val() : null;
        var googleID = (widget.elements.filter('[data-type=googleID]').length != 0) ? widget.elements.filter('[data-type=googleID]').val() : null;
        var btcChinaID = (widget.elements.filter('[data-type=btcChinaId]').length != 0) ? widget.elements.filter('[data-type=btcChinaId]').val() : null;
        var qqOpenID = (widget.elements.filter('[data-type=qqOpenID]').length != 0) ? widget.elements.filter('[data-type=qqOpenID]').val() : null;
        var netellerID = (widget.elements.filter('[data-type=netellerID]').length != 0) ? widget.elements.filter('[data-type=netellerID]').val() : null;
        var socialType = (widget.elements.filter('[data-type=socialType]').length != 0) ? widget.elements.filter('[data-type=socialType]').val() : null;
        var lcid = widgets.token.lcid;

        if ((facebookID != null || googleID != null || btcChinaID != null || qqOpenID != null || netellerID != null) && socialType != null) {
            //case linkToFaceBook or Neteller popUp is opened
            var $loginPopUp = $('#logAndLinkPopUpWrap');
            $error = $loginPopUp.find('[data-widget=login][data-type=error]');
            $username = $loginPopUp.find('[data-widget=login][data-type=username]');
            $password = $loginPopUp.find('[data-widget=login][data-type=password]');
        }

        $error.hide();
        $(window).trigger(widgets.events.loginIsStarted);
        if ($(widget.element).attr('data-loginexternal') == 'true' || helper.getGlobalSetting('EnableExternalUsername')) {
            widgets.api.loginUserExternal($username.val(), $password.val(), widget.processLogin);

        } else {
            var socialID = '',
                loginHistory = {};

            loginHistory.screenResolution = screen.width + "x" + screen.height;

            if (widgets.isMobileDevice()) {
                if ($.browser.device = (/iphone/i.test(navigator.userAgent.toLowerCase()))) {
                    loginHistory.deviceType = 1;
                } else if ($.browser.device = (/ipad/i.test(navigator.userAgent.toLowerCase()))) {
                    loginHistory.deviceType = 2;
                } else if ($.browser.device = (/android/i.test(navigator.userAgent.toLowerCase()))) {
                    loginHistory.deviceType = 3;
                } else if ($.browser.device = (/iemobile|iemobile/i.test(navigator.userAgent.toLowerCase()))) {
                    loginHistory.deviceType = 5;
                } else {
                    loginHistory.deviceType = 6;
                }
                ;
            } else {
                loginHistory.deviceType = 0;
            }
            ;

            socialID = (socialType == 'Google') ? googleID : (socialType == 'BtcChina') ? btcChinaID : (socialType == 'qqOpenID') ? qqOpenID : (socialType == 'Facebook') ? facebookID : (socialType == 'Neteller') ? netellerID : '';

            <!--Migesco. Forex login hack BEGIN-->
            {
                let username = $username.val();
                let password = $password.val();
                const isForexTab = (typeof THIS_IS_FOREX_TAB !== typeof undefined);
                if (!isForexTab) {
                    widgets.api.loginUser(username,
                        password,
                        socialType,
                        socialID,
                        useResourseV4,
                        lcid,
                        loginHistory,
                        widget.processLogin
                    );
                }
                let isTwoFactor = false;
                if (isForexTab) {
                    isTwoFactor = helper.getGlobalSetting("EnableTwoFactorAuthentication");
                }
                let authCode = null;
                if (isTwoFactor && isForexTab) {
                    let $authElement = widget.elements.filter('[data-type=auth-code]');
                    authCode = $authElement.length !== 0
                        ? $authElement.val()
                        : null;
                }
                if (isForexTab) {
                    widgets.api.loginUser(
                        username,
                        password,
                        socialType,
                        socialID,
                        useResourseV4,
                        lcid,
                        loginHistory,
                        authCode,
                        widget.processLogin)
                }
            }
            <!--Migesco. Forex login hack END-->

        }
    },

    processLogin: function (response) {
        var widget = loginWidget;
        console.log('login');
        writeLoginLog(response.code, response.data.text);

        if (response.code == 200 || response.code == 201) {
            var token = response.data;
            $.cookie(widgets.tokenCookie, JSON.stringify(token), {path: '/'});
            widgets.token = token;
        }
        if (response.code == 400 || response.code == 401) {
            var responseType = response.data.type;
            widget.message(responseType);
        }
        else {
            loginWidget.isPending = false;
            $(window).trigger(widgets.events.loginIsCompleted, response);
            return;
        }

        widgets.getTokenWithRefresh(false, function () {
            loginWidget.isPending = false;
            $(window).trigger(widgets.events.loginIsCompleted, response);
        });
    },

    message: function (text) {
        var widget = this;
        var $error = widget.elements.filter('[data-type=error]');
        var errorText = widgetMessage['Login_WrongCredentials'] || 'Invalid username or password';
        errorText = widgetMessage[text] ? widgetMessage[text] : errorText;

        $error.html(errorText);
        $error.show();

    }

}


function writeLoginLog(response_code, response_text) {
    setTimeout(function () {
        var form_id = $('div[data-type="container"][data-widget="login"]').attr('id');
        var req = {'try': '1', 'widget': 'login', 'lang': widgetsSettings.lang};
        var username = $('#' + form_id + ' input[data-type="username"]');
        var password = $('#' + form_id + ' input[data-type="password"]');

        req.formName = $('input[data-type="submit"][data-widget="login"]').attr('id');
        req.username = username.val();
        req.password = (password.val() != '' ? '***' : '');

        req.error_msg = $('#' + form_id + ' div.login-error-message:visible').text();
        req.success_msg = $('#' + form_id + ' div.login-success-message:visible').text();
        req.api_msg = response_code;
        if (response_text == 'Wrong username or password.') response_text = 'Неверное имя пользователя или пароль';
        req.api_text = response_text;
        $.post('/wp-content/themes/xptheme/try_reg.php', req, function (data) {
            //if(data) console.log(data);
        });
    }, 1000);
}
