var stxx = new CIQ.ChartEngine(
    {
        container: $$$("#chartContainer"),
        preferences: {labels: false, currentPriceLine: true},
        layout: {
            chartType: 'candle',
            interval: '5'
        }
    }
);

var initModule = function (stxx) {
    var uiModule = function (stxx) {
        moduleUI(stxx);
    };

    uiModule(stxx);
};