
var stxx = new CIQ.ChartEngine(
    {
        container: $$$("#chartContainer"),
        preferences: {labels: true, currentPriceLine: true},
        layout: {
            chartType: 'mountain',
            period:1,
            interval: 1,
            timeUnit: 'minute',
            crosshair: true
        }
    }
);
stxx.chart.xAxis.futureTicks = false;
var initModule = function (stxx) {
    var uiModule = function (stxx) {
        moduleUI(stxx);
    };

    uiModule(stxx);
};