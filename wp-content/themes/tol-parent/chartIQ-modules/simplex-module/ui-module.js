var moduleUI = function(stxx) {
    var UIContext;

    function displayChart() {
        startUI();
        resizeScreen();
    }

    var checkWidth  = function checkWidth(){
        if ($(window).width() > 700) {
            $('body').removeClass('break-md break-sm').addClass('break-lg');
            $('.icon-toggles').removeClass('sidenav active').addClass('ciq-toggles');
            stxx.layout.sidenav = 'sidenavOff';
            $('#symbol').attr("placeholder", "Enter Symbol");
            return;
        }
        if ($(window).width() <= 700 && $(window).width() > 584) {
            $('body').removeClass('break-lg break-sm').addClass('break-md');
            $('.icon-toggles').removeClass('sidenav active').addClass('ciq-toggles');
            stxx.layout.sidenav = 'sidenavOff';
            $('#symbol').attr("placeholder", "Symbol");
            return;
        }
        if ($(window).width() <= 584) {
            $('body').removeClass('break-md break-lg').addClass('break-sm');
            $('.icon-toggles').removeClass('ciq-toggles').addClass('sidenav');
            $('#symbol').attr("placeholder", "");
        }
    }

    function setHeight() {
        var windowHeight=$(window).height();
        var ciqHeight = $('.ciq-chart').height();
        var chartwrapper = $('.modal-content').height();

        $('#chartContainer').css('height','100%');

        // This little snippet will ensure that dialog boxes are never larger than the screen height
        $('#maxHeightCSS').remove();
        $('head').append('<style id="maxHeightCSS">cq-dialog { max-height: ' +  windowHeight + 'px }</style>');
    }

    var restorePreferences = function restorePreferences(){
        var pref=CIQ.localStorage.getItem("myChartPreferences");
        if (pref) stxx.importPreferences(JSON.parse(pref));
    };

    var savePreferences = function savePreferences(obj){
        CIQ.localStorageSetItem("myChartPreferences",JSON.stringify(stxx.exportPreferences()));
    };

    var stxCallbacks = function () {
        stxx.callbacks.layout=saveLayout;
        stxx.callbacks.symbolChange=saveLayout;
        stxx.callbacks.drawing=saveDrawings;
        stxx.callbacks.newChart=retoggleEvents;
        stxx.callbacks.preferences=savePreferences;
    };

    var  startUI = function startUI(){
        chartCustumElementModule.navBarPanelBG(stxx,'div','navbar-bg');

        $("div[data-type='assetsList']").on('click', '[data-type="invest-btn"]', function (event) {
             var assetData ={
                 assetId:$(this).data('taid').toString(),
                 assetSymbolName:$(this).parents('.tradearea-box').find('.asset-long-name').text()
             };
             $(window).trigger('tradeFromChartAssetSelectedSimplex', assetData);
         });

        $(".asset-row-element .assetName").on('click', function (event) {
            var assetData ={
                assetId: $(this).parent().parent().data('taid').toString(),
                assetSymbolName: $(this).text()
            };
            $(window).trigger('tradeFromChartAssetSelectedBinary', assetData);
        });


        $('.toggleChart').on('click',function () {
            $('cq-context').toggle('slow')
        });


        UIContext = new CIQ.UI.Context(stxx, $("cq-context,[cq-context]"));

        var UIHeadsUpStatic=new CIQ.UI.HeadsUp($("cq-hu-static"), UIContext, {autoStart: true});
        var UIStorage    = new CIQ.NameValueStore();
        var UIThemes=$("cq-themes");

        UIThemes[0].initialize({
            defaultTheme: "ciq-night",
            nameValueStore: UIStorage
        });

        if(UIContext.loader) UIContext.loader.show();
        restorePreferences();

        CIQ.UI.begin();
    };

    var  resizeScreen = function resizeScreen(){
        if(!UIContext) return;
        checkWidth();
        setHeight();
        var sidePanel=$("cq-side-panel")[0];
        if(sidePanel){
            $('.ciq-chart-area').css({'right': sidePanel.nonAnimatedWidth() +'px'});
            $('cq-tradingcentral').css({'margin-right': sidePanel.nonAnimatedWidth() + 15 + 'px'});
        }
        stxx.resizeChart();
        if(stxx.slider) stxx.slider.display(stxx.layout.rangeSlider);
    };

    var  webComponentsSupported = ('registerElement' in document && 'import' in document.createElement('link') && 'content' in document.createElement('template'));
    var  checkWebComponentsSupported = function () {
        if(webComponentsSupported){
            displayChart();
        }else{
            window.addEventListener('WebComponentsReady', function(e) {
                startUI();
                resizeScreen();
            });
        }
        if(typeof Promise === 'undefined') CIQ.loadScript('js/thirdparty/promise.min.js'); // Necessary for IE and MSFT Edge if you are using sharing (because html2canvas uses promises)
        $(window).resize(resizeScreen);
    };

    setTimeout(function () {
        checkWebComponentsSupported();
    },2000);
};
