var chartCustumElementModule = {

    fogElement : function (stxx,elementType,elementClass) {
        var element = document.createElement(elementType);
        element.classList.add(elementClass);
        stxx.chart.container.appendChild(element);
    },

    navBarPanelBG : function (stxx,elementType,elementClass) {
        var element = document.createElement(elementType);
        element.classList.add(elementClass);
        stxx.chart.container.appendChild(element);
    },

    strikeRateLine: function (stxx,elementType,elementID) {
        var element = document.createElement(elementType);
        element.setAttribute("id",elementID );
        stxx.chart.container.appendChild(element);
    }

};