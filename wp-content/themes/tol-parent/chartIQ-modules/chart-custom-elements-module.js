var chartCustumElementModule = {

    fogElement : function (stxx,elementType,elementClass) {
        var element = document.createElement(elementType);
        element.classList.add(elementClass);
        stxx.chart.container.appendChild(element);
    },

    navBarPanelBG : function (stxx,elementType,elementClass) {
        var element = document.createElement(elementType);
        element.classList.add(elementClass);
        stxx.chart.container.appendChild(element);
        $(element).css('width',$('.ciq-chart').width()+ 'px');
    },


};