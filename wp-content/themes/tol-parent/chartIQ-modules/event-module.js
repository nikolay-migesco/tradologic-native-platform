var eventModule = {
    studiesColAnimation : function (handleElement, scopeElement) {
            $(handleElement).on('click', function () {
                $(scopeElement).each(function(i,el){
                    el.id = 'studie-item-'+ i;
                    MotionUI.animateIn('#studie-item-'+i+' ', 'hinge-in-from-top', function () {
                    })
                });
            });
    },

    menuDropDownAnimation: function (handleElement,scopeElement) {
        $(handleElement).on('click', function () {
            MotionUI.animateIn( scopeElement, 'hinge-in-from-top', function () {})
        })
    }


};