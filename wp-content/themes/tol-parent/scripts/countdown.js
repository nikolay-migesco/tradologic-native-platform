//$(document).ready(function() {
if ($.cookie('widgetUserData')) {
    var widgetUserData = JSON.parse($.cookie('widgetUserData'));
    var user_id = widgetUserData.id + '_';
    $.cookie("temp_cookie", user_id);
}
//if(widgetUserData && widgetUserData.isReal==false) {
var counterTime = new (function () {
    var $countdown,
        incrementTime = 70,
        currentTime = parseInt($.cookie($.cookie("temp_cookie") + "countdown")),
        updateTimer = function () {
            $.cookie($.cookie("temp_cookie") + "countdown", parseInt(($.cookie($.cookie("temp_cookie") + "close_time") - $.now()) / 10));
            currentTime = parseInt($.cookie($.cookie("temp_cookie") + "countdown"));
            $countdown.html(formatTime(currentTime));
            if (currentTime < 0) {
                counterTime.Timer.stop();
                timerComplete();
                counterTime.resetCountdown();
                return;
            }
            currentTime -= incrementTime / 10;
            $.cookie($.cookie("temp_cookie") + "countdown", parseInt(currentTime));
            if (currentTime < 0) currentTime = 0;
        },
        timerComplete = function () {
            $.removeCookie($.cookie("temp_cookie") + "popup_timeout2");
            $.removeCookie($.cookie("temp_cookie") + "countdown");
            $.removeCookie($.cookie("temp_cookie") + "popup_hide");
            $.removeCookie($.cookie("temp_cookie") + "close_time");
            $('#popup_overlay3').remove();
            $('#ban_nobonus').show();
            $('#ban_bonus').hide();
            if ($('input[name="promo_code"]').val().toLowerCase() == 'bonusplus') {
                $('#my_bonus').prop('value', 'BonusSt').keyup();
                $('input[name="promo_code"]').prop('value', 'BonusSt').keyup();
                $('#proc_total').prop('checked', 'checked').change();
            }
        },
        init = function () {
            $countdown = $('span.countdown');
            counterTime.Timer = $.timer(updateTimer, incrementTime, true);
        };
    this.resetCountdown = function () {
        if ($.cookie($.cookie("temp_cookie") + "close_time") > $.now()) $.cookie($.cookie("temp_cookie") + "countdown", parseInt(($.cookie($.cookie("temp_cookie") + "close_time") - $.now()) / 10));
        currentTime = parseInt($.cookie($.cookie("temp_cookie") + "countdown"));
        this.Timer.stop().once();
        this.Timer.play().once();
    };
    $(init);
});

//});

function pad(number, length) {
    var str = '' + number;
    while (str.length < length) {
        str = '0' + str;
    }
    return str;
}

function formatTime(time) {
    var min = parseInt(time / 6000),
        sec = parseInt(time / 100) - (min * 60);
    return (min > 0 ? pad(min, 2) : "00") + ":" + pad(sec, 2);
}

//}
