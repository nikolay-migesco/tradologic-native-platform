var isUpPressed = true;
var dynClassGlobal = "";

var showRight = function (isUp, tradearea, dynClass, isHelp) {
    if (tradearea != null) {
        var parent = $('.' + tradearea);
        var index = tradearea.split('tradearea')[1];
        resetTradeBox(tradearea);
    } else {
        var parent = $('.tol-traderoom');
        //var index = tradearea.split('tradearea')[1];
        //resetTradeBox(tradearea);       

    }

    isUpPressed = isUp;
    dynClassGlobal = dynClass;

    if (!isHelp) {
        if (widgets.isLogged()) {
            if (Number(widgets.user.balance) < Number(widgets.settings.application.DefaultInvestAmount.Value)) {

                $('.quickDepositInfo, .quick-cancel-BTN, .goToCashierLink').show();
                $('[data-widget=cashierCardDepositQuick][data-translate=cashierDepositSuccessful], [data-widget=cashierCardDepositQuick][data-translate=cashierLabelTryAgain], .quick-cancel-BTN.endDeposit, #tryAgainQuick').hide();

                if ($('[data-widget=cashierCardDepositQuick][data-type=activeCreditCards] option').length > 0) {
                    $('.cashierCardDepositQuickButton, .cashierCardDepositQuickAmount, .cashierCardDepositQuickActiveCC').show();
                } else {
                    $('.cashierCardDepositQuickNoCC').show();
                }

                if (isHelp != true) {
                    $('#tol-quick-deposit').modal('show');
                    return;
                }
            }
        };
    } else {
        $('.tradearea1 .widgetButtonAlpha').click();
    };

    var isOptionUp = widgets.links['tradologic'+index].option.isUp;
    var selectionText = (isOptionUp) ? (isUp) ? widgetMessage.oneTouch_touchUp : widgetMessage.oneTouch_noTouchUp : (isUp) ? widgetMessage.oneTouch_touchDown : widgetMessage.oneTouch_noTouchDown;

    $(parent).find('.chartWidget').on('mouseover', function () {
        $(parent) + $(' .multi-view .highcharts-container > svg > path:nth-of-type(3)').css('stroke', '#44aad5');
    })

    if (tradearea != null) {
        var isOptionUp = widgets.links['tradologic' + index].option.isUp;        
    } else {
        var isOptionUp = widgets.links.tradologic.option.isUp;
    }

    var selectionText = (isOptionUp) ? (isUp) ? widgetMessage.oneTouch_touchUp : widgetMessage.oneTouch_noTouchUp : (isUp) ? widgetMessage.oneTouch_touchDown : widgetMessage.oneTouch_noTouchDown;

    if (isUp) {
        $(parent).find('.widgetButtonBeta').removeClass('isClicked');
        $(parent).find('.widgetButtonAlpha').addClass('isClicked');
        $(parent).find('span.positionWrapperValue').addClass(dynClass);
        if (widgets.game == "touch")
            $(parent).find('label.selectionStateLabel').text(widgetMessage.touch_selection_up_short);
        else if (widgets.game == "oneTouch") {
            $(parent).find('label.selectionStateLabel').text(selectionText);
        }
        else
            $(parent).find('label.selectionStateLabel').text($(parent).find('.widgetButtonAlpha').text());

    } else {
        $(parent).find('.widgetButtonAlpha').removeClass('isClicked');
        $(parent).find('.widgetButtonBeta').addClass('isClicked');
        $(parent).find('span.positionWrapperValue').addClass(dynClass);
        if (widgets.game == "touch")
            $(parent).find('label.selectionStateLabel').text(widgetMessage.touch_selection_down_short);
        else if (widgets.game == "oneTouch") {
            $('label.selectionStateLabel').text(selectionText);
        }
        else
            $(parent).find('label.selectionStateLabel').text($(parent).find('.widgetButtonBeta').text());

    };
    

    $(parent).find('.tol-tradebox-left').hide();
    $(parent).find('.tol-tradebox-right').show();
};


var resetTradeBox = function (tradearea) {

    if (!$('.tol-tradebox-left').is(":visible")
        && $('.tol-tradebox-left').length > 0)
        $(window).trigger('clearHighlights');

    $('.tol-tradebox-right,.cancelButton-wrapper,.countdown-wrapper').hide();
    $('.tol-tradebox-left').show();

    //todo to check where what to close / select
    $('.tol-tradebox-right').hide();
    $('.tol-tradebox-left').show();
    $('.helpContainerLabels1').show();
    $('.helpContainerLabels2').hide();
    $('.contractsDDL').val('-');
    $('span.selectionStateLabel').removeClass(dynClassGlobal);
    $('.widgetButtonAlpha').removeClass('isClicked');
    $('.widgetButtonBeta').removeClass('isClicked');
    $('.tradeboxRightHide').show();
    $('.tradeboxRightWrap').hide();
    $('.tradeboxLeft').show();
    $('.tol-tradebox-close-button').show();
    $('.selectionStateLabel').html('');
    $('.' + tradearea + ' .tradeButton').prop("disabled", false);
};


var showOpenTrades = function () {
    if ($('.notActive').hasClass('openTradesBtn')) {
        $('.closedTradesWrapper, .EconomicCalnedarWrapper').hide();
        $('.openTrades, .turboBoxSection').show();
        $('.openTradesBtn').removeClass('notActive');
        $('.openTradesBtn').addClass('active');
        $('.closedTradesBtn').removeClass('active');
        $('.closedTradesBtn').addClass('notActive');
        $('.EconomicCalendarBtn').removeClass('active').addClass('notActive');
        $('.turboBox').show();
    }
};
var showClosedTrades = function () {
    if ($('.notActive').hasClass('closedTradesBtn')) {
        $('.closedTradesWrapper').show();
        $('.openTrades, .turboBoxSection, .EconomicCalnedarWrapper').hide();
        $('.closedTradesBtn').removeClass('notActive');
        $('.closedTradesBtn').addClass('active');
        $('.openTradesBtn').removeClass('active');
        $('.openTradesBtn').addClass('notActive');
        $('.EconomicCalendarBtn').removeClass('active').addClass('notActive');
        $(window).trigger(widgets.events.expireOptions);
        $('.turboBox').hide();
    }
};
var showEconomicCalendar = function () {
    if ($('.notActive').hasClass('EconomicCalendarBtn')) {
        $('.EconomicCalnedarWrapper').show();
        $('.openTrades, .turboBox, .turboBoxSection, .showTurboSlider, .closedTradesWrapper').hide();
        $('.turboChart').remove();
        $('.EconomicCalendarBtn').removeClass('notActive').addClass('active');
        $('.openTradesBtn').removeClass('active').addClass('notActive');
        $('.closedTradesBtn').removeClass('active').addClass('notActive');
    }
};
var showAutochartist = function () {
    $('.tol-signals-table').show();
    $('.turboChart').remove();
    if (widgets.game == "digital")
        $('.signalsNoOptions').hide();
};
var showTournaments = function () {
    $('.tournamentsWrapper').show();
    $('.tradesGridWrapper').css('overflow-y', 'hidden');
    $('.introTitleText').html('Be part of "' + $('[data-widget=tournamentLeaderboard][data-type=name]').text() + '"');
    $('.openTrades, .turboBoxSection, .closedTradesWrapper').hide();
    $('.turboChart').remove();
};


$('.cancelButton').click(function () {
    resetTradeBox();
});

$(document).ready(function () {
    var height = $(window).height() - $('.bodyWrapper').height();
    if (height > 0) {
        $('.bodyWrapper');
    }

    $('.expireDropDownWidget').change(function () {
        resetTradeBox();
    })

    $('.optionDropDownWidget').change(function () {
        resetTradeBox();
    });
});

// Help functions
function navClose() {
    $('span.help').removeClass('active');
    $('.helpWrapper').hide();
    resetTradeBox();
}

$('span.help').click(function () {
    if (!$(this).hasClass('active')) {
        $(this).addClass('active');
        $('.helpWrapper').show();
        if ($('.tradearea1').find('.tradeboxLeft').is(":visible")) {
            $('.helpContainerLabels1').show();
            $('.helpContainerLabels2').hide();
        } else {
            $('.helpContainerLabels2').show();
            $('.helpContainerLabels1').hide();
        }
    } else {
        navClose();
    }
});

$('.navClose').click(function () {
    navClose();
});

$('.navNext').click(function () {
    showRight(true, 'tradearea1', 'call', true);
    $('.helpContainerLabels2').show();
    $('.helpContainerLabels1').hide();
});
/*
 * This is fix for tournaments
 */
$(window).on('loadingIsCompleted', function () {
    /*if(widgets.isMobileDevice()){
        if($('#tol-contactUs-layout-1 #phone').length > 0){
            $('#tol-contactUs-layout-1 #phone').attr("data-decorate","")
        }
    }*/
   // $(window).trigger('clickTournamentDetails');
});