/** CUSTOM SCRIPTS THAT INTERACT WITH WIDGETS */

$(document).ready(function () {

    $('.icon.icon-icon3').mouseover(function () {
        $('.balancetooltip').show();
    }).mouseout(function () {
        $('.balancetooltip').hide();
    });

    $('.nav').on('click', function (event) {
        var $element = $(event.target),
            id = event.target.id;

        if ($('.navtabs').filter('.active').length > 0) {
            $('.navtabs').filter('.active').removeClass('active');
        }
        $element.addClass('active');

        if ($('img').filter('.active').length > 0) {
            $('img').filter('.active').removeClass('active');
            $('img#' + id + '').addClass('active');
        }

        $('.tabs img').fadeOut('fast');
        $('.tabs img.active').fadeIn('fast');

    });

    $(document).on("click", ".scrollContent td, .optionAccordion", function () {
        resetTradeBox();
    });

    $('.howtotrade-wrapper .navigation a').click(function () {
        selectedName = $(this).attr('data-name');
        // update buttons
        $('.howtotrade-wrapper .navigation a')
            .removeClass('selected')
            .parent().removeClass('selected');
        $(this)
            .addClass('selected')
            .parent().addClass('selected');
        // update panels
        $('.howtotrade-wrapper .panel').fadeOut('fast');
        $('.howtotrade-wrapper .panel.' + selectedName).fadeIn('fast');

        // update text
        $('.TradeGameIn3steps .showSteps').fadeOut('fast');
        $('.TradeGameIn3steps .showSteps.' + selectedName).fadeIn('fast');

        // update swfs
        $('.tradingsimulator-wrapper .panel').fadeOut('fast');
        $('.tradingsimulator-wrapper .panel.' + selectedName).fadeIn('fast');
    });

    $('.tol-userbar-inbox').click(function () {
        window.location.href = getPathLanguagePrefix() + '/inbox';
        return;
    });

    $('.portfolio-details-back').click(function () {
        window.location.href = getPathLanguagePrefix() + '/my-portfolios';
        return;
    });

    $('.tol-quick-next').click(function (e) {
        e.preventDefault();
        $('.compliance-tab.col-sm-8.active').removeClass('active');
        $('.compliance-tab.quest').addClass('active');
        $('.compliance-userdetails.compliance-container').hide();
        $('.compliance-questionnaire.compliance-container').show();
        return;
    });


    $("#clearButton").click(function () {
        $('.widgetButtonAlpha').removeClass('isClicked');
        $('.widgetButtonBeta').removeClass('isClicked');
        $('.selectionStateLabel').html('');
        $('.returnHighAmount span.value').html('');
        $('.returnLowAmount span.value').html('');
        $('.widgetInvestAmount').val(widgets.settings.application.DefaultInvestAmount.Value).trigger("keyup");
        $('.widgetDynamicPayout option:eq(0)').prop('selected', true)
        $('.tradeButton').attr("disabled", "true");

        $(window).trigger('clearHighlights');
    });

    $('.tol-userbar-logout-user').click(function () {
        widgets.logout();
        return;
    });

    $('.tol-userbar-my-trades').click(function () {
        window.location.href = getPathLanguagePrefix() + '/my-trades';
        return;
    });
    $('.tol-userbar-my-statistics').click(function () {
        window.location.href = getPathLanguagePrefix() + '/my-statistics';
        return;
    });
    $('.tol-userbar-my-portfolios').click(function () {
        window.location.href = getPathLanguagePrefix() + '/my-portfolios';
        return;
    });
    $('.tol-userbar-my-profile').click(function () {
        window.location.href = getPathLanguagePrefix() + '/my-profile';
        return;
    });
    $('.tol-userbar-social-profile').click(function () {
        forexSocialProfileWidget.initPopup(widgets.user.id, 'stats', true);
        return;
    });
    $('.tol-userbar-change-password').click(function () {
        window.location.href = getPathLanguagePrefix() + '/change-password';
        return;
    });

    $('a.navbar-brand').click(function () {
        window.location.href = getPathLanguagePrefix() + "/";
        return;
    });

    $('.tol-create-account').click(function () {
        window.location.href = getPathLanguagePrefix() + '/registration';
        return;
    });

    $('.hide-dev').click(function () {
        $('.theme-dev-container').toggleClass('gone');
    });

    $('.compliance-tab').click(function () {
        $('.compliance-tab').removeClass('active');
        $(this).addClass('active');
    });

    $('.navbar-toggle').click(function () {
			if ($('#slidemenu').attr('aria-expanded') == 'false' || typeof $('#slidemenu').attr('aria-expanded') == 'undefined') {
				$('.tol-login-userbar-container').addClass('mobile-expanded');
			} else {
				$('.tol-login-userbar-container').removeClass('mobile-expanded');
			};
		
    });

    $(".column-settings>.settings").click(function (e) {
        e.stopPropagation();
        if(!$(e.target).hasClass('onoffswitch-inner') && !$(e.target).hasClass('onoffswitch-checkbox')){
            if ($(this).parent().hasClass('show')) {
                $(this).parent().removeClass('show');
                if (widgets.isMobileDevice() && (window.location.href.indexOf('my-trades') > 0)) {
                    $('.ui-widget-overlay').hide();
                }
            } else {
                if (widgets.isMobileDevice() && (window.location.href.indexOf('my-trades') > 0)) {
                    $('.ui-widget-overlay').show();
                }
                $(this).parent().addClass('show');

                if ($('[data-type="assetstypescontainer"]').length > 0)
                    $('[data-type="assetstypescontainer"]').remove();

                $('[data-type=amountcontainer]').remove();
            };
            return;
        }

    });

    $(document).click(function () {
        $(".column-settings").removeClass('show');
        $(".nav-button.has-icon.main-menu").removeClass('show');
    });
});

$(window).on('loadingIsCompleted', function () {
    if (widgets.isLogged()) {
        $('.quick-wrapForm').hide();
        $('.page-about-us .entry-content p:last-of-type').append("<div style='margin-top:15px'><a href='../traderoom' class='btn btn-primary' role='button' data-translate='quickRegistrationSubmit'></a></div>");
    } else {
        $('.quick-wrapForm').show();
        $('.page-about-us .entry-content p:last-of-type').append("<div style='margin-top:15px'><a href='#' id='notLoggedLinkAboutUs'class='btn btn-primary' role='button' data-translate='quickRegistrationSubmit'></a></div>");
    };

    $(window).scroll(function () {
        if (window.location.pathname.indexOf('traderoom') == -1 && $(window).width() > 767) {
            if ($(window).scrollTop() > 0) {
                $('#tol-header-layout-26').addClass('sticky');
            } else {
                $('#tol-header-layout-26').removeClass('sticky');
            };
        };
    });

    $("#notLoggedLinkAboutUs").click(function () {
        $('#tol-login-popup').modal('show');
    });

    function ShowQuickSignUpForm() {
        $('#tol-login-popup').modal('show');
    }

    $('a.investAmountDDLButton').click(function (e) {
        e.preventDefault();
        return false;
    });

    if (typeof widgetsUser != 'undefined') {
        $('.input-currency-symbol').text(widgetsUser.currencySymbol);
    };

    $("#mobileGameMenu").val(helper.getParameterByName('game') != '' ? helper.getParameterByName('game') : 'digital');

    $(".dataLinkChange").attr({
        "data-link": getPathLanguagePrefix() + "/terms-and-conditions"
    });


    // Orientation change for Traderoom
    function updateOrientation(e) {
        const pathname = window.location.pathname;
        const isTraderoom = pathname.includes("traderoom");

        let isForex = false;
        let isSimplex = false;
        if (isTraderoom ) {
            isForex = widgets.game === traderoomMode.realForex;
            isSimplex = widgets.game === traderoomMode.easyForex;
        }

        if (isForex) {
            $('.traderoom-loader').show();
            window.location.href = getPathLanguagePrefix() + getTraderoomLink(traderoomMode.realForex);
        }
        if (isSimplex) {
            $('.traderoom-loader').show();
            window.location.href = getPathLanguagePrefix() + getTraderoomLink(traderoomMode.easyForex);
        }

        const isMarketResearch = pathname.includes("market-research");
        if (isMarketResearch) {
            window.location.reload();
        }
    }
    // Listen for orientation changes
    $(window).on("orientationchange", updateOrientation);

    // Forex Traderoom expand .tradepanel-title
    $('.page-traderoom .tradepanel-control-bar .tradepanel-title').click(function () {
        $(this).toggleClass('blue-border')
    });

    $('.tol-social-sidebar .fake-thead th').click(function (event) {
        $('.tol-social-sidebar .active .scrollable-area-content table th:nth-child(' + ($(this).index() + 1) + ')').click();
    });
});

$(window).on('contactUsSubmited', function (response) {
    if ($(".appriseOuter").length > 0) {
        return;
    }
    apprise(widgetMessage.ContactUs_ThankYou);
    $('.wrapper-contacts').hide();
});

$(window).on('userDetailsUpdated', function () {
    function hideSuccessMsg() {
        $("#tol-myProfile-layout-1 .myprofile-successmsg").html('');
    }
    setTimeout(hideSuccessMsg, 2000);
});

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

$(window).on("tournamentIsUserRegister", function () {
    $('.tournamentsBackImage').hide();
    $('.tournamentsBack').hide();
    $('.tournamentsBody').show();

    setTimeout(function () {
        $('.tableBodyDiv').mCustomScrollbar("destroy");
        $(".tableBodyDiv").mCustomScrollbar({
            advanced: {
                updateOnContentResize: true
            },
            scrollButtons: {
                enable: false
            }

        });
    }, 10);
});

$('.detailsButton').live('click', function (e) {
    e.preventDefault();
    window.location.href = './tournaments?tournamentID=' + $(this).attr("data-tournamendetailsid");
    return false;
});

$('.tournamentsBtn').live('click', function (e) {
    $(window).trigger('clickTournamentDetails');
});

if ($.cookie('widgetSession') == null) {
    $('.joinNowButton, [data-widget=tournamentRegistration][data-type=register]').live('click', onLoginBtnClick);
};

$('.tournamentDetailsTraderoom').click(function () {
    window.location.href = getPathLanguagePrefix() + '/tournaments?tournamentID=current';
});

function onLoginBtnClick(e) {
    e.preventDefault();
    apprise(widgetMessage.notLoggedJoinTournament);
};

$(window).on('clickTournamentJoinButton', function () {
    if (widgetsUser.isReal === false) {
        window.location.pathname = getPathLanguagePrefix() + "/cashier";
    }
});

$(window).on('tournamentLeaderboardIsReady', function () {
    $('.introTitleText').html('Be part of "' + $('[data-widget=tournamentLeaderboard][data-type=name]').text() + '"');
    if (window.location.pathname === "/ar/tournaments") {
        $('.tableDiv').css('direction', 'ltr');
    };
    if (window.location.pathname === "/ar/tournaments" || window.location.pathname === "/tournaments") {
        $('.tableDiv').css('width', 'auto');
    };
    $('.ui-widget-header').css('width', 'auto');
});

$(window).on('socialLoadingComplete', function () {
    $('.flag').each(function (i) {
        var img = $(this).attr('class').toLowerCase();
        img = img.split(" ")[1];
        $(this).css('background', 'url(./wp-content/plugins/sitepress-multilingual-cms/res/flags/' + img + '.png)');
    });
});
