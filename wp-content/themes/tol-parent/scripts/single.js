var isUpPressed = true;
var showRight = function (isUp, isClickedHelpNext) {

    if (!widgets.isLogged())
        return;

    isUpPressed = isUp;
    resetTradeBox();

    if (Number(widgets.user.balance) < Number(widgets.settings.application.DefaultInvestAmount.Value)) {

        $('.quickDepositInfo, .quick-cancel-BTN, .goToCashierLink').show();
        $('[data-widget=cashierCardDepositQuick][data-translate=cashierDepositSuccessful], [data-widget=cashierCardDepositQuick][data-translate=cashierLabelTryAgain], .quick-cancel-BTN.endDeposit, #tryAgainQuick').hide();

        if ($('[data-widget=cashierCardDepositQuick][data-type=activeCreditCards] option').length > 0) {
            $('.cashierCardDepositQuickButton, .cashierCardDepositQuickAmount, .cashierCardDepositQuickActiveCC').show();
        } else {
            $('.cashierCardDepositQuickNoCC').show();
        }

        if (isClickedHelpNext != true) {
            $('#tol-quick-deposit').modal('show');
            return;
        }
    }

    var isOptionUp = widgets.links.tradologic.option.isUp;
    var selectionText = (isOptionUp) ? (isUp) ? widgetMessage.oneTouch_touchUp : widgetMessage.oneTouch_noTouchUp : (isUp) ? widgetMessage.oneTouch_touchDown : widgetMessage.oneTouch_noTouchDown;
    $('#tol-bxzone-trade').modal('show');

    if (isUp) {
        $('.widgetButtonAlpha').addClass('isClicked');
        $('.widgetButtonBeta').removeClass('isClicked');
        $('.selectionStateLabel').html('High');
        $('span.selectionStateLabel').addClass("call");
        if (widgets.game == "touch")
            $('label.selectionStateLabel').text(widgetMessage.touch_selection_up_short);
        else if (widgets.game == "oneTouch") {
            $('label.selectionStateLabel').text(selectionText);
        }
        else
            $('label.selectionStateLabel').text($('.widgetButtonAlpha').text());
    } else {
        $('.widgetButtonBeta').addClass('isClicked');
        $('.widgetButtonAlpha').removeClass('isClicked');
        $('.selectionStateLabel').html('Low');
        $('span.selectionStateLabel').addClass("put");
        if (widgets.game == "touch")
            $('label.selectionStateLabel').text(widgetMessage.touch_selection_down_short);
        else if (widgets.game == "oneTouch") {
            $('label.selectionStateLabel').text(selectionText);
        }
        else
            $('label.selectionStateLabel').text($('.widgetButtonBeta').text());
    };

    $('.ticks_tradologic_tradeAreaWrapper').addClass('nopadding');
    $('.tol-tradebox-left').hide();
    $('.tol-tradebox-right').show();
    $('.selectionAsset').text(widgets.links.tradologic.option.name);
    if (widgets.game == "turboBinary") {
        //$('.investAmount').text(helper.getCurrencySymbol() + " " + $('.investDropDown').val());
        $('.investAmount').text(helper.getCurrencySymbol() + " " + $('.widgetInvestAmount').val());
        $('.ratePopup.expiration').html($('.activeRule').val());
    } else {
        $('.investAmount').text(helper.getCurrencySymbol() + " " + $('.widgetInvestAmount').val());
        $('.expiry-value').html(helper.formatExpiration(new Date(widgets.links.tradologic.option.expirationTimestamp)));
        $('.ratePopup.expiration').html(helper.formatExpiration(widgets.getDate(widgets.links.tradologic.option.expirationTimestamp)));
    };
};

var startTimer = function () {
    var timer = 2, seconds;
    refreshTimer = setInterval(function () {
        seconds = parseInt(timer % 60, 10);

        $('.countdown-timer').text(seconds);

        if (--timer < 0) {
            $('.tradeButton').click();
            clearInterval(refreshTimer);
            return;
        }
    }, 1000);
};
var resetTradeBox = function () {

    if (!$('.tol-tradebox-left').is(":visible")
            && $('.tol-tradebox-left').length > 0)
        $(window).trigger('clearHighlights');

    $('.tol-tradebox-right,.cancelButton-wrapper,.countdown-wrapper').hide();
    $('.tol-tradebox-left').show();

    if (typeof widgets.token.parameters != 'undefined') {
        if (widgets.token.parameters.CySECRegulated == 1) {
            $('.buy-button-fake, .tol-tradebox-close-button, .investAmountDDLButton').show();
            $('.real-buy-button').hide();
            $('.rate-down, .rate-up, .tol-widgetInvestAmount, .tol-widgetContracts').prop('disabled', false);
            if( $('.cancelButton.bxzone')!= null && !$('.cancelButton.bxzone').is(':visible')) {
                $('.btn-danger, .close').show();
            }
        };
    };

    $('.ticks_tradologic_tradeAreaWrapper').removeClass('nopadding');
    $('#tol-bxzone-trade').modal('hide');
    $('.widgetButtonAlpha').removeClass('isClicked');
    $('.widgetButtonBeta').removeClass('isClicked');
    $('.positionStateLabel').html('');
    $('.tol-tradebox-close-button').show();
    $('.selectionStateLabel').removeClass("call put");
    $('.selectionStateLabel').html('');
};
var showAutochartist = function () {
    $('.tol-signals-table').show();
    $('.turboChart').remove();
    if (widgets.game == "digital")
        $('.signalsNoOptions').hide();
};
var showTournaments = function () {
    $('.tournamentsWrapper').show();
    $('.tradesGridWrapper').css('overflow-y', 'hidden');
    $('.introTitleText').html('Be part of "' + $('[data-widget=tournamentLeaderboard][data-type=name]').text() + '"');
    $('.openTrades, .turboBoxSection, .closedTradesWrapper').hide();
    $('.turboChart').remove();
};


$('.buy-button-fake').click(function () {
    $('.cancelButton-wrapper, .countdown-wrapper').show();
    $('.tol-tradebox-close-button, .investAmountDDLButton').hide(); // X button
    $('.rate-down, .rate-up, .tol-widgetInvestAmount, .tol-widgetContracts').prop('disabled', true); //disable amount, autotrade, rate
    $('.buy-button-fake').hide();
    $('.countdown-timer').text(3);
    startTimer();
});

// Help functions
function navClose() {
    $('button.help').removeClass('active');
    $('.helpWrapper').hide();
    resetTradeBox();
}

$('button.help').click(function () {
    if (!$(this).hasClass('active')) {
        $(this).addClass('active');
        $('.helpWrapper').show();
        if (widgets.game == "overUnder") {
            $('.overUnder .scrollContent tr.overUnder').prev().click();
            $('.helpContainerLabels2').show();
            $('.overUnder .scrollContent tr:nth-child(3)').trigger("click");
            var test = $('.overUnder .scrollContent tr.overUnder .payoutOverWrap')[0];
            $(test).click();
        } else {
            if ($('.tol-tradebox-left').is(":visible")) {
                $('.helpContainerLabels1').show();
                $('.helpContainerLabels2').hide();
            } else {
                $('.helpContainerLabels2').show();
                $('.helpContainerLabels1').hide();
            }
        }
    } else {
        navClose();
        $('.overUnder .scrollContent tr.overUnder').prev().click();
    }
});

$('.navClose').click(function () {
    navClose();
});

$('.navNext').click(function () {
    showRight(true, 'tradearea1', true, true);
    $('.helpContainerLabels2').show();
    $('.helpContainerLabels1').hide();
});

$(window).on('loadingIsCompleted', function () {
    if (widgets.game === "advanced" || widgets.game === "ticks" || widgets.game === "forex") {
        $('.tol-help-btn').hide();
    };
});

//END Help functions


/*
 * This is fix for tournaments
 */
$(window).on('loadingIsCompleted', function () {
    /*if(widgets.isMobileDevice()){
        if($('#tol-contactUs-layout-1 #phone').length > 0){
            $('#tol-contactUs-layout-1 #phone').attr("data-decorate","")
        }
    }*/
    //$(window).trigger('clickTournamentDetails');
});
$(window).on('resetWidget', function() {
    $( ".cancelButton").unbind( "click" );
    $('.cancelButton').click(function () {
        $('.cancelButton-wrapper, .countdown-wrapper').hide();
        $('.buy-button-fake').show();
        clearInterval(refreshTimer);
        resetTradeBox();
    });
    
    $( ".buy-button-fake").unbind( "click" );
    $('.buy-button-fake').click(function () {
        $('.cancelButton-wrapper, .countdown-wrapper').show();
        $('.tol-tradebox-close-button, .investAmountDDLButton').hide(); // X button
        $('.rate-down, .rate-up, .tol-widgetInvestAmount, .tol-widgetContracts').prop('disabled', true); //disable amount, autotrade, rate
        $('.buy-button-fake').hide();
        $('.countdown-timer').text(3);
        startTimer();
    });
    $( ".button.help").unbind( "click" );
    $('button.help').click(function () {
        if (!$(this).hasClass('active')) {
            $(this).addClass('active');
            $('.helpWrapper').show();
            if (widgets.game == "overUnder") {
                $('.overUnder .scrollContent tr.overUnder').prev().click();
                $('.helpContainerLabels2').show();
                $('.overUnder .scrollContent tr:nth-child(3)').trigger("click");
                var test = $('.overUnder .scrollContent tr.overUnder .payoutOverWrap')[0];
                $(test).click();
            } else {
                if ($('.tol-tradebox-left').is(":visible")) {
                    $('.helpContainerLabels1').show();
                    $('.helpContainerLabels2').hide();
                } else {
                    $('.helpContainerLabels2').show();
                    $('.helpContainerLabels1').hide();
                }
            }
        } else {
            navClose();
            $('.overUnder .scrollContent tr.overUnder').prev().click();
        }
    });
    $( ".navClose").unbind( "click" );
    $('.navClose').click(function () {
        navClose();
    });
    $( ".navNext").unbind( "click" );
    $('.navNext').click(function () {
        showRight(true, 'tradearea1', true, true);
        $('.helpContainerLabels2').show();
        $('.helpContainerLabels1').hide();
    });
});