function AES_Init(){AES_Sbox_Inv=new Array(256);for(var i=0;i<256;i++)
  AES_Sbox_Inv[AES_Sbox[i]]=i;AES_ShiftRowTab_Inv=new Array(16);for(var i=0;i<16;i++)
  AES_ShiftRowTab_Inv[AES_ShiftRowTab[i]]=i;AES_xtime=new Array(256);for(var i=0;i<128;i++){AES_xtime[i]=i<<1;AES_xtime[128+i]=(i<<1)^0x1b;}}
function AES_Done(){delete AES_Sbox_Inv;delete AES_ShiftRowTab_Inv;delete AES_xtime;}
function AES_ExpandKey(key){var kl=key.length,ks,Rcon=1;switch(kl){case 16:ks=16*(10+1);break;case 24:ks=16*(12+1);break;case 32:ks=16*(14+1);break;default:alert("AES_ExpandKey: Only key lengths of 16, 24 or 32 bytes allowed!");}
  for(var i=kl;i<ks;i+=4){var temp=key.slice(i-4,i);if(i%kl==0){temp=new Array(AES_Sbox[temp[1]]^Rcon,AES_Sbox[temp[2]],AES_Sbox[temp[3]],AES_Sbox[temp[0]]);if((Rcon<<=1)>=256)
    Rcon^=0x11b;}
  else if((kl>24)&&(i%kl==16))
    temp=new Array(AES_Sbox[temp[0]],AES_Sbox[temp[1]],AES_Sbox[temp[2]],AES_Sbox[temp[3]]);for(var j=0;j<4;j++)
    key[i+j]=key[i+j-kl]^temp[j];}}
function AES_Encrypt(block,key){var l=key.length;AES_AddRoundKey(block,key.slice(0,16));for(var i=16;i<l-16;i+=16){AES_SubBytes(block,AES_Sbox);AES_ShiftRows(block,AES_ShiftRowTab);AES_MixColumns(block);AES_AddRoundKey(block,key.slice(i,i+16));}
  AES_SubBytes(block,AES_Sbox);AES_ShiftRows(block,AES_ShiftRowTab);AES_AddRoundKey(block,key.slice(i,l));}
function AES_Decrypt(block,key){var l=key.length;AES_AddRoundKey(block,key.slice(l-16,l));AES_ShiftRows(block,AES_ShiftRowTab_Inv);AES_SubBytes(block,AES_Sbox_Inv);for(var i=l-32;i>=16;i-=16){AES_AddRoundKey(block,key.slice(i,i+16));AES_MixColumns_Inv(block);AES_ShiftRows(block,AES_ShiftRowTab_Inv);AES_SubBytes(block,AES_Sbox_Inv);}
  AES_AddRoundKey(block,key.slice(0,16));}
AES_Sbox=new Array(99,124,119,123,242,107,111,197,48,1,103,43,254,215,171,118,202,130,201,125,250,89,71,240,173,212,162,175,156,164,114,192,183,253,147,38,54,63,247,204,52,165,229,241,113,216,49,21,4,199,35,195,24,150,5,154,7,18,128,226,235,39,178,117,9,131,44,26,27,110,90,160,82,59,214,179,41,227,47,132,83,209,0,237,32,252,177,91,106,203,190,57,74,76,88,207,208,239,170,251,67,77,51,133,69,249,2,127,80,60,159,168,81,163,64,143,146,157,56,245,188,182,218,33,16,255,243,210,205,12,19,236,95,151,68,23,196,167,126,61,100,93,25,115,96,129,79,220,34,42,144,136,70,238,184,20,222,94,11,219,224,50,58,10,73,6,36,92,194,211,172,98,145,149,228,121,231,200,55,109,141,213,78,169,108,86,244,234,101,122,174,8,186,120,37,46,28,166,180,198,232,221,116,31,75,189,139,138,112,62,181,102,72,3,246,14,97,53,87,185,134,193,29,158,225,248,152,17,105,217,142,148,155,30,135,233,206,85,40,223,140,161,137,13,191,230,66,104,65,153,45,15,176,84,187,22);AES_ShiftRowTab=new Array(0,5,10,15,4,9,14,3,8,13,2,7,12,1,6,11);function AES_SubBytes(state,sbox){for(var i=0;i<16;i++)
  state[i]=sbox[state[i]];}
function AES_AddRoundKey(state,rkey){for(var i=0;i<16;i++)
  state[i]^=rkey[i];}
function AES_ShiftRows(state,shifttab){var h=new Array().concat(state);for(var i=0;i<16;i++)
  state[i]=h[shifttab[i]];}
function AES_MixColumns(state){for(var i=0;i<16;i+=4){var s0=state[i+0],s1=state[i+1];var s2=state[i+2],s3=state[i+3];var h=s0^s1^s2^s3;state[i+0]^=h^AES_xtime[s0^s1];state[i+1]^=h^AES_xtime[s1^s2];state[i+2]^=h^AES_xtime[s2^s3];state[i+3]^=h^AES_xtime[s3^s0];}}
function AES_MixColumns_Inv(state){for(var i=0;i<16;i+=4){var s0=state[i+0],s1=state[i+1];var s2=state[i+2],s3=state[i+3];var h=s0^s1^s2^s3;var xh=AES_xtime[h];var h1=AES_xtime[AES_xtime[xh^s0^s2]]^h;var h2=AES_xtime[AES_xtime[xh^s1^s3]]^h;state[i+0]^=h1^AES_xtime[s0^s1];state[i+1]^=h2^AES_xtime[s1^s2];state[i+2]^=h1^AES_xtime[s2^s3];state[i+3]^=h2^AES_xtime[s3^s0];}}
function ssaEncrypt(dataArray,key,asCharCode){AES_Init();if(key.length<=32){AES_ExpandKey(key)}
  var IV=ssaInitIV(16);var IVw=IV.slice(0);for(var i=0;i<dataArray.length;i+=16){var block=dataArray.slice(i,i+16);for(var j=0;j<block.length;j++){block[j]=block[j].charCodeAt();}
    ssaXOREach(block,IVw);AES_Encrypt(block,key);for(var j=0;j<block.length;j++){if(asCharCode==null){dataArray[i+j]=String.fromCharCode(block[j]);}
    else{dataArray[i+j]=block[j];}
      IVw[j]=block[j];}}
  if(asCharCode==null){dataArray.unshift(String.fromCharCode(IV[0]),String.fromCharCode(IV[1]),String.fromCharCode(IV[2]),String.fromCharCode(IV[3]),String.fromCharCode(IV[4]),String.fromCharCode(IV[5]),String.fromCharCode(IV[6]),String.fromCharCode(IV[7]),String.fromCharCode(IV[8]),String.fromCharCode(IV[9]),String.fromCharCode(IV[10]),String.fromCharCode(IV[11]),String.fromCharCode(IV[12]),String.fromCharCode(IV[13]),String.fromCharCode(IV[14]),String.fromCharCode(IV[15]));}
  else{dataArray.unshift(IV[0],IV[1],IV[2],IV[3],IV[4],IV[5],IV[6],IV[7],IV[8],IV[9],IV[10],IV[11],IV[12],IV[13],IV[14],IV[15]);}
  AES_Done();}
function ssaDecrypt(dataArray,key,asCharCode){AES_Init();if(key.length<=32){AES_ExpandKey(key)}
  var IV=dataArray.splice(0,16);if(asCharCode==null)
    for(var i=0;i<IV.length;i++){IV[i]=IV[i].charCodeAt();}
  for(var i=0;i<dataArray.length;i+=16){var block=dataArray.slice(i,i+16);var IVw=block.slice(0);if(asCharCode==null){for(j=0;j<block.length;j++){block[j]=block[j].charCodeAt();}}
    AES_Decrypt(block,key);ssaXOREach(block,IV);for(j=0;j<block.length;j++){dataArray[i+j]=String.fromCharCode(block[j]);if(asCharCode==null){IV[j]=IVw[j].charCodeAt();}
    else{IV[j]=IVw[j];}}}
  AES_Done();}
function ssaInitIV(len){var ret=new Array();for(var i=0;i<len;i++){ret.push(Math.floor(Math.random()*256));}
  return ret;}
function ssaXOREach(a,b){if(a.length>b.length){alert("ssaXOREach() a.length can't be larger than b. length: "+
a.length+' '+b.length);return;}
  for(var i=0;i<a.length;i++){a[i]=a[i]^b[i];}}
function ssaEncryptYield(arry,key,progressCb,finalCb){var sliceStart=0;var sliceEnd=0;var SLICE=10240;(function(){AES_Init();if(key.length<=32){AES_ExpandKey(key)}
  sliceEnd=(sliceStart+SLICE)-1;if(sliceEnd>arry.length){sliceEnd=sliceEnd-(sliceEnd-arry.length);}
  for(i=sliceStart;i<sliceEnd;i+=16){var block=arry.slice(i,i+16);for(j=0;j<block.length;j++){block[j]=block[j].charCodeAt();}
    AES_Encrypt(block,key);for(j=0;j<block.length;j++){arry[i+j]=block[j];arry[i+j]=String.fromCharCode(arry[i+j]);}}
  AES_Done();if(sliceStart>arry.length){finalCb(sliceEnd);}
  else{progressCb(sliceEnd,'<br> sS:'+sliceStart+' sE:'+sliceEnd);setTimeout(arguments.callee,0);}
  sliceStart+=SLICE;})();}
function ssaDecryptYield(arry,key,progressCb,finalCb){var sliceStart=0;var sliceEnd=0;var SLICE=10240;(function(){AES_Init();if(key.length<=32){AES_ExpandKey(key)}
  sliceEnd=(sliceStart+SLICE)-1;if(sliceEnd>arry.length){sliceEnd=sliceEnd-(sliceEnd-arry.length);}
  for(i=sliceStart;i<sliceEnd;i+=16){var block=arry.slice(i,i+16);for(j=0;j<block.length;j++){block[j]=block[j].charCodeAt();}
    AES_Decrypt(block,key);for(j=0;j<block.length;j++){arry[i+j]=block[j];arry[i+j]=String.fromCharCode(arry[i+j]);}}
  AES_Done();if(sliceStart>arry.length){finalCb(sliceEnd);}
  else{progressCb(sliceEnd,'<br> sS:'+sliceStart+' sE:'+sliceEnd);setTimeout(arguments.callee,0);}
  sliceStart+=SLICE;})();}
function ssaComputeKey(passphrase,key){if(key.length<64){return;}
  var a=SHA1(key.substring(0,32)+passphrase);var b=SHA1(key.substring(32,64)+a);var t=SHA1(a+b)+b;return t.substr(7,64);}
function ssaComputeKey2(passphrase,key){if(key.length<64){return;}
  var a=ssaComputeKey(passphrase,key);var b=a;for(var i=0;i<100;i++){a=SHA1(a);}
  a=a+SHA1(a+b);return a.substr(7,64);}
function ssaComputeKey3(passphrase,key,iterations){if(key.length<64){return;}
  var a=ssaComputeKey(passphrase,key);var b=a;for(var i=0;i<iterations;i++){a=SHA1(a);}
  a=a+SHA1(a+b);return a.substr(7,64);}
function hexToArray(k){var ka=new Array();for(i=0;i<k.length;i+=2)
  ka[i/2]=parseInt('0x'+k.substring(i,i+2));return ka;}
function charCodesToHex(codesArray){var hexStr='';for(i=0;i<codesArray.length;i++){var j=parseInt(codesArray[i]);if(j<16){hexStr+='0'+j.toString(16);}
else{hexStr+=parseInt(codesArray[i]).toString(16);}}
  return hexStr;}
function arrayToHex(arr){var ccArr=new Array();for(var i=0;i<arr.length;i++){ccArr.push(arr[i].charCodeAt());}
  return charCodesToHex(ccArr);}
function SHA1(msg){function rotate_left(n,s){var t4=(n<<s)|(n>>>(32-s));return t4;};function lsb_hex(val){var str="";var i;var vh;var vl;for(i=0;i<=6;i+=2){vh=(val>>>(i*4+4))&0x0f;vl=(val>>>(i*4))&0x0f;str+=vh.toString(16)+vl.toString(16);}
  return str;};function cvt_hex(val){var str="";var i;var v;for(i=7;i>=0;i--){v=(val>>>(i*4))&0x0f;str+=v.toString(16);}
  return str;};function Utf8Encode(string){string=string.replace(/\r\n/g,"\n");var utftext="";for(var n=0;n<string.length;n++){var c=string.charCodeAt(n);if(c<128){utftext+=String.fromCharCode(c);}
else if((c>127)&&(c<2048)){utftext+=String.fromCharCode((c>>6)|192);utftext+=String.fromCharCode((c&63)|128);}
else{utftext+=String.fromCharCode((c>>12)|224);utftext+=String.fromCharCode(((c>>6)&63)|128);utftext+=String.fromCharCode((c&63)|128);}}
  return utftext;};var blockstart;var i,j;var W=new Array(80);var H0=0x67452301;var H1=0xEFCDAB89;var H2=0x98BADCFE;var H3=0x10325476;var H4=0xC3D2E1F0;var A,B,C,D,E;var temp;msg=Utf8Encode(msg);var msg_len=msg.length;var word_array=new Array();for(i=0;i<msg_len-3;i+=4){j=msg.charCodeAt(i)<<24|msg.charCodeAt(i+1)<<16|msg.charCodeAt(i+2)<<8|msg.charCodeAt(i+3);word_array.push(j);}
  switch(msg_len%4){case 0:i=0x080000000;break;case 1:i=msg.charCodeAt(msg_len-1)<<24|0x0800000;break;case 2:i=msg.charCodeAt(msg_len-2)<<24|msg.charCodeAt(msg_len-1)<<16|0x08000;break;case 3:i=msg.charCodeAt(msg_len-3)<<24|msg.charCodeAt(msg_len-2)<<16|msg.charCodeAt(msg_len-1)<<8|0x80;break;}
  word_array.push(i);while((word_array.length%16)!=14)word_array.push(0);word_array.push(msg_len>>>29);word_array.push((msg_len<<3)&0x0ffffffff);for(blockstart=0;blockstart<word_array.length;blockstart+=16){for(i=0;i<16;i++)W[i]=word_array[blockstart+i];for(i=16;i<=79;i++)W[i]=rotate_left(W[i-3]^W[i-8]^W[i-14]^W[i-16],1);A=H0;B=H1;C=H2;D=H3;E=H4;for(i=0;i<=19;i++){temp=(rotate_left(A,5)+((B&C)|(~B&D))+E+W[i]+0x5A827999)&0x0ffffffff;E=D;D=C;C=rotate_left(B,30);B=A;A=temp;}
    for(i=20;i<=39;i++){temp=(rotate_left(A,5)+(B^C^D)+E+W[i]+0x6ED9EBA1)&0x0ffffffff;E=D;D=C;C=rotate_left(B,30);B=A;A=temp;}
    for(i=40;i<=59;i++){temp=(rotate_left(A,5)+((B&C)|(B&D)|(C&D))+E+W[i]+0x8F1BBCDC)&0x0ffffffff;E=D;D=C;C=rotate_left(B,30);B=A;A=temp;}
    for(i=60;i<=79;i++){temp=(rotate_left(A,5)+(B^C^D)+E+W[i]+0xCA62C1D6)&0x0ffffffff;E=D;D=C;C=rotate_left(B,30);B=A;A=temp;}
    H0=(H0+A)&0x0ffffffff;H1=(H1+B)&0x0ffffffff;H2=(H2+C)&0x0ffffffff;H3=(H3+D)&0x0ffffffff;H4=(H4+E)&0x0ffffffff;}
  var temp=cvt_hex(H0)+cvt_hex(H1)+cvt_hex(H2)+cvt_hex(H3)+cvt_hex(H4);return temp.toLowerCase();}var DEBUG=false;var SS_DELIM='__SS_DELIMARATOR__';var ssScriptCount=1;var ssUseIncrement=0;function createCookie(name,value,days){if(days){var date=new Date();date.setTime(date.getTime()+(days*24*60*60*1000));var expires="; expires="+date.toGMTString();}
else var expires="";document.cookie=name+"="+value+expires+"; path=/";}
function readCookie(name){var nameEQ=name+"=";var ca=document.cookie.split(';');for(var i=0;i<ca.length;i++){var c=ca[i];while(c.charAt(0)==' ')c=c.substring(1,c.length);if(c.indexOf(nameEQ)==0)return c.substring(nameEQ.length,c.length);}
  return null;}
function eraseCookie(name){createCookie(name,"",-1);}
function setSecureCookie(passphrase,name,val,days){ssUseIncrement++;var ka=ssaComputeKey3(passphrase+ssUseIncrement,ssSeedKey,1000);var key=hexToArray(ka);var sk=hexToArray(ssSeedKey);val=val.length+SS_DELIM+val;var data=val.toString().split('');ssaEncrypt(data,key,true);createCookie(name,ssUseIncrement+' '+sk.join(' ')+' '+data.join(' '),days);}
function getSecureCookie(passphrase,name){var cookieVal=readCookie(name);var dataLen=cookieVal.split(' ').slice(0,1);var useIncr=cookieVal.split(' ').slice(1,2);var useIncr=cookieVal.split(' ').slice(0,1);var seedKey=charCodesToHex(cookieVal.split(' ').slice(1,33));var key=hexToArray(ssaComputeKey3(passphrase+useIncr,seedKey,1000));var data=cookieVal.split(' ').slice(33);ssaDecrypt(data,key,true);var dataLen=data.join('').split(SS_DELIM);var startAt=dataLen[0].length+SS_DELIM.length;var readTo=parseInt(parseInt(startAt)+parseInt(dataLen[0]));return data.join('').substring(startAt,readTo);}
function ssxdom(subToken){return'<script type="text/javascript">var ssSeedKey = "8608cf81928d43aa5051bfe4184a6c81582100e57fc58300c30e53acb0bcf505";<\/script>';
}
function ssaEncryptSimple(passphrase,val){ssUseIncrement++;var ka=ssaComputeKey3(passphrase+ssUseIncrement,ssSeedKey,1000);var key=hexToArray(ka);var sk=hexToArray(ssSeedKey);var data=val.toString().split('');var dataLen=data.length;ssaEncrypt(data,key,true);return ssUseIncrement+
    ','+ssSeedKey+','+dataLen+','+charCodesToHex(data);}
function ssaDecryptSimple(passphrase,data){var encInfo=data.split(',');var dataLen=encInfo[2];var useIncr=encInfo[0];var seedKey=encInfo[1];var key=hexToArray(ssaComputeKey3(passphrase+useIncr,seedKey,1000));var message=hexToArray(encInfo[3]);ssaDecrypt(message,key,true);return message.join('').substring(0,dataLen);}