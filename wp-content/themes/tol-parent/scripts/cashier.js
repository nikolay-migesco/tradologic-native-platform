$(document).ready(function () {
    $('#depositTypeSelect').change(function () {
        changeFunc();
        updateSummary();
    });
    $('#depositAmountInput').keyup(function () {
        $('span.depositAmount').text($('#depositAmountInput').val());
    });
    $(document).on('click', '.promoCodeLink', function () {
        $('#tol-promoCode-wrapper').show();
    });
    //Fix for enabling fields on Back button for FF
    $('.cashierDepositFields select').removeAttr('disabled');
    $('.cashierDepositFields input').removeAttr('disabled');
});

$(window).on('widgetsRegisterEvents', function () {
    $(window).on(widgets.events.cashierCurrencyChanged, function (event, numMethods) {
        if (numMethods < 2) {
            //$('.cashierDepositMethod, .cashierDepositMethod .styled_cashier').hide();
            $('#tol-method-wrapper').hide();
        } else {
            //$('.cashierDepositMethod, .cashierDepositMethod .styled_cashier').show();
            $('#tol-method-wrapper').show();
        };
    });
    $(window).on(widgets.events.cashierSubmitDeposit, function (event, params) {
        if (params.redirect == 'card') {
            window.location.href = '?cpage=ccdeposit' + '&currencyId=' + params.currency + '&amount=' + params.amount + '&promocode=' + params.promocode;
        };
        $('.cashierDepositFields').hide();
    });
    $(window).on(widgets.events.cashierDepositLimitationFail, function (event, params) {
        $('.depositAmountInput, .promotionalCode, .cashierDepositMethod, .cashierCurencySelect').show();
        if ((widgetsUser.isReal == true)) {
            $('.cashierCurencySelect').hide();
        };
        apprise(widgetMessage.cashierDepositLimitationFail.replace(/\{MinimumDepositSum}/g, params.data.parameters.MinimumDepositSum).replace(/\{MaximumDepositSum}/g, params.data.parameters.MaximumDepositSum), { confirm: false }, function (r) {
            if (r) {
                window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'cashier?cpage=deposit';
                $('#loadingElementProcessing').hide();
            };
        });
    });
    $(window).on(widgets.events.loadingIsCompleted, function () {
        if (widgets.isLogged() && window.location.pathname.indexOf("cashier") >= 0 ) {
            if ($('.praxis-wrapper')[0]) {
                var iFrame = createFrame('iframePraxis');
                iFrame.setAttribute("src", "/wp-content/themes/tol-parent/layouts/cashier-frame.php?user=" + widgets.user.id + "&locale=" + $.cookie('current_language') + "&sessionid=" + widgets.token.sessionId + "&curr=" + widgets.user.currencyCode.toUpperCase());
            };
        };
    });
    $(window).on(widgets.events.cashierLoaded, function (event) {
        pageLingks();
        isUserReal();
        setTimeout(updateSummary(), 200);
    });
    $(window).on(widgets.events.cashierCCDepositProvinceHide, function (event, params) {
        $('.styled_cashier.stateProvinceLabel').hide();
    });
    $(window).on(widgets.events.cashierCCDepositProvinceShow, function (event, params) {
        $('.styled_cashier.stateProvinceLabel').show();
    });
    $(window).on(widgets.events.cashierWithdrawalUnsuccessful, function (event, params) {
        apprise(params.data.text);
    });
    $(window).on(widgets.events.cashierWithdrawalSuccessful, function (event, params) {
        apprise(widgetMessage['cashierWithdrawalSuccessful'], {}, function (r) {
            if (r) {
                window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'cashier?cpage=withdrawHistory';
            }
        });
    });
    $(window).on(widgets.events.cashierPendingWithdrawal, function (event, params) {
        $('.styled_cashier').hide()
        apprise(widgetMessage['cashierPendingWithdrawal'], {}, function (r) {
            if (r) {
                window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'cashier?cpage=withdrawHistory';
            }
        });
    });
    $(window).on(widgets.events.cashierStartWithdrawProcessing, function (event) {
        $('#loadingElementProcessing').show();
    });
    $(window).on(widgets.events.cashierWithdrawLimitationResult, function (event, code) {
        if (code == 406) {
            $('.styled_cashier').show()
        } else {
            $('.withdrawMessages').css('top', '150px');
        }
    });
    // Code to end displaying "processing" animations or effects on withdrawal forms
    $(window).on(widgets.events.cashierEndWithdrawProcessing, function (event) {
        $('#loadingElementProcessing, .styled_cashier').hide();
    });
    $(window).on(widgets.events.cashierWithdrawalBlocked, function (event) {
        // Code to execute for cashier blocked withdrawals
    });
    $(window).on(widgets.events.cashierWithdrawBonusTRNotMet, function (event) {
        apprise(widgetMessage['cashierBonusTRNotMet']);
    });
    $(window).on(widgets.events.cashierWithdrawBonusTRMet, function (event) {
        apprise(widgetMessage['cashierBonusTRMet']);
    });
    // Triggers when the Cancel button is clicked on the withdraw page
    $(window).on(widgets.events.cashierWithdrawCancelled, function (event) {
        window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'cashier?cpage=deposit'
    });
    $(window).on(widgets.events.cashierCancelBonusSuccessful, function (event) {
        apprise(widgetMessage['cashierBonusCancellationSuccessful']);
    });
    $(window).on(widgets.events.cashierCancelBonusUnsuccessful, function (event) {
        apprise(widgetMessage['cashierBonusCancellationFailed']);
    });
    $(window).on(widgets.events.cashierActivateBonusSuccessful, function (event) {
        apprise(widgetMessage['cashierBonusActivationSuccessful']);
    });
    $(window).on(widgets.events.cashierActivateBonusUnsuccessful, function (event) {
        apprise(widgetMessage['cashierBonusActivationFailed']);
    });
    // Code to display bonus description
    $(window).on(widgets.events.cashierProcessBonusDescription, function (event, params) {
        apprise(params.data.text);
    });
    $(window).on(widgets.events.cashierWireTransferCMSRedirect, function (event, params) {
        // Code to display bonus description       
        var $wireTransfer = $('[data-widget=cashierDeposit][data-type=wireTransferHolder]');
        $wireTransfer.empty();
        $wireTransfer.show();
        $wireTransfer.load("./cashier/wire-transfer");
        $('.depositBTN, .cashierDepositMethod, .depositSummary, #tol-method-wrapper, #tol-currency-wrapper').hide();
    });
    $(window).on(widgets.events.cashierWireTransferRedirect, function (event, params) {
        // Code to display bonus description        
        var $wireTransfer = $('[data-widget=cashierDeposit][data-type=wireTransferHolder]');
        $wireTransfer.empty();
        $wireTransfer.show();
        window.wireTransferInfo = JSON.stringify(params.data.elements);
        window.wireTransferInfoObject = JSON.parse(wireTransferInfo);
        for (i = 0; i < wireTransferInfoObject.length ; i++) {
            $wireTransfer.append('currencyId:' + params.data.elements[i].currencyId + '<br/>currencyName:' + params.data.elements[i].currencyName + '<br/>accountHoldingBank:' + params.data.elements[i].accountHoldingBank + '<br/>accountName:' + params.data.elements[i].accountName + '<br/>accountNo:' + params.data.elements[i].accountNo + '<br/>ibanNumber:' + params.data.elements[i].ibanNumber + '<br/>swift:' + params.data.elements[i].swift + '<br/>beneficiaryName:' + params.data.elements[i].beneficiaryName + '<br/><br/><br/><br/>');
        };
        $('.depositBTN, .cashierDepositMethod, .cashierCurencySelect, .depositSummary, #tol-method-wrapper, #tol-currency-wrapper').hide();
    });
    $(window).on(widgets.events.cashierCancelWithdrawalRequest, function (event, cancelRequest) {
        apprise(cancelRequest.confirmText, { 'verify': true }, function (r) {
            if (r) {
                widgets.api.cancelWithdrawRequest(cancelRequest.id, function (response) {
                    widgets.api.getWithdrawalHistory(cancelRequest.params, function (response) {
                        cancelRequest.widget.processWithdrawHistory(cancelRequest.params, response);
                    });
                });
            }
        });
    });
    $(window).on(widgets.events.cashierDepositGenerateExtraFields, function (event, response, widget) {
        var fieldsHtml = '';
        $('[data-widget=cashierDeposit][data-type=additionalFieldsPlaceholder]').empty();
        $.each(response.data, function (index, element) {
            var fieldType = response.data[index].fieldType == 1 ? 'text' : 'password';
            var translatedLabel = widgetMessage['cashierLabel' + response.data[index].translationKey];
            if (translatedLabel !== undefined) {
                translatedLabel = (widgetMessage['cashierLabel' + response.data[index].translationKey]).indexOf(':') > -1 ? (widgetMessage['cashierLabel' + response.data[index].translationKey]) : (widgetMessage['cashierLabel' + response.data[index].translationKey]) + ':';
            }
            else {
                translatedLabel = response.data[index].defaultLabelText + ':';
            }
            fieldsHtml += '<div class="row col-sm-12 nopadding"><div class="row col-sm-6"><div class="form-group col-sm-12"><label class="hidden-xs col-sm-12 col-md-4 control-label text-left">' + translatedLabel + '</label><div class="col-sm-12 col-md-8"><input type="' + fieldType + '" data-widget="cashierDeposit" data-isrequired="true" class="form-control" data-type="extraField' + response.data[index].fieldName + '" /></div></div></div></div>';
        });

        if (fieldsHtml != '') {
            $('[data-widget=cashierDeposit][data-type=additionalFieldsPlaceholder]').show();
            $('[data-widget=cashierDeposit][data-type=additionalFieldsPlaceholder]').html(fieldsHtml);
            var extraFields = $('[data-widget=cashierDeposit][data-type^=extraField]');
            $.each(extraFields, function (index, element) {
                $($(element)).blur(function () {
                    if ($(element).val() == '' || $.trim($(element).val()) == '') {
                        widget.addError($(element).attr('data-type'), widgetMessage['requiredField']);
                        $('#' + $(element).attr('data-type') + 'Validation').remove();
                        $(element).after('<div id="' + $(element).attr('data-type') + 'Validation" class="validationError">' + widgetMessage['requiredField'] + '</div>');
                    }
                    else {
                        $('#' + $(element).attr('data-type') + 'Validation').remove();
                    }
                });
            });
        }
        else {
            $('[data-widget=cashierDeposit][data-type=additionalFieldsPlaceholder]').empty();
            $('[data-widget=cashierDeposit][data-type=additionalFieldsPlaceholder]').hide();
        }
    });

});
var createFrame = function (name) {
    var ifrm = '<iframe src="" id="' + name + '" height="100%" width="100%" align="left" marginheight="0" marginwidth="0" frameborder="0" style="overflow-x:hidden;"></iframe>';
    var iframeHolder = $('.praxis-wrapper');
    $(iframeHolder).append(ifrm);
    iframeHolder.show();
    return document.getElementById(name);
};
// Used when the Deposit type is changed from the DDL
var changeFunc = function () {
    depositSelectValue = $('#depositTypeSelect').val();
    $('.savedCreditCardWrap, .validationError, .nettelerFields').hide();
    $('.cashierCurencySelect').show();

    if ([55, 60, 76, 107].indexOf(depositSelectValue) > -1) {
        if (widgets.user.isReal) {
            $('.cashierCurencySelect').hide();
        }
    } else if (depositSelectValue === "73" || depositSelectValue === "127") {
        $('.nettelerFields').show();
    };

    if (depositSelectValue === "-1") {
        if (widgets.user.isReal == true) {
            $('#tol-active-cc-wrapper').show();
            widgets.api.getCashierQuickDepositElements(function (response) {
                if (response.data.activeCreditCards != undefined) {
                    if (response.data.activeCreditCards.length != 0) {
                        $('.savedCreditCardWrap').show();
                    };
                };
            });
        }
    } else {
        $('#tol-active-cc-wrapper').hide();
    }

    if (widgets.user.isReal != true) {
        $('#tol-active-cc-wrapper').hide();
        $('#tol-currency-wrapper').show();
    }
};
//---------------------------------------------------------

// Used to update the infromation in the summary
var updateSummary = function () {
    $('span.depositType').text($("#depositTypeSelect option:selected").text());
    $('span.depositCurr').text($("#depositCurrSelect option:selected").text());
    if (!isNaN($('#depositAmountInput').val())) {
        $('span.depositAmount').text($('#depositAmountInput').val());
    };
};
//----------------------------------------------
// used to check is the user real
isUserReal = function () {
    if (typeof widgets.user != null) {
        if (!widgets.user.isReal) {
            $('.cashierCurencySelectWrapper').show();
            $('span.depositCurr').text((widgetsUser.currencyCode));
            $('.cashierWithdawColum1').hide();
            $('body').addClass('demoUser');
        };
    };
};

// Returns the current page in cashier: deposit, ccdeposit, withdraw, withdrawHistory, bonusHistory & transactionHistory
isPage = function () {
    var href = window.location.href.split("cpage=");
    var result = "deposit";
    if (typeof href[1] != 'undefined') {
        var hrefa = href[1].split("&");
        result = hrefa[0];
    };
    return result;
};
pageLingks = function () {
    rowsLength = $('tbody tr').length;
    rowsToAdd = 10 - rowsLength;
    rowsAdded = '';

    switch (isPage()) {
        case "bonusHistory":
            if (rowsLength < 10) {
                for (rowsToAdd; rowsAdded < rowsToAdd ; rowsToAdd--) { $('tbody').append("<tr><td>&nbsp;</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>"); };
            };
            break;
        case "transactionHistory":
        case "withdrawHistory":
        case "withdrawHistory#":
            if (rowsLength < 10) {
                for (rowsToAdd; rowsAdded < rowsToAdd ; rowsToAdd--) { $('tbody').append("<tr><td>&nbsp;</td><td></td><td></td><td></td></tr>"); };
            };
            break;
        case "deposit":
            changeFunc();
            break;
    };
};