$(document).ready(function () {
    assetListShown = true;
    rightSidebarShown = true;

    $('.mobile-switch-btn').click(function() {
        if ($(this).hasClass('active')) {
            return;
        } else if ($(this).hasClass('inv-switch')) {
            $('.mobile-switch-btn').removeClass('active');
            $(this).addClass('active');
            $('.chartwrapper.loaded').removeClass('showmobile');
        } else {
            $('.mobile-switch-btn').removeClass('active');
            $(this).addClass('active');
            $('.chartwrapper.loaded').addClass('showmobile');
        };
    });

    $('.footer-icon-positions > a').click(function(e) {
        if (!widgets.isLogged()) {
            e.stopPropagation();
            $('#tol-login-popup').modal('toggle');
        };

        if ($(this).parent().hasClass('active')) {
            return;
        } else {
            if ($('#balance-positions').html() == '') {
                $('#balance-positions').html($('#balance-orders').html());
                $('#balance-orders').html('');
            };
        };

        setTimeout(function() {
            if ($('.balance-toggle[data-target="#balance-positions"]').hasClass('collapsed')) {
                $('.positions-active .scrollable-area').css('height', $('#content').height() - 67 - $('#trades .fakeTableHead').height());
            } else {
                $('.positions-active .scrollable-area').css('height', $('#content').height() - $('.equity-container').height() - 67 - $('#trades .fakeTableHead').height());
            };

            $(".positions-active .scrollable-area-content").mCustomScrollbar("destroy");
            $(".positions-active .scrollable-area-content").mCustomScrollbar({
                scrollButtons: {
                    enable: false
                },
                horizontalScroll: false,
                autoDraggerLength: true,
                contentTouchScroll: true,
                scrollInertia: 600,
                autoHideScrollbar: true,
                advanced: {
                    autoScrollOnFocus: false,
                    updateOnContentResize: true,
                    updateOnBrowserResize: true
                }
            });
        }, 300);
    });

    $('.footer-icon-orders > a').click(function(e) {
        if (!widgets.isLogged()) {
            e.stopPropagation();
            $('#tol-login-popup').modal('toggle');
        };

        if ($(this).parent().hasClass('active')) {
            return;
        } else {
            if ($('#balance-orders').html() == '') {
                $('#balance-orders').html($('#balance-positions').html());
                $('#balance-positions').html('');
            };
        };

        setTimeout(function(){
            if ($('.balance-toggle[data-target="#balance-orders"]').hasClass('collapsed')) {
                $('.orders-active .scrollable-area').css('height', $('#content').height() - $('.forexPendingOrders').height() - 67);
            } else {
                $('.orders-active .scrollable-area').css('height', $('#content').height() - $('.equity-container').height() - $('.forexPendingOrders').height() - 67);
            };

            $(".orders-active .scrollable-area-content").mCustomScrollbar("destroy");
            $(".orders-active .scrollable-area-content").mCustomScrollbar({
                scrollButtons: {
                    enable: false
                },
                horizontalScroll: false,
                autoDraggerLength: true,
                contentTouchScroll: true,
                scrollInertia: 600,
                autoHideScrollbar: true,
                advanced: {
                    autoScrollOnFocus: false,
                    updateOnContentResize: true,
                    updateOnBrowserResize: true
                }
            });
        }, 300);
    });

    $('.balance-toggle').click(function() {
        if ($(this).hasClass('collapsed')) {
            if ($(this).attr('data-target') == '#balance-positions') {
                $('.positions-active .scrollable-area').css('height', $('#content').height() - $('.equity-container').height() - 67 - $('#trades .fakeTableHead').height());

                $(".positions-active .scrollable-area-content").mCustomScrollbar("destroy");
                $(".positions-active .scrollable-area-content").mCustomScrollbar({
                    scrollButtons: {
                        enable: false
                    },
                    horizontalScroll: false,
                    autoDraggerLength: true,
                    contentTouchScroll: true,
                    scrollInertia: 600,
                    autoHideScrollbar: true,
                    advanced: {
                        autoScrollOnFocus: false,
                        updateOnContentResize: true,
                        updateOnBrowserResize: true
                    }
                });
            } else {
                $('.orders-active .scrollable-area').css('height', $('#content').height() - $('.equity-container').height() - $('.forexPendingOrders').height() - 67);

                $(".orders-active .scrollable-area-content").mCustomScrollbar("destroy");
                $(".orders-active .scrollable-area-content").mCustomScrollbar({
                    scrollButtons: {
                        enable: false
                    },
                    horizontalScroll: false,
                    autoDraggerLength: true,
                    contentTouchScroll: true,
                    scrollInertia: 600,
                    autoHideScrollbar: true,
                    advanced: {
                        autoScrollOnFocus: false,
                        updateOnContentResize: true,
                        updateOnBrowserResize: true
                    }
                });
            }
        } else {
            if ($(this).attr('data-target') == '#balance-positions') {
                $('.positions-active .scrollable-area').css('height', $('#content').height() - 67 - $('#trades .fakeTableHead').height());

                $(".positions-active .scrollable-area-content").mCustomScrollbar("destroy");
                $(".positions-active .scrollable-area-content").mCustomScrollbar({
                    scrollButtons: {
                        enable: false
                    },
                    horizontalScroll: false,
                    autoDraggerLength: true,
                    contentTouchScroll: true,
                    scrollInertia: 600,
                    autoHideScrollbar: true,
                    advanced: {
                        autoScrollOnFocus: false,
                        updateOnContentResize: true,
                        updateOnBrowserResize: true
                    }
                });
            } else {
                $('.orders-active .scrollable-area').css('height', $('#content').height() - $('.forexPendingOrders').height() - 67);

                $(".orders-active .scrollable-area-content").mCustomScrollbar("destroy");
                $(".orders-active .scrollable-area-content").mCustomScrollbar({
                    scrollButtons: {
                        enable: false
                    },
                    horizontalScroll: false,
                    autoDraggerLength: true,
                    contentTouchScroll: true,
                    scrollInertia: 600,
                    autoHideScrollbar: true,
                    advanced: {
                        autoScrollOnFocus: false,
                        updateOnContentResize: true,
                        updateOnBrowserResize: true
                    }
                });
            }
        }
    });

    $( '#toggle-menu, #toggle-menu2' ).on( 'click', function(e) {
        var $body = $( 'body' );

        if ( $body.hasClass( 'menu-visible' ) ) {
            $body.removeClass( 'left right menu-visible' );
            $('.page-traderoom').css('width','100%');
            $('.page-traderoom #content').css('height', '100%');
            return;
        };

        $body.addClass( 'menu-visible' );
        $('.page-traderoom').css('width',$(document).innerWidth()+'px');
        $('.page-traderoom #content').css('height', $(document).innerHeight());

        if ( $body.hasClass( 'menu-visible' ) ) {
            $body.addClass( 'left' );
        } else {
            $body.addClass( 'right' );
        };
    } );

    $('#userbar-mobilemenu, #toggle-usermenu').on('click', function(e) {
        var $body = $( 'body' );

        $body.toggleClass( 'usermenu-visible' );

        if ( $body.hasClass( 'usermenu-visible' ) ) {
            $body.addClass( 'right' );
        } else {
            $body.addClass( 'left' );
        };
    });

    $('.info-icon').tooltip();

    if ($(document).width() > 1024 && window.location.pathname.indexOf('traderoom') > -1) {
        $('.chartpanel, .orders-trades-table-wrapper').css('width', $(document).width() - (assetListShown ? (rightSidebarShown ? 650 : 320) : (rightSidebarShown ? 330 : 0)));
        $('.chart-separator').css('width', $(document).width() - (assetListShown ? (rightSidebarShown ? 650 : 320) : (rightSidebarShown ? 330 : 0))).css('top', ($(window.top).height() - 119) * 0.64);
        $('.chartpanel').css('height', ($(window.top).height() - 119) * 0.64);
        $('#tradePanel, .trade-separator').css('height', ($(window.top).height() - 119));
        $('.sidepanel-tabs').css('height', ($(window.top).height() - 121));
        $('.sidepanel-tabs .tab-content').css('height', ($(window.top).height() - 156));
        $('.sidepanel-tabs .tab-content .scrollable-area').css('height', ($(window.top).height() - 237));
        $('.sidepanel-tabs .tab-content #economic-calendar .scrollable-area').css('height', ($(window.top).height() - 267));
        $('.scrollable-social').css('height', $(window).height() - 349);
        $('.orders-trades-table-wrapper').css('height', ($(window.top).height() - 119) * 0.36 - 7).css('top', ($(window.top).height() - 119) * 0.64 + 5);
        $('.orders-trades-table-wrapper .scrollable-area').css('height', $('.orders-trades-table-wrapper').height() - 77);
        $('.scrollable-area.asset-table-left').css('height', $(window.top).height() - 221);
        $('.tol-wp-navigation').css('height', ($(document).innerHeight()));
        //easy forex
        $('.easy-forex-table-wrapper, .middle-separator, .tradearea, .boxfilter, .tradewrapper').css('width', $(document).width() - (rightSidebarShown ? 325 : 0));
        $('.tradearea').css('height', ($(window.top).height() - 119) * 0.64 - 65);
        $('.vertical-separator.side-separator').css('height', $(window.top).height() - 119);
        $('.tradewrapper').css('height', ($(window.top).height() - 119) * 0.64);
        $('.easy-forex-table-wrapper').css('height', ($(window.top).height() - 119) * 0.36 - 5);
        $('.easy-forex-table-wrapper').css('top', ($(window.top).height() - 119) * 0.64 + 5);
        $('.easy-forex-table-wrapper .scrollable-area').css('height', $('.easy-forex-table-wrapper').height() - 69);
        $('.middle-separator').css('top', ($(window.top).height() - 119) * 0.64);
    } else {
        rightSidebarShown = false;
    };

    $(window).resize(function(){
        if (screen.width > 1024 && window.location.pathname.indexOf('traderoom') > -1) {
            $('.chartpanel, .orders-trades-table-wrapper').css('width', $(document).width() - (assetListShown ? (rightSidebarShown ? 650 : 320) : (rightSidebarShown ? 330 : 0)));
            $('.chart-separator').css('width', $(document).width() - (assetListShown ? (rightSidebarShown ? 650 : 320) : (rightSidebarShown ? 330 : 0)));
            $('.chartpanel').css('height', ($(window.top).height() - 119) * 0.64);
            $('.asset-table-left').css('height', ($(window.top).height() - 119));
            $('.sidepanel-tabs').css('height', ($(window.top).height() - 119));
            $('.sidepanel-tabs .tab-content').css('height', ($(window.top).height() - 160));
            $('.sidepanel-tabs .tab-content .scrollable-area').css('height', ($(window.top).height() - 237));
            $('.orders-trades-table-wrapper').css('height', ($(window.top).height() - 119) * 0.36 - 5);
            $('.orders-trades-table-wrapper').css('top', ($(window.top).height() - 119) * 0.64 + 5);
            $('.chart-separator').css('top', ($(window.top).height() - 119) * 0.64);
            $('.orders-trades-table-wrapper .scrollable-area').css('height', $('.orders-trades-table-wrapper').height() - 77);
            $('.scrollable-area.asset-table-left').css('height', $(window.top).height() - 229);
            //easy forex
            $('.easy-forex-table-wrapper, .middle-separator, .tradearea, .boxfilter, .tradewrapper').css('width', $(document).width() - (rightSidebarShown ? 325 : 0));
            $('.tradearea').css('height', ($(window.top).height() - 119) * 0.64 - 65);
            $('.tradewrapper').css('height', ($(window.top).height() - 119) * 0.64);
            $('.easy-forex-table-wrapper').css('height', ($(window.top).height() - 119) * 0.36 - 5);
            $('.easy-forex-table-wrapper').css('top', ($(window.top).height() - 119) * 0.64 + 5);
            $('.easy-forex-table-wrapper .scrollable-area').css('height', $('.easy-forex-table-wrapper').height() - 69);
            $('.middle-separator').css('top', ($(window.top).height() - 119) * 0.64);
        } else {
            rightSidebarShown = false;

            if ($(document).width() == 1024) {
                $('.scrollable-area.asset-table-left').css('height', $(window.top).height() - 187);
            };
        };
    });

    //toggle panels
    $(".trade-separator .separator-collapse").on("click", function(e) {
        e.preventDefault();

        if($('.trade-separator').hasClass("closed")) {
            $('.trade-separator').removeClass("closed");
            openAssetList();
        } else {
            closeAssetList();
        };
    });
    function openAssetList() {
        $('.trade-separator').animate({
            left: "320px"
        }, 200);
        $('.tradepanel').animate({
            left: "0"
        }, 200);
        $('.chartpanel').animate({
            width : $(document).width() - (rightSidebarShown ? 650 : 320),
            marginLeft: "325px"
        }, 800);
        if (screen.width > 768) {
            $('.orders-trades-table-wrapper').animate({
                width : $(document).width() - (rightSidebarShown ? 650 : 320),
                left: "325px"
            }, 800);
        };
        assetListShown = true;
    };
    function closeAssetList(){
        $('.trade-separator').addClass("closed");
        $('.tradepanel').animate({
            left: ("-" + $('.tradepanel').width() + "px")
        }, 800);
        $('.chartpanel').animate({
            left: 0,
            width : $(document).width()-(rightSidebarShown ? 330 : 10),
            marginLeft: "5px"
        }, 200);
        if (screen.width > 768) {
            $('.orders-trades-table-wrapper').animate({
                left: "5px",
                width: $(document).width() - (rightSidebarShown ? 330 : 0)
            }, 200);
        };
        assetListShown = false;
    };

    $(".side-separator").on("click", function(e) {
        e.preventDefault();
        var distance = $('.chartpanel').css('margin-right');

        if (widgets.game == 'EasyForex') {
            distance = $('.tradewrapper').css('margin-right');
        };

        if(distance == "0px") {
            $(this).removeClass("closed");
            openRightSidebar();
        } else {
            closeRightSidebar();
        };
    });
    function openRightSidebar() {
        $('.side-separator').animate({
            right: "320px"
        }, 200);
        $('.sidepanel-tabs').animate({
            right: "0"
        }, 200);
        $('.chartpanel').animate({
            width : $(document).width() - (assetListShown?650:330),
            marginRight: "350px"
        }, 800);
        $('.tradewrapper, .tradearea, .boxfilter').animate({
            width : $(document).width() - 325,
            marginRight: "5px"
        }, 200);
        if (screen.width > 768) {
            $('.orders-trades-table-wrapper').animate({
                width : $(document).width()-(assetListShown?($(document).width() > 1024 ? 650: 330):330),
            }, 800);
            $('.easy-forex-table-wrapper').animate({
                width: $(document).width() - 325
            }, 200);
        };
        rightSidebarShown = true;

        setTimeout(function() {
            $(window).trigger(widgets.events.easyForexContainerResize);
        }, 200);
    };
    function closeRightSidebar(){
        $('.side-separator').addClass("closed").animate({
            right: "0"
        }, 800);
        $('.sidepanel-tabs').animate({
            right: "-340px"
        }, 800);
        $('.chartpanel').animate({
            width : $(document).width() - (assetListShown ? 330 : 10),
            marginRight: "0px"
        }, 200);
        $('.tradewrapper, .tradearea, .boxfilter').animate({
            width : screen.width,
            marginRight: "0px"
        }, 200);
        if (screen.width > 768) {
            $('.orders-trades-table-wrapper').animate({
                width: $(document).width() - (assetListShown ? 320 : 0)
            }, 200);
            $('.easy-forex-table-wrapper').animate({
                width: $(document).width()
            }, 200);
        };
        rightSidebarShown = false;

        setTimeout(function() {
            $(window).trigger(widgets.events.easyForexContainerResize);
        }, 200);
    };

    //start easy forex middle separator functionality
    $('.easyForex-middle-separator').on("click", function(e) {
        e.preventDefault();

        if($('.tradewrapper').css('margin-bottom') == "5px") {
            $('.easyForex-middle-separator').removeClass("closed");
            expandEasyCardContainer();
        } else {
            collapseEasyCardContainer();
        };
    });
    function expandEasyCardContainer() {
        $('.easyForex-middle-separator').animate({
            top: ($(window.top).height() - 119) * 0.64
        }, 800);
        $('.tradewrapper').animate({
            height : ($(window.top).height() - 119) * 0.64,
            marginBottom: "0"
        }, 800);
        $('.tradearea').animate({
            height : (($(window.top).height() - 119) * 0.64) -65
        }, 200);

        $('.easy-forex-table-wrapper').animate({
            height : ($(window.top).height() - 119) * 0.36 - 5,
            top: ($(window.top).height() - 119) * 0.64 + 5
        }, 200);

        setTimeout(function() {
            $(window).trigger(widgets.events.easyForexContainerResize);
        }, 800);
    };
    function collapseEasyCardContainer() {
        $('.easyForex-middle-separator').addClass("closed");
        $('.easyForex-middle-separator').animate({
            top: $(window.top).height() - 129
        }, 200);
        $('.tradewrapper').animate({
            height : $(window.top).height() - 129,
            marginBottom: "5px"
        }, 200);
        $('.tradearea').animate({
            height : $(window.top).height() - 184,
        }, 200);

        $('.easy-forex-table-wrapper').animate({
            top: $(window.top).height() - 119
        }, 800);

        setTimeout(function() {
            $(window).trigger(widgets.events.easyForexContainerResize);
        }, 200);
    };
    //end easy forex middle separator functionality

    $(".chart-separator .separator-collapse").on("click", function(e) {
        e.preventDefault();

        if (screen.width < 1025) {
            return;
        };

        var distance = $('.chartpanel').css('margin-bottom');

        if(distance == "5px") {
            $('.chart-separator').removeClass("closed");
            openTradesTables();
        } else {
            closeTradesTables();
        };
    });
    function openTradesTables() {
        $('.chart-separator').animate({
            top: ($(window.top).height() - 119) * 0.64
        }, 800);
        $('.chartpanel').animate({
            height : ($(window.top).height() - 119) * 0.64,
            marginBottom: "0"
        }, 800);
        $('.orders-trades-table-wrapper').animate({
            height : ($(window.top).height() - 119) * 0.36 - 5,
            top: ($(window.top).height() - 119) * 0.64 + 5
        }, 200);
    };
    function closeTradesTables() {
        $('.chart-separator').addClass("closed");
        $('.chart-separator').animate({
            top: $(window.top).height() - 124
        }, 200);
        $('.chartpanel').animate({
            height : $(window.top).height() - 124,
            marginBottom: "5px"
        }, 200);
        $('.orders-trades-table-wrapper').animate({
            top: $(window.top).height() - 119
        }, 800);
    };
    // end toggle panels

    $('.account .tabpositions .multiple-selection-content .item').on('click', function (event) {
        var columnIndex = $(this).attr('data-column');
        event.stopPropagation();

        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            if (widgets.openPositionsColumns){
                widgets.openPositionsColumns[columnIndex] = false;
            };

            $("#trades .dataTable th:nth-child("+(columnIndex)+")").hide();

            if (widgets.isMobileDevice()) {
                $("#trades .dataTable div[data-type='openPositionsRow'] > div:nth-child("+(columnIndex)+")").hide();
            } else {
                $('#trades .dataTable tr td:nth-child('+(columnIndex)+')').hide();
            };
        } else {
            $(this).addClass('selected');
            if (widgets.openPositionsColumns){
                widgets.openPositionsColumns[columnIndex] = true;
            };

            $("#trades .dataTable th:nth-child("+(columnIndex)+")").show();

            if (widgets.isMobileDevice()) {
                $("#trades .dataTable div[data-type='openPositionsRow'] > div:nth-child("+(columnIndex)+")").show();
            } else {
                $('#trades .dataTable tr td:nth-child('+(columnIndex)+')').show();
            };
        };

        if($.cookie('openPositionsColumns')){
            $.cookie('openPositionsColumns',JSON.stringify(widgets.openPositionsColumns), {path: "/"});
        }else{
            $.cookie('openPositionsColumns',JSON.stringify(widgets.openPositionsColumns), {path: "/"});
        }
    });

    $('#tol-login-popup').on('shown.bs.modal', function () {
        $('#username').focus();
    });

    $('.account .taborders .multiple-selection-content .item').on('click', function (event) {
        var columnIndex = $(this).attr('data-column');
        event.stopPropagation();

        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            if (widgets.pendingOrdersColumns){
                widgets.pendingOrdersColumns[columnIndex] = false;
            };

            $("#orders .dataTable th:nth-child("+(columnIndex)+")").hide();

            if (widgets.isMobileDevice()) {
                $("#orders .dataTable div[data-type='pendingOrderRow'] > div:nth-child("+(columnIndex)+")").hide();
            } else {
                $('#orders .dataTable tr td:nth-child('+(columnIndex)+')').hide();
            };
        } else {
            $(this).addClass('selected');
            if (widgets.pendingOrdersColumns){
                widgets.pendingOrdersColumns[columnIndex] = true;
            };

            $("#orders .dataTable th:nth-child("+(columnIndex)+")").show();

            if (widgets.isMobileDevice()) {
                $("#orders .dataTable div[data-type='pendingOrderRow'] > div:nth-child("+(columnIndex)+")").show();
            } else {
                $('#orders .dataTable tr td:nth-child('+(columnIndex)+')').show();
            };
        };

        if($.cookie('pendingOrdersColumns')){
            $.cookie('pendingOrdersColumns',JSON.stringify(widgets.pendingOrdersColumns), {path: "/"});
        }else{
            $.cookie('pendingOrdersColumns',JSON.stringify(widgets.pendingOrdersColumns), {path: "/"});
        }
    });

    //open positions Easy Forex show/hide columns
    $('.account .tabeasypositions .multiple-selection-content .item').on('click', function (event) {
        var columnIndex = $(this).attr('data-column');
        event.stopPropagation();

        if(columnIndex==3 || columnIndex==4) return;

        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            if (widgets.easyOpenPositionsColumns){
                widgets.easyOpenPositionsColumns[columnIndex] = false;
            };

            if (widgets.isMobileDevice()) {
                $('#trades .fakeTableHead thead th:nth-child('+(columnIndex)+'), #forexProOpenTradesTable tbody > div[data-type="openPositionsRow"] > div:nth-child('+(columnIndex)+')').hide();
            } else {
                $('.tab-pane.active .fake-table thead th:nth-child('+(columnIndex)+'), .tab-pane.active #forexProOpenTradesTable tbody td:nth-child('+(columnIndex)+')').hide();
            };
        } else {
            $(this).addClass('selected');
            if (widgets.easyOpenPositionsColumns){
                widgets.easyOpenPositionsColumns[columnIndex] = true;
            };

            if (widgets.isMobileDevice()) {
                $('#trades .fakeTableHead thead th:nth-child('+(columnIndex)+'), #forexProOpenTradesTable tbody > div[data-type="openPositionsRow"] > div:nth-child('+(columnIndex)+')').show();
            } else {
                $('.tab-pane.active .fake-table thead th:nth-child('+(columnIndex)+'), .tab-pane.active #forexProOpenTradesTable tbody td:nth-child('+(columnIndex)+')').show();
            };
        };

        if($.cookie('easyOpenPositionsColumns')){
            $.cookie('easyOpenPositionsColumns',JSON.stringify(widgets.easyOpenPositionsColumns));
        } else {
            $.cookie('easyOpenPositionsColumns',JSON.stringify(widgets.easyOpenPositionsColumns), {path: "/"});
        };
    });

    //closed positions Easy Forex show/hide columns
    $('.account .tab-easy-closed-positions .multiple-selection-content .item').on('click', function (event) {
        var columnIndex = $(this).attr('data-column');
        event.stopPropagation();

        if(columnIndex==3 || columnIndex==4) return;

        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            if (widgets.easyClosedPositionsColumns){
                widgets.easyClosedPositionsColumns[columnIndex] = false;
            };

            if (widgets.isMobileDevice()) {
                $('#pastInvestments .fakeTableHead thead th:nth-child('+(columnIndex)+'), #easyForexClosedTradesTable tbody > div[data-type="openPositionsRow"] > div:nth-child('+(columnIndex)+')').hide();
            } else {
                $('.tab-pane.active .fake-table thead th:nth-child('+(columnIndex)+'), .tab-pane.active #forexProOpenTradesTable tbody td:nth-child('+(columnIndex)+')').hide();
            };
        } else {
            $(this).addClass('selected');
            if (widgets.easyClosedPositionsColumns){
                widgets.easyClosedPositionsColumns[columnIndex] = true;
            };

            if (widgets.isMobileDevice()) {
                $('#pastInvestments .fakeTableHead thead th:nth-child('+(columnIndex)+'), #easyForexClosedTradesTable tbody > div[data-type="openPositionsRow"] > div:nth-child('+(columnIndex)+')').show();
            } else {
                $('.tab-pane.active .fake-table thead th:nth-child('+(columnIndex)+'), .tab-pane.active #forexProOpenTradesTable tbody td:nth-child('+(columnIndex)+')').show();
            };
        };

        if($.cookie('easyClosedPositionsColumns')){
            $.cookie('easyClosedPositionsColumns',JSON.stringify(widgets.easyClosedPositionsColumns), {path: "/"});
        } else {
            $.cookie('easyClosedPositionsColumns',JSON.stringify(widgets.easyClosedPositionsColumns), {path: "/"});
        };
    });

    //easy forex
    $('#tol-trades-popup .tabeasytrades .multiple-selection-content .item').on('click', function (event) {
        var columnIndex = $(this).attr('data-column');
        event.stopPropagation();

        if(columnIndex==3 || columnIndex==4) return;
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            if (widgets.myEasyForexTradesColumns){
                widgets.myEasyForexTradesColumns[columnIndex] = false;
            }
            $('#tol-trades-popup .my-trades-table.easy-trades-table thead th:nth-child('+(columnIndex)+'), #tol-trades-popup .my-trades-table.easy-trades-table tbody td:nth-child('+(columnIndex)+')').hide();
        } else {
            $(this).addClass('selected');
            if (widgets.myEasyForexTradesColumns){
                widgets.myEasyForexTradesColumns[columnIndex] = true;
            }
            $('#tol-trades-popup .my-trades-table.easy-trades-table thead th:nth-child('+(columnIndex)+'), #tol-trades-popup .my-trades-table.easy-trades-table tbody td:nth-child('+(columnIndex)+')').show();
        };

        // set cookie
        if($.cookie('myTradesColumns')){
            $.cookie('myTradesColumns',JSON.stringify(widgets.myTradesColumns));
        }else{
            $.cookie('myTradesColumns',JSON.stringify(widgets.myTradesColumns), {path: "/"});
        }
    });

    //defaultSettings btn on the trades tables
    $('.account .tabpositions .defaultSettings').on('click', function (event) {
        $('.account .tabpositions .multiple-selection-content .item').addClass('selected');
        $.each(openPositionsColumnsDefault, function (i, isVisible) {
			
            if (isVisible === true){
                //hide header column
                $("#trades .dataTable th:nth-child(" + i +")").show();
                //hide rows				
				if (widgets.isMobileDevice()) {
					$("#trades .dataTable div[data-type='openPositionsRow'] > div:nth-child("+(i)+")").show();
				} else {
					$('#trades .dataTable tr td:nth-child('+(i)+')').show();
				};
				
            } else{
                $("#trades .dataTable th:nth-child(" + i +")").hide();
                //hide rows
				if (widgets.isMobileDevice()) {
					$("#trades .dataTable div[data-type='openPositionsRow'] > div:nth-child("+(i)+")").hide();
				} else {
					$('#trades .dataTable tr td:nth-child('+(i)+')').hide();
				};
				
                $(".account .tabpositions .multiple-selection-content .item[data-column="+ i +"]").removeClass('selected');
            }
        });
        widgets.openPositionsColumns = $.extend(true, {}, openPositionsColumnsDefault);

        // set cookie
        if($.cookie('openPositionsColumns')){
            $.cookie('openPositionsColumns',JSON.stringify(widgets.openPositionsColumns), {path: "/"});
        }else{
            $.cookie('openPositionsColumns',JSON.stringify(widgets.openPositionsColumns), {path: "/"});
        }
    });

     //easy Forex open positions default settings
    $('.account .tabeasypositions .defaultSettings').on('click', function (event) {

        $('.account .tabeasypositions .multiple-selection-content .item').addClass('selected');
        $.each(easyOpenPositionsColumnsDefault, function (i, isVisible) {
            if (isVisible === true){
                $("#trades .dataTable th:nth-child(" + i +")").show();
				
                if (widgets.isMobileDevice()) {
                $('#trades .fakeTableHead thead th:nth-child('+i+'), #forexProOpenTradesTable tbody > div[data-type="openPositionsRow"] > div:nth-child('+i+')').show();
				} else {
					$('.tab-pane.active .fake-table thead th:nth-child('+i+'), .tab-pane.active #forexProOpenTradesTable tbody td:nth-child('+i+')').show();
				};
            } else{
                $("#trades .dataTable th:nth-child(" + i +")").hide();
					if (widgets.isMobileDevice()) {
						$('#trades .fakeTableHead thead th:nth-child('+i+'), #forexProOpenTradesTable tbody > div[data-type="openPositionsRow"] > div:nth-child('+i+')').hide();
					} else {
						$('.tab-pane.active .fake-table thead th:nth-child('+i+'), .tab-pane.active #forexProOpenTradesTable tbody td:nth-child('+i+')').hide();
					};
                $(".account .tabeasypositions .multiple-selection-content .item:nth-child("+ i +")").removeClass('selected');
            }
        });
        widgets.easyOpenPositionsColumns = $.extend(true, {}, easyOpenPositionsColumnsDefault);

        // set cookie
        if($.cookie('easyOpenPositionsColumns')){
            $.cookie('easyOpenPositionsColumns',JSON.stringify(widgets.easyOpenPositionsColumns));
        }else{
            $.cookie('easyOpenPositionsColumns',JSON.stringify(widgets.easyOpenPositionsColumns), {path: "/"});
        };
    });

    //easy Forex closed positions default settings
    $('.account .tab-easy-closed-positions .defaultSettings').on('click', function (event) {

        $('.account .tab-easy-closed-positions .multiple-selection-content .item').addClass('selected');
        $.each(easyClosedPositionsColumnsDefault, function (i, isVisible) {
            if (isVisible === true){
				
                $("#pastInvestments .dataTable th:nth-child(" + i +")").show();
				if (widgets.isMobileDevice()) {
                    $('#pastInvestments .fakeTableHead thead th:nth-child('+i+'), #easyForexClosedTradesTable tbody > div[data-type="openPositionsRow"] > div:nth-child('+i+')').show();
                } else {
                    $('.tab-pane.active .fake-table thead th:nth-child('+i+'), .tab-pane.active #forexProOpenTradesTable tbody td:nth-child('+i+')').show();
                };
            } else{
                $("#pastInvestments .dataTable th:nth-child(" + i +")").hide();
				if (widgets.isMobileDevice()) {
                    $('#pastInvestments .fakeTableHead thead th:nth-child('+i+'), #easyForexClosedTradesTable tbody > div[data-type="openPositionsRow"] > div:nth-child('+ i +')').hide();
                } else {
                    $('.tab-pane.active .fake-table thead th:nth-child('+ i +'), .tab-pane.active #forexProOpenTradesTable tbody td:nth-child('+ i +')').hide();
                };
                $(".account .tab-easy-closed-positions .multiple-selection-content .item:nth-child("+ i +")").removeClass('selected');
            }
        });
        widgets.easyClosedPositionsColumns = $.extend(true, {}, easyClosedPositionsColumnsDefault);

        // set cookie
        if($.cookie('easyClosedPositionsColumns')){
            $.cookie('easyClosedPositionsColumns',JSON.stringify(widgets.easyClosedPositionsColumns));
        }else{
            $.cookie('easyClosedPositionsColumns',JSON.stringify(widgets.easyClosedPositionsColumns), {path: "/"});
        };
    });

    //defaultSettings btn on the trades tables
    $('.account .taborders .defaultSettings').on('click', function (event) {

        $('.account .taborders .multiple-selection-content .item').addClass('selected');
        $.each(pendingOrdersColumnsDefault, function (i, isVisible) {
            if (isVisible === true){
                //show header column
                $("#orders .dataTable th:nth-child(" + i +")").show();
                //show rows
				if (widgets.isMobileDevice()) {
					$("#orders .dataTable div[data-type='pendingOrderRow'] > div:nth-child("+(i)+")").show();
				} else {
					$("#orders .dataTable td:nth-child("+ i +")").show();
				};
            }
            else{
                //hide header column
                $("#orders .dataTable th:nth-child(" + i +")").hide();
                //hide rows
				if (widgets.isMobileDevice()) {
					$("#orders .dataTable div[data-type='pendingOrderRow'] > div:nth-child("+(i)+")").hide();
				} else {
					$("#orders .dataTable td:nth-child("+ i +")").hide();
				};

				$(".account .taborders .multiple-selection-content .item[data-column="+ i +"]").removeClass('selected');
            }
        });
        widgets.pendingOrdersColumns = $.extend(true, {}, pendingOrdersColumnsDefault);
        if($.cookie('pendingOrdersColumns')){
            $.cookie('pendingOrdersColumns',JSON.stringify(widgets.pendingOrdersColumns), {path: "/"});
        }else{
            $.cookie('pendingOrdersColumns',JSON.stringify(widgets.pendingOrdersColumns), {path: "/"});
        }
    });

    //Footer settings icon
    $('.footer-settings-icon .multiple-selection-content .item').on('click', function (event) {
        var elToHide = $(this).attr('data-column');
        event.stopPropagation();

        if(elToHide==1) return;
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            if (widgets.footerSettings){
                widgets.footerSettings[elToHide] = false;
            }
            $('.equity-content .equity-item:nth-child('+(elToHide)+')').hide();
        } else {
            $(this).addClass('selected');
            if (widgets.footerSettings){
                widgets.footerSettings[elToHide] = true;
            }
            $('.equity-content .equity-item:nth-child('+(elToHide)+')').show();
        };

        // set cookie
        if($.cookie('footerSettings')){
            $.cookie('footerSettings',JSON.stringify(widgets.footerSettings), {path: "/"});
        }else{
            $.cookie('footerSettings',JSON.stringify(widgets.footerSettings), {path: "/"});
        }
    });

    $('.footer-settings-icon .defaultSettings').on('click', function (event) {
        $('.equity-item').show();
        $('.footer-settings-icon .multiple-selection-content .item').addClass('selected');
        widgets.footerSettings = $.extend(true, {}, footerSettingsDefault);
        // set cookie
        if($.cookie('footerSettings')){
            $.cookie('footerSettings', JSON.stringify(footerSettings), {path: "/"});
        } else {
            $.cookie('footerSettings', JSON.stringify(footerSettings), {path: "/"});
        };
    });
    //End footer settings icon

    $('.nav').on('click', function (event) {
        var $element = $(event.target),
            id = event.target.id;

        if ($('.navtabs').filter('.active').length > 0) {
            $('.navtabs').filter('.active').removeClass('active');
        }
        $element.addClass('active');

        if ($('img').filter('.active').length > 0) {
            $('img').filter('.active').removeClass('active');
            $('img#' + id + '').addClass('active');
        }

        $('.tabs img').fadeOut('fast');
        $('.tabs img.active').fadeIn('fast');
    });

    $('.navigation .nav-user.nav-button').click(function(){
        if ($('.tol-wp-navigation').is('visible')) {
            $('.nav-button.main-menu').click();
        };
    });

    //ninja forms submit request
    $( '.ninja-forms-form' ).on( 'submitResponse', function( e, response ) {
        var errors = response.errors;

        if ( errors == false ) {
            $('.close').click()
        };
    });

    $(document).on("click", ".account .tab-item", function () {
        $('.account').removeClass('orders-active').removeClass('positions-active');

        if ($(this).hasClass('taborders')) {
            $('.account').addClass('orders-active');
            $('.closeAll.orders.button').show();
            $('.closeAll.positions.button').hide();
        }
        if($(this).hasClass('tabpositions')) {
            $('.account').addClass('positions-active');
            $('.closeAll.positions.button').show();
            $('.closeAll.orders.button').hide();
        }

        $('.account .tab-item').removeClass('tab-active');
        $(this).addClass('tab-active');
    });

    $('.tabHolder a.simple_page').click(function() {
        $("#subMenu").hide();
        var this_id = $(this).attr('href').replace('#', '');
        $('#'+this_id+' iframe').attr('src', location.origin+'/wp-content/themes/tol-parent/widget-blank-page.php?alias='+$(this).attr('data-url'));
    });

    $(document).on("click", ".account .tab-item", function () {
        $('.account').removeClass('close-trades-active open-trades-active positions-active');

        if ($(this).hasClass('taborders')) {
            $('.account').addClass('close-trades-active').removeClass('positions-active');
            $('.column-settings.tabpositions').hide();
            $('.column-settings.taborders').show();
            $('.myTradesInfo').show();
        }
        if($(this).hasClass('tabpositions')) {
            $('.account').addClass('open-trades-active positions-active');
            $('.column-settings.tabpositions').show();
            $('.column-settings.taborders').hide();
            $('.myTradesInfo').hide();
        }

        $('.account .tab-item').removeClass('tab-active');
        $(this).addClass('tab-active');
    });

    $(document).on("click", ".right-sidebar-nav li a", function () {
        $('.right-sidebar-nav li a').removeClass('active');
        $(this).addClass('active');
    });

    $(document).on("click", ".tol-quick-next", function () {
        $('.my-profile-us-wrapper-btns').hide();
        $('.game-btn-box.userdetails').removeClass('active');
        $('.game-btn-box.questionnaires').addClass('active');
        $('.questionnaires a').click();
        return false;
    });

    $(document).on("click", ".my-profile-form .game-btn-box a", function () {
        $('.my-profile-us-wrapper-btns').hide();
        $('.game-btn-box').removeClass('active');
        $(this).parent().addClass('active');
        if ($(this).parent().hasClass('userdetails')) {
            $('.my-profile-us-wrapper-btns').show();
        };
        return false;
    });

    $(".toggleChart").click(function () {

        if (typeof tradingViewWidget !== typeof undefined) {
            tradingViewWidget.setSymbol($('#tol-trade-popup').data('taid').toString(), tradingViewWidget.symbolInterval().interval);
        }

        const waitingForPopup = 200;
        setTimeout( function waitForPopup(){
            const displayProperty = $("#tol-trade-popup").css("display");
            if(displayProperty !== "none"){
                tradingViewWidget.chart().executeActionById("drawingToolbarAction");
                tradingViewWidget.chart().executeActionById("drawingToolbarAction");
                tradingViewWidget.chart().executeActionById("timeScaleReset");
            }
            if(displayProperty === "none"){
                setTimeout(waitForPopup,waitingForPopup);
            }
        },waitingForPopup);
    });

    $('.tol-userbar-inbox').click(function () {
        window.location.href = getPathLanguagePrefix() + '/inbox';
        return;
    });

    $('.tol-userbar-my-statistics').click(function () {
        window.location.href = getPathLanguagePrefix() + '/my-statistics';
        return;
    });
    $('.tol-userbar-my-profile').click(function () {
        window.location.href = getPathLanguagePrefix() + '/my-profile';
        return;
    });
    $('.tol-userbar-change-password').click(function () {
        window.location.href = getPathLanguagePrefix() + '/change-password';
        return;
    });

    $('.tol-create-account').click(function () {
        window.location.href = getPathLanguagePrefix() + '/registration';
        return;
    });

    $('.tol-trade').click(function () {
        if (widgets.isLogged()) {
            window.location.href = getPathLanguagePrefix() + getTraderoomLink(traderoomMode.realForex);
        } else {
            $('#tol-login-popup').modal('toggle');
        }
    });
	
	$('.tol-trade-experience-platform').click(function () {
        if (widgets.isLogged()) {
            window.location.href = getPathLanguagePrefix() + getTraderoomLink(traderoomMode.realForex);
        } else {
            window.location.href = getPathLanguagePrefix() + '/registration';
        }
    });
	
	$('.tol-trade-nav').click(function () {    
        window.location.href = getPathLanguagePrefix() + getTraderoomLink(traderoomMode.realForex);
    });
	

    $('.real-register-button').click(function () {
        window.location.href = getPathLanguagePrefix() + '/registration';
        return;
    });

    $('.trade-now-btn').click(function () {
        window.location.href = getPathLanguagePrefix() + '/registration';
        return;
    });

    $('.tol-quick-deposit').click(function () {
        window.location.href = getPathLanguagePrefix() + '/cashier';
        return;
    });

    $('.demo-register-button').click(function () {
        $('#tol-registration-popup').modal('toggle');
        return;
    });


    $('.nav-button.has-icon.main-menu').click(function(e){
        if ($(this).hasClass('show')) {
            $(this).removeClass('show');
        } else {
            $(this).addClass('show');
        };
        return;
    });

    $(document).click(function(){
        $(".column-settings").removeClass('show');
        $(".nav-button.has-icon.main-menu").removeClass('show');
        if (typeof realForexWidget != 'undefined') {
            $('.amount-dropdown, .assetsType-dropdown').remove();
            if ($('[data-type="quantitycontainer"]').length > 0) {
                realForexWidget.removeQuantityDropDown();
            };
        };
    });

    $('.tol-bxzone-trade-cancel-BTN').click(function () {
        $('#tol-bxzone-trade').modal('hide');
    });

    $('#orders-menu').on('click', function () {
        $(this).addClass('active');
        $('#trades-menu').removeClass('active');
        $('[data-widget="exchangeTrades"]').removeClass('active').hide();
        $('[data-widget="exchangeOrders"]').addClass('active').show();
        $('#be-orders-count').addClass('active');
        $('#be-trades-count').removeClass('active');
    });

    $('#trades-menu').on('click', function () {
        $(this).addClass('active');
        $('#orders-menu').removeClass('active');
        $('[data-widget="exchangeOrders"]').removeClass('active').hide();
        $('[data-widget="exchangeTrades"]').addClass('active').show();
        $('#be-trades-count').addClass('active');
        $('#be-orders-count').removeClass('active');
    });

	$('.learning-section.hvr-push').on("click", function () {
        window.location.href = getPathLanguagePrefix() + '/learn';

    });
	$('.news-section.hvr-push').on("click", function () {
        window.location.href = getPathLanguagePrefix() + '/news';

    });
	$('.calendar-section.hvr-push').on("click", function () {
        window.location.href = getPathLanguagePrefix() + '/calendar';

    });
	$('.faqSection.hvr-push').on("click", function () {
        window.location.href = getPathLanguagePrefix() + '/support';

    });

	$(window).trigger(migescoEvents.forexJsIsCompleteLoad);
});

$(window).on('loadingIsCompleted', function () {
    $('.tol-subject-selector.deposit').click();

    //Navigation on My Profile page
    $('#tol-myProfile-layout-1 ul.nav.tabs.nav-wizard li').click(function () {
        $('#tol-myProfile-layout-1 ul.nav.tabs.nav-wizard li').removeClass('active');
        $(this).addClass('active');
    });

    $('#tol-myProfile-layout-1 ul.nav.tabs.nav-wizard li').first().click();

    $('.tooltipQuickSignUp').hover(function () {
        $('.quickSignUpDeclaration').show();
    });
    $('.tooltipQuickSignUp').mouseleave(function () {
        $('.quickSignUpDeclaration').hide();
    });

    $("#notLoggedLinkAboutUs").click(function () {
        $('#tol-login-popup').modal('show');
    });

    function ShowQuickSignUpForm() {
        $('#tol-login-popup').modal('show');
    }

    $('a.investAmountDDLButton').click(function (e) {
        e.preventDefault();
        return false;
    });

    $('.tol-assets-table td').on("click", function () {
        if (widgets.game == 'forex' && $.cookie('widgetSession') == null && widgets.isLogged() == false) {
            apprise(widgetMessage.SaveTradeOrder_NotLoggedIn);
        };
    });

    if (typeof widgetsUser != 'undefined') {
        $('.input-currency-symbol').text(widgetsUser.currencySymbol);
    };

    if ($.cookie('current_language') !== 'en') {
        $(".dataLinkChange").attr({
            "data-link": "/" + $.cookie('current_language') + "/terms-and-conditions"
        });
    };

    removeULRparam('game');
    removeULRparam('view');
});

$(window).on('contactUsSubmited', function (response) {
	if ($(".appriseOuter").length > 0) {
        return;
    }
    apprise(widgetMessage.ContactUs_ThankYou);
    $('.wrapper-contacts').hide();
});

/* My profile - hide msg for successful profile update */
$(window).on('userDetailsUpdated', function () {
    function hideSuccessMsg() {
        $("#tol-myProfile-layout-1 .myprofile-successmsg").html('');
    }
    setTimeout(hideSuccessMsg, 2000);
});
/* /My profile - hide msg for successful profile update */

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

$(window).on("tournamentIsUserRegister", function () {
    $('.tournamentsBackImage').hide();
    $('.tournamentsBack').hide();
    $('.tournamentsBody').show();

    setTimeout(function () {
        $('.tableBodyDiv').mCustomScrollbar("destroy");
        $(".tableBodyDiv").mCustomScrollbar({
            advanced: {
                updateOnContentResize: true
            },
            scrollButtons: {
                enable: false
            }

        });
    }, 10);
});
$('.detailsButton').live('click', function (e) {
    e.preventDefault();
    window.location.href = './tournaments?tournamentID=' + $(this).attr("data-tournamendetailsid");
    return false;
});

$('.tournamentsBtn').live('click', function (e) {
    $(window).trigger('clickTournamentDetails');
});

if ($.cookie('widgetSession') == null) {
    $('.joinNowButton, [data-widget=tournamentRegistration][data-type=register]').live('click', onLoginBtnClick);
};

$('.tournamentDetailsTraderoom').click(function () {
    window.location.href = './tournaments?tournamentID=current';
});

function onLoginBtnClick(e) {
    e.preventDefault();
    // custom script to execute when the user in to logged and clicks to join a tournament
    apprise(widgetMessage.notLoggedJoinTournament);
};

$(window).on('clickTournamentJoinButton', function () {
    if (widgetsUser.isReal === false) {
        window.location.pathname = "./cashier";
    }
});

$(window).on('tournamentLeaderboardIsReady', function () {
    $('.introTitleText').html('Be part of "' + $('[data-widget=tournamentLeaderboard][data-type=name]').text() + '"');
    if (window.location.pathname === "/ar/tournaments") {
        $('.tableDiv').css('direction', 'ltr');
    };
    if (window.location.pathname === "/ar/tournaments" || window.location.pathname === "/tournaments") {
        $('.tableDiv').css('width', 'auto');
    };
    $('.ui-widget-header').css('width', 'auto');
});

$(window).on('socialLoadingComplete', function () {
    $('.flag').each(function (i) {
        var img = $(this).attr('class').toLowerCase();
        img = img.split(" ")[1];
        $(this).css('background', 'url(./wp-content/plugins/sitepress-multilingual-cms/res/flags/' + img + '.png)');
    });
});

function removeULRparam(param) {
    var haveParam = false;

    if (window.location.href.split(param)[1] != undefined) {
        var paramsURL = (window.location.href.split("?")[1]).split("&");
        var stack = '';

        for (i = 0; i < paramsURL.length; i++) {
            if (paramsURL[i].indexOf(param + '=') > -1) {
                haveParam = true;
            } else {
                if (stack == '')
                    stack += '?' + paramsURL[i]
                else
                    stack += '&' + paramsURL[i]
            }
        };

        stack = window.location.href.split("?")[0] + stack;

        if (haveParam) {
            if ($.browser.msie && $.browser.version < 10)
                window.location.href = stack;
            else
                window.history.pushState("string", "Title", stack);
        };
    };
};
