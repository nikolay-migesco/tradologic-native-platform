<?php //if(!TolWidgets::isUserLogged()):?>
<div class="resetpassword-wrap" style="display:none;">
    <div class="box_title"><span>Reset password</span></div>
    <div class="reset_text">If you can’t remember your account password, please type below your email and press
        <br/>“Reset password”. <br/>
        <br/>You will need to use the same email you registered with us when you opened your account. <br/>
        <br/>For further help please contact <br/><a href="/en/contact-us">customer support.</a></div>
    <div id="resetpassword_form">
        <form>
            <div class="form_input" style="height:68px !important;">
                <input class="widgetPlaceholder input" type="text" placeholder="E-mail" data-widget="resetPassword"
                       data-type="email" data-regexpmessage="E-mail are incorrect" data-isrequired="true"/>
            </div>
            <div><input type="button" data-widget="resetPassword" data-type="submit" value="Reset password"/></div>
            <div class="reset-link"><a href="#login">Sign in MyMigesco</a></div>
        </form>
    </div>
    <div id="close_popup3"><span class="resetpassword-close"></span></div>
</div>
<?php //endif;?>
