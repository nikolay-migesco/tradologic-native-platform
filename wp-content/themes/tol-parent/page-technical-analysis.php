<?php
/**
 * Template Name: MTE technical analysis page
 */
    $userId = json_decode(stripslashes($_COOKIE['widgetUserData']), true)["id"];
    $redirectDestination = $userId ? '/traderoom' : '';
    
    if (json_decode(stripslashes($_COOKIE['widgetUserData']), true)["Package"] < 1200) {
        $url         =  "//{$_SERVER['HTTP_HOST']}";
        $escaped_url = htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' );
        header("Location: " . $escaped_url . $redirectDestination);
    }

    get_header();
    switch (ICL_LANGUAGE_CODE) {

    case "de":
        $language = "de";
        break;
    case "ja":
        $language = "jp";
        break;
    case "ar":
        $language = "ar";
        break;
    case "zh-hans":
        $language = "cn";
        break;
    case "it":
        $language = "it";
        break;
    case "ru":
        $language = "ru";
        break;
    default:
        $language = "en";
}
 ?>


	<div id="content" role="main">
        <div class="market-tools-header">
            <div class="market-tools-header-title" data-translate="technicalAnalysis"></div>
        </div>
        <div class="edu-intro-text" data-translate="edu-intro-text-video"></div>
        <div class="container">

        <div class="pageHeadTitle">
            <?php require_once(ABSPATH.'wp-content/themes/tol-parent/layouts/breadcrumb.php' ); ?>
        </div>

            <script src='//www.mte-media.com/admin2/js/mte.js'></script>
            <script>
                
                
            function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for(var i=0; i<ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1);
                if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
            }
            return "";
            }
    
    
             if(getCookie('widgetSession')){
             $(' .container').append("<iframe allowfullscreen frameborder=0 src='//www.mte-media.com/admin2/frames/widgets_index.php?ref=9ff909f&set=Default&widg=tecan&lng=en' width='100%' height='800px' ></iframe>");       
                
          }else {
             $('.container').append("<iframe allowfullscreen frameborder=0 src='//www.mte-media.com/admin2/frames/widgets_index.php?ref=9ff909f&set=Default&widg=tecan&lng=en&demo=1' width='100%' height='800px' ></iframe>");
            }   

            </script>

        </div>

	</div><!-- #content -->	




<?php
    get_footer();
 ?>