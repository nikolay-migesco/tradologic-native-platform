<div class="tolUserIsLogged" style="display:none">
    <style type="text/css">
        <?php if(prior_lang()=='ru') { ?>
        .demo_text {
            position: absolute;
            left: -51px;
            top: 0;
            font-size: 14px;
        }

        <?php } else { ?>
        .demo_text {
            position: absolute;
            left: -84px;
            top: 0;
            font-size: 14px;
        }

        <?php } ?>
        a.header_cash.demo_deposit {
            white-space: nowrap !important;
            width: auto !important;
            padding-left: 5px !important;
            padding-right: 5px !important;
            color: white !important;
        }

        a.header_button {
            color: #fff
        }
    </style>
    <div style="vertical-align:top; text-align:right; padding-top:2px;float:right;margin-top:-20px;" id="auth_info">
        <div class="header_personal" style="position:relative;">
            <div style="display:inline-block; width:169px; text-align:left;">
                <img src="/wp-content/themes/tol-parent/images/xp-theme/user-data/cashe.png" align="center"/> <span
                        class="balance-amount"></span> <span class="currency-symbol"></span>
            </div>
            <div style="display:inline-block; width:86px; text-align:left;">
                <img src="/wp-content/themes/tol-parent/images/xp-theme/user-data/person.png" align="center"/> <span
                        class="first-name"></span>
            </div>
            <?php if (prior_lang() == 'ru') { ?>
                <div class="op-demo-account demo_text" style="display:none;">Демо-счет:</div>
            <?php } else { ?>
                <div class="op-demo-account demo_text" style="display:none;">Demo account:</div>
            <?php } ?>
        </div>
    </div>
</div>
<div style="float:right;<?php if (!isUserLogged()): ?>margin-top:-10px<?php endif; ?>" id="auth_button">
    <div class="tolUserIsLogged" style="display:none">
        <?php if (prior_lang() == 'ru') { ?>
            <a href="/cashier?cpage=deposit" class="header_button header_cash deposit-BTN op-real-account"
               data-translate="deposit-BTN" style="display:none;"></a>
            <a href="/cashier?cpage=deposit" class="header_button header_cash deposit-BTN op-demo-account demo_deposit"
               style="display:none;">Торговать на реальные деньги</a>
        <?php } else { ?>
            <a href="/en/cashier?cpage=deposit" class="header_button header_cash deposit-BTN op-real-account"
               data-translate="deposit-BTN" style="display:none;"></a>
            <a href="/en/cashier?cpage=deposit"
               class="header_button header_cash deposit-BTN op-demo-account demo_deposit" style="display:none;">Start
                real money trading</a>
        <?php } ?>
        <a href="#" class="header_button header_logout logout-action tol-userbar-logout-user"
           data-translate="userDataLogout"></a>
    </div>
    <div class="tolUserIsNotLogged" style="display:none">
        <div class="registration-BTN" style="display:inline-block;">
            <a class="header_button header_cash" href="#register"
               data-translate="UserData_userBlock_OpenAccountNow"></a>
        </div>
        <div class="login-BNT" style="display:inline-block;">
            <a class="header_button header_logout" href="#login" data-translate="UserData_userBlock_Login"></a>
        </div>
    </div>
</div>
