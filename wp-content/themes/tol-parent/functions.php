<?php

include_once(dirname(__FILE__) . '/funtcions_prior.php');

if (isset($_GET['mte'])) {
    global $sitepress;

    $lang = isset($_COOKIE['_icl_current_language']) ? $_COOKIE['_icl_current_language'] : $sitepress->get_default_language();

    $newUrl = $sitepress->get_default_language() == $lang ? $_GET['mte'] : '/' . $lang . '/' . $_GET['mte'];
    header("Location: $newUrl");
    exit;
}

/*
 * Include externel classes
 */
require_once get_template_directory() . '/lessc.inc.php';
require_once get_template_directory() . '/wp-less.php';
require_once get_template_directory() . '/classes/TolLang.php';
require_once get_template_directory() . '/classes/WPPSModule.php';
require_once get_template_directory() . '/classes/TolRestrict.php';
require_once get_template_directory() . '/widgets/shortcodes.inc.php';

if(WP_DEBUG_BAR){
    require_once get_template_directory() . '/devbar/init.php';
    $devbar = new TolThemedev();
}

/**
 * Add link to favicon.
 */
function xptheme_favicon_link() {
    echo '<link rel="shortcut icon" type="image/x-icon" href="' . site_url() . FAVICON_PATH . '" />' . "\n";
}
add_action('wp_head', 'xptheme_favicon_link');

if (!function_exists('parent_theme_setup')) {
    function parent_theme_setup() {
        register_nav_menu('primary', 'Primary Menu');
        register_nav_menu('primary_logged', 'Primary Menu (Logged-in)');
        register_nav_menu('footer_hor', 'Footer Horizontal Menu');
		register_nav_menu('footer_hor_bottom', 'Footer Horizontal Bottom Menu');
    }
    add_action('after_setup_theme', 'parent_theme_setup');
}

if (!function_exists('xptheme_widgets_init')) {
    /**
     * Registers our widget areas
     */
    function xptheme_widgets_init() {
       /*
        * Include Sidebars per Homepage widgets init
        */

        if(file_exists(ABSPATH.'/wp-content/themes/tol-parent/layouts/sidebars/'.HOMEPAGE_LAYOUT.'-sidebars.php')){
            require_once( 'layouts/sidebars/'.HOMEPAGE_LAYOUT.'-sidebars.php' );
        }else{
            require_once( 'layouts/sidebars/default-sidebars.php' );
       }
       
       register_sidebar(array(
            'name' => 'Footer javascript codes',
            'id' => 'tol-jscodes',
            'description' => 'Footer javascript codes',
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));
    }

    add_action('widgets_init', 'xptheme_widgets_init');
}
/*
 * Add slug class to the body
 */
function add_slug_body_class($classes) {
    global $post;
    if (isset($post)) {
        $classes[] = $post->post_type . '-' . $post->post_name;
    }
    return $classes;
}
add_filter('body_class', 'add_slug_body_class');

/*
 * Prepare array with configs
 */
function config_settings() {
    if(!$_COOKIE['current_language']) setcookie('current_language', TolLang::getCurrentIso());
    else $_COOKIE['current_language'] = TolLang::getCurrentIso();
    
    $data = array(
        'widgets_version' => (defined('WIDGETS_VERSION')) ? WIDGETS_VERSION : 'v6',
        'enablePraxis' => (defined('ENABLEPRAXIS')) ? ENABLEPRAXIS : 'false',
        'baseUrl' => (defined('BASE_URL')) ? BASE_URL : '',
        'libUrl' => (defined('LIB_URL')) ? LIB_URL : '',
        'loggerUrl' => (defined('LOGGER_URL')) ? LOGGER_URL : '',
        'webApiUrl' => (defined('REST_API')) ? REST_API : '',
        'restApiUrl' => (defined('REST_API')) ? REST_API : '',
        'feedApiUrl' => (defined('FEED_API')) ? FEED_API : '',
        'advfeedApiUrl' => (defined('ADV_FEED_API')) ? ADV_FEED_API : '',
        'loginSuccessUrl' => (defined('LOGIN_SUCCESS_URL')) ? LOGIN_SUCCESS_URL : '',
        'logoutSuccessUrl' => (defined('LOGOUT_SUCCESS_URL')) ? LOGOUT_SUCCESS_URL : '',
        'registrationSuccessUrl' => (defined('REGISTRATION_SUCCESS_URL')) ? REGISTRATION_SUCCESS_URL : '',
        'facebookAppID' => (defined('FACEBOOK_ID')) ? FACEBOOK_ID : '',
        'googleClientId' => (defined('GOOGLE_ID')) ? GOOGLE_ID : '',
        'qqId' => (defined('QQ_ID')) ? QQ_ID : '',
        'webId' => (defined('WEB_ID')) ? WEB_ID : '',
        'newbinary' => (defined('NEWBINARY')) ? NEWBINARY : false,
        'enable_portfolio' => (defined('ENABLE_PORTFOLIO')) ? ENABLE_PORTFOLIO : false,
        'enable_forex_social' => (defined('ENABLE_FOREX_SOCIAL')) ? ENABLE_FOREX_SOCIAL : false,
		'enable_mte' => (defined('ENABLE_MTE')) ? ENABLE_MTE : false,
        'enable_demo_flow' => (defined('ENABLE_DEMO_FLOW')) ? ENABLE_DEMO_FLOW : false,
        'lcid' => TolLang::getCurrentLCID(),
        'lang' => TolLang::getCurrentIso(),
        'languagesISO' => TolLang::getWidgetsLanguages(),
        'theme' => (defined('BOOTSTRAP_THEME')) ? BOOTSTRAP_THEME : '',
        'traderoom_color' => (defined('DEFAULT_TRADEROOM')) ? DEFAULT_TRADEROOM : '',
        'theme_color' => (defined('THEME_COLOR')) ? THEME_COLOR : '',
        'games' => array(
            'digital'=> (defined('ENABLE_DIGITAL')) ? ENABLE_DIGITAL : '',
            'turboBinary'=> (defined('ENABLE_TURBOBINARY')) ? ENABLE_TURBOBINARY : '',
            'touch'=> (defined('ENABLE_TOUCH')) ? ENABLE_TOUCH : '',
            'range'=> (defined('ENABLE_RANGE')) ? ENABLE_RANGE : '',
            'advanced'=> (defined('ENABLE_ADVANCED')) ? ENABLE_ADVANCED : '',
            'binaryExchange'=> (defined('ENABLE_BINARYEXCHANGE')) ? ENABLE_BINARYEXCHANGE : '',
			'simplex'=> (defined('ENABLE_SIMPLEX')) ? ENABLE_SIMPLEX : '',
			'forex'=> (defined('ENABLE_REALFOREX')) ? ENABLE_REALFOREX : '',
            'forexHA'=> (defined('ENABLE_FOREXHA')) ? ENABLE_FOREXHA : ''
        )
    );
    return $data;
}

const LOCAL_CONFIG = 'widget-config';
const JQUERY = 'jquery';
const DEPS_JQUERY = array(JQUERY);
function attachNativePageAssets(string $webPath, string $filePath)
{

    wp_deregister_script(JQUERY);
    wp_register_script(JQUERY, $webPath . '/javascript/jquery.min.js', false, NULL, true);
    wp_register_script('jquery-migrate', $webPath . '/javascript/jquery-migrate-1.2.1.min.js', false, NULL, true);

    wp_enqueue_script(JQUERY);
    wp_enqueue_script('jquery-migrate');

    wp_enqueue_script('jquery-ui', $webPath . '/scripts/jquery-ui.js', false, null, true);

    wp_register_script('apprise-js', $webPath . '/scripts/apprise.min.js', DEPS_JQUERY, null, true);
    wp_enqueue_script('apprise-js');

    $bootsrap_js = 'boostrap_js';
    wp_register_script('customSlider', $webPath . '/javascript/jquery.mCustomScrollbar.concat.min.js', $bootsrap_js, NULL, true);
    wp_enqueue_script('customSlider');

    wp_register_script('encrypt-js', $webPath . '/scripts/encrypt.js', DEPS_JQUERY, null, true);
    wp_enqueue_script('encrypt-js');

    wp_register_script('cryptojs', $webPath . '/scripts/cryptojs.js', DEPS_JQUERY, null, true);
    wp_enqueue_script('cryptojs');

    wp_register_script('jquery-cookie', $webPath . '/javascript/jquery.cookie.js', DEPS_JQUERY, NULL, true);
    wp_enqueue_script('jquery-cookie');

    $isProductKeyExists = array_key_exists('product',$_GET);
    $product = '';
    $isNewBinary = false;
    if($isProductKeyExists){
        $product = $_GET['product'];
        $isNewBinary = $product == 'newbinary';
    }
    $widgetConfig = 'widget-config';
    if(!$isNewBinary){
        wp_register_script($widgetConfig, $webPath . '/javascript/widgets-config-forex.js', DEPS_JQUERY, md5_file($filePath . '/javascript/widgets-config-forex.js'), true);
    }else{
        wp_register_script($widgetConfig, $webPath . '/javascript/widgets-config.js', DEPS_JQUERY, md5_file($filePath . '/javascript/widgets-config.js'), true);
    }
    wp_enqueue_script($widgetConfig);

    wp_localize_script($widgetConfig, 'widgetsSettings', config_settings());

    $widgetsLang = 'widgets-lang';
    wp_register_script($widgetsLang, BASE_URL . WIDGETS_VERSION . '/lang/en.js', $widgetConfig, NULL, true);
    wp_enqueue_script($widgetsLang);

    wp_register_script('en-js', BASE_URL . WIDGETS_VERSION . '/wp-lang/en.js', $widgetConfig, NULL, true);
    wp_enqueue_script('en-js');

    wp_register_script('notify_js', '/wp-content/themes/tol-parent/scripts/notify.min.js', DEPS_JQUERY, null, true);
    wp_enqueue_script('notify_js');

    wp_register_style('education', $webPath . '/styles/layouts/custom.css', array(), '1.0.6');

    wp_register_script('select2', '/wp-content/themes/tol-parent/scripts/select2.min.js', DEPS_JQUERY, null, true);
    wp_enqueue_script('select2');

    $child_lang = 'child-en-js';
    if (ICL_LANGUAGE_CODE != '') {
        $language = ICL_LANGUAGE_CODE;

        wp_deregister_script($widgetsLang);
        wp_register_script($widgetsLang, BASE_URL . WIDGETS_VERSION . '/lang/' . $language . '.js', false, NULL, true);

        $child_lang = "child-$language-js";
        wp_register_script($child_lang, '/wp-content/themes/tol-child/languages/' . $language . '.js', false, md5_file($filePath . '/../tol-child/languages/' . $language . '.js'), true);
        wp_enqueue_script($child_lang, $widgetConfig);
    }
    $deps_child = array($child_lang);

    if (MIN_WIDGETS) {
        wp_register_script('min_widgets', BASE_URL . WIDGETS_VERSION . '/widgets.min.js', $deps_child, NULL, true);
        wp_enqueue_script('min_widgets');
    } else {
        wp_register_script('engine', BASE_URL . WIDGETS_VERSION . '/lib/engine.js', $deps_child, NULL, true);
        wp_enqueue_script('engine');
    }

    wp_register_script($bootsrap_js, $webPath . '/bootstrap/js/bootstrap.min.js', DEPS_JQUERY, null, true);
    wp_enqueue_script($bootsrap_js);

    wp_register_script('cashier-js', $webPath . '/scripts/cashier.js', DEPS_JQUERY, md5_file($filePath . '/scripts/cashier.js'), true);
    wp_enqueue_script('cashier-js');

    wp_register_script('tol-common', $webPath . '/scripts/common.js', DEPS_JQUERY, md5_file($filePath . '/scripts/common.js'), true);
    wp_enqueue_script('tol-common');

    wp_enqueue_style('jquery-ui', $webPath . "/styles/jquery-ui.css", array(), NULL, false);
    wp_enqueue_style('datepicker', $webPath . "/styles/ui.datepicker.css", array(), NULL, false);
    wp_enqueue_style('apprise', $webPath . "/styles/apprise.min.css", array(), NULL, false);
    wp_enqueue_style('bootstrap-ng', $webPath . "/bootstrap/css/bootstrap.min.css", array(), NULL, false);
    wp_enqueue_style('theme', $webPath . "/bootstrap/themes/theme-" . BOOTSTRAP_THEME . ".less", array(), NULL, false);

    $isConfigKeyExists = array_key_exists('config',$_GET);
    $config = '';
    if($isConfigKeyExists){
        $config = $_GET['config'];
    }
    $shouldChoose = (TRADEROOM_LAYOUT == 'traderoom-all') && ($isProductKeyExists || $isConfigKeyExists);

    if(!$shouldChoose){
        $product = getDefaultTraderoom();
    }
    if($config == 'simplex'){
        wp_enqueue_style('traderoom-25', $webPath . "/styles/layouts/traderoom-25.less", array(), NULL, false);
    }
    switch ($product){
        case 'simplex';
        case 'crypto':
            wp_enqueue_style('traderoom-25', $webPath . "/styles/layouts/traderoom-25.less", array(), NULL, false);
            break;
        case 'forex':
            wp_enqueue_style('traderoom-24', $webPath . "/styles/layouts/traderoom-24.less", array(), NULL, false);
            break;
        case 'simplexm';
        case 'cryptom':
            wp_enqueue_style('traderoom-25m', $webPath . "/styles/layouts/traderoom-25m.less", array(), NULL, false);
            break;
        case 'realm':
            wp_enqueue_style('traderoom-24m', $webPath . "/styles/layouts/traderoom-24m.less", array(), NULL, false);
            break;
        case 'realil':
            wp_enqueue_style('traderoom-24m', $webPath . "/styles/layouts/traderoom-24il.less", array(), NULL, false);
            break;
        case 'exchange':
            wp_enqueue_style('traderoom-20', $webPath . "/styles/layouts/traderoom-20.less", array(), NULL, false);
            break;
        case 'newbinary' :
            wp_enqueue_style('custom-select', "/wp-content/themes/tol-parent/styles/select2.min.css", array(), NULL, false);
            wp_enqueue_style('traderoom-28', $webPath . "/styles/layouts/traderoom-28.less", array(), NULL, false);
            break;
        case 'newbinaryil':
            wp_enqueue_style('custom-select', "/wp-content/themes/tol-parent/styles/select2.min.css", array(), NULL, false);
            wp_enqueue_style('traderoom-28', $webPath . "/styles/layouts/traderoom-28il.less", array(), NULL, false);
            break;
    }

    wp_enqueue_style('homepage', $webPath . "/styles/layouts/" . HOMEPAGE_LAYOUT . ".less", array(), NULL, false);
    wp_enqueue_style(FOOTER_LAYOUT, $webPath . "/styles/layouts/" . FOOTER_LAYOUT . ".css", array(), md5_file($filePath . "/styles/layouts/" . FOOTER_LAYOUT . ".css"), false);
    wp_enqueue_style("past-expiries", $webPath . "/styles/layouts/past-expiries.css", array(), md5_file($filePath . "/styles/layouts/past-expiries.css"), false);
    wp_enqueue_style("my-trades", $webPath . "/styles/layouts/my-trades.css", array(), md5_file($filePath . "/styles/layouts/my-trades.css"), false);
    wp_enqueue_style('leaderboard-layout', $webPath . "/styles/layouts/" . LEADERBOARD_LAYOUT . ".css", array(), md5_file($filePath . "/styles/layouts/" . LEADERBOARD_LAYOUT . ".css"), false);
    wp_enqueue_style("contact-us", $webPath . "/styles/layouts/contact-us.css", array(), NULL, false);
    wp_register_style("my-profile", $webPath . "/styles/layouts/" . MYPROFILE_LAYOUT . ".less", array(), NULL, false);
    wp_enqueue_style("asset-list", $webPath . "/styles/layouts/asset-list.less", array(), NULL, false);
    wp_register_style('cashier-styles', $webPath . "/styles/layouts/" . CASHIER_LAYOUT . ".css", array(), md5_file($filePath . "/styles/layouts/" . CASHIER_LAYOUT . ".css"), false);
    wp_enqueue_style('customScroll', $webPath . "/styles/jquery.mCustomScrollbar.css", array(), NULL, false);
    wp_enqueue_style(REGISTRATION_LAYOUT, $webPath . "/styles/layouts/" . REGISTRATION_LAYOUT . ".less", array(), NULL, false);
    if(!$isNewBinary){
        wp_register_script('jscrollpane-js', '/wp-content/themes/tol-parent/scripts/jquery.jscrollpane.min.js', false, null, true);
        wp_register_script('mousewheel-js', '/wp-content/themes/tol-parent/scripts/jquery.mousewheel.js', false, null, true);
        wp_register_script('forex', $webPath . "/scripts/forex.js", DEPS_JQUERY, md5_file($filePath . "/scripts/forex.js"), true);
        wp_enqueue_script('forex');
    }

    if (BOOTSTRAP_THEME != 'white-forex' && BOOTSTRAP_THEME != 'dark-forex') {
        wp_enqueue_style('style', $webPath . "/styles/style.css", array(), NULL, false);
    } else {
        add_filter('body_class', 'custom_class');
        function custom_class($classes)
        {
            if (isset($_COOKIE['theme'])) {
                if ($_COOKIE['theme'] == 'light' && strpos($_SERVER['REQUEST_URI'], 'traderoom') > -1) {
                    $classes[] = 'white-theme';
                };
            } else {
                if (DEFAULT_TRADEROOM == 'light') {
                    $classes[] = 'white-theme';
                    setcookie('theme', 'light', 0, '/');
                } else {
                    $classes[] = 'dark-theme';
                    setcookie('theme', 'dark', 0, '/');
                };
            };
            return $classes;
        }

        ;
    };
}

function attachMigescoPageAssets(ResourceLoader $loader)
{
    wp_enqueue_script('clientJs','/wp-content/themes/tol-parent/javascript/client.min.js', null,null);

    wp_deregister_script(JQUERY);
    $loader->registerScript(JQUERY,'/javascript/jquery.min.js');

    $loader->registerScript('jquery-migrate','/javascript/jquery-migrate-1.2.1.min.js',DEPS_JQUERY);

    wp_enqueue_script('jquery-ui', 'https://code.jquery.com/ui/1.12.1/jquery-ui.js', false, null, true);
    $loader->registerScript('apprise-js','/scripts/apprise.min.js',DEPS_JQUERY);
    $loader->registerScript('boostrap_js','/bootstrap/js/bootstrap.min.js',DEPS_JQUERY);
    $loader->registerScript('customSlider','/javascript/jquery.mCustomScrollbar.concat.min.js',array('boostrap_js'));

    if (strpos($_SERVER['REQUEST_URI'], '/cashier?cpage=deposit') === false) {
        $loader->registerScriptWithFileTime('cashier-js','/scripts/cashier.js',DEPS_JQUERY);
    }

    $loader->registerScript('encrypt-js','/scripts/encrypt.js',DEPS_JQUERY);

    $loader->registerScript('cryptojs','/scripts/cryptojs.js',DEPS_JQUERY);

    if (isset($_COOKIE['view'])) {
        $traderoom_view = $_COOKIE['view'];
    } else if (defined('TRADEROOM_VIEW')) {
        $traderoom_view = TRADEROOM_VIEW;
    } else {
        $traderoom_view = 'single';
    }

    if (TRADEROOM_LAYOUT != 'traderoom-28') {
        $loader->registerScriptWithFileTime('traderoom-single-js','/scripts/' . $traderoom_view . '.js',DEPS_JQUERY);
    };

    $loader->registerScript('jquery-cookie','/scripts/jquery.cookie.js',DEPS_JQUERY);
    $loader->registerScript('jquery-timer','/scripts/jquery.timer.js',array('jquery-cookie'));
    $loader->registerScript('jquery-countdown','/scripts/countdown.js',array('jquery-timer'));
    $loader->registerScript('jquery-vendor','/javascript/vendor.js',DEPS_JQUERY);
    $loader->registerScript('jquery-parallax','/javascript/jquery.parallax.min.js',DEPS_JQUERY);
    $loader->registerScript('jquery-wow','/javascript/wow.min.js',DEPS_JQUERY);


    #todo to pass data to widgets config
    if (defined('ISFOREX') && ISFOREX == true) {
        $loader->registerScriptWithFileTime(LOCAL_CONFIG,'/javascript/widgets-config-forex.js',DEPS_JQUERY);
    } else {
        $loader->registerScriptWithFileTime(LOCAL_CONFIG,'/javascript/widgets-config.js',DEPS_JQUERY);
    }

    $loader->registerScriptWithFileTime('tol-common','/scripts/common.js',DEPS_JQUERY);

    wp_localize_script(LOCAL_CONFIG, 'widgetsSettings', config_settings());    // widgets settings

    wp_register_script('widgets-lang', BASE_URL . WIDGETS_VERSION . '/lang/en.js', array(LOCAL_CONFIG), NULL, true);
    wp_enqueue_script('widgets-lang');

    wp_register_script('en-js', BASE_URL . WIDGETS_VERSION . '/wp-lang/en.js', array(LOCAL_CONFIG), NULL, true);
    wp_enqueue_script('en-js');

    $language = ICL_LANGUAGE_CODE != '' ? ICL_LANGUAGE_CODE : 'en';
    $childLanguage ='child-language-js' ;
    wp_register_script($childLanguage,
        '/wp-content/themes/tol-child/languages/' . $language . '.js',
        array(LOCAL_CONFIG),
        filemtime(get_template_directory() . '/../tol-child/languages/' . $language . '.js'), true);
    wp_enqueue_script($childLanguage );

    if (MIN_WIDGETS) {

        if (strpos($_SERVER['REQUEST_URI'], '/cashier?cpage=summaryReport') === false &&
            strpos($_SERVER['REQUEST_URI'], '/cashier?cpage=historyDeposit') === false) {

            wp_register_script('engine', BASE_URL . WIDGETS_VERSION . '/widgets.min.js', array($childLanguage), NULL, true);
            wp_enqueue_script('engine');
        }
    } else {
        wp_register_script('engine', BASE_URL . WIDGETS_VERSION . '/lib/engine.js', array($childLanguage), NULL, true);
        wp_enqueue_script('engine');
    }

    $loader->enqueueStyle('jquery-ui',"/styles/jquery-ui.css");
    $loader->enqueueStyle('datepicker',"/styles/ui.datepicker.css");
    $loader->enqueueStyle('apprise',"/styles/apprise.min.css");
    $loader->enqueueStyle('bootstrap-ng',"/bootstrap/css/bootstrap.min.css");
    $loader->enqueueStyle('theme',"/bootstrap/themes/theme-dark-forex.less");

    $loader->enqueueStyle('homepage-38',"/styles/layouts/homepage-38.less");
    $loader->enqueueStyleWithFileTime('header-migesco.css','/styles/layouts/header-migesco.css');
    $loader->enqueueStyleWithFileTime('footer-migesco.css','/styles/layouts/footer-migesco.css');
    $loader->enqueueStyleWithFileTime('past-expiries-3','/styles/layouts/past-expiries-3.css');
    $loader->enqueueStyleWithFileTime('my-trades-1','/styles/layouts/my-trades-1.css');
    $loader->enqueueStyleWithFileTime('my-statistic-1.css','/styles/layouts/my-statistic-1.css');
    $loader->enqueueStyleWithFileTime('leaderboard-layout','/styles/layouts/leader-board-migesco.css');
    $loader->enqueueStyleWithFileTime('contact-us-1.css','/styles/layouts/contact-us-1.css');
    $loader->enqueueStyleWithFileTime('my-profile-migesco.less','/styles/layouts/my-profile-migesco.less');
    $loader->enqueueStyleWithFileTime('asset-list-1.less','/styles/layouts/asset-list-1.less');
    $loader->enqueueStyleWithFileTime('cashier-styles','/styles/layouts/cashier-67.css');
    $loader->enqueueStyleWithFileTime('cashier-styles-67','/styles/layouts/cashier.css');
    $loader->enqueueStyleWithFileTime('customScroll','/styles/jquery.mCustomScrollbar.css');

    wp_register_script('notify_js', '/wp-content/themes/tol-parent/scripts/notify.min.js', DEPS_JQUERY, null, true);
    wp_enqueue_script('notify_js');


    wp_register_script('customSelect', '/wp-content/themes/tol-parent/scripts/jquery.customSelect.min.js', DEPS_JQUERY, NULL, false);
    /*wp_register_script( 'cashier', '/wp-content/themes/tol-child/js/cashier.js', array('jquery'), NULL );*/
    wp_register_script('cashier', '/wp-content/themes/tol-parent/scripts/cashier.js', DEPS_JQUERY, NULL);

    // TODO: Exclude if it's not a forex
    if (defined('ISFOREX') && ISFOREX == true) {
        wp_register_script('jscrollpane-js', '/wp-content/themes/tol-parent/scripts/jquery.jscrollpane.min.js', false, null, true);
        wp_register_script('mousewheel-js', '/wp-content/themes/tol-parent/scripts/jquery.mousewheel.js', false, null, true);
        $loader->registerScriptWithFileTime('forex','/scripts/forex.js',DEPS_JQUERY);
        wp_enqueue_script('forex');
    }
}

/*
 * Load all scripts from parent theme
 */
function my_scripts_method()
{
    wp_enqueue_script('configuration.js', '/configuration.js', null, filemtime('configuration.js'), false);
    $webPath = get_template_directory_uri();
    $filePath = get_template_directory();
    $isLoadingMigescoLayout = isMigescoLayout();
    if (!$isLoadingMigescoLayout) {
        attachNativePageAssets($webPath, $filePath);
    }
    if ($isLoadingMigescoLayout) {
        $loader = new ResourceLoader($filePath,$webPath);
        attachMigescoPageAssets($loader);
    }
}
add_action('wp_enqueue_scripts', 'my_scripts_method');

/**
 * Load child less and css files
 */
function child_scripts_styles() {
    wp_enqueue_style('style-child', "/themes/tol-child/styles/style" . ((BOOTSTRAP_THEME=='white-forex' || BOOTSTRAP_THEME=='dark-forex') ? (BOOTSTRAP_THEME=='white-forex' ? "-white" : "-dark") : "") .".less", array(), NULL, false);

    $isLoadingMigescoLayout = isMigescoLayout();
    if (!$isLoadingMigescoLayout) {
        wp_register_script('childcommon_js', '/wp-content/themes/tol-child/javascript/common.js', false, md5_file(get_template_directory() . '/../tol-child/javascript/common.js'), true);
        wp_enqueue_script('childcommon_js', 'jquery');
    }

    if ($isLoadingMigescoLayout) {
        wp_register_script('childcommon_js', '/wp-content/themes/tol-child/javascript/common1-migesco.js', false, md5_file(get_template_directory() . '/../tol-child/javascript/common1-migesco.js'), true);
        wp_enqueue_script('childcommon_js', 'jquery');

        wp_register_script('childcommon_js2', '/wp-content/themes/tol-child/javascript/common2-migesco.js', 'childcommon_js', md5_file(get_template_directory() . '/../tol-child/javascript/common2-migesco.js'), true);
        wp_enqueue_script('childcommon_js2', 'childcommon_js');
    }
}
add_action('wp_enqueue_scripts', 'child_scripts_styles');


function updateSessionFromGet() {
    $pattern = '/(http[s]?:\/\/)?(www[a-z0-9]?\.)?/i';
    $replace = "";
    $domain = preg_replace('/\/$/', '', preg_replace($pattern, $replace, $_SERVER['SERVER_NAME']));

    if (isset($_GET['hash'])) {
        setcookie('widgetSession', $_GET['hash'], 0, '/');
        $_COOKIE['widgetSession'] = $_GET['hash'];
    };
    return true;
}

if ($_SERVER['SCRIPT_FILENAME'] == __FILE__)
    die('Access denied.');

if ($_SERVER['SCRIPT_FILENAME'] == __FILE__)
    die('Access denied.');

TolRestrict::getInstance();
require_once get_template_directory() . '/classes/customizer.php';
/*
 * Make all links relative and don't do anything if:
 *  - In feed
 *  - In sitemap by WordPress SEO plugin
 */
function relative_url() {
    if (is_feed() || get_query_var('sitemap'))
        return;
    $filters = array(
        'post_link', // Normal post link
        'post_type_link', // Custom post type link
        'page_link', // Page link
        'attachment_link', // Attachment link
        'get_shortlink', // Shortlink
        'post_type_archive_link', // Post type archive link
        'get_pagenum_link', // Paginated link
        'get_comments_pagenum_link', // Paginated comment link
        'term_link', // Term link, including category, tag
        'search_link', // Search link
        'day_link', // Date archive link
        'month_link',
        'year_link',
        // site location
        'option_siteurl',
        'blog_option_siteurl',
        'admin_url',
        'includes_url',
        'site_url',
        'site_option_siteurl',
        'network_home_url',
        'network_site_url',
        '',
        // debug only filters
        'get_the_author_url',
        'get_comment_link',
        'wp_get_attachment_image_src',
        'wp_get_attachment_thumb_url',
        'wp_get_attachment_url',
        'wp_login_url',
        'wp_logout_url',
        'wp_lostpassword_url',
        'get_stylesheet_uri',
        'get_locale_stylesheet_uri',
        'style_loader_src', // plugin styles url
        'get_theme_root_uri'
        //'home_url'
    );

    $filters = array();
    foreach ($filters as $filter) {
        add_filter($filter, 'wp_make_link_relative');
    }
    home_url($path = '', $scheme = null);
    add_filter('script_loader_src', 'scrpts_src_url');

    function scrpts_src_url($url) {
        return str_replace(site_url(), '', $url);
    }
}
add_action('template_redirect', 'relative_url');

add_action( 'init', 'add_slider' );
function add_slider() {
    $labels = array(
        'name' => _x('slider', 'post type general name'),
        'singular_name' => _x('slider', 'post type singular name'),
        'add_new' => _x('Add slider', 'slider'),
        'add_new_item' => __('Add slider'),
        'edit_item' => __('Edit slider'),
        'new_item' => __('New slider '),
        'all_items' => __('All slider'),
        'view_item' => __('View slider'),
        'search_items' => __('Search slider'),
        'not_found' => __('No slider found'),
        'not_found_in_trash' => __('No slider found in Trash'),
        'parent_item_colon' => '',
        'menu_name' => 'slider'
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('thumbnail', 'custom-fields', 'title', 'editor', 'author')
    );

    register_post_type('slider', $args);
}

add_action('init', 'add_FAQ');

function add_FAQ() {

    $labels = array(
        'name' => _x('FAQ', 'post type general name'),
        'singular_name' => _x('FAQ', 'post type singular name'),
        'add_new' => _x('Add FAQ', 'FAQ'),
        'add_new_item' => __('Add FAQ'),
        'edit_item' => __('Edit FAQ'),
        'new_item' => __('New FAQ '),
        'all_items' => __('All FAQ'),
        'view_item' => __('View FAQ'),
        'search_items' => __('Search FAQ'),
        'not_found' => __('No FAQ found'),
        'not_found_in_trash' => __('No FAQ found in Trash'),
        'parent_item_colon' => '',
        'menu_name' => 'FAQ'
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('thumbnail', 'custom-fields', 'title', 'editor', 'author')
    );

    register_post_type('FAQ', $args);
}

// Remove version of WP
function wpbeginner_remove_version() { 
    return ''; 
}
add_filter('the_generator', 'wpbeginner_remove_version');

// Remove Version of WPML plugin
remove_action( 'wp_head', array($sitepress, 'meta_generator_tag' ) );

/*

Detect requested URL and analyze it 

*/
$cashierRequestUrl = $_SERVER["REQUEST_URI"];

$cashierRequestUrlSeparated = explode("/", $cashierRequestUrl);

$requestLangKey =  $cashierRequestUrlSeparated[1];

$langList = TolLang::getWidgetsLanguages();

$langList = array_flip($langList);

if(array_key_exists($requestLangKey, $langList)){

    $cashierRequestLang = $langList[$cashierRequestUrlSeparated[1]];
    
    $cashierRequestUrlSeparated[1] = $cashierRequestLang;

    if(isset($sitepress)){
        global $sitepress;
        $sitepress->get_default_language();
        $deflang = $sitepress->get_default_language();       
    }else{
        $deflang = '';
    }
    

    if($cashierRequestLang == $deflang){
        
        unset($cashierRequestUrlSeparated[1]);
    }
    
    $cashierRequestUrl = implode("/",$cashierRequestUrlSeparated);
    
    if(trim($cashierRequestUrl))
        header("Location: ".$cashierRequestUrl);
    else
        header("Location: /");
    die();
   
}

function isUserLogged()
{
    $isLogged = false;
    if (isset($_COOKIE['widgetSession'])) {
        $isLogged = true; // standard widget-based login
    }

    return $isLogged;
}
