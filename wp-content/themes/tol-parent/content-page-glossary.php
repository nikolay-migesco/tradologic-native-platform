<?php
/**
 *
 * Template Name: Wilkins Glossary page
 *
 */
 get_header();
?>

<style>
.accordion-toggle[aria-expanded="false"] .box-icon .expand {
  display: block;
}
.accordion-toggle[aria-expanded="true"] .box-icon .expand {
  display: none;
}
.accordion-toggle[aria-expanded="false"] .box-icon .contract {
  display: none;
}
.accordion-toggle[aria-expanded="true"] .box-icon .contract {
  display: block;
}
.cms-page-content strong {
    font-weight: 800;
}
.cms-page-content h2:not(:first-child) {
    margin-top: 60px;
}
.breadcrumb {
    padding: 0;
    margin-bottom: 20px;
	line-height: 52px;
    list-style: none;
    background-color: transparent;
    max-width: 1150px;
    margin: auto;
}
.breadcrumbs__link {
    color: #fff !important;
}
.breadcrumbs__divider {
    margin: 0 10px;
    color: #f9a134;
    font-size: 13px;
    font-weight: 900;
}
.breadcrumb>.active {
    color: #fff;
}
.breadcrumb a{
   color: #fff !important;
}
.breadcrumbs__wrapper {
    height: 52px;
    background: #1a1d1f;
}
.page-content {
    padding-top: 140px;
    padding-bottom: 0;
    background-color: #111214;
}
.container.cfd-container {
	 padding-top: 30px;
}
.page-content.page-content-home {
    padding-top: 0
}

@media screen and (max-width: 767px) {
    .page-content {
        padding-top:50px;
        padding-bottom: 0
    }
}
.cfd-container {
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
}
.cfd-container, .cfd__main-text {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
}
.container.cfd-containe {
    height: 100%;
    margin: 0 auto;
    max-width: 1150px;
    position: relative;
    width: 100%;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
	padding: 0;
}
.sidebar {
    width: 230px;
	margin-right: 20px;
}
.sidebar__menu {
    background: #000;
    margin-bottom: 30px;
}
.sidebar__menu__header {
    display: block;
    height: 38px;
    line-height: 38px;
    padding-left: 30px;
    background: #1a1c1f;
    color: #f9a134;
    font-size: 15px;
    font-weight: 500;
	margin: 0;
}
.sidebar__menu__item {
    height: 38px;
    color: #fff;
    border-top: 1px solid #383d43;
    -webkit-transition: .1s;
    transition: .1s;
}
.sidebar__menu__item.active {
	color: #f9a134;
}
.sidebar__menu a {
    display: block;
    height: 38px;
    padding-left: 30px;
    line-height: 38px;
    font-size: 14px;
    text-decoration: none;
    color: inherit;
    font-family: Open Sans,Arial,Tahoma,sans-serif;
}
.cfd__main-text {
    font-family: Open Sans,Arial,Tahoma,sans-serif;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    max-width: 900px;
    width: 900px;
    padding: 6px;
    -webkit-box-flex: 1;
    -ms-flex-positive: 1;
    flex-grow: 1;
    -ms-flex-negative: 1;
    flex-shrink: 1;
}
h1 {
    font-size: 35px;
	margin: 0;
    margin-bottom: 30px;
    font-family: Open Sans,Arial,Tahoma,sans-serif;
    color: #f9a134;
	font-weight: 400;
}
h2 {
    font-size: 25px;
    color: #fff;
    margin: 0 0 25px 0;
	font-family: Open Sans,Arial,Tahoma,sans-serif;
	font-weight: 400;
}
.cms-page-content li, .cms-page-content p {
    margin-bottom: 15px;
    line-height: 22px;
    font-size: 15px;
    color: #fff;
	font-family: Open Sans,Arial,Tahoma,sans-serif;
}
ul, ol {
    list-style: none;
    padding-left: 0;
}
.sidebar__menu__item:hover {
    color: #f9a134;
}
.glossary-wrapper {
	background: #fff;
    border-radius: 6px;
    max-width: 880px;
    overflow: hidden;
    color: #000;
}
.glossary-wrapper .nav-tabs.clearfix {
	-webkit-box-orient: horizontal;
    -ms-flex-flow: row wrap;
    flex-flow: row wrap;
	display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-direction: normal;
	border: 0;
}
.glossary-wrapper .nav-tabs.clearfix li {
    width: 54px;
    cursor: pointer;
    font-size: 13px;
    font-weight: 700;
    color: #444;
    border-right: 1px solid #d3d3d3;
    border-bottom: 1px solid #d3d3d3;
    background: #f6f6f6;
	line-height: 42px;
}
.nav-tabs>li>a {
    margin-right: 0;
    line-height: 15px;
    border: 0;
    border-radius: 0;
    color: #444;
	padding: 12px 23px;
}
.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
    box-shadow: none;
    border: 0;
}
.glossary-wrapper .nav-tabs.clearfix li.active {
	background-color: #fff;
    border-bottom: none;
}
.glossary-wrapper ul  {
	    -webkit-box-orient: vertical;
    -ms-flex-direction: column;
    flex-direction: column;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    padding:  0;
	margin: 0;
}

.collapse-sub-menu {
	    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    max-width: 890px;
    min-height: 49px;
    padding: 0 14px 0 0;
    font-size: 14px;
    line-height: 22px;
    font-family: Noto Sans,serif;
    font-weight: 700;
    background-color: #282d30;
    border: none;
    color: #fff;
}
.box-icon {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    min-width: 50px;
    border-right: 1px solid #e5e5e5;
    min-height: 49px;
	float: left;
}
.collapse-sub-menu .panel-title {
    font-size: 24px;
    padding-top: 14px;
    padding-left: 10px;
}
.tab-content {
    padding: 0;
}
.panel-body {
    padding: 15px 15px 15px 60px;
    background-color: #191d20;
    color: #fff;
}
.panel.panel-default {
	border: 1px solid #d3d3d3;
    border-radius: 5px;
    max-width: 850px;
    width: 97%;
	min-height: 49px;
    list-style: none;
    margin-bottom: 7px;
    cursor: pointer;
	margin: auto;
}
.panel-default > .panel-heading {
    border-bottom-color: transparent;
    border-top-color: transparent;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    max-width: 890px;
    min-height: 49px;
    padding: 0 14px 0 0;
    font-size: 14px;
    line-height: 22px;
    font-family: Noto Sans,serif;
    font-weight: 700;
    background-color: #282d30 !important;
    border: none;
    color: #fff;
}
.panel-default h4 {
    display: inline-block;
    padding-left: 20px;
    padding-right: 5px;
    font-family: Open Sans,Arial,Tahoma,sans-serif;
    font-size: 14px;
    text-transform: uppercase;
	line-height: 49px;
	color: #fff;
	width: calc(100% - 50px);
}
.accordion-toggle {
	width: 100%;
}
.fa-icon {
    display: inline-block;
    fill: #fff;
	margin: auto;
}
@media(max-width: 1024px){
	 .breadcrumb {
		padding: 0 3%;
	}
}
@media screen and (max-width: 767px){
	.cfd-container {
		-webkit-box-orient: vertical;
		-webkit-box-direction: reverse;
		-ms-flex-direction: column-reverse;
		flex-direction: column-reverse;
		-ms-flex-line-pack: center;
		align-content: center;
		padding-left: 15px;
	}
	.sidebar {
		width: 100%;
		min-width: 230px;
	}
	.cfd__main-text {
		width: 100%;
	}
}
.cms-page-content .panel-body p {
	margin-bottom: 0px;
}
</style>

<div id="content" role="main">
	<div class="page-content" style="background-color:#111214;">
		<div class="breadcrumbs__wrapper">
			<?php require_once(ABSPATH.'wp-content/themes/tol-parent/layouts/breadcrumb.php' ); ?>
		</div>
		<div class="container cfd-container">
			<div class="sidebar">
   <div class="sidebar__menu">
      <p class="sidebar__menu__header">Education</p>
      <ul>
         <li class="sidebar__menu__item active"><a href="/education/glossary" class="">Glossary</a></li>
      </ul>
   </div>
</div>
<div class="cfd__main-text">
<h1>Glossary</h1>
<div class="cms-page-content">
   <div class="glossary-wrapper">
      <ul class="nav-tabs clearfix">
         <li class="active"><a href="#glossary-a" data-toggle="tab" aria-expanded="true">A</a></li>
         <li><a href="#glossary-b" data-toggle="tab">B</a></li>
         <li class=""><a href="#glossary-c" data-toggle="tab" aria-expanded="false">C</a></li>
         <li><a href="#glossary-d" data-toggle="tab">D</a></li>
		 <li><a href="#glossary-e" data-toggle="tab">E</a></li>
         <li><a href="#glossary-f" data-toggle="tab">F</a></li>
         <li><a href="#glossary-i" data-toggle="tab">I</a></li>
		 <li><a href="#glossary-k" data-toggle="tab">K</a></li>
         <li><a href="#glossary-l" data-toggle="tab">L</a></li>
         <li><a href="#glossary-n" data-toggle="tab">N</a></li>
         <li><a href="#glossary-o" data-toggle="tab">O</a></li>
         <li><a href="#glossary-p" data-toggle="tab">P</a></li>
         <li><a href="#glossary-r" data-toggle="tab">R</a></li>
         <li><a href="#glossary-s" data-toggle="tab">S</a></li>
         <li><a href="#glossary-t" data-toggle="tab">T</a></li>
         <li><a href="#glossary-u" data-toggle="tab">U</a></li>
      </ul>
      <div class="clearfix"></div>
      <div class="tab-content">
         <div class="tab-pane fade active in" id="glossary-a">
           <div id="accordion-A">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-A" href="#collapseA1" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">Above option</h4>
                        </a>
                     </div>
                     <div id="collapseA1" class="panel-collapse collapse">
                        <div class="panel-body"><p>A CFD contract is said to be "Above Option" if at expiry the market price ruling the underlying asset on that contract at expiry is actually higher than the "Strike Price".</p>
                        </div>
                     </div>
                  </div>
				  
				  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-A" href="#collapseA2" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">Ask price</h4>
                        </a>
                     </div>
                     <div id="collapseA2" class="panel-collapse collapse" >
                        <div class="panel-body"><p>The price that a seller is asks for an asset. In other words, the market price.</p>
                        </div>
                     </div>
                  </div>
				  
				  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-A" href="#collapseA3" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">Asset</h4>
                        </a>
                     </div>
                     <div id="collapseA3" class="panel-collapse collapse" >
                        <div class="panel-body"><p>Any single Commodity, Currency, Stock, or Index appearing on the Migesco trading platform.</p>
                        </div>
                     </div>
                  </div>
				  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-A" href="#collapseA4" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">AT THE MONEY</h4>
                        </a>
                     </div>
                     <div id="collapseA4" class="panel-collapse collapse">
                        <div class="panel-body"><p>If at the moment when a contract expires the market price for any given "Asset" is the same as the ‘’Strike Price’’ then that contract is "at-the-money".</p>
                        </div>
                     </div>
                  </div>

               </div>
         </div>
         <div class="tab-pane fade" id="glossary-b">
           <div id="accordion-B">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-B" href="#collapseB1" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">BELOW OPTION</h4>
                        </a>
                     </div>
                     <div id="collapseB1" class="panel-collapse collapse">
                        <div class="panel-body"><p>A CFD contract is said to be "Below Option" if at expiry the market price ruling the underlying asset on that contract is actually less than the "Strike Price"</p>
                        </div>
                     </div>
                  </div>
				  
				  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-B" href="#collapseB2" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">BID PRICE</h4>
                        </a>
                     </div>
                     <div id="collapseB2" class="panel-collapse collapse" >
                        <div class="panel-body"><p>The price at which you can sell an asset or security, i.e., the price at which someone is willing to buy an asset.</p>
                        </div>
                     </div>
                  </div>
				  
				  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-B" href="#collapseB3" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">BONUS</h4>
                        </a>
                     </div>
                     <div id="collapseB3" class="panel-collapse collapse" >
                        <div class="panel-body"><p>A "Bonus" is an amount of money which Migesco credits to your trading account. The bonus enables you to make a larger volume of CFD contracts. Bonuses are awarded on the condition that you commit yourself to make a certain amount of trade volume.</p>
                        </div>
                     </div>
                  </div>
               </div>
         </div>
         <div class="tab-pane fade" id="glossary-c">
           <div id="accordion-C">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-C" href="#collapseC1" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">CFD CONTRACT</h4>
                        </a>
                     </div>
                     <div id="collapseC1" class="panel-collapse collapse">
                        <div class="panel-body"><p>A CFD contract is an ‘all or nothing’ contract by which an investor will either receive a profit or no profit at all. A CFD contract is a time limited contract for which there are only two outcomes at expiry time: (A) the market price for an asset is higher than the ‘’Strike Price’’, or (B) the market price for an asset is lower than the "Strike Price".</p>
                        </div>
                     </div>
                  </div>
				  
				  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-C" href="#collapseC2" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">COMMODITIES</h4>
                        </a>
                     </div>
                     <div id="collapseC2" class="panel-collapse collapse" >
                        <div class="panel-body"><p>Commodities are raw or naturally occurring materials. Such as for example: Gold, Oil, Copper, Coffee and Soya Bean.</p>
                        </div>
                     </div>
                  </div>
				  
				  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-C" href="#collapseC3" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">CURRENCY</h4>
                        </a>
                     </div>
                     <div id="collapseC3" class="panel-collapse collapse" >
                        <div class="panel-body"><p>A "currency" is a national system of money or monetary units in circulation as banknotes and coins. Currencies are organized into pairs for CFDs. E.g., EUR/USD; GBP/USD</p>
                        </div>
                     </div>
                  </div>
				  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-C" href="#collapseC4" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">CURRENT PRICE</h4>
                        </a>
                     </div>
                     <div id="collapseC4" class="panel-collapse collapse" >
                        <div class="panel-body"><p>The "Current Price" is the asset price on the actual stocks, currencies and commodity markets at any given moment.</p>
                        </div>
                     </div>
                  </div>
               </div>
         </div>
         <div class="tab-pane fade" id="glossary-d">
           <div id="accordion-D">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-D" href="#collapseD1" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">DECLARATION OF DEPOSIT (DOD)</h4>
                        </a>
                     </div>
                     <div id="collapseD1" class="panel-collapse collapse">
                        <div class="panel-body"><p>When you deposit money into your Migesco Account, we will send you a Declaration of Deposit/s (DOD). The DOD is your declaration that you agree to the deposits showing on your Migesco account. It is also a declaration that you agree to Migesco’s Anti-Money Laundering policy. You are required to print the DOD, sign it, scan or photo it, and then send the signed copy to Migesco’s Compliance Department.</p>
                        </div>
                     </div>
                  </div>
               </div>
         </div>
		 <div class="tab-pane fade" id="glossary-e">
		        <div id="accordion-E">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-E" href="#collapseE1" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">EXERCISE PRICE</h4>
                        </a>
                     </div>
                     <div id="collapseE1" class="panel-collapse collapse">
                        <div class="panel-body"><p>This is the price of the “Asset” or “Instrument” being traded when the contract was placed.</p>
                        </div>
                     </div>
                  </div>
				  
				  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-E" href="#collapseE2" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">EXPIRY PRICE</h4>
                        </a>
                     </div>
                     <div id="collapseE2" class="panel-collapse collapse" >
                        <div class="panel-body"><p>The "Expiry Price" is the market price of any asset at the moment a CFD contract expires.</p>
                        </div>
                     </div>
                  </div>
				  
				  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-E" href="#collapseE3" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">EXPIRY TIME</h4>
                        </a>
                     </div>
                     <div id="collapseE3" class="panel-collapse collapse" >
                        <div class="panel-body"><p>CFD contracts have a fixed life-span which is chosen by you at the moment the contract is made. The expiry time is expressed as a date/time.</p>
                        </div>
                     </div>
                  </div>
               </div>
		 </div>
         <div class="tab-pane fade" id="glossary-f">
       <div id="accordion-F">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-F" href="#collapseF1" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">FIXED RETURN</h4>
                        </a>
                     </div>
                     <div id="collapseF1" class="panel-collapse collapse">
                        <div class="panel-body"><p>The actual percentage of return on a CFD contract fixed at the moment the contract is made.</p>
                        </div>
                     </div>
                  </div>
				  
				  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-F" href="#collapseF2" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">FUNDAMENTAL ANALYSIS</h4>
                        </a>
                     </div>
                     <div id="collapseF2" class="panel-collapse collapse" >
                        <div class="panel-body"><p>‘’Fundamental Analysis’’ denotes analysis of current economic and political data affecting the markets. Fundamental Analysis is the evaluation of an asset by studying all of the factors affecting its price, e.g. consumer confidence, financial policy, global economic conditions, company reports, etc.</p>
                        </div>
                     </div>
                  </div>

               </div>
         </div>
         <div class="tab-pane fade" id="glossary-i">
        <div id="accordion-I">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-I" href="#collapseI1" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">INDICES</h4>
                        </a>
                     </div>
                     <div id="collapseI1" class="panel-collapse collapse">
                        <div class="panel-body"><p>‘’Indices’’ (singular ‘Index’) are combinations of several stocks or other investment vehicles whose combined value is expressed as a total and that is measured against a base value from a specific date. Examples of ‘’Indices’’ are: US Dow Jones Index; UK FTSE; Germany’s DAX.</p>
                        </div>
                     </div>
                  </div>
				  
				  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-I" href="#collapseI2" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">IN-THE-MONEY</h4>
                        </a>
                     </div>
                     <div id="collapseI2" class="panel-collapse collapse" >
                        <div class="panel-body"><p>A CFD contract is ‘’in-the-money’’ if, at the moment the contract expires, the predicted direction for the price movement on the underlying asset (regardless of whether it was above or below, i.e., a ‘’Call’’ or a ‘’Put’’) turns out to be correct.</p>
                        </div>
                     </div>
                  </div>
					<div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-I" href="#collapseI3" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">INSTRUMENT</h4>
                        </a>
                     </div>
                     <div id="collapseI3" class="panel-collapse collapse" >
                        <div class="panel-body"><p>"Instrument" is a word often used inter-changeably. It is chiefly used to refer to contractual documents. Sometimes, it refers to the "Asset" or underlying index, stock, commodity, or currency on which a CFD contract is made.</p>
                        </div>
                     </div>
                  </div>
				  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-I" href="#collapseI4" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">INVESTMENT</h4>
                        </a>
                     </div>
                     <div id="collapseI4" class="panel-collapse collapse" >
                        <div class="panel-body"><p>The amount of money assigned to a CFD contract.</p>
                        </div>
                     </div>
                  </div>

               </div>
         </div>
		 <div class="tab-pane fade" id="glossary-k">
           <div id="accordion-K">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-K" href="#collapseK1" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">KYC</h4>
                        </a>
                     </div>
                     <div id="collapseK1" class="panel-collapse collapse">
                        <div class="panel-body"><p>‘Know Your Client’ is a procedure we follow in order to protect our clients interests. Learn how to fully validate and verify your trading account.</p>
                        </div>
                     </div>
                  </div>
               </div>
         </div>
         <div class="tab-pane fade" id="glossary-l">
                   <div id="accordion-L">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-L" href="#collapseL1" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">LADDER OPTION</h4>
                        </a>
                     </div>
                     <div id="collapseL1" class="panel-collapse collapse">
                        <div class="panel-body"><p>A kind of CFD contract which allows you to earn a profit when the asset's market price reaches one or more of five levels above or below the ‘’Strike Price’’ any time before the contract expires.</p>
                        </div>
                     </div>
                  </div>
				  
				  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-L" href="#collapseL2" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">LONG TERM OPTION</h4>
                        </a>
                     </div>
                     <div id="collapseL2" class="panel-collapse collapse" >
                        <div class="panel-body"><p>A CFD contract with a time-frame of from a minimum of seven days up to a maximum of ten months.</p>
                        </div>
                     </div>
                  </div>
               </div>
         </div>
         <div class="tab-pane fade" id="glossary-n">
            <div id="accordion-N">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-N" href="#collapseN1" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">NET EXPOSURE</h4>
                        </a>
                     </div>
                     <div id="collapseN1" class="panel-collapse collapse">
                        <div class="panel-body"><p>Your ‘’Net Exposure’’ is the total value of all the CFD contracts you have already placed which have not expired.</p>
                        </div>
                     </div>
                  </div>
               </div>
         </div>
         <div class="tab-pane fade" id="glossary-o">
          <div id="accordion-O">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-O" href="#collapseO1" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">ONE TOUCH</h4>
                        </a>
                     </div>
                     <div id="collapseO1" class="panel-collapse collapse">
                        <div class="panel-body"><p>‘’One Touch’’ CFDs are contracts where the trader stays in-the-money if the market price for an asset touches a predefined price at any moment before the contract expires.</p>
                        </div>
                     </div>
                  </div>
				  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-O" href="#collapseO2" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">OUT-OF-THE-MONEY</h4>
                        </a>
                     </div>
                     <div id="collapseO2" class="panel-collapse collapse">
                        <div class="panel-body"><p>An investor is said to be "out-of-the-money" any time their prediction on the direction of the asset price is incorrect. No payout will be awarded to the trader.</p>
                        </div>
                     </div>
                  </div>
               </div>
         </div>
         <div class="tab-pane fade" id="glossary-p">
       <div id="accordion-P">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-P" href="#collapseP1" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">PAYOUT</h4>
                        </a>
                     </div>
                     <div id="collapseP1" class="panel-collapse collapse">
                        <div class="panel-body"><p>The percentage or amount of money you are entitled to receive at moment the CFD contract expires.</p>
                        </div>
                     </div>
                  </div>
				  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-P" href="#collapseP2" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">PAIRS</h4>
                        </a>
                     </div>
                     <div id="collapseP2" class="panel-collapse collapse">
                        <div class="panel-body"><p>An exciting trading type where you put two different assets against each other to determine which will perform better.</p>
                        </div>
                     </div>
                  </div>
				  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-P" href="#collapseP3" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">PIP</h4>
                        </a>
                     </div>
                     <div id="collapseP3" class="panel-collapse collapse">
                        <div class="panel-body"><p>A Pip or “Point In Percentage” is any of up to four “decimal points” to the right of the decimal for prices. E.g., 0001 = 1 pip; 0010 = 10 pips; 1.9875 = 1 plus nine thousand eight hundred and seventy-five pips.</p>
                        </div>
                     </div>
                  </div>
				  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-P" href="#collapseP4" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">PROFIT</h4>
                        </a>
                     </div>
                     <div id="collapseP4" class="panel-collapse collapse">
                        <div class="panel-body"><p>“Profit” is the amount you win when a CFD contract is “ïn-the-money”.</p>
                        </div>
                     </div>
                  </div>
               </div>
         </div>
         <div class="tab-pane fade" id="glossary-r">
           <div id="accordion-R">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-R" href="#collapseR1" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">RANGE INSTRUMENT</h4>
                        </a>
                     </div>
                     <div id="collapseR1" class="panel-collapse collapse">
                        <div class="panel-body"><p>An instrument that allows the customer to choose whether the value of an underlying asset at expiry time for the CFD contract will fall inside or outside a specified range. The range is demarcated by lower and higher target price limits.</p>
                        </div>
                     </div>
                  </div>
				  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-R" href="#collapseR2" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">RETURN</h4>
                        </a>
                     </div>
                     <div id="collapseR2" class="panel-collapse collapse">
                        <div class="panel-body"><p>The "Return" is the sum total of profit plus the amount you originally assign to a CFD contract. You receive your ‘’Return’’ when a CFD contract expires in-the-money at the expiry time.</p>
                        </div>
                     </div>
                  </div>
               </div>
         </div>
         <div class="tab-pane fade" id="glossary-s">
            <div id="accordion-S">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-S" href="#collapseS1" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">SIGNALS</h4>
                        </a>
                     </div>
                     <div id="collapseS1" class="panel-collapse collapse">
                        <div class="panel-body"><p>‘’Signals’’ are formulated by expert financial analysts, and these are intended to alert you to exciting investment opportunities. Signals are sent to you as email bulletins. Signals are intended as a guide only however and are not necessarily 100% accurate.</p>
                        </div>
                     </div>
                  </div>
				  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-S" href="#collapseS2" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">SPOT FOLLOW</h4>
                        </a>
                     </div>
                     <div id="collapseS2" class="panel-collapse collapse">
                        <div class="panel-body"><p>Migescos online trading platform has a ‘’Spot Follow’’ tool which enables you to watch and follow the performance of other investors on the Migesco website platform, giving you the ability to view the degree of successful or unsuccessful price direction positions.</p>
                        </div>
                     </div>
                  </div>
				  	  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-S" href="#collapseS3" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">STAKE</h4>
                        </a>
                     </div>
                     <div id="collapseS3" class="panel-collapse collapse">
                        <div class="panel-body"><p>“Stake” is the amount of money invested in a CFD contract.</p>
                        </div>
                     </div>
                  </div>
				  	  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-S" href="#collapseS4" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">STRIKE PRICE</h4>
                        </a>
                     </div>
                     <div id="collapseS4" class="panel-collapse collapse">
                        <div class="panel-body"><p>The Strike Price, also commonly referred to as the “Exercise Price”, is the value of the original “Asset” during when the initial contract was agreed upon</p>
                        </div>
                     </div>
                  </div>
               </div>
         </div>
         <div class="tab-pane fade" id="glossary-t">
			<div id="accordion-T">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-T" href="#collapseT1" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">TECHNICAL ANALYSIS</h4>
                        </a>
                     </div>
                     <div id="collapseT1" class="panel-collapse collapse">
                        <div class="panel-body"><p>Technical Analysis is concerned with the mathematical study of the price of an asset. It focusses on data and information concerned with: supply and demand factors; as well as factors relating to trend lines, patterns and other movements on charts and graphs for individual assets.</p>
                        </div>
                     </div>
                  </div>
               </div>
         </div>
         <div class="tab-pane fade" id="glossary-u">
			<div id="accordion-U">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-U" href="#collapseU1" aria-expanded="false">
						<div class="box-icon"><svg  version="1.1" role="presentation" width="12.571428571428571" height="16" viewBox="0 0 1408 1792" class="icon-plus fa-icon box-icon-open">
						<path class="contract" d="M1408 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path>
						<path class="expand" d="M1408 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"></path></svg></div>
                           <h4 class="panel-title">UNDERLYING ASSET</h4>
                        </a>
                     </div>
                     <div id="collapseU1" class="panel-collapse collapse">
                        <div class="panel-body"><p>Any individual commodity; currency; stock; or index that is the basis for a CFD contract.</p>
                        </div>
                     </div>
                  </div>
               </div>
         </div>
         <p></p>
      </div>
   </div>
</div>
		</div>
	</div>
</div>
<!-- #content -->

<?php get_footer(); ?>
