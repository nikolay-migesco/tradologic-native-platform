<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Handles restrict pages/posts to plugin settings and user profile meta fields
 * @package TradologicPlugin
 */
class TolRestrict extends WPPSModule {

    const REQUIRED_CAPABILITY = 'edit_others_pages';
    const RESTRICT_PAGE_META = '_TOLP_restrictpage';
    const RESTRICT_PAGE_META_REDIRECT = '_TOLP_restrictpage_redirect';

    /* Path for client redirect if page is restricted for those client */
    const RESTRICTED_ALL = '';
    const RESTRICTED_LOGGED = '';
    const RESTRICTED_NOTLOGGED = '';

    protected static $META_VALUES = array(
        0 => 'Show to all users',
        1 => 'Logged in users only',
        2 => 'Not logged in user only',
    );
    protected static $META_FORWARDS = array(
        0 => self::RESTRICTED_ALL,
        1 => self::RESTRICTED_LOGGED,
        2 => self::RESTRICTED_NOTLOGGED,
    );

    /*
     * General methods
     */

    /**
     * Constructor
     * @mvc Controller
     */
    protected function __construct() {
        $this->registerHookCallbacks();
    }

    /**
     * Register callbacks for actions and filters
     * @mvc Controller
     */
    public function registerHookCallbacks() {
        add_action('add_meta_boxes', __CLASS__ . '::addCustomBox');
        add_action('save_post', __CLASS__ . '::saveCustomBoxData');
    }

    /**
     * Prepares site to use the plugin during activation
     * @mvc Controller
     * @param bool $networkWide
     */
    public function activate($networkWide) {
        
    }

    /**
     * Rolls back activation procedures when de-activating the plugin
     * @mvc Controller
     */
    public function deactivate() {
        
    }

    /**
     * Initializes variables
     * @mvc Controller
     */
    public function init() {
        if (did_action('init') !== 1)
            return;
        // do stuff on init
    }

    /**
     * Executes the logic of upgrading from specific older versions of the plugin to the current version
     * @mvc Model
     * @param string $dbVersion
     */
    public function upgrade($dbVersion = 0) {
        /*
          if( version_compare( $dbVersion, 'x.y.z', '<' ) )
          {
          // Do stuff
          }
         */
    }

    /**
     * Checks that the object is in a correct state
     * @mvc Model
     * @param string $property An individual property to check, or 'all' to check all of them
     * @return bool
     */
    protected function isValid($property = 'all') {
        // Note: __set() calls validateSettings(), so settings are never invalid

        return true;
    }

    /*
     * Plugin Settings
     */
    /* Adds a box to the main column on the Post and Page edit screens */

    public static function addCustomBox() {
        if (did_action('add_meta_boxes') !== 1)
            return;

        $screens = array('post', 'page');
        foreach ($screens as $screen) {
            add_meta_box(
                    'tolrestrict_custombox', 'Restricted Access (Tradologic context)', __CLASS__ . '::markupCustomBox', $screen, 'normal'
            );
        }
    }

    /*
     * Creates the markup for the box content
     * @mvc Controller
     */

    public static function markupCustomBox($post) {
        if (current_user_can(self::REQUIRED_CAPABILITY)) {
            $view[self::RESTRICT_PAGE_META] = (int) get_post_meta($post->ID, self::RESTRICT_PAGE_META, true);
            $view[self::RESTRICT_PAGE_META_REDIRECT] = get_post_meta($post->ID, self::RESTRICT_PAGE_META_REDIRECT, true);
            // prepare html for wp-admin
            echo '<label for="'.self::RESTRICT_PAGE_META.'">
                    <select id="'.self::RESTRICT_PAGE_META.'" name="'.self::RESTRICT_PAGE_META.'">';
                        foreach (self::$META_VALUES as $key => $value) {
                            echo '<option value="'.$key.'"'.( $view[self::RESTRICT_PAGE_META] == $key ? 'selected' : '' ).'>'.$value.'</option>';
                        }
            echo '  </select> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    '.esc_url( home_url( '/' ) ).'<input id="'.self::RESTRICT_PAGE_META_REDIRECT.'" name="'.self::RESTRICT_PAGE_META_REDIRECT.'" type="text" value ="'.$view[self::RESTRICT_PAGE_META_REDIRECT].'" />
                </label>';
        } else {
            echo 'Your user cannot modify this setting.';
        }
    }

    /*
     * When the post is saved, saves our custom data
     * @mvc Controller
     */

    public static function saveCustomBoxData($post_id) {
        if (did_action('save_post') !== 1)
            return;

        // check for post_ID as this action is also called on Autodraft creation in brand new posts, i.e. on opening "New post/page"
        if (!isset($_POST['post_ID']))
            return;

        //if saving in a custom table, get post_ID
        $post_ID = $_POST['post_ID'];
        
        // save custom fields
        $mydata = (int) $_POST[self::RESTRICT_PAGE_META];
        add_post_meta($post_ID, self::RESTRICT_PAGE_META, $mydata, true) OR update_post_meta($post_ID, self::RESTRICT_PAGE_META, $mydata);
        
        $mydata = $_POST[self::RESTRICT_PAGE_META_REDIRECT];
        add_post_meta($post_ID, self::RESTRICT_PAGE_META_REDIRECT, $mydata, true) OR update_post_meta($post_ID, self::RESTRICT_PAGE_META_REDIRECT, $mydata);
    }

    /*
     * When the post is saved, saves our custom data
     * @mvc Controller
     */

    public static function checkRestrictedPage() {
        global $post;

        $restrictedMeta = (int) @get_post_meta($post->ID, self::RESTRICT_PAGE_META, true);
        $restrictedMetaRedirect = get_post_meta($post->ID, self::RESTRICT_PAGE_META_REDIRECT, true);
        
        $redirectUrl = self::getUrl(self::$META_FORWARDS[$restrictedMeta]);

        if (!isset($_GET['hash']))
            echo "<script type='text/javascript' src='" . get_stylesheet_directory_uri() . "/javascript/restrict-pages.js'></script><script type='text/javascript'>tolWidgetsRedirect(" . $restrictedMeta . ",'" . (string) $redirectUrl.$restrictedMetaRedirect . "');</script>";
    }

    /**
     * Get home URL replacement (using WPML or default WP fallback)
     * @param string $path
     * @return string
     */
    public static function getUrl($path) {
        if (function_exists('icl_get_home_url')) {
            $result = icl_get_home_url();
            //$result .= $path;
        } else {
            $result = home_url($path);
        }
        return $result;
    }

}

// end WPPSSettings
