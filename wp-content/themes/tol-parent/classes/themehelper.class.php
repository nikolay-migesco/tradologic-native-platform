<?php

/**
 * Theme helper methods
 *
 * @package XPTheme
 * @author Tradologic
 */
class ThemeHelper
{

    /**
     * Return http link to language-specific resource folder
     * @return string
     */
    static function getLangFolderUrl()
    {
        $upload_dir = wp_upload_dir();
        $lang_dir = self::getCurrentIso();

        $my_theme = wp_get_theme();
        $my_theme_template = strtolower($my_theme->get('Name'));

        $result = $upload_dir['baseurl'] . '/' . $my_theme_template . '/languages/' . $lang_dir . '/';
        //$result = get_stylesheet_directory_uri().'/languages/'.$lang_dir.'/';

        return $result;
    }

    /**
     * Get current language
     */
    static function getCurrentIso()
    {
        return strtolower(defined('ICL_LANGUAGE_CODE') ? ICL_LANGUAGE_CODE : 'ru');
    }

    /**
     * Return http link to thems (current/child) folder
     * @return string
     */
    static function getCssFolderUrl()
    {
        return get_stylesheet_directory_uri();
    }

    /**
     * Get the current page template file name (w/o extension)
     * @return string
     */
    static function getPageTemplateFile()
    {
        $pathinfo = pathinfo(@get_page_template());
        return $pathinfo['filename'];
    }

    /**
     * Get the browser name, version, etc.
     * @return array
     */
    static function getBrowser()
    {
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $bname = 'unknown';
        $platform = 'unknown';
        $ub = 'unknown';
        $version = "unknown";

        //First get the platform?
        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'linux';
        } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        } elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }

        // Next get the name of the useragent yes seperately and for good reason
        if (strpos($u_agent, 'Trident/7.0; rv:11')) {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        } else if ((preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent))) {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        } elseif (preg_match('/Firefox/i', $u_agent)) {
            $bname = 'Mozilla Firefox';
            $ub = "Firefox";
        } elseif (preg_match('/Chrome/i', $u_agent)) {
            $bname = 'Google Chrome';
            $ub = "Chrome";
        } elseif (preg_match('/Safari/i', $u_agent)) {
            $bname = 'Apple Safari';
            $ub = "Safari";
        } elseif (preg_match('/Opera/i', $u_agent)) {
            $bname = 'Opera';
            $ub = "Opera";
        } elseif (preg_match('/Netscape/i', $u_agent)) {
            $bname = 'Netscape';
            $ub = "Netscape";
        }

        // finally get the correct version number
        $known = array('Version', $ub, 'other', 'rv');
        $pattern = '#(?<browser>' . join('|', $known) .
            ')[/ :]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
            // we have no matching number just continue
        }

        // see how many we have
        $i = count($matches['browser']);
        if ($i > 1) {
            //we will have two since we are not using 'other' argument yet
            //see if version is before or after the name
            if (strripos($u_agent, "Version") < strripos($u_agent, $ub)) {
                $version = $matches['version'][0];
            } else {
                $version = $matches['version'][1];
            }
        } else if ($i == 1) {
            $version = $matches['version'][0];
        }

        // get major version
        $version_major = explode('.', $version);
        $version_major = $version_major[0];

        // check if we have a number
        if ($version == null || $version == "") {
            $version = "?";
        }

        return array(
            'userAgent' => $u_agent,
            'name' => $bname,
            'version' => $version,
            'platform' => $platform,
            'pattern' => $pattern,
            'ub' => $ub,
            'version_major' => $version_major
        );
    }

    /**
     * Returns (wraps) the result of WP's  get_template_part($template_name, $part_name)  as result
     * @param type $template_name
     * @param type $part_name
     * @return type
     */
    static function loadTemplatePart($template_name, $part_name = null)
    {
        ob_start();
        get_template_part($template_name, $part_name);
        $var = ob_get_contents();
        ob_end_clean();
        return $var;
    }

    /**
     * Display error message for outdated plugin version
     */
    static function checkRequirements()
    {
        if (!self::pluginMetRequirements()) {
            echo '<div id="message" class="error">';
            echo '<h2>Missing or outdated Tradologic plugin</h2>';
            echo '<p><strong>
				Your Tradologic plugin seems to be missing or with outdated version. <br />Please address this important issue with your Tradologic contact.
				</strong></p>';
            echo '</div>';
        }
        return;
    }

    /**
     * Return boolean if requirements have been met (plugin version)
     * @return boolean
     */
    static function pluginMetRequirements()
    {
        $isTolPDefined = false;
        if (defined('TOLP_VERSION')) {
            $isTolPDefined = true;
        }
        $isRequirementDefined = false;
        if ($isTolPDefined) {
            $isRequirementDefined = defined('REQUIRED_TOLP_VERSION');
        }
        if (!$isRequirementDefined && $isTolPDefined) {
            define('REQUIRED_TOLP_VERSION', TOLP_VERSION);
        }
        $result = false;
        if ($isTolPDefined) {
            $result = version_compare(REQUIRED_TOLP_VERSION, TOLP_VERSION, '<=');;
        }
        return $result;
    }

    /**
     * Return current theme's version number
     */
    static function getThemeVersionFingerprint()
    {
        $theme = wp_get_theme();
        $result = '';

        // parent theme's version
        if (is_object($theme->parent()))
            $result .= $theme->parent()->Version . '-';

        // and current theme verion
        $result .= $theme->Version;

        return $result;
    }

}
