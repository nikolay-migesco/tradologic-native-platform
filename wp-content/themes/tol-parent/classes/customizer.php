<?php
/**
 * TOL-Parent Theme Customizer
 */
function quota_customize_register( $wp_customize ) {


    /** ===============
     * Extends controls class to add textarea
     */
    class quota_customize_textarea_control extends WP_Customize_Control {
        public $type = 'textarea';
        public function render_content() { ?>
            <label>
                <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
                <textarea rows="5" style="width:98%;" <?php $this->link(); ?>><?php echo esc_textarea( $this->value() ); ?></textarea>
            </label>
            <?php
        }
    }

    $wp_customize->get_section( 'colors' )->title = __( 'Color Settings', 'quota' );
    $wp_customize->get_section( 'colors' )->priority = 20;

    // Color customization options
    $colors = array();
    $colors[] = array(
        'slug'		=> 'quota_primary_color',
        'default'	=> '#0c6b9b',
        'label'		=> __( 'Primary Button Color', 'quota' ),
        'priority'	=> 20
    );
    $colors[] = array(
        'slug'		=> 'quota_primary_color_hover',
        'default'	=> '#034f75',
        'label'		=> __( 'Primary Button Color - Hover', 'quota' ),
        'priority'	=> 21
    );
    $colors[] = array(
        'slug'		=> 'quota_primary_text',
        'default'	=> '#FFFFFF',
        'label'		=> __( 'Primary Button Text', 'quota' ),
        'priority'	=> 22
    );
    $colors[] = array(
        'slug'		=> 'quota_primary_text_hover',
        'default'	=> '#FFFFFF',
        'label'		=> __( 'Primary Button Text - Hover', 'quota' ),
        'priority'	=> 23
    );


    $colors[] = array(
        'slug'		=> 'quota_secondary_color',
        'default'	=> '#75ba95',
        'label'		=> __( 'Secondary Button Color', 'quota' ),
        'priority'	=> 23
    );
    $colors[] = array(
        'slug'		=>'quota_secondary_color_hover',
        'default'	=> '#5e9678',
        'label'		=> __( 'Secondary Button Color - Hover', 'quota' ),
        'priority'	=> 24
    );
    $colors[] = array(
        'slug'		=> 'quota_text_color',
        'default'	=> '#404040',
        'label'		=> __( 'Main Text Color', 'quota' ),
        'priority'	=> 25
    );
    // Build settings from $colors array
    foreach( $colors as $color ) {

        // customizer settings
        $wp_customize->add_setting( $color['slug'], array(
            'default'		=> $color['default'],
            'type'			=> 'option',
            'capability'	=> 'edit_theme_options'
        ) );

        // customizer controls
        $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, $color['slug'], array(
            'label'		=> $color['label'],
            'section'	=> 'colors',
            'settings'	=> $color['slug'],
            'priority'	=> $color['priority']
        ) ) );
    }
}
add_action( 'customize_register', 'quota_customize_register' );

/** ===============
 * Add Quota theme Customizer color style options to <head>
 */
function quota_customizer_head_styles() {
    $quota_primary_color = get_option('quota_primary_color');
    $quota_primary_color_hover = get_option('quota_primary_color_hover');
    $quota_primary_text = get_option('quota_primary_text');
    $quota_primary_text_hover = get_option('quota_primary_text_hover');

    if($quota_primary_color === false){
        $quota_primary_color = '#3176FC';
    }
    if($quota_primary_color_hover === false){
        $quota_primary_color_hover = '#3493FF';
    }
    if($quota_primary_text === false){
        $quota_primary_text = '#FFF';
    }
    if($quota_primary_text_hover === false){
        $quota_primary_text_hover = '#000';
    }

    echo '<style type="text/css">';

    /**
     * Only add styles to the head of the document if the styles
     * have been changed from default.
     */

    // Primary design color
    echo ".btn-primary { background-color: {$quota_primary_color}; color: {$quota_primary_text}; }\n";
    echo ".btn-primary:hover { background-color: {$quota_primary_color_hover}; color: {$quota_primary_text_hover}; }";
    // Responsive
    // Primary design color
    if ('#0c6b9b' != $quota_primary_color ) :
        echo "@media all and (max-width: 768px) { .headline-area { background: {$quota_primary_color}; } .headline-text { color: #fff; } }";
    endif;

    echo '</style>';
}
add_action( 'wp_head', 'quota_customizer_head_styles' );


/** ===============
 * Add Customizer style to the <head> only on Customizer page
 */
function quota_customizer_styles() { ?>
    <style type="text/css">
        body { background: #fff; }
        #customize-controls #customize-theme-controls .description { display: block; color: #999; margin: 2px 0 15px; font-style: italic; }
        textarea, input, select, .customize-description { font-size: 12px !important; }
        .customize-control-title { font-size: 13px !important; margin: 10px 0 3px !important; }
        .customize-control label { font-size: 12px !important; }
        #customize-control-quota_read_more, #customize-control-quota_store_front_count { margin-bottom: 1.5em; }
        #customize-control-quota_store_front_count input { width: 50px; }
    </style>
<?php }
add_action( 'customize_controls_print_styles', 'quota_customizer_styles' );
