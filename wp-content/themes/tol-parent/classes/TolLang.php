<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TolLang
 *
 * @author hmitev
 */
class TolLang {

    protected static $iso_lcid = array(
        'en' => array('lcid' => 1033, 'iso2code' => 'en', 'language' => 'English', 'nativeLanguage' => 'English'),
        'ro' => array('lcid' => 1048, 'iso2code' => 'ro', 'language' => 'Romanian', 'nativeLanguage' => 'Română'),
        'de' => array('lcid' => 1031, 'iso2code' => 'de', 'language' => 'Deutsch', 'nativeLanguage' => 'Deutsch'),
        'es' => array('lcid' => 3082, 'iso2code' => 'es', 'language' => 'Spanish', 'nativeLanguage' => 'Español'),
        'ru' => array('lcid' => 1049, 'iso2code' => 'ru', 'language' => 'Russian', 'nativeLanguage' => 'Русский'),
        'tr' => array('lcid' => 1055, 'iso2code' => 'tr', 'language' => 'Turkish', 'nativeLanguage' => 'Türkçe'),
        'ar' => array('lcid' => 1025, 'iso2code' => 'ar', 'language' => 'Arabic', 'nativeLanguage' => 'العربية'),
        'fr' => array('lcid' => 1036, 'iso2code' => 'fr', 'language' => 'French', 'nativeLanguage' => 'Français'),
        'pt-pt' => array('lcid' => 1046, 'iso2code' => 'pt-pt', 'language' => 'Portugese', 'nativeLanguage' => 'Português'),
        'it' => array('lcid' => 1040, 'iso2code' => 'it', 'language' => 'Italian', 'nativeLanguage' => 'Italiano'),
        'ko' => array('lcid' => 1042, 'iso2code' => 'ko', 'language' => 'Korean', 'nativeLanguage' => '한국어'),
        'ja' => array('lcid' => 1041, 'iso2code' => 'ja', 'language' => 'Japanese', 'nativeLanguage' => '日本語'),
        'zh-hans' => array('lcid' => 2052, 'iso2code' => 'zh-cn', 'language' => 'Chinese (Simplified)', 'nativeLanguage' => '中文'),
        'zh-hant' => array('lcid' => 1028, 'iso2code' => 'zh-tw', 'language' => 'Chinese (Traditional)', 'nativeLanguage' => '中文(繁體)'),
        'cs' => array('lcid' => 1029, 'iso2code' => 'zh-tw', 'language' => 'Czech', 'nativeLanguage' => 'Čeština'),
        'sv' => array('lcid' => 1089, 'iso2code' => 'sv', 'language' => 'Swedish', 'nativeLanguage' => 'Svenska'),
        'vi' => array('lcid' => 1066, 'iso2code' => 'vi', 'language' => 'Vietnamese', 'nativeLanguage' => 'Vietnamese'),
        'km' => array('lcid' => 1107, 'iso2code' => 'km', 'language' => 'Khmer', 'nativeLanguage' => 'ភាសាខ្មែរ'),
        'id' => array('lcid' => 1057, 'iso2code' => 'id', 'language' => 'Indonesian', 'nativeLanguage' => 'Indonesia'),
        'pl' => array('lcid' => 1045, 'iso2code' => 'pl', 'language' => 'Polish', 'nativeLanguage' => 'Polski'),
        'bg' => array('lcid' => 1026, 'iso2code' => 'bg', 'language' => 'Bulgarian', 'nativeLanguage' => 'Български'),
        'el' => array('lcid' => 1032, 'iso2code' => 'el', 'language' => 'Greek', 'nativeLanguage' => 'Ελληνικα'),
        'da' => array('lcid' => 1030, 'iso2code' => 'da', 'language' => 'Danish', 'nativeLanguage' => 'Dansk'),
        'fi' => array('lcid' => 1035, 'iso2code' => 'fi', 'language' => 'Finnish', 'nativeLanguage' => 'Suomi'),
        'th' => array('lcid' => 1054, 'iso2code' => 'th', 'language' => 'Thai', 'nativeLanguage' => 'ภาษาไทย'),
        'nl' => array('lcid' => 1043, 'iso2code' => 'nl', 'language' => 'Netherlands', 'nativeLanguage' => 'Dutch'),
        'fa' => array('lcid' => 1065, 'iso2code' => 'fa', 'language' => 'Persian', 'nativeLanguage' => 'Farsi'),
        'hu' => array('lcid' => 1038, 'iso2code' => 'hu', 'language' => 'Hungarian', 'nativeLanguage' => 'Magyar'),
        'all' => array('lcid' => null)
    );

    public static function getWidgetsLanguages() {
        $result = array();
        foreach (self::$iso_lcid as $this_key => $this_value) {
            if ($this_value['lcid'])
                $result[$this_key] = $this_value['lcid'];
        }
        return $result;
    }

    /**
     * Get current language
     */
    public static function getCurrentIso() {
        if (defined('ICL_LANGUAGE_CODE')) {
            return strtolower(ICL_LANGUAGE_CODE);
        } else {
            return 'ru';
        }
    }

    /**
     * Get current language LCID
     */
    public static function getCurrentLCID() {
        return self::getLCID(self::getCurrentIso());
    }

    /**
     * Get language LCID of ISO
     */
    public static function getLCID($lang_iso) {
        return self::$iso_lcid[$lang_iso]['lcid'];
    }

}
