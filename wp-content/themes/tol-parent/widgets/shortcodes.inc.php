<?php

/*****************
 * buttonAlpha.js
 * buttonBeta.js
 *****************/
/*---------------------------  BEGIN * buttonAlphaBeta * --------------------------------*/
function buttonAlphaBeta_shortcode($atts) {
    // Attributes
    $a = shortcode_atts(
        array(
            'game' => 'digital',
            'direction' => 'Alpha',
            'skiptouchrate' => 'no',
            'custom' => true,
            'translate' => 'call',
            'class' => 'btn  btn-primary  col-sm-12 col-xs-4 ',
            'tradearea' => '',
            'knockin' => false
        ), $atts);

    // Code
    $knockinAdd = ($a['knockin'] == true) ? 'data-subtype="knockin"' : '';
    if ($a['direction'] == "Alpha") {
        $showRight = "true";
        $button_class = "btn-up";
    } else {
        $showRight = "false";
        $button_class = "btn-down";
    }

    if ($a['tradearea'] != '') {
        $tradearea = ",'tradearea" . $a['tradearea'] . "', '" . $a['translate'] . "'";
    } else {
        $tradearea = "";
    }

    #todo
    if ($a['game'] == 'touch' && $a['skiptouchrate'] == 'no') {
         if ($a['direction'] == "Alpha") {
             return '<div class="touch-button-wrapper"><span data-widget="' . $a['game'] . '_tradologic' . $a['tradearea'] . '_rate' . $a['direction'] . '" class="' . 'col-sm-12 col-xs-4 ' . $a['game'] . 'rate' . $a['direction'] . '"></span><a href="javascript:showRight(' . $showRight . $tradearea . ');" data-widget="' . $a['game'] . '_tradologic' . $a['tradearea'] . '_button' . $a['direction'] . '"                data-custom="' . $a['custom'] . '"  data-translate="' . $a['translate'] . '" class="' . $button_class . ' ' . $a['class'] . ' ' . $a['direction'] . ' widgetButton"></a></div>';
        } else { 
        return '<div class="touch-button-wrapper"><a href="javascript:showRight(' . $showRight . $tradearea . ');" data-widget="' . $a['game'] . '_tradologic' . $a['tradearea'] . '_button' . $a['direction'] . '"                data-custom="' . $a['custom'] . '"  data-translate="' . $a['translate'] . '" class="' . $button_class . ' ' . $a['class'] . ' ' . $a['direction'] . ' widgetButton"></a><span data-widget="' . $a['game'] . '_tradologic' . $a['tradearea'] . '_rate' . $a['direction'] . '" class="' . 'col-sm-12 col-xs-4 ' . $a['game'] . 'rate' . $a['direction'] . '"></span></div>';
        }
    } else if($a['game']== 'oneTouch') {
        return '<a href="javascript:showRight(' . $showRight . $tradearea . ');" data-widget="' . $a['game'] . '_tradologic' . $a['tradearea'] . '_button' . $a['direction'] . '"'. $knockinAdd .' data-custom="' . $a['custom'] . '"  data-translate="' . $a['translate'] . '" class="'  . 'col-md-12 col-sm-3 col-xs-3 ' . $button_class . ' ' . $a['class'] . ' ' . $a['direction'] . ' widgetButton"></a>';
    } else {
        return '<a href="javascript:showRight(' . $showRight . $tradearea . ');" data-widget="' . $a['game'] . '_tradologic' . $a['tradearea'] . '_button' . $a['direction'] . '"'. $knockinAdd .' data-custom="' . $a['custom'] . '"  data-translate="' . $a['translate'] . '" class="' . $button_class . ' ' . $a['class'] . ' ' . $a['direction'] . ' widgetButton"></a>';
    }
}

add_shortcode('buttonAlphaBeta', 'buttonAlphaBeta_shortcode');
/*---------------------------  * END buttonAlphaBeta * --------------------------------*/

/*****************
 * amount.js
 *****************/
/*---------------------------  BEGIN * amount * --------------------------------*/
function amount_shortcode($atts) {

    // Attributes
    $a = shortcode_atts(
        array(
            'game' => 'digital',
            'hideSymbol' => 'false',
            'direction' => 'win',
            'tradearea' => '',
            'knockin' => false
        ), $atts);

        if ($a['direction'] == "win") {
            $button_class = "returnHighAmount";
        } else {
            $button_class = "returnLowAmount";
        }
        
        $widget = '';
        if($a['game']=='ticks'){
            if($a['direction']=='lose'){
                $widget = "Beta";
            }elseif ($a['direction']=='win') {
                $widget = "Alpha";
            }
        }

    // Code
    $knockinAdd = ($a['knockin'] == true) ? 'data-subtype="knockin"' : '';
    return '<span data-widget="' . $a['game'] . '_tradologic' . $a['tradearea'] . '_amount"'. $knockinAdd .'  data-type="' . $a['direction'] . '" data-hideSymbol="' . $a['hideSymbol'] . '" class='.$button_class.'></span>';
}

add_shortcode('amount', 'amount_shortcode');
/*---------------------------  * END amount * --------------------------------*/


/*****************
 * assetCarousel.js
 *****************/
/*---------------------------  BEGIN * assetCarousel * --------------------------------*/
function assetCarousel_shortcode($atts) {

    // Attributes
    $a = shortcode_atts(
        array(
            'game' => 'digital',
            'paging' => 'false',
            'prevnext' => 'true',
        ), $atts);

    // Code
    return '<div data-widget="' . $a['game'] . '_tradologic_assetCarousel" class="assetCarouselWidget" data-prevnext="' . $a['prevnext'] . '" data-paging="' . $a['paging'] . '" style="display: none;"></div>';

}

add_shortcode('assetCarousel', 'assetCarousel_shortcode');
/*---------------------------  * END assetCarousel * --------------------------------*/


/*****************
 * assetName.js
 *****************/
/*---------------------------  BEGIN * assetName * --------------------------------*/
function assetName_shortcode($atts) {

    // Attributes
    $a = shortcode_atts(
        array(
            'game' => 'digital',
        ), $atts);

    // Code
    return '<div data-widget="' . $a['game'] . '_tradologic_assetName" />';

}

add_shortcode('assetName', 'assetName_shortcode');
/*---------------------------  * END assetName * --------------------------------*/

/*****************
 * amountAlpha.js
 * amountBeta.js
 *****************/
/*---------------------------  BEGIN * amountAlphaBeta * --------------------------------*/
function amountAlphaBeta_shortcode($atts) {

    //Attributes
    $a = shortcode_atts(
        array(
            'game' => 'digital',
            'direction' => 'Alpha',
            'hidesymbol' => 'false',
            'defaultvalueload' => 'false',
        ), $atts);

    // Code
    return '<span data-widget="' . $a['game'] . '_tradologic_amount' . $a['direction'] . '" data-hidesymbol="' . $a['hidesymbol'] . '" data-defaultvalueload="' . $a['defaultvalueload'] . '"></span>';
}

add_shortcode('amountAlphaBeta', 'amountAlphaBeta_shortcode');
/*---------------------------  * END amountAlphaBeta * --------------------------------*/

/*****************
 * buttonBuy.js
 *****************/
/*---------------------------  BEGIN * buttonBuy * --------------------------------*/
function buttonBuy_shortcode($atts) {

    //Attributes
    $a = shortcode_atts(
        array(
            'game' => 'digital',
            'tradearea' => '',
            'class' => 'btn btn-primary col-xs-12 tradeButton',
            'knockin' => false,
            'slippage' => false
        ), $atts);

    // Code
    #todo
    $knockinAdd = ($a['knockin'] == true) ? 'data-subtype="knockin"' : '';
    $data = '<input type="button" data-widget="' . $a['game'] . '_tradologic' . $a['tradearea'] . '_buttonBuy"'. $knockinAdd .' value="Buy" class="' . $a['class'] . '" />';
    ##$data = '<input type="button" data-widget="'.$a['game'].'_tradologic_buttonBuy" value="Buy" />';
    return $data;
}

add_shortcode('buttonBuy', 'buttonBuy_shortcode');
/*---------------------------  * END buttonBuy * --------------------------------*/

/*****************
 * chart.js
 *****************/
/*---------------------------  BEGIN * chart * --------------------------------*/
/*function chart_shortcode($atts) {

    //Attributes
    $a = shortcode_atts(
        array(
            'game' => 'digital',
            'gridlines' => 'false',
            'alignxright' => 'false',
            'alignxleft' => 'false',
            'customClass' => '',
        ), $atts);

    // Code
    return '<div id="oldChartContainer" class="' . $a['customClass'] . '" data-widget="' . $a['game'] . '_tradologic_chart" data-gridlines="' . $a['gridlines'] . '" data-alignxright="' . $a['alignxright'] . '" data-alignxleft="' . $a['alignxleft'] . '"></div>';
}

add_shortcode('chart', 'chart_shortcode');*/
/*---------------------------  * END chart * --------------------------------*/

/*****************
 * chartv2.js
 *****************/
/*---------------------------  BEGIN * chartV2 * --------------------------------*/
function chartv2_shortcode($atts) {

    //Attributes
    $a = shortcode_atts(
        array(
            'game' => 'digital',
            'settings' => '"gradientTopColor":"#86BCED","gradientBottomColor":"#F0F7FD","baseColor":"#74B1EA","navHeight":20,"navMaskFill":"rgba(214,228,240,0.75)","navMargin":5,"navHandlesBackground":"#53abe0","navHandlesBorder":"#2b76b6","xLineWidth":0,"xTickWidth":0,"xLabelsY":-2',
            'alignxright' => 'false',
            'alignxleft' => 'false',
            'gridlines' => 'false',
            'tradearea' => '',
            'knockin' => false
        ), $atts);

    // Code
    $knockinAdd = ($a['knockin'] == true) ? 'data-subtype="knockin"' : '';
    $data = '<div data-widget="' . $a['game'] . '_tradologic' . $a['tradearea'] . '_chartv2"'. $knockinAdd .' data-settings={' . $a['settings'] . '} class="chartV2Wrapper">';


        if ($a['game'] == 'ticks') {
            $data .= '<div id="ticksTradeExpiryResultWin" style="display: none;">
                                          <div data-translate="expiryResultWin" style="font-size:25pt;"></div>
                                      </div>
                                      <div id="chartTicksTrade"></div><div id="chartv2">';
        }
        if ($a['game'] != 'turboBinary') {
            $data .= '<ul class="navigationContainer">
                    <li><input class="chartNavigation chartType" type="button" value="Line" id="chartTypeLine" data-translate="chartTypeLine" /></li>
                    <li><input class="chartNavigation chartType" type="button" value="Candle" id="chartTypeCandle" data-translate="chartTypeCandle" /></li>
                    <li><input class="chartNavigation chartType" type="button" value="Area" id="chartTypeArea" data-translate="chartTypeArea" /></li>
                </ul>
            ';
        }else{
             $data .= '<ul class="navigationContainer">
                    <li><input class="chartNavigation chartType" type="button" value="Line" id="chartTypeLine" data-translate="chartTypeLine" /></li>
                    <li><input class="chartNavigation chartType" type="button" value="Area" id="chartTypeArea" data-translate="chartTypeArea" /></li>
                </ul>
            ';
        }
        if (defined('ADVANCED_CHART') && ADVANCED_CHART) {
            $data .= '<div class="advanced_chart tradologic">
                    <a href="#" data-translate="advanced-chart" onmouseover="javascript: changeLink(&quot;AdvancedChartsLink' . $a['tradearea'] . '&quot;);" target="_blank" class="AdvancedChartsLink hidden-xs" id="AdvancedChartsLink' . $a['tradearea'] . '"></a></div>';
        }
              if ($a['game'] != 'turboBinary') {
                    $data .= '<div id="navigationContainer" class="navigationContainerPeriod" style="display: none;">
                            <input type="button" class="chartNavigationPeriod" id="btn1Tick" value="Tick" disabled/>
                            <input type="button" class="chartNavigationPeriod" id="btn1Min" value="1min" />
                            <input type="button" class="chartNavigationPeriod" id="btn5Min" value="5min" />
                            <input type="button" class="chartNavigationPeriod" id="btn15Min" value="15min" />
                            <input type="button" class="chartNavigationPeriod" id="btn30Min" value="30min" />
                            <input type="button" class="chartNavigationPeriod" id="btn60Min" value="1hour" />
                            <input type="button" class="chartNavigationPeriod" id="btn240Min" value="4hours" />
                            <input type="button" class="chartNavigationPeriod" id="btn1440Min" value="1day" />
                        </div>';
                    }



    $data .= '<div id="chartContainer" class="widgetPlaceholder chartWidget chartv2" style="display:none;"></div>
                        <div id="oldChartContainer" class="widgetPlaceholder chartWidget"></div>

                    </div>';
    if ($a['game'] == 'ticks') {
        $data .= '</div>';
    }
    return $data;
}

add_shortcode('chartv2', 'chartv2_shortcode');
/*---------------------------  * END chartV2 * --------------------------------*/

/*---------------------------  BEGIN * chartV2 + Turbo Candle* --------------------------------*/
function chart_shortcode($atts) {

    //Attributes
    $a = shortcode_atts(
        array(
            'game' => 'digital',
            'settings' => '"gradientTopColor":"#86BCED","gradientBottomColor":"#F0F7FD","baseColor":"#74B1EA","navHeight":20,"navMaskFill":"rgba(214,228,240,0.75)","navMargin":5,"navHandlesBackground":"#53abe0","navHandlesBorder":"#2b76b6","xLineWidth":0,"xTickWidth":0,"xLabelsY":-2',
            'alignxright' => 'false',
            'alignxleft' => 'false',
            'gridlines' => 'false',
            'tradearea' => '',
            'knockin' => false
        ), $atts);

    // Code
    $knockinAdd = ($a['knockin'] == true) ? 'data-subtype="knockin"' : '';
    $data = '<div data-widget="' . $a['game'] . '_tradologic' . $a['tradearea'] . '_chartv2"'. $knockinAdd .' data-settings={' . $a['settings'] . '} class="chartV2Wrapper">';


        if ($a['game'] == 'ticks') {
            $data .= '<div id="ticksTradeExpiryResultWin" style="display: none;">
                                          <div data-translate="expiryResultWin" style="font-size:25pt;"></div>
                                      </div>
                                      <div id="chartTicksTrade"></div><div id="chartv2">';
        }
     
        $data .= '<ul class="navigationContainer">
                    <li><input class="chartNavigation chartType" type="button" value="Line" id="chartTypeLine" data-translate="chartTypeLine" /></li>
                    <li><input class="chartNavigation chartType" type="button" value="Candle" id="chartTypeCandle" data-translate="chartTypeCandle" /></li>
                    <li><input class="chartNavigation chartType" type="button" value="Area" id="chartTypeArea" data-translate="chartTypeArea" /></li>
                 </ul>';
     
        if (defined('ADVANCED_CHART') && ADVANCED_CHART) {
            $data .= '<div class="advanced_chart tradologic">
                    <a href="#" data-translate="advanced-chart" onmouseover="javascript: changeLink(&quot;AdvancedChartsLink&quot;);" target="_blank" class="AdvancedChartsLink" id="AdvancedChartsLink"></a></div>';
        }
             
                    $data .= '<div id="navigationContainer" class="navigationContainerPeriod" style="display: none;">
                            <input type="button" class="chartNavigationPeriod" id="btn1Tick" value="Tick" disabled/>
                            <input type="button" class="chartNavigationPeriod" id="btn1Min" value="1min" />
                            <input type="button" class="chartNavigationPeriod" id="btn5Min" value="5min" />
                            <input type="button" class="chartNavigationPeriod" id="btn15Min" value="15min" />
                            <input type="button" class="chartNavigationPeriod" id="btn30Min" value="30min" />
                            <input type="button" class="chartNavigationPeriod" id="btn60Min" value="1hour" />
                            <input type="button" class="chartNavigationPeriod" id="btn240Min" value="4hours" />
                            <input type="button" class="chartNavigationPeriod" id="btn1440Min" value="1day" />
                        </div>';

    $data .= '<div id="chartContainer" class="widgetPlaceholder chartWidget chartv2" style="display:none;"></div>
                        <div id="oldChartContainer" class="widgetPlaceholder chartWidget"></div>

                    </div>';
    if ($a['game'] == 'ticks') {
        $data .= '</div>';
    }
    return $data;
}

add_shortcode('chart', 'chart_shortcode');
/*---------------------------  * END chartV2 + Turbo Candle* --------------------------------*/

/*****************
 * duration.js
 *****************/
/*---------------------------  BEGIN * duration * --------------------------------*/
function duration_shortcode() {

    // Code
    return '<span data-widget="advanced_tradologic_duration" ></span>';
}

add_shortcode('duration', 'duration_shortcode');
/*---------------------------  * END duration * --------------------------------*/

/*****************
 * expireDisplay.js
 *****************/
/*---------------------------  BEGIN * expireDisplay * --------------------------------*/
function expireDisplay_shortcode($atts) {
    //Attributes
    $a = shortcode_atts(
        array(
            'game' => 'digital',
        ), $atts);

    // Code
    return '<span data-widget="' . $a['game'] . '_tradologic_expireDisplay" ></span>';
}

add_shortcode('expireDisplay', 'expireDisplay_shortcode');
/*---------------------------  * END expireDisplay * --------------------------------*/

/*****************
 * expireDropDown.js
 *****************/
/*---------------------------  BEGIN * expireDropDown * --------------------------------*/
function expireDropDown_shortcode($atts) {
    //Attributes
    $a = shortcode_atts(
        array(
            'game' => 'digital',
            'tradearea' => '',
            'knockin' => false
        ), $atts);

    $knockinAdd = ($a['knockin'] == true) ? 'data-subtype="knockin"' : '';

    return '<select data-widget="' . $a['game'] . '_tradologic' . $a['tradearea'] . '_expireDropDown" '. $knockinAdd .' class="form-control btn-default form-control-sm"></select>';
    ##return '<select data-widget="'.$a['game'].'_tradologic_expireDropDown"></select>';
}

add_shortcode('expireDropDown', 'expireDropDown_shortcode');
/*---------------------------  * END expireDropDown * --------------------------------*/

/*****************
 * expireTurbo.js
 *****************/
/*---------------------------  BEGIN * expireTurbo * --------------------------------*/
function expireTurbo_shortcode($atts) {
    //Attributes
    $a = shortcode_atts(
        array(
            'settings' => '',
            'tradearea' => '',
        ), $atts);

    // Code
    return '<div id="expireTurbo-tradologic" data-settings="' . $a['settings'] . '" data-widget="turboBinary_tradologic' . $a['tradearea'] . '_expireTurbo"></div>';
}

add_shortcode('expireTurbo', 'expireTurbo_shortcode');
/*---------------------------  * END expireTurbo * --------------------------------*/

/*****************
 * informationBar.js
 *****************/
/*---------------------------  BEGIN * informationBar * --------------------------------*/
function informationBar_shortcode() {

    // Code
    return '<div data-widget="binary100_tradologic_informationBar"></div>
';
}

add_shortcode('informationBar', 'informationBar_shortcode');
/*---------------------------  * END expireTurbo * --------------------------------*/

/*****************
 * investAmount.js
 *****************/
/*---------------------------  BEGIN * investAmount * --------------------------------*/
function investAmount_shortcode($atts) {
    //Attributes
    $a = shortcode_atts(
        array(
            'game' => 'digital',
            'hidesymbol' => 'false',
            'spinner' => 'false',
            'maxlength' => '5',
            'tradearea' => '',
            'class' => '',
            'knockin' => false
        ), $atts);

    // Code
    #todo
    $knockinAdd = ($a['knockin'] == true) ? 'data-subtype="knockin"' : '';
    $data = '<input data-widget="' . $a['game'] . '_tradologic' . $a['tradearea'] . '_investAmount"'. $knockinAdd .' data-hidesymbol="' . $a['hidesymbol'] . '" data-spinner="' . $a['spinner'] . '" maxlength="' . $a['maxlength'] . '" class="form-control tol-widgetInvestAmount btn-silver form-control-sm ' . $a["class"] . '">';
    #$data = '<input data-widget="'.$a['game'].'_tradologic_investAmount" data-hidesymbol="'.$a['hidesymbol'].'" data-spinner="'.$a['spinner'].'" maxlength="'.$a['maxlength'].'">';
    return $data;
}

add_shortcode('investAmount', 'investAmount_shortcode');
/*---------------------------  * END investAmount * --------------------------------*/

/*****************
 * investTime.js
 *****************/
/*---------------------------  BEGIN * investTime * --------------------------------*/
function investTime_shortcode($atts) {
    //Attributes
    $a = shortcode_atts(
        array(
            'game' => 'digital',
            'singletime' => 'false',
            'time' => 'true',
            'class' => ''
        ), $atts);

    // Code
    return '<span data-widget="' . $a['game'] . '_tradologic_investTime" data-time="' . $a['time'] . '" data-singletime="' . $a['singletime'] . '" class="tol-invest-time ' . $a["class"] . '"></span>';
}

add_shortcode('investTime', 'investTime_shortcode');
/*---------------------------  * END investTime * --------------------------------*/

/*****************
 * openTradeInfo.js
 *****************/
/*---------------------------  BEGIN * openTradeInfo * --------------------------------*/
function openTradeInfo_shortcode() {

    // Code
    return '<div class="ticks_tradologic_openTradeInfo tradeAreaExecutedTrade" data-widget="ticks_tradologic_openTradeInfo">
                <div class="ticksButtonsAndRate">
                    <div data-translate="action"></div>
                    <div class="openTradePutDiv openTradeDirection" data-translate="put"></div>
                    <div class="openTradeCallDiv openTradeDirection" data-translate="call"></div>
                </div>

                <div class="rateAmountWrapper">
                    <div class="ourRateDiv">
                        <span data-translate="ourRate_5ticks"></span>
                        <div class="openTradeRateDiv"></div>
                    </div>

                    <div class="ticksVolumeAmount">
                        <div data-translate="amountLabel" class="amountLabel"></div>
                        <div class="openTradeAmoutDiv"></div>
                    </div>
                </div>

                <div class="tickPayoutDiv">
                    <span data-translate="payoutLabel" class="payoutDesc"></span>
                    <div class="openTradePayoutWrapper">
                        <div class="openTradePayoutValueDiv"></div>
                        <div class="openTradePayoutPercentDiv"></div>
                    </div>
                </div>
                <div class="openTradePutDisplayDiv widgetPlaceholder"></div>
                <div class="openTradeCallDisplayDiv widgetPlaceholder"></div>
            </div>';
}

add_shortcode('openTradeInfo', 'openTradeInfo_shortcode');
/*---------------------------  * END openTradeInfo * --------------------------------*/

/*****************
 * openTrades.js
 *****************/
/*---------------------------  BEGIN * openTrades * --------------------------------*/
function openTrades_shortcode($atts) {
    //Attributes
    $a = shortcode_atts(
        array(
            'turbotime' => 'false',
            'turboslide' => 'false',
            'chart' => 'false',
            'column' => 'digital',
            'showToolsAsDialog' => 'true',
            'minrows' => '10',
        ), $atts);

    // Code
    return '<table class="table table-striped table-responsive" data-widget="'
    . 'openTrades" data-offsetremove="true" data-turbotime="' . $a['turbotime'] . '" data-turboslide="' . $a['turboslide'] . '" data-chart="' . $a['chart'] . '" data-column="' . $a['column'] . '" data-showToolsAsDialog="' . $a['showToolsAsDialog'] . '" data-minrows="' . $a['minrows'] . '">
      <thead>
       <tr>
        <th data-column="type" data-translate="type"></th>
        <th data-column="name" data-sort="string" class="type-float" data-translate="asset"></th>
        <th data-column="action" data-translate="action"></th>
        <th data-column="created" data-sort="int" data-translate="entryDate"></th>
        <th data-column="expires" data-sort="int" class="type-int" data-translate="expire"></th>
        <th data-column="strike" data-sort="int" class="type-int" data-translate="strike"></th>
        <th data-column="invested" data-sort="float" class="type-int" data-translate="invest"></th>
         <th data-column="payout" class="type-int" data-translate="payout"></th>
        <th data-column="actions" data-sort="string" class="type-string" data-translate="tool" data-chart="' . $a['chart'] . '"></th>
        <th data-column="leverage" data-sort="int" class="type-int" data-translate="leverage"></th>
        <th data-column="stopLoss" data-translate="stopLoss"></th>
        <th data-column="takeProfit" data-translate="takeProfit"></th>

       </tr>
      </thead>
     </table>';
}

add_shortcode('openTrades', 'openTrades_shortcode');
/*---------------------------  * END openTrades * --------------------------------*/

/*---------------------------  BEGIN * openTrades for all Games* --------------------------------*/
function openTradesAllGames_shortcode($atts) {
    //Attributes
    $a = shortcode_atts(
        array(
            'turbotime' => 'false',
            'turboslide' => 'false',
            'chart' => 'false',
            'column' => 'digital',
            'showToolsAsDialog' => 'true',
            'minrows' => '10',
        ), $atts);
    

    // Code
    return '<table class="table table-striped table-responsive" data-widget="'
    . 'openTrades" data-offsetremove="true" data-turbotime="' . $a['turbotime'] . '" data-turboslide="' . $a['turboslide'] . '" data-chart="' . $a['chart'] . '" data-column="' . $a['column'] . '" data-showToolsAsDialog="' . $a['showToolsAsDialog'] . '" data-minrows="' . $a['minrows'] . '">
      <thead>
       <tr>
            <th data-column="created" data-sort="int" data-translate="entryDate"></th>
            <th data-column="type" data-translate="openTrades_game"></th>
            <th data-column="name" data-sort="string" class="type-float" data-translate="asset"></th>
            <th data-column="action" data-translate="action"></th>
            <th data-column="strike" data-sort="int" class="type-int" data-translate="strike"></th>
            <th data-column="expires" data-sort="int" class="type-int" data-translate="MyTrades_expiryDate"></th>
            <th data-column="invested" data-sort="float" class="type-int" data-translate="invest"></th>
            <th data-column="leverage" data-sort="int" class="type-int" data-translate="leverage"></th>
            <th data-column="stopLoss" data-translate="stopLoss"></th>
            <th data-column="takeProfit" data-translate="takeProfit"></th>
            <th data-column="payout" class="type-int" data-translate="payout"></th>
            <th data-column="profit" class="type-int" data-translate="profit"></th>
            <th data-column="actions" data-sort="string" class="type-string" data-translate="tool" data-chart="' . $a['chart'] . '"></th>
       </tr>
      </thead>
     </table>';
}


add_shortcode('openTradesAllGames', 'openTradesAllGames_shortcode');
/*---------------------------  * END openTrades for all Games* --------------------------------*/

/*---------------------------  BEGIN * openTradesForex* --------------------------------*/
function openTradesForex_shortcode($atts) {
    //Attributes
    $a = shortcode_atts(
        array(
            'turbotime' => 'false',
            'turboslide' => 'false',
            'chart' => 'false',
            'column' => 'digital',
            'showToolsAsDialog' => 'true',
            'minrows' => '10',
        ), $atts);

    // Code
    return '<table class="table table-striped table-responsive" data-widget="'
    . 'openTrades" data-offsetremove="true" data-turbotime="' . $a['turbotime'] . '" data-turboslide="' . $a['turboslide'] . '" data-chart="' . $a['chart'] . '" data-column="' . $a['column'] . '" data-showToolsAsDialog="' . $a['showToolsAsDialog'] . '" data-minrows="' . $a['minrows'] . '">
      <thead>
       <tr>
            <th data-column="tradeId" data-sort="int" data-translate="tradeID"></th>
            <th data-column="orderId" data-sort="int" data-translate="orderId"></th>
            <th data-column="created" data-sort="int" data-translate="entryTime"></th>
            <th data-column="name" data-sort="string" class="type-float" data-translate="asset"></th>
            <th data-column="action" data-translate="action"></th>
            <th data-column="strike" data-sort="int" class="type-int" data-translate="openPrice"></th>
            <th data-column="invested" data-sort="float" class="type-int" data-translate="investAmount"></th>
            <th data-column="leverage" data-sort="int" class="type-int" data-translate="leverage"></th>
            <th data-column="stopLoss" data-translate="stopLoss"></th>
            <th data-column="takeProfit" data-translate="takeProfit"></th>
            <th data-column="expires" data-sort="int" class="type-int" data-translate="expiryTime"></th>
            <th data-column="currentPrice" class="type-float" data-translate="currentPrice"></th>
            <th data-column="profit" class="type-int" data-translate="profit"></th>
            <th data-column="swap" class="type-int" data-translate="forexSwap"></th>
            <th data-column="actions" data-sort="string" class="type-string" data-translate="tool" data-chart="' . $a['chart'] . '"></th>
       </tr>
      </thead>
     </table>';
}

add_shortcode('openTradesForex', 'openTradesForex_shortcode');
/*---------------------------  * END openTradesForex* --------------------------------*/

/*---------------------------  BEGIN forexPendingOrders * --------------------------------*/

function forexPendingOrders_shortcode($atts) {
    //Attributes
    $a = shortcode_atts(
        array(
            'turbotime' => false,
            'turboslide' => false,
            'chart' => false,
            'column' => 'digital',
            'showToolsAsDialog' => true,
            'minrows' => 10,
        ), $atts);

    return
        '<table class="table table-striped table-responsive" data-widget="forexPendingOrders">
            <thead>
               <tr>
                    <th data-column="orderId" data-sort="int" data-translate="orderId"></th>
                    <th data-column="orderDate" data-translate="orderDate"></th>
                    <th data-column="asset" data-sort="string" data-translate="asset"></th>
                    <th data-column="action" data-translate="action"></th>
                    <th data-column="strike" data-sort="int" class="type-int" data-translate="order_price"></th>
                    <th data-column="invested" data-sort="float" class="type-int" data-translate="investAmount"></th>
                    <th data-column="leverage" data-sort="int" class="type-int" data-translate="leverage"></th>
                    <th data-column="stopLoss" data-translate="stopLoss"></th>
                    <th data-column="takeProfit" data-translate="takeProfit"></th>
                    <th data-column="tools" data-sort="string" class="type-string" data-translate="tool" data-chart="' . $a['chart'] . '"></th>
               </tr>
            </thead>
        </table>';
}

add_shortcode('forexPendingOrders', 'forexPendingOrders_shortcode');

/*---------------------------  * END forexPendingOrders* --------------------------------*/


/*****************
 * optionAccordion.js
 *****************/
/*---------------------------  BEGIN * optionAccordion * --------------------------------*/
function optionAccordion_shortcode($atts) {
    //Attributes
    $a = shortcode_atts(
        array(
            'game' => 'digital',
            'accordiondisable' => 'false',
        ), $atts);

    // Code
    return '<div data-widget="' . $a['game'] . '_tradologic_optionAccordion" data-accordiondisable=' . $a['accordiondisable'] . '></div>';
}

add_shortcode('optionAccordion', 'optionAccordion_shortcode');
/*---------------------------  * END optionAccordion * --------------------------------*/

/*****************
 * optionDropDown.js
 *****************/
/*---------------------------  BEGIN * optionDropDown * --------------------------------*/
function optionDropDown_shortcode($atts) {
    //Attributes
    $a = shortcode_atts(
        array(
            'game' => 'digital',
            'tradearea' => ''
        ), $atts);

    // Code
    return '<select class="form-control btn-default form-control-sm" data-widget="' . $a['game'] . '_tradologic' . $a['tradearea'] . '_optionDropDown"></select>';
}

add_shortcode('optionDropDown', 'optionDropDown_shortcode');
/*---------------------------  * END optionDropDown * --------------------------------*/

/*****************
 * payout.js
 *****************/
/*---------------------------  BEGIN * payout * --------------------------------*/
function payout_shortcode($atts) {
    //Attributes
    $a = shortcode_atts(
        array(
            'game' => 'digital',
            'tradearea' => '',
            'knockin' => false
        ), $atts);

    // Code
    $knockinAdd = ($a['knockin'] == true) ? 'data-subtype="knockin"' : '';

    return '<span data-widget="' . $a['game'] . '_tradologic' . $a['tradearea'] . '_payout"'. $knockinAdd .'class="widgetPayout"></span>';
}

add_shortcode('payout', 'payout_shortcode');
/*---------------------------  * END payout * --------------------------------*/


/*****************
 * payoutDynamic.js
 *****************/
/*---------------------------  BEGIN * payoutDynamic * --------------------------------*/
function payoutDynamic_shortcode($atts) {
    //Attributes
    $a = shortcode_atts(
        array(
            'game' => 'digital',
            'tradearea' => '',
            'knockin' => false
        ), $atts);

    // Code
    $knockinAdd = ($a['knockin'] == true) ? 'data-subtype="knockin"' : '';
    return '<select class="form-control btn-default form-control-sm" data-widget="' . $a['game'] . '_tradologic' . $a['tradearea'] . '_payoutDynamic"'. $knockinAdd .'></select>';
}

add_shortcode('payoutDynamic', 'payoutDynamic_shortcode');
/*---------------------------  * END payoutDynamic * --------------------------------*/


/*****************
 * payoutType.js
 *****************/
/*---------------------------  BEGIN * payoutType * --------------------------------*/
function payoutType_shortcode() {

    // Code

    return '<select data-widget="payoutType"></select>';
}

add_shortcode('payoutType', 'payoutType_shortcode');
/*---------------------------  * END payoutType * --------------------------------*/

/*****************
 * rate.js
 *****************/
/*---------------------------  BEGIN * rate * --------------------------------*/
function rate_shortcode($atts) {
    //Attributes
    $a = shortcode_atts(
        array(
            'game' => 'digital',
            'screen' => 'true',
            'class' => 'form-control input-lg col-sm-12 rate-widget',
            'tradearea' => '',
            'knockin' => false
        ), $atts);


    // Code
    #todo
    $knockinAdd = ($a['knockin'] == true) ? 'data-subtype="knockin"' : '';
    return '<span data-widget="' . $a['game'] . '_tradologic' . $a['tradearea'] . '_rate"'. $knockinAdd .' class="' . $a['class'] . '"></span>';
    ##return '<span data-widget="'.$a['game'].'_tradologic_rate" ></span>';
}

add_shortcode('rate', 'rate_shortcode');
/*---------------------------  * END rate * --------------------------------*/

/*****************
 * rateRisk.js
 *****************/
/*---------------------------  BEGIN * rateRisk * --------------------------------*/
function rateRisk_shortcode($atts) {
    //Attributes
    $a = shortcode_atts(
        array(
            'game' => 'digital',
            'tradearea' => '',
            'knockin' => false
        ), $atts);

    // Code
    $knockinAdd = ($a['knockin'] == true) ? 'data-subtype="knockin"' : '';
    $data = ' <span data-widget="' . $a['game'] . '_tradologic' . $a['tradearea'] . '_rateRisk"'. $knockinAdd .'></span>';
    return $data;
}

add_shortcode('rateRisk', 'rateRisk_shortcode');
/*---------------------------  * END rateRisk * --------------------------------*/

/*****************
 * rateAlpha.js
 * rateBeta.js
 *****************/
/*---------------------------  BEGIN * rateAlphaBeta * --------------------------------*/
function rateAlphaBeta_shortcode($atts) {

    // Attributes
    $a = shortcode_atts(
        array(
            'game' => 'digital',
            'direction' => 'Alpha',
            'translate' => 'call',
            'position' => ''
        ), $atts);

    if ($a['direction'] == "Alpha") {
        if ($a['position'] == "front")
            $widgetRateDir = "tol-widgetRateAlpha col-sm-12 col-xs-4";
        else
            $widgetRateDir = "tol-widgetRateAlpha ";
    } else {
        if ($a['position'] == "front")
            $widgetRateDir = "tol-widgetRateBeta col-sm-12 col-xs-4";
        else
            $widgetRateDir = "tol-widgetRateBeta";
    }

    // Code
    return '<div class="col-sm-12 col-xs-4" style="min-height: 20px; position: absolute;"><span data-widget="' . $a['game'] . '_tradologic_rate' . $a['direction'] . '" class="' . $widgetRateDir . '"></span></div>';
}

add_shortcode('rateAlphaBeta', 'rateAlphaBeta_shortcode');
/*---------------------------  * END rateAlphaBeta * --------------------------------*/

/*****************
 * userBar.js
 *****************/
/*---------------------------  BEGIN * userBar - userIcon * --------------------------------*/
function userBarUserIcon_shortcode()
{

    // Code
    return '<div class="userBarElem" data-widget="userBar" data-type="userIcon"></div>';
}

add_shortcode('userBarUserIcon', 'userBarUserIcon_shortcode');
/*---------------------------  * END userBar - userIcon * --------------------------------*/

/*****************
 * userBar.js
 *****************/
/*---------------------------  BEGIN * userBar - userName * --------------------------------*/
function userBarUserName_shortcode()
{

    // Code
    return '<span class="userBarElem userName" data-widget="userBar" data-type="userName"></span>';
}

add_shortcode('userBarUserName', 'userBarUserName_shortcode');
/*---------------------------  * END userBar - userName * --------------------------------*/

/*****************
 * userBar.js
 *****************/
/*---------------------------  BEGIN * userBar - messages * --------------------------------*/
function userBarMessages_shortcode($atts)
{

    // Attributes
    $a = shortcode_atts(
        array(
            'inboxhref' => '/inbox',
        ), $atts);

    // Code
    return '<a class="userBarElem badge badge-red" data-widget="userBar" data-type="messages" href="' . $a['inboxhref'] . '"></a>';
}

add_shortcode('userBarMessages', 'userBarMessages_shortcode');
/*---------------------------  * END userBar - messages * --------------------------------*/

/*****************
 * userBar.js
 *****************/
/*---------------------------  BEGIN * userBar - bonuses * --------------------------------*/
function userBarBonuses_shortcode($atts)
{

    // Attributes
    $a = shortcode_atts(
        array(
            'bonusesCashback' => 'false',
        ), $atts);

    // Code
    return '<span class="badge badge-red" data-widget="userBar" data-type="bonuses" data-cashback="' . $a['bonusesCashback'] . '"></span>';
}

add_shortcode('userBarBonuses', 'userBarBonuses_shortcode');
/*---------------------------  * END userBar - bonuses * --------------------------------*/

/*****************
 * userBar.js
 *****************/
/*---------------------------  BEGIN * userBar - pending-bonuses * --------------------------------*/
function userBarPendingBonuses_shortcode()
{

    // Code
    return '<span class="badge badge-red"  data-widget="userBar" data-type="messages-pending-bonuses"></span>';
}

add_shortcode('userBarPendingBonuses', 'userBarPendingBonuses_shortcode');
/*---------------------------  * END userBar - pending-bonuses * --------------------------------*/

/*****************
 * userBar.js
 *****************/
/*---------------------------  BEGIN * userBar - balance * --------------------------------*/
function userBarBalance_shortcode($atts)
{

    // Attributes
    $a = shortcode_atts(
        array(
            'balanceNumberWithCommas' => 'true',
        ), $atts);

    // Code
    return '<span class="userBarElem" data-widget="userBar" data-type="balance" data-numberwithcommas="' . $a['balanceNumberWithCommas'] . '"></span>';
}

add_shortcode('userBarBalance', 'userBarBalance_shortcode');
/*---------------------------  * END userBar - balance * --------------------------------*/

/*****************
 * userBar.js
 *****************/
/*---------------------------  BEGIN * userBar - logout * --------------------------------*/
function userBarLogout_shortcode()
{

    // Code
    return '<span class="userBarElem logout" data-widget="userBar" data-type="logout" data-translate="logout"></span>';
}

add_shortcode('userBarLogout', 'userBarLogout_shortcode');
/*---------------------------  * END userBar - logout * --------------------------------*/

/*---------------------------  BEGIN * Bonuses Pop Up * --------------------------------*/
function bonusesPopUp_shortcode($atts)
{

    //Attributes
    $a = shortcode_atts(
        array(
            'showitexplicitly' => 'false',
            'class' => ''
        ), $atts);

    // Code
    $data = '<div class="tol-wrapper tol-bonuses" data-widget="bonusesPopUp" data-show-it-explicitly="false" data-usetraderpoints="false" style="display: none;">
                     <h5 class="tol-title">Pending Bonuses</h5>
                     <!--Header-->
                     <div class="tol-header">
                         <div class="tol-header-type"><!--Bonuse type--></div>
                         <div class="tol-header-bonus" data-translate="pendingBonuses"></div>
                         <div class="tol-header-volume" data-translate="remainingVolume"></div>
                     </div>

                     <!--Pending Bonuses-->
                     <div class="tol-body pending"></div>
                     <!--Regular Bonuses-->
                     <h5 class="tol-title">Bonuses</h5>
                     <div class="tol-body active"></div>
                 </div>';
    return $data;
}

add_shortcode('bonusesPopUp', 'bonusesPopUp_shortcode');
/*---------------------------  * END Bonuses Pop Up * --------------------------------*/

/*---------------------------  BEGIN * Login - username * --------------------------------*/
function loginUsername_shortcode($atts)
{
    //Attributes
    $a = shortcode_atts(
        array(
            'class' => ''
        ), $atts);

    // Code
    return '<input type="text" id="username" data-widget="login" data-type="username" class="' . $a['class']
        . '" placeholder="Email" data-translate="Login_UserName" />';
}

add_shortcode('loginUsername', 'loginUsername_shortcode');
/*---------------------------  END * Login - username * --------------------------------*/

/*---------------------------  BEGIN * Login - password * --------------------------------*/
function loginPassword_shortcode($atts)
{
    //Attributes
    $a = shortcode_atts(
        array(
            'class' => ''
        ), $atts);

    // Code
    return '<input type="password" id="password" data-widget="login" data-type="password" class="' . $a['class']
        . '" placeholder="Password" data-translate="Login_Password" />';
}

add_shortcode('loginPassword', 'loginPassword_shortcode');
/*---------------------------  END * Login - password * --------------------------------*/

/*---------------------------  BEGIN * Login - button * --------------------------------*/
function loginButton_shortcode($atts)
{
    //Attributes
    $a = shortcode_atts(
        array(
            'class' => 'login-button-popup',
            'value' => ''
        ), $atts);

    // Code
    $value = ($a['value'] != '') ? $a['value'] : 'Log in';
    return '<input type="button" data-widget="login" data-type="submit" value="'.$value.'" class="'
        . $a['class'] . '" data-translate="login-title" />';
}

add_shortcode('loginButton', 'loginButton_shortcode');
/*---------------------------  END * Login - button * --------------------------------*/

/*---------------------------  BEGIN * Login - error message * --------------------------------*/
function loginErrorMsg_shortcode()
{

    // Code
    return '<div data-widget="login" data-type="error" style="display:none"></div>';
}

add_shortcode('loginErrorMsg', 'loginErrorMsg_shortcode');
/*---------------------------  END * Login - error message * --------------------------------*/

/*---------------------------  BEGIN * Logout * --------------------------------*/
function logout_shortcode()
{

    // Code
    return '<a href="#" data-widget="logout">Logout</a>';
}

add_shortcode('logout', 'logout_shortcode');
/*---------------------------  END * Logout * --------------------------------*/

/*---------------------------  BEGIN * Slider * --------------------------------*/
function slider_shortcode()
{

    // Code
    $data = '<div data-widget="slider" data-categories="currencies,cryptotokens,cryptocoins"></div>';
    return $data;
}

add_shortcode('slider', 'slider_shortcode');
/*---------------------------  END * Slider * --------------------------------*//*---------------------------  * END userBar - logout * --------------------------------*/


/*****************
 *  My Statistics
 *****************/
/*---------------------------  BEGIN * myStatisticsOptions * --------------------------------*/
function myStatisticsOptions_shortcode($atts)
{
    //Attributes
    $a = shortcode_atts(
        array(
            'class' => ''
        ), $atts);

    // Code
    $data = '<select data-widget="myStatistics" data-type="taTypes" class="' . $a['class'] . '"></select>';
    return $data;
}

add_shortcode('myStatisticsOptions', 'myStatisticsOptions_shortcode');
/*---------------------------  * END myStatisticsOptions  * --------------------------------*/

/*---------------------------  BEGIN * myStatisticsFromDate * --------------------------------*/
function myStatisticsFromDate_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-past' => '',
            'class' => '',
        ), $atts);


    if ($a['data-past'] == '') {

        $data = '<input data-widget="myStatistics" data-type="fromDate" class="' . $a['class'] . '" type="text">';
    } else {

        $data = '<input data-widget="myStatistics" data-type="fromDate" data-past="' . $a['data-past'] . '" class="' . $a['class'] . '" type="text">';
    }
    // Code
    return $data;
}

add_shortcode('myStatisticsFromDate', 'myStatisticsFromDate_shortcode');
/*---------------------------  * END myStatisticsFromDate  * --------------------------------*/
/*****************
 * myTradesToDate.js
 *****************/
/*---------------------------  BEGIN * myStatisticsToDate  * --------------------------------*/
function myStatisticsToDate_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'class' => ''
        ), $atts);


    // Code
    $data = '<input data-widget="myStatistics" data-type="toDate" class="' . $a['class'] . '" type="text">';
    return $data;
}

add_shortcode('myStatisticsToDate', 'myStatisticsToDate_shortcode');
/*---------------------------  * END myStatisticsToDate * --------------------------------*/

/*---------------------------  BEGIN * myStatisticsSearchButton  * --------------------------------*/
function myStatisticsSearchButton_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'translate' => 'show',
            'class' => '',
        ), $atts);
    // Code
    $data = '<input data-widget="myStatistics" data-type="submit" type="button" data-translate="' . $a['translate'] . '" class="' . $a['class'] . '">';
    return $data;
}

add_shortcode('myStatisticsSearchButton', 'myStatisticsSearchButton_shortcode');
/*---------------------------  * END myStatisticsSearchButton  * --------------------------------*/

/*---------------------------  BEGIN * myStatisticsVolume * --------------------------------*/
function myStatisticsVolume_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'class' => '',
        ), $atts);
    // Code
    $data = '<span data-widget="myStatistics" data-type="tradesVolume" class="' . $a['class'] . '" disabled></span>';
    return $data;
}

add_shortcode('myStatisticsVolume', 'myStatisticsVolume_shortcode');
/*---------------------------  * END myTradesVolume * --------------------------------*/
/*****************
 * myTradesProfit.js
 *****************/
/*---------------------------  BEGIN * myStatisticsProfit * --------------------------------*/
function myStatisticsProfit_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'class' => '',
        ), $atts);
    // Code
    $data = '<span data-widget="myStatistics" data-type="tradesProfit" class="' . $a['class'] . '" disabled></span>';
    return $data;
}

add_shortcode('myStatisticsProfit', 'myStatisticsProfit_shortcode');
/*---------------------------  * END myTradesProfit * --------------------------------*/
/*****************
 * myTradesTotalTrades.js
 *****************/
/*---------------------------  BEGIN * myTradesTotalTrades * --------------------------------*/
function myStatisticsTotalTrades_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'class' => '',
        ), $atts);
    // Code
    $data = '<span data-widget="myStatistics" data-type="tradesTotal" class="' . $a['class'] . '" disabled></span>';
    return $data;
}

add_shortcode('myStatisticsTotalTrades', 'myStatisticsTotalTrades_shortcode');
/*---------------------------  * END myTradesTotalTrades * --------------------------------*/


/*****************
 * myTradesOptions.js
 *****************/
/*---------------------------  BEGIN * myTradesOptions - logout * --------------------------------*/
function myTradesOptions_shortcode($atts)
{
    //Attributes
    $a = shortcode_atts(
        array(
            'class' => ''
        ), $atts);

    // Code
    $data = '<select data-widget="myTradesOptions" class="' . $a['class'] . '"></select>';
    return $data;
}

add_shortcode('myTradesOptions', 'myTradesOptions_shortcode');
/*---------------------------  * END myTradesOptions  * --------------------------------*/
/*****************
 * myTradesFromDate.js
 *****************/
/*---------------------------  BEGIN * myTradesFromDate * --------------------------------*/
function myTradesFromDate_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-past' => '',
            'class' => '',
        ), $atts);


    if ($a['data-past'] == '') {

        $data = '<input data-widget="myTradesFromDate" class="' . $a['class'] . '" type="text">';
    } else {
        $data = '<input data-widget="myTradesFromDate" data-past="' . $a['data-past'] . '" class="' . $a['class'] . '" type="text">';
    }
    // Code
    return $data;
}

add_shortcode('myTradesFromDate', 'myTradesFromDate_shortcode');
/*---------------------------  * END myTradesFromDate  * --------------------------------*/
/*****************
 * myTradesToDate.js
 *****************/
/*---------------------------  BEGIN * myTradesToDate  * --------------------------------*/
function myTradesToDate_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'class' => ''
        ), $atts);


    // Code
    $data = '<input data-widget="myTradesToDate" class="' . $a['class'] . '" type="text">';
    return $data;
}

add_shortcode('myTradesToDate', 'myTradesToDate_shortcode');
/*---------------------------  * END myTradesToDate * --------------------------------*/
/*****************
 * myTradesSearchButton.js
 *****************/
/*---------------------------  BEGIN * myTradesSearchButton  * --------------------------------*/
function myTradesSearchButton_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'translate' => 'show',
            'class' => '',
            'showallgames' => 'false', //display close trades for all games true/false -- add in imput - data-showallgames="' . $a['showAllgames'] . '"
        ), $atts);
    // Code
    $data = '<input data-widget="myTradesSearchButton" type="button" data-showallgames="' . $a['showallgames'] . '" data-translate="' . $a['translate'] . '" class="' . $a['class'] . '">';
    return $data;
}

add_shortcode('myTradesSearchButton', 'myTradesSearchButton_shortcode');
/*---------------------------  * END myTradesSearchButton  * --------------------------------*/
/*---------------------------  BEGIN * myTradesSearchButton  * --------------------------------*/
function myTradesSearchButton2_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'translate' => 'show',
            'class' => '',
        ), $atts);
    // Code
    $data = '<input data-widget="myTradesSearchButton" type="button" data-preselectedgame="binaryExchange" data-translate="' . $a['translate'] . '" class="' . $a['class'] . '">';
    return $data;
}

add_shortcode('myTradesSearchButton2', 'myTradesSearchButton2_shortcode');
/*---------------------------  * END myTradesSearchButton  * --------------------------------*/
/*****************
 * myTradesVolume.js
 *****************/
/*---------------------------  BEGIN * myTradesVolume * --------------------------------*/
function myTradesVolume_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'class' => '',
        ), $atts);
    // Code
    $data = '<span data-widget="myTradesVolume" class="' . $a['class'] . '" disabled></span>';
    return $data;
}

add_shortcode('myTradesVolume', 'myTradesVolume_shortcode');
/*---------------------------  * END myTradesVolume * --------------------------------*/
/*---------------------------  BEGIN * myRealForexTradesVolume  * --------------------------------*/
function myRealForexTradesVolume_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'class' => '',
        ), $atts);
    // Code
    $data = '<span id="myTradesVolume" data-widget="realForex-myRealForexTradesVolume" class="' . $a['class'] . '" disabled></span>';
    return $data;
}

add_shortcode('myRealForexTradesVolume', 'myRealForexTradesVolume_shortcode');
/*---------------------------  * END myRealForexTradesVolume * --------------------------------*/
/*****************
 * myTradesProfit.js
 *****************/
/*---------------------------  BEGIN * myTradesProfit * --------------------------------*/
function myTradesProfit_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'class' => '',
        ), $atts);
    // Code
    $data = '<span data-widget="myTradesProfit" class="' . $a['class'] . '" disabled></span>';
    return $data;
}

add_shortcode('myTradesProfit', 'myTradesProfit_shortcode');
/*---------------------------  * END myTradesProfit * --------------------------------*/
/*****************
 * myTradesTotalTrades.js
 *****************/
/*---------------------------  BEGIN * myTradesTotalTrades * --------------------------------*/
function myTradesTotalTrades_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'class' => '',
        ), $atts);
    // Code
    $data = '<span data-widget="myTradesTotalTrades" class="' . $a['class'] . '" disabled></span>';
    return $data;
}

add_shortcode('myTradesTotalTrades', 'myTradesTotalTrades_shortcode');
/*---------------------------  * END myTradesTotalTrades * --------------------------------*/
/*****************
 * myTradesTable.js
 *****************/
/*---------------------------  BEGIN * myTradesTable  * --------------------------------*/
function myTradesTable_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-rowsperpage' => '5',
            'class' => '',
        ), $atts);
    #todo rowsperpage as attribute
    // Code
    $data = '<table data-widget="myTradesTable" data-rowsperpage="' . $a['data-rowsperpage'] . '" class="' . $a['class'] . '">
        <thead>
         <tr>
          <th data-column="name" class="type-float" data-translate="asset"></th>
          <th data-column="optionType" data-translate="option_type"></th>
          <th data-column="action" data-translate="action"></th>
          <th data-column="rate" data-translate="tradeRate"></th>
          <th data-column="invested" class="type-int" data-translate="invest"></th>
          <th data-column="leverage" class="type-int" data-translate="leverage"></th>
          <th data-column="stopLoss" class="type-int" data-translate="stopLoss"></th>
          <th data-column="takeProfit" class="type-int" data-translate="takeProfit"></th>
          <th data-column="expiryRate" class="type-int" data-translate="expiryRate_closeRate"></th>
          <th data-column="expired" data-translate="MyTrades_expiryDate"></th>
          <th data-column="profit" class="type-int" data-translate="profit"></th>
          <th data-column="swap" class="type-int" data-translate="forexSwap"></th>
          <th data-column="social" class="type-int" data-translate="social"></th>
         </tr>
        </thead>
       </table>';
    return $data;
}

add_shortcode('myTradesTable', 'myTradesTable_shortcode');

function myTradesTableRealForex_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-rowsperpage' => '10',
            'class' => '',
        ), $atts);
    #todo rowsperpage as attribute
    // Code
    $data = '<table data-widget="myRealForexTradesTable" data-rowsperpage="' . $a['data-rowsperpage'] . '" class="' . $a['class'] . '">
        <thead>
            <tr>
                <th data-column="position" class="type-string" data-translate="positionId">Position ID</th>
                <th data-column="id" class="type-string" data-translate="forexTable_id"></th>
                <th data-column="name" class="type-float" data-translate="forexTable_asset"></th>
                <th data-column="volume" data-translate="forexTable_quantity"></th>
                <th data-column="action" data-translate="forexTable_direction"></th>
                <th data-column="rate" data-translate="forexTable_rate"></th>
                <th data-column="created" data-translate="forexTable_created"></th>            
                <th data-column="swap" class="type-int" data-translate="forexTable_swap"></th>
                <th data-column="commission" data-sort="float">Commission</th>
                <th data-column="result" class="type-int" data-translate="forexTable_result"></th>
                <th data-column="copied-from" class="type-string" data-translate="copiedFrom"></th>
            </tr>
        </thead></table>';
    return $data;
}

add_shortcode('myTradesTableRealForex', 'myTradesTableRealForex_shortcode');
/*---------------------------  * END myTradesTable  * --------------------------------*/
/*---------------------------  BEGIN * myEasyForexTradesTable  * --------------------------------*/
function myEasyForexTradesTable_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-rowsperpage' => '10',
            'class' => '',
        ), $atts);
    #todo rowsperpage as attribute
    // Code
    $data = '<table data-widget="easyForex-myEasyForexTradesTable" data-rowsperpage="' . $a['data-rowsperpage'] . '" class="' . $a['class'] . '">
        <thead>
            <tr>
                <th data-column="tradeID" data-sort="int" class="name canSort canResize canDrag">
                                <div class="cellWrapper"><span class="content" data-translate="tradeID">Trade ID</span></div>
                            </th>
                            <th data-column="openTime" data-sort="int" class="name canSort canResize canDrag">
                                <div class="cellWrapper"><span class="content" data-translate="openTime">Open Time</span></div>
                            </th>
                            <th data-column="asset" data-sort="string" class="name canSort canResize canDrag">
                                <div class="cellWrapper"><span class="content" data-translate="asset">Asset</span></div>
                            </th>
                            <th data-column="type" data-sort="float" class="name canSort canResize canDrag">
                                <div class="cellWrapper"><span class="content" data-translate="type">Type</span></div>
                            </th>
                            <th data-column="openPrice" data-sort="float" class="name canSort canResize canDrag">
                                <div class="cellWrapper"><span class="content" data-translate="openPrice">Open Price</span></div>
                            </th>
                            <th data-column="investAmount" data-sort="float" class="name canSort canResize canDrag">
                                <div class="cellWrapper"><span class="content" data-translate="investAmount">Invest Amount</span></div>
                            </th>
                            <th data-column="risk" data-sort="float" class="name canSort canResize canDrag">
                                <div class="cellWrapper"><span class="content" data-translate="risk">Risk</span></div>
                            </th>
                            <th data-column="stopLoss" data-sort="float" class="name canSort canResize canDrag">
                                <div class="cellWrapper"><span class="content" data-translate="stopLoss">Stop Loss</span></div>
                            </th>
                            <th data-column="takeProfit" data-sort="float" class="name canSort canResize canDrag">
                                <div class="cellWrapper"><span class="content" data-translate="takeProfit">Take Profit</span></div>
                            </th>
                            <th data-column="closeTime" data-sort="float" class="name canSort canResize canDrag">
                                <div class="cellWrapper"><span class="content" data-translate="closeTime">Close Time</span></div>
                            </th>
                            <th data-column="closePrice" data-sort="float" class="name canSort canResize canDrag">
                                <div class="cellWrapper"><span class="content" data-translate="closePrice">Close Price</span></div>
                            </th>
                            <th data-column="swap" data-sort="float" class="name canSort canResize canDrag">
                                <div class="cellWrapper"><span class="content" data-translate="swap">Swap</span></div>
                            </th>
                            <th data-column="commissions" data-sort="float" class="name canSort canResize canDrag">
                                <div class="cellWrapper"><span class="content" data-translate="commissions">Commissions</span></div>
                            </th>
                            <th data-column="profit" data-sort="string" class="name canSort canResize canDrag">
                                <div class="cellWrapper"><span class="content" data-translate="profit">Profit</span></div>
                            </th>
                            <th data-column="actions" data-sort="string" class="name canSort canResize canDrag">
                                <div class="cellWrapper"><span class="content" ></span></div>
                            </th>
            </tr>
        </thead></table>';
    return $data;
}

add_shortcode('myEasyForexTradesTable', 'myEasyForexTradesTable_shortcode');
function myTradesTableBinaryExchange_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-rowsperpage' => '5',
            'class' => '',
        ), $atts);
    #todo rowsperpage as attribute
    // Code
    $data = '<table data-widget="myTradesTable" data-type="exchange" data-rowsperpage="' . $a['data-rowsperpage'] . '" class="' . $a['class'] . '"  style="display: none;">
                <thead>
                <tr>
                    <th data-column="tradeId" class="type-string" data-translate="mytrades-tradeid"></th>
                    <th data-column="timestamp"  data-translate="mytrades-timestamp"></th>
                    <th data-column="asset" class="type-string" data-translate="mytrades-asset"></th>
                    <th data-column="strike" class="type-int" data-translate="mytrades-strike"></th>
                    <th data-column="expiry" class="type-int" data-translate="mytrades-expiry"></th>                                     
                    <th data-column="direction" data-translate="mytrades-direction"></th>                                  
                    <th data-column="lots" class="type-int" data-translate="mytrades-lots"></th>
                    <th data-column="limitPrice" class="type-int" data-translate="mytrades-price"></th>
                    <th data-column="rate" class="type-string" data-translate="mytrades-rate">Rate</th>
                    <th data-column="fee" class="type-string" data-translate="mytrades-fee"></th>
                    <th data-column="pl" class="type-string" data-translate="mytrades-pl"></th>            
                </tr>
                </thead>
       </table>';
    return $data;
}

add_shortcode('myTradesTableBinaryExchange', 'myTradesTableBinaryExchange_shortcode');


/*---------------------------  BEGIN * myTradesTable  * --------------------------------*/
function closeTradesTable_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-rowsperpage' => '5',
            'class' => '',
        ), $atts);
    #todo rowsperpage as attribute
    // Code
    $data = '<table data-widget="myTradesTable" data-rowsperpage="' . $a['data-rowsperpage'] . '" class="' . $a['class'] . '">
        <thead>
         <tr>
          <th data-column="created" data-translate="entryDate"></th>
          <th data-column="optionType" data-translate="option_type"></th>
          <th data-column="name" class="type-float" data-translate="asset"></th>
          <th data-column="action" data-translate="action"></th>
          <th data-column="expired" data-translate="Expiry_Date"></th>
          <th data-column="rate" data-translate="MyTrades_openRate"></th>
          <th data-column="invested" class="type-int" data-translate="invest"></th>
          <th data-column="leverage" class="type-int" data-translate="leverage"></th>
          <th data-column="stopLoss" class="type-int" data-translate="stopLoss"></th>
          <th data-column="takeProfit" class="type-int" data-translate="takeProfit"></th>
          <th data-column="expiryRate" class="type-int" data-translate="MyTrades_closeRate"></th>
          <th data-column="profit" class="type-int" data-translate="profit"></th>

         </tr>
        </thead>
       </table>';
    return $data;
}

add_shortcode('closeTradesTable', 'closeTradesTable_shortcode');
/*---------------------------  * END myTradesTable  * --------------------------------*/

/*****************
 * myTradesPaging.js
 *****************/
/*---------------------------  BEGIN * myTradesPaging  * --------------------------------*/
function myTradesPaging_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'paginglimit' => '5',
            'class' => 'my-trades-paging',
        ), $atts);
    // Code
    $data = '<div data-widget="myTradesPaging" data-pagingLimit="' . $a['paginglimit'] . '" class="' . $a['class'] . '"></div>';

    return $data;
}

add_shortcode('myTradesPaging', 'myTradesPaging_shortcode');

function myRealForexTradesPaging_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'paginglimit' => '5',
            'class' => 'my-trades-paging',
        ), $atts);
    // Code
    $data = '<div data-widget="myRealForexTradesPaging" data-pagingLimit="' . $a['paginglimit'] . '" class="' . $a['class'] . '"></div>';

    return $data;
}

add_shortcode('myRealForexTradesPaging', 'myRealForexTradesPaging_shortcode');
/*---------------------------  * END myTradesPaging  * --------------------------------*/
/*****************
 * notificatioins.js
 *****************/
/*---------------------------  BEGIN * notificatioins * --------------------------------*/
function notificatioins_shortcode()
{

    // Code
    $data = '<span data-widget="notificatioins"></span>';
    return $data;
}

add_shortcode('notificatioins', 'notificatioins_shortcode');
/*---------------------------  * END notificatioins * --------------------------------*/
/*****************
 * traderInsightBar.js
 *****************/
/*---------------------------  BEGIN * traderInsightBar  * --------------------------------*/
function traderInsightBar_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'game' => 'digital',
            'paginglimit' => 'show',
            'assetid' => 0,
            'orientation' => 'vertical',
            'tradearea' => ''
        ), $atts);
    // Code
    $data = '<div data-widget="' . $a['game'] . '_tradologic' . $a['tradearea'] . '_traderInsightBar" data-assetid="' . $a['assetid'] . '" data-orientation="' . $a['orientation'] . '" class="traderInsightBar"></div>';
    return $data;
}

add_shortcode('traderInsightBar', 'traderInsightBar_shortcode');
/*---------------------------  * END traderInsightBar  * --------------------------------*/

/*****************
 * contracts.js
 *****************/
/*---------------------------  BEGIN * contracts * --------------------------------*/
function contracts_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'game' => 'digital',
            'tradearea' => '',
            'class' => ''
        ), $atts);
    // Code
    $data = '<select  data-widget="' . $a['game'] . '_tradologic' . $a['tradearea'] . '_contracts" class="' . $a["class"] . ' form-control tol-widgetContracts btn-default form-control-sm"></select>';
    return $data;
}

add_shortcode('contracts', 'contracts_shortcode');
/*---------------------------  * END contracts * --------------------------------*/
/*****************
 * tableBase.js
 *****************/
/*---------------------------  BEGIN * tableBase  * --------------------------------*/
function tableBase_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'game' => 'digital',
            'knockin' => false
        ), $atts);

    $knockinAdd = ($a['knockin'] == true) ? 'data-subtype="knockin"' : '';
    if ($a['game'] == 'turboBinary') {

        $data = '<table data-widget="' . $a['game'] . '_tradologic_tableBase" data-colums="name,rate,payout" class="table table-striped table-responsive">
 <thead>
  <tr>
   <th data-column="name" data-sort="string" class="type-string" data-translate="asset"></th>
   <th data-column="rate" class="type-int" data-translate="rate"></th>
  </tr>
 </thead>
</table>';
    } else {
        $data = ' <table data-widget="' . $a['game'] . '_tradologic_tableBase"'. $knockinAdd .' data-colums="name,rate,payout" class="table table-striped table-responsive">
 <thead>
  <tr>
   <th data-column="name" data-sort="string-ins" class="type-string" data-translate="asset"></th>
   <th data-column="expire" data-sort="string-ins" class="type-int" data-translate="expire"></th>
   <th data-column="rate" class="type-int" data-translate="rate"></th>
   <th data-column="payout" data-sort="int" class="type-int" data-sort-const="asc" data-translate="payout"></th>
  </tr>
 </thead>
</table>';
    };
    return $data;
}

add_shortcode('tableBase', 'tableBase_shortcode');
/*---------------------------  * END tableBase  * --------------------------------*/
/*****************
 * assetList.js
 *****************/
/*---------------------------  BEGIN * assetList  * --------------------------------*/
function assetList_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'game' => 'digital',
            'knockin' => false
        ), $atts);

    $data = '<div data-widget="' . $a['game'] . '_tradologic_assetList" class="asset-list-new"></div>';
    return $data;
}

add_shortcode('assetList', 'assetList_shortcode');
/*---------------------------  * END assetList  * --------------------------------*/

/*****************
 * table.js
 *****************/
/*---------------------------  BEGIN * tableBase  * --------------------------------*/
function table_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'game' => 'digital',
        ), $atts);
    // Code
    #todo to remove  bootstrap classes
    if ($a['game'] == 'turboBinary') {

        $data = '<table data-widget="' . $a['game'] . '_tradologic_table" data-colums="name,rate,payout" class="table table-striped table-responsive widget-table">
 <thead>
  <tr>
   <th data-column="name" data-sort="string" class="type-string" data-translate="asset"></th>
   <th data-column="rate" class="type-int" data-translate="rate"></th>
  </tr>
 </thead>
</table>';
    } else if ($a['game'] == 'advanced') {
        $data = '<table data-widget="advanced_tradologic_table" class="table table-striped table-responsive widget-table">
                 <thead>
                     <tr>
                         <th data-column="name" data-sort="string-ins" class="type-string" data-translate="asset"></th>
                         <th data-column="rate" data-sort="float" class="type-int" data-translate="rate"></th>
                     </tr>
                 </thead>
             </table>';
    } else if ($a['game'] == 'forex') {
        $data = '<table data-widget="forex_tradologic_table" class="table table-striped table-responsive widget-table">
                     <thead>
                         <tr>
                             <th data-column="name" data-sort="string-ins" class="type-string" data-translate="asset"></th>
                             <th data-column="rateBid" data-sort="float" class="type-int" data-translate="rateAsk"></th>
                             <th data-column="rateAsk" data-sort="float" class="type-int" data-translate="rateBid"></th>
                         </tr>
                     </thead>
                 </table>';
    } else if ($a['game'] == 'ticks') {
        $data = '<table data-widget="ticks_tradologic_table" class="table table-striped table-responsive widget-table">
                <thead>
                    <tr>
                        <th data-column="name" data-sort="string-ins" class="type-string" data-translate="asset"></th>
                        <th data-column="rate" data-sort="float" class="type-int" data-translate="rate"></th>
                    </tr>
                </thead>
            </table>';
    } else {
        $data = ' <table data-widget="' . $a['game'] . '_tradologic_table" data-colums="name,rate,payout" class="table table-striped table-responsive widget-table" style="display: none">
 <thead>
  <tr>
   <th data-column="name" data-sort="string-ins" class="type-string" data-translate="asset"></th>
   <th data-column="expire" data-sort="string-ins" class="type-int" data-translate="expire"></th>
   <th data-column="rate" class="type-int" data-translate="rate"></th>
   <th data-column="payout" data-sort="int" class="type-int" data-sort-const="asc" data-translate="payout"></th>
  </tr>
 </thead>
</table>';
    };
    return $data;
}

add_shortcode('table', 'table_shortcode');
/*---------------------------  * END tableBase  * --------------------------------*/


/*****************
 * assetFilterCustom.js
 *****************/
/*---------------------------  BEGIN * assetFilterCustom popup* --------------------------------*/
function assetFilterCustom_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'type' => 'assetTypeFilter',
            'dropdown_arrow' => '',
        ), $atts);
    // Code
    $data = '<button data-widget="assetFilterCustom" data-type="' . $a['type'] . '" style="display: none;" type="button" class="btn btn-sm dropdown-toggle btn-default" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-cog" aria-hidden="true"><span class="caret" aria-hidden="true"></span></button>';
    return $data;
}

add_shortcode('assetFilterCustomPopup', 'assetFilterCustom_shortcode');

/*---------------------------  * END assetFilterCustom popup* --------------------------------*/
/*****************
 * assetFilterCustom.js
 *****************/
/*---------------------------  BEGIN * assetFilterCustom * --------------------------------*/
function assetFilterCustomPopup_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'show_custom_assets' => 'true',
        ), $atts);
    // Code
    $data = '<div data-widget="assetFilterCustom" data-translate=\'{"cancel":"Cancel","confirm":"Apply"}\' data-type="customFilter" data-show-custom-assets="' . $a['show_custom_assets'] . '" id="tol-assset-filter" class="custom-select btn-default btn-group bootstrap-select form-control btn-default form-control-sm" style="display: none;"></div>';
    return $data;
}

add_shortcode('assetFilterCustom', 'assetFilterCustomPopup_shortcode');
/*---------------------------  * END assetFilterCustom * --------------------------------*/
/*****************
 * traderInsight.js
 *****************/
/*---------------------------  BEGIN * traderInsight * --------------------------------*/
function traderInsight_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'max_items' => '8',
        ), $atts);
    // Code
    $data = '<div data-widget="traderInsight" data-maxlength="' . $a['max_items'] . '" class="widgetPlaceholder traderInsight" style="display: none"></div>';
    return $data;
}

add_shortcode('traderInsight', 'traderInsight_shortcode');
/*---------------------------  * END traderInsight * --------------------------------*/

/********** CASHIER SHORTCODES **********/

/*---------------------------  BEGIN * cashierCardDeposit * --------------------------------*/

function cashierCardDepositErrorPlaceholder_shortcode()
{
    // Code
    $data = '<div data-widget="cashierCardDeposit" data-type="error" style="display: none"></div>';
    return $data;
}

add_shortcode('cashierCardDepositErrorPlaceholder', 'cashierCardDepositErrorPlaceholder_shortcode');

function cashierCardDepositAmount_shortcode()
{
    // Code
    $data = '<div data-widget="cashierCardDeposit" data-type="amount"></div>';
    return $data;
}

add_shortcode('cashierCardDepositAmount', 'cashierCardDepositAmount_shortcode');

function cashierCardDepositCardNumber_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'maxlength' => '20',
            'data-isrequired' => 'true'
        ), $atts);
    // Code
    $data = '<input type="text" data-widget="cashierCardDeposit" data-type="number" maxlength="' . $a['maxlength'] . '" data-isrequired="' . $a['data-isrequired'] . '" style="display: none" />';
    return $data;
}

add_shortcode('cashierCardDepositCardNumber', 'cashierCardDepositCardNumber_shortcode');

function cashierCardDepositExpirationMonth_shortcode()
{
    // Code
    $data = '<select data-widget="cashierCardDeposit" data-type="expirationMonth" style="display: none"></select>';
    return $data;
}

add_shortcode('cashierCardDepositExpirationMonth', 'cashierCardDepositExpirationMonth_shortcode');

function cashierCardDepositExpirationYear_shortcode()
{
    // Code
    $data = '<select data-widget="cashierCardDeposit" data-type="expirationYear" style="display: none"></select>';
    return $data;
}

add_shortcode('cashierCardDepositExpirationYear', 'cashierCardDepositExpirationYear_shortcode');

function cashierCardDepositCVV_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'maxlength' => '4',
            'data-isrequired' => 'true'
        ), $atts);
    // Code
    $data = '<input type="text" data-widget="cashierCardDeposit" data-type="cvv" data-minLength="3" maxlength="' . $a['maxlength'] . '" data-isrequired="' . $a['data-isrequired'] . '" style="display: none" />';
    return $data;
}

add_shortcode('cashierCardDepositCVV', 'cashierCardDepositCVV_shortcode');

function cashierCardDepositCardName_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'maxlength' => '20',
            'data-isrequired' => 'true'
        ), $atts);
    // Code
    $data = '<input type="text" data-widget="cashierCardDeposit" data-type="nameOnCard" maxlength="' . $a['maxlength'] . '" data-isrequired="' . $a['data-isrequired'] . '" style="display: none" />';
    return $data;
}

add_shortcode('cashierCardDepositCardName', 'cashierCardDepositCardName_shortcode');

function cashierCardDepositSubmitButton_shortcode()
{
    // Code
    $data = '<input type="button" data-widget="cashierCardDeposit" data-type="submit" value="Deposit" style="display: none" />';
    return $data;
}

add_shortcode('cashierCardDepositSubmitButton', 'cashierCardDepositSubmitButton_shortcode');

/*---------------------------  END * cashierCardDeposit * --------------------------------*/

/*---------------------------  BEGIN * cashierCardDepositQuick * --------------------------------*/

function cashierCardDepositQuickAmount_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-regexpmessage' => 'Enter valid amount'
        ), $atts);
    // Code
    $data = '<input type="text" data-widget="cashierCardDepositQuick" data-type="amount" data-regexpmessage="' . $a['data-regexpmessage'] . '" style="display: none" />';
    return $data;
}

add_shortcode('cashierCardDepositQuickAmount', 'cashierCardDepositQuickAmount_shortcode');

function cashierCardDepositQuickActiveCC_shortcode()
{
    // Code
    $data = '<select data-widget="cashierCardDepositQuick" data-type="activeCreditCards" style="display: none"></select>';
    return $data;
}

add_shortcode('cashierCardDepositQuickActiveCC', 'cashierCardDepositQuickActiveCC_shortcode');

function cashierCardDepositQuickCardCVV_shortcode()
{
    // Code
    $data = '<input type="text" data-widget="cashierCardDepositQuick" data-minLength="3" data-type="cardCvv" maxlength="4" data-isRequired="true" style="display: none" />';
    return $data;
}

add_shortcode('cashierCardDepositQuickCardCVV', 'cashierCardDepositQuickCardCVV_shortcode');

function cashierCardDepositQuickPromoCodeLink_shortcode()
{
    // Code
    $data = '<a data-widget="cashierCardDepositQuick" href="#" style="display:none">Have a promo code?</a>';
    return $data;
}

add_shortcode('cashierCardDepositQuickPromoCodeLink', 'cashierCardDepositQuickPromoCodeLink_shortcode');

function cashierCardDepositQuickPromoCode_shortcode()
{
    // Code
    $data = '<input type="text" data-widget="cashierCardDepositQuick" data-type="promotionalCode" style="display: none" />';
    return $data;
}

add_shortcode('cashierCardDepositQuickPromoCode', 'cashierCardDepositQuickPromoCode_shortcode');

function cashierCardDepositQuickSubmitButton_shortcode()
{
    // Code
    $data = '<input type="button" data-widget="cashierCardDepositQuick" data-type="submit" value="Submit Deposit" style="display: none" />';
    return $data;
}

add_shortcode('cashierCardDepositQuickSubmitButton', 'cashierCardDepositQuickSubmitButton_shortcode');

function cashierCardDepositQuickTryAgain_shortcode()
{
    // Code
    $data = '<input type="button" data-widget="cashierCardDepositQuick" data-type="tryagain" value="Try Again" style="display: none" />';
    return $data;
}

add_shortcode('cashierCardDepositQuickTryAgain', 'cashierCardDepositQuickTryAgain_shortcode');

function cashierCardDepositQuickError_shortcode()
{
    // Code
    $data = '<div data-widget="cashierCardDepositQuick" data-type="error" style="display: none"></div>';
    return $data;
}

add_shortcode('cashierCardDepositQuickError', 'cashierCardDepositQuickError_shortcode');

function cashierCardDepositQuickIFrameHolder_shortcode()
{
    // Code
    $data = '<div data-widget="cashierCardDepositQuick" data-type="iframeHolder" style="display: none"></div>';
    return $data;
}

add_shortcode('cashierCardDepositQuickIFrameHolder', 'cashierCardDepositQuickIFrameHolder_shortcode');

function cashierCardDepositQuickPromoCodeValidation_shortcode()
{
    // Code
    $data = '<div data-widget="cashierCardDepositQuick" data-type="promoCodeValidation" style="display: none">';
    return $data;
}

add_shortcode('cashierCardDepositQuickPromoCodeValidation', 'cashierCardDepositQuickPromoCodeValidation_shortcode');

//<span data-widget="cashierCardDepositQuick" data-translate="cashierLabelPromoCodeValidation"></span> ???

/*---------------------------  END * cashierCardDepositQuick * --------------------------------*/

/*---------------------------  BEGIN * cashierCreditCardDeposit * --------------------------------*/
function cashierCreditCardDepositErrorMsg_shortcode()
{
    // Code
    $data = '<div data-widget="cashierCreditCardDeposit" data-type="error" style="display: none"></div>';
    return $data;
}

add_shortcode('cashierCreditCardDepositErrorMsg', 'cashierCreditCardDepositErrorMsg_shortcode');

function cashierCreditCardDepositAmount_shortcode()
{
    // Code
    $data = '<label data-widget="cashierCreditCardDeposit" data-type="amount"></label>';
    return $data;
}

add_shortcode('cashierCreditCardDepositAmount', 'cashierCreditCardDepositAmount_shortcode');

function cashierCreditCardDepositCardType_shortcode()
{
    // Code
    $data = '<select data-widget="cashierCreditCardDeposit" data-type="cardType" style="display:none" class="form-control"></select>';
    return $data;
}

add_shortcode('cashierCreditCardDepositCardType', 'cashierCreditCardDepositCardType_shortcode');

function cashierCreditCardDepositCardNumber_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'maxlength' => '20',
            'data-isRequired' => 'true',
            'data-regexp' => '^5[1-5]\d{2}-?\d{4}-?\d{4}-?\d{4}$',
            'data-regexpmessage' => 'Invalid credit card number'
        ), $atts);
    // Code
    $data = '<input type="text" data-widget="cashierCreditCardDeposit" data-type="number" maxlength="' . $a['maxlength'] . '" data-isRequired="' . $a['data-isRequired'] . '" data-regexpmessage="' . $a['data-regexpmessage'] . '" data-regexp="' . $a['data-regexp'] . '" class="form-control" style="display:none" />';
    return $data;
}

add_shortcode('cashierCreditCardDepositCardNumber', 'cashierCreditCardDepositCardNumber_shortcode');

function cashierCreditCardDepositExpirationMonth_shortcode()
{
    // Code
    $data = '<select data-widget="cashierCreditCardDeposit" data-type="expirationMonth" style="display:none" class="form-control"></select>';
    return $data;
}

add_shortcode('cashierCreditCardDepositExpirationMonth', 'cashierCreditCardDepositExpirationMonth_shortcode');

function cashierCreditCardDepositExpirationYear_shortcode()
{
    // Code
    $data = '<select data-widget="cashierCreditCardDeposit" data-type="expirationYear" style="display:none" class="form-control"></select>';
    return $data;
}

add_shortcode('cashierCreditCardDepositExpirationYear', 'cashierCreditCardDepositExpirationYear_shortcode');

function cashierCreditCardDepositCVV_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'maxlength' => '3',
            'data-isRequired' => 'true'
        ), $atts);
    // Code
    $data = '<input type="text" data-widget="cashierCreditCardDeposit" data-type="cvv" data-minLength="3" maxlength="' . $a['maxlength'] . '" data-isRequired="' . $a['data-isRequired'] . '" style="display:none" class="form-control" />';
    return $data;
}

add_shortcode('cashierCreditCardDepositCVV', 'cashierCreditCardDepositCVV_shortcode');

function cashierCreditCardDepositFirstName_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'maxlength' => '20',
            'data-isRequired' => 'true'
        ), $atts);
    // Code
    $data = '<input type="text" data-widget="cashierCreditCardDeposit" data-type="firstName" data-regexp="[A-Za-z]" maxlength="' . $a['maxlength'] . '" data-isRequired="' . $a['data-isRequired'] . '" style="display:none" class="form-control" />';
    return $data;
}

add_shortcode('cashierCreditCardDepositFirstName', 'cashierCreditCardDepositFirstName_shortcode');

function cashierCreditCardDepositLastName_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'maxlength' => '20',
            'data-isRequired' => 'true'
        ), $atts);
    // Code
    $data = '<input type="text" data-widget="cashierCreditCardDeposit" data-type="lastName" data-regexp="[A-Za-z]" maxlength="' . $a['maxlength'] . '" data-isRequired="' . $a['data-isRequired'] . '" style="display:none" class="form-control" />';
    return $data;
}

add_shortcode('cashierCreditCardDepositLastName', 'cashierCreditCardDepositLastName_shortcode');

function cashierCreditCardDepositAddress1_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'maxlength' => '40',
            'data-isRequired' => 'true'
        ), $atts);
    // Code
    $data = '<input type="text" data-widget="cashierCreditCardDeposit" data-type="address1" maxlength="' . $a['maxlength'] . '" data-isRequired="' . $a['data-isRequired'] . '" style="display:none" class="form-control" />';
    return $data;
}

add_shortcode('cashierCreditCardDepositAddress1', 'cashierCreditCardDepositAddress1_shortcode');

function cashierCreditCardDepositAddress2_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'maxlength' => '40'
        ), $atts);
    // Code
    $data = '<input type="text" data-widget="cashierCreditCardDeposit" data-type="address2" maxlength="' . $a['maxlength'] . '" style="display:none" class="form-control" />';
    return $data;
}

add_shortcode('cashierCreditCardDepositAddress2', 'cashierCreditCardDepositAddress2_shortcode');

function cashierCreditCardDepositCity_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'maxlength' => '20',
            'data-isRequired' => 'true'
        ), $atts);
    // Code
    $data = '<input type="text" data-widget="cashierCreditCardDeposit" data-type="city" maxlength="' . $a['maxlength'] . '" data-isRequired="' . $a['data-isRequired'] . '" style="display:none" class="form-control" />';
    return $data;
}

add_shortcode('cashierCreditCardDepositCity', 'cashierCreditCardDepositCity_shortcode');

function cashierCreditCardDepositPostalCode_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'maxlength' => '15',
            'data-isRequired' => 'true'
        ), $atts);
    // Code
    $data = '<input type="text" data-widget="cashierCreditCardDeposit" data-type="zip" maxlength="' . $a['maxlength'] . '" data-isRequired="' . $a['data-isRequired'] . '" style="display:none" class="form-control" />';
    return $data;
}

add_shortcode('cashierCreditCardDepositPostalCode', 'cashierCreditCardDepositPostalCode_shortcode');

function cashierCreditCardDepositCountry_shortcode()
{
    // Code
    $data = '<select data-widget="cashierCreditCardDeposit" data-type="country" style="display:none" class="form-control"></select>';
    return $data;
}

add_shortcode('cashierCreditCardDepositCountry', 'cashierCreditCardDepositCountry_shortcode');

function cashierCreditCardDepositProvince_shortcode()
{
    // Code
    $data = '<select data-widget="cashierCreditCardDeposit" data-type="province" style="display:none" class="form-control cashierProvinceField"></select>';
    return $data;
}

add_shortcode('cashierCreditCardDepositProvince', 'cashierCreditCardDepositProvince_shortcode');

function cashierCreditCardDepositSubmitButton_shortcode()
{
    // Code
    $data = '<input type="button" data-widget="cashierCreditCardDeposit" data-type="submit" value="Deposit" style="display:none" class="ccdepositBTN btn btn-primary" />';
    return $data;
}

add_shortcode('cashierCreditCardDepositSubmitButton', 'cashierCreditCardDepositSubmitButton_shortcode');

/*---------------------------  END * cashierCreditCardDeposit * --------------------------------*/

/*---------------------------  BEGIN * cashierDeposit * --------------------------------*/
function cashierDepositCurrency_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-isrequired' => 'true'
        ), $atts);
    // Code
    $data = '<select data-widget="cashierDeposit" data-type="currencies" data-isrequired="' . $a['data-isrequired'] . '" style="display: none" id="depositCurrSelect" class="cashierDDL form-control"></select>';
    return $data;
}

add_shortcode('cashierDepositCurrency', 'cashierDepositCurrency_shortcode');

function cashierDepositMethod_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-isrequired' => 'true'
        ), $atts);
    // Code
    $data = '<select data-widget="cashierDeposit" data-type="methods" data-isrequired="' . $a['data-isrequired'] . '" style="display: none" id="depositTypeSelect" class="form-control"></select>';
    return $data;
}

add_shortcode('cashierDepositMethod', 'cashierDepositMethod_shortcode');

function cashierDepositValidationError_shortcode()
{
    // Code
    $data = '<span data-widget="cashierDeposit" class="validationError" style="display:none">No payment methods available for your country.</span>';
    return $data;
}

add_shortcode('cashierDepositValidationError', 'cashierDepositValidationError_shortcode');

function cashierDepositUserID_shortcode()
{
    // Code
    $data = '<input type="text" data-widget="cashierDeposit" data-type="userId" style="display: none" />';
    return $data;
}

add_shortcode('cashierDepositUserID', 'cashierDepositUserID_shortcode');

function cashierDepositPassword_shortcode()
{
    // Code
    $data = '<input type="password" data-widget="cashierDeposit" data-type="password" style="display: none" />';
    return $data;
}

add_shortcode('cashierDepositPassword', 'cashierDepositPassword_shortcode');

function cashierDepositPasswordOpen_shortcode()
{
    // Code
    $data = '<input type="text" data-widget="cashierDeposit" data-type="passwordOpen" style="display: none" />';
    return $data;
}

add_shortcode('cashierDepositPasswordOpen', 'cashierDepositPasswordOpen_shortcode');

function cashierDepositAmount_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-regexpmessage' => 'Enter valid amount'
        ), $atts);
    // Code
    $data = '<input type="text" data-widget="cashierDeposit" data-type="amount" data-regexpmessage="' . $a['data-regexpmessage'] . '" id="depositAmountInput" class="form-control" />';
    return $data;
}

function cashierDepositTo()
{
    // Code
    $data = '<select class="form-control"  data-widget="cashierDeposit" data-type="depositTo" ></select>';
    return $data;
}
function cashierTransferFrom()
{
    // Code
    $data = '<select class="form-control"  data-widget="cashierWithdraw" data-type="transferFrom" ></select>';
    return $data;
}


function cashierDepositCVV_shortcode()
{
    // Code
    $data = '<input type="text" data-widget="cashierDeposit" data-type="cvv" data-minLength="3" maxlength="4" data-isRequired="false" class="form-control" style="display: none" />';
    return $data;
}

add_shortcode('cashierDepositCVV', 'cashierDepositCVV_shortcode');

add_shortcode('cashierDepositTo', 'cashierDepositTo');

add_shortcode('cashierTransferFrom', 'cashierTransferFrom');

add_shortcode('cashierDepositAmount', 'cashierDepositAmount_shortcode');

function cashierDepositActiveCC_shortcode()
{
    // Code
    $data = '<select data-widget="cashierDeposit" data-type="activeCreditCards" id="activeCreditCards" class="form-control cashierDDL"></select>';
    return $data;
}

add_shortcode('cashierDepositActiveCC', 'cashierDepositActiveCC_shortcode');

function cashierDepositPromoCode_shortcode()
{
    // Code
    $data = '<input type="text" data-widget="cashierDeposit" data-type="promotionalCode" class="promoCode form-control">';
    return $data;
}

add_shortcode('cashierDepositPromoCode', 'cashierDepositPromoCode_shortcode');

function cashierDepositButton_shortcode()
{
    // Code
    $data = '<input type="button" class="depositBTN btn btn-primary" data-widget="cashierDeposit" data-type="submit" value="Continue" data-translate="cashierContinueButton">';
    return $data;
}

add_shortcode('cashierDepositButton', 'cashierDepositButton_shortcode');


/*---------------------------  END * cashierDeposit * --------------------------------*/

/*---------------------------  BEGIN * cashierWithdraw * --------------------------------*/
function cashierWithdrawValidationText_shortcode()
{
    // Code
    $data = '<span data-widget="cashierWithdraw" data-translate="cashierLabelValidationLinkText" class="cashierLabelValidationLinkText" style="display: none"></span>';
    return $data;
}

add_shortcode('cashierWithdrawValidationText', 'cashierWithdrawValidationText_shortcode');

function cashierWithdrawAmount_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'maxlength' => '10',
            'data-isrequired' => 'true'
        ), $atts);
    // Code
    $data = '<input type="text" data-widget="cashierWithdraw" data-type="amount" data-isrequired="' . $a['data-isrequired'] . '" maxlength="' . $a['maxlength'] . '" style="display: none" class="form-control tol-cashier-withdraw-amount" />';
    return $data;
}

add_shortcode('cashierWithdrawAmount', 'cashierWithdrawAmount_shortcode');

function cashierWithdrawCannotBeMoreThan_shortcode()
{
    // Code
    $data = '<span data-widget="cashierWithdraw" data-translate="cashierLabelCannotBeMoreThan" style="display: none"></span>';
    return $data;
}

add_shortcode('cashierWithdrawCannotBeMoreThan', 'cashierWithdrawCannotBeMoreThan_shortcode');

function cashierWithdrawMethod_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-isrequired' => 'true'
        ), $atts);
    // Code
    $data = '<select data-widget="cashierWithdraw" data-type="method" data-isrequired="' . $a['data-isrequired'] . '" style="display: none" id="cashierPreferredPaymentMethod" class="form-control"></select>';
    return $data;
}

add_shortcode('cashierWithdrawMethod', 'cashierWithdrawMethod_shortcode');

function cashierWithdrawAccount_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-isrequired' => 'true'
        ), $atts);
    // Code
    $data = '<input type="text" data-widget="cashierWithdraw" data-type="account" data-isrequired="' . $a['data-isrequired'] . '" style="display: none" class="form-control" />';
    return $data;
}

add_shortcode('cashierWithdrawAccount', 'cashierWithdrawAccount_shortcode');

function cashierWithdrawComment_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-isrequired' => 'true'
        ), $atts);
    // Code
    $data = '<input type="text" data-widget="cashierWithdraw" data-type="comment" data-isrequired="' . $a['data-isrequired'] . '" style="display: none" class="form-control" />';
    return $data;
}

add_shortcode('cashierWithdrawComment', 'cashierWithdrawComment_shortcode');

function cashierWithdrawCustomerName_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-isrequired' => 'true'
        ), $atts);
    // Code
    $data = '<input type="text" data-widget="cashierWithdraw" data-type="customerName" data-isrequired="' . $a['data-isrequired'] . '" style="display: none" class="form-control" />';
    return $data;
}

add_shortcode('cashierWithdrawCustomerName', 'cashierWithdrawCustomerName_shortcode');

function cashierWithdrawBankName_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-isrequired' => 'true'
        ), $atts);
    // Code
    $data = '<input type="text" data-widget="cashierWithdraw" data-type="bankName" data-isrequired="' . $a['data-isrequired'] . '" style="display: none" class="form-control" /> ';
    return $data;
}

add_shortcode('cashierWithdrawBankName', 'cashierWithdrawBankName_shortcode');

function cashierWithdrawBankCode_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-isrequired' => 'true'
        ), $atts);
    // Code
    $data = '<input type="text" data-widget="cashierWithdraw" data-type="bankCode" data-isrequired="' . $a['data-isrequired'] . '" style="display: none" class="form-control" />';
    return $data;
}

add_shortcode('cashierWithdrawBankCode', 'cashierWithdrawBankCode_shortcode');

function cashierWithdrawBranchAddress_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-isrequired' => 'true'
        ), $atts);
    // Code
    $data = '<input type="text" data-widget="cashierWithdraw" data-type="branchAddress" data-isrequired="' . $a['data-isrequired'] . '" style="display: none" class="form-control" />';
    return $data;
}

add_shortcode('cashierWithdrawBranchAddress', 'cashierWithdrawBranchAddress_shortcode');

function cashierWithdrawBranchCode_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-isrequired' => 'true'
        ), $atts);
    // Code
    $data = '<input type="text" data-widget="cashierWithdraw" data-type="branchCode" data-isrequired="' . $a['data-isrequired'] . '" style="display: none" class="form-control" />';
    return $data;
}

add_shortcode('ashierWithdrawBranchCode', 'cashierWithdrawBranchCode_shortcode');

function cashierWithdrawAccountNumber_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-isrequired' => 'true'
        ), $atts);
    // Code
    $data = '<input type="text" data-widget="cashierWithdraw" data-type="accountNumber" data-isrequired="' . $a['data-isrequired'] . '" style="display: none" class="form-control" />';
    return $data;
}

add_shortcode('cashierWithdrawAccountNumber', 'cashierWithdrawAccountNumber_shortcode');

function cashierWithdrawIban_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-isrequired' => 'true'
        ), $atts);
    // Code
    $data = '<input type="text" data-widget="cashierWithdraw" data-type="iban" data-isrequired="' . $a['data-isrequired'] . '" style="display: none" class="form-control" />';
    return $data;
}

add_shortcode('cashierWithdrawIban', 'cashierWithdrawIban_shortcode');


function cashierWithdrawSwift_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-isrequired' => 'true'
        ), $atts);
    // Code
    $data = '<input type="text" data-widget="cashierWithdraw" data-type="swift" data-isrequired="' . $a['data-isrequired'] . '" style="display: none" class="form-control" />';
    return $data;
}

add_shortcode('cashierWithdrawSwift', 'cashierWithdrawSwift_shortcode');

function cashierWithdrawCurrency_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-isrequired' => 'true'
        ), $atts);
    // Code
    $data = '<input type="text" data-widget="cashierWithdraw" data-type="currency" data-isrequired="' . $a['data-isrequired'] . '" style="display: none" class="form-control" />';
    return $data;
}

add_shortcode('cashierWithdrawCurrency', 'cashierWithdrawCurrency_shortcode');

function cashierWithdrawSubmit_shortcode()
{
    // Code
    $data = '<input type="button" data-widget="cashierWithdraw" data-type="submit" value="" style="display: none" class="withdrawBtn btn btn-primary" />';
    return $data;
}

add_shortcode('cashierWithdrawSubmit', 'cashierWithdrawSubmit_shortcode');

function cashierWithdrawCancel_shortcode()
{
    // Code
    $data = '<input type="button" data-widget="cashierWithdraw" data-type="cancel" value="" style="display: none" class="withdrawCancelBtn btn btn-primary" />';
    return $data;
}

add_shortcode('cashierWithdrawCancel', 'cashierWithdrawCancel_shortcode');

function cashierWithdrawInfo_shortcode()
{
    // Code
    $data = '<span data-widget="cashierWithdraw" data-translate="cashierWithdrawInfo" style="display:none"></span>';
    return $data;
}

add_shortcode('cashierWithdrawInfo', 'cashierWithdrawInfo_shortcode');

function cashierWithdrawOK_shortcode()
{
    // Code
    $data = '<span data-widget="cashierWithdraw"  data-translate="cashierWithdrawOK" style="display:none"></span>';
    return $data;
}

add_shortcode('cashierWithdrawOK', 'cashierWithdrawOK_shortcode');

function cashierWithdrawErrorCC_shortcode()
{
    // Code
    $data = '<span data-widget="cashierWithdraw"  data-translate="cashierWithdrawErrorCC" style="display:none"></span>';
    return $data;
}

add_shortcode('cashierWithdrawErrorCC', 'cashierWithdrawErrorCC_shortcode');

/*---------------------------  END * cashierWithdraw * --------------------------------*/

/*---------------------------  BEGIN * cashierWithdrawalHistory * --------------------------------*/
function cashierWithdrawalHistory_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-date-format' => 'yyyy-mm-dd'
        ), $atts);
    // Code
    $data = '<table data-widget="cashierWithdrawalHistory" data-date-format="' . $a['data-date-format'] . '" data-type="withdrawalTable" class="table table-striped">
             <thead>
              <tr>
               <th data-column="dateTime" data-translate="cashierwithdrawDate"></th>
               <th data-column="withdrawalTransactionId">Transaction ID</th>
               <th data-column="amount" data-translate="cashierwithdrawAmount"></th>
               <th data-column="status" data-translate="cashierwithdrawStatus"></th>
               <th data-column="cancel" data-translate="cashierwithdrawCancel"></th>
              </tr>
             </thead>
            </table>';
    return $data;
}

add_shortcode('cashierWithdrawalHistory', 'cashierWithdrawalHistory_shortcode');

function cashierWithdrawalHistoryPaging_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-pagingLimit' => '10'
        ), $atts);
    // Code
    $data = '<div data-widget="cashierWithdrawalHistory" data-type="paging" data-pagingLimit="' . $a['data-pagingLimit'] . '"></div>';
    return $data;
}

add_shortcode('cashierWithdrawalHistoryPaging', 'cashierWithdrawalHistoryPaging_shortcode');
/*---------------------------  END * cashierWithdrawalHistory * --------------------------------*/

/*---------------------------  BEGIN * cashierBonusHistory * --------------------------------*/
function cashierBonusHistory_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-date-format' => 'yyyy-mm-dd'
        ), $atts);
    // Code
    $data = '<table data-widget="cashierBonusHistory"  data-date-format="' . $a['data-date-format'] . '" data-type="bonusTable" data-acceptBonusPopUp="true" class="table table-striped cashier-bonus-history">
              <thead>
               <tr>
                <th data-column="bonus" data-translate="cashierBonusHistoryBonusLabel"></th>
                <th data-column="amount" data-translate="cashierBonusHistoryAmountLabel"></th>
                <th data-column="dateTime" data-translate="cashierBonusHistoryDateLabel"></th>
                <th data-column="expiryTime" data-translate="cashierBonusHistoryExpiryDateLabel" class="dataLinkChange" data-link="/terms-and-conditions" style="display: none"></th>
                <th data-column="tradingRequirements" data-translate="cashierBonusHistoryTRLabel"></th>
                <th data-column="remaining" data-translate="cashierBonusHistoryRemainingLabel"></th>
                <th data-column="status" data-translate="cashierBonusHistoryStatusLabel"></th>
                <th data-column="bonusType" data-translate="cashierBonusHistoryTypeLabel"></th>
                <th data-column="action" data-translate="cashierBonusHistoryActionLabel"></th>
               </tr>
              </thead>
            </table>';
    return $data;
}

add_shortcode('cashierBonusHistory', 'cashierBonusHistory_shortcode');

//* bonus promo code *//

function cashierBonusHistoryPromoCode_shortcode($atts)
{
    $data = '<div data-widget="cashierBonusHistory" data-type="promocodewrapper" class="cashier-bonus-history promocode-wrapper" style="display: none">
                <input type="text" data-widget="cashierBonusHistory" data-type="promocodeinput" class="form-control cashier-bonus-history promocodes-input"></input>
                <button type="button" data-widget="cashierBonusHistory" data-type="promocodebutton" class="btn btn-primary cashier-bonus-history promocodes-button" data-translate="cashierContinueButton"></button>
                <div data-widget="cashierBonusHistory" data-type="message" class="promocode-message" style="display: none"></div>
            </div>';
    return $data;
}

add_shortcode('cashierBonusHistoryPromoCode', 'cashierBonusHistoryPromoCode_shortcode');

function cashierBonusHistoryPaging_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-pagingLimit' => '10'
        ), $atts);
    // Code
    $data = '<div data-widget="cashierBonusHistory" data-type="paging" data-pagingLimit="' . $a['data-pagingLimit'] . '" ></div>';
    return $data;
}

add_shortcode('cashierBonusHistoryPaging', 'cashierBonusHistoryPaging_shortcode');
/*---------------------------  END * cashierBonusHistory * --------------------------------*/

/*---------------------------  BEGIN * cashierTransactionHistory * --------------------------------*/
function cashierTransactionHistory_shortcode()
{
    // Code
    $data = '<table data-widget="cashierTransactionHistory" data-type="depositTable" class="table table-striped">
              <thead>
               <tr>
                <th data-column="dateTime" data-translate="cashierTransactionHistoryChangeAt"></th>
                <th data-column="amount" data-translate="cashierTransactionHistoryBalance"></th>
                <th data-column="method" data-translate="cashierTransactionHistoryChangeType"></th>
                <th data-column="description" data-translate="cashierTransactionHistoryDescription"></th>
               </tr>
              </thead>
            </table>';
    return $data;
}

add_shortcode('cashierTransactionHistory', 'cashierTransactionHistory_shortcode');

function cashierTransactionHistoryPaging_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-pagingLimit' => '10'
        ), $atts);
    // Code
    $data = '<div data-widget="cashierTransactionHistory" data-type="paging" data-pagingLimit="' . $a['data-pagingLimit'] . '" ></div>';
    return $data;
}

add_shortcode('cashierTransactionHistoryPaging', 'cashierTransactionHistoryPaging_shortcode');
/*---------------------------  END * cashierTransactionHistory * --------------------------------*/

/********** END CASHIER SHORTCODES **********/


/*---------------------------  BEGIN * asset list gamesMenu * --------------------------------*/
function assetIndexPerGameGamesMenu_shortcode()
{


    $data = '<div data-widget="assetsIndexPerGame" data-type="gamesMenu"></div>';
    return $data;
}

add_shortcode('assetIndexPerGameGamesMenu', 'assetIndexPerGameGamesMenu_shortcode');
/*---------------------------  END * asset list gamesMenu * --------------------------------*/

/*---------------------------  BEGIN * asset list preloader * --------------------------------*/
function assetIndexPerGamePreLoader_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-translate' => 'loading',
        ), $atts);

    $data = '<div data-widget="assetsIndexPerGame" data-type="preloader"><span data-translate="' . $a['data-translate'] . '"></span></div>';
    return $data;
}

add_shortcode('assetIndexPerGamePreLoader', 'assetIndexPerGamePreLoader_shortcode');
/*---------------------------  END * asset list preloader * --------------------------------*/

/*---------------------------  BEGIN * asset list assetsContainer * --------------------------------*/
function assetIndexPerGameAssetsContainer_shortcode($atts)
{
    if ($_COOKIE['current_language']!='en') $traderoomURL = '/'.$_COOKIE['current_language'].'/traderoom';
    else $traderoomURL = '/traderoom';

    $a = shortcode_atts(
        array(
            'data-popup-details' => 'click',
            'data-popup-hover-timeout' => '1500',
            'data-translate' => 'menu_turbobinary":"menu_turbo", "menu_onetouch":"menu_oneTouch',
            'data-initial-game' => '',
            'data-traderoom-url' => $traderoomURL,
            'data-games' => 'digital,range,touch,turboBinary,oneTouch,advanced,forex,ticks,knockin',
        ), $atts);

    $data = '<div data-widget="assetsIndexPerGame" data-type="assetsContainer" data-popup-details="' . $a['data-popup-details'] . '" data-popup-hover-timeout="' . $a['data-popup-hover-timeout'] . '" data-translate=\'{
    "' . $a['data-translate'] . '"}\' data-initial-game="' . $a['data-initial-game'] . '" data-traderoom-url="' . $a['data-traderoom-url'] . '" data-games="' . $a['data-games'] . '" id="tol-assetsContainer"></div>';
    return $data;
}

add_shortcode('assetIndexPerGameAssetsContainer', 'assetIndexPerGameAssetsContainer_shortcode');
/*---------------------------  END * asset list assetsContainer * --------------------------------*/


#todo check if needed
/*---------------------------  BEGIN * contactUs container * --------------------------------*/
function contactUsContainer_shortcode($atts, $content = null)
{
    $a = shortcode_atts(array(
        'class' => 'wrapper-contacts',
    ), $atts);
    return '<div data-widget="contactUs" data-type="container" style="display: none" class="wrapper-contacts">' . do_shortcode($content) . '</div>';
}

add_shortcode('contactUsContainer', 'contactUsContainer_shortcode');

/*---------------------------  END * contactUs container * --------------------------------*/

/*---------------------------  BEGIN * contactUs name * --------------------------------*/
function contactUsContactName_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-isRequired' => 'true',
            'class' => '',
            'placeholder' => '',
            'translate' => ''
        ), $atts);

    $data = '<input placeholder="'.$a['placeholder'].'" data-translate="'.$a['translate'].'" id="name" type="text" data-widget="contactUs" data-type="contactName" data-isRequired="' . $a['data-isRequired'] . '" class="' . $a['class'] . ' widgetPlaceholder input"/>';
    return $data;
}

add_shortcode('contactUsContactName', 'contactUsContactName_shortcode');
/*---------------------------  END * contactUs name * --------------------------------*/

/*---------------------------  BEGIN * contactUs phone * --------------------------------*/
function contactUsContactPhone_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-isRequired' => 'true',
            'data-regexpMessage' => "Please enter your Phone",
            'data-decorate' => "maskPhone",
            'placeholder' => "+_(___)___-____",
            'class' => '',

        ), $atts);

    $data = '<input id="phone" type="text" data-widget="contactUs" data-regexpMessage="' . $a['data-regexpMessage'] . '"
data-type="phone" data-isRequired="' . $a['data-isRequired'] . '" data-decorate="' . $a['data-decorate'] . '" placeholder="' . $a['placeholder'] . '" data-regexpmessage="' . $a['data-regexpMessage'] . '" class="' . $a['class'] . ' widgetPlaceholder input" />';
    return $data;
}

add_shortcode('contactUsContactPhone', 'contactUsContactPhone_shortcode');
/*---------------------------  END * contactUs phone * --------------------------------*/

/*---------------------------  BEGIN * contactUs email * --------------------------------*/
function contactUsContactEmail_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-isRequired' => 'true',
            'data-regexpMessage' => "Please enter a valid email address",
            'class' => '',
            'placeholder' => '',
            'translate' => ''
        ), $atts);

    $data = '<input placeholder="'.$a['placeholder'].'" data-translate="'.$a['translate'].'" type="text" data-widget="contactUs" data-regexpMessage="' . $a['data-regexpMessage'] . '" data-type="email" data-isRequired="' . $a['data-isRequired'] . '" class="' . $a['class'] . ' widgetPlaceholder input" />';
    return $data;
}

add_shortcode('contactUsContactEmail', 'contactUsContactEmail_shortcode');
/*---------------------------  END * contactUs email * --------------------------------*/

/*---------------------------  BEGIN * contactUs subject * --------------------------------*/
function contactUsContactSubject_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-isRequired' => 'true',
            'class' => '',
        ), $atts);

    $data = '<input id="subject" type="text" data-widget="contactUs" data-type="subject" data-isRequired="' . $a['data-isRequired'] . '" class="' . $a['class'] . ' widgetPlaceholder input" />';
    return $data;
}

add_shortcode('contactUsContactSubject', 'contactUsContactSubject_shortcode');
/*---------------------------  END * contactUs subject * --------------------------------*/

/*---------------------------  BEGIN * contactUs subjectSelect * --------------------------------*/
function contactUsContactSubjectSelect_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-isRequired' => 'true',
            'class' => '',
        ), $atts);

    $data = '<div data-widget="contactUs" data-type="subjectSelect" data-isRequired="' . $a['data-isRequired'] . '" class="' . $a['class'] . ' widgetPlaceholder">

            <div data-subjecttext="deposit/withdraw" data-translate="conactUsDeposit" class="tol-subject-selector deposit" style="text-transform:capitalize"></div>
            <div data-subjecttext="trading" data-translate="conactUsTrading" class="tol-subject-selector trading"></div>
            <div data-subjecttext="technical" data-translate="conactUsTechnical" class="tol-subject-selector technical"></div>
            <div data-subjecttext="registration" data-translate="conactUsRegistration" class="tol-subject-selector registration" style="text-transform:capitalize"></div>
            <div data-subjecttext="general" data-translate="conactUsGenaral" class="tol-subject-selector general"></div>
        </div>';
    return $data;
}

add_shortcode('contactUsContactSubjectSelect', 'contactUsContactSubjectSelect_shortcode');
/*---------------------------  END * contactUs subjectSelect * --------------------------------*/

/*---------------------------  BEGIN * contactUs submit * --------------------------------*/
function contactUsContactSubmit_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-translate' => 'ContactUs_Submit',
            'class' => '',
        ), $atts);

    $data = '<input type="button" data-translate="' . $a['data-translate'] . '" id="submit"  data-widget="contactUs" data-type="submit" class="' . $a['class'] . ' widgetPlaceholder submitBtn" />';
    return $data;
}

add_shortcode('contactUsContactSubmit', 'contactUsContactSubmit_shortcode');
/*---------------------------  END * contactUs submit * --------------------------------*/

/*---------------------------  BEGIN * contactUs error * --------------------------------*/
function contactUsContactError_shortcode()
{

    $data = '<div data-widget="contactUs" data-type="error" style=" display: none"></div>';
    return $data;
}

add_shortcode('contactUsContactError', 'contactUsContactError_shortcode');
/*---------------------------  END * contactUs error * --------------------------------*/

/*---------------------------  BEGIN * contactUs contactMessage * --------------------------------*/
function contactUsContactContactMessage_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-isRequired' => 'true',
            'cols' => "50",
            'rows' => "6",
            'class' => '',
        ), $atts);

    $data = ' <textarea id="message" cols="50" rows="6" data-widget="contactUs"
                  data-type="contactMessage" data-isRequired="' . $a['data-isRequired'] . '" class="' . $a['class'] . ' widgetPlaceholder input" ></textarea>';
    return $data;
}

add_shortcode('contactUsContactContactMessage', 'contactUsContactContactMessage_shortcode');
/*---------------------------  END * contactUs contactMessage * --------------------------------*/


/*---------------------------  BEGIN * copy board paging * --------------------------------*/
function socialCopyBoardPaging_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-rowsperpage' => '1',
            'data-prevnextpage' => "true",

        ), $atts);
    $data = ' <div data-widget="socialCopyBoard2" class="copyBrdPaging" data-type="paging" data-rowsperpage="' . $a['data-rowsperpage'] . '" data-prevnextpage="' . $a['data-prevnextpage'] . '"></div>';
    return $data;
}

add_shortcode('socialCopyBoardPaging', 'socialCopyBoardPaging_shortcode');
/*---------------------------  END * copy board paging * --------------------------------*/


/*---------------------------  BEGIN * copy board table * --------------------------------*/
function socialCopyBoardTable_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-columns' => 'name,profit,hitrate,followers,trades,investAmount,edit',

        ), $atts);
    $data = '<table data-widget="socialCopyBoard2" class="social-copy-board table table-responsive" data-type="table" data-columns="' . $a['data-columns'] . '"></table>';
    return $data;
}

add_shortcode('socialCopyBoardTable', 'socialCopyBoardTable_shortcode');
/*---------------------------  END * copy board table * --------------------------------*/


/*---------------------------  BEGIN * reset password error  * --------------------------------*/
function resetPasswordError_shortcode()
{

    $data = '<div style="display: none;" data-widget="resetPassword" data-type="error"></div>';
    return $data;
}

add_shortcode('resetPasswordError', 'resetPasswordError_shortcode');
/*---------------------------  END * reset password error  * --------------------------------*/


/*---------------------------  BEGIN * reset password error  * --------------------------------*/
function resetPasswordEmail_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-regexpMessage' => 'Please enter a valid email address',
            'data-isrequired' => 'true',
            'class' => '',

        ), $atts);
    $data = '<input class="' . $a['class'] . '" type="text" data-widget="resetPassword" data-type="email" data-regexpMessage="' . $a['data-regexpMessage'] . '" data-isrequired="' . $a['data-isrequired'] . '" />';
    return $data;
}

add_shortcode('resetPasswordEmail', 'resetPasswordEmail_shortcode');
/*---------------------------  END * reset password error  * --------------------------------*/


/*---------------------------  BEGIN * reset password error  * --------------------------------*/
function resetPasswordSubmit_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-translate' => 'ForgotPassword_Submit',
            'class' => '',

        ), $atts);
    $data = '<input class="' . $a['class'] . '" type="button" data-translate="' . $a['data-translate'] . '" data-widget="resetPassword" data-type="submit" />';
    return $data;
}

add_shortcode('resetPasswordSubmit', 'resetPasswordSubmit_shortcode');
/*---------------------------  END * reset password error  * --------------------------------*/


/*---------------------------  BEGIN * inbox message Inbox  * --------------------------------*/
function messageInboxAllMessages_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-translate-inbox' => 'inboxSubject',
            'data-translate-from' => 'inboxActiveFrom',

        ), $atts);
    $data = '
        <table id="tol-messageInbox" data-widget="messageInbox" data-type="allMessages" style="display:none;" class="table table-striped">
        <thead>
        <tr>
            <th data-column="subject" data-translate="' . $a['data-translate-inbox'] . '"></th>
            <th data-column="activeFrom" data-translate="' . $a['data-translate-from'] . '"></th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>';
    return $data;
}

add_shortcode('messageInboxAllMessages', 'messageInboxAllMessages_shortcode');
/*---------------------------  END * inbox message Inbox  * --------------------------------*/


/*---------------------------  BEGIN * inbox message content  * --------------------------------*/
function messageInboxContent_shortcode()
{

    $data = ' <div data-widget="messageInbox" class="msgContent" data-type="content" style="display:none;"></div>';
    return $data;
}

add_shortcode('messageInboxContent', 'messageInboxContent_shortcode');
/*---------------------------  END * inbox message content  * --------------------------------*/


/*---------------------------  BEGIN * inbox paging  * --------------------------------*/
function messageInboxPaging_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-prevnextpage' => 'true',
            'data-rowsperpage' => '50',

        ), $atts);
    $data = '<div data-widget="messageInbox" data-type="paging" data-rowsperpage="' . $a['data-rowsperpage'] . '" data-prevnextpage="' . $a['data-prevnextpage'] . '"></div>';
    return $data;
}

add_shortcode('messageInboxPaging', 'messageInboxPaging_shortcode');
/*---------------------------  END * inbox paging  * --------------------------------*/


/*---------------------------  BEGIN * leaderboard gameFilter  * --------------------------------*/
function socialLeaderboardGameFilter_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'class' => ''
        ), $atts);

    $data = '<select data-widget="socialLeaderboard2" data-type="gameFilter" class="' . $a['class'] . '"></select>';
    return $data;
}

add_shortcode('socialLeaderboardGameFilter', 'socialLeaderboardGameFilter_shortcode');
/*---------------------------  END * leaderboard gameFilter * --------------------------------*/

/*---------------------------  BEGIN * leaderboard assetTypeFilter  * --------------------------------*/
function socialLeaderboardAssetTypeFilter_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'class' => ''
        ), $atts);

    $data = '<select data-widget="socialLeaderboard2" data-type="assetTypeFilter" class="' . $a['class'] . '"></select>';
    return $data;
}

add_shortcode('socialLeaderboardAssetTypeFilter', 'socialLeaderboardAssetTypeFilter_shortcode');

function socialLeaderboardAssetFilter_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'class' => ''
        ), $atts);

    $data = '<select data-widget="socialLeaderboard2" data-type="assetFilter" class="' . $a['class'] . '" style="display: none"></select>';
    return $data;
}

add_shortcode('socialLeaderboardAssetFilter', 'socialLeaderboardAssetFilter_shortcode');


/*---------------------------  END * leaderboard assetTypeFilter * --------------------------------*/

/*---------------------------  BEGIN * leaderboard periodFilter  * --------------------------------*/
function socialLeaderboardPeriodFilter_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'class' => ''
        ), $atts);

    $data = '<select data-widget="socialLeaderboard2" data-type="periodFilter" class="' . $a['class'] . '"></select>';
    return $data;
}

add_shortcode('socialLeaderboardPeriodFilter', 'socialLeaderboardPeriodFilter_shortcode');
/*---------------------------  END * leaderboard periodFilter * --------------------------------*/


/*---------------------------  BEGIN * leaderboard  * --------------------------------*/
function socialLeaderboardSearch_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-translate' => 'search',
            'class' => ''
        ), $atts);
    $data = '<button data-widget="socialLeaderboard2" data-type="search" data-translate="' . $a['data-translate'] . '" class="' . $a['class'] . '">Search</button>';
    return $data;
}

add_shortcode('socialLeaderboardSearch', 'socialLeaderboardSearch_shortcode');

/*---------------------------  END * leaderboard  * --------------------------------*/


/*---------------------------  BEGIN * leaderboard  * --------------------------------*/
function socialLeaderboardTable_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-rowsperpage' => '',
            'data-columns' => 'name,rank,hitRate,followers,trades,orderCopyButton',
            'class' => ''
        ), $atts);
    $data = '<table data-widget="socialLeaderboard2" data-type="table" data-rowsperpage="' . $a['data-rowsperpage'] . '" data-columns="' . $a['data-columns'] . '" id="tol-social-leaderboard-table" class="social-leader-board table table-responsive"></table>';
    return $data;
}

add_shortcode('socialLeaderboardTable', 'socialLeaderboardTable_shortcode');
/*---------------------------  END * leaderboard  * --------------------------------*/


/*---------------------------  BEGIN * pastexpiries options  * --------------------------------*/
function pastExpiriesOptions_shortcode($atts)
{

    $a = shortcode_atts(
        array(
            'class' => '',

        ), $atts);
    $data = '<select style="display: none;" data-widget="pastExpiriesOptions" class="' . $a['class'] . '"></select>';
    return $data;
}

add_shortcode('pastExpiriesOptions', 'pastExpiriesOptions_shortcode');
/*---------------------------  END * pastexpiries options  * --------------------------------*/

/*---------------------------  BEGIN * pastexpiries from to  * --------------------------------*/
function pastExpiriesFromTo_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'widget' => '',
            'class' => '',
        ), $atts);
    $data = '<input style="display: none;" type="text" data-widget="' . $a['widget'] . '" class="' . $a['class'] . '" />';
    return $data;
}

add_shortcode('pastExpiriesFromTo', 'pastExpiriesFromTo_shortcode');
/*---------------------------  END * pastexpiries from to  * --------------------------------*/

/*---------------------------  BEGIN * pastexpiries table  * --------------------------------*/
function pastExpiriesTable_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-translate-assets' => 'MyOptions_Assets',
            'data-translate-order-date' => 'MyOptions_OrderDate',
            'data-translate-expiry-rate' => 'closed_expiry_rate',
            'data-translate-menu-chart' => 'viewMenu_Chart',
            'class' => '',
        ), $atts);
    $data = '<table data-widget="pastExpiriesTable" class="' . $a['class'] . '">
            <thead>
            <tr>
                <th data-column="asset" data-translate="' . $a['data-translate-assets'] . '"></th>
                <th data-column="expiryDate" data-translate="' . $a['data-translate-order-date'] . '"></th>
                <th data-column="expiryRate" data-translate="' . $a['data-translate-expiry-rate'] . '"></th>
                <th data-column="chart" data-translate="' . $a['data-translate-menu-chart'] . '"></th>
            </tr>
            </thead>
        </table>';
    return $data;
}

add_shortcode('pastExpiriesTable', 'pastExpiriesTable_shortcode');
/*---------------------------  END * leaderboard  * --------------------------------*/


/*---------------------------  BEGIN * pastexpiries search  * --------------------------------*/
function pastExpiriesSearch_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-translate' => 'MyTrades_SearchBtn',
            'class' => '',
        ), $atts);
    $data = '<input style="display: none;" type="button" data-translate="' . $a['data-translate'] . '" data-widget="pastExpiriesSearchButton" class="' . $a['class'] . '"/>';
    return $data;
}

add_shortcode('pastExpiriesSearch', 'pastExpiriesSearch_shortcode');
/*---------------------------  END * pastexpiries search  * --------------------------------*/

/*---------------------------  BEGIN * pastexpiries paging  * --------------------------------*/
function pastExpiriesPaging_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-paginglimit' => '10',
            'class' => '',
        ), $atts);
    $data = '<div style="display: none;" data-widget="pastExpiriesPaging" data-paginglimit="' . $a['data-paginglimit'] . '" class="' . $a['class'] . '"></div>';
    return $data;
}

add_shortcode('pastExpiriesPaging', 'pastExpiriesPaging_shortcode');
/*---------------------------  END * pastexpiries paging  * --------------------------------*/


/*---------------------------  BEGIN * change password error  * --------------------------------*/
function changePasswordError_shortcode()
{

    $data = '<div data-widget="changePassword" data-type="error" style="display: none"></div>';
    return $data;
}

add_shortcode('changePasswordError', 'changePasswordError_shortcode');
/*---------------------------  END * change password error  * --------------------------------*/


/*---------------------------  BEGIN * change password   * --------------------------------*/
function changePassword_shortcode($atts)
{
    $atts = shortcode_atts(array(
        'datatype' => '',
        'dataisRequired' => 'true',
        'class' => '',
    ), $atts);
    extract($atts);

    $data = '<input type="password" data-widget="changePassword" data-type="' . $atts['datatype'] . '" data-isRequired="' . $atts['dataisRequired'] . '" class="' . $atts['class'] . '" />';
    return $data;
}

add_shortcode('changePassword', 'changePassword_shortcode');
/*---------------------------  END * change password   * --------------------------------*/


/*---------------------------  BEGIN * change password strength  * --------------------------------*/
function changePasswordPasswordStrength_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'class' => ''
        ), $atts);

    $data = '<div data-widget="changePassword" data-type="passwordStrength" class="' . $a['class'] . '"></div>';
    return $data;
}

add_shortcode('changePasswordPasswordStrength', 'changePasswordPasswordStrength_shortcode');
/*---------------------------  END * change password strength  * --------------------------------*/


/*---------------------------  BEGIN * change password submit * --------------------------------*/
function changePasswordSubmit_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-translate' => 'Button_Submit',
            'class' => ''
        ), $atts);
    $data = ' <input type="button" data-widget="changePassword" data-type="submit" data-translate="' . $a['data-translate'] . '" class="' . $a['class'] . '" />';
    return $data;
}

add_shortcode('changePasswordSubmit', 'changePasswordSubmit_shortcode');
/*---------------------------  END * change password  submit * --------------------------------*/

/*---------------------------  BEGIN * change password submit * --------------------------------*/
function myTradesSocialFilter_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'class' => '',
        ), $atts);
    $data = '<select data-widget="myTradesSocialFilter" style="display: none" class="' . $a['class'] . '"></select>';
    return $data;
}

add_shortcode('myTradesSocialFilter', 'myTradesSocialFilter_shortcode');

function myTradesGameFilter_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'class' => '',
        ), $atts);
    $data = '<select data-widget="myTradesGameFilter" class="' . $a['class'] . '"></select>';
    return $data;
}

add_shortcode('myTradesGameFilter', 'myTradesGameFilter_shortcode');
/*---------------------------  END * change password  submit * --------------------------------*/


/*---------------------------  BEGIN * user details error  * --------------------------------*/
function userDetailsError_shortcode()
{

    $data = '<div data-widget="userDetails" data-type="error" style="display: none"></div>';
    return $data;
}

add_shortcode('userDetailsError', 'userDetailsError_shortcode');
/*---------------------------  END * user details error  * --------------------------------*/


/*---------------------------  BEGIN * user details email * --------------------------------*/
function userDetailsEmail_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-regexpMessage' => 'Please enter a valid email address',
            'data-isRequired' => 'true',
            'class' => ''
        ), $atts);
    $data = '<input type="text" data-widget="userDetails" data-type="email" data-isRequired="' . $a['data-isRequired'] . '" data-regexpMessage="' . $a['data-regexpMessage'] . '" class="' . $a['class'] . '" disabled/>';
    return $data;
}

add_shortcode('userDetailsEmail', 'userDetailsEmail_shortcode');
/*---------------------------  END * user details  email * --------------------------------*/

/*---------------------------  BEGIN * user details title  * --------------------------------*/
function userDetailsTitle_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'class' => ''
        ), $atts);

    $data = '<select data-widget="userDetails" data-type="title" class="' . $a['class'] . '"></select>';
    return $data;
}

add_shortcode('userDetailsETitle', 'userDetailsTitle_shortcode');
/*---------------------------  END * user details  title  * --------------------------------*/

/*---------------------------  BEGIN * user details firstname * --------------------------------*/
function userDetailsFirstName_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-isRequired' => 'true',
            'data-minLength' => '3',
            'class' => ''
        ), $atts);
    $data = '<input type="text" data-widget="userDetails" data-type="firstName" data-isRequired="' . $a['data-isRequired'] . '" data-minLength="' . $a['data-minLength'] . '" class="' . $a['class'] . '" />';
    return $data;
}

add_shortcode('userDetailsFirstName', 'userDetailsFirstName_shortcode');
/*---------------------------  END * user details  firstname * --------------------------------*/

/*---------------------------  BEGIN * user details lastname * --------------------------------*/
function userDetailsLastName_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-isRequired' => 'true',
            'data-minLength' => '3',
            'class' => ''
        ), $atts);
    $data = '<input type="text" data-widget="userDetails" data-type="lastName" data-isRequired="' . $a['data-isRequired'] . '" data-minLength="' . $a['data-minLength'] . '" class="' . $a['class'] . '" />';
    return $data;
}

add_shortcode('userDetailsLastName', 'userDetailsLastName_shortcode');
/*---------------------------  END * user details  lastname * --------------------------------*/


/*---------------------------  BEGIN * user details CountryCode  * --------------------------------*/
function userDetailsCountryCode_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'isRequired' => 'true',
            'class' => '',
            'style' => ''
        ), $atts);

    $data = '<select style="'.$a['style'].'" data-widget="userDetails" data-type="countryCode" data-isRequired="' . $atts['isrequired'] . '" class="' . $a['class'] . '" ></select>';
    return $data;
}

add_shortcode('userDetailsCountryCode', 'userDetailsCountryCode_shortcode');
/*---------------------------  END * user details  CountryCode  * --------------------------------*/

/*---------------------------  BEGIN * user details Nationality  * --------------------------------*/
function userDetailsNationality_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'isRequired' => 'true',
            'class' => '',
            'style' => ''
        ), $atts);

    $data = '<select style="'.$a['style'].'" data-widget="userDetails" data-type="nationalityId" data-isRequired="' . $atts['isrequired'] . '" class="' . $a['class'] . '" ></select>';
    return $data;
}

add_shortcode('userDetailsNationality', 'userDetailsNationality_shortcode');
/*---------------------------  END * user details  Nationality  * --------------------------------*/

/*---------------------------  BEGIN * user details BirthDay  * --------------------------------*/
function userDetailsBirthDay_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'class' => '',
        ), $atts);

    $data = '<select data-widget="userDetails" data-type="birthDay" class="' . $a['class'] . '"></select>';
    return $data;
}

add_shortcode('userDetailsBirthDay', 'userDetailsBirthDay_shortcode');
/*---------------------------  END * user details  BirthDay  * --------------------------------*/

/*---------------------------  BEGIN * user details birthMonth  * --------------------------------*/
function userDetailsBirthMonth_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'class' => '',
        ), $atts);

    $data = '<select data-widget="userDetails" data-type="birthMonth" class="' . $a['class'] . '"></select>';
    return $data;
}

add_shortcode('userDetailsBirthMonth', 'userDetailsBirthMonth_shortcode');
/*---------------------------  END * user details  birthMonth  * --------------------------------*/

/*---------------------------  BEGIN * user details birthYear  * --------------------------------*/
function userDetailsBirthYear_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-regexpMessage' => 'Please enter your date of birth',
            'class' => '',
        ), $atts);
    $data = '<select data-widget="userDetails" data-type="birthYear" data-regexpMessage="Please enter your date of birth" class="' . $a['class'] . '"></select>';
    return $data;
}

add_shortcode('userDetailsBirthYear', 'userDetailsBirthYear_shortcode');
/*---------------------------  END * user details  birthYear  * --------------------------------*/


/*---------------------------  BEGIN * user details addressLine1  * --------------------------------*/
function userDetailsAddressLine1_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'class' => '',
        ), $atts);

    $data = '<input type="text" data-widget="userDetails" data-type="addressLine1" class="' . $a['class'] . '"/>';
    return $data;
}

add_shortcode('userDetailsAddressLine1', 'userDetailsAddressLine1_shortcode');
/*---------------------------  END * user details  addressLine1  * --------------------------------*/

/*---------------------------  BEGIN * user details addressLine2  * --------------------------------*/
function userDetailsAddressLine2_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'class' => '',
        ), $atts);

    $data = '<input type="text" data-widget="userDetails" data-type="addressLine2" class="' . $a['class'] . '"/>';
    return $data;
}

add_shortcode('userDetailsAddressLine2', 'userDetailsAddressLine2_shortcode');
/*---------------------------  END * user details  addressLine2  * --------------------------------*/

/*---------------------------  BEGIN * user details city  * --------------------------------*/
function userDetailsCity_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'class' => '',
        ), $atts);

    $data = '<input type="text" data-widget="userDetails" data-type="city" class="' . $a['class'] . '"/>';
    return $data;
}

add_shortcode('userDetailsCity', 'userDetailsCity_shortcode');
/*---------------------------  END * user details  city  * --------------------------------*/

/*---------------------------  BEGIN * user details countryPhoneCode  * --------------------------------*/
function userDetailsCountryPhoneCode_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'class' => '',
        ), $atts);

    $data = '<input type="text" data-widget="userDetails" data-type="countryPhoneCode" class="' . $a['class'] . '" />';
    return $data;
}

add_shortcode('userDetailsCountryPhoneCode', 'userDetailsCountryPhoneCode_shortcode');
/*---------------------------  END * user details  countryPhoneCode  * --------------------------------*/

/*---------------------------  BEGIN * user details areaPhoneCode  * --------------------------------*/
function userDetailsAreaPhoneCode_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'class' => '',
        ), $atts);

    $data = '<input type="text" data-widget="userDetails" data-type="areaPhoneCode" class="' . $a['class'] . '" />';
    return $data;
}

add_shortcode('userDetailsAreaPhoneCode', 'userDetailsAreaPhoneCode_shortcode');
/*---------------------------  END * user details  areaPhoneCode  * --------------------------------*/


/*---------------------------  BEGIN * user details phone  * --------------------------------*/
function userDetailsPhone_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-regexpMessage' => 'Please enter your Primary Phone',
            'class' => '',
        ), $atts);
    $data = '<input id="phone" data-decorate="flags" type="text" data-widget="userDetails" data-type="phone" data-regexpMessage="' . $a['data-regexpMessage'] . '" class="' . $a['class'] . '"/>';
    return $data;
}

add_shortcode('userDetailsPhone', 'userDetailsPhone_shortcode');
/*---------------------------  END * user details  phone  * --------------------------------*/

/*---------------------------  BEGIN * user details secondary phone  * --------------------------------*/
function userDetailsSecondaryPhone_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-regexpMessage' => 'Please enter your Secondary phone/Mobile',
            'class' => '',
        ), $atts);
    $data = '<input id="phone-2"  type="text" data-widget="userDetails" data-type="secondaryPhone" data-regexpMessage="' . $a['data-regexpMessage'] . '" class="' . $a['class'] . '"/>';
    return $data;
}

add_shortcode('userDetailsSecondaryPhone', 'userDetailsSecondaryPhone_shortcode');
/*---------------------------  END * user details  secondary phone  * --------------------------------*/


/*---------------------------  BEGIN * user details googleLoginBtn  * --------------------------------*/
function userDetailsGoogleLoginBtn_shortcode()
{

    $data = '<div data-widget="userDetails" data-type="googleLoginBtn"></div>';
    return $data;
}

add_shortcode('userDetailsGoogleLoginBtn', 'userDetailsGoogleLoginBtn_shortcode');
/*---------------------------  END * user details  googleLoginBtn  * --------------------------------*/

/*---------------------------  BEGIN * user details fbLoginBtn  * --------------------------------*/
function userDetailsFbLoginBtn_shortcode()
{

    $data = '<div data-widget="userDetails" data-type="fbLoginBtn"></div>';
    return $data;
}

add_shortcode('userDetailsFbLoginBtn', 'userDetailsFbLoginBtn_shortcode');
/*---------------------------  END * user details  fbLoginBtn  * --------------------------------*/


/*---------------------------  BEGIN * user details qqLoginBtn  * --------------------------------*/
function userDetailsQqLoginBtn_shortcode()
{

    $data = '<div data-widget="userDetails" data-type="qqLoginBtn"></div>';
    return $data;
}

add_shortcode('userDetailsQqLoginBtn', 'userDetailsQqLoginBtn_shortcode');
/*---------------------------  END * user details  qqLoginBtn  * --------------------------------*/


/*---------------------------  BEGIN * user details fb login error  * --------------------------------*/
function userDetailsFbLoginErrorMessage_shortcode()
{

    $data = '<div data-widget="userDetails" data-type="fbLoginErrorMessage"></div>';
    return $data;
}

add_shortcode('userDetailsFbLoginErrorMessage', 'userDetailsFbLoginErrorMessage_shortcode');
/*---------------------------  END * user details  fb login error  * --------------------------------*/


/*---------------------------  BEGIN * user details google login error  * --------------------------------*/
function userDetailsGoogleLoginErrorMessage_shortcode()
{

    $data = '<div data-widget="userDetails" data-type="googleLoginErrorMessage"></div>';
    return $data;
}

add_shortcode('userDetailsGoogleLoginErrorMessage', 'userDetailsGoogleLoginErrorMessage_shortcode');
/*---------------------------  END * user details  google login error  * --------------------------------*/

/*---------------------------  BEGIN * user details qq login error  * --------------------------------*/
function userDetailsQqLoginErrorMessage_shortcode()
{

    $data = '<div data-widget="userDetails" data-type="qqLoginErrorMessage"></div>';
    return $data;
}

add_shortcode('userDetailsQqLoginErrorMessage', 'userDetailsQqLoginErrorMessage_shortcode');
/*---------------------------  END * user details  qq login error  * --------------------------------*/


/*---------------------------  BEGIN * user details submit  * --------------------------------*/
function userDetailsSubmit_shortcode($atts)
{

    $a = shortcode_atts(
        array(
            'data-translate' => 'MyProfile_Submit',
            'class' => ''
        ), $atts);

    $data = '<input type="button" data-widget="userDetails" data-type="submit" data-translate="' . $a['data-translate'] . '" class="' . $a['class'] . '" />';
    return $data;
}

add_shortcode('userDetailsSubmit', 'userDetailsSubmit_shortcode');
/*---------------------------  END * user details  submit  * --------------------------------*/


/*---------------------------  BEGIN * registration error  * --------------------------------*/
function registrationError_shortcode()
{

    $data = '<div data-widget="registration" data-type="error" style="display: none" class="tos-error"></div>';
    return $data;
}

add_shortcode('registrationError', 'registrationError_shortcode');
/*---------------------------  END * registration error  * --------------------------------*/


/*---------------------------  BEGIN * registration email * --------------------------------*/
function registrationEmail_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-regexpMessage' => 'Please enter a valid email address',
            'data-isRequired' => 'true',
            'data-translate' => 'Registration_Email',
            'class' => ''
        ), $atts);
    $data = '<input type="text"   placeholder="" data-translate="' . $a['data-translate'] . '" data-widget="registration" data-type="email" data-isRequired="' . $a['data-isRequired'] . '" data-regexpMessage="' . $a['data-regexpMessage'] . '" class="' . $a['class'] . '" placeholder="Email"/>';
    return $data;
}

add_shortcode('registrationEmail', 'registrationEmail_shortcode');
/*---------------------------  END * registration  email * --------------------------------*/


/*---------------------------  BEGIN * registration title  * --------------------------------*/
function registrationTitle_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'class' => ''
        ), $atts);

    $data = '<select data-widget="registration" data-type="title" class="' . $a['class'] . '"></select>';
    return $data;
}

add_shortcode('registrationTitle', 'registrationTitle_shortcode');
/*---------------------------  END * registration  title  * --------------------------------*/

/*---------------------------  BEGIN * registration firstname * --------------------------------*/
function registrationFirstName_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-isRequired' => 'true',
            'data-minLength' => '3',
            'data-translate' => 'Registration_FirstName',
            'class' => ''
        ), $atts);
    $data = '<input type="text" placeholder="" data-translate="Registration_FirstName"  data-widget="registration" data-type="firstName" data-isRequired="true" data-minLength="3" class="' . $a['class'] . '" />';
    return $data;
}

add_shortcode('registrationFirstName', 'registrationFirstName_shortcode');
/*---------------------------  END * registration  firstname * --------------------------------*/

/*---------------------------  BEGIN * registration lastname * --------------------------------*/
function registrationLastName_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-isRequired' => 'true',
            'data-minLength' => '3',
            'data-translate' => 'Registration_LastName',
            'class' => ''
        ), $atts);
    $data = '<input type="text"   placeholder="" data-translate="Registration_LastName"  data-widget="registration" data-type="lastName" data-isRequired="true" data-minLength="3" class="' . $a['class'] . '" />';
    return $data;
}

add_shortcode('registrationLastName', 'registrationLastName_shortcode');
/*---------------------------  END * registration  lastname * --------------------------------*/


/*---------------------------  BEGIN * registration CountryCode  * --------------------------------*/
function registrationCountryCode_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'isrequired' => 'true',
            'class' => '',
            'style' => ''
        ), $atts);
    $data = '<select data-widget="registration" data-type="countryCode" data-isRequired="true" class="' . $a['class'] . '" style="'.$a['style'].'" ></select>';
    return $data;
}

add_shortcode('registrationCountryCode', 'registrationCountryCode_shortcode');
/*---------------------------  END * registration  CountryCode  * --------------------------------*/

/*---------------------------  BEGIN * registration Nationality  * --------------------------------*/
function registrationNationality_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'isrequired' => 'true',
            'class' => '',
            'style' => ''
        ), $atts);
    $data = '<select data-widget="registration" data-type="nationalityId" data-isRequired="true" class="' . $a['class'] . '" style="'.$a['style'].'" ></select>';
    return $data;
}

add_shortcode('registrationNationality', 'registrationNationality_shortcode');
/*---------------------------  END * registration  Nationality  * --------------------------------*/

/*---------------------------  BEGIN * registration BirthDay  * --------------------------------*/
function registrationBirthDay_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'class' => '',
            'style' => '',
            'isrequired' => 'true'
        ), $atts);

    $data = '<select data-isrequired="'.$a['isrequired'].'" data-widget="registration" data-type="birthDay" class="' . $a['class'] . '"></select>';
    return $data;
}

add_shortcode('registrationBirthDay', 'registrationBirthDay_shortcode');
/*---------------------------  END * registration  BirthDay  * --------------------------------*/

/*---------------------------  BEGIN * registration birthMonth  * --------------------------------*/
function registrationBirthMonth_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'class' => ''
        ), $atts);

    $data = '<select data-widget="registration" data-type="birthMonth" class="' . $a['class'] . '"></select>';
    return $data;
}

add_shortcode('registrationBirthMonth', 'registrationBirthMonth_shortcode');
/*---------------------------  END * registration  birthMonth  * --------------------------------*/

/*---------------------------  BEGIN * registration birthYear  * --------------------------------*/
function registrationBirthYear_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-regexpMessage' => 'Please enter your date of birth',
            'class' => ''
        ), $atts);
    $data = '<select data-widget="registration" data-regexpMessage="Please enter your date of birth" data-type="birthYear" class="' . $a['class'] . '"></select>';
    return $data;
}

add_shortcode('registrationBirthYear', 'registrationBirthYear_shortcode');
/*---------------------------  END * registration  birthYear  * --------------------------------*/


/*---------------------------  BEGIN * registration addressLine1  * --------------------------------*/
function registrationAddressLine1_shortcode()
{

    $data = '<input type="text" data-widget="registration" data-type="addressLine1"/>';
    return $data;
}

add_shortcode('registrationAddressLine1', 'registrationAddressLine1_shortcode');
/*---------------------------  END * registration  addressLine1  * --------------------------------*/

/*---------------------------  BEGIN * registration addressLine2  * --------------------------------*/
function registrationAddressLine2_shortcode()
{

    $data = '<input type="text" data-widget="registration" data-type="addressLine2"/>';
    return $data;
}

add_shortcode('registrationAddressLine2', 'registrationAddressLine2_shortcode');
/*---------------------------  END * registration  addressLine2  * --------------------------------*/

/*---------------------------  BEGIN * registration city  * --------------------------------*/
function registrationCity_shortcode()
{

    $data = '<input type="text" data-widget="registration" data-type="city"/>';
    return $data;
}

add_shortcode('registrationCity', 'registrationCity_shortcode');
/*---------------------------  END * registration  city  * --------------------------------*/

/*---------------------------  BEGIN * registration countryPhoneCode  * --------------------------------*/
function registrationCountryPhoneCode_shortcode()
{

    $data = '<input id="country" type="text" data-widget="registration" data-type="countryPhoneCode" data-translate="qS_countryCode" placeholder="" class="widgetPlaceholder phoneCountryCode form-control" value="'.sanitize_text_field(@$_GET['countryPhoneCode']).'" />';
    return $data;
}

add_shortcode('registrationCountryPhoneCode', 'registrationCountryPhoneCode_shortcode');
/*---------------------------  END * registration  countryPhoneCode  * --------------------------------*/

/*---------------------------  BEGIN * registration areaPhoneCode  * --------------------------------*/
function registrationAreaPhoneCode_shortcode()
{

    $data = '<input id="code" type="text" data-widget="registration" data-type="areaCode" data-translate="qS_countryArea" placeholder="" class="widgetPlaceholder phoneAreaCode form-control" value="'.sanitize_text_field(@$_GET['areaCode']).'" />';
    return $data;
}

add_shortcode('registrationAreaPhoneCode', 'registrationAreaPhoneCode_shortcode');
/*---------------------------  END * registration  areaPhoneCode  * --------------------------------*/


/*---------------------------  BEGIN * registration phone  * --------------------------------*/
function registrationPhone_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-regexpMessage' => 'Please enter your Primary Phone',
            'data-minLength' => '5',
            'decorate' => 'maskPhone',
            'placeholder' => '+_(___)___-____',
            'isrequired' => 'true',
            'class' => ''
        ), $atts);

    ob_start();
    ?>
    <input id="phone" type="text" data-decorate="<?php echo $a['decorate']; ?>" data-widget="registration" data-type="phone" data-regexpMessage="Please enter your Primary Phone" data-isRequired="true" class="phone form-control" />
    <?php
    return ob_get_clean();
}

add_shortcode('registrationPhone', 'registrationPhone_shortcode');

/*---------------------------  END * registration  phone  * -------------------------------*/


/*---------------------------  BEGIN * registration areaPhoneCode  * --------------------------------*/
function quickRegistrationPhone_shortcode()
{

    $data = '<input id="phone" type="text" data-widget="registration" data-type="phone" data-translate="Quick_SignUp_Phone" placeholder="" data-isRequired="true" data-minLength="6" data-quick="true" class="widgetPlaceholder phone form-control"/>';
    return $data;
}

add_shortcode('quickRegistrationPhone', 'quickRegistrationPhone_shortcode');
/*---------------------------  END * registration  areaPhoneCode  * --------------------------------*/



/*---------------------------  BEGIN * registration secondary phone  * --------------------------------*/
function registrationSecondaryPhone_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-regexpMessage' => 'Please enter your Secondary phone/Mobile',
            'class' => ''
        ), $atts);
    $data = '<input id="phone-2" placeholder="Second Phone" type="text" data-widget="registration" data-type="secondaryPhone" data-regexpMessage="Please enter your Secondary phone/Mobile" class="' . $a['class'] . '"/>';
    return $data;
}

add_shortcode('registrationSecondaryPhone', 'registrationSecondaryPhone_shortcode');
/*---------------------------  END * registration  secondary phone  * --------------------------------*/


/*---------------------------  BEGIN * registration id expiry date  * --------------------------------*/
function registrationidExpiryDate_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-regexpMessage' => 'Please enter your ID/Passport Number Expire Date',
            'class' => ''
        ), $atts);
    $data = '<input id="idExpiryDate" placeholder="DD-MM-YYYY" type="text" maxlength="10" data-widget="registration" data-type="IDExpiryDate" data-regexpMessage="Please enter your ID/Passport Number Expire Date" class="' . $a['class'] . '"/><input type="checkbox" data-widget="registration" data-type="doesNotExpire" id="doesNotExpire" class="doesNotExpire" /><label>Does not Expire</label>';
    return $data;
}

add_shortcode('registrationidExpiryDate', 'registrationidExpiryDate_shortcode');
/*---------------------------  END * registration id expiry date   * --------------------------------*/

/*---------------------------  BEGIN * registration id expiry date  * --------------------------------*/
function registrationidIssuer_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-regexpMessage' => 'Please enter your ID/Passport Number Expire Date',
            'class' => ''
        ), $atts);
    $data = '<input id="idIssuer" placeholder="ID Issuer" type="text" data-widget="registration" data-type="IDIssuer" data-regexpMessage="Please enter your ID/Passport Number Issuer" class="' . $a['class'] . '"/>';
    return $data;
}

add_shortcode('registrationidIssuer', 'registrationidIssuer_shortcode');
/*---------------------------  END * registration id expiry date   * --------------------------------*/


/*---------------------------  BEGIN * registration submit  * --------------------------------*/
function registrationSubmit_shortcode($atts)
{

    $a = shortcode_atts(
        array(
            'data-translate' => 'quickRegistrationSubmit',
            'class' => ''
        ), $atts);

    $data = '<input type="button" data-widget="registration" data-type="submit" data-translate="' . $a['data-translate'] . '" class="' . $a['class'] . '" />';
    return $data;
}

add_shortcode('registrationSubmit', 'registrationSubmit_shortcode');
/*---------------------------  END * registration  submit  * --------------------------------*/

/* --------------------------- BEGIN * registration password * -------------------------------- */

function registrationPassword_shortcode($atts) {

    $a = shortcode_atts(
            array(
        'data-regexpMessage' => 'The password should contain at least one uppercase character',
        'data-isRequired' => 'true',
        'data-minLength' => '5',
        'translate' => 'Registration_Password',
        'class' => '',
        'placeholder' => ''
            ), $atts);

    $data = '<input type="password" placeholder="" data-translate="Registration_Password" data-widget="registration" data-type="password" data-minLength="6" data-isRequired="true" data-regexpMessage="The password should contain at least one uppercase character" class="' . $a['class'] . '" />';
    return $data;
}

add_shortcode('registrationPassword', 'registrationPassword_shortcode');
/* --------------------------- END * registration password * -------------------------------- */


/*---------------------------  BEGIN * registration confirmPassword  * --------------------------------*/
function registrationConfirmPassword_shortcode($atts)
{

    $a = shortcode_atts(
        array(
            'data-isRequired' => 'true',
            'data-translate' => 'Registration_Password2',
            'class' => ''
        ), $atts);

    $data = '<input type="password" placeholder="" data-translate="Registration_Password2" data-widget="registration" data-type="confirmPassword" data-isRequired="true" class="' . $a['class'] . '" />';
    return $data;
}

add_shortcode('registrationConfirmPassword', 'registrationConfirmPassword_shortcode');
/*---------------------------  END * registration  confirmPassword  * --------------------------------*/


/*---------------------------  BEGIN * registration passwordStrength  * --------------------------------*/
function registrationPasswordStrength_shortcode()
{


    $data = '<div data-widget="registration" data-type="passwordStrength" class="passwordStrength"></div>';
    return $data;
}

add_shortcode('registrationPasswordStrength', 'registrationPasswordStrength_shortcode');
/*---------------------------  END * registration  passwordStrength  * --------------------------------*/


/*---------------------------  BEGIN * registration languageCode  * --------------------------------*/
function registrationLanguageCode_shortcode($atts)
{

    $a = shortcode_atts(
        array(
            'data-isRequired' => 'true',
            'class' => ''
        ), $atts);

    $data = '<select data-widget="registration" data-type="languageCode" data-isRequired="true" class="' . $a['class'] . '"></select>';
    return $data;
}

add_shortcode('registrationLanguageCode', 'registrationLanguageCode_shortcode');
/*---------------------------  END * registration  languageCode  * --------------------------------*/


/*---------------------------  BEGIN * registration currencyCode  * --------------------------------*/
function registrationCurrencyCode_shortcode($atts)
{

    $a = shortcode_atts(
        array(
            'data-isRequired' => 'true',
            'class' => ''
        ), $atts);

    $data = '<select data-widget="registration" data-type="currencyCode" data-isRequired="' . $a['data-isRequired'] . '" class="' . $a['class'] . '"></select>';
    return $data;
}

add_shortcode('registrationCurrencyCode', 'registrationCurrencyCode_shortcode');
/*---------------------------  END * registration  currencyCode  * --------------------------------*/

/*---------------------------  BEGIN * registration agentName  * --------------------------------*/
function registrationAgentName_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-isRequired' => 'false',
            'data-minLength' => '3',
            'data-translate' => 'Registration_AgentName',
            'class' => ''
        ), $atts);
    $data = '<input type="text"   placeholder="" data-translate="' . $a['data-translate'] . '"  data-widget="registration" data-type="agentName" data-isRequired="' . $a['data-isRequired'] . '" data-minLength="' . $a['data-minLength'] . '" class="' . $a['class'] . '" />';
    return $data;
}

add_shortcode('registrationAgentName', 'registrationAgentName_shortcode');
/*---------------------------  END * registration  agentName  * --------------------------------*/

/*---------------------------  BEGIN * registration agentEmail  * --------------------------------*/
function registrationAgentEmail_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-isRequired' => 'false',
            'data-minLength' => '3',
            'data-translate' => 'Registration_AgentEmail',
            'class' => ''
        ), $atts);
    $data = '<input type="email" placeholder="" data-translate="' . $a['data-translate'] . '"  data-widget="registration" data-type="agentEmail" data-minLength="' . $a['data-minLength'] . '" class="' . $a['class'] . '" />';
    return $data;
}

add_shortcode('registrationAgentEmail', 'registrationAgentEmail_shortcode');
/*---------------------------  END * registration  agentEmail  * --------------------------------*/


/*---------------------------  BEGIN * registration subscribeToPromoSms  * --------------------------------*/
function registrationSubscribeToPromoSms_shortcode()
{


    $data = '<input type="checkbox" data-widget="registration" data-type="subscribeToPromoSms" />';
    return $data;
}

add_shortcode('registrationSubscribeToPromoSms', 'registrationSubscribeToPromoSms_shortcode');
/*---------------------------  END * registration  subscribeToPromoSms  * --------------------------------*/


/*---------------------------  BEGIN * registration subscribeToPromoEmail  * --------------------------------*/
function registrationSubscribeToPromoEmail_shortcode()
{


    $data = '<input type="checkbox" data-widget="registration" data-type="subscribeToPromoEmail" />';
    return $data;
}

add_shortcode('registrationSubscribeToPromoEmail', 'registrationSubscribeToPromoEmail_shortcode');
/*---------------------------  END * registration  subscribeToPromoEmail  * --------------------------------*/


/*---------------------------  BEGIN * registration TERMS AND CONDITIONS  * --------------------------------*/
function registrationTos_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-isRequired' => 'true',
            'id' => '',
        ), $atts);


    $data = '<input id="'.$a['id'].'" type="checkbox" data-widget="registration" data-type="tos"  data-isRequired="' . $a['data-isRequired'] . '"/>';
    return $data;
}

add_shortcode('registrationTos', 'registrationTos_shortcode');
/*---------------------------  END * registration  TERMS AND CONDITIONS  * --------------------------------*/

/*---------------------------  BEGIN * Cashier Iframe Holder  * --------------------------------*/
function cashierIframeHolder_shortcode()
{

    $data = '<div class="iframeHolder" data-widget="cashierDeposit" data-type="iframeHolder" style="display: none"></div>';
    return $data;
}

add_shortcode('cashierIframeHolder', 'cashierIframeHolder_shortcode');
/*---------------------------  END * Cashier Iframe Holder  * --------------------------------*/


/*---------------------------  Time Widget Start  * --------------------------------*/
function timeWidget_shortcode($atts) {

    $a = shortcode_atts(array(
        'data-type' => 'timeUTC',
        'class' => ''
    ),$atts);

    ob_start();
    ?>
    <div class="currentDateTimeWrapper <?php echo $a['class'] ?>">
        <span data-widget="currentDateTime" data-type="date" class="widgetPlaceholder clock-date"></span>
        <span data-widget="currentDateTime" data-type="time" class="widgetPlaceholder currentTimeWidget"></span>
        <span class="utctime">(<span data-widget="currentDateTime" data-type="<?php echo $a['data-type'] ?>" class="widgetPlaceholder currentTimeUtcWidget"></span>&nbsp;UTC)</span>
    </div>
    <?php
    return ob_get_clean();
}

add_shortcode('timeWidget', 'timeWidget_shortcode');
/*---------------------------  Time Widget End  * --------------------------------*/

/*---------------------------  Show off Start  * --------------------------------*/
function showOff_shortcode($atts) {
    $a = shortcode_atts(array(
        'class' => 'showOffPanel'
    ),$atts);

    ob_start();
    ?>
        <div class="<?php echo $a["class"] ?>">
            <ul data-widget="showOff" data-type="showOffList" ></ul>
        </div>
    <?php
    return ob_get_clean();
}
add_shortcode('showOff','showOff_shortcode');

/*---------------------------  Show off End  * --------------------------------*/

function getSiteName(){
	return get_bloginfo('name');
}
add_shortcode('sitename', 'getSiteName');

function getSupportEmail(){
	return SUPPORT_EMAIL;
}
add_shortcode('supportemail', 'getSupportEmail');


function getSiteUrl() {
    return get_site_url();
}

add_shortcode('getsiteurl','getSiteUrl' );

/*****************
 * myEasyForexTradesPaging.js
 *****************/
/*---------------------------  BEGIN * myEasyForexTradesPaging  * --------------------------------*/
function myEasyForexTradesPaging_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'paginglimit' => '5',
            'class' => 'my-trades-paging',
        ), $atts);
    // Code
    $data = '<div data-widget="easyForex-myEasyForexTradesPaging" data-pagingLimit="' . $a['paginglimit'] . '" class="' . $a['class'] . '"></div>';

    return $data;
}

add_shortcode('myEasyForexTradesPaging', 'myEasyForexTradesPaging_shortcode');
/*---------------------------  * END myEasyForexTradesPaging  * --------------------------------*/



/*****************
 * openTradesNewBinary.js
 *****************/
/*---------------------------  BEGIN * openTradesNewBinary * --------------------------------*/
function openTradesNewBinary_shortcode($atts) {
    //Attributes
    $a = shortcode_atts(
        array(
            'turbotime' => 'false',
            'turboslide' => 'false',
            'chart' => 'false',
            'column' => 'digital',
            'showToolsAsDialog' => 'true',
            'minrows' => '10',
        ), $atts);

    // Code
    return '<table data-widget="openTradesNewBinary" id="openTradesTable" data-offsetremove="true" data-turbotime="' . $a['turbotime'] . '" data-turboslide="' . $a['turboslide'] . '" data-chart="' . $a['chart'] . '" data-column="' . $a['column'] . '" data-showToolsAsDialog="' . $a['showToolsAsDialog'] . '" data-minrows="0">
        <thead>
            <tr>
                <th data-column="tradeId" data-sort="string" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="tradeID"></span></div>
                </th>
                <th data-column="created" data-sort="int" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="entryDate"></span></div>
                </th>
                <th data-column="type" data-sort="int" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="type"></span></div>
                </th>
                <th data-column="name" data-sort="string" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="asset"></span></div>
                </th>
                <th data-column="invested" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="invest"></span></div>
                </th>
                <th data-column="action" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="action"></span></div>
                </th>
                <th data-column="strike" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="strike"></span></div>
                </th>
                <th data-column="expires" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="expires"></span></div>
                </th>
                <th data-column="payoutpercent" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="payout"></span></div>
                </th>
                <th data-column="payout" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="return"></span></div>
                </th>
                <th data-column="profit" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="profit"></span></div>
                </th>
                <th data-column="actions" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="tool"></span></div>
                </th>
            </tr>
        </thead>
    </table>';
}

add_shortcode('openTradesNewBinary', 'openTradesNewBinary_shortcode');
/*---------------------------  * END openTradesNewBinary * --------------------------------*/
/*---------------------------  BEGIN * myBinaryTradesTable  * --------------------------------*/
function myBinaryTradesTable_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-rowsperpage' => '1000',
            'class' => '',
			'data-enableemptyrows' => false,
        ), $atts);
    #todo rowsperpage as attribute
    // Code
	$data ='<table data-widget="myTradesTable" id="closedTradesTable" data-offsetremove="true" data-column="' . $a['column'] . '"  data-minrows="0" data-enableemptyrows="false"  data-rowsperpage="1000">
        <thead>
            <tr>
                <th data-column="id" data-sort="string" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="tradeID"></span></div>
                </th>
                <th data-column="created" data-sort="int" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="entryDate"></span></div>
                </th>
                <th data-column="type" data-sort="int" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="type"></span></div>
                </th>
                <th data-column="name" data-sort="string" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="asset"></span></div>
                </th>
                <th data-column="invested" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="invest"></span></div>
                </th>
                <th data-column="action" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="action"></span></div>
                </th>
                <th data-column="rate" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="strike"></span></div>
                </th>
                <th data-column="expired" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="Expiry_Date"></span></div>
                </th>
				<th data-column="expiryRate" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="MyTrades_closeRate"></span></div>
                </th>
                <th data-column="payout" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="payout"></span></div>
                </th>
                <th data-column="profit" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="profit"></span></div>
                </th>
            </tr>
        </thead>
    </table>';
    return $data;
}

add_shortcode('myBinaryTradesTable', 'myBinaryTradesTable_shortcode');
/*---------------------------  * END myBinaryTradesTable  * --------------------------------*/

/*---------------------------  BEGIN * myTradesTableNewBinary  * --------------------------------*/
function myTradesTableNewBinary_shortcode($atts)
{
	
    $a = shortcode_atts(
        array(
            'data-rowsperpage' => '10',
            'class' => '',
			'data-enableemptyrows' => true,
        ), $atts);
    #todo rowsperpage as attribute
    // Code
	$data ='<table data-widget="myTradesTableNew" data-type="binary" id="closedTradesTable" data-offsetremove="true" data-column="' . $a['column'] . '"  data-minrows="10" data-enableemptyrows="true"  data-rowsperpage="10">
        <thead>
            <tr>
                <th data-column="id" data-sort="string" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="tradeID"></span></div>
                </th>
                <th data-column="created" data-sort="int" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="entryDate"></span></div>
                </th>
                <th data-column="asset" data-sort="string" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="asset"></span></div>
                </th>
				<th data-column="action" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="action"></span></div>
                </th>
				 <th data-column="rate" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="strike"></span></div>
                </th>
                <th data-column="invested" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="invest"></span></div>
                </th>
                <th data-column="expired" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="Expiry_Date"></span></div>
                </th>
				<th data-column="closeRate" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="MyTrades_closeRate"></span></div>
                </th>
                <th data-column="profit" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="profit"></span></div>
                </th>
            </tr>
        </thead>
    </table>';
    return $data;
}

add_shortcode('myTradesTableNewBinary', 'myTradesTableNewBinary_shortcode');
/*---------------------------  * END myTradesTableNewBinary  * --------------------------------*/
/*****************
 * myTradesSearchButtonNew.js
 *****************/
/*---------------------------  BEGIN * myTradesSearchButtonNew  * --------------------------------*/
function myTradesSearchButtonNew_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'translate' => 'show',
            'class' => '',
            'showallgames' => 'false', //display close trades for all games true/false -- add in imput - data-showallgames="' . $a['showAllgames'] . '"
        ), $atts);
    // Code
    $data = '<input data-widget="myTradesSearchButtonNew" data-preselectedgame="all" id="myTradesSearchButton" type="button" data-showallgames="' . $a['showallgames'] . '" data-translate="' . $a['translate'] . '" class="' . $a['class'] . '">';
    return $data;
}

add_shortcode('myTradesSearchButtonNew', 'myTradesSearchButtonNew_shortcode');
/*---------------------------  * END myTradesSearchButtonNew  * --------------------------------*/

/*---------------------------  * START myTradesTableBinaryExchangeNew  * --------------------------------*/

function myTradesTableBinaryExchangeNew_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-rowsperpage' => '10',
            'class' => '',
        ), $atts);
    #todo rowsperpage as attribute
    // Code

    $data = '	<table data-widget="myTradesTableNew" data-type="exchange" id="exchangeTradesTable" data-offsetremove="true" data-column="' . $a['column'] . '"  data-minrows="10" data-enableemptyrows="true"  data-rowsperpage="10">
        <thead>
            <tr>
               <th data-column="tradeId" data-sort="string" class="name canSort canResize canDrag">
                              <div class="cellWrapper"><span class="content" data-translate="tradeID"></span></div>
                           </th>
                           <th data-column="timestamp" data-sort="int" class="name canSort canResize canDrag">
                              <div class="cellWrapper"><span class="content" data-translate="mytrades-timestamp"></span></div>
                           </th>
                           <th data-column="asset" data-sort="string" class="name canSort canResize canDrag">
                              <div class="cellWrapper"><span class="content" data-translate="asset"></span></div>
                           </th>
                           <th data-column="strike" data-sort="float" class="name canSort canResize canDrag">
                              <div class="cellWrapper"><span class="content" data-translate="mytrades-strike"></span></div>
                           </th>
                           <th data-column="expiry" data-sort="float" class="name canSort canResize canDrag">
                              <div class="cellWrapper"><span class="content" data-translate="mytrades-expiry"></span></div>
                           </th>
                           <th data-column="direction" data-sort="float" class="name canSort canResize canDrag">
                              <div class="cellWrapper"><span class="content" data-translate="mytrades-direction"></span></div>
                           </th>
                           <th data-column="lots" data-sort="float" class="name canSort canResize canDrag">
                              <div class="cellWrapper"><span class="content" data-translate="mytrades-lots"></span></div>
                           </th>
                           <th data-column="limitPrice" data-sort="float" class="name canSort canResize canDrag">
                              <div class="cellWrapper"><span class="content" data-translate="mytrades-price"></span></div>
                           </th>
                           <th data-column="rate" data-sort="float" class="name canSort canResize canDrag">
                              <div class="cellWrapper"><span class="content" data-translate="mytrades-rate"></span></div>
                           </th>
						   <th data-column="fee" data-sort="float" class="name canSort canResize canDrag">
                              <div class="cellWrapper"><span class="content" data-translate="mytrades-fee"></span></div>
                           </th>
						   <th data-column="pl" data-sort="float" class="name canSort canResize canDrag">
                              <div class="cellWrapper"><span class="content" data-translate="mytrades-pl"></span></div>
                           </th>
            </tr>
        </thead>
    </table>';
    return $data;
}

add_shortcode('myTradesTableBinaryExchangeNew', 'myTradesTableBinaryExchangeNew_shortcode');
/*---------------------------  * END myTradesTableBinaryExchangeNew  * --------------------------------*/


/*---------------------------  BEGIN * myTradesTableNewSimpleForex  * --------------------------------*/
function myTradesTableNewSimpleForex_shortcode($atts)
{
	
    $a = shortcode_atts(
        array(
            'data-rowsperpage' => '1000',
            'class' => '',
			'data-enableemptyrows' => true,
        ), $atts);
    #todo rowsperpage as attribute
    // Code
	$data ='<table data-widget="myTradesTableNew" data-type="simpleForex" id="simpleForexTradesTable" data-offsetremove="true" data-column="' . $a['column'] . '"  data-minrows="10" data-enableemptyrows="true"  data-rowsperpage="10">
        <thead>
            <tr>
				<th data-column="name" data-sort="string" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="asset"></span></div>
                </th>
                <th data-column="action" data-sort="int" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="type"></span></div>
                </th>
				<th data-column="created" data-sort="int" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="entryDate"></span></div>
                </th>
				<th data-column="rate" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="tradeRate"></span></div>
                </th>
				 <th data-column="invested" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="invest"></span></div>
                </th>
                <th data-column="leverage" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="leverage"></span></div>
                </th>
                <th data-column="stopLoss" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="stopLoss"></span></div>
                </th>
				<th data-column="takeProfit" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="takeProfit"></span></div>
                </th>
                <th data-column="expiryRate" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="expiryRate_closeRate"></span></div>
                </th>
				<th data-column="expired" data-sort="string" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="MyTrades_expiryDate"></span></div>
                </th>
				<th data-column="swap" data-sort="int" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="forexSwap"></span></div>
                </th>
                <th data-column="profit" data-sort="string" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="profit"></span></div>
                </th>
            </tr>
        </thead>
    </table>';
    return $data;
}

add_shortcode('myTradesTableNewSimpleForex', 'myTradesTableNewSimpleForex_shortcode');
/*---------------------------  * END myTradesTableNewSimpleForex  * --------------------------------*/

/*---------------------------  BEGIN * myTradesTableNewSimpex  * --------------------------------*/
function myTradesTableNewSimpex_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-rowsperpage' => '10',
            'class' => '',
        ), $atts);
    #todo rowsperpage as attribute
    // Code	 
    $data = '<table data-widget="myTradesTableNew" data-type="simplex" id="simplexTradesTable" data-offsetremove="true" data-column="' . $a['column'] . '"  data-minrows="0" data-enableemptyrows="true"  data-rowsperpage="10">
        <thead>
            <tr>
                <th data-column="tradeID" data-sort="int" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="tradeID">Trade ID</span></div>
                </th>
                <th data-column="openTime" data-sort="int" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="openTime">Open Time</span></div
                </th>
                <th data-column="asset" data-sort="string" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="asset">Asset</span></div>
                </th>
                <th data-column="type" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="type">Type</span></div>
                </th>
                <th data-column="openPrice" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="openPrice">Open Price</span></div>
                </th>
                <th data-column="investAmount" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="investAmount">Invest Amount</span></div>
                </th>
                <th data-column="risk" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="risk">Risk</span></div>
                </th>
                <th data-column="stopLoss" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="stopLoss">Stop Loss</span></div>
                </th>
                <th data-column="takeProfit" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="takeProfit">Take Profit</span></div>
                </th>
                <th data-column="closeTime" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="closeTime">Close Time</span></div>
                </th>
                <th data-column="closePrice" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="closePrice">Close Price</span></div>
                </th>
                <th data-column="swap" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="swap">Swap</span></div>
                </th>
                <th data-column="commissions" data-sort="float" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="commissions">Commissions</span></div>
                </th>
                <th data-column="return" data-sort="string" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="return">Return</span></div>
                </th>
                <th data-column="profit" data-sort="string" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" data-translate="profit">Profit</span></div>
                </th>
                <th data-column="actions" data-sort="string" class="name canSort canResize canDrag">
                    <div class="cellWrapper"><span class="content" ></span></div>
                </th>
            </tr>
        </thead></table>';
    return $data;
}

add_shortcode('myTradesTableNewSimpex', 'myTradesTableNewSimpex_shortcode');

/*---------------------------  END * myTradesTableNewSimpex  * --------------------------------*/
/*---------------------------  START * myTradesTableNewRealForex * --------------------------------*/
function myTradesTableNewRealForex_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'data-rowsperpage' => '10',
            'class' => '',
        ), $atts);
    #todo rowsperpage as attribute
    // Code
    $data = '<table data-widget="myTradesTableNew" data-type="realForex" id="realForexTradesTable" data-offsetremove="true" data-column="' . $a['column'] . '"  data-minrows="10" data-enableemptyrows="true"  data-rowsperpage="10">
        <thead>
            <tr>
                <th data-column="position" data-sort="int" class="name canSort canResize canDrag">
					<div class="cellWrapper"><span class="content" data-translate="positionId"></span></div>
				</th>
				<th data-column="id" data-sort="int" class="name canSort canResize canDrag">
					<div class="cellWrapper"><span class="content" data-translate="forexTable_id"></span></div>
				</th>
				<th data-column="name" data-sort="string" class="name canSort canResize canDrag">
					<div class="cellWrapper"><span class="content" data-translate="forexTable_asset"></span></div>
				</th>
				<th data-column="volume" data-sort="float" class="name canSort canResize canDrag">
					<div class="cellWrapper"><span class="content" data-translate="forexTable_quantity"></span></div>
				</th>
				<th data-column="action" data-sort="float" class="name canSort canResize canDrag">
					<div class="cellWrapper"><span class="content" data-translate="forexTable_direction"></span></div>
				</th>
				<th data-column="rate" data-sort="float" class="name canSort canResize canDrag">
					<div class="cellWrapper"><span class="content" data-translate="forexTable_rate"></span></div>
				</th>
				<th data-column="expiryRate" data-sort="float" class="name canSort canResize canDrag">
					<div class="cellWrapper"><span class="content" data-translate="expiryRate_closeRate"></span></div>
				</th>
				<th data-column="created" data-sort="float" class="name canSort canResize canDrag">
					<div class="cellWrapper"><span class="content" data-translate="forexTable_created"></span></div>
				</th>
				<th data-column="expired" data-sort="float" class="name canSort canResize canDrag">
					<div class="cellWrapper"><span class="content" data-translate="forexTable_expired"></span></div>
				</th>
				<th data-column="swap" data-sort="float" class="name canSort canResize canDrag">
					<div class="cellWrapper"><span class="content" data-translate="forexTable_swap"></span></div>
				</th>
				<th data-column="commission" data-sort="float" class="name canSort canResize canDrag">
					<div class="cellWrapper"><span class="content" data-translate="commissions"></span></div>
				</th>
				<th data-column="result" data-sort="float" class="name canSort canResize canDrag">
					<div class="cellWrapper"><span class="content" data-translate="forexTable_result"></span></div>
				</th>
				<th data-column="copied-from" data-sort="float" class="name canSort canResize canDrag">
					<div class="cellWrapper"><span class="content" data-translate="copiedFrom"></span></div>
				</th>
            </tr>
        </thead></table>';
    return $data;
}

add_shortcode('myTradesTableNewRealForex', 'myTradesTableNewRealForex_shortcode');
/*---------------------------  END * myTradesTableNewRealForex * --------------------------------*/

/*****************
 * myTradesPagingNew.js
 *****************/
/*---------------------------  BEGIN * myTradesPagingNew  * --------------------------------*/
function myTradesPagingNew_shortcode($atts)
{
    $a = shortcode_atts(
        array(
            'paginglimit' => '5',
            'class' => 'my-trades-paging',
			'prevNextPage' => true,
        ), $atts);
    // Code
    $data = '<div data-widget="myTradesPagingNew" data-pagingLimit="' . $a['paginglimit'] . '" class="' . $a['class'] . ' " data-prevNextPage="' . $a['prevNextPage'] . '"></div>';

    return $data;
}

add_shortcode('myTradesPagingNew', 'myTradesPagingNew_shortcode');
/*---------------------------  END * myTradesPagingNew  * --------------------------------*/
