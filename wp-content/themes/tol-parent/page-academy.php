<?php
get_header();
switch (ICL_LANGUAGE_CODE) {

    case "de":
        $language = "de";
        break;
    case "ja":
        $language = "jp";
        break;
    case "ar":
        $language = "ar";
        break;
    case "zh-hans":
        $language = "cn";
        break;
    case "it":
        $language = "it";
        break;
    case "pl":
        $language = "pl";
        break;
    case "pt-pt":
        $language = "pt";
        break;
    case "ru":
        $language = "ru";
        break;
    case "tr":
        $language = "tr";
        break;
    default:
        $language = "en";
}
?>

<div id="content" role="main">

    <div class="container<?php echo(NO_PADDING_ALL) ? '-fluid no-padding-all' : '' ?>">
        <?php if (NO_PADDING_ALL) { ?>
            <div class="col-sm-offset-1 col-sm-10 no-padding-all">
            <?php } ?>
            <script src='//www.mte-media.com/admin2/js/mte.js'></script>
            <?php if (BREADCRUMB) { ?>
                <div class="pageHeadTitle no-padding-all">
                    <?php require_once(ABSPATH.'wp-content/themes/tol-parent/layouts/'.BREADCRUMB.'.php' ); ?>
                </div>
            <?php } ?>
            <?php if (SHOW_TITLE) { ?>
                <header class="entry-header no-padding-all">
                    <h1 class="entry-title blue-heading"><?php the_title(); ?></h1>
                </header>
            <?php } ?>
            <script>

                function getCookie(cname) {
                    var name = cname + "=";
                    var ca = document.cookie.split(';');
                    for (var i = 0; i < ca.length; i++) {
                        var c = ca[i];
                        while (c.charAt(0) == ' ')
                            c = c.substring(1);
                        if (c.indexOf(name) == 0)
                            return c.substring(name.length, c.length);
                    }
                    return "";
                }


                if (getCookie('widgetSession')) {
                    var userRef = '<?php echo MTE_CLIENT_ID ?>', language = '<?php echo $language ?>', mte_idx = true, set = 'Default'
                    var products = ['vin', 'bbk', 'abin', 'eco']
                    $(function () {
                        $(document).on('MTE_IDX_READY', function () {
                            Idx = new Mte_idx(products, set);
                        });
                    })

                } else {
                    var userRef = '<?php echo MTE_CLIENT_ID ?>', language = '<?php echo $language ?>', mte_idx = true, set = 'Default'
                    var products = ['vin', 'bbk', 'abin', 'eco', 'dem']
                    $(function () {
                        $(document).on('MTE_IDX_READY', function () {
                            Idx = new Mte_idx(products, set);
                        });
                    })
                }

            </script>

            <section id='learning_center'></section>
        </div>
        <?php if (NO_PADDING_ALL) { ?>
        </div>
    <?php } ?>
</div><!-- #content -->

<?php
get_footer();
?>