<?php
/**
 * Created by PhpStorm.
 * User: SbWereWolf
 * Date: 2019-05-16
 * Time: 22:56
 */

const LAYOUT_PROCESSOR ='layout-processor';
function process_category($slug)
{
    include_once(ABSPATH.'configuration.php');
    defineMigescoLayout();
    ?>
    <?php
    get_header();

    global $paged, $wp_query, $wp;
    /* @var $wp_query WP_Query */
    /* @var $wp WP */
    $args = wp_parse_args($wp->matched_query);
    if (!empty ($args['paged']) && 0 == $paged) {
        $wp_query->set('paged', $args['paged']);
        $paged = $args['paged'];
    }
    ?>
    <div id="content" role="main">
        <?php
        $filePath = get_template_directory();
        $scriptExists = file_exists("$filePath/javascript/posts/layout-processor.js");
        if ($scriptExists) {
            $webPath = get_template_directory_uri();
            (new ResourceLoader($filePath, $webPath))->registerScriptWithFileTime(LAYOUT_PROCESSOR,
                "/javascript/posts/layout-processor.js");
        }

        $wpb_all_query = new WP_Query(array('post_type' => 'post',
            'category_name' => $slug,
            'post_status' => 'publish',
            'posts_per_page' => 5,
            'paged' => $paged));
        if ($wpb_all_query->have_posts()):
            $postFields = array();
            while ($wpb_all_query->have_posts()) :
                $wpb_all_query->the_post();
                $postId = get_the_ID();
                $postFields[$postId] = get_fields();
                ?>
                <article
                        class="post-format-standard hentry">
                    <header class="entry-header"><h1 class="entry-title"><?php the_title(); ?></h1></header>
                    <div class="entry-content" data-post-id="<?php echo $postId; ?>"><?php the_content(); ?></div>
                </article>
            <?php
            endwhile;
            wp_reset_postdata();
            wp_localize_script(LAYOUT_PROCESSOR, 'categoryPosts', $postFields);
        endif;
        ?>
    </div>
    <?php
    $maxNumberOfPages = 10;
    $pageNumberOfPageText = get_option('pagenavi_auto_page_translation') . " %u " . get_option('pagenavi_auto_of_translation') . " %u";
    $previousPage = "&lt;";
    $nextPage = "&gt;";
    $isFirstLastNumbers = true;
    $isThisFirstLastGap = true;
    $firstGap = "...";
    $lastGap = "...";

    $total_number_of_pages = $wpb_all_query->max_num_pages; // total number of pages in category
    if ($total_number_of_pages > 1) {  // only one page so no navigation is needed

        $current_page_number = (!empty($paged)) ? $paged : 1; // current page where we are on

        $min_page = $current_page_number - floor(intval($maxNumberOfPages) / 2); //The lowest page
        $maxNumberOfPages = (intval($maxNumberOfPages) - 1);
        if ($min_page < 1) $min_page = 1;
        $max_page = $min_page + $maxNumberOfPages; // The highest page
        if ($max_page > $total_number_of_pages) $max_page = $total_number_of_pages;
        if ($max_page == $total_number_of_pages && $max_page > $maxNumberOfPages) $min_page = ($max_page - $maxNumberOfPages); // changes min_page if max is last page now

        $pagingOutputString = "<ul class='page_navistyle'>"; //String to output

        // displays "Page x of y, based on the settrings from translation"
        $pagingOutputString .= sprintf("<li class='page_info'>" . $pageNumberOfPageText . "</li>", floor($current_page_number), floor($total_number_of_pages));

        // displays link to previous page number
        if ($current_page_number != 1)
            $pagingOutputString .= sprintf("<li><a href='%s'>%s</a></li>", "/category/$slug/page/" . ($current_page_number - 1), $previousPage);

        // displays page 1 links and ellipses further when min page is more than 1
        if ($min_page > 1) {
            if ($isFirstLastNumbers) $pagingOutputString .= sprintf("<li class='first_last_page'><a href='%s'>%u</a>", "/category/$slug/page/1", 1);
            if ($isThisFirstLastGap) $pagingOutputString .= sprintf("<li class='space'>%s</li>", $firstGap);
        }

        // displays lowest to highest page output
        for ($i = $min_page; $i <= $max_page; $i++)
            $pagingOutputString .= ($current_page_number == $i) ?
                sprintf("<li class='current'><span><a>%u</a></span></li>", $i) :
                sprintf("<li %s><a href='%s'>%u</a></li>", ($current_page_number == $i) ? "class='after'" : null, "/category/$slug/page/" . $i, $i);

        // displays total page link and ellipses when max page is lower than the total page
        if ($max_page < $total_number_of_pages) {
            if ($isThisFirstLastGap) $pagingOutputString .= sprintf("<li class='space'>%s</li>", $lastGap);
            if ($isFirstLastNumbers) $pagingOutputString .= sprintf("<li class='first_last_page'><a href='%s'>%u</a>", "/category/$slug/page/" . ($total_number_of_pages), $total_number_of_pages);
        }

        // displays link to next page
        if ($current_page_number != $total_number_of_pages)
            $pagingOutputString .= sprintf("<li><a href='%s'>%s</a></li>", "/category/$slug/page/" . ($current_page_number + 1), $nextPage);

        $pagingOutputString .= "</ul>";

        echo $pagingOutputString;

    }
    ?>
    <?php
    get_footer();
    ?>
    <?php
}
