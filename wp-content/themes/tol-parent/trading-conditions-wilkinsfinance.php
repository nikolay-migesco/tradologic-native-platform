<?php
/**
 *
 * Template Name: Wilkins Trading Conditions page
 *
 */
 get_header();
?>

<style>
.cms-page-content strong {
    font-weight: 800;
}
.cms-page-content h2:not(:first-child) {
    margin-top: 60px;
}
.breadcrumb {
    padding: 0;
    margin-bottom: 20px;
	line-height: 52px;
    list-style: none;
    background-color: transparent;
    max-width: 1150px;
    margin: auto;
}
.breadcrumbs__link {
    color: #fff !important;
}
.breadcrumbs__divider {
    margin: 0 10px;
    color: #3176FC;
    font-size: 13px;
    font-weight: 900;
}
.breadcrumb>.active {
    color: #fff;
}
.breadcrumb a{
   color: #fff !important;
}
.breadcrumbs__wrapper {
    height: 52px;
    background: #1a1d1f;
}
.page-content {
    padding-top: 140px;
    padding-bottom: 0;
    background-color: #111214;
}
.container.cfd-container {
	 padding-top: 30px;
}
.page-content.page-content-home {
    padding-top: 0
}

@media screen and (max-width: 767px) {
    .page-content {
        padding-top:50px;
        padding-bottom: 0
    }
}
.cfd-container {
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
}
.cfd-container, .cfd__main-text {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
}
.container.cfd-containe {
    height: 100%;
    margin: 0 auto;
    max-width: 1150px;
    position: relative;
    width: 100%;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
	padding: 0;
}
.sidebar {
    width: 230px;
	margin-right: 20px;
}
.sidebar__menu {
    background: #000;
    margin-bottom: 30px;
}
.sidebar__menu__header {
    display: block;
    height: 38px;
    line-height: 38px;
    padding-left: 30px;
    background: #1a1c1f;
    color: #3176FC;
    font-size: 15px;
    font-weight: 500;
	margin: 0;
}
.sidebar__menu__item {
    height: 38px;
    color: #fff;
    border-top: 1px solid #383d43;
    -webkit-transition: .1s;
    transition: .1s;
}
.sidebar__menu__item.active {
	color: #3176FC;
}
.sidebar__menu a {
    display: block;
    height: 38px;
    padding-left: 30px;
    line-height: 38px;
    font-size: 14px;
    text-decoration: none;
    color: inherit;
    font-family: Open Sans,Arial,Tahoma,sans-serif;
}
.cfd__main-text {
    font-family: Open Sans,Arial,Tahoma,sans-serif;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    max-width: 900px;
    width: 900px;
    padding: 6px;
    -webkit-box-flex: 1;
    -ms-flex-positive: 1;
    flex-grow: 1;
    -ms-flex-negative: 1;
    flex-shrink: 1;
}
h1 {
    font-size: 35px;
	margin: 0;
    margin-bottom: 30px;
    font-family: Open Sans,Arial,Tahoma,sans-serif;
    color: #3176FC;
	font-weight: 400;
}
h2 {
    font-size: 25px;
    color: #fff;
    margin: 0 0 25px 0;
	font-family: Open Sans,Arial,Tahoma,sans-serif;
	font-weight: 400;
}
.cms-page-content li, .cms-page-content p {
    margin-bottom: 15px;
    line-height: 22px;
    font-size: 15px;
    color: #fff;
	font-family: Open Sans,Arial,Tahoma,sans-serif;
}
ul, ol {
    list-style: none;
    padding-left: 0;
}
.sidebar__menu__item:hover {
    color: #3176FC;
}
.contactus__main-footer {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: nowrap;
    flex-wrap: nowrap;
}
.contactus__main-form {
    position: relative;
    max-width: 430px;
    -ms-flex-preferred-size: 100%;
    flex-basis: 100%;
    margin-right: 40px;
}
.contactus__main-form input{
    height: 40px;
    font-size: 14px;
    padding: 12px;
	width: 100%;
    border: 1px solid #ccc;
    color: #626262;
    font-weight: 400;
    background: #fff;
    -webkit-transition: border-color .2s ease-in,color .2s ease-in;
    transition: border-color .2s ease-in,color .2s ease-in;
}
.contactus__main-form input:focus,
.contactus__main-form input:hover {
    background-color: #e9f5fc;
}
.page-contact-us .text,
.page-contact-us p.text {
    margin-bottom: 15px;
    line-height: 22px;
    font-size: 15px;
    color: #fff;
}
.contactus__email, .contactus__tel {
    color: #fff;
}
.contactus__email:hover, .contactus__tel:hover {
    color: #3176FC;
	text-decoration: none;
}
.contactus__address-text {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    min-width: 170px;
}

.contactus__address-wrapper {
    color: #fff;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    line-height: 24px;
	font-size: 16px;
}
.contactus__adress-header {
    font-size: 20px;
}
.cms-page-content h2:not(:first-child) {
    margin-top: 60px;
}
div.cms-page-content h2:not(:first-child), h2:not(:first-child) {
    margin-top: 25px;
    margin-bottom: 15px;
}
.tol-subject-selector {
    float: left;
    font-size: 13px;
    height: 90px;
    line-height: 1.4;
    padding: 50px 10px 10px 10px;
    text-align: center;
    width: 25%;
    color: #b0b0b0;
    margin-bottom: 7%;
    cursor: pointer;
}
@media(max-width: 1024px){
	.breadcrumb {
		padding: 0 3%;
	}
	.container.cfd-container {
		width: 100%;
	}
}
@media(width: 1024px){
	.row.traderinstruments-wrapper {
		width: 1000px;
		margin-left: -197px;
	}
}
@media(width: 768px){
	.cfd__main-text {
		width: 490px;
	}
	.page-assets-index .trading-conditions-table .asset-row .asset-data .kid-icon {
		margin-left: -6px;
	}
	.row.traderinstruments-wrapper {
		width: 750px;
		margin-left: -260px;
	}
	.page-assets-index .trading-conditions-table .asset-row .asset-data .kid-icon .trading-hours-wrapper {
		right: 0px;
	}
	.page-assets-index .trading-conditions-head div, .page-assets-index .trading-conditions-fake-head div {
		text-align: right;
		font-size: 11px;
	}
}
@media screen and (max-width: 767px){
	.cfd-container {
		-webkit-box-orient: vertical;
		-webkit-box-direction: reverse;
		-ms-flex-direction: column-reverse;
		flex-direction: column-reverse;
		-ms-flex-line-pack: center;
		align-content: center;
		padding-left: 15px;
	}
	.sidebar {
		width: 100%;
		min-width: 230px;
	}
	.cfd__main-text {
		width: 100%;
	}
}
</style>

<div id="content" role="main">
	<div class="page-content" style="background-color:#111214;">
		<div class="breadcrumbs__wrapper">
			<?php require_once(ABSPATH.'wp-content/themes/tol-parent/layouts/breadcrumb.php' ); ?>
		</div>
		<div class="container cfd-container">
		
			<div class="sidebar">
				<div class="sidebar__menu">
				   <p class="sidebar__menu__header" data-translate="trading-tools-node-title"></p>
				   <ul>
					  <li class="sidebar__menu__item active"><a href="/trading-tools/assets-index"
                                                                data-translate="assets-index-title"></a></li>
					  <li class="sidebar__menu__item"><a href="/trading-tools/economic-calendar"
                                                         data-translate="economic-calendar-title"></a></li>
				   </ul>
				</div>
			</div>
			 <div class="cfd__main-text">
				<h1 data-translate="assets-index-title"></h1>
				<div class="cms-page-content" ><span data-translate="assets-index-content"></span>
			      <div class="row traderinstruments-wrapper">
            <div data-widget="forexSwapRates" class="forexswaprates">
                <div class="forexswaprates-game-menu">
                    <ul class="nav">
                        <li data-widget="forexSwapRates" data-type="tab" data-game="EasyForex" class="active" data-translate="game-name-easy"></li>
                        <li data-widget="forexSwapRates" data-type="tab" data-game="RealForex" data-translate="game-name-real"></li>
                    </ul>
                </div>
                <input data-widget="forexSwapRates" data-target="search" class="forexswaprates-menu-search" placeholder data-translate="asset-list-search"/>
                <div class="forexswaprates-menu">
                    <div data-widget="forexSwapRates" data-type="menu" data-target="all" data-translate="All" class="forexswaprates-menu-currencies active" ></div>
                    <div data-widget="forexSwapRates" data-type="menu" data-target="currencies" data-translate="Currencies" class="forexswaprates-menu-currencies"></div>
                    <div data-widget="forexSwapRates" data-type="menu" data-target="crypto" data-translate="CryptoCurrencies" class="forexswaprates-menu-crypto"></div>
                    <div data-widget="forexSwapRates" data-type="menu" data-target="indices" data-translate="Indices" class="forexswaprates-menu-indices"></div>
                    <div data-widget="forexSwapRates" data-type="menu" data-target="stocks" data-translate="Stocks" class="forexswaprates-menu-stocks"></div>
                    <div data-widget="forexSwapRates" data-type="menu" data-target="commodities" data-translate="Commodities" class="forexswaprates-menu-commodities"></div>
                </div>
                <div class="trading-conditions-table-container">
                    <div data-widget="forexSwapRates" data-type="loading" class="trading-conditions-loading"></div>
                    <div data-widget="forexSwapRates" data-type="fakeHead" class="trading-conditions-fake-head"></div>
                    <div class="scrollable-trading-conditions-table">
                        <div class="scrollable-area-body">
                            <div class="scrollable-area-content">
                                <div class="clearfix flat-top-borders">
                                    <div data-widget="forexSwapRates" data-type="table" class="trading-conditions-table"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="forexswaprates-export-container">
                    <div data-widget="forexSwapRates" data-type="export" data-file="pdf" class="forexswaprates-export-item exportPDF" data-translate="export-pdf"></div>
                    <div data-widget="forexSwapRates" data-type="export" data-file="xls" class="forexswaprates-export-item exportXLS" data-translate="export-xls"></div>
                </div>
            </div>
        </div>
		</div>
	</div>
</div>
</div>
</div>
<!-- #content -->
<script>
    $(document).ready(function () {
        $(window).on('tableIsLoaded', function(){
            $(".scrollable-trading-conditions-table").mCustomScrollbar("destroy");
            $(".scrollable-trading-conditions-table").mCustomScrollbar({
                scrollButtons: {
                    enable: false
                },
                theme: 'minimal-dark',
                horizontalScroll: false,
                autoDraggerLength: true,
                autoHideScrollbar: true,
                advanced: {
                    autoScrollOnFocus: false,
                    updateOnContentResize: true,
                    updateOnBrowserResize: true
                }
            });
        });
    });
</script>

<?php get_footer(); ?>
