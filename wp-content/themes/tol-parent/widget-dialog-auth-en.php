<?php //if(!TolWidgets::isUserLogged()):?>
<script type="text/javascript" src="/wp-content/themes/tol-parent/login.js?1"></script>
<script type="text/javascript">
    var loadingLogin;
    var loginCount = 0;

    $(window).on('loginIsStarted', function (event, response) {
        if (loginCount == 0) {
            $('#login-widget').attr('value', 'Signing In').attr('style', 'background-color:#69bb1b !important; border:1px solid #69bb1b !important;');
            $('#preload_login').css('right', '41px');
        } else {
            $('#login-widget').attr('value', 'Connection').attr('style', 'background-color:#ff7200 !important; border:1px solid #ff7200 !important;');
            $('#preload_login').css('right', '35px');
        }
        $('#preload_login').show();
        loadingLogin = setTimeout(function () {
            $('#preload_login').hide();
            if (loginCount == 3) {
                $('#login-widget').attr('value', 'Connection failed').attr('style', 'background-color:#DE0404 !important; border:1px solid #DE0404 !important;');
            } else {
                $('#login-widget').attr('value', 'Connection').attr('style', 'background-color:#ff7200 !important; border:1px solid #ff7200 !important;');
                $('#preload_login').css('right', '35px');
                loginCount++;
            }
            loginWidget.isPending = false;
            writeLoginLog('', 'Login Timeout [' + loginCount + '] Tradologic (20 sec.)');
            if (loginCount != 3) {
                $('#login-widget').click();
            }
            if (loginCount == 3) {
                $('#login-widget').attr('value', 'Connection failed').attr('style', 'background-color:#DE0404 !important; border:1px solid #DE0404 !important;');
                setTimeout(function () {
                    $('#login-widget').attr('value', 'Sign In').attr('style', 'background-color:#69bb1b !important');
                }, 1000);
                loginCount = 0;
            }
        }, 20000);
    });

    $(window).on('loginIsCompleted', function (event, response) {
        clearTimeout(loadingLogin);
        $('#preload_login').hide();
    });
</script>
<style type="text/css">
    #preload_login {
        position: absolute;
        top: 21px;
        right: 56px;
        width: 30px;
        height: 30px;
    }
</style>
<div class="login-wrap" style="display:none;">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td width="50%" id="login_col">
                <div class="box_title"><span>Sign in MyMigesco</span></div>
                <div id="login_form" data-widget="login" data-type="container" class="widgetPlaceholder">
                    <div class="form_input"><input type="text" placeholder="E-mail" id="username" data-widget="login"
                                                   data-type="username" class="widgetPlaceholder input"/></div>
                    <div class="form_input"><input type="password" placeholder="Password" id="password"
                                                   data-widget="login" data-type="password"
                                                   class="widgetPlaceholder input"/></div>
                    <div class="form_check"><input id="save_pass" type="checkbox" class="savePass"
                                                   checked="checked"/><label for="save_pass">Remember me</label></div>
                    <div style="height:44px !important;position:relative;clear:both;"><input type="button"
                                                                                             value="Sign in"
                                                                                             data-widget="login"
                                                                                             data-type="submit"
                                                                                             id="login-widget"/>
                        <div id="preload_login" style="display:none;"><img
                                    src="/wp-content/themes/tol-parent/images/xp-theme/load.gif" width="30" height="30">
                        </div>
                    </div>
                    <div style="clear:both;"></div>
                    <div style="height:20px;margin-top:3px !important;">
                        <!--    <div class="login-error-message error-message" data-type="error" style="display:none;"><span>E-mail or Password are incorrect</span></div>-->
                        <div data-widget="login" data-type="error" style="display: none"
                             class="widgetPlaceholder error-message"></div>
                        <div class="login-success-message success-message" style="display:none;"><span>You have been logged in...</span>
                        </div>
                    </div>
                    <div class="forgot-password"><a href="#resetpassword">Forgot your password?</a></div>
                </div>
            </td>
            <td width="50%" id="reg_col">
                <div class="box_title"><span>Open account</span></div>
                <div class="reg_text">Open account and get the chance to make money on the world financial markets</div>
                <div class="reg_button"><a href="#register">New account</a></div>
            </td>
        </tr>
    </table>
    <div id="close_popup"><span class="login-close"></span></div>
</div>
<?php //endif;?>
