<?php
if (!isUserLogged()):
    ?>
    <style type="text/css">
        #form_reg .preload_registration {
            position: absolute;
            top: 14px;
            left: 30px;
            width: 30px;
            height: 30px;
        }
    </style>

    <!-- begin widget -->
    <div id="reg_title"><span>Open account</span></div>
    <div id="form_reg" class="cloneReg">
        <form>
            <div class="row_input label-field-wrapper firstName-container">
                <input type="text" placeholder="Name" data-widget="registration" data-type="firstName" data-isRequired="true"
                       data-minLength="1" class="widgetPlaceholder input"/>
            </div>

            <div class="row_input label-field-wrapper email-container">
                <input type="text" placeholder="E-mail" data-widget="registration" data-type="email"
                       data-regexpMessage="E-mail is not correct" data-isRequired="true"
                       class="widgetPlaceholder input"/>
            </div>

            <div class="row_input label-field-wrapper phone-container">
                <input type="hidden" placeholder="Phone" id="customer_phone"/>
                <input type="text" placeholder="Phone" data-widget="registration" data-type="phone"
                       data-regexpMessage="This is required field" data-isRequired="true"
                       class="widgetPlaceholder phone" value=""/>
            </div>

            <div class="row_input label-field-wrapper email-container">
                <input type="password" placeholder="Password" data-widget="registration" data-type="password"
                       data-minLength="6" data-isRequired="true"
                       data-regexpMessage="Password must contain at least 6 symbols"/>
            </div>

            <div class="form_check">
                <input id="agree_term2" type="checkbox" data-widget="registration" data-type="tos"
                       data-isRequired="true"
                       value="true" <?php echo(sanitize_text_field(@$_POST['tos']) == 'on' ? 'checked="checked"' : 'checked="checked"'); ?> /><label
                        for="agree_term2"><span>I agree with terms and accept
 <br/>условия <a href="/en/legal-documentation?register=widget" target="_blank">legal documentation</a></span></label>
                <span data-widget="registration" data-type="error" style="display: none;" class="error-message"></span>
            </div>

            <div class="label-field-wrapper row_input" style="height:66px !important;position:relative;clear:both;">
                <input type="button" data-widget="registration" data-type="submit" value="Sign Up"
                       class="widgetPlaceholder dialog-widget" id="short-reg-dialog-widget"/>
                <div class="preload_registration" style="display:none;"><img
                            src="/wp-content/themes/tol-parent/images/home/load.gif" width="30" height="30"></div>
            </div>

            <div style="display:none;">
                <input type="text" value="" data-widget="registration" data-type="lastName" data-minLength="1"
                       data-isRequired="true" class="widgetPlaceholder input"/>
                <div data-widget="registration" data-type="error" class="error-message"></div>
                <input type="text" data-widget="registration" data-type="languageCode" data-isRequired="true"
                       value="<?php echo (TolLang::getCurrentIso() == 'ru') ? 1049 : 1033; ?>">
                <input type="text" data-widget="registration" data-type="countryPhoneCode"
                       class="widgetPlaceholder phoneCountryCode" value=""/>
                <input type="text" data-widget="registration" data-type="areaCode"
                       class="widgetPlaceholder phoneAreaCode" value=""/>
                <input type="text" data-widget="registration" data-type="confirmPassword" data-isRequired="true"
                       class="widgetPlaceholder input"/>
                <select data-widget="registration" data-type="countryCode" data-isRequired="true"></select>
            </div>
        </form>
        <div id="reg_footer" style="bottom:-48px !important;">Do you already have trading account? <a href="#login">Sign
                In</a></div>
    </div>


    <script type="text/javascript">
        $(document).ready(function () {

            $('#form_reg input').on("change", function (e) {
                $(this).val($(this).val().trim());

                if ($(this).attr('data-type') == 'phone') {
                    $(this).val($(this).val().replace(/[^0-9\(\)\[\]\{\}\-\+"'@\#\$%\^&<>\,\?=_~\\/\|;:\ ]/gim, ''));
                }
                if ($(this).attr('data-type') == 'email') {
                    $(this).val($(this).val().replace(/[^a-zA-Z]+$/gim, ''));
                }

                if($(this).attr("data-type") === "firstName"){
                    const firstName = $(this).val();
                    $('#reg_form [data-type="firstName"]').val(firstName);
                }

            });

            $('#form_reg input').on("keypress", function (e) {
                if ($(this).attr('data-type') == 'phone') {
                    if (String.fromCharCode(e.which).match(/[^0-9\(\)\[\]\{\}\-\+"'@\#\$%\^&<>\,\?=_~\\/\|;:\ ]/gim)) {
                        e.preventDefault();
                    }
                }

                if ($(this).attr('data-type') == 'email') {
                    if ($(this).val().match(/[^a-zA-Z]+$/gim)) {
                    }
                }

                if (e.keyCode == 13) {
                    var inputs = $(this).parents("form").eq(0).find(':input:visible');
                    var idx = inputs.index(this);

                    if (idx == inputs.length - 1) {
                        inputs[0].select()
                    } else {
                        inputs[idx + 1].focus();
                        inputs[idx + 1].select();
                    }
                    return false;
                }
            });
        });

        $('#form_reg input[data-type="password"]').change(function () {
            $('#form_reg input[data-type="confirmPassword"]').val($(this).val());
        });
    </script>
    <!-- end widget -->

<?php endif; ?>
