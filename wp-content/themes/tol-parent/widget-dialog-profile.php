<?php
/**
 * User: SbWereWolf Date: 2018-12-24 19:15
 * Project: Migesco
 */
?>
<div style="position:absolute;width:980px;left:50%;margin-left:-490px;height:100%;top:0;">
    <?php if (prior_lang('ru')): ?>
        <?php include(dirname(__FILE__) . '/widget-dialog-auth.php'); ?>
        <?php include(dirname(__FILE__) . '/widget-dialog-registration.php'); ?>
        <?php include(dirname(__FILE__) . '/widget-dialog-reset-password.php'); ?>
        <?php include(dirname(__FILE__) . '/widget-dialog-myprofile.php'); ?>
        <?php include(dirname(__FILE__) . '/widget-dialog-changepassword.php'); ?>
        <?php include(dirname(__FILE__) . '/widget-dialog-changephone.php'); ?>
        <?php include(dirname(__FILE__) . '/widget-dialog-changevaluta.php'); ?>
    <?php elseif (prior_lang('en')): ?>
        <?php include(dirname(__FILE__) . '/widget-dialog-auth-en.php'); ?>
        <?php include(dirname(__FILE__) . '/widget-dialog-registration-en.php'); ?>
        <?php include(dirname(__FILE__) . '/widget-dialog-reset-password-en.php'); ?>
        <?php include(dirname(__FILE__) . '/widget-dialog-myprofile-en.php'); ?>
        <?php include(dirname(__FILE__) . '/widget-dialog-changepassword-en.php'); ?>
        <?php include(dirname(__FILE__) . '/widget-dialog-changephone-en.php'); ?>
        <?php include(dirname(__FILE__) . '/widget-dialog-changevaluta-en.php'); ?>
    <?php endif; ?>
</div>
<div class="login-popup-bg" style="display:none;"></div>
