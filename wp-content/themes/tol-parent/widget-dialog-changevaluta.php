<?php //if(TolWidgets::isUserLogged()):?>
<div class="changevaluta-wrap" style="display:none;">
    <form>
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td width="50%" id="profile_col1" valign="top">
                    <div class="box_title"><span>Внимание</span></div>
                    <div class="change_text">Выбранная валюта платежа - <strong>Доллары США</strong><br/><br/>Подтвердите
                        операцию пополнения в выбранной валюте или измените валюту платежа.
                    </div>
                    <div class="form_input" style="height:77px !important;position:relative;">
                        <input type="button" value="Изменить" id="change_valuta"/> <input type="button"
                                                                                          value="Продолжить"
                                                                                          id="submit_valuta"/>
                    </div>
                </td>
            </tr>
        </table>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        $('#change_valuta').click(function () {
            $('.login-popup-bg, .changevaluta-wrap').hide();
            $('#depositCurrSelect').focus();
        });

        $('#submit_valuta').click(function () {

            $('.login-popup-bg, .changevaluta-wrap').hide();
            if ($('#depositTypeSelect').val() == '33') {
                $('input[data-widget="cashierDeposit"][data-type="submit"], input#depositBTN2').hide();
                $('input#depositBTN').show();
                $('input#depositBTN').click();
            } else {
                $('input[data-widget="cashierDeposit"][data-type="submit"]').show();
                $('input#depositBTN, input#depositBTN2').hide();
                $('input[data-widget="cashierDeposit"][data-type="submit"]').click();
            }

        });
    });
</script>
<?php //endif;?>
