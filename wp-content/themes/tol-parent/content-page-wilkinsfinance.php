<?php
/**
 *
 * Template Name: Wilkins Finance content page
 *
 */

defineMigescoLayout();

 get_header();
?>
<div id="content" role="main">
	<div class="page-content">
		<div class="breadcrumbs__wrapper">
			<?php require_once(ABSPATH.'wp-content/themes/tol-parent/layouts/breadcrumb.php' ); ?>
		</div>
		<div>
			<?php the_content(); ?>
		</div>
	</div>
</div>
<!-- #content -->

<?php get_footer(); ?>
