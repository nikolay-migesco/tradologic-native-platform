<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/header.css" type="text/css" media="all"/>
<hgroup style="width:1320px; margin: 0px auto; position: relative; z-index:999;">
    <div class="header_table">
        <div style="width:205px; float:left; position: relative; z-index: 10; margin-top:-3px;">
            <a href="<?php echo esc_url(home_url('/')); ?>"
               title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"
               style="display:inline-block;"><img
                        src="<?php echo get_template_directory_uri(); ?>/images/home/logo.png"/></a>
        </div>
        <div>
            <div style="height:34px; text-align:right; padding-top:2px; position:relative; z-index:2;">
                <div class="header_lang">
                    <?php do_action('wpml_add_language_selector'); ?>
                </div>
                <a class="header_toplink header_chat header_icon_chat" href="#chat" onclick="jivo_api.open();">
                    <?php echo prior_lang('ru') ? 'Онлайн чат' : 'Live Chat'; ?>
                </a>
                <span class="header_tooltip">
                    <span class="header_toplink header_icon_phone">
                        8 (800) 2002-780
                    </span>
                    <div class="header_tooltip_content">
                        <div style="padding-left:16px; text-align:left; font:13px Tahoma;">
                            <div style="line-height:30px; padding-top:10px;"><?php echo prior_lang('ru') ? 'Дополнительные номера:' : 'Additional phone:'; ?></div>
                            <div class="header_tooltip_phone">+7 499 403-32-95</div>
                            <div class="header_tooltip_phone" style="margin-top:5px;padding-left:23px;"><a
                                        href="#request_call"
                                        style="text-decoration:none;color:#000;"><?php echo prior_lang('ru') ? 'Заказать обратный звонок' : 'Request a call back'; ?></a></div></div>
                    </div>
                </span>
                <span class="header_tooltip">
                    <a class="header_toplink header_icon_info"
                       href="<?php echo prior_lang('ru') ? '/svyazatsya-s-nami' : '/en/contact-us'; ?>">
                        <?php echo prior_lang('ru') ? 'Поддержка клиентов' : 'Client support'; ?>
                    </a>
                    <?php if (prior_lang('ru')): ?>
                        <div class="header_tooltip_content header_tooltip_content2">
                        <ul class="header_tooltip_info">
                            <li><a href="/kak-nachat-torgovat">Как начать торговать</a></li>
                            <li><a href="/poshagovoe-rukovodstvo">Пошаговое руководство</a></li>
                            <li><a href="#request_call">Заказать обратный звонок</a></li>
                            <li><a href="/otvety-na-voprosy">Ответы на вопросы</a></li>
                        </ul>
                    </div>
                    <?php endif; ?>
                </span>
                <div class="header_clock">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/home/time_icon.png" alt=""/>
                    <span class="header_date"><?php echo date_i18n('d M'); ?></span> <span
                            class="header_time"><?php echo date_i18n('H:i:s'); ?></span>
                </div>
            </div>
            <script type="text/javascript">
                var activeHeaderMenu = '';
                (function (a) {
                    (jQuery.browser = jQuery.browser || {}).mobile = /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))
                })(navigator.userAgent || navigator.vendor || window.opera);
                jQuery(function ($) {
                    if (jQuery.browser.mobile) {
                        $('.header_menu_list li > a, .header_icon_info').click(function () {
                            if ($(this).attr('href') == activeHeaderMenu)
                                return true;

                            activeHeaderMenu = $(this).attr('href');
                            return false;
                        });
                    }
                    setInterval(function () {
                        var currTime = new Date();
                        <?php if(prior_lang() == 'ru') { ?>
                        var monthNames = ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'];
                        <?php } else { ?>
                        var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                        <?php } ?>
                        $(".header_date").html(currTime.getDate() + ' ' + monthNames[currTime.getMonth()]);
                        $(".header_time").html((currTime.getHours() < 10 ? "0" + currTime.getHours() : currTime.getHours()) + ':' + (currTime.getMinutes() < 10 ? '0' + currTime.getMinutes() : currTime.getMinutes()) + ':' + (currTime.getSeconds() < 10 ? '0' + currTime.getSeconds() : currTime.getSeconds()));
                    }, 1000);


                    $('#form_reg input').on('keyup change', function () {
                        if ($(this).attr('data-type') === 'password') {
                            $('#reg_form input[data-type="confirmPassword"]').val($(this).val());
                        }
                        if ($(this).attr('data-type')) {
                            $('#reg_form input[data-type="' + $(this).attr('data-type') + '"]').val($(this).val());
                        }
                        if ($(this).attr('placeholder')) {
                            $('#reg_form input[placeholder="' + $(this).attr('placeholder') + '"]').val($(this).val());
                        }
                    });

                    $('#reg_form input').on('keyup change', function () {
                        if ($(this).attr('data-type') === 'password') {
                            $('#form_reg input[data-type="confirmPassword"]').val($(this).val());
                        }
                        if ($(this).attr('data-type')) {
                            $('#form_reg input[data-type="' + $(this).attr('data-type') + '"]').val($(this).val());
                        }
                        if ($(this).attr('placeholder')) {
                            $('#form_reg input[placeholder="' + $(this).attr('placeholder') + '"]').val($(this).val());
                        }
                    });

                    $('.cloneReg input[data-type="tos"]').live("click", function () {
                        if ($(this).attr('checked') === 'checked') $('.cloneReg input[data-type="tos"]').attr('checked', true);
                        else $('.cloneReg input[data-type="tos"]').attr('checked', false);
                    });

                    $('#form_reg input').keyup(function () {
                        //$('.cloneReg input[data-type="tos"]').attr('checked', true);
                        $('#reg_form input[data-type="password"]').val($('#form_reg input[data-type="password"]').val());
                        $('#reg_form input[data-type="confirmPassword"]').val($('#form_reg input[data-type="confirmPassword"]').val());
                    });

                });

                function setCookie(name, value, expires, path, domain, secure) {
                    document.cookie = name + "=" + escape(value) +
                        ((expires) ? "; expires=" + expires : "") +
                        ((path) ? "; path=" + path : "") +
                        ((domain) ? "; domain=" + domain : "") +
                        ((secure) ? "; secure" : "");
                }

                date = new Date();
                date.setHours(date.getHours() + 1);
                setCookie('_icl_current_language', '<?php echo prior_lang(); ?>', date.toUTCString(), '/');
            </script>


            <script type="text/javascript">
                jQuery(function ($) {
                    if (/(^|;)\s*widgetSession=/.test(document.cookie)) {
                        $('.op-users-only').attr('style', 'display: inline-block !important');
                        $('div[class^="migesco_howtotrade_block"] .op-users-only').attr('style', 'display: inline !important');
                        $('#footer_contact .op-users-only').attr('style', 'display: block !important');
                        $('.op-guests-only').attr('style', 'display: none !important');

                        $('a[href="#myprofile"]').live('click', function () {
                            $('.login-popup-bg, .login-wrap, .reg-wrap, .resetpassword-wrap, .change-wrap, .changephone-wrap').hide();
                            $('.login-popup-bg, .profile-wrap').show();
                            $('.profile-wrap input[data-type="firstName"]').focus();

                            setTimeout(function () {
                                $('[data-type=phone]').val($.parseJSON($.cookie('widgetUserData')).phone);
                            }, 3000);

                            $('.profile-wrap div.validationError').remove();
                            $('.profile-wrap .myprofile-successmsg').hide();
                        });

                        $('a[href="#changepassword"]').live('click', function () {
                            $('.login-popup-bg, .login-wrap, .reg-wrap, .resetpassword-wrap, .profile-wrap, .changephone-wrap').hide();
                            $('.login-popup-bg, .change-wrap').show();
                            $('.profile-wrap input[data-type="firstName"]').focus();
                        });

                        $('input#depositBTN').live('click', function () {
                            $('.login-popup-bg, .login-wrap, .reg-wrap, .resetpassword-wrap, .change-wrap, .profile-wrap').hide();
                            $('.login-popup-bg').css('opacity', '0.1');
                            $('.login-popup-bg, .changephone-wrap').show();
                            $('.changephone-wrap input[data-type="phone"]').focus();
                            $('.profile-wrap div.validationError').remove();
                            $('.profile-wrap .myprofile-successmsg').hide();
                        });

                        $('input#depositBTN2').live('click', function () {
                            $('.login-popup-bg, .login-wrap, .reg-wrap, .resetpassword-wrap, .change-wrap, .profile-wrap, .changephone-wrap').hide();
                            $('.login-popup-bg').css('opacity', '0.1');
                            $('.login-popup-bg').show();
                            if ($.cookie('widgetUserData')) {
                                var json = ($.cookie('widgetUserData'));
                                var obj = $.parseJSON(json);
                            } else {
                                var obj = '';
                                obj.isReal = false;
                            }
                            if (obj.isReal === false) {
                                if ('<?=prior_lang()?>' != 'en') {
                                    $('.changevaluta-wrap').show();
                                } else {
                                    $('.login-popup-bg, .changevaluta-wrap').hide();
                                    if ($('#depositTypeSelect').val() == '33') {
                                        $('input[data-widget="cashierDeposit"][data-type="submit"], input#depositBTN2').hide();
                                        $('input#depositBTN').show();
                                        $('input#depositBTN').click();
                                    } else {
                                        $('input[data-widget="cashierDeposit"][data-type="submit"]').show();
                                        $('input#depositBTN, input#depositBTN2').hide();
                                        $('input[data-widget="cashierDeposit"][data-type="submit"]').click();
                                    }

                                }
                            }
                        });

                        $('.appriseOuter .appriseInner button[value="ok"]').live('click', function () {
                            $('.login-popup-bg, .change-wrap').hide();
                        });
                    } else {
                        $('.op-guests-only').attr('style', 'display: inline-block !important');
                        $('div[class^="migesco_howtotrade_block"] .op-guests-only').attr('style', 'display: inline !important');
                        $('#footer_contact .op-guests-only').attr('style', 'display: block !important');
                        $('.op-users-only').attr('style', 'display: none !important');

                        var hash = window.location.hash;
                        if (hash.indexOf('#register') !== -1) {
                            $('.login-popup-bg, .reg-wrap').show();
                            $('#blank_name2').focus();
                            $('html, body').animate({scrollTop: 0}, 500);
                        }

                        if (hash.indexOf('#login') !== -1) {
                            $('.login-popup-bg, .login-wrap').show();
                            $('#username').focus();
                            $('html, body').animate({scrollTop: 0}, 500);
                        }

                        if (hash.indexOf('#resetpassword') !== -1) {
                            $('.login-popup-bg, .resetpassword-wrap').show();
                            $('.resetpassword-wrap input.widgetPlaceholder').focus();
                            $('html, body').animate({scrollTop: 0}, 500);
                        }

                        $('a[href="#login"]').live('click', function () {
                            $('.login-popup-bg, .reg-wrap, .resetpassword-wrap, .profile-wrap, .change-wrap, .changephone-wrap').hide();
                            $('.login-popup-bg, .login-wrap').show();
                            $('#username').focus();
                        });

                        $('a[href="#register"]').live('click', function () {
                            $('.login-popup-bg, .login-wrap, .resetpassword-wrap, .profile-wrap, .change-wrap, .changephone-wrap').hide();
                            $('.login-popup-bg, .reg-wrap').show();
                            $('#blank_name2').focus();
                            $('#phone').val('');
                        });

                        $('a[href="#resetpassword"]').live('click', function () {
                            $('.login-popup-bg, .login-wrap, .reg-wrap, .profile-wrap, .change-wrap, .changephone-wrap').hide();
                            $('.login-popup-bg, .resetpassword-wrap').show();
                            $('.resetpassword-wrap input.widgetPlaceholder').focus();
                        });

                        $('.appriseOuter .appriseInner button[value="ok"]').live('click', function () {
                            $('.login-popup-bg, .resetpassword-wrap').hide();
                        });
                    }

                    if (/(^|;)\s*widgetSession=/.test(document.cookie)) {
                        if ($.cookie('widgetUserData')) {
                            var widgetUserData = JSON.parse($.cookie('widgetUserData'));
                        } else {
                            var widgetUserData = '';
                            widgetUserData.isReal = false;
                        }
                        if (widgetUserData.isReal) {
                            $('.op-demo-account').attr('style', 'display: none !important');
                            $('.op-real-account').attr('style', 'display: inline-block !important');
                        } else {
                            $('.op-demo-account').attr('style', 'display: inline-block !important');
                            $('.op-real-account').attr('style', 'display: none !important');
                        }
                    }

                });
            </script>

            <?php if (!empty($_GET['go'])): $vars = @explode(':', base64_decode(urldecode($_GET['go']))); /*Auto login after LP*/ ?>
                <script type="text/javascript">
                    <?php if (strpos($_SERVER['HTTP_REFERER'], '/zarabotat/3') !== false) echo "widgetsSettings.loginSuccessUrl = 'traderoom#registration-success';"; ?>
                    <?php if (strpos($_SERVER['HTTP_REFERER'], '/zarabotat/4') !== false) echo "widgetsSettings.loginSuccessUrl = 'traderoom#registration-success';"; ?>
                    <?php if (strpos($_SERVER['HTTP_REFERER'], '/horoshiy-zarabotok/1') !== false) echo "widgetsSettings.loginSuccessUrl = 'traderoom#registration-success';"; ?>
                    <?php if (strpos($_SERVER['HTTP_REFERER'], '/horoshiy-zarabotok/2') !== false) echo "widgetsSettings.loginSuccessUrl = 'traderoom#registration-success';"; ?>
                    <?php if (strpos($_SERVER['HTTP_REFERER'], '/horoshiy-zarabotok/3') !== false) echo "widgetsSettings.loginSuccessUrl = 'traderoom#registration-success';"; ?>
                    <?php if (strpos($_SERVER['HTTP_REFERER'], '/horoshiy-zarabotok/4') !== false) echo "widgetsSettings.loginSuccessUrl = 'traderoom#registration-success';"; ?>
                    <?php if (strpos($_SERVER['HTTP_REFERER'], '/horoshiy-zarabotok/5') !== false) echo "widgetsSettings.loginSuccessUrl = 'traderoom#registration-success';"; ?>
                    <?php if (strpos($_SERVER['HTTP_REFERER'], '/horoshiy-zarabotok/6') !== false) echo "widgetsSettings.loginSuccessUrl = 'traderoom#registration-success';"; ?>
                    <?php if (strpos($_SERVER['HTTP_REFERER'], '/horoshiy-zarabotok/7') !== false) echo "widgetsSettings.loginSuccessUrl = 'traderoom#registration-success';"; ?>
                    <?php if (strpos($_SERVER['HTTP_REFERER'], '/horoshiy-zarabotok/8') !== false) echo "widgetsSettings.loginSuccessUrl = 'traderoom#registration-success';"; ?>
                    <?php if (strpos($_SERVER['HTTP_REFERER'], '/horoshiy-zarabotok/9') !== false) echo "widgetsSettings.loginSuccessUrl = '/deposit-account-no-risk#registration-success';"; ?>
                    <?php if (strpos($_SERVER['HTTP_REFERER'], '/innovacionnaya-platforma/1') !== false) echo "widgetsSettings.loginSuccessUrl = 'traderoom#registration-success';"; ?>
                    <?php if (strpos($_SERVER['HTTP_REFERER'], '/innovacionnaya-platforma/2') !== false) echo "widgetsSettings.loginSuccessUrl = 'traderoom#registration-success';"; ?>
                    <?php if (strpos($_SERVER['HTTP_REFERER'], '/innovacionnaya-platforma/3') !== false) echo "widgetsSettings.loginSuccessUrl = 'traderoom#registration-success';"; ?>
                    <?php if (strpos($_SERVER['HTTP_REFERER'], '/bonus/1') !== false) echo "widgetsSettings.loginSuccessUrl = 'traderoom#registration-success';"; ?>
                    <?php if (strpos($_SERVER['HTTP_REFERER'], '/trading-plan/1') !== false) echo "widgetsSettings.loginSuccessUrl = 'traderoom#registration-success';"; ?>
                    <?php if (strpos($_SERVER['HTTP_REFERER'], '/support/1') !== false) echo "widgetsSettings.loginSuccessUrl = 'traderoom#registration-success';"; ?>
                    <?php if (strpos($_SERVER['HTTP_REFERER'], '/broker/1') !== false) echo "widgetsSettings.loginSuccessUrl = 'traderoom#registration-success';"; ?>
                    <?php if (strpos($_SERVER['HTTP_REFERER'], '/broker/2') !== false) echo "widgetsSettings.loginSuccessUrl = 'traderoom#registration-success';"; ?>
                    <?php if (strpos($_SERVER['HTTP_REFERER'], '/broker/3') !== false) echo "widgetsSettings.loginSuccessUrl = 'traderoom#registration-success';"; ?>
                    <?php if (strpos($_SERVER['HTTP_REFERER'], '/broker/4') !== false) echo "widgetsSettings.loginSuccessUrl = 'traderoom#registration-success';"; ?>
                    <?php if (strpos($_SERVER['HTTP_REFERER'], '/broker/5') !== false) echo "widgetsSettings.loginSuccessUrl = 'traderoom#registration-success';"; ?>
                    <?php if (strpos($_SERVER['HTTP_REFERER'], '/broker/6') !== false) echo "widgetsSettings.loginSuccessUrl = '/deposit-account#registration-success';"; ?>
                    <?php if (strpos($_SERVER['HTTP_REFERER'], '/broker/7') !== false) echo "widgetsSettings.loginSuccessUrl = '/deposit-account#registration-success';"; ?>
                    <?php if (strpos($_SERVER['HTTP_REFERER'], '/broker/8') !== false) echo "widgetsSettings.loginSuccessUrl = 'traderoom#registration-success';"; ?>
                    <?php if (strpos($_SERVER['HTTP_REFERER'], '/new-forex/1') !== false) echo "widgetsSettings.loginSuccessUrl = 'traderoom#registration-success';"; ?>
                    <?php if (strpos($_SERVER['HTTP_REFERER'], '/new-trading/1') !== false) echo "widgetsSettings.loginSuccessUrl = 'traderoom#registration-success';"; ?>
                    <?php if (strpos($_SERVER['HTTP_REFERER'], '/new-invest/1') !== false) echo "widgetsSettings.loginSuccessUrl = 'traderoom#registration-success';"; ?>
                    <?php if ($_GET['form'] == '1') echo "widgetsSettings.loginSuccessUrl = 'traderoom#registration-success';"; ?>
                    <?php if ($_GET['form'] == '2') echo "widgetsSettings.loginSuccessUrl = 'traderoom#registration-success';"; ?>
                    jQuery(function ($) {
                        $('.login-BNT').click();
                        $('#username').val('<?php echo $vars[0];?>');
                        $('#password').val('<?php echo $vars[1];?>');
                        $('input[data-type=submit]').click();
                        setInterval(function () {
                            $('input[data-type=submit]').click();
                        }, 3000);
                    });
                </script>
            <?php endif; ?>

        </div>

        <?php include_once(dirname(__FILE__) . '/widget-dialog-profile.php'); ?>

        <!-- aff cookie -->

        <?php
        if (isset($_GET['A'])) {
            $AffiliateID = @$_GET['A'];
            $ClickBannerID = @$_GET['B'];
            $SubAffiliateID = @$_GET['SubAffiliateID'];
            $ClickDateTime = date('d/m/Y H:i:s A');
            $ExpireDateTime = date('D, d/M/Y H:i:s A', time() + 3600 * 24 * 365);
            $cookieVal = "AffiliateID=" . $AffiliateID . "&ClickBannerID=" . $ClickBannerID . '&SubAffiliateID=' . $SubAffiliateID . '&ClickDateTime=' . $ClickDateTime;
            header("Set-Cookie: TLAffiliateID=" . $cookieVal . "; expires=" . $ExpireDateTime . " GMT; domain=.migesco.com; path=/");
        }
        ?>

        <!-- aff cookie -->

</hgroup>
