<?php

if ($_SERVER['SCRIPT_FILENAME'] == __FILE__)
    die('Access denied.');

if (!class_exists('TolThemedev')) {

    /**
     * Example of an instance class
     * @package TradologicPlugin
     */
    class TolThemedev extends WPPSModule {

        protected $readableProtectedVars, $writableProtectedVars;

        const VERSION = TOLP_VERSION;

        /*
         * Magic methods
         */

        /**
         * Constructor
         * @mvc Controller
         */
        public function __construct() {
            $this->registerHookCallbacks();
        }

        /**
         * Prepares site to use the plugin during activation
         * @mvc Controller
         * @param bool $networkWide
         */
        public function activate($networkWide) {
            
        }

        /**
         * Rolls back activation procedures when de-activating the plugin
         * @mvc Controller
         */
        public function deactivate() {
            
        }

        /**
         * Initializes variables
         * @mvc Controller
         */
        public function init() {
            if (did_action('init') !== 1)
                return;
        }

        /**
         * Executes the logic of upgrading from specific older versions of the plugin to the current version
         * @mvc Model
         * @param string $dbVersion
         */
        public function upgrade($dbVersion = 0) {
            
        }

        /**
         * Checks that the object is in a correct state
         * @mvc Model
         * @param string $property An individual property to check, or 'all' to check all of them
         * @return bool
         */
        protected function isValid($property = 'all') {
            return true;
        }

        /*
         * Static methods
         */

        /**
         * Register callbacks for actions and filters
         * @mvc Controller
         */
        public function registerHookCallbacks() {
            // NOTE: Make sure you update the did_action() parameter in the corresponding callback method when changing the hooks here
            add_action('wp_enqueue_scripts', __CLASS__ . '::registerScriptsStyles');
            add_action('wp_footer', __CLASS__ . '::printViewFooter');
        }

        public static function registerScriptsStyles() {
            if (did_action('wp_enqueue_scripts') !== 1)
                return;

            // add theme-dev's custom js
            wp_enqueue_script('jquery-cookie');
            wp_enqueue_script('theme-dev',  get_template_directory_uri() . '/devbar/javascript/themedev.js', array('jquery-cookie'), self::VERSION, false);
            wp_enqueue_style('theme-dev',  get_template_directory_uri() . '/devbar/css/themedev.css', false, self::VERSION);
        }

        public static function printViewFooter() {
            if (did_action('wp_footer') !== 1)
                return;

            require( dirname(__DIR__) . '/devbar/views/tol-themedev/footer.php' );
        }

        protected function fs_get_wp_config_path() {
            $path = ABSPATH;
            if ($path != false) {
                $path = str_replace("\\", "/", $path);
            }
            return $path;
        }

    }

    // end class
}
