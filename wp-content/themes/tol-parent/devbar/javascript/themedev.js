jQuery(function ($) {
    var themedev_html = '<label><a id="themedev-cleartoken" href="#">clear cookies</a></label>';
    $('#theme-dev-container').append(themedev_html);
    $('#themedev-cleartoken').click(function(e){
        e.preventDefault();
        //widgets.config.sessionId = '00000000-0000-0000-0000-000000000000';
        $.removeCookie('token', { path: '/' });
        $.removeCookie('widgetSession', { path: '/' });
        $.removeCookie("widgetUserData", { path: '/' });
        $.removeCookie("view", { path: '/' });
        $.removeCookie("config", { path: '/' });
        $.removeCookie("configFile", { path: '/' });
        $.removeCookie("changeWidgets", { path: '/' });
        $.removeCookie("firstLoginAction", { path: '/' });

        clearListCookies();
    });

    function clearListCookies(){   
        var cookies = document.cookie.split(";");
        for (var i = 0; i < cookies.length; i++)
        {   
            var spcook =  cookies[i].split("=");
            deleteCookie(spcook[0]);
        }
        window.location.href = "/"; // TO REFRESH THE PAGE
    }
    function deleteCookie(cookiename){
        var d = new Date();
        d.setDate(d.getDate() - 1);
        var expires = ";expires="+d;
        var name=cookiename;
        //alert(name);
        var value="";
        document.cookie = name + "=" + value + expires + "; path=/";                    
    }
    $('#changeConfig').change(function(){
        $.cookie("config", $(this).val(), { path: '/' } );	// set value of checkbox in cookie
        $.cookie("configFile", $(this).val(), { path: '/' } );	// set value of checkbox in cookie
        location.reload(true);
    });
    
    $('#changeWidgets').change(function(){
        $.cookie("changeWidgets", $(this).val(), { path: '/' } );	// set value of checkbox in cookie
        location.reload(true);
    });
    
    //add sessionid link
    $('#themedev-sessionId').click(function(e){
        e.preventDefault();
        alert(widgets.token.sessionId);
    });
    /*if(!widgets.token.sessionId){
        $('#themedev-sessionId').hide();
    }*/
});