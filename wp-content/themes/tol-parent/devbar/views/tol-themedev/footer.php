<?php
    global $configs;
    $pathinfo = pathinfo(get_page_template());

    if($pathinfo['filename']=='page' && is_home()){
        $layout = HOMEPAGE_LAYOUT;
    }else{
        $fileSTR = file_get_contents(get_page_template());
        preg_match_all("/\/\'[\s]?\.[\s]?([A-Z\_]*)[\s]?\.[\s]?\'[\s]?\.php/", $fileSTR, $layout);
        $layout = constant($layout[1][0]);
    }
    if($layout=='') $layout = '<span title="'.($pathinfo['dirname'].'/'.$pathinfo['basename']).'">'.$pathinfo['basename'].'</span>';

    $widget_config = '';
    $widget_config .= 'BASE_URL: '	. $configs['widget_config'][ENV_NAME]['base_url'] . "\n";
    $widget_config .= 'LIB_URL: '	. $configs['widget_config'][ENV_NAME]['lib_url'] . "\n";
    $widget_config .= 'FEED_API: '. $configs['widget_config'][ENV_NAME]['feed_api'] . "\n";
    $widget_config .= 'ADV_FEED_API: '	. $configs['widget_config'][ENV_NAME]['adv_feed_api'] . "\n";
    $widget_config .= 'REST_API: '	. $configs['widget_config'][ENV_NAME]['rest_api'] . "\n\n";

    $widget_config .= 'WEB_ID: '	. $configs['env'][ENV_NAME]['web_id'] . "\n";

    $widgetsENV = '';
    if($configs['widget_config'][ENV_NAME]['base_url']=='//widgets8.tradologic.net/Widgets/') $widgetsENV ='(<b>stage</b>)';
    elseif($configs['widget_config'][ENV_NAME]['base_url']=='//widgets7.tradologic.net/Widgets/') $widgetsENV ='(<b>qa</b>)';
    elseif($configs['widget_config'][ENV_NAME]['base_url']=='//widgets.tradologic.net/Widgets/') $widgetsENV ='(<b>prod</b>)';

    $dbhost = DB_HOST;
    if(DB_HOST=='127.0.0.1') $dbhost = DB_HOST.' (<b>localhost</b>)';
    elseif(DB_HOST=='10.0.8.87') $dbhost = DB_HOST.' (<b>stage</b>)';
    elseif(DB_HOST=='10.0.8.86') $dbhost = DB_HOST.' (<b>qa</b>)';
    elseif(DB_HOST=='10.0.8.84') $dbhost = DB_HOST.' (<b>preview</b>)';
    elseif(DB_HOST=='10.0.8.89') $dbhost = DB_HOST.' (<b>uat</b>)';
    elseif(DB_HOST=='10.50.80.30') $dbhost = DB_HOST.' (<b>prod</b>)';
?>

<div id="theme-dev-container" class="theme-dev-container">
    <?php if(!wp_is_mobile()) { ?><label>Version: <b><?php echo $configs['general']['version']?></b></label><?php } ?>
    <?php if(!wp_is_mobile()) { ?>
        <label><span title="<?php print_r($configs['layouts'])?>">Layout Settings</span></label>
        <label>Layout: <b><?php echo $layout; ?></b></label>
        <label>
            <span title="<?php echo $widget_config;?>">Widgets Status</span>: <?php echo $widgetsENV; ?> <span class="themedevloader"><span class="loadingElementText"></span></span>
            <select id="changeWidgets" name="changeWidgets">
                <?php 
                    foreach ($configs['widget_config']as $key=>$envConfig){
                        echo '<option value="'.$key.'" '.((ENV_NAME==$key)? "selected=\"selected\"":"").'>'.$key.'</option>';
                    }
                ?>
            </select>
        </label>
        <label>DataBase: <?php echo DB_USER .'@'. $dbhost;  ?></label>
        <label><a id="themedev-sessionId" href="#">SessionId</a></label>
    <?php } ?>
    <span class="hide-dev"></span>
</div>
