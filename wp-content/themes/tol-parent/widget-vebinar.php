<?php
$is_real = TolGetInfo('isReal');

$isValid = isset($user_id);
if (!$isValid) {
    $user_id = '';
}
$isValid = isset($_COOKIE[$user_id . 'countdown']);
if ($isValid) {
    $isValid = $_COOKIE[$user_id . 'countdown'] > 0;
}
if ($isValid && $is_real == false) {
} else {
    $wpb_all_query = new WP_Query(array('post_type' => 'post', 'category_name' => 'vebinar', 'post_status' => 'publish', 'posts_per_page' => 1, 'paged' => $paged));
    if ($wpb_all_query->have_posts()) {
        while ($wpb_all_query->have_posts()) : $wpb_all_query->the_post();

            $json = stripslashes($_COOKIE['widgetUserData']);
            $userdata = json_decode($json, true);
            if (time() > strtotime("+1 day", strtotime($userdata['registeredSince']['dateTime']))) {
                ?>

                <script type="text/javascript">
                    $(document).ready(function () {

                        if (jQuery.browser.mobile) {
                            $('#vebinar_close').css('width', '600px').css('height', '280px');
                        }

                        $('#vebinar_close').live("click", function () {
                            $('#vebinar_overlay').addClass('active_popup');
                            $('#vebinar_overlay').removeClass('inactive_popup');
                            $.cookie("vebinar_hide_<?php echo get_the_ID(); ?>", 1);
                        });

                        $('.active_popup .vebinar_box').live("click", function (event) {
                            $('#vebinar_overlay').removeClass('active_popup');
                            $('#vebinar_overlay').addClass('inactive_popup');
                            $.cookie("vebinar_hide_<?php echo get_the_ID(); ?>", 0);
                        });

                        $('#vebinar_overlay').live("click", function () {
                            $('#vebinar_close').show();
                        });

                        setTimeout(function () {
                            $('#vebinar_close').show();
                        }, 3000);

                    });
                </script>

                <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&subset=cyrillic,cyrillic-ext,latin-ext"
                      rel="stylesheet"/>
                <style type="text/css">
                    /* vebinar */
                    #vebinar_close {
                        width: 54px;
                        height: 54px;
                        position: absolute;
                        right: 3px;
                        top: 3px;
                        background: url('/wp-content/themes/tol-parent/images/xp-theme/popup/close.png') top right no-repeat;
                        cursor: pointer;
                    }

                    #vebinar_overlay {
                        top: 0;
                        left: 0;
                        position: fixed;
                        width: 100%;
                        height: 100%;
                        background: rgba(0, 0, 0, .5);
                        z-index: 9999999;
                    }

                    .active_popup {
                        top: auto !important;
                        right: auto !important;
                        position: fixed !important;
                        width: 100% !important;
                        height: auto !important;
                        background: none !important;
                        z-index: 9999999 !important;
                    }

                    #vebinar_overlay .vebinar_box .open_box {
                        display: block;
                    }

                    #vebinar_overlay .vebinar_box .close_box {
                        display: none;
                    }

                    .active_popup .vebinar_box .open_box {
                        display: none !important;
                    }

                    .active_popup .vebinar_box .close_box {
                        display: block !important;
                    }
                </style>

                <?php
                if ($_COOKIE['vebinar_hide_' . get_the_ID()] == 1 || $_GET['hide'] == 1) {
                    $class_popup = 'active_popup';
                } else {
                    $class_popup = 'inactive_popup';
                }

                ?>

                <script type="text/javascript">
                    $('.inactive_popup .vebinar_box').live("click", function (event) {
                        if (!$(event.target).is('#vebinar_close')) {
                            event.preventDefault();
                            window.open("<?php echo the_field('link'); ?>", '_blank');
                        }
                    });
                </script>
                <style type="text/css">
                    #vebinar_overlay .vebinar_box {
                        width: 600px;
                        height: 400px;
                        margin: auto;
                        position: absolute;
                        top: 0;
                        left: 0;
                        bottom: 0;
                        right: 0;
                        background: #fff url('/wp-content/themes/tol-parent/images/xp-theme/popup/big_popup.jpg');
                        border: 0;
                        cursor: pointer;
                    }

                    .active_popup .vebinar_box {
                        width: 200px !important;
                        height: 100px !important;
                        left: auto !important;
                        top: auto !important;
                        right: 0 !important;
                        bottom: 0 !important;
                        position: fixed !important;
                        cursor: pointer;
                        background: url('/wp-content/themes/tol-parent/images/xp-theme/popup/mini_popup.jpg') !important;
                    }
                </style>
                <div id="vebinar_overlay" class="<?php echo $class_popup; ?>">
                    <div class="vebinar_box">
                        <div class="open_box">
                            <div style="margin-top:10px;font:bold 34px 'Open Sans';color:#4b6e9b;text-align:center;">Как
                                зарабатывать
                            </div>
                            <div style="margin-top:-5px;font:bold 19px 'Open Sans';color:#4b6e9b;text-align:center;">на
                                бинарных опционах в Migesco?
                            </div>
                            <div style="width:262px;height:54px;margin:2px auto 0;background:url('/wp-content/themes/tol-parent/images/xp-theme/popup/date_bg.png');">
                                <span style="padding-top:15px;font:20px 'Open Sans';color:#5b7aa4;display:inline-block;margin-left:49px;width:102px;"><?php echo the_field('date'); ?></span>
                                <span style="padding-top:15px;font:20px 'Open Sans';color:#69bb1b;display:inline-block;margin-left:24px;"><?php echo the_field('time'); ?></span>
                            </div>
                            <div style="margin-top:0px;font:11px 'Open Sans';color:#7b7979;text-align:center;">*указано
                                московское время
                            </div>
                            <div style="margin-top:10px;margin-left:13px;padding-left:33px;color:black;font:14px/26px 'Open Sans';height:26px;background:url('/wp-content/themes/tol-parent/images/xp-theme/popup/check.png') left center no-repeat;">
                                Что такое бинарные опционы и как на них заработать
                            </div>
                            <div style="margin-top:10px;margin-left:13px;padding-left:33px;color:black;font:14px/26px 'Open Sans';height:26px;background:url('/wp-content/themes/tol-parent/images/xp-theme/popup/check.png') left center no-repeat;">
                                Чем успешный трейдер отличается от обычного
                            </div>
                            <div style="margin-top:10px;margin-left:13px;padding-left:33px;color:black;font:14px/26px 'Open Sans';height:26px;background:url('/wp-content/themes/tol-parent/images/xp-theme/popup/check.png') left center no-repeat;">
                                Какие преимущества дает платформа Migesco
                            </div>
                            <div style="margin-top:10px;margin-left:13px;padding-left:33px;color:black;font:14px/26px 'Open Sans';height:26px;background:url('/wp-content/themes/tol-parent/images/xp-theme/popup/check.png') left center no-repeat;">
                                Почему успех в торговле зависит от выбора платформы
                            </div>
                            <div style="margin-top:10px;margin-left:13px;padding-left:33px;color:black;font:14px/26px 'Open Sans';height:26px;background:url('/wp-content/themes/tol-parent/images/xp-theme/popup/check.png') left center no-repeat;">
                                Как за один вечер стать успешным трейдером
                            </div>
                            <div style="text-align:center;">
                                <a style="margin-top:11px;border-radius:65px;display:inline-block;font:14px/45px 'Open Sans';color:#fff;padding-left:20px;padding-right:20px;height:45px;background-color:#69bb1b;">Записаться
                                    на бесплатный вебинар</a>
                            </div>
                            <div id="vebinar_close"></div>
                        </div>
                        <div class="close_box"></div>
                    </div>
                </div>

                <?php
            }
        endwhile;
        wp_reset_postdata();
    }
}
?>
