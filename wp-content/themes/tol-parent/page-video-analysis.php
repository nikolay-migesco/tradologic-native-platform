<?php
/**
 * Template Name: MTE video analysis page
 */
    $userId = json_decode(stripslashes($_COOKIE['widgetUserData']), true)["id"];
    $redirectDestination = $userId ? '/traderoom' : '';

    if (json_decode(stripslashes($_COOKIE['widgetUserData']), true)["Package"] < 3000) {
        $url         =  "//{$_SERVER['HTTP_HOST']}";
        $escaped_url = htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' );
        header("Location: " . $escaped_url . $redirectDestination);
    }

    get_header();
    switch (ICL_LANGUAGE_CODE) {

    case "de":
        $language = "de";
        break;
    case "ja":
        $language = "jp";
        break;
    case "ar":
        $language = "ar";
        break;
    case "zh-hans":
        $language = "cn";
        break;
    case "it":
        $language = "it";
        break;
    case "pl":
        $language = "pl";
        break;
    case "pt-pt":
        $language = "pt";
        break;
    case "ru":
        $language = "ru";
        break;
    case "tr":
        $language = "tr";
        break;
    default:
        $language = "en";
}
 ?>


	<div id="content" role="main">
        <div class="market-tools-header">
            <div class="market-tools-header-title" data-translate="videoAnalysis"></div>
        </div>
        <div class="edu-intro-text" data-translate="edu-intro-text-video"></div>
        <div class="container">
            <script>
                function getCookie(cname) {
                    var name = cname + "=";
                    var ca = document.cookie.split(';');
                    for(var i=0; i<ca.length; i++) {
                        var c = ca[i];
                        while (c.charAt(0)==' ') c = c.substring(1);
                        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
                    }
                    return "";
                };

                if(getCookie('widgetSession')){
                    $(' .container').append("<iframe allowfullscreen frameborder=0 src='//www.mte-media.com/admin2/frames/widgets_index.php?ref=9ff909f&set=Default&widg=tecvid&lng=en' width='100%' height='800px' ></iframe>");
                } else {
                    $('.container').append("<iframe allowfullscreen frameborder=0 src='//www.mte-media.com/admin2/frames/widgets_index.php?ref=9ff909f&set=Default&widg=tecvid&lng=en&demo=1' width='100%' height='800px' ></iframe>");
                };
            </script>

            <div class="edu-intro-text" data-translate="edu-intro-text-video"></div>

            <section id='learning_center'></section>
        </div>

	</div><!-- #content -->	




<?php
    get_footer();
 ?>