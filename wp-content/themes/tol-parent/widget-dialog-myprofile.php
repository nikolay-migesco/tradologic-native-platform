<?php //if(TolWidgets::isUserLogged()):?>
<script type="text/javascript" src="/wp-content/themes/tol-parent/userDetails.js"></script>
<style>
    #profile_col1 span.date_text_ru {
        display: inline-block !important;
        font: 13px Tahoma !important;
        color: white !important;
        padding-left: 0px !important;
        /*padding-right: 12px !important;*/
        padding-top: 2px !important;
    }

</style>
<div class="profile-wrap" style="display:none;">
    <form>
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td width="50%" id="profile_col1" valign="top">
                    <div class="box_title"><span>Личные данные</span></div>
                    <div class="form_input"><input type="text" placeholder="Электронная почта" data-widget="userDetails"
                                                   data-type="email" data-isRequired="true"
                                                   data-regexpMessage="Это поле обязательно для заполнения"
                                                   readonly="readonly" disabled="disabled"/></div>
                    <div class="form_input"><input type="text" placeholder="Имя" data-widget="userDetails"
                                                   data-type="firstName" data-isRequired="true"/></div>
                    <div class="form_input"><input type="text" placeholder="Фамилия" data-widget="userDetails"
                                                   data-type="lastName" data-isRequired="true"/></div>
                    <div class="form_input">
                        <input type="hidden" placeholder="Телефон" id="profile_phone"/>
                        <input type="hidden" data-widget="userDetails" data-type="countryPhoneCode" value=""/>
                        <input type="hidden" data-widget="userDetails" data-type="areaPhoneCode" value=""/>
                        <input type="text" placeholder="Телефон" data-widget="userDetails" data-type="phone"
                               data-regexpMessage="Это поле обязательно для заполнения"/>
                    </div>
                    <div class="form_input date_box">
                        <span class="date_text_ru">Дата<br/>рождения</span>
                        <select class="select_day" data-widget="userDetails" data-type="birthDay"></select>
                        <select class="select_month" data-widget="userDetails" data-type="birthMonth"></select>
                        <select class="select_year" data-widget="userDetails" data-type="birthYear"
                                data-regexpMessage="Это поле обязательно для заполнения"></select>
                    </div>
                    <div class="reset-password"><a href="#changepassword">Изменить пароль</a></div>
                </td>
                <td width="50%" id="profile_col2" valign="top">
                    <div class="box_title"></div>
                    <div class="form_input country_box">
                        <select class="select_country" data-widget="userDetails" data-type="countryCode"
                                data-isRequired="true"></select>
                    </div>
                    <div class="form_input"><input type="text" placeholder="Город" data-widget="userDetails"
                                                   data-type="city"/></div>
                    <div class="form_input"><input type="text" placeholder="Улица" data-widget="userDetails"
                                                   data-type="addressLine1"/></div>
                    <div class="form_input"><input type="text" placeholder="Дом / Квартира" data-widget="userDetails"
                                                   data-type="addressLine2"/></div>
                    <div class="form_input"><input type="text" placeholder="Дополнительный телефон"
                                                   data-widget="userDetails" data-type="secondaryPhone"
                                                   data-regexpMessage="Это поле обязательно для заполнения"
                                                   name="secondaryPhone"/></div>
                    <div class="profile_submit"><input type="button" value="Сохранить" data-widget="userDetails"
                                                       data-type="submit"/>
                        <div class="myprofile-successmsg" style="display:none;"></div>
                    </div>
                </td>
            </tr>
        </table>
    </form>
    <div id="close_popup4"><span class="profile-close"></span></div>
</div>

<script type="text/javascript">
    if (/(^|;)\s*widgetSession=/.test(document.cookie)) {
        $('.profile-wrap input, .profile-wrap select').attr("data-widget", "userDetails");
    } else {
        $('.profile-wrap input, .profile-wrap select').removeAttr("data-widget");
    }

    $(document).ready(function () {

        $('.profile-wrap input[data-type="phone"]').live("keyup", function (e) {
            $('.changephone-wrap input[data-type="phone"]').val($(this).val());
        });

        $('div.profile-wrap input').live("change", function (e) {
            if ($(this).attr('data-type') == 'phone') {
                $(this).val($(this).val().replace(/[^0-9\(\)\[\]\{\}\-\+"'@\#\$%\^&<>\,\?=_~\\/\|;:\ ]/gim, ''));
            }
        });

        $('div.profile-wrap input').live("keypress", function (e) {
            if ($(this).attr('data-type') == 'phone') {
                if (String.fromCharCode(e.which).match(/[^0-9\(\)\[\]\{\}\-\+"'@\#\$%\^&<>\,\?=_~\\/\|;:\ ]/gim)) {
                    e.preventDefault();
                }
            }
        });

        $('.profile-wrap input[data-type="submit"]').click(function () {
            setTimeout(function () {
                if ($('div.myprofile-successmsg').text() != '') {
                    $('div.myprofile-successmsg').css('display', 'inline-block');
                    setTimeout(function () {
                        $('.login-popup-bg, .profile-wrap').hide();
                    }, 2000);
                }
                if ($('.profile-wrap div.validationError').length > 0) {
                    $('div.myprofile-successmsg').hide();
                }
            }, 1500);
        });

        (function ($) {
            $.fn.extend({
                customStyleMyProfile: function (options) {
                    if (!$.browser.msie || ($.browser.msie && $.browser.version > 6)) {
                        return this.each(function () {
                            var currentSelected = $(this).find(':selected');
                            $(this).after('<span class="' + $(this).attr('class') + '" style=""><span class="customStyleSelectBoxInner">' + currentSelected.text() + '</span></span>').css({fontSize: $(this).next().css('font-size')});
                            var selectBoxSpan = $(this).next();
                            var selectBoxWidth = parseInt($(this).width()) - parseInt(selectBoxSpan.css('padding-left')) - parseInt(selectBoxSpan.css('padding-right'));
                            var selectBoxSpanInner = selectBoxSpan.find(':first-child');
                            selectBoxSpan.css({display: 'inline-block'});
                            selectBoxSpanInner.css({width: selectBoxWidth, display: 'inline-block'});
                            var selectBoxHeight = parseInt(selectBoxSpan.height()) + parseInt(selectBoxSpan.css('padding-top')) + parseInt(selectBoxSpan.css('padding-bottom'));
                            $(this).height(selectBoxHeight).change(function () {
                                selectBoxSpanInner.text($('option:selected', this).text()).parent().addClass('changed');
                            });

                        });
                    }
                }
            });
        })(jQuery);

        setTimeout(function () {
            $('select.select_country, select.select_day, select.select_month, select.select_year').customStyleMyProfile();
        }, 4000);
    });
</script>
