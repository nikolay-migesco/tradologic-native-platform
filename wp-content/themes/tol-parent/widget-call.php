<script>
    $(window).on('contactUsSubmited', function (event, response) {
        //apprise(widgetMessage.ContactUs_ThankYou);
        $('#call_overlay').hide();
        history.pushState("", document.title, window.location.pathname);
    });
    $(window).on('loadingIsCompleted', function (event, response) {

        $('#submit').val(widgetMessage.ContactUs_Button2);

        if (window.location.hash == '#request_call') {
            $('#call_overlay').show();
        }
        $('a[href="#request_call"]').click(function () {
            if ($('#contact_form').length > 0) location.href = '/#request_call';
            else {
                $('#call_overlay').show();
                $('.wrapper-contacts').show();
            }
        });
        $('#call_close').click(function () {
            $('#call_overlay').hide();
            history.pushState("", document.title, window.location.pathname);
        });
    });
</script>
<style type="text/css">
    .input_label {
        margin-left: auto !important;
        vertical-align: middle !important;
    }

    .validationError {
        width: auto !important;
    }

    #call_overlay {
        top: 0;
        left: 0;
        position: fixed;
        width: 100%;
        height: 100%;
        background: rgba(0, 0, 0, .5);
        z-index: 9999999;
    }

    #call_overlay .popup_box {
        width: 404px;
        max-height: 276px;
        margin: auto;
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        background-color: #e8ecf0;
        border: 1px solid grey;
    }

    #call_close {
        width: 11px;
        height: 11px;
        position: absolute;
        right: 10px;
        top: 10px;
        background: url('/wp-content/themes/xptheme-child/images/close.png') no-repeat;
        cursor: pointer;
    }

    #call_form {
        background-color: #e8ecf0;
        padding: 28px 26px 21px 0;
    }

    #call_form input[type="text"] {
        border: 1px solid #cccccc;
        width: 232px;
        height: 30px;
        font: 13px/30px Arial;
        padding-left: 5px;
        padding-right: 5px;
        margin-top: 5px;
        margin-bottom: 5px;
        padding-top: 0;
        padding-bottom: 0;
        border-radius: 0px;
    }

    #call_form textarea {
        border: 1px solid #cccccc;
        width: 232px;
        height: 78px;
        font: 13px Arial;
        padding: 5px;
        margin-top: 5px;
        margin-bottom: 5px;
        resize: none;
        border-radius: 0px;
    }

    #call_form input[type="button"] {
        border: 0;
        width: 244px;
        height: 38px;
        font: 14px/38px Arial;
        color: white;
        text-transform: uppercase;
        text-align: center;
        margin-top: 5px;
        background-color: #69bb1b;
        float: right;
        padding: 0;
        border-radius: 0px;
    }

    #call_form .input_block {
        clear: both;
    }

    #call_form span.input_label {
        display: inline-block;
        width: 130px;
        text-align: right;
        padding-right: 4px;
        color: #222222;
        font: 12px Arial;
    }

    #call_form .validationError {
        float: right;
        position: static;
        margin-top: -2px;
    }

    .header_tooltip_content {
        width: 225px !important;
        background: url(/wp-content/themes/tol-parent/images/header/tooltip.png?11111) left top no-repeat !important;
        height: 99px !important;
    }

    .header_tooltip_content2 {
        width: 233px !important;
        height: 126px !important;
        background: url(/wp-content/themes/tol-parent/images/header/tooltip2.png?3333) left top no-repeat !important;
    }

    #call_form .validationError, #contact_form .validationError {
        color: red !important;
    }
</style>

<?php if (basename(get_permalink()) != 'svyazatsya-s-nami') { ?>
    <div id="call_overlay" style="display:none;">
        <div class="popup_box">

            <div id="call_form">
                <div class="widgetContent contactus-wrapper">
                    <div data-widget="contactUs" data-type="container" class="wrapper-contacts" style="display: none">
                        <div class="input_block"><span class="input_label"
                                                       data-translate="ContactUs_Surname2"></span><input id="name"
                                                                                                         type="text"
                                                                                                         data-widget="contactUs"
                                                                                                         data-type="contactName"
                                                                                                         data-isRequired="true"
                                                                                                         style="width:244px !important;height:30px !important;"/>
                        </div>
                        <div class="input_block"><span class="input_label"
                                                       data-translate="ContactUs_Phone2"></span><input id="phone"
                                                                                                       type="text"
                                                                                                       data-widget="contactUs"
                                                                                                       data-regexpMessage="Введите телефон без знаков “+”, “(”, “)” и “-”, только цифры"
                                                                                                       data-type="phone"
                                                                                                       data-isRequired="true"
                                                                                                       style="width:244px !important;height:30px !important;"/>
                        </div>
                        <div style="display:none;"><span class="input_label">Электронная почта:</span><input id="email"
                                                                                                             value="support@migesco.com"
                                                                                                             type="text"
                                                                                                             data-widget="contactUs"
                                                                                                             data-type="email"
                                                                                                             data-isRequired="false"
                                                                                                             style="width:244px !important;height:30px !important;"/>
                        </div>
                        <div style="display:none;"><span class="input_label">Тема:</span><input id="subject" type="text"
                                                                                                value="Заказать обратный звонок"
                                                                                                data-widget="contactUs"
                                                                                                data-type="subject"
                                                                                                data-isRequired="false"
                                                                                                style="width:244px !important;height:30px !important;"/>
                        </div>
                        <div class="input_block"><span class="input_label" style="vertical-align:top;margin-top:10px;"
                                                       data-translate="ContactUs_Topic2"></span><textarea id="message"
                                                                                                          data-widget="contactUs"
                                                                                                          data-type="contactMessage"
                                                                                                          data-isRequired="true"
                                                                                                          style="width:244px !important;height:78px !important;"></textarea>
                        </div>
                        <div class="input_block"><input type="button" value="Заказать обратный звонок" id="submit"
                                                        data-widget="contactUs" data-type="submit"/></div>
                        <div style="clear:both;"></div>
                    </div>
                    <div data-widget="contactUs" data-type="error" style="display: none"></div>
                </div>
            </div>
            <div id="call_close"></div>

        </div>
    </div>
<?php } ?>
