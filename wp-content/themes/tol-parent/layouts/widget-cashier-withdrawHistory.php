<div id="tol-withdraw-history">
    <div class="well well-sm clearfix flat-top-borders">
        <?php echo do_shortcode('[cashierWithdrawalHistory]'); ?>
        <?php echo do_shortcode('[cashierWithdrawalHistoryPaging]'); ?>
    </div>
</div>