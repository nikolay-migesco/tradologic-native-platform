<?php

register_sidebar(array(
    'name' => 'Slider',
    'id' => 'tol-homepage-slider',
    'description' => 'Tradologic Slider',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));
register_sidebar(array(
    'name' => 'Home Page RSS Feed',
    'id' => 'tol-homepage-rss',
    'description' => 'RSS homepage area',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'FooterMenu',
    'id' => 'tol-footer-menu',
    'description' => 'Footer Menu',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'HpBanner',
    'id' => 'tol-hp-banner',
    'description' => 'HP Banner',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'HpSteps',
    'id' => 'tol-hp-steps',
    'description' => 'HP Steps',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'TradeOnEveryDevice',
    'id' => 'tol-app-store',
    'description' => 'App Store',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'LiveTradingProfits',
    'id' => 'tol-live-profits',
    'description' => 'Live Profits',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'Promotions',
    'id' => 'tol-promotions',
    'description' => 'Promotions',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'AdvancedCharts',
    'id' => 'tol-charts',
    'description' => 'Advanced Charts',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'LeftSidebar',
    'id' => 'sidebar-left',
    'description' => 'Left Sidebar',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'RightSidebar',
    'id' => 'sidebar-right',
    'description' => 'Right Sidebar',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'Disclosure',
    'id' => 'tol-disclosure',
    'description' => 'Footer Disclosure',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'Homepage Area 2',
    'id' => 'sidebar-homepage-2',
    'description' => 'Second homepage area',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));


register_sidebar(array(
    'name' => 'Homepage Area 3',
    'id' => 'sidebar-homepage-3',
    'description' => 'Third homepage area',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'Our Platform',
    'id' => 'sidebar-blueEurope-homepageArea-1',
    'description' => 'BlueEurope homepage area 1',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'Tailor Made',
    'id' => 'sidebar-blueEurope-homepageArea-2',
    'description' => 'BlueEurope homepage area 2',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));
