<?php

register_sidebar(array(
    'name' => 'IntroDescription',
    'id' => 'IntroDescription',
    'description' => 'hp 33 section 1 content',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'Traderoom RSS Feed',
    'id' => 'tol-traderoom-rss',
    'description' => 'RSS Traderoom area',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'Footer Disclosure',
    'id' => 'tol-disclosure',
    'description' => 'Footer Disclosure',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));