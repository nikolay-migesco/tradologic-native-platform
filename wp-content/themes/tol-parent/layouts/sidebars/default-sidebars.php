<?php

    register_sidebar(array(
        'name' => 'Slider',
        'id' => 'tol-homepage-slider',
        'description' => 'Tradologic Slider',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    register_sidebar(array(
        'name' => 'Home Page RSS Feed',
        'id' => 'tol-homepage-rss',
        'description' => 'RSS homepage area',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    register_sidebar(array(
        'name' => 'Traderoom RSS Feed',
        'id' => 'tol-traderoom-rss',
        'description' => 'RSS Traderoom area',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    register_sidebar(array(
        'name' => 'Disclosure',
        'id' => 'tol-disclosure',
        'description' => 'Footer Disclosure',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    register_sidebar(array(
        'name' => 'Homepage Area 2',
        'id' => 'sidebar-homepage-2',
        'description' => 'Second homepage area',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));


    register_sidebar(array(
        'name' => 'Homepage Area 3',
        'id' => 'sidebar-homepage-3',
        'description' => 'Third homepage area',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    register_sidebar(array(
        'name' => 'Our Platform',
        'id' => 'sidebar-blueEurope-homepageArea-1',
        'description' => 'BlueEurope homepage area 1',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    register_sidebar(array(
        'name' => 'Tailor Made',
        'id' => 'sidebar-blueEurope-homepageArea-2',
        'description' => 'BlueEurope homepage area 2',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    register_sidebar(array(
        'name' => 'Google Analytics',
        'id' => 'tol-googleAnalytics',
        'description' => 'Google Analytics',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));