<?php   
            register_sidebar(array(
                'name' => 'Trade assets area',
                'id' => 'sidebar-homepage12-sidebar-1',
                'description' => 'Homepage area centrilized header and content',
                'before_widget' => '<aside id="%1$s" class="widget %2$s">',
                'after_widget' => '</aside>',
                'before_title' => '<h3 class="widget-title">',
                'after_title' => '</h3>',
            ));

            register_sidebar(array(
                'name' => 'Advanced Trading Platform',
                'id' => 'sidebar-homepage12-sidebar-2',
                'description' => 'Advanced Trading Platform Area',
                'before_widget' => '<aside id="%1$s" class="widget %2$s">',
                'after_widget' => '</aside>',
                'before_title' => '<h3 class="widget-title">',
                'after_title' => '</h3>',
            ));
        
            register_sidebar(array(
                'name' => 'Why Us',
                'id' => 'sidebar-homepage12-sidebar-3',
                'description' => 'Why the customer have to choose us',
                'before_widget' => '<aside id="%1$s" class="widget %2$s">',
                'after_widget' => '</aside>',
                'before_title' => '<h3 class="widget-title">',
                'after_title' => '</h3>',
            ));
       
            register_sidebar(array(
                'name' => 'Mobile Section',
                'id' => 'sidebar-homepage12-sidebar-4',
                'description' => 'Mobile section content',
                'before_widget' => '<aside id="%1$s" class="widget %2$s">',
                'after_widget' => '</aside>',
                'before_title' => '<h3 class="widget-title">',
                'after_title' => '</h3>',
            ));
     