<?php

register_sidebar(array(
    'name' => 'Delivers top performance',
    'id' => 'sidebar-homepage30-sidebar-1',
    'description' => 'Homepage area centrilized header and content',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));
register_sidebar(array(
    'name' => 'Trade with confidence',
    'id' => 'sidebar-homepage30-sidebar-2',
    'description' => 'Homepage area centrilized header and content',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));
register_sidebar(array(
    'name' => 'Financial anchor',
    'id' => 'sidebar-homepage30-sidebar-3',
    'description' => 'Homepage area centrilized header and content',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));
register_sidebar(array(
    'name' => 'Home Page RSS Feed',
    'id' => 'tol-homepage-rss',
    'description' => 'RSS homepage area',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'Disclosure',
    'id' => 'tol-disclosure',
    'description' => 'Footer Disclosure',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'What is Binary Options',
    'id' => 'sidebar-homepage16-6',
    'description' => 'What is Binary Options, homepage 6',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'short registration sidebar',
    'id' => 'sidebar-b4binary-short-reg',
    'description' => 'short registration sidebar',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));
if (defined('HOW_TO_TRADE_SIDEBAR') && HOW_TO_TRADE_SIDEBAR) {
    register_sidebar(array(
        'name' => 'How To Trade Section',
        'id' => 'sidebar-homepage16-1',
        'description' => 'How to trade section, homepage area 1',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
}

if (defined('TOP_TRADES_SIDEBAR') && TOP_TRADES_SIDEBAR) {
    register_sidebar(array(
        'name' => 'Top Trades',
        'id' => 'sidebar-homepage16-toptrades',
        'description' => 'Top trades section, homepage',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
}


if (defined('TRADE_ASSETS_SIDEBAR') && TRADE_ASSETS_SIDEBAR) {
    register_sidebar(array(
        'name' => 'Trade Assets',
        'id' => 'sidebar-homepage16-3',
        'description' => 'Tradable assets section, homepage area 4',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
}

if (defined('WHI_B4BINARY_SIDEBAR') && WHI_B4BINARY_SIDEBAR) {
    register_sidebar(array(
        'name' => 'Why B4Binary',
        'id' => 'sidebar-homepage16-5-left',
        'description' => 'Why B4Binary section homepage',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
}
if (defined('TRADE_ANYWHERE_SIDEBAR') && TRADE_ANYWHERE_SIDEBAR) {
    register_sidebar(array(
        'name' => 'Trade Anywhere',
        'id' => 'sidebar-homepage16-5-right',
        'description' => 'Trade Anywhere statement homepage',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
}
if (defined('WHAT_IS_BINARY_SIDEBAR') && WHAT_IS_BINARY_SIDEBAR) {
    register_sidebar(array(
        'name' => 'What is Binary Options',
        'id' => 'sidebar-homepage16-6',
        'description' => 'What is Binary Options, homepage 6',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
}

if (defined('BECOME_AN_AFFILATE_SIDEBAR') && BECOME_AN_AFFILATE_SIDEBAR) {
    register_sidebar(array(
        'name' => 'Become an Affiliate',
        'id' => 'sidebar-homepage16-7',
        'description' => 'Become an Affiliate section homepage',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
}

if (defined('SHORT_REGISTRATION_SIDEBAR') && SHORT_REGISTRATION_SIDEBAR) {
    register_sidebar(array(
        'name' => 'short registration sidebar',
        'id' => 'sidebar-b4binary-short-reg',
        'description' => 'short registration sidebar',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    // start g7trader
    if (defined('SIDEBAR_1_HOMEPAGEAREA_1') && SIDEBAR_1_HOMEPAGEAREA_1) {
        register_sidebar(array(
            'name' => 'Trade assets area',
            'id' => 'sidebar-homepage12-sidebar-1',
            'description' => 'Homepage area centrilized header and content',
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));
    }

    if (defined('SIDEBAR_2_HOMEPAGEAREA_2') && SIDEBAR_2_HOMEPAGEAREA_2) {
        register_sidebar(array(
            'name' => 'Advanced Trading Platform',
            'id' => 'sidebar-homepage12-sidebar-2',
            'description' => 'Advanced Trading Platform Area',
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));
    }

    if (defined('SIDEBAR_3_HOMEPAGEAREA_3') && SIDEBAR_3_HOMEPAGEAREA_3) {
        register_sidebar(array(
            'name' => 'Why Us',
            'id' => 'sidebar-homepage12-sidebar-3',
            'description' => 'Why the customer have to choose us',
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));
    }

    if (defined('SIDEBAR_4_HOMEPAGEAREA_4') && SIDEBAR_4_HOMEPAGEAREA_4) {
        register_sidebar(array(
            'name' => 'Mobile Section',
            'id' => 'sidebar-homepage12-sidebar-4',
            'description' => 'Mobile section content',
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));
    }
    // end g7trader
}

register_sidebar(array(
    'name' => 'Slider',
    'id' => 'tol-homepage-slider',
    'description' => 'Tradologic Slider',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));
register_sidebar(array(
    'name' => 'Home Page RSS Feed',
    'id' => 'tol-homepage-rss',
    'description' => 'RSS homepage area',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'Disclosure',
    'id' => 'tol-disclosure',
    'description' => 'Footer Disclosure',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'How To Trade Section',
    'id' => 'sidebar-homepage16-1',
    'description' => 'How to trade section, homepage area 1',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'Testimonials',
    'id' => 'sidebar-homepage16-testimonials',
    'description' => 'Testimonials section, homepage area 3',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'Top Trades',
    'id' => 'sidebar-homepage16-toptrades',
    'description' => 'Top trades section, homepage',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'Trade Assets',
    'id' => 'sidebar-homepage16-3',
    'description' => 'Tradable assets section, homepage area 4',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'Why B4Binary',
    'id' => 'sidebar-homepage16-5-left',
    'description' => 'Why B4Binary section homepage',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'Trade Anywhere',
    'id' => 'sidebar-homepage16-5-right',
    'description' => 'Trade Anywhere statement homepage',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'What is Binary Options',
    'id' => 'sidebar-homepage16-6',
    'description' => 'What is Binary Options, homepage 6',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'Become an Affiliate',
    'id' => 'sidebar-homepage16-7',
    'description' => 'Become an Affiliate section homepage',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'short registration sidebar',
    'id' => 'sidebar-b4binary-short-reg',
    'description' => 'short registration sidebar',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
));