<?php
wp_enqueue_style("my-profile");
?>
<div id="tol-myProfile-layout-11">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<h1 class="main-temple__section__bannertitle">My Profile</h1>
        <div class="entry-content">
            <div class="container">
                <div class="inner-pages-wrap">
                    <ul class="nav my-profile-menu">
                        <li class="myprofile active">
                            <a href="#tab1" data-toggle="tab" class="active" aria-expanded="true" data-translate="Personal_Details">Personal Details</a>
                        </li>
                        <li class="changepassword-tab">
                            <a href="#tab2" data-toggle="tab" data-translate="Change_Password">Change Password</a>
                        </li>
                        <!--li class="questionnaire-tab">
                            <a href="#tab3" data-toggle="tab" data-translate="Registration_Questionaries">Questionnaire</a>
                        </li-->
                        <li class="uploads-tab">
                            <a href="#tab4" data-toggle="tab" data-translate="Registration_Upload">Upload Documents</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="tab1">
                            <!-- Personal Details -->

                            <div class="widgetContent myprofile-wrapper">
                                <form>
                                    <fieldset class="wrapper userDetails">
                                        <div class="myprofile-successmsg"></div>
                                        <div data-widget="userDetails" data-type="error" style="display: none"></div>
                                        <div style="width:90%;margin-left:auto;margin-right:auto;margin-top: 33px;margin-bottom: 33px;">
                                            <div class="left-side">
                                                <div class="label-field-wrapper text-container email-container">
                                                    <label class="label required">Email:</label>
                                                    <input type="text" data-widget="userDetails" data-type="email" data-isRequired="true" data-regexpMessage="Please enter a valid email address" class="widgetPlaceholder input" />
                                                </div>

                                                <div class="label-field-wrapper text-container title-container">
                                                    <label class="label">Title:</label>
                                                    <span class="select-container">
													<select data-widget="userDetails" data-type="title" class="widgetPlaceholder select"></select>
												</span>
                                                </div>
                                                <div class="label-field-wrapper text-container firstName-container">
                                                    <label class="label required">First Name:</label>
                                                    <input type="text" data-widget="userDetails" data-type="firstName" data-isRequired="true" data-minLength="3" class="widgetPlaceholder input" />
                                                </div>
                                                <div class="label-field-wrapper text-container lastName-container">
                                                    <label class="label required">Last Name:</label>
                                                    <input type="text" data-widget="userDetails" data-type="lastName" data-isRequired="true" data-minLength="3" class="widgetPlaceholder input" />
                                                </div>
                                                <div class="label-field-wrapper text-container countryCode-container">
                                                    <label class="label required">Country:</label>
                                                    <span class="select-container">
                                            <select data-widget="userDetails" data-type="countryCode" data-isRequired="true" class="widgetPlaceholder select"></select>
                                        </span>
                                                </div>
                                                <div class="label-field-wrapper text-container birth-container">
                                                    <label class="label required">Date of birth:</label>
                                                    <span class="select-container birthDay">
                                            <select type="date" data-widget="userDetails" data-type="birthDay" class="widgetPlaceholder"></select>
                                        </span>
                                                    <span class="select-container birthMonth">
                                            <select type="date" data-widget="userDetails" data-type="birthMonth" class="widgetPlaceholder"></select>
                                        </span>
                                                    <span class="select-container birthYear">
                                            <select type="date" data-widget="userDetails" data-type="birthYear" data-regexpMessage="Please enter your date of birth" class="widgetPlaceholder"></select>
                                        </span>
                                                </div>
                                            </div>
                                            <div class="right-side">
                                                <div class="label-field-wrapper text-container addressLine1-container">
                                                    <label class="label">Street:</label>
                                                    <input type="text" data-widget="userDetails" data-type="addressLine1"  class="widgetPlaceholder input" />
                                                </div>
                                                <div class="label-field-wrapper text-container addressLine2-container">
                                                    <label class="label">House / Flat Number:</label>
                                                    <input type="text" data-widget="userDetails" data-type="addressLine2"  class="widgetPlaceholder input" />
                                                </div>
                                                <div class="label-field-wrapper text-container city-container">
                                                    <label class="label">City:</label>
                                                    <input type="text" data-widget="userDetails" data-type="city"  class="widgetPlaceholder input" />
                                                </div>
                                                <div class="label-field-wrapper text-container phone-container">
                                                    <label class="label required">Primary Phone/Mobile:</label>
                                                    <input type="text" data-widget="userDetails" data-type="countryPhoneCode" class="widgetPlaceholder phoneCountryCode" />
                                                    <input type="text" data-widget="userDetails" data-type="areaPhoneCode" class="widgetPlaceholder phoneAreaCode" />
                                                    <input type="text" data-widget="userDetails" data-type="phone" data-regexpMessage="Please enter your Primary Phone" class="widgetPlaceholder phone" />
                                                </div>

                                                <div class="label-field-wrapper text-container secondaryPhone-container">
                                                    <label class="label">Secondary Phone/Mobile:</label>
                                                    <input type="text" data-widget="userDetails" data-type="secondaryPhone" data-regexpMessage="Please enter your Secondary phone/Mobile" name="secondaryPhone" class="widgetPlaceholder input" />
                                                </div>

                                                <div class="label-field-wrapper text-container idExpiryDate-container" style="display:none">
                                                    <label class="label">ID Expiry Date:</label>
                                                    <input type="text" id="IDExpiryDate" maxlength="10" placeholder="DD-MM-YYYY" data-widget="userDetails" data-type="IDExpiryDate" data-regexpMessage="Please enter your ID/Passport Number Expire Date" name="idExpiryDate" class="widgetPlaceholder input" />
                                                    <input type="checkbox" data-type="doesNotExpire" id="doesNotExpire" data-widget="userDetails" value="Does not Expire" class="doesNotExpire" /><label>Does not Expire</label>
                                                </div>

                                                <div class="label-field-wrapper text-container idIssuer-container" style="display:none">
                                                    <label class="label">ID Issuer:</label>
                                                    <input type="text" placeholder="ID Issuer" data-widget="userDetails" data-type="idIssuer" data-regexpMessage="Please enter your ID/Passport Issuer" name="idIssuer" class="widgetPlaceholder input" />
                                                </div>

                                                <div class="label-field-wrapper submit-container">
                                                    <label class="label empty"></label>
                                                    <div data-widget="userDetails" data-type="googleLoginBtn" class="widgetPlaceholder loginGoogle" style>Connect with Google</div>
                                                    <div data-widget="userDetails" data-type="fbLoginBtn" class="widgetPlaceholder linkFacebook">Connect with Facebook</div>
                                                    <div data-widget="userDetails" data-type="fbLoginErrorMessage" class="fbLoginErrorMessage"></div>
                                                    <div data-widget="userDetails" data-type="googleLoginErrorMessage" class="fbLoginErrorMessage"></div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="label-field-wrapper submit-container">
                                                <label class="label empty"></label>
                                                <input type="button" data-widget="userDetails" data-type="submit" value="Submit" class="btn btn-primary" />
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab2">
                            <!-- Change Password -->

                            <div class="widgetContent changepassword-wrapper">
                                <form>
                                    <div data-widget="changePassword" data-type="error" style="display: none" class="error-message"></div>
                                    <fieldset class="wrapper">
                                        <label class="label" data-translate="Current_Password"></label>
                                        <input type="password" data-widget="changePassword" data-type="currentPassword" data-isRequired="true" class="widgetPlaceholder input" />
                                        <br />
                                        <label class="label" data-translate="New_Password">
                                        </label>
                                        <input type="password" data-widget="changePassword" data-type="newPassword" data-isRequired="true" class="widgetPlaceholder input" />
                                        <br />
                                        <label class="label" data-translate="Repeat_Password"></label>
                                        <input type="password" data-widget="changePassword" data-type="confirmPassword" data-isRequired="true" class="widgetPlaceholder input" />
                                        <br />
                                        <div data-widget="changePassword" data-type="passwordStrength" class="passwordStrength"></div>
                                        <label class="label empty"></label>
                                        <input type="button" data-widget="changePassword" data-type="submit" value="Save" class="btn btn-primary" data-translate="Save_button" />
                                        <br />
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                        <!--Questionnaire-->
                        <!--div class="tab-pane fade" id="tab3">
                            <div class="compliance-documents compliance-container row" data-widget="compliance" data-type="section" data-item="questionnaires" >
                                <div class="questionnaireguide" data-translate="questionnareGuide"></div>
                                <div class="questionnaireerror" data-type="questionnaireerror" style="display: none"></div>
                                <div data-widget="compliance" data-type="questions" class="compliance-questions"></div>
                            </div>
                        </di-->
                        <!--End Questionnaire-->
                        <div class="tab-pane fade" id="tab4">
                            <!-- Uploads -->
                            <div class="compliance-documents compliance-container row" data-widget="compliance" data-type="section" data-item="documents" >
                                <div class="documentsguide" data-translate="missingdocuments"></div>
                                <div id="docsguide-upload" class="documentsguide">Upload Document</div>
                                <div class="documenthint"></div>
                                <div data-widget="compliance" data-type="documentsupload"></div>
                                <table class="table table-striped table-responsive">
                                    <thead class="doctable">
                                    <tr>
                                        <th data-translate="documentName"></th>
                                        <th data-translate="documentType"></th>
                                        <th data-translate="documentState"></th>
                                    </tr>
                                    </thead>
                                    <tbody class="uploadeddocs" data-widget="compliance" data-item="uploadedDocs"></tbody>
                                </table>
                            </div>
                            <!--End Uploads-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</div>
<script>
    $(window).on('loadingIsCompleted', function(){
        if (helper.getParameterByName('tab') == 'docs') {
            $('.uploads-tab a').click();
        };

        if (!helper.compliance.showUploadDocuments()) {
            $('.uploads-tab').hide();
            if (!helper.compliance.showQuestionnaire()) {
                $('.questionnaire-tab').hide();
                $('.my-profile-menu li').css('width','50%');
            } else {
                $('.my-profile-menu li').css('width','33.33%');
            };
        } else {
            if (!helper.compliance.showQuestionnaire()) {
                $('.questionnaire-tab').hide();
                $('.my-profile-menu li').css('width','33.33%');
            };
        };
    });
</script>