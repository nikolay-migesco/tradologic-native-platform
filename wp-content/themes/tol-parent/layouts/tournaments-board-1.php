<?php
    wp_enqueue_style('tournaments', get_stylesheet_directory_uri() . '/styles/layouts/tournaments-1.css', array(), NULL, false);
    wp_enqueue_script('customScroll', get_stylesheet_directory_uri() . '/scripts/jquery.mCustomScrollbar.concat.min.js');
    wp_enqueue_style('customScrollStyle', get_stylesheet_directory_uri() . '/styles/jquery.mCustomScrollbar.css');
?>
<div class="container">

        <div class="pageHeadTitle">
             <?php require_once(ABSPATH.'wp-content/themes/tol-parent/layouts/'.BREADCRUMB.'.php' ); ?>
        </div>
        <header class="entry-header">
            <h1 class="entry-title"><?php the_title(); ?></h1>
        </header>

	<div class="row">
		<div data-widget="tournamentTabBoard" data-type="tabsButtons" class="tabsButtons"></div>
		<div data-widget="tournamentTabBoard" data-type="noTournamentsMsg"></div>

		<div data-widget="tournamentTabBoard" data-type="table" data-columns="buttons,header,startDate,endDate,prizeFund,participants"></div>

		<div data-widget="tournamentLeaderboard" data-type="name"></div>
		<div data-widget="tournamentLeaderboard" data-type="conditions"></div>
		<div data-widget="tournamentLeaderboard" data-type="duration"></div>
		<div data-widget="tournamentLeaderboard" data-type="amount"></div>
		<div data-widget="tournamentLeaderboard" data-type="type"></div>
		<div data-widget="tournamentLeaderboard" data-type="games"></div>

		<div data-widget="tournamentLeaderboard" data-type="timer"></div>
		<div data-widget="tournamentLeaderboard" data-type="userRank"></div>
		<div data-widget="tournamentLeaderboard" data-type="finished"></div>

		<div data-widget="tournamentLeaderboard" data-type="table" data-columns="rank,name,country,results,prize"></div>
		<div data-widget="tournamentLeaderboard"  data-type="paging" data-rowsperpage="10"  data-prevnextpage="true"></div>

		<div class="tournamentRegistration-widget-wrapper">
			<div data-widget="tournamentRegistration" data-translate="tournamentRegistration"><br/>
				<input type="button" value="Join" data-widget="tournamentRegistration" data-type="register" class="widgetPlaceholder input" data-translate="Join" style="display: none"/>
				<div data-widget="tournamentRegistration" data-type="error" style="display: none" class="widgetPlaceholder error-message"></div>
			</div>
		</div>
	</div>
</div>