<?php if (ENABLEPRAXIS) { ?>
    <div class="well well-sm clearfix flat-top-borders">
        <div class="praxis-wrapper"></div>

        <script>
            // Create IE + others compatible event handler
            var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
            var eventer = window[eventMethod];
            var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";

            // Listen to message from child window
            eventer(messageEvent,function(e) {
                // OPTIONAL - check if request comes from authorized domain
                //if (e.origin == "https://cashier.yoursite.com") {
                document.getElementById('iframePraxis').height = e.data + 'px';
                // search Cashier iframe by ID and set height property
                //}
            },false);
        </script>

        <script>
            // SAFARI bounceback –redirect Safari clients momentarily to Cashier and return back (cookie created)
            if ( navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1 ){
                if (!iOkun.readCookie('CashierCookie')) {
                    iOkun.createCookie('CashierCookie', '1', 0.0075); // Cookie for 10 minutes
                    window.location.reload();
                };
            };
        </script>
    </div>
<?php } else { ?>
    <div class="well well-sm clearfix flat-top-borders">
        <div action="" class="form-horizontal clearfix">
            <div id="tol-amount-wrapper" class="col-sm-12 tol-deposit-fields">
                <div class="row col-sm-6">
                    <div class="form-group col-sm-12">
                        <label class="hidden-xs col-sm-12 col-md-4 control-label text-left" data-widget="cashierDeposit" data-translate="cashierLabelChooseAmount">Choose amount</label>
                        <div class="col-sm-12 col-md-8 ">
                            <?php echo do_shortcode('[cashierDepositAmount]'); ?>
                        </div>
                    </div>
                </div>
                <div id="tol-active-cc-wrapper" class="col-sm-6 tol-deposit-fields">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label class="hidden-xs col-sm-12 col-md-4 control-label text-left" data-widget="cashierDeposit" data-translate="cashierLabelActiveCC">using</label>
                            <div class="col-sm-12 col-md-8 ">
                                <?php echo do_shortcode('[cashierDepositActiveCC]'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="tol-method-wrapper" class="col-sm-12 tol-deposit-fields">
                <div class="row col-sm-6">
                    <div class="form-group col-sm-12">
                        <label class="hidden-xs col-sm-12 col-md-4 control-label text-left" data-widget="cashierDeposit" data-translate="cashierLabelChooseMethod">Choose method</label>
                        <div class="col-sm-12 col-md-8 ">
                            <?php echo do_shortcode('[cashierDepositMethod]'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="tol-currency-wrapper" class="col-sm-12 tol-deposit-fields" style="display: none;">
                <div class="row col-sm-6">
                    <div class="form-group col-sm-12">
                        <label class="hidden-xs col-sm-12 col-md-4 control-label text-left" data-widget="cashierDeposit" data-translate="cashierLabelChooseCurrency">Choose currency</label>
                        <div class="col-sm-12 col-md-8 ">
                            <?php echo do_shortcode('[cashierDepositCurrency]'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="tol-promoCode-wrapper" class="col-sm-12 tol-deposit-fields" style="display: none;">
                <div class="row col-sm-6 ">
                    <div class="form-group col-sm-12">
                        <label class="hidden-xs col-sm-12 col-md-4 control-label text-left" data-translate="cashierLabelPromoCode">Promo code</label>
                        <div class="col-sm-12 col-md-8 ">
                            <?php echo do_shortcode('[cashierDepositPromoCode]'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div data-widget="cashierDeposit" data-type="additionalFieldsPlaceholder" class="col-sm-12 tol-additional-fields tol-deposit-fields"></div>
            <div class="cashierField wireTransfer" data-widget="cashierDeposit" data-type="wireTransferHolder" style="display: none"></div>
            <!--div class="col-sm-12 tol-deposit-fields">
                <div class="col-sm-6 depositSummary">
                    <p>Summary:</p>
                    <p>Deposit <span class="depositAmount"></span> <span class="depositCurr"></span> via <span class="depositType"></span></p>
                </div>
                <div class="col-sm-6 text-right">
                    <span data-translate="cashierHavePromoCode" class="promoCodeLink"></span>
                </div>
            </div-->
            <span class="cashierRedirectText" data-translate="cashierRedirectText" style="display:none"></span><br /><br />
            <input type="button" data-widget="cashierDeposit" class="redirectToGatewayBTN depositBTN btn btn-primary" data-type="redirectToGateway" value="Go To Provider Gateway" style="display:none" />
            <span data-widget="cashierDeposit" data-type="formPlaceholder" style="display:none"></span>
            <div class="col-sm-12">
                <hr>
            </div>
            <div class="form-group col-xs-12 col-sm-12">
                <div class="col-sm-6 hidden-xs"></div>
                <div class="col-sm-6 text-right">
                    <?php echo do_shortcode('[cashierDepositButton]'); ?>
                </div>
            </div>
        </div>
        <?php echo do_shortcode('[cashierIframeHolder]'); ?>
    </div>
    </div>
    <div data-widget="cashierDeposit" data-type="promoCodeValidation" style="display: none">
        <span data-widget="cashierDeposit" data-translate="cashierLabelPromoCodeValidation"></span><br />
    </div>
<?php } ?>