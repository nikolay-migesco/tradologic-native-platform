<?php
$cashier_page = 'deposit';

if (isset($_GET['cpage'])) {

    $current_url = explode('?', $_GET['cpage']);
    $baseName = $current_url[0];

    switch (@$baseName) {

        case 'ccdeposit':
            $cashier_page = 'ccdeposit';
            break;
        case 'depositSuccess':
            $cashier_page = 'depositSuccess';
            //setcookie('cpage', 'depositSuccess', 0, '/');
            break;
        case 'transfer':
            $cashier_page = 'comingSoon';
            //setcookie('cpage', 'comingSoon', 0, '/');
            break;
        case 'withdraw':
            $cashier_page = 'withdraw';
            // setcookie('cpage', 'withdraw', 0, '/');
            break;
        case 'withdrawHistory':
            $cashier_page = 'withdrawHistory';
            // setcookie('cpage', 'withdrawHistory', 0, '/');
            break;
        case 'bonusHistory':
            $cashier_page = 'bonusHistory';
            // setcookie('cpage', 'bonusHistory', 0, '/');
            break;
        case 'transactionHistory':
            $cashier_page = 'transactionHistory';
            // setcookie('cpage', 'transactionHistory', 0, '/');
            break;
        default :
            $cashier_page = 'deposit';
            // setcookie('cpage', 'deposit', 0, '/');
            break;
    }
}
?>

<div id="tol-cashier-layout-1" class="container">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="row">
            <div id="tol-cashier">
                <div class="pageHeadTitle">
                    <?php require_once(ABSPATH . 'wp-content/themes/tol-parent/layouts/' . BREADCRUMB . '.php' ); ?>
                </div>
                <header class="entry-header">
                    <h1 class="entry-title"><?php the_title(); ?></h1>
                </header>

                <div id="loadingElementCashier" style="display:none;"></div>
                <div id="loadingElementProcessing" style="display:none;"></div>
                <nav id="tol-cashier-main-nav">
                    <ul class="nav nav-tabs">
                        <li>
                            <a href="?cpage=deposit" class="<?php echo($cashier_page == 'deposit' ? 'active' : '') ?><?php echo($cashier_page == 'depositSuccess' ? 'active' : '') ?><?php echo($cashier_page == 'ccdeposit' ? 'active' : '') ?>">
                                <span class="uppercase" data-translate="depositPageTitle"></span>
                                <i class="show-xs hidden-sm hidden-md hidden-lg glyphicon glyphicon-piggy-bank"></i>
                            </a>
                        </li>
                        <li>
                            <a href="?cpage=withdraw" class="<?php echo($cashier_page == 'withdraw' ? 'active' : '') ?>">
                                <span class="uppercase" data-translate="withdrawPageTitle"></span>
                                <i class="show-xs hidden-sm hidden-md hidden-lg glyphicon glyphicon-log-out"></i>
                            </a>
                        </li>
                        <li>
                            <a href="?cpage=withdrawHistory" class="<?php echo($cashier_page == 'withdrawHistory' ? 'active' : '') ?>">
                                <span class="uppercase" data-translate="withdrawHistoryPageTitle"></span>
                                <i class="show-xs hidden-sm hidden-md hidden-lg glyphicon glyphicon-list-alt"></i>
                            </a>
                        </li>
                        <li>
                            <a href="?cpage=bonusHistory" class="<?php echo($cashier_page == 'bonusHistory' ? 'active' : '') ?>">
                                <span class="uppercase" data-translate="bonusHistoryPageTableHeadTitle"></span>
                                <i class="show-xs hidden-sm hidden-md hidden-lg glyphicon glyphicon-gift"></i>
                            </a>
                        </li>
                        <li>
                            <a href="?cpage=transactionHistory" class="<?php echo($cashier_page == 'transactionHistory' ? 'active' : '') ?>">
                                <span class="uppercase" data-translate="transactionHistoryPageTitle"></span>
                                <i class="show-xs hidden-sm hidden-md hidden-lg glyphicon glyphicon-header"></i></a>
                        </li>
                    </ul>
                </nav>
                <div class="clearfix">
                    <?php
                    if ($cashier_page != 'deposit') {
                        get_template_part('layouts/widget', 'cashier-' . $cashier_page);
                    } else {
                        echo $post->post_content;
                        echo do_shortcode('[ninja_form id=1]');
                    }
                    ?>
                </div>
            </div>
            <div class="entry-content info-after-cashier">
                <h3 data-translate="contact-us-title"></h3>
                <?php if (CONTACTUS_EMAIL) { ?>
                    <div class=""><span class="bold">Email:</span> <a href="mailto:<?php echo CONTACTUS_EMAIL; ?>"><?php echo CONTACTUS_EMAIL; ?></a>, via our live-chat or</div>
                <?php } ?>
                <?php if (ENGLISH_PHONE) { ?>
                    <div class=""><span class="bold">Phone:</span> <?php echo ENGLISH_PHONE; ?></div>
                <?php } ?>
            </div>           
        </div>
    </article>
</div>
<script type="text/javascript">
    $(window).on('loadingIsCompleted', function () {
        $('.email-bitcoin-wrapper .ninja-forms-field[type=email]').val(widgets.user.email).trigger( 'change' );
    });
</script>