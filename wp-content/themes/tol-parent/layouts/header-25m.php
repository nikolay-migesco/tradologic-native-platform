<!DOCTYPE html <?php language_attributes(); ?>>
<!--[if lt IE 7 ]>
<html class="ie ie6 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>
<html class="ie ie7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>
<html class="ie ie9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js lang-<?php echo ICL_LANGUAGE_CODE; ?>">
<!--<![endif]-->

    <head>
        <?php require_once 'header.php'; ?>
        <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
        <!-- Metas Page details-->
        <title><?php wp_title(' | ', true, 'right'); ?><?php bloginfo('name'); ?></title>
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!--google font style-->
        <link href='//fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
        <script src="/wp-content/themes/tol-parent/scripts/select2.min.js"></script>
        <!-- Favicons -->
        <link rel="shortcut icon" href="/wp-content/themes/tol-child/images/fav.ico">
        <link rel="apple-touch-icon" href="../images/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="../images/apple-touch-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="../images/apple-touch-114x114.png">
    </head>
    <body id="top" <?php body_class(); ?>>
     <!-- START PostionID history popup -->
     <div id="tol-position-history" tabindex="-1" role="dialog" style="height: auto; width: 320px; display: none;" class="ui-dialog ui-corner-all ui-widget ui-widget-content ui-front ui-draggable forex-poshistory-dialog">
            <div class="ui-dialog-titlebar ui-corner-all ui-widget-header ui-helper-clearfix ui-draggable-handle">
                <span class="ui-dialog-title">Position History</span>
                <button type="button" class="ui-button ui-corner-all ui-widget ui-button-icon-only ui-dialog-titlebar-close" data-dismiss="modal" title="Close">
                    <span class="ui-button-icon ui-icon ui-icon-closethick"></span>
                    <span class="ui-button-icon-space"> </span>
                    Close
                </button>
            </div>
            <div class="asset-orderid">
                <div class="forex-position-id"></div>
                <div class="forex-posid-asset"></div>
                <div class="forex-profit-title two-lines">Profit</div>
                <div class="forex-posid-profit"></div>
            </div>
            <div class="position-history-content ui-dialog-content ui-widget-content">
                <div class="position-history-body"></div>
            </div>
        </div>
        <!-- END PostionID history popup -->
        <!-- SOCIAL PROFILE POP UP -->
        <div id="tol-uncopy-popup" data-widget="realForex-forexSocialProfile" data-type="profile-uncopy-popup" data-keyboard="false" data-backdrop="static" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="tol-social-label">
            <div class="modal-dialog modal-lg modal-default" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="switch-btns">
                            <div class="uncopy-header-title">
                                Uncopy from master trader <span data-widget="realForex-forexSocialProfile" data-type="nickname" class="blue-text"></span>
                            </div>
                            <button data-widget="realForex-forexSocialProfile" data-type="profile-uncopy-close" type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <h4 class="modal-title" id="tol-social-label">Confirm Uncopy</h4>
                    </div>
                    <div class="modal-body">
                        <!--                            <div class="uncopy-header-title">-->
                        Please note that all positions copied from <span data-widget="realForex-forexSocialProfile" data-type="nickname" class="blue-text"></span> will be unlinked. Your positions will NOT close until you close them yourself.
                        <!--                            </div>-->
                    </div>
                    <div class="modal-footer">
                        <button data-widget="realForex-forexSocialProfile"  data-type="profile-uncopy-close" type="button" class="btn btn-primary uncopyClose" data-dismiss="modal">Close</button>
                        <button data-widget="realForex-forexSocialProfile"  data-type="profile-uncopy-confirm" type="button" class="btn btn-primary">Uncopy</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="tol-social-popup" data-widget="realForex-forexSocialProfile" data-type="container" data-keyboard="false" data-backdrop="static" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="tol-social-label">
            <div class="modal-dialog modal-lg modal-default" role="document">
                <div class="modal-content">
                    <div class="profile-loader" data-widget="realForex-forexSocialProfile" data-type="loader" style="display: none;">
                        <div class="loader">
                            <span class="loader-block"></span>
                            <span class="loader-block"></span>
                            <span class="loader-block"></span>
                            <span class="loader-block"></span>
                            <span class="loader-block"></span>
                            <span class="loader-block"></span>
                            <span class="loader-block"></span>
                            <span class="loader-block"></span>
                            <span class="loader-block"></span>
                        </div>
                    </div>
                    <div class="modal-header">
                        <div class="switch-btns">
                            <div data-widget="realForex-forexSocialProfile" data-type="previous-profile" data-id="" class="social-prev-profile disabled"><</div>
                            <div data-widget="realForex-forexSocialProfile" data-type="next-profile" data-id="" class="social-next-profile disabled">></div>
                        </div>
                        <div data-widget="realForex-forexSocialProfile" data-type="nickname-error" class="nickname-error"></div>
                        <button data-widget="realForex-forexSocialProfile" data-type="close" type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 data-widget="realForex-forexSocialProfile" data-type="container-title" class="modal-title" id="tol-social-label"></h4>
                    </div>
                    <div class="modal-body">
                        <div class="leader-profile-left">
                            <div class="trader-name">
                                <span data-widget="realForex-forexSocialProfile" data-type="nickname"></span>
                                <span data-widget="realForex-forexSocialProfile" data-type="edit-nickname" class="edit-nickname"></span>
                                <input data-widget="realForex-forexSocialProfile" data-type="input-nickname" type="text" class="input-nickname" value="" style="display:none;" />
                            </div>
                            <span data-widget="realForex-forexSocialProfile" data-type="avatar" class="popup-avatar"></span>
                            <div data-widget="realForex-forexSocialProfile" data-type="profile-copy-link-button" class="profile-copy-button">Copy</div>
                            <div data-widget="realForex-forexSocialProfile" data-type="change-avatar" class="change-avatar-btn">Edit avatar</div>
                            <div class="trader-profile-copiers">
                                <span class="copiers-title">COPIERS</span>
                                <span data-widget="realForex-forexSocialProfile" data-type="profile-trader-copiers" class="copiers-value"></span>
                            </div>
                            <div class="trader-profile-about">
                                <div class="trader-about-title">About Me</div>
                                <div data-widget="realForex-forexSocialProfile" data-type="aboutme" title="" class="trader-about-content" ></div>
                                <span data-widget="realForex-forexSocialProfile" data-type="edit-aboutme" class="edit-aboutme"></span>
                                <textarea data-widget="realForex-forexSocialProfile" data-type="input-aboutme" type="text" class="input-aboutme" value="" style="display:none;"></textarea>
                            </div>
                        </div>
                        <div class="leader-profile-right">
                            <ul class="nav trader-popup-nav">
                                <li class="stats-tab active">
                                    <a href="#tab31" data-widget="realForex-forexSocialProfile" data-type="stats-tab" data-toggle="tab" class="active" aria-expanded="true"><span>Stats</span></a>
                                </li>
                                <li class="portfolio-tab">
                                    <a href="#tab32" data-widget="realForex-forexSocialProfile" data-type="portfolio-tab" data-toggle="tab"><span>Portfolio</span></a>
                                </li>
                                <li class="community-tab">
                                    <a href="#tab33" data-widget="realForex-forexSocialProfile" data-type="community-tab" data-toggle="tab"><span>Community</span></a>
                                </li>
                                <li class="copy-tab">
                                    <a href="#tab34" data-widget="realForex-forexSocialProfile" data-type="copy-tab" data-toggle="tab"><span>Copy</span></a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane stats-pane fade active in" id="tab31">
                                    <div class="stats-wrapper">

                                        <div class="stats-header">
                                            <div class="stats-pandl" data-widget="realForex-forexSocialProfile" data-type="stats-header"></div>
                                            <div class="stats-time-frame">
                                                <span class="stats-time-title">Time Period</span>
                                                <span data-widget="realForex-forexSocialProfile" data-type="stats-timeboxes" data-time="1" class="stats-time-box selected">1m</span>
                                                <span data-widget="realForex-forexSocialProfile" data-type="stats-timeboxes" data-time="3" class="stats-time-box">3m</span>
                                                <span data-widget="realForex-forexSocialProfile" data-type="stats-timeboxes" data-time="6" class="stats-time-box">6m</span>
                                                <span data-widget="realForex-forexSocialProfile" data-type="stats-timeboxes" data-time="12" class="stats-time-box">1y</span>
                                            </div>
                                        </div>

                                        <div class="stats-body" data-widget="realForex-forexSocialProfile" data-type="stats-chart"></div>

                                        <div class="stats-footer">
                                            <div class="col-sm-4 stats-footer-btn stats-footer-btn1">
                                                <span class="footer-btn-title">Success Rate</span>
                                                <span data-widget="realForex-forexSocialProfile" data-type="stats-footer-srate" class="footer-btn-value">+58.97%</span>
                                            </div>
                                            <div class="col-sm-4 stats-footer-btn stats-footer-btn2">
                                                <span class="footer-btn-title">Best Position</span>
                                                <span data-widget="realForex-forexSocialProfile" data-type="stats-footer-best" class="footer-btn-value">+158.97%</span>
                                            </div>
                                            <div class="col-sm-4 stats-footer-btn stats-footer-btn3">
                                                <span class="footer-btn-title">Worst Position</span>
                                                <span data-widget="realForex-forexSocialProfile" data-type="stats-footer-worst" class="footer-btn-value">-234.97%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tab32">
                                    <div class="portfolio-wrapper">
                                        <div class="portfolio-header">
                                            <div class="portfolio-time-frame">
                                                <span class="stats-time-title">Time Period</span>
                                                <span data-widget="realForex-forexSocialProfile" data-type="portfolio-timeboxes" data-time="1" class="stats-time-box selected">1m</span>
                                                <span data-widget="realForex-forexSocialProfile" data-type="portfolio-timeboxes" data-time="3" class="stats-time-box">3m</span>
                                                <span data-widget="realForex-forexSocialProfile" data-type="portfolio-timeboxes" data-time="6" class="stats-time-box">6m</span>
                                                <span data-widget="realForex-forexSocialProfile" data-type="portfolio-timeboxes" data-time="12" class="stats-time-box">1y</span>
                                            </div>
                                        </div>
                                        <div class="portfolio-body" data-widget="realForex-forexSocialProfile" data-type="portfolio-chart"></div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tab33">
                                    <div class="community-wrapper">
                                        <ul class="nav trader-popup-nav">
                                            <li class="copiers-tab active">
                                                <a href="#tab21" data-toggle="tab" class="active" aria-expanded="true">
                                                    <span>COPIERS</span>
                                                    <span data-widget="realForex-forexSocialProfile" data-type="copiers-count"></span>
                                                </a>
                                            </li>
                                            <li class="copying-tab">
                                                <a href="#tab22" data-toggle="tab">
                                                    <span>COPYING</span>
                                                    <span data-widget="realForex-forexSocialProfile" data-type="copying-count"></span>
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane dataTable fade active in" id="tab21">
                                                <div class="scrollable-area scrollable-area-at-top">
                                                    <div class="scrollable-area-body">
                                                        <div class="scrollable-area-content">
                                                            <table class="popup-social copiers-copying hasArrows" data-widget="realForex-forexSocialProfile" data-type="profile-copiers-table">
                                                                <thead data-type="profile-copiers-table-header">
                                                                <tr>
                                                                    <th data-column="avatar" data-sort="string" class="canSort canResize canDrag tarder-avatar">
                                                                        <div class="cellWrapper">
                                                                            <span class="content">Name</span>
                                                                        </div>
                                                                    </th>
                                                                    <th data-column="name" data-sort="string" class="canSort canResize canDrag">
                                                                        <div class="cellWrapper">
                                                                            <span class="content"></span>
                                                                        </div>
                                                                    </th>
                                                                    <th data-column="plperc" data-sort="int" class="pandlcol canSort canResize canDrag">
                                                                        <div class="cellWrapper">
                                                                            <span class="content">P/L(%)</span>
                                                                        </div>
                                                                    </th>
                                                                    <th data-column="action" data-sort="string" class="copycol canSort canResize canDrag">
                                                                        <div class="cellWrapper">
                                                                            <span class="content">Copy</span>
                                                                        </div>
                                                                    </th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane dataTable fade" id="tab22">
                                                <div class="scrollable-area scrollable-area-at-top">
                                                    <div class="scrollable-area-body">
                                                        <div class="scrollable-area-content">
                                                            <table class="popup-social copiers-copying hasArrows" data-widget="realForex-forexSocialProfile" data-type="profile-copying-table">
                                                                <thead>
                                                                <tr>
                                                                    <th data-column="avatar" data-sort="string" class="canSort canResize canDrag tarder-avatar">
                                                                        <div class="cellWrapper">
                                                                            <span class="content">Name</span>
                                                                        </div>
                                                                    </th>
                                                                    <th data-column="name" data-sort="string" class="canSort canResize canDrag">
                                                                        <div class="cellWrapper">
                                                                            <span class="content"></span>
                                                                        </div>
                                                                    </th>
                                                                    <th data-column="plperc" data-sort="int" class="pandlcol canSort canResize canDrag">
                                                                        <div class="cellWrapper">
                                                                            <span class="content">P/L(%)</span>
                                                                        </div>
                                                                    </th>
                                                                    <th data-column="mypl" data-sort="int" class="mypl canSort canResize canDrag">
                                                                        <div class="cellWrapper">
                                                                            <span class="content">My P/L</span>
                                                                        </div>
                                                                    </th>
                                                                    <th data-column="action" data-sort="string" class="copycol canSort canResize canDrag">
                                                                        <div class="cellWrapper">
                                                                            <span class="content">Copy</span>
                                                                        </div>
                                                                    </th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane copy-pane fade" id="tab34">
                                    <div class="copy-wrapper">
                                        <a href="#" class="info-icon" data-toggle="tooltip" data-placement="right" title="The Percentage is the ration between the amount of the trade at the provider versus the amount that will open in your account"></a>
                                        <div class="copy-header">
                                            <div class="copy-header-title">
                                                Copy <span data-widget="realForex-forexSocialProfile" data-type="nickname" class="blue-text"></span> strategy proportinally on:
                                            </div>
                                            <div class="copy-header-body" data-type="profile-proportion-holder">
                                                <div class="asset-type-wrapper narrow">
                                                    <div type="button" class="nav-button button asset-type narrow" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <span data-widget="realForex-forexSocialProfile" data-type="profile-proportion-selected"></span>
                                                        <span class="dropdown-arrow"></span>
                                                    </div>
                                                    <ul class="dropdown-menu" data-type="profile-proportion-list" data-widget="realForex-forexSocialProfile">
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="copy-body">
                                            <div class="copy-body-row">
                                                <span class="text-row">If <span data-widget="realForex-forexSocialProfile" data-type="nickname" class="blue-text"></span> place 10,000 units trade in his account, then <span data-widget="realForex-forexSocialProfile" data-type="example-units" class="blue-text">10000</span> units trade on the same asset will be executed in your account.</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="copy-footer">
                                        <div class="copy-footer-title">Stop Copying if losses exceeded:</div>
                                        <div class="copy-header-body">
                                            <input class="profile-popup-input" data-widget="realForex-forexSocialProfile" data-type="profile-stoploss" type="text" value="" oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
                                        </div>
                                    </div>
                                    <div class="profile-copy-holder" data-widget="realForex-forexSocialProfile" data-type="profile-copy-holder"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END SOCIAL PROFILE POP UP-->
        <!-- SOCIAL SEARCH POP UP -->
        <div id="tol-explore-popup" data-widget="realForex-forexAdvancedSearch" data-type="container" class="modal fade" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="tol-explore-label">
            <div class="modal-dialog modal-sm modal-default" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="tol-social-label">Advanced Search</h4>
                        <h4 class="modal-title">Search Tool</h4>
                    </div>
                    <div class="modal-body explore-search-fields">
                        <div class="explore-search-row explore-search-row-timeperiod">
                            <span class="explore-search-title">Time Period</span>
                            <div class="time-box-wrapper">
                                <span data-widget="realForex-forexAdvancedSearch" data-type="timeboxes" data-time="1" class="stats-time-box selected">1m</span>
                                <span data-widget="realForex-forexAdvancedSearch" data-type="timeboxes" data-time="3"class="stats-time-box">3m</span>
                                <span data-widget="realForex-forexAdvancedSearch" data-type="timeboxes" data-time="6" class="stats-time-box">6m</span>
                                <span data-widget="realForex-forexAdvancedSearch" data-type="timeboxes" data-time="12" class="stats-time-box">1y</span>
                            </div>
                        </div>
                        <div class="explore-search-row explore-search-row-pl">
                            <span class="explore-search-title">P/L</span>
                            <div class="less-than-wrapper">
                                <div type="button" class="nav-button button less-than" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span data-widget="realForex-forexAdvancedSearch" data-type="pltext">More</span>
                                    <span class="dropdown-arrow"></span>
                                </div>
                                <ul class="dropdown-menu">
                                    <li data-widget="realForex-forexAdvancedSearch" data-type="plDdl" direction="1">Less</li>
                                    <li data-widget="realForex-forexAdvancedSearch" data-type="plDdl" direction="2">More</li>
                                </ul>
                            </div>
                            <span class="explore-search-than">than</span>
                            <input data-widget="realForex-forexAdvancedSearch" data-type="plVal" class="advanced-search-percent" type="text" oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" value ="0%"/>
                        </div>
                        <div class="explore-search-row explore-search-row-copiers">
                            <span class="explore-search-title">Copiers</span>
                            <div class="less-than-wrapper">
                                <div type="button" class="nav-button button less-than" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span data-widget="realForex-forexAdvancedSearch" data-type="copierstext">More</span>
                                    <span class="dropdown-arrow"></span>
                                </div>
                                <ul class="dropdown-menu">
                                    <li data-widget="realForex-forexAdvancedSearch" data-type="copiersDdl" direction="1">Less</li>
                                    <li data-widget="realForex-forexAdvancedSearch" data-type="copiersDdl" direction="2">More</li>
                                </ul>
                            </div>
                            <span class="explore-search-than">than</span>
                            <input data-widget="realForex-forexAdvancedSearch" data-type="copiersVal" class="advanced-search-percent" type="number" min="0" step="1" value ="0"/>
                        </div>
                        <div class="explore-search-row explore-search-row-srate">
                            <span class="explore-search-title">Success Rate</span>
                            <div class="less-than-wrapper">
                                <div type="button" class="nav-button button less-than" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span data-widget="realForex-forexAdvancedSearch" data-type="ratetext">More</span>
                                    <span class="dropdown-arrow"></span>
                                </div>
                                <ul class="dropdown-menu">
                                    <li data-widget="realForex-forexAdvancedSearch" data-type="sRateDdl" direction="1">Less</li>
                                    <li data-widget="realForex-forexAdvancedSearch" data-type="sRateDdl" direction="2">More</li>
                                </ul>
                            </div>
                            <span class="explore-search-than">than</span>
                            <input data-widget="realForex-forexAdvancedSearch" data-type="sRateVal" class="advanced-search-percent" type="text" oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" value ="0%"/>
                        </div>
                        <div class="explore-search-row explore-search-row-asset-type">
                            <span class="explore-search-title">Asset Type</span>
                            <div class="asset-type-wrapper">
                                <div type="button" class="nav-button button less-than" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span data-widget="realForex-forexAdvancedSearch" data-type="assettext">All</span>
                                    <span class="dropdown-arrow"></span>
                                </div>
                                <ul class="dropdown-menu">
                                    <li data-widget="realForex-forexAdvancedSearch" data-type="assetType" astype="">All</li>
                                    <li data-widget="realForex-forexAdvancedSearch" data-type="assetType" astype="1">Currencies</li>
                                    <li data-widget="realForex-forexAdvancedSearch" data-type="assetType" astype="2">Indices</li>
                                    <li data-widget="realForex-forexAdvancedSearch" data-type="assetType" astype="3">Stocks</li>
                                    <li data-widget="realForex-forexAdvancedSearch" data-type="assetType" astype="4">Commodities</li>
                                    <li data-widget="realForex-forexAdvancedSearch" data-type="assetType" astype="5">Futures</li>
                                </ul>
                            </div>
                        </div>
                        <div data-widget="realForex-forexAdvancedSearch" data-type="submit" class="advanced-search-btn">Search</div>
                    </div>
                    <div class="modal-header">
                        <h4 class="modal-title">Search Results</h4>
                    </div>
                    <div class="modal-body explore-results dataTable">
                        <table class="fake-search-thead traderoom-social hasArrows">
                            <thead>
                            <tr>
                                <th data-column="name" data-sort="string" class="trader-avatar canSort canResize canDrag"><span class="content">Name</span></th>
                                <th data-column="pl" data-sort="int" class="pandl canSort canResize canDrag"><span class="content">P/L(%)</span></th>
                                <th data-column="copiers" data-sort="string" class="copiers canSort canResize canDrag"><span class="content">Copiers</span></th>
                                <th data-column="srate" data-sort="int" class="success-rate canSort canResize canDrag"><span class="content">Success Rate</span></th>
                                <th data-column="copy" data-sort="string" class="copy canSort canResize canDrag"><span class="content">Copy</span></th>
                            </tr>
                            </thead>
                        </table>
                        <div class="scrollable-search-area">
                            <div class="scrollable-area-body">
                                <div class="scrollable-area-content">
                                    <table data-widget="realForex-forexAdvancedSearch" data-type="table" class="real-search-thead traderoom-social hasArrows">
                                        <thead>
                                        <tr>
                                            <th data-column="name" data-sort="string" class="trader-avatar canSort canResize canDrag">
                                                <span class="content">Name</span>
                                            </th>
                                            <th data-column="pl" data-sort="int" class="pandl canSort canResize canDrag">
                                                <span class="content">P/L(%)</span>
                                            </th>
                                            <th data-column="copiers" data-sort="string" class="copiers canSort canResize canDrag">
                                                <span class="content">Copiers</span>
                                            </th>
                                            <th data-column="srate" data-sort="int" class="success-rate canSort canResize canDrag">
                                                <span class="content">Success Rate</span>
                                            </th>
                                            <th data-column="copy" data-sort="string" class="copy canSort canResize canDrag">
                                                <span class="content">Copy</span>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END SOCIAL SEARCH POP UP -->
        <!-- MOBILE MENU -->
        <div id="mobilemenu-shadow"></div>
        <div id="mobilemenu" class="tol-wp-navigation">
            <?php
            wp_nav_menu(
                array(
                    'menu' => 'Primary Menu',
                    'theme_location' => 'primary',
                    'container' => 'false',
                    'menu_class' => 'nav navbar-nav main-nav'
                )
            );
            ?>
            <div class="tol-language-switcher">
                <?php do_action('wpml_add_language_selector'); ?>
            </div>
        </div>
        <!-- END MOBILE MENU -->
        <!-- START PostionID history popup -->
        <div id="tol-position-history" tabindex="-1" role="dialog" style="height: auto; width: 320px; display: none;" class="ui-dialog ui-corner-all ui-widget ui-widget-content ui-front ui-draggable forex-poshistory-dialog">
            <div class="ui-dialog-titlebar ui-corner-all ui-widget-header ui-helper-clearfix ui-draggable-handle">
                <span class="ui-dialog-title">Position History</span>
                <button type="button" class="ui-button ui-corner-all ui-widget ui-button-icon-only ui-dialog-titlebar-close" data-dismiss="modal" title="Close">
                    <span class="ui-button-icon ui-icon ui-icon-closethick"></span>
                    <span class="ui-button-icon-space"> </span>
                    Close
                </button>
            </div>
            <div class="asset-orderid">
                <div class="forex-position-id"></div>
                <div class="forex-posid-asset"></div>
                <div class="forex-profit-title two-lines">Profit</div>
                <div class="forex-posid-profit"></div>
            </div>
            <div class="position-history-content ui-dialog-content ui-widget-content">
                <div class="position-history-body"></div>
            </div>
        </div>
        <!-- END PostionID history popup -->
        <div id="tol-header-layout-25">
            <header id="tol-header">
                <div class="navigation" id="navigation">
                    <div class="cf">
                        <div class="nav-button has-icon main-menu">
                            <div id="toggle-menu" class="main-menu-icon">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                        <div class="nav_logo">
                            <a class="navbar-brand" href="#"></a>
                        </div>
                        <div class="right-buttons">
                            <div class="tol-login-userbar-container">
                                <!-- SIGN IN NAVIGATION -->
                                <div id="tol-login-area" class="nav navbar-nav user-nav tolUserIsNotLogged" style="display:none;">
                                    <div class="nav-button button tol-login-account" data-toggle="modal" data-target="#tol-login-popup"><span class="icon icon-card"></span><span data-translate="UserData_userBlock_Login"></span></div>
                                    <div class="nav-button button open-real-account tol-create-account" data-translate="UserData_userBlock_CreateAccount"></div>
                                </div>
                                <!-- END SIGN IN NAVIGATION -->
                                <!-- USER BAR - FOR LOGGED USER -->
                                <ul id="tol-userbar" class="nav navbar-nav user-nav tolUserIsLogged" style="display:none">
                                    <li>
                                        <?php if (strpos($_SERVER['REQUEST_URI'],'cashier') == false) { ?>
                                            <div class="nav-button button open-real-account tol-quick-deposit">Trade with Real Money</div>
                                        <?php } else { ?>
                                            <div class="nav-button button open-real-account tol-trade-button" data-translate="trade"></div>
                                        <?php } ?>
                                    </li>
                                    <li>
                                        <div id="userbar-mobilemenu" type="button" class="nav-button button nav-user" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="userBarElem userName" data-widget="userBar"></span>
                                            <span class="dropdown-arrow"></span>
                                        </div>
                                    </li>
                                </ul>
                                <!-- END USER BAR- FOR LOGGED USER -->
                            </div>
                        </div>
                        <div id="userbarmenu" class="dropdown-menu">
                            <ul>
                                <li class="usermenu-icon">
                                    <div id="toggle-usermenu">X</div>
                                </li>
								<li>
                                    <a class="tol-userbar-my-portfolios" href="#">My Portfolios</a>
                                </li>
                                <li>
                                    <a class="tol-userbar-my-trades" href="#" data-toggle="modal" data-type="myTradesButton" data-target="#tol-trades-popup" data-translate="userDataMyTrades"></a>
                                </li>
                                <li>
                                    <a class="tol-userbar-my-profile" href="#" data-translate="Personal_Details"></a>
                                </li>
                                <li>
                                    <a class="tol-userbar-social-profile" href="#" data-translate="Social_Profile"></a>
                                </li>
                                <li>
                                    <a class="tol-userbar-logout-user" href="#" data-translate="logout"></a>
                                </li>
                            </ul>
                        </div>
                    </div>
            </header>
        </div>
