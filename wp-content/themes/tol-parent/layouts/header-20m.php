<!DOCTYPE html <?php language_attributes(); ?>>
<!--[if lt IE 7 ]>
<html class="ie ie6 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>
<html class="ie ie7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>
<html class="ie ie9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js lang-<?php echo ICL_LANGUAGE_CODE; ?>">
<!--<![endif]-->

    <head>
        <?php require_once 'header.php'; ?>
        <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
        <!-- Metas Page details-->
        <title><?php wp_title(' | ', true, 'right'); ?><?php bloginfo('name'); ?></title>
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!--google font style-->
        <link href='//fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>

        <!-- Favicons -->
        <link rel="shortcut icon" href="/wp-content/themes/tol-child/images/fav.ico">
        <link rel="apple-touch-icon" href="../images/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="../images/apple-touch-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="../images/apple-touch-114x114.png">
    </head>
    <body id="top" <?php body_class(); ?>>
        <!-- MOBILE MENU -->
        <div id="mobilemenu-shadow"></div>
        <div id="toggle-menu2" class="main-menu-icon">X</div>
        <div id="mobilemenu" class="tol-wp-navigation">
            <?php
            wp_nav_menu(
                array(
                    'menu' => 'Primary Menu',
                    'theme_location' => 'primary',
                    'container' => 'false',
                    'menu_class' => 'nav navbar-nav main-nav'
                )
            );
            ?>
            <div class="tol-language-switcher">
                <?php do_action('wpml_add_language_selector'); ?>
            </div>
        </div>
        <!-- END MOBILE MENU -->
        <div id="tol-header-layout-25">
            <header id="tol-header">
                <div class="navigation" id="navigation">
                    <div class="cf">
                        <div class="nav-button has-icon main-menu">
                            <div id="toggle-menu" class="main-menu-icon">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                        <div class="nav_logo">
                            <a class="navbar-brand" href="#"></a>
                        </div>
                        <div class="right-buttons">
                            <div class="tol-login-userbar-container">
                                <!-- SIGN IN NAVIGATION -->
                                <div id="tol-login-area" class="nav navbar-nav user-nav tolUserIsNotLogged" style="display:none;">
                                    <div class="nav-button button tol-login-account" data-toggle="modal" data-target="#tol-login-popup"><span class="icon icon-card"></span><span data-translate="UserData_userBlock_Login"></span></div>
                                    <div class="nav-button button open-real-account tol-create-account" data-translate="UserData_userBlock_CreateAccount"></div>
                                </div>
                                <!-- END SIGN IN NAVIGATION -->
                                <!-- USER BAR - FOR LOGGED USER -->
                                <ul id="tol-userbar" class="nav navbar-nav user-nav tolUserIsLogged" style="display:none">
                                    <li>
                                        <?php if (strpos($_SERVER['REQUEST_URI'],'cashier') == false) { ?>
                                            <div class="nav-button button open-real-account tol-quick-deposit" data-translate="depositBtn"></div>
                                        <?php } else { ?>
                                            <div class="nav-button button open-real-account tol-trade-button" data-translate="trade"></div>
                                        <?php } ?>
                                    </li>
                                    <li>
                                        <div id="userbar-mobilemenu" class="nav-button button nav-user">
                                            <span class="userBarElem userName"></span>
                                        </div>
                                    </li>
                                </ul>
                                <!-- END USER BAR- FOR LOGGED USER -->
                            </div>
                        </div>
                        <div id="userbarmenu" class="dropdown-menu">
                            <ul>
                                <li class="usermenu-icon">
                                    <div id="toggle-usermenu">X</div>
                                </li>
                                <li>
                                    <a class="tol-userbar-my-trades" href="#" data-toggle="modal" data-type="myTradesButton" data-target="#tol-trades-popup" data-translate="userDataMyTrades"></a>
                                </li>
                                <li>
                                    <a class="tol-userbar-my-profile" href="#" data-translate="Personal_Details"></a>
                                </li>
                                <li>
                                    <a class="tol-userbar-logout-user" href="#" data-translate="logout"></a>
                                </li>
                            </ul>
                        </div>
                    </div>
            </header>
        </div>
