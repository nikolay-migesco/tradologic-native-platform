<!DOCTYPE html <?php language_attributes(); ?>>
<!--[if lt IE 7 ]>
<html class="ie ie6 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>
<html class="ie ie7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>
<html class="ie ie9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js lang-<?php echo ICL_LANGUAGE_CODE; ?>">
<!--<![endif]-->
    <head>
        <?php require_once 'header.php'; ?>
        <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
        <!-- Metas Page details-->
        <title><?php wp_title(' | ', true, 'right'); ?><?php bloginfo('name'); ?></title>
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- Favicons -->
        <link rel="shortcut icon" href="/wp-content/themes/tol-child/images/fav.ico">
        <link rel="apple-touch-icon" href="../images/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="../images/apple-touch-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="../images/apple-touch-114x114.png">
    </head>
    <body id="top" <?php body_class(); ?>>
         <!-- START PostionID history popup -->
         <div id="tol-position-history" tabindex="-1" role="dialog" style="height: auto; width: 320px; display: none;" class="ui-dialog ui-corner-all ui-widget ui-widget-content ui-front ui-draggable forex-poshistory-dialog">
            <div class="ui-dialog-titlebar ui-corner-all ui-widget-header ui-helper-clearfix ui-draggable-handle">
                <span class="ui-dialog-title">Position History</span>
                <button type="button" class="ui-button ui-corner-all ui-widget ui-button-icon-only ui-dialog-titlebar-close" data-dismiss="modal" title="Close">
                    <span class="ui-button-icon ui-icon ui-icon-closethick"></span>
                    <span class="ui-button-icon-space"> </span>
                    Close
                </button>
            </div>
            <div class="asset-orderid">
                <div class="forex-position-id"></div>
                <div class="forex-posid-asset"></div>
                <div class="forex-profit-title two-lines">Profit</div>
                <div class="forex-posid-profit"></div>
            </div>
            <div class="position-history-content ui-dialog-content ui-widget-content">
                <div class="position-history-body"></div>
            </div>
        </div>
        <!-- END PostionID history popup -->
        <div id="tol-header-layout-25">
            <header id="tol-header">
                <div class="navigation" id="navigation">
                    <div class="cf">
                        <div class="nav-button has-icon main-menu">
                            <div id="toggle-menu" class="main-menu-icon">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                            <div id="mobilemenu" class="tol-wp-navigation">
                                <?php
                                wp_nav_menu(
                                    array(
                                        'menu' => 'Primary Menu',
                                        'theme_location' => 'primary',
                                        'container' => 'false',
                                        'menu_class' => 'nav navbar-nav main-nav'
                                    )
                                );
                                ?>
                            </div>
                        </div>
                        <div class="nav_logo">
                            <a class="navbar-brand" href="#"></a>
                        </div>
                        <div class="right-buttons">
                            <div class="tol-login-userbar-container">
                                <!-- SIGN IN NAVIGATION -->
                                <div id="tol-login-area" class="nav navbar-nav user-nav tolUserIsNotLogged" style="display:none;">
                                    <div class="nav-button button tol-login-account" data-toggle="modal" data-target="#tol-login-popup"><span class="icon icon-card"></span><span data-translate="UserData_userBlock_Login"></span></div>
                                    <div class="nav-button button open-real-account tol-create-account" data-translate="UserData_userBlock_CreateAccount"></div>
                                </div>
                                <!-- END SIGN IN NAVIGATION -->
                                <div class="tol-language-switcher">
                                    <?php do_action('wpml_add_language_selector'); ?>
                                </div>
                                <!-- USER BAR - FOR LOGGED USER -->
                                <ul id="tol-userbar" class="nav navbar-nav user-nav tolUserIsLogged" style="display:none">
                                    <li>
                                        <div type="button" class="nav-button button nav-game" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="userBarElem currgame">Forex</span>
                                            <span class="dropdown-arrow"></span>
                                        </div>
                                        <ul class="navgame dropdown-menu">
                                            <li>
                                                <a class="tol-exchange-game" href="<?php echo home_url('/'); ?>traderoom?config=exchange">Exchange</a>
                                            </li>
                                            <li>
                                                <a class="tol-forex-game" href="<?php echo home_url('/'); ?>traderoom?config=simplex">Simplex</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <div class="nav-button button open-real-account tol-quick-deposit" data-translate="DepositBtn"></div>
                                    </li>
                                    <li>
                                        <div type="button" class="nav-button button nav-user" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <?php echo do_shortcode('[userBarUserName]'); ?>
                                            <span class="dropdown-arrow"></span>
                                        </div>
                                        <ul class="dropdown-menu">
											<li>
                                                <a class="tol-userbar-my-portfolios" href="#">My Portfolios</a>
                                            </li>
                                            <li>
                                                <a class="tol-userbar-my-trades" href="#" data-toggle="modal" data-target="#tol-trades-popup" data-translate="userDataMyTrades"></a>
                                            </li>
                                            <li>
                                                <a class="tol-userbar-my-profile" href="#" data-translate="Personal_Details"></a>
                                            </li>
                                            <li>
                                                <a class="tol-userbar-logout-user" href="#" data-translate="logout"></a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                                <!-- END USER BAR- FOR LOGGED USER -->
                            </div>
                        </div>
                    </div>
            </header>
        </div>
