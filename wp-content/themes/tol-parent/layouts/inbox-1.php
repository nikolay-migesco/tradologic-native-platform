<div id="tol-inbox-layout-1">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="container">
            <div class="pageHeadTitle">
                <?php require_once(ABSPATH.'wp-content/themes/tol-parent/layouts/'.BREADCRUMB.'.php' ); ?>
            </div>
            <header class="entry-header">
                <h1 class="entry-title"><?php the_title(); ?></h1>
            </header>
            <div class="clearfix flat-top-borders">
                <?php echo do_shortcode('[messageInboxAllMessages]'); ?>               
                <?php echo do_shortcode('[messageInboxContent]'); ?>
                <?php echo do_shortcode('[messageInboxPaging]'); ?>
            </div>
        </div>

        <div class="entry-content">
            <?php echo $post->post_content; ?>
        </div>
    </article>
</div>