<?php
/**
 * Created by PhpStorm.
 * User: OBenov
 * Date: 24-Oct-17
 * Time: 10:26 AM
 */
?>
  <!-- // my footer // -->
<footer id="tol-footer">
    <div class="tol-footer-layout-99 mb-5">
        <section class="footer-section">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <h6 data-translate="products"></h6>
                        <ul>
                            <li><a href="#" data-translate="binary-options"></a></li>
                            <li><a href="#" data-translate="simplex"></a></li>
                            <li><a href="#" data-translate="forex"></a></li>
                            <li><a href="#" data-translate="meta-trader-5"></a></li>
                            <li><a href="#" data-translate="portfolio-management"></a></li>
                            <li><a href="#" data-translate="crypto-invest"></a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <h6  data-translate="legal"></h6>
                        <ul>
                            <li><a href="#" class="legal-link" data-target="legal-docs" data-translate="legal-documents"></a></li>
                            <li><a href="#" class="legal-link" data-target="terms-and-conditions" data-translate="termsConditionsLong"></a></li>
                            <li><a href="#" class="legal-link" data-target="privacy-policy" data-translate="ShortPolicy"></a></li>
                            <li><a href="#"  class="legal-link" data-target="risk-disclosure" data-translate="risk_disclosure"></a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-12 ">
                        <h6  data-translate="quick_links"></h6>
                        <ul class="dashed">
                            <li><a href="../registration"  data-translate="open_account"></a></li>
                            <li><a href="../login"  data-translate="Login_LogIn"></a></li>
                            <li><a href="../education"  data-translate="education"></a></li>
                            <li><a href="../faq"  data-translate="faq"></a></li>
                            <li><a href="../contact-us" data-translate="contact-us"></a></li>
                            <li><a href="../about-us" data-translate="homeAboutUs"></a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <h6 class="join" data-translate="join-our-community"></h6>
                        <div class="row social-icons">
                            <div class="ico-container"><a href="#" class="fb-ico"></a> </div>
                            <div class="ico-container"><a href="#" class="google-ico"></a> </div>
                            <div class="ico-container"><a href="#" class="linkedin-ico"></a> </div>
                            <div class="ico-container"><a href="#" class="twitter-ico"></a> </div>
                        </div>
                    </div>  
                </div>
            </div>
        </section>
        <section class="diclaimer-section">
            <div class="container-fluid">
                <div class="row">
                    <?php dynamic_sidebar('tol-disclosure'); ?>
                </div>
                <div class="row justify-content-center">
                    <p class="text-center">Copyright &copy; 2017 TRADOLOGIC</p>
                </div>
            </div>
        </section>

    </div>
</footer>