<div id="tol-withdraw" class="well well-sm">
    <fieldset class="tol-withdraw row" style="display: none;">
    
        <div class="col-xs-12 col-sm-12"><?php echo do_shortcode('[cashierWithdrawValidationText]'); ?></div>

        <div class="col-xs-12 col-sm-6">
            <div class="form-group col-xs-12 col-sm-12 choose-amount">
                <label data-widget="cashierWithdraw" class="col-xs-12 col-sm-12 col-md-4 control-label text-left" data-translate="cashierLabelChooseAmount"></label>
                <div class="col-sm-12 col-md-8">
                    <?php echo do_shortcode('[cashierWithdrawAmount]'); ?>
                </div>
            </div>
            <div class="form-group col-xs-12 col-sm-12 transfer-from-field" style="display: none;">
                <label data-widget="cashierWithdraw" class="col-xs-12 col-sm-12 col-md-4 control-label text-left" data-translate="cashierLabelTransferFrom"></label>
                <div class="col-sm-12 col-md-8">
                    <?php echo do_shortcode('[cashierTransferFrom]'); ?>
                </div>
            </div>
            <div class="form-group col-xs-12 col-sm-12 cashierWithdrawCannotBeMoreThan text-left">
                <?php echo do_shortcode('[cashierWithdrawCannotBeMoreThan]'); ?>
            </div>
            <div class="form-group col-xs-12 col-sm-12">
                <label data-widget="cashierWithdraw" data-type="transferToField" class="col-xs-12 col-sm-12 col-md-4 control-label text-left" data-translate="cashierLabelPreferredPaymentMethod"></label>
                <div class="col-sm-12 col-md-8">
                    <?php echo do_shortcode('[cashierWithdrawMethod]'); ?>
                </div>
            </div>
            
            <div class="form-group col-xs-12 col-sm-12">
                <label data-widget="cashierWithdraw" class="col-xs-12 col-sm-12 col-md-4 control-label text-left" data-translate="cashierLabelAccount"></label>
                <div class="col-sm-12 col-md-8">
                    <?php echo do_shortcode('[cashierWithdrawAccount]'); ?>
                </div>
            </div>
            
            <div class="form-group col-xs-12 col-sm-12">
                <label data-widget="cashierWithdraw" class="col-xs-12 col-sm-12 col-md-4 control-label text-left" data-translate="cashierLabelComment"></label>
                <div class="col-sm-12 col-md-8">
                    <?php echo do_shortcode('[cashierWithdrawComment]'); ?>
                </div>
            </div>
            
            <div class="form-group col-xs-12 col-sm-12">
                <label data-widget="cashierWithdraw" class="col-xs-12 col-sm-12 col-md-4 control-label text-left" data-translate="cashierLabelCustomerName"></label>
                <div class="col-sm-12 col-md-8">
                    <?php echo do_shortcode('[cashierWithdrawCustomerName]'); ?>
                </div>
            </div>
            
            <div class="form-group col-xs-12 col-sm-12">
                <label data-widget="cashierWithdraw"  class="col-xs-12 col-sm-12 col-md-4 control-label text-left" data-translate="cashierLabelBankName"></label>
                <div class="col-sm-12 col-md-8">
                    <?php echo do_shortcode('[cashierWithdrawBankName]'); ?>
                </div>
            </div>
            
            <div class="form-group col-xs-12 col-sm-12">
                <label data-widget="cashierWithdraw"  class="col-xs-12 col-sm-12 col-md-4 control-label text-left" data-translate="cashierLabelBankCode" data-translate="cashierLabelBankCode"></label>
                <div class="col-sm-12 col-md-8">
                    <?php echo do_shortcode('[cashierWithdrawBankCode]'); ?>
                </div>
            </div> 
        </div>
        
        <div class="col-xs-12 col-sm-6">
            
            <div class="form-group col-xs-12 col-sm-12">
                <label data-widget="cashierWithdraw" class="col-xs-12 col-sm-12 col-md-4 control-label text-left" data-translate="cashierLabelBranchAddress"></label>
                <div class="col-sm-12 col-md-8">
                    <?php echo do_shortcode('[cashierWithdrawBranchAddress]'); ?>
                </div>
            </div>
            
            <div class="form-group col-xs-12 col-sm-12">
                <label data-widget="cashierWithdraw" class="col-xs-12 col-sm-12 col-md-4 control-label text-left" data-translate="cashierLabelBranchCode"></label>
                <div class="col-sm-12 col-md-8">
                    <?php echo do_shortcode('[ashierWithdrawBranchCode]'); ?>
                </div>
            </div>
            
            <div class="form-group col-xs-12 col-sm-12">
                <label data-widget="cashierWithdraw" class="col-xs-12 col-sm-12 col-md-4 control-label text-left" data-translate="cashierLabelAccountNumber"></label>
                <div class="col-sm-12 col-md-8">
                    <?php echo do_shortcode('[cashierWithdrawAccountNumber]'); ?>
                </div>
            </div>
            
            <div class="form-group col-xs-12 col-sm-12">
                <label data-widget="cashierWithdraw" class="col-xs-12 col-sm-12 col-md-4 control-label text-left" data-translate="cashierLabelIBAN"></label>
                <div class="col-sm-12 col-md-8">
                    <?php echo do_shortcode('[cashierWithdrawIban]'); ?>
                </div>
            </div>
            
            <div class="form-group col-xs-12 col-sm-12">
                <label data-widget="cashierWithdraw" class="col-xs-12 col-sm-12 col-md-4 control-label text-left" data-translate="cashierLabelSwift"></label>
                <div class="col-sm-12 col-md-8">
                    <?php echo do_shortcode('[cashierWithdrawSwift]'); ?>
                </div>
            </div>
            
            <div class="form-group col-xs-12 col-sm-12">
                <label data-widget="cashierWithdraw" class="col-xs-12 col-sm-12 col-md-4 control-label text-left" data-translate="cashierLabelCurrency"></label>
                <div class="col-sm-12 col-md-8">
                    <?php echo do_shortcode('[cashierWithdrawCurrency]'); ?>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 tol-withdraw-messages">
            <?php echo do_shortcode('[cashierWithdrawInfo]'); ?>
            <?php echo do_shortcode('[cashierWithdrawOK]'); ?>  
            <?php echo do_shortcode('[cashierWithdrawErrorCC]'); ?>
        </div> 
        <div class="col-xs-12 col-sm-12 text-right">
            <?php echo do_shortcode('[cashierWithdrawCancel]'); ?>  
            <?php echo do_shortcode('[cashierWithdrawSubmit]'); ?>                     
        </div>  
    </fieldset>
</div>