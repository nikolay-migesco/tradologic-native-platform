<?php
     wp_enqueue_style('tournaments', get_stylesheet_directory_uri() . '/styles/layouts/tournaments-1.css', array(), NULL, false);
?>

<script type="text/javascript">
    $(window).on('loadingIsCompleted', function () {
    $(window).trigger('clickTournamentDetails', getParameterByName("tournamentID"));
        $('.tournamentInfo, .introAdditionalText, .tournamentNameSpan').show();
    });
</script>
<div class="container">
	<div class="row">
<div class="tournamentsHeader">
    <span class="yellow">FREE TOURNAMENT!</span>
    <span class="yellow">WIN $1,000</span>
    <span class="white">THE BINARY OPTIONS COMPETITION</span>
</div>
<div class="tournamentsWrapper">
    <a href="./tournaments-board">
        <span class="backButton"></span>
    </a>
    <span class="cup"></span>
    <div data-widget="tournamentLeaderboard" data-type="finished"></div>
    <span class="tournamentNameSpan" style="display: none;">Become the leader of the <div data-widget="tournamentLeaderboard" data-type="name" class="tournamentName"></div></span>
    <span class="introAdditionalText" style="display: none;">Win one of <font color="yellow">5 CASH</font> prizes in the world's biggest trading competition</span>
    <div class="tournamentInfo" style="display: none;">
        <div class="col-sm-6">
            <div data-widget="tournamentLeaderboard" data-type="duration" class="tournamentDuration"></div>
            <div data-widget="tournamentLeaderboard" data-type="type" class="tournamentTypeWrapper"></div>
        </div>
        <div class="col-sm-6">
            <div data-widget="tournamentLeaderboard" data-type="games" class="tournamentGames"></div>
            <div data-widget="tournamentLeaderboard" data-type="amount" class="tournamentAmount"></div>
        </div>
    </div>
    <div data-widget="tournamentLeaderboard" data-type="conditions" class="tournamentTimeToStart"></div>
    <div data-widget="tournamentLeaderboard" data-type="timer" class="tournamentTimer"></div>
    <div class="tournamentRegistration-widget-wrapper">
        <div data-widget="tournamentRegistration" data-translate="tournamentRegistration">
        <input type="button" value="Join" data-widget="tournamentRegistration" data-type="register" class="widgetPlaceholder input" data-translate="Join" style="display: none;" />
        <div data-widget="tournamentRegistration" data-type="error" style="display: none" class="widgetPlaceholder error-message"></div>
         </div>
    </div>
    <div data-widget="tournamentLeaderboard" data-type="userRank"></div>

    <div data-widget="tournamentLeaderboard" data-type="table" data-columns="rank,name,country,results,prize"></div>
    <div data-widget="tournamentLeaderboard"  data-type="paging" data-rowsperpage="10"  data-prevnextpage="true"></div>
</div></div></div>