<?php
setcookie('oneClickTrade', 'on', time()+3600*24 );
?>
<script type="text/javascript">
    $(document).ready(function () {
        $.cookie("game", 'forex', {path: "/"});
        migesco.setProfexPhone();

        $('.footer__container').empty();

        $('#changeCtrls').on('show.bs.collapse','.collapse', function() {
            $('#changeCtrls').find('.collapse.in').collapse('hide');
        });

        $('.row-settings .one-click-trading-title').click(function(e){
            e.stopImmediatePropagation();
            return false;
        });

        $('.tol-wp-navigation .menu-item').removeClass('current-menu-item');
        $('.mobile-forex').addClass('current-menu-item');

        $('#mobilemenu').css('height', $(document).innerHeight());
        $('.panel.chartpanel').css('height', $('#content').height() - 143);

        if (screen.width == 768) {
            $('#mobilemenu').css('height', $(window).height() - 48);
            $('#userbarmenu').css('height', $(window).height());
            $('.scrollable-social').css('height', $(window).height() - 276);
            $('.sidepanel-tabs > .tab-content').css('height', $(window).height() - 155);
            $('.asset-table-left').css('height', $(window).height() - 196);
            $('.orders-trades-table-wrapper').css('height', $(window).height() - 228);
            $('.panel.chartpanel').css('height', $(window).height() - 236);
            $('.footer-icon-positions a').text('Open Positions');
            $('.footer-icon-orders a').text('Pending Orders');
        };

        if ($(document).innerHeight() < 492) {
            $('#changeCtrls').height($(document).innerHeight() - 75);
        }

        $(window).on('loadingIsCompleted', function(event){
            setTimeout(function () {
                $('.asset-table-left').css('height', $(window).height() - 169);
            }, 500);
        });
    });
</script>
<div class="layout" id="layout">
    <div class="traderoom-loader" style="display: none;">
        <div class="loader">
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
        </div>
    </div>
    <div class="tab-content">
        <div id="home" class="tab-pane fade in active">
            <div data-widget="forex-forex" data-type="toolsmenu" class="tradepanel" id="tradePanel">
                <div class="tradepanel-control-bar">
                    <div data-widget="forex-forex" data-link="boxes" data-isactive="false" class="assets-boxes" data-translate="box-view"></div>
                    <div data-widget="forex-forex" data-link="list" data-isactive="true" class="assets-list active" data-translate="list-view"></div>
                </div>
                <div class="tradepanel-control-bar assetslist-tools" >
                    <div data-widget="forex-forex" data-type="assetstype" class="tradepanel-title assetstype" data-translate="All">
                        <span>All</span>
                    </div>
                    <div data-type="searchcontainer" class="tradepanel-control-bar asset-search-bar">
                        <input data-widget="forex-forex" data-type="searchinput" type="text" class="input-assetssearch" placeholder="" data-translate="search"/>
                        <span class="list-icon has-tooltip svg-icon-holder" no-blur-id="instruments-list" data-tooltip-message="label.add-instrument" data-tooltip-orientation="rightCenter"></span>
                    </div>
                    <span class="asset-table-settings-icon column-settings" data-widget="forex-forex" data-type="widgetsettings">
                        <div class="settings">
                            <div class="asset-table-settings-outer">
                                <div class="asset-table-settings-content">
                                    <div class="asset-table-settings-title" data-translate="asset-settings"></div>
                                    <div class="row-settings row-settings-oneclick">
                                        <div class="one-click-trading-title">
                                            <span data-translate="asset-settings-one"></span>
                                            <span class="info-icon" data-toggle="tooltip" data-placement="bottom" title="One-Click Trading allows you to create orders with the click of a single button in Box View mode, without the need of additional confirmation."></span>
                                        </div>
                                        <button class="close-button-tick"></button>
                                        <div class="onoffswitch">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" data-widget="forex-forex" data-type="oneclicktrade" id="myonoffswitch">
                                            <label class="onoffswitch-label" for="myonoffswitch">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row-settings">
                                        <div class="one-click-trading-title">
                                            <span data-translate="asset-settings-two"></span>
                                            <span class="info-icon" data-toggle="tooltip" data-placement="bottom" title="Traders Trend represents the Sell and Buy sentiment among the investors."></span>
                                        </div>
                                        <button class="close-button-tick"></button>
                                        <div class="onoffswitch">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" data-widget="forex-forex" data-type="traderstrend" id="traderstrend">
                                            <label class="onoffswitch-label" for="traderstrend">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row-settings">
                                        <div class="one-click-trading-title">
                                            <span data-translate="asset-settings-three"></span>
                                            <span class="info-icon" data-toggle="tooltip" data-placement="bottom" title="Shows the current spread(the difference between Ask and Bid price) of the instrument. When disabled(off), shows the daily change of the instrument."></span>
                                        </div>
                                        <button class="close-button-tick"></button>
                                        <div class="onoffswitch">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" data-widget="forex-forex" data-type="dailypercent" id="dailypercent">
                                            <label class="onoffswitch-label" for="dailypercent">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </span>
                </div>
                <div data-type="optionscontainer" class="tradepanel-control-bar"  style="display: none">
                    <div data-type="searchoptions" class="col-xs-12 col-sm-12 assetssearch-options-container" data-translate="All"></div>
                </div>
                <div class="row assetslist-optionscontainer" data-type="optionscontainer" style="display: none"></div>
                <div class="tradepanel-header-bar" >
                    <span class="tradepanel-instrument-label" data-translate="instrument"></span>
                    <span class="tradepanel-sell-label" data-translate="sell"></span>
                    <span class="tradepanel-buy-label" data-translate="buy"></span>
                </div>
                <div class="scrollable-area scrollable-area-at-top scrollable-area-active asset-table-left">
                    <div class="scrollable-area-body">
                        <div class="scrollable-area-content">
                            <div  data-widget="forex-forex" data-type="assetsBoxes" class="assetsview-boxes" style="display: none;"></div>
                            <div data-widget="forex-forex" data-type="assetsList" class="assetsview-list"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="menu1" class="tab-pane fade">
            <div class="panel chartpanel disable_remove tabs-mode">
                <div id="tv_chart_container"></div>
            </div>
            <div class="asset-box">
                <span class="asset-table-settings-icon column-settings" data-widget="forex-forex" data-type="widgetsettings">
                    <div class="settings">
                        <div class="asset-table-settings-outer">
                            <div class="asset-table-settings-content">
                                <div class="asset-table-settings-title" data-translate="asset-settings"></div>
                                <div class="row-settings">
                                    <div class="one-click-trading-title">
                                        <span data-translate="asset-settings-one"></span>
                                        <span class="info-icon" data-toggle="tooltip" data-placement="bottom" title="One-Click Trading allows you to create orders with the click of a single button in Box View mode, without the need of additional confirmation."></span>
                                    </div>
                                    <button class="close-button-tick"></button>
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" data-widget="forex-forex" data-type="oneclicktrade" id="myonoffswitch">
                                        <label class="onoffswitch-label" for="myonoffswitch">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="row-settings">
                                    <div class="one-click-trading-title">
                                        <span data-translate="asset-settings-two"></span>
                                        <span class="info-icon" data-toggle="tooltip" data-placement="bottom" title="Traders Trend represents the Sell and Buy sentiment among the investors."></span>
                                    </div>
                                    <button class="close-button-tick"></button>
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" data-widget="forex-forex" data-type="traderstrend" id="traderstrend">
                                        <label class="onoffswitch-label" for="traderstrend">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="row-settings">
                                    <div class="one-click-trading-title">
                                        <span data-translate="asset-settings-three"></span>
                                        <span class="info-icon" data-toggle="tooltip" data-placement="top" title="Shows the daily change of the instrument."></span>
                                    </div>
                                    <button class="close-button-tick"></button>
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" data-widget="forex-forex" data-type="dailypercent" id="dailypercent">
                                        <label class="onoffswitch-label" for="dailypercent">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </span>
                <div data-widget="forex-forex" data-type="mobile-single-box"></div>
            </div>
        </div>
        <div id="menu2" class="tab-pane fade">
            <button type="button" class="balance-toggle" data-toggle="collapse" data-target="#balance-positions" aria-expanded="true" data-translate="userDataBalance"></button>
            <div id="balance-positions" class="collapse in" aria-expanded="true">
                <div class="equity-container">
                    <div class="equity-content">
                        <div class="equity-item balance-item" id="equity-ppl">
                            <span class="label" data-translate="userDataBalance"></span>
                            <span data-widget="balancev2" data-type="balance" class="value blue"></span>
                        </div>
                        <div class="equity-item profit-item" id="equity-ppl">
                            <span class="label" data-translate="profit"></span>
                            <span data-widget="balancev2" data-type="profit" class="value"></span>
                        </div>
                        <div class="equity-item equity-item" id="equity-total">
                            <span class="label" data-translate="equity"></span>
                            <span data-widget="balancev2" data-type="equity" class="value"></span>
                        </div>
                        <div class="equity-item margin-item" id="equity-indicator">
                            <span class="label" data-translate="footer_margin"></span>
                            <span data-widget="balancev2" data-type="forexMargin" class="value"></span>
                            <div class="margin-indicator" data-widget="balancev2" data-type="marginPercentBar"></div>
                            <span data-widget="balancev2" data-type="marginPercentValue" class="value margin-percent-value blue"></span>
                        </div>
                        <div class="equity-item avbalance-item" id="equity-total">
                            <span class="label" data-translate="availableBalance"></span>
                            <span data-widget="balancev2" data-type="availableBalance" class="value"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pending-orders-title"><span data-translate="openPositions"></span></div>
            <div class="account panel orders-trades-table-wrapper positions-active" id="accountPanel">
                <div class="tabHolder">
                    <span class="column-settings tabpositions">
                        <div class="settings"></div>
                        <div class="contextmenu multiple-selection datatable-contextmenu _focusable leftCenter" tabindex="-1">
                            <div class="multiple-selection-content">
                                <div class="item item-datatable-contextmenu-name selected" data-column="1">
                                    <span data-translate="positionId">Position ID</span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="item item-datatable-contextmenu-quantity selected" data-column="3">
                                    <span data-translate="quantity">Quantity</span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="item item-datatable-contextmenu-averagePrice selected" data-column="5">
                                    <span data-translate="forexTable_rate">Average Rate</span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="item item-datatable-contextmenu-currentPrice selected" data-column="6">
                                    <span data-translate="marketRate">Market Rate</span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="item item-datatable-contextmenu-limitPrice selected" data-column="7">
                                    <span data-translate="takeProfit-short">TP</span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="item item-datatable-contextmenu-stopPrice selected" data-column="8">
                                    <span data-translate="stopLoss-short">SL</span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="item item-datatable-contextmenu-margin selected" data-column="9">
                                    <span data-translate="margin">Margin</span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="item item-datatable-contextmenu-swap" data-column="10">
                                    <span data-translate="swap">Swap</span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="item item-datatable-contextmenu-ppl selected" data-column="11">
                                    <span data-translate="commission">Commission</span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="item item-datatable-contextmenu-created" data-column="12">
                                    <span data-translate="forexTable_created">Date Created</span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="item item-datatable-contextmenu-ppl selected" data-column="13">
                                    <span data-translate="copiedFrom">Copied From</span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="item item-datatable-contextmenu-ppl selected" data-column="14">
                                    <span data-translate="result">Result</span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="btn-wrapper navigation">
                                    <div class="nav-button defaultSettings" data-translate="defaultSettings"></div>
                                </div>
                            </div>
                        </div>
                    </span>
                </div>
                <div class="positions-scrollable-header" id="trades">
                    <div class="dataTable">
                        <table class="fakeTableHead">
                            <thead>
                            <tr>
                                <th data-column="positionId" data-sort="int" class="name canSort canResize canDrag">
                                    <div class="cellWrapper">
                                        <span class="content" data-translate="positionid">Position ID</span>
                                    </div>
                                </th>
                                <th data-column="asset" data-sort="string" class="name canSort canResize canDrag">
                                    <div class="cellWrapper">
                                        <span class="content" data-translate="asset">Asset</span>
                                    </div>
                                </th>
                                <th data-column="quantity" data-sort="int" class="name canSort canResize canDrag">
                                    <div class="cellWrapper">
                                        <span class="content" data-translate="quantity">Quantity</span>
                                    </div>
                                </th>
                                <th data-column="direction" data-sort="string" class="name canSort canResize canDrag">
                                    <div class="cellWrapper">
                                        <span class="content" data-translate="direction">Direction</span>
                                    </div>
                                </th>
                                <th data-column="rate" data-sort="float" class="name canSort canResize canDrag">
                                    <div class="cellWrapper">
                                        <span class="content" data-translate="forexTable_rate">Rate</span>
                                    </div>
                                </th>
                                <th data-column="marketRate" data-sort="float" class="name canSort canResize canDrag">
                                    <div class="cellWrapper">
                                        <span class="content" data-translate="marketRate">Market Rate</span>
                                    </div>
                                </th>
                                <th data-column="takeProfit" data-sort="float" class="name canSort canResize canDrag">
                                    <div class="cellWrapper">
                                        <span class="content" data-translate="takeProfit-short">TP</span>
                                    </div>
                                </th>
                                <th data-column="stopLoss" data-sort="float" class="name canSort canResize canDrag">
                                    <div class="cellWrapper">
                                        <span class="content" data-translate="stopLoss-short">SL</span>
                                    </div>
                                </th>
                                <th data-column="margin" data-sort="float" class="name canSort canResize canDrag">
                                    <div class="cellWrapper">
                                        <span class="content" data-translate="margin">Margin</span>
                                    </div>
                                </th>
                                <th data-column="swap" data-sort="float" class="name canSort canResize canDrag">
                                    <div class="cellWrapper">
                                        <span class="content" data-translate="swap">Swap</span>
                                    </div>
                                </th>
                                <th data-column="commission" data-sort="float" class="name canSort canResize canDrag">
                                    <div class="cellWrapper">
                                        <span class="content" data-translate="commission">Commission</span>
                                    </div>
                                </th>
                                <th data-column="orderDate" data-sort="float" class="name canSort canResize canDrag">
                                    <div class="cellWrapper">
                                        <span class="content" data-translate="forexTable_created">Date</span>
                                    </div>
                                </th>
                                <th data-column="copied-from" data-sort="float" class="name canSort canResize canDrag">
                                    <div class="cellWrapper">
                                        <span class="content" data-translate="copiedFrom">Copied From</span>
                                    </div>
                                </th>
                                <th data-column="result" data-sort="float" class="name canSort canResize canDrag">
                                    <div class="cellWrapper">
                                        <span class="content" data-translate="result">Result</span>
                                    </div>
                                </th>
                                <th data-column="actions" data-sort="string" class="name canSort canResize canDrag">
                                    <div class="cellWrapper">
                                        <span class="content"></span>
                                    </div>
                                </th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="scrollable-area scrollable-area-at-top">
                    <div class="scrollable-area-body">
                        <div class="scrollable-area-content">
                            <div id="trades">
                                <div class="dataTable">
                                    <table id="forexProOpenTradesTable" class="hasArrows" data-widget="forex-openPositions">
                                        <thead data-widget="forex-openPositions" data-type="header" class="hide-thead">
                                        <tr>
                                            <th data-column="positionId" data-sort="int" class="name canSort canResize canDrag">
                                                <div class="cellWrapper">
                                                    <span class="content" data-translate="positionId">Position ID</span>
                                                </div>
                                            </th>
                                            <th data-column="asset" data-sort="string" class="name canSort canResize canDrag">
                                                <div class="cellWrapper">
                                                    <span class="content" data-translate="instrument">Asset</span>
                                                </div>
                                            </th>
                                            <th data-column="quantity" data-sort="int" class="name canSort canResize canDrag">
                                                <div class="cellWrapper">
                                                    <span class="content" data-translate="quantity">Quantity</span>
                                                </div>
                                            </th>
                                            <th data-column="direction" data-sort="string" class="name canSort canResize canDrag">
                                                <div class="cellWrapper">
                                                    <span class="content" data-translate="direction">Direction</span>
                                                </div>
                                            </th>
                                            <th data-column="rate" data-sort="float" class="name canSort canResize canDrag">
                                                <div class="cellWrapper">
                                                    <span class="content" data-translate="rate">Rate</span>
                                                </div>
                                            </th>
                                            <th data-column="marketRate" data-sort="float" class="name canSort canResize canDrag">
                                                <div class="cellWrapper">
                                                    <span class="content" data-translate="marketRate">Market Rate</span>
                                                </div>
                                            </th>
                                            <th data-column="takeProfit" data-sort="float" class="name canSort canResize canDrag">
                                                <div class="cellWrapper">
                                                    <span class="content" data-translate="takeProfit-short">TP</span>
                                                </div>
                                            </th>
                                            <th data-column="stopLoss" data-sort="float" class="name canSort canResize canDrag">
                                                <div class="cellWrapper">
                                                    <span class="content" data-translate="stopLoss-short">SL</span>
                                                </div>
                                            </th>
                                            <th data-column="margin" data-sort="float" class="name canSort canResize canDrag">
                                                <div class="cellWrapper">
                                                    <span class="content" data-translate="margin">Margin</span>
                                                </div>
                                            </th>
                                            <th data-column="swap" data-sort="float" class="name canSort canResize canDrag">
                                                <div class="cellWrapper">
                                                    <span class="content" data-translate="swap">Swap</span>
                                                </div>
                                            </th>
                                            <th data-column="commission" data-sort="float" class="name canSort canResize canDrag">
                                                <div class="cellWrapper">
                                                    <span class="content" data-translate="commission">Commission</span>
                                                </div>
                                            </th>
                                            <th data-column="orderDate" data-sort="float" class="name canSort canResize canDrag">
                                                <div class="cellWrapper">
                                                    <span class="content" data-translate="orderDate">Date</span>
                                                </div>
                                            </th>
                                            <th data-column="copied-from" data-sort="float" class="name canSort canResize canDrag">
                                                <div class="cellWrapper">
                                                    <span class="content" data-translate="copiedFrom">Copied From</span>
                                                </div>
                                            </th>
                                            <th data-column="result" data-sort="float" class="name canSort canResize canDrag">
                                                <div class="cellWrapper">
                                                    <span class="content" data-translate="result">Result</span>
                                                </div>
                                            </th>
                                            <th data-column="actions" data-sort="string" class="name canSort canResize canDrag">
                                                <div class="cellWrapper">
                                                    <span class="content" data-translate="actions"></span>
                                                </div>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody data-widget="forex-openPositions" data-type="table"></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="menu3" class="tab-pane fade">
            <button type="button" class="balance-toggle" data-toggle="collapse" data-target="#balance-orders" aria-expanded="true" data-translate="userDataBalance"></button>
            <div id="balance-orders" class="collapse in" aria-expanded="true"></div>
            <div class="pending-orders-title"><span data-translate="pendingOrders"></span></div>
            <div class="account panel orders-trades-table-wrapper orders-active" id="accountPanel">
                <div class="tabHolder">
                    <span class="column-settings taborders">
                        <div class="settings"></div>
                        <div class="contextmenu multiple-selection datatable-contextmenu _focusable leftCenter" tabindex="-1">
                            <div class="multiple-selection-content">
                                <div class="item item-datatable-contextmenu-name" data-column="1">
                                    <span data-translate="orderId">Order ID</span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="item item-datatable-contextmenu-direction selected" data-column="3">
                                    <span data-translate="direction"></span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="item item-datatable-contextmenu-quantity selected" data-column="5">
                                    <span data-translate="quantity">Quantity</span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="item item-datatable-contextmenu-averagePrice selected" data-column="6">
                                    <span data-translate="rate"></span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="item item-datatable-contextmenu-marketrate selected" data-column="7">
                                    <span data-translate="marketrate"></span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="item item-datatable-contextmenu-orderDate selected" data-column="8">
                                    <span data-translate="forexTable_created">Date Created</span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="btn-wrapper navigation">
                                    <div class="nav-button defaultSettings" data-translate="defaultSettings"></div>
                                </div>
                            </div>
                        </div>
                    </span>
                </div>
                <div class="positions-scrollable-header" id="orders">
                    <div class="dataTable">
                        <div class="forexPendingOrders">
                            <table class="fakeTableHead">
                                <thead>
                                <tr data-type="header" >
                                    <th data-column="orderId" data-sort="int" class="name canSort canResize canDrag">
                                        <div class="cellWrapper">
                                            <span class="content" data-translate="orderId"></span>
                                        </div>
                                    </th>
                                    <th data-column="asset" data-sort="string" class="name canSort canResize canDrag">
                                        <div class="cellWrapper">
                                            <span class="content" data-translate="asset"></span>
                                        </div>
                                    </th>
                                    <th data-column="direction" data-sort="string" class="name canSort canResize canDrag">
                                        <div class="cellWrapper">
                                            <span class="content" data-translate="direction"></span>
                                        </div>
                                    </th>
                                    <th data-column="type" data-sort="float" class="name canSort canResize canDrag">
                                        <div class="cellWrapper">
                                            <span class="content" data-translate="type"></span>
                                        </div>
                                    </th>
                                    <th data-column="quantity" data-sort="float" class="name canSort canResize canDrag">
                                        <div class="cellWrapper">
                                            <span class="content" data-translate="quantity"></span>
                                        </div>
                                    </th>
                                    <th data-column="rate" data-sort="float" class="name canSort canResize canDrag">
                                        <div class="cellWrapper">
                                            <span class="content" data-translate="rate"></span>
                                        </div>
                                    </th>
                                    <th data-column="marketrate" data-sort="float" class="name canSort canResize canDrag">
                                        <div class="cellWrapper">
                                            <span class="content" data-translate="marketrate"></span>
                                        </div>
                                    </th>
                                    <th data-column="orderDate" data-sort="float" class="name canSort canResize canDrag">
                                        <div class="cellWrapper">
                                            <span class="content" data-translate="forexTable_created"></span>
                                        </div>
                                    </th>
                                    <th data-column="actions" data-sort="string" class="name canSort canResize canDrag">
                                        <div class="cellWrapper">
                                            <span class="content"></span>
                                        </div>
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="scrollable-area scrollable-area-at-top">
                    <div class="scrollable-area-body">
                        <div class="scrollable-area-content">
                            <div class="tab-content" id="real-forex-tab-content">
                                <div id="orders">
                                    <div class="dataTable">
                                        <div class="forexPendingOrders">
                                            <table id="real-forex-orders-table" class="hasArrows" data-widget="forex-pendingOrders">
                                                <thead class="hide-thead">
                                                <tr data-type="header" >
                                                    <th data-column="orderId" data-sort="int" class="name canSort canResize canDrag">
                                                        <div class="cellWrapper">
                                                            <span class="content" data-translate="orderId"></span>
                                                        </div>
                                                    </th>
                                                    <th data-column="asset" data-sort="string" class="name canSort canResize canDrag">
                                                        <div class="cellWrapper">
                                                            <span class="content" data-translate="asset"></span>
                                                        </div>
                                                    </th>
                                                    <th data-column="direction" data-sort="string" class="name canSort canResize canDrag">
                                                        <div class="cellWrapper">
                                                            <span class="content" data-translate="direction"></span>
                                                        </div>
                                                    </th>
                                                    <th data-column="type" data-sort="float" class="name canSort canResize canDrag">
                                                        <div class="cellWrapper">
                                                            <span class="content" data-translate="type"></span>
                                                        </div>
                                                    </th>
                                                    <th data-column="quantity" data-sort="float" class="name canSort canResize canDrag">
                                                        <div class="cellWrapper">
                                                            <span class="content" data-translate="quantity"></span>
                                                        </div>
                                                    </th>
                                                    <th data-column="rate" data-sort="float" class="name canSort canResize canDrag">
                                                        <div class="cellWrapper">
                                                            <span class="content" data-translate="rate"></span>
                                                        </div>
                                                    </th>
                                                    <th data-column="marketrate" data-sort="float" class="name canSort canResize canDrag">
                                                        <div class="cellWrapper">
                                                            <span class="content" data-translate="marketrate"></span>
                                                        </div>
                                                    </th>
                                                    <th data-column="orderDate" data-sort="float" class="name canSort canResize canDrag">
                                                        <div class="cellWrapper">
                                                            <span class="content" data-translate="forexTable_created"></span>
                                                        </div>
                                                    </th>
                                                    <th data-column="actions" data-sort="string" class="name canSort canResize canDrag">
                                                        <div class="cellWrapper">
                                                            <span class="content"></span>
                                                        </div>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody data-widget="forex-pendingOrders" data-type="table"></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="menu4" class="tab-pane fade">
            <div class="sidepanel-tabs focusable" id="sidePanel" tabindex="-1">
                <ul class="nav right-sidebar-nav">
                    <?php if (ENABLE_FOREX_SOCIAL) { ?>
                        <li class="social-tab active">
                            <a href="#tab1" data-toggle="tab" class="active">
                                <span data-translate="userDataSocial"></span>
                            </a>
                        </li>
                    <?php } ?>

                    <li class="messages-tab <?php echo(ENABLE_FOREX_SOCIAL ? '' : 'active'); ?>">
                        <a href="#tab3" data-toggle="tab" class=" <?php echo(ENABLE_FOREX_SOCIAL ? '' : 'active'); ?>">
                            <span data-translate="userDataMessages"></span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <?php if (ENABLE_FOREX_SOCIAL) { ?>
                        <div class="tab-pane active in" id="tab1">
                            <div class="tol-social-sidebar" data-widget="forex-forexTraders">
                                <div class="search-part-social">
                                    <input data-widget="forex-forexTraderSearch" data-type="search" type="text" placeholder="Trader Name" class="input-tradersearch" />
                                    <div data-widget="forex-forexTraderSearch" data-type="options" class="tradersearch-dropdown" style="display:none"></div>
                                    <span class="tradersearch-button" data-toggle="modal" data-target="#tol-explore-popup">Explore</span>
                                </div>
                                <div class="content-part-social">
                                    <ul class="nav content-part-social">
                                        <li class="most-profit-tab active">
                                            <a href="#tab11" data-toggle="tab" class="active">Most Profitable</a>
                                        </li>
                                        <li class="most-copied-tab">
                                            <a href="#tab12" data-toggle="tab">Most Copied</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane dataTable active in" id="tab11">
                                            <table class="traderoom-social table-mostprofitable hasArrows">
                                                <thead class="fake-thead">
                                                <tr>
                                                    <th data-column="avatar" data-sort="string" class="canSort canResize canDrag tarder-avatar"><div class="cellWrapper"><span class="content">Name</span></span></div></th>
                                                    <th data-column="name" data-sort="string" class="canSort canResize canDrag tarder-name"><div class="cellWrapper"><span class="content"></span></div></th>
                                                    <th data-column="plperc" data-sort="float" class="name canSort canResize canDrag" aria-sort="descending"><div class="cellWrapper"><span class="content">P/L(%)</span></div></th>
                                                    <th data-column="copies" data-sort="int" class="name canSort canResize canDrag"><div class="cellWrapper"><span class="content">Copiers</span></div></th>
                                                    <th data-column="action" data-sort="string" class="name canSort canResize canDrag"><div class="cellWrapper"><span class="content">Copy</span></div></th>
                                                </tr>
                                                </thead>
                                            </table>
                                            <div class="scrollable-social">
                                                <div class="scrollable-area-body">
                                                    <div class="scrollable-area-content">
                                                        <div class="clearfix flat-top-borders">
                                                            <table class="traderoom-social table-mostprofitable hasArrows traders-head" data-widget="forex-forexTraders" data-type="mostprofitable-table">
                                                                <thead data-type="mostprofitable-header">
                                                                <tr>
                                                                    <th data-column="avatar" data-sort="string" class="canSort canResize canDrag tarder-avatar">
                                                                        <div class="cellWrapper">
                                                                            <span class="content">Name</span>
                                                                            <span class="sortArrow"></span>
                                                                            <span class="sortBackground"></span>
                                                                        </div>
                                                                    </th>
                                                                    <th data-column="name" data-sort="string" class="canSort canResize canDrag tarder-name">
                                                                        <div class="cellWrapper">
                                                                            <span class="content"></span>
                                                                            <span class="sortArrow"></span>
                                                                            <span class="sortBackground"></span>
                                                                        </div>
                                                                    </th>
                                                                    <th data-column="plperc" data-sort="float" class="name canSort canResize canDrag" aria-sort="descending">
                                                                        <div class="cellWrapper">
                                                                            <span class="content">P/L(%)</span>
                                                                            <span class="sortArrow"></span>
                                                                            <span class="sortBackground"></span>
                                                                        </div>
                                                                    </th>
                                                                    <th data-column="copies" data-sort="int" class="name canSort canResize canDrag">
                                                                        <div class="cellWrapper">
                                                                            <span class="content">Copies</span>
                                                                            <span class="sortArrow"></span>
                                                                            <span class="sortBackground"></span>
                                                                        </div>
                                                                    </th>
                                                                    <th data-column="action" data-sort="string" class="name canSort canResize canDrag">
                                                                        <div class="cellWrapper">
                                                                            <span class="content">Copy</span>
                                                                            <span class="sortArrow"></span>
                                                                            <span class="sortBackground"></span>
                                                                        </div>
                                                                    </th>
                                                                </tr>
                                                                </thead>
                                                                <tbody></tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane dataTable fade" id="tab12">
                                            <table class="traderoom-social table-mostprofitable hasArrows">
                                                <thead class="fake-thead">
                                                <tr>
                                                    <th data-column="avatar" data-sort="string" class="canSort canResize canDrag tarder-avatar"><div class="cellWrapper"><span class="content">Name</span></span></div></th>
                                                    <th data-column="name" data-sort="string" class="canSort canResize canDrag tarder-name"><div class="cellWrapper"><span class="content"></span></div></th>
                                                    <th data-column="plperc" data-sort="float" class="name canSort canResize canDrag" aria-sort="descending"><div class="cellWrapper"><span class="content">P/L(%)</span></div></th>
                                                    <th data-column="copies" data-sort="int" class="name canSort canResize canDrag"><div class="cellWrapper"><span class="content">Copiers</span></div></th>
                                                    <th data-column="action" data-sort="string" class="name canSort canResize canDrag"><div class="cellWrapper"><span class="content">Copy</span></div></th>
                                                </tr>
                                                </thead>
                                            </table>
                                            <div class="scrollable-social">
                                                <div class="scrollable-area-body">
                                                    <div class="scrollable-area-content">
                                                        <div class="clearfix flat-top-borders">
                                                            <table class="traderoom-social table-mostcopied hasArrows traders-head" data-widget="forex-forexTraders" data-type="mostcopied-table">
                                                                <thead data-type="mostcopied-header">
                                                                <tr>
                                                                    <th data-column="avatar" data-sort="string" class="canSort canResize canDrag trader-avatar">
                                                                        <div class="cellWrapper">
                                                                            <span class="content">Name</span>
                                                                            <span class="sortArrow"></span>
                                                                            <span class="sortBackground"></span>
                                                                        </div>
                                                                    </th>
                                                                    <th data-column="name" data-sort="string" class="canSort canResize canDrag">
                                                                        <div class="cellWrapper">
                                                                            <span class="content"></span>
                                                                            <span class="sortArrow"></span>
                                                                            <span class="sortBackground"></span>
                                                                        </div>
                                                                    </th>
                                                                    <th data-column="plperc" data-sort="float" class="name canSort canResize canDrag">
                                                                        <div class="cellWrapper">
                                                                            <span class="content">P/L(%)</span>
                                                                            <span class="sortArrow"></span>
                                                                            <span class="sortBackground"></span>
                                                                        </div>
                                                                    </th>
                                                                    <th data-column="copies" data-sort="int" class="name canSort canResize canDrag" aria-sort="descending">
                                                                        <div class="cellWrapper">
                                                                            <span class="content">Copies</span>
                                                                            <span class="sortArrow"></span>
                                                                            <span class="sortBackground"></span>
                                                                        </div>
                                                                    </th>
                                                                    <th data-column="action" data-sort="string" class="name canSort canResize canDrag">
                                                                        <div class="cellWrapper">
                                                                            <span class="content">Copy</span>
                                                                            <span class="sortArrow"></span>
                                                                            <span class="sortBackground"></span>
                                                                        </div>
                                                                    </th>
                                                                </tr>
                                                                </thead>
                                                                <tbody></tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="tab-pane fade" id="tab3">
                        <div class="tol-messages-sidebar">
                            <div class="scrollable-area scrollable-area-at-top">
                                <div class="scrollable-area-body">
                                    <div class="scrollable-area-content">
                                        <div class="clearfix flat-top-borders">
                                            <?php echo do_shortcode('[messageInboxAllMessages]'); ?>
                                            <?php echo do_shortcode('[messageInboxContent]'); ?>
                                            <div class="pagination-wrapper">
                                                <?php echo do_shortcode('[messageInboxPaging]'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<ul class="footer-nav nav nav-tabs">
    <li class="footer-icon-quotes active">
        <a data-toggle="tab" href="#home" data-translate="Quotes"></a>
    </li>
    <li class="footer-icon-chart">
        <a data-toggle="tab" href="#menu1" data-translate="chart"></a>
    </li>
    <li class="footer-icon-positions">
        <a data-toggle="tab" href="#menu2" data-translate="openPositions"></a>
    </li>
    <li class="footer-icon-orders">
        <a data-toggle="tab" href="#menu3" data-translate="pendingOrders"></a>
    </li>
    <li class="footer-icon-more">
        <a data-toggle="tab" href="#menu4" data-translate="More"></a>
    </li>
</ul>

<!-- Trading View chart library starts here -->
<script data-cfasync="false" type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/tradingview/charting_library/charting_library.min.js"></script>
<script data-cfasync="false" type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/tradingview/datafeeds/datafeed.js"></script>
<script type="text/javascript">
    function initializeChart() {
        $(window).on('loadingIsCompleted', function (event) {
            const waitingForOptions = 100;
            let checkOptions = setTimeout(function waitForOptions (){
                const tradeOptions = forexHelper.options;
                const stillWaiting = tradeOptions.length === 0;
                if (stillWaiting) {
                    checkOptions = setTimeout(waitForOptions,waitingForOptions);
                }
                if (!stillWaiting) {
                    const chartTheme = migesco.chooseTheme();
                    tradingViewWidget = window.tvWidget = new TradingView.widget({
                        theme: chartTheme,
                        autosize: true,
                        symbol: tradeOptions[0].name,
                        interval: 1,
                        container_id: "tv_chart_container",
                        datafeed: new Datafeeds.UDFCompatibleDatafeed("https://advfeed.finte.co/tradingviewservices.asmx", 1000),
                        library_path: "<?php echo get_template_directory_uri(); ?>/tradingview/charting_library/",
                        locale: "<?php echo prior_lang() ?>",
                        drawings_access: {
                            type: 'black',
                            tools: [
                                {
                                    name: "Regression Trend"
                                }
                            ]
                        },
                        disabled_features: [
                            "create_volume_indicator_by_default",
                            "footer_screenshot",
                            "header_screenshot",
                            "header_interval_dialog_button",
                            "header_symbol_search",
                            "symbol_search_hot_key",
                            "border_around_the_chart",
                            "uppercase_instrument_names",
                            "constraint_dialogs_movement",
                        ],
                        enabled_features: [
                            "side_toolbar_in_fullscreen_mode",
                            "caption_buttons_text_if_possible",
                            "lock_visible_time_range_on_resize",
                            "study_templates",
                            "no_min_chart_width",
                            "timeframes_toolbar",
                            "same_data_requery",
                        ],
                        charts_storage_url: 'https://saveload.tradingview.com',
                        charts_storage_api_version: "1.1",
                        supports_search: false,
                        client_id: 'tradingview.com',
                        user_id: 'public_user',
                        overrides: {
                            "timezone": "Europe/Moscow"
                        },
                    });

                    $("#tv_chart_container").parent().addClass('loaded');
                }
            }, waitingForOptions);
        });
    }
    window.addEventListener('DOMContentLoaded', initializeChart, false);

    $(document).ready(function () {
        $('#tol-change-modes').on('hidden.bs.modal', function () {
            $('.body-text #themes, .body-text #modes').removeClass('in').attr('aria-expanded', false);
            $('.settings-row').removeClass('collapsed').attr('aria-expanded', false)
        });

        $('#changeCtrls').on('show.bs.collapse','.collapse', function() {
            $('#changeCtrls').find('.collapse.in').collapse('hide');
        });

        $('.forex-change-mode').on('click', function(){
            if ((document.getElementById("aggregate").checked && widgets.user.forexModeId === 2) || (document.getElementById("hedging").checked && widgets.user.forexModeId == 3)) {
                $('#tol-change-modes').modal('hide');
            } else {
                widgets.api.changeForexMode((document.getElementById("aggregate").checked ? '2' : '3'), function(response){
                    if (response.code === 200) {
                        widgets.api.getUserData([widgets.processUser, function () {
                            $('#tol-change-modes').modal('hide');
                            $('#tol-change-modes-result .forex-asset-name').text(document.getElementById("aggregate").checked ? widgetMessage['change-mode-to-agg-title'] : widgetMessage['change-mode-to-hedg-title']);
                            $('#tol-change-modes-result .body-text').text(helper.getTranslation('change-mode-success','Trading mode has been switched successfully.'));

                            if (bringBackSocialProfile) {
                                forexSocialProfileWidget.initPopup(currentTraderID, 'copy', false);
                                bringBackSocialProfile = false;
                                currentTraderID = null;
                            }
                        }]);
                    } else {
                        $('#tol-change-modes').modal('hide');
                        $('#tol-change-modes-result .forex-asset-name').text(document.getElementById("aggregate").checked ? widgetMessage['change-mode-to-agg-title'] : widgetMessage['change-mode-to-hedg-title']);
                        $('#tol-change-modes-result .body-text').text(document.getElementById("aggregate").checked ? widgetMessage['change-mode-to-agg-fail'] : widgetMessage['change-mode-to-hedg-fail']);
                        $('#tol-change-modes-result').modal('show');
                        if (widgets.user.forexModeId === 3) {
                            document.getElementById("aggregate").checked = false;
                            document.getElementById("hedging").checked = true;
                        } else {
                            document.getElementById("aggregate").checked = true;
                            document.getElementById("hedging").checked = false;
                        }
                    }
                });
            }
        });

        $('.forex-change-theme').on('click', function() {
            $('#themes').removeClass('in');
            $('.settings-row[data-target=#themes]').attr('aria-expanded',false);

            if (document.getElementById("whitetheme").checked) {
                $('body').addClass('white-theme');
                $('label[for=darktheme]').text('Switch to Dark theme');
                $('label[for=whitetheme]').text('White theme');

                migesco.becomeLight(tradingViewWidget);

            } else {
                $('body').removeClass('white-theme');
                $('label[for=darktheme]').text('Dark theme');
                $('label[for=whitetheme]').text('Switch to White theme');

                migesco.becomeDark(tradingViewWidget);
            }
        });


        $('.copy-diff-mode').on('click', function() {
            bringBackSocialProfile = true;
            currentTraderID = $('.copy-diff-mode').parent().data('traderid');
            $('#tol-social-popup').modal('hide');
            $('#tol-change-modes').modal('show');
            $('.settings-row[data-target="#modes"]').click();
        });
    });

    $(window).on('loadingIsCompleted', function(){
        bringBackSocialProfile = false;

        if (widgets.isLogged()) {
            if (widgets.user.forexModeId === 3) {
                document.getElementById("aggregate").checked = false;
                document.getElementById("hedging").checked = true;
            } else {
                document.getElementById("aggregate").checked = true;
                document.getElementById("hedging").checked = false;
            }
        }

        if ($.cookie('theme') == 'light') {
            document.getElementById("darktheme").checked = false;
            document.getElementById("whitetheme").checked = true;
            $('label[for=darktheme]').text('Switch to Dark theme');
            $('label[for=whitetheme]').text('White theme');
        } else {
            $('label[for=darktheme]').text('Dark theme');
            $('label[for=whitetheme]').text('Switch to White theme');
            document.getElementById("darktheme").checked = true;
            document.getElementById("whitetheme").checked = false;
        }
    });
</script>
<style>
    #tv_chart_container{visibility:hidden;height:100%;}
    #tv_chart_container iframe {height:100% !important;}
    .panel.loaded{border:0;}
    .panel.loaded #tv_chart_container{visibility:visible}
</style>
<div class="ui-widget-overlay"></div>
