 <div id="tol-myProfile-layout-1">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="pageHeadTitle">
            <?php require_once( ABSPATH . 'wp-content/themes/tol-parent/layouts/breadcrumb.php' ); ?>
        </div>

        <div id="form-wrapper" class="container compliance-widget-wrapper">
            <div data-widget="compliance" data-type="main">

                <div class="compliance-menu row">
                    <div class="col-xs-4 arrow tabs  udetails">
                        <a href="javascript:;" class="compliance-tab" data-widget="compliance" data-type="menu" data-target="userdetails">Personal Details
                        </a>
                        <span class="compliance-arrow"></span>
                    </div>
                    <div class="col-xs-4 arrow tabs  questionnaires">
                        <a href="javascript:;" class="compliance-tab" data-widget="compliance" data-type="menu" data-target="questionnaires">Questionaries
                        </a>
                        <span class="compliance-arrow"></span>
                    </div>
                    <div class="col-xs-4 arrow tabs  documents">
                        <a href="javascript:;" class="compliance-tab" data-widget="compliance" data-type="menu" data-target="documents">Documents
                        </a>
                    </div>

                </div>
 
 <div data-widget="compliance" data-type="section" data-item="userdetails" class="compliance-userdetails compliance-container row">
                    <!-- Start My Profile -->
                    <form class="form-horizontal col-md-6">
                        <div class="my-profile-title"></div>
                        <div class="myprofile-successmsg"></div>
                        <?php echo do_shortcode('[userDetailsError]'); ?>
                        <div class="row tol-my-profile-column">
                            <div class="col-sm-6">
                                <div class="form-group col-sm-12">
                                    <a id="mobilescroll"></a>
                                    <label class="hidden-xs col-sm-12 col-md-4 control-label text-left" data-translate="Registration_Email"></label>
                                    <div class="col-sm-12 col-md-8">
                                        <?php echo do_shortcode('[userDetailsEmail class="form-control"]'); ?>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label class="hidden-xs col-sm-12 col-md-4 control-label text-left" data-translate="Registration_Title"></label>
                                    <div class="col-sm-12 col-md-8">
                                        <?php echo do_shortcode('[userDetailsETitle class="form-control"]'); ?>
                                    </div>
                                </div>
                                <div class="form-group col-xs-5 col-sm-12">
                                    <label class="hidden-xs col-sm-12 col-md-4 control-label text-left" data-translate="Registration_FirstName"></label>
                                    <div class="col-sm-12 col-md-8">
                                        <?php echo do_shortcode('[userDetailsFirstName class="form-control"]'); ?>
                                    </div>
                                </div>
                                <div class="form-group col-xs-7 col-sm-12">
                                    <label class="hidden-xs col-sm-12 col-md-4 control-label text-left" data-translate="Registration_LastName"></label>
                                    <div class="col-sm-12 col-md-8">
                                        <?php echo do_shortcode('[userDetailsLastName class="form-control"]'); ?>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label class="hidden-xs col-sm-12 col-md-4 control-label text-left" data-translate="Registration_Country"></label>
                                    <div class="col-sm-12 col-md-8">
                                        <?php echo do_shortcode('[userDetailsCountryCode class="form-control"]'); ?>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label class="hidden-xs col-sm-12 col-md-4 control-label text-left" data-translate="Registration_DateOfBirth"></label>
                                    <div class="col-sm-12 col-md-8">
                                        <div class="col-xs-3 col-sm-2 nopadding">
                                            <?php echo do_shortcode('[userDetailsBirthDay class="form-control my-profile-birth-day"]'); ?>
                                        </div>
                                        <div class="col-xs-6 col-sm-6">
                                            <?php echo do_shortcode('[userDetailsBirthMonth class="form-control"]'); ?>
                                        </div>
                                        <div class="col-xs-3 col-sm-4 nopadding">
                                            <?php echo do_shortcode('[userDetailsBirthYear class="form-control"]'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group col-sm-12">
                                    <label class="hidden-xs col-sm-12 col-md-4 control-label text-left" data-translate="Registration_Street"></label>
                                    <div class="col-sm-12 col-md-8">
                                        <?php echo do_shortcode('[userDetailsAddressLine1 class="form-control"]'); ?>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label class="hidden-xs col-sm-12 col-md-4 control-label text-left" data-translate="Registration_HouseNum"></label>
                                    <div class="col-sm-12 col-md-8">
                                        <?php echo do_shortcode('[userDetailsAddressLine2 class="form-control"]'); ?>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label class="hidden-xs col-sm-12 col-md-4 control-label text-left" data-translate="Registration_City"></label>
                                    <div class="col-sm-12 col-md-8">
                                        <?php echo do_shortcode('[userDetailsCity class="form-control"]'); ?>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label class="hidden-xs col-sm-12 col-md-4 control-label text-left" data-translate="Registration_Phone1"></label>
                                    <div class="col-sm-12 col-md-8">
                                        <div class="col-xs-2 col-sm-2 nopadding">
                                            <?php echo do_shortcode('[userDetailsCountryPhoneCode class="form-control"]'); ?>
                                        </div>
                                        <div class="col-xs-4 col-sm-4">
                                            <?php echo do_shortcode('[userDetailsAreaPhoneCode class="form-control"]'); ?>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 nopadding">
                                            <?php echo do_shortcode('[userDetailsPhone class="form-control"]'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label class="hidden-xs col-sm-12 col-md-4 control-label text-left" data-translate="Registration_Phone2"></label>
                                    <div class="col-sm-12 col-md-8">
                                        <?php echo do_shortcode('[userDetailsSecondaryPhone class="form-control"]'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group col-sm-12">
                                    <label class="hidden-xs col-sm-12 col-md-4 control-label text-left">&nbsp;</label>
                                    <div class="col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="btn-group col-xs-12">
                                            <a href="#" class="btn btn-default btn-connect col-xs-2">
                                                <svg class="social-login">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#gl"></use>
                                                </svg>
                                            </a>
                                            <div data-widget="userDetails" data-type="googleLoginBtn" class="widgetPlaceholder loginGoogle btn btn-default col-xs-10" style="display:none;"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="btn-group col-xs-12">
                                            <a href="#" class="btn btn-default btn-connect col-xs-2">
                                                <svg class="social-login">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#fb"></use>
                                                </svg>
                                            </a>
                                            <div data-widget="userDetails" data-type="fbLoginBtn" class="widgetPlaceholder linkFacebook btn btn-default col-xs-10"></div>
                                        </div>
                                    </div>

                                      <div data-widget="userDetails" data-type="fbLoginErrorMessage" class="fbLoginErrorMessage"></div>
                                      <div data-widget="userDetails" data-type="googleLoginErrorMessage" class="fbLoginErrorMessage"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">&nbsp;</div>
                            <div class="col-sm-6">
                                <div class="form-group col-sm-12">
                                    <label class="hidden-xs col-sm-12 col-md-4 control-label text-left">&nbsp;</label>
                                    <div class="col-sm-12 col-md-8">
                                        <div class="col-sm-6 nopadding text-left">
                                            <button class="tol-quick-submit col-sm-11 btn btn-primary" data-widget="userDetails" data-type="submit" data-translate="Button_Submit"></button>
                                            </form>
                                        </div>
                                        <div class="col-sm-6 nopadding">
                                            <a class="tol-quick-next col-sm-11 btn btn-primary pull-right">Next Step</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                    
                    <!-- End My Profile -->
                </div>

                <div data-widget="compliance" data-type="section" data-item="questionnaires" class="compliance-questionnaire compliance-container row" style="display: none;">
                    <div class="questionnaireguide" data-translate="questionnareGuide"></div>
                    <div class="questionnaireerror" data-type="questionnaireerror" style="display: none"></div>
                    <div data-widget="compliance" data-type="questions" class="compliance-questions"></div>
                </div>

                <div class="compliance-documents compliance-container row" data-widget="compliance" data-type="section" data-item="documents" style="display: none;">
                    <div class="documentsguide" data-translate="missingdocuments"></div>
                    <div id="docsguide-upload" class="documentsguide">Upload Document</div>

                    <div class="documenthint"></div>
                    <div data-widget="compliance" data-type="documentsupload"></div>

                    <table class="table table-striped table-responsive">
                        <thead class="doctable">
                            <tr>
                                <th data-translate="documentName"></th>
                                <th data-translate="documentType"></th>
                                <th data-translate="documentState"></th>
                            </tr>
                         </thead>
                         <tbody class="uploadeddocs" data-widget="compliance" data-item="uploadedDocs"></tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="entry-content">
            <?php echo $post->post_content; ?>
        </div>
    </article>
</div>