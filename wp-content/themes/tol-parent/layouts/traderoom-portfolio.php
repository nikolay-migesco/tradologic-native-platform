<?php
wp_enqueue_style('customScroll');
wp_enqueue_script('customSlider', 'jquery');
wp_enqueue_style('customSlider', 'jquery');
?>

<div class="layout" id="layout">
    <div class="traderoom-loader" style="display: none;">
        <div class="loader">
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
        </div>
    </div>
    <div class="portfolio-investment-header">
        <div  class="portfolio-investment-header-title">YOUR INVESTMENT PLAN</div>
        <div  class="portfolio-investment-header-description">WE RECOMMEND A GROWTH PORTFOLIO FOR YOU</div>
    </div>
    <div class="portfolio-investment-content">
        <ul class="nav nav-tabs navigation" role="tablist">
            <li role="presentation" class="active"><a href="#overview" aria-controls="overview" role="tab" data-toggle="tab">OVERVIEW</a></li>
            <li role="presentation"><a href="#pastPerformance" aria-controls="pastPerformance" role="tab" data-toggle="tab">PAST PERFORMANCE</a></li>
            <li role="presentation"><a href="#predictedPerformance" aria-controls="predictedPerformance" role="tab" data-toggle="tab">PREDICTED PERFORMANCE</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="row portfolio-investment-tabcontent" data-widget="portfolioAdviser">
            <div class="col-md-3 portfolio-investment-tabcontent-left">
                <div class="portfolio-investment-tabcontent-left-title">PORTFOLIO NAME</div>
                <div class="portfolio-investment-tabcontent-left-controls">
                    <span class="init-left">Initial investment</span>
                    <input class="init-right" data-widget="portfolioAdviser" data-type="investmentInput"/>
                    <input type="range" data-widget="portfolioAdviser" min="0" max="10000" data-type="investmentRange" value="1000">
                </div>
                <div class="portfolio-investment-tabcontent-left-controls">
                    <span class="init-left">Investment period</span>
                    <input class="init-right" data-widget="portfolioAdviser" data-type="periodInput"/>
                    <input type="range" data-widget="portfolioAdviser" min="0" max="10" data-type="periodRange"  value="5">
                </div>
                <div class="portfolio-investment-tabcontent-left-controls">
                    <span class="init-left">Risk Level</span>
                    <ul class="nav nav-tabs risk-tabs" id="ul-risk" role="tablist" data-widget="portfolioAdviser" data-type="riskOptions">
                        <li role="presentation" value="1" class="active"><a href="#" role="tab" data-toggle="tab">Low</a></li>
                        <li role="presentation" value="2"><a href="#" role="tab" data-toggle="tab">Medium</a></li>
                        <li role="presentation" value="3"><a href="#" role="tab" data-toggle="tab">High</a></li>
                    </ul>
                </div>
                <div class="portfolio-investment-tabcontent-left-controls">
                    <div data-widget="portfolioAdviser" data-type="investBtn" class="nav-button button portfolio-investment-investbtn" data-translate="InvestBtn">Invest</div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active portfolio-investment-overview" id="overview">
                        <div class="row">
                            <div class="col-md-8" data-widget="portfolioAdviser" data-type="strategiesOverview">
<!--                                <div class="portfolio-investment-overview-item">-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-4">EMERGING MARKETS</div>-->
<!--                                        <div class="col-md-4">$10,000</div>-->
<!--                                        <div class="col-md-4">10%</div>-->
<!--                                    </div>-->
<!--                                    <div class="w3-light-yellow portfolio-investment-overview-item-bar">-->
<!--                                        <div class="w3-yellow" style="width: 10%"></div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="portfolio-investment-overview-item">-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-4">DAILY FOREX</div>-->
<!--                                        <div class="col-md-4">$10,000</div>-->
<!--                                        <div class="col-md-4">17%</div>-->
<!--                                    </div>-->
<!--                                    <div class="w3-light-yellow portfolio-investment-overview-item-bar">-->
<!--                                        <div class="w3-cyan" style="width: 17%"></div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="portfolio-investment-overview-item">-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-4">US STOCKS</div>-->
<!--                                        <div class="col-md-4">$10,000</div>-->
<!--                                        <div class="col-md-4">35%</div>-->
<!--                                    </div>-->
<!--                                    <div class="w3-light-yellow portfolio-investment-overview-item-bar">-->
<!--                                        <div class="w3-blue" style="width: 35%"></div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="portfolio-investment-overview-item">-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-4">WORLD INVESTOR</div>-->
<!--                                        <div class="col-md-4">$10,000</div>-->
<!--                                        <div class="col-md-4">13%</div>-->
<!--                                    </div>-->
<!--                                    <div class="w3-light-yellow portfolio-investment-overview-item-bar">-->
<!--                                        <div class="w3-green" style="width: 13%"></div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="portfolio-investment-overview-item">-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-4">AGGRESIVE TRADER</div>-->
<!--                                        <div class="col-md-4">$10,000</div>-->
<!--                                        <div class="col-md-4">4%</div>-->
<!--                                    </div>-->
<!--                                    <div class="w3-light-yellow portfolio-investment-overview-item-bar">-->
<!--                                        <div class="w3-purple" style="width: 4%"></div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="portfolio-investment-overview-item">-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-4">TOTAL:</div>-->
<!--                                        <div class="col-md-4">$10,000</div>-->
<!--                                        <div class="col-md-4">100%</div>-->
<!--                                    </div>-->
<!--                                </div>-->
                            </div>
                            <div class="col-md-4 portfolio-investment-donut" data-widget="portfolioAdviser" data-type="strategiesChart">
                                <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="pastPerformance">
                        <div class="row">
                            <div class="col-md-8 portfolio-investment-pastp-chart">
                                <div id="pastPerformanceChart"></div>
                            </div>
                            <div class="col-md-4">
                                <div class="portfolio-investment-pastp-right rec">
                                    <span class="value" data-widget="portfolioAdviser" data-type="pastPerformanceTotal">+38.5%</span>
                                    <span class="title">Recommended portfolio</span>
                                </div>
                                <div class="portfolio-investment-pastp-right glob">
                                    <span class="value" data-widget="portfolioAdviser" data-type="globalIndexTotal">+15.2%</span>
                                    <span class="title">Global index</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="predictedPerformance">
                        <div class="row">
                            <div class="col-md-8">
                                <div id="predictedPerformanceChart"></div>
                            </div>
                            <div class="col-md-4 portfolio-investment-predictedp-stats">
                                <div class="row">
                                    <span class="title">INVESTMENT</span>
                                    <span class="value" data-widget="portfolioAdviser" data-type="summaryInvestment">$100,000</span>
                                </div>
                                <div class="row">
                                    <span class="title">PREDICTED VALUE</span>
                                    <span class="value" data-widget="portfolioAdviser" data-type="summaryValue">$461,386</span>
                                </div>
                                <div class="row">
                                    <span class="title">PROFIT</span>
                                    <span class="value blue" data-widget="portfolioAdviser" data-type="summaryProfit">$361,386</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Trading View chart library starts here -->
<script data-cfasync="false" type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/charting_library/charting_library.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script data-cfasync="false" type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/charting_library/datafeed/udf/datafeed.js"></script>
<script data-cfasync="false" type="text/javascript">

    // TAB 3

    $(function() {
        // Make monochrome colors and set them as default for all pies
        Highcharts.getOptions().plotOptions.column.colors = (function() {
            var colors = [],
                base = Highcharts.getOptions().colors[0],
                i;

            base = '#058373';

            for (i = 0; i < 50; i += 1) {
                // Start out with a darkened base color (negative brighten), and end
                // up with a much brighter color
                colors.push(Highcharts.Color(base).brighten((i - 10) / 25).get());
            }
            return colors;
        }());

        var chart = new Highcharts.Chart({
            chart: {
                renderTo: 'predictedPerformanceChart',
                backgroundColor: '#0f1721',
                type: 'column'
            },
            title: {
                text: ''
            },
            xAxis: {
                lineColor: '#2D3D4F',
                labels: {
                    enabled: false,
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                title: {
                    text: ''
                },
                gridLineColor: '#2D3D4F'
            },
            tooltip: {
                formatter: function() {
                    return '<b>' + this.series.name + '</b><br/>' +
                        this.x + ': ' + this.y;
                }
            },
            plotOptions: {
                column: {
                    colorByPoint: true,
                    borderWidth: 0
                }
            },
            series: [{
                data: [500, 1000, 2500, 5000, 7500, 10000, 12500, 15000, 17500, 20000, 22500, 25000, 27500, 30000, 32500, 35000, 37500, 40000, 42500, 45000, 50000, 52500, 55000, 57500, 60000]
            }]
        });
    });



    // TAB 3 - End

    // TAB 2


    Highcharts.chart('pastPerformanceChart', {
        chart: {
            backgroundColor: '#0f1721',
            type: 'line'
        },

        title: {
            text: ''
        },

        yAxis: {
            title: {
                text: ''
            },
            gridLineColor: '#2D3D4F',
            labels: {
                style: { "color": "#A8A8A8" }
            }
        },

        xAxis: {
            labels: {
                style: { "color": "#A8A8A8" }
            }
        },

        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            enabled: false
        },

        plotOptions: {
            series: {
                pointStart: 2010
            }
        },

        series: [{
            name: 'Installation',
            data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
        }, {
            name: 'Manufacturing',
            data: [24916, 24064, 29742, 29851, 32490, 30282, 38121, 40434]
        }, {
            name: 'Sales & Distribution',
            data: [11744, 17722, 16005, 19771, 20185, 24377, 32147, 39387]
        }, {
            name: 'Project Development',
            data: [null, null, 7988, 12169, 15112, 22452, 34400, 34227]
        }, {
            name: 'Other',
            data: [12908, 5948, 8105, 11248, 8989, 11816, 18274, 18111]
        }]

    });


    // TAB 2 - END

    // TAB 1

//    Highcharts.chart('container', {
//        chart: {
//            type: 'pie',
//            backgroundColor: '#0f1721',
//            borderColor: '#0f1721'
//        },
//        title: {
//            text: '19.6%<br>EXPECTED RETURN<br>',
//            align: 'center',
//            verticalAlign: 'middle',
//            y: -10,
//            style: { "color": "white" }
//        },
//        credits: {
//            enabled: false
//        },
//        plotOptions: {
//            pie: {
//                borderWidth: 0,
//                dataLabels: {
//                    enabled: false
//                }
//            }
//        },
//        series: [{
//            size: 260,
//            innerSize: 190,
//            startAngle: 0,
//            endAngle: 10,
//            data: [{
//                y: 1,
//                color: '#D54AED'
//            }]
//        }, {
//            size: 245,
//            innerSize: 210,
//            startAngle: 10,
//            endAngle: 40,
//            data: [{
//                y: 1,
//                color: '#F2ECC4'
//            }]
//        }, {
//            size: 255,
//            innerSize: 200,
//            startAngle: 40,
//            endAngle: 90,
//            data: [{
//                y: 1,
//                color: '#A9E39A'
//            }]
//        }, {
//            size: 260,
//            innerSize: 190,
//            startAngle: 90,
//            endAngle: 180,
//            data: [{
//                y: 1,
//                color: '#9AE3D3'
//            }]
//        }, {
//            size: 270,
//            innerSize: 180,
//            startAngle: 180,
//            endAngle: 360,
//            data: [{
//                y: 1,
//                color: '#7C8CE6'
//            }]
//        }]
//    });

    // TAB 1 - END

//    Highcharts.chart('container', {
//        chart: {
//            plotBackgroundColor: null,
//            plotBorderWidth: 0,
//            plotShadow: false,
//            backgroundColor: '#0f1721',
//            borderColor: '#0f1721',
//            height: '300px'
//        },
//        title: {
//            text: '19.6%<br>EXPECTED RETURN<br>',
//            align: 'center',
//            verticalAlign: 'middle',
//            y: 40,
//            style: { "color": "white" }
//        },
//        tooltip: {
//            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
//        },
//        plotOptions: {
//            pie: {
//                dataLabels: {
//                    enabled: false,
//                    distance: -50,
//                    style: {
//                        fontWeight: 'bold',
//                        color: 'white'
//                    }
//                },
//                startAngle: -180,
//                endAngle: 180,
//                center: ['50%', '75%'],
//                borderColor: '#0f1721'
//            }
//        },
//        series: [{
//            type: 'pie',
//            name: 'Browser share',
//            innerSize: '70%',
//            data: [
//                ['Firefox',   10.38],
//                ['IE',       56.33],
//                ['Chrome', 24.03],
//                ['Safari',    4.77],
//                ['Opera',     0.91],
//                {
//                    name: 'Proprietary or Undetectable',
//                    y: 0.2,
//                    dataLabels: {
//                        enabled: false
//                    }
//                }
//            ]
//        }]
//    });


</script>
<div class="ui-widget-overlay"></div>
