<?php
    wp_enqueue_style('customScroll');
    wp_enqueue_script('customSlider', 'jquery');

    if ($GLOBALS['curr_product'] == 'newbinary') {
        setcookie('view', 'single', time()+3600*24 );

        $game = "digital";
        $digital_active = 'active';
        $range_active = '';
        $touch_active = '';
        $turbo_active = '';
        $advanced_active = '';

        switch (@$_GET['game']) {
            case "digital":
                $game = "digital";
                $digital_active = 'active';
                $range_active = '';
                break;
            case "range":
                $game = "range";
                $digital_active = '';
                $turbo_active = '';
                $range_active = 'active';
                break;
            case "touch":
                $game = "touch";
                $digital_active = '';
                $touch_active = 'active';
                $range_active = '';
                break;
            case "turboBinary":
                $game = "turboBinary";
                $digital_active = '';
                $turbo_active = 'active';
                break;
            case "advanced":
                $game = "advanced";
                $digital_active = '';
                $advanced_active = 'active';
                break;
            default:
                $game = "digital";
        };

        setcookie('game', $game);
        setcookie('chartType', 'chartTypeCandle');
    } else {
        setcookie('oneClickTrade', 'off', time()+3600*24 );
    };
?>

<div class="layout <?php echo($GLOBALS['curr_product'] == 'newbinary' ? ('binary ' . $game) : $GLOBALS['curr_product']) ?>" id="layout">
    <div class="traderoom-loader">
        <div class="loader">
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
        </div>
    </div>

    <!-- START Traderoom content here -->
    <?php
        if ($GLOBALS['curr_product'] == 'forex') {
            setcookie('game', 'RealForex', time()+3600*24, "/");
            include(get_template_directory() . '/layouts/traderoom-24.php');
        } else if ($GLOBALS['curr_product'] == 'realm') {
            setcookie('game', 'RealForex', time() + 3600 * 24, "/");
            include(get_template_directory() . '/layouts/traderoom-24m.php');
        } else if ($GLOBALS['curr_product'] == 'realil') {
            setcookie('game', 'RealForex', time() + 3600 * 24, "/");
            include(get_template_directory() . '/layouts/traderoom-24il.php');
        } else if ($GLOBALS['curr_product'] == 'simplex' || $GLOBALS['curr_product'] == 'crypto') {
            setcookie('game', 'EasyForex', time()+3600*24, "/");
            include(get_template_directory() . '/layouts/traderoom-25.php');
        } else if ($GLOBALS['curr_product'] == 'simplexm' || $GLOBALS['curr_product'] == 'cryptom') {
            setcookie('game', 'EasyForex', time()+3600*24, "/");
            include(get_template_directory() . '/layouts/traderoom-25m.php');
        } else if ($GLOBALS['curr_product'] == 'newbinary') {
            include(get_template_directory() . '/layouts/traderoom-28.php');
        } else if ($GLOBALS['curr_product'] == 'newbinaryil') {
            include(get_template_directory() . '/layouts/traderoom-28il.php');
        } else if ($GLOBALS['curr_product'] == 'newbinarym') {
            include(get_template_directory() . '/layouts/traderoom-28m.php');
        } else if ($GLOBALS['curr_product'] == 'exchange') {
            include(get_template_directory() . '/layouts/traderoom-20.php');
        }
    ?>
    <!-- END Traderoom content here -->
</div>
<div class="ui-widget-overlay"></div>
