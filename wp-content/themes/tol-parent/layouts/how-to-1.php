<?php
get_header();
switch (ICL_LANGUAGE_CODE) {

    case "de":
        $language = "de";
        break;
    case "ja":
        $language = "jp";
        break;
    case "ar":
        $language = "ar";
        break;
    case "zh-hans":
        $language = "cn";
        break;
    case "it":
        $language = "it";
        break;
    case "pl":
        $language = "pl";
        break;
    case "pt-pt":
        $language = "pt";
        break;
    case "ru":
        $language = "ru";
        break;
    case "tr":
        $language = "tr";
        break;
    default:
        $language = "en";
}
?>

<div id="content" role="main" class="container-fluid">
    <div class="row col-sm-offset-1">
        <div class="row col-sm-offset-1">
            <div class="col-sm-6">
                <p class="helvetica-bold-head">Binary Option Trading</p>
            </div>
            <div class="col-sm-6">
                <span class="breadrumb-line"></span><span class="index-top">Education</span>
            </div>
        </div>
        <div class="row col-sm-offset-1">
            <p class="info-slides-par col-sm-8">Name welcomes the opportunity to work with new partners and we value every partnership inquiry we receive. Please fill in your details and we will get back to you as quickly as possible.</p>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <p class="helvetica-bold-dark">What's in the video</p>
                <ul class="video-list">
                    <li>
                        <span class="video-list-item">Lorem ipsum dolor sit amet</span>
                    </li>
                    <li>
                        <span class="video-list-item">Lorem ipsum dolor sit amet</span>
                    </li>
                    <li>
                        <span class="video-list-item">Lorem ipsum dolor sit amet</span>
                    </li>
                    <li>
                        <span class="video-list-item">Lorem ipsum dolor sit amet</span>
                    </li>
                    <li>
                        <span class="video-list-item">Lorem ipsum dolor sit amet</span>
                    </li>
                </ul>
            </div>
            <div class="col-sm-8">
                <div id="videoSection">
                    <img class="img-responsive" src="/wp-content/themes/tol-child/images/Shape.png"/>
                    <img class="player img-responsive" src="/wp-content/themes/tol-child/images/Play-button.png"/>
                </div>
            </div>
        </div>
        <div id="" class="row">
            <button id="nextVideo" class="invest-btn btn-primary">See next tutorial</button>
        </div>
        <div id="boxes" class="row">
            <div class="row">
                <div class="col-sm-3">
                    <div class="height grey dark">
                        <div class="boxHead row">
                            <p class="col-sm-6">Basic Trading</p><span class="col-sm-6 text-right"><img src="/wp-content/themes/tol-child/images/basictrading.png"/></span>
                        </div>
                        <div class="boxPara row">
                            <p>
                                Nunc ultrices dolor libero, nec maximus metus mattis sed. Etiam ornare luctus neque, sit amet aliquam odio pellentesque et. 
                            </p>
                        </div>
                        <div id="learnMore" class="row">
                            <span><span><img class="grey" src="/wp-content/themes/tol-child/images/gray.png"/></span><a>Learn more</a></span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div id="purple-frame" class="height dark">
                        <div class="boxHead row">
                            <p class="col-sm-6">Advanced Binary</p><span class="col-sm-6 text-right"><img src="/wp-content/themes/tol-child/images/next.png"/></span>
                        </div>
                         <div class="boxPara row">
                            <p>
                                Nunc ultrices dolor libero, nec maximus metus mattis sed. Etiam ornare luctus neque, sit amet aliquam odio pellentesque et.
                            </p>
                        </div>
                        <div id="learnMore" class="row">
                            <span><span><img class="greyArrow" src="/wp-content/themes/tol-child/images/gray.png"/></span><a>Learn more</a></span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="height light">
                         <div class="boxHead row">
                            <p class="col-sm-10">Financial Calendar</p><span class="col-sm-2 text-right"><img src="/wp-content/themes/tol-child/images/finacial.png"/></span>
                        </div>
                        <div class="boxPara row">
                            <p>
                                Nunc ultrices dolor libero, nec maximus metus mattis sed. Etiam ornare luctus neque, sit amet aliquam odio pellentesque et.
                            </p>
                        </div>
                         <div id="learnMore" class="row">
                            <span><span><img class="greyArrow" src="/wp-content/themes/tol-child/images/white.png"/></span><a>Learn more</a></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div id="violetFrame" class="height dark">
                        <div class="boxHead row">
                            <p class="col-sm-10">Trading Center Alerts</p><span class="col-sm-2 text-right"><img src="/wp-content/themes/tol-child/images/Trading.png"/></span>
                        </div>
                         <div class="boxPara row">
                            <p>
                                Nunc ultrices dolor libero, nec maximus metus mattis sed. Etiam ornare luctus neque, sit amet aliquam odio pellentesque et.
                            </p>
                        </div>
                        <div id="learnMore" class="row">
                            <span><span><img class="greyArrow" src="/wp-content/themes/tol-child/images/gray.png"/></span><a>Learn more</a></span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div id="purple" class="height light">
                         <div class="boxHead row">
                            <p class="col-sm-10">Boost your Trades</p><span class="col-sm-2 text-right"><img src="/wp-content/themes/tol-child/images/boost.png"/></span>
                        </div>
                         <div class="boxPara row">
                            <p>
                                Nunc ultrices dolor libero, nec maximus metus mattis sed. Etiam ornare luctus neque, sit amet aliquam odio pellentesque et.
                            </p>
                        </div>
                        <div id="learnMore" class="row">
                            <span><span><img class="greyArrow" src="/wp-content/themes/tol-child/images/white.png"/></span><a>Learn more</a></span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                     <div class="height grey dark">
                        <div class="boxHead row">
                            <p class="col-sm-6">Trading Central</p><span class="col-sm-6 text-right"><img src="/wp-content/themes/tol-child/images/Trading_center.png"/></span>
                        </div>
                        <div class="boxPara row">
                            <p>
                                Nunc ultrices dolor libero, nec maximus metus mattis sed. Etiam ornare luctus neque, sit amet aliquam odio pellentesque et. 
                            </p>
                        </div>
                        <div id="learnMore" class="row">
                            <span><span><img class="grey" src="/wp-content/themes/tol-child/images/gray.png"/></span><a>Learn more</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>   
</div><!-- #content -->

<?php
get_footer();
?>