	<div class="page-content" style="background-color:#111214;">
		<div class="breadcrumbs__wrapper">
			<?php require_once(ABSPATH.'wp-content/themes/tol-parent/layouts/breadcrumb.php' ); ?>
		</div>
		<div class="container cfd-container">
			
<div id="tol-registration-layout-12" class="widgetContent registration-wrapper">
        <div class="entry-content container">
            <div class="">
                
                <div class="inner-pages-wrap">
                    <div class="registration-page">
                        <div class="widgetContent registration-wrapper">
                            <form>
                                <fieldset class="wrapper registration">
                                    <div class="left-side">
										<div class="label-field-wrapper text-container firstName-container">
                                            <label class="label required" data-translate="Registration_FirstName"></label>
                                            <?php echo do_shortcode('[registrationFirstName class="form-control"]'); ?>
                                        </div>

                                        <div class="label-field-wrapper text-container lastName-container">
                                            <label class="label required" data-translate="Registration_LastName"></label>
                                            <?php echo do_shortcode('[registrationLastName class="form-control"]'); ?>
                                        </div>
										
                                        <div class="label-field-wrapper text-container email-container">
                                            <label class="label required" data-translate="Registration_Email"></label>
                                            <?php echo do_shortcode('[registrationEmail class="form-control"]'); ?>
                                        </div>

                                        <div class="label-field-wrapper text-container password-container">
                                            <label class="label required" data-translate="Registration_Password_:"></label>
                                            <?php echo do_shortcode('[registrationPassword class="form-control"]'); ?>
                                        </div>

                                        <div class="label-field-wrapper text-container confirmPassword-container">
                                            <label class="label required" data-translate="Registration_Password2_:"></label>
                                            <?php echo do_shortcode('[registrationConfirmPassword class="form-control"]'); ?>
                                        </div>
                                      <div class="label-field-wrapper text-container countryCode-container">
                                           <label class="label required" data-translate="Registration_Country"></label>
                                        <?php echo do_shortcode('[registrationCountryCode class="form-control"]'); ?>
                                      </div>
                                     
                                        <div class="label-field-wrapper text-container phone-container">
                                            <label class="label required" data-translate="Registration_Phone1"></label>
                                            <?php echo do_shortcode('[registrationCountryPhoneCode class="phoneCountryCode"]'); ?>
                                            <?php echo do_shortcode('[registrationAreaPhoneCode class="phoneAreaCode"]'); ?>
                                            <?php echo do_shortcode('[registrationPhone class="phone"]'); ?>
                                        </div>
										
                                        <div class="label-field-wrapper text-container currencyCode-container">
                                            <label class="label required" data-translate="Registration_BillingCurrency"></label>
                                            
                                                <?php echo do_shortcode('[registrationCurrencyCode class="form-control"]'); ?>
                                            
                                        </div>

                                        <div class="label-field-wrapper checkbox-container subscribeToPromoEmail-container">
                                            <label class="checkbox">
                                                <input type="checkbox">
                                                <label data-translate="subscribeToPromoEmail"></label>
                                            </label>
                                        </div>

                                        <div class="label-field-wrapper checkbox-container subscribeToPromoSms-container">
                                            <label class="checkbox">
                                                <input type="checkbox">
                                                <label data-translate="subscribeToPromoSms" class="widgetPlaceholder"></label>
                                            </label>
                                        </div>

                                        <div class="label-field-wrapper checkbox-container tos-container">
                                            <label class="checkbox">
                                                <input data-widget="registration" data-type="tos" data-isrequired="true" type="checkbox" />
                                                <label data-translate="registrationTos" class="widgetPlaceholder"></label>
                                                <div data-widget="registration" data-type="error" style="display: none" class="error-message"></div>
                                            </label>
                                            <br>
                                        </div>
										<div class="label-field-wrapper submit-container">
                                        <label class="label empty"></label>
                                        <?php echo do_shortcode('[registrationSubmit class="btn btn-primary btn-yellow col-xs-12 col-sm-12"]'); ?>
                                        <div class="loading-animation" style="display:none;"></div>
                                    </div>
                                    </div>
                                    
                                </fieldset>
                            </form>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div><!-- .site -->
        </div><!-- .entry-content -->
</div>
		</div>
	</div>



