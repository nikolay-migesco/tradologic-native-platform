<script>
Xclient.render({
    clientId: 'ab56181', // Client ID
    product: 'news', // Product code (cal,news,sumr-d,sumr-w,sumr-m,tecan,tecvid,sgnl)
    lang: 'en',
    assetmode: 'forex', // binary or forex (sgnl only)
    asset: '$csi', //retreive data for specific assets ($csib)
    limit: 100, // data limit (news, sumr-d,sumr-w,sumr-m, tecan, tecvid only)

    getData: function(data) {
        var data = JSON.parse(data).data;
        if (data) {
            var content = '';
            for (var i = 0; i < data.length; i++) {
                var currentItem = data[i];
                content += '<div class="col-sm-12 news-item">' +
								'<div class="col-sm-8">' +
									'<div class="col-sm-12 title">' + currentItem.title + '</div>' +
									'<div class="col-sm-12 details" style="display: none;">' + currentItem.description + '</div>' +
									'<div class="col-sm-12 date">' + currentItem.postedat + '</div>' +
								'</div>' +
								'<div class="col-sm-4 news-image">' +
									'<div class="image"><img class="img-responsive" src="' + currentItem.imageurl + '">' + '</div>' +
								'</div>' +
							'</div>';
            }

            $("#mte-news-container").append(content);
        }
    },

    added: function(message) {
        //console.log("added", message);
    },
    changed: function(message) {
        //console.log("changed", message);
    },
    removed: function(message) {
        //console.log("removed", message);
    }
}, '#mte-news-container');

$(document).on("click", ".news-item", function() {
    var content = '';
    content += '<div class="ui-dialog-titlebar ui-corner-all ui-widget-header ui-helper-clearfix ui-draggable-handle">' +
					'<span class="ui-dialog-title">News</span>' +
						'<button type="button" class="ui-button ui-corner-all ui-widget ui-button-icon-only ui-dialog-titlebar-close" data-dismiss="modal" title="Close"><span class="ui-button-icon ui-icon ui-icon-closethick"></span>' +
						'<span class="ui-button-icon-space"> </span>' +
						'</button>' +
				'</div>' +
				'<div class="news-title">' + $(this).find(".title").text() + '</div>' +
				'<div class=" ui-dialog-content ui-widget-content">' +
					'<div class="feed-row-image"><img src=' + $(this).find("img").attr('src') + '></div>' +
					'<div class="date-container">' + $(this).find(".date").text() + '</div>' +
					'<div class="scrollable-area scrollable-area-at-top news-popup">' +
						'<div class="scrollable-area-body">' +
							'<div class="scrollable-area-content">' +
								'<div class="details-container">' + $(this).find(".details").text() + '</div>' +
							'</div>' +
						'</div>' +
					'</div>' +
				'</div>';

    $(".scrollable-area.news-popup").mCustomScrollbar("destroy");
    $(".scrollable-area.news-popup").mCustomScrollbar({
        scrollButtons: {
            enable: false
        },
        theme: 'minimal-dark',
        horizontalScroll: false,
        autoDraggerLength: true,
        autoHideScrollbar: true,
        advanced: {
            autoScrollOnFocus: false,
            updateOnContentResize: true,
            updateOnBrowserResize: true
        }
    });

    $('#mte-dialog').html(content);
    $('#mte-dialog').modal('show');
});
</script>