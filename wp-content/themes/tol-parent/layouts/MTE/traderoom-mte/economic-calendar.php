<script>
var sortingDay = 'all';
// Economic Calendar
Xclient.render({
    clientId: 'ab56181', // Client ID
    product: 'cal', // Product code (cal,news,sumr-d,sumr-w,sumr-m,tecan,tecvid,sgnl)
    lang: 'en',
    assetmode: 'forex', // binary or forex (sgnl only)
    asset: '$csi', //retreive data for specific assets ($csib)
    limit: 100, // data limit (news, sumr-d,sumr-w,sumr-m, tecan, tecvid only)

    getData: function(data) {
        var data = JSON.parse(data).data.events;
        if (data) {
            var content = '';
            for (var i = 0; i < data.length; i++) {
				var currentItem = data[i],
					allItemsArray = [];
				 // Date and Hours
				 currentItemDate = currentItem.date,
                    getDate = [];
                    getDate.push(currentItemDate);
                    var getDateAndHours = getDate[0].split(' '),
                        getDateSplitDate = getDateAndHours[0].split('-');
                    // Take the hours and minutes
                    var getDateSplitHours = getDateAndHours[1].split(':'),
                        currentItemHour = getDateSplitHours[0],
                        currentItemMinutes = getDateSplitHours[1];
                    // Take the date
                    var currentItemMounth = getDateSplitDate[1],
                        currentItemDay = getDateSplitDate[2],
                    // Date To print
						currentItemDateToPrint= `${currentItemHour}:${currentItemMinutes}`
						
					var currentAssetIdToTriggerClick,
						currentItemCurrency = currentItem.currency.toLowerCase();
					// Help to sort assets by days
					var currentItemDateDay = new Date(currentItem.date),
                        weekdays = new Array(7);
                            weekdays[0] = "Sunday";
                            weekdays[1] = "Monday";
                            weekdays[2] = "Tuesday";
                            weekdays[3] = "Wednesday";
                            weekdays[4] = "Thursday";
                            weekdays[5] = "Friday";
                            weekdays[6] = "Saturday";

                    var weekday_value = currentItemDateDay.getDay(),
                        currentDayAsString = '';
                    
                    
                    switch(weekdays[weekday_value]) {
						case 'Monday':
                            currentDayAsString = 'Monday';
							break;
                        case 'Tuesday':
                            currentDayAsString = 'Tuesday';
                            break;
                        case 'Wednesday':
                            currentDayAsString = 'Wednesday';
                            break;
                        case '':
                            currentDayAsString = '';
                            break;
                        case 'Friday':
                            currentDayAsString = 'Friday';
                            break;
						default:
							break;
					}

					// TLP-6390 => When the user press on trade depending of the country from which the event is coming we will preselect the following pairs: 
					switch(currentItemCurrency) {
						case 'aud':
							currentAssetIdToTriggerClick = 10; 
							break;
							// AUD/USD
						case 'cad':
							currentAssetIdToTriggerClick = 9;
							break;
							// USD/CAD
						case 'cny':
							currentAssetIdToTriggerClick = 174;
							break;
							// USD/CNY
						case 'jpy':
							currentAssetIdToTriggerClick = 21;
							break;
							// USD/JPY
						case 'nzd':
							currentAssetIdToTriggerClick = 12;
							break;
							// NZD/USD
						case 'chf':
							currentAssetIdToTriggerClick = 8;
							break;
							// USD/CHF
						case 'eur':
							currentAssetIdToTriggerClick = 6;
							break;
							// EUR/USD
						case 'gbp':
							currentAssetIdToTriggerClick = 7;
							break;
							// GBP/USD
						case 'usd':
							currentAssetIdToTriggerClick = 6;
							break;
							// EUR/USD
						default:
							break;
					}
					
				content += '<div class="col-sm-12 cal-item">' +
								'<span class="keep_id" style="display: none;">' + currentAssetIdToTriggerClick + '</span>' +
								'<span class="currentDay" style="display: none;">'+ currentDayAsString +'</span>' +
								'<div class="col-sm-12 cal-details">' +
									'<div class="col-sm-2 flag-icon flag-icon-' + currentItem.currency.toLowerCase() + '"></div>' +
									'<div class="col-sm-2 currency">' + currentItem.currency + '</div>' +
									'<div class="col-sm-8 msg">' + currentItem.msg + '</div>' +
									'<button class="btn" data-toggle="collapse" aria-expanded="false" data-target="#notes'+ i + '"></button>' +
								'</div>' +
								'<div class="col-sm-12 msg-details">' +
									'<div class="col-sm-2 date">' + currentItemDateToPrint + ' |  </div>' +
									'<div class="col-sm-10"><span  class="impact' + currentItem.impact + '"></span>' + (currentItem.impact == '1' ? 'Low' : currentItem.impact == '2' ? 'Medium' : 'High') + ' Impact </div>' +
								'</div>' +
								'<div id="notes'+ i + '" class="col-sm-12 collapsed-content collapse ">' + 
									'<div class="col-sm-12 measures"><b>Measures - </b>' + currentItem.measures  + '</div>' +
									'<div class="col-sm-12 effect"><b>Ususal effect - </b>' + currentItem.usualeffect  + '</div>' +
									'<div class="col-sm-12 frequency"><b>Frequency - </b>' + currentItem.frequency  + '</div>' +
									'<div class="col-sm-12 notes"><b>Note - </b>' + currentItem.notes  + '</div>' +
									'<div class="col-sm-12 prices">' + 
										'<div class="col-sm-4 ">' + 
											'<div class="col-sm-12 title">Actual</div>' +
											'<div class="col-sm-12 value">' + currentItem.actual + '</div>' +
										'</div>' +
										'<div class="col-sm-4 ">' + 
											'<div class="col-sm-12 title">Forecast</div>' +
											'<div class="col-sm-12 value">' + currentItem.forcast + '</div>' +
										'</div>' +
										'<div class="col-sm-4 ">' + 
											'<div class="col-sm-12 title">Previous</div>' +
											'<div class="col-sm-12 value">' + currentItem.previous + '</div>' +
										'</div>' +
									'</div>' +
								'</div>' +

								'<div class="col-sm-12 cal-trade-btn">Trade' +
								'</div>' +
								
								
							'</div>';
            }
            $("#mte-calendar-traderoom").append(content);
		}

		var row = $('.page-traderoom .cal-item');
		for (let i = 0; i < row.length; i++) {
			allItemsArray.push($(row[i]));
		}
		
		$(document).on("click", "#sortingByDays .nav-item", function() {
			for (let i = 0; i < row.length; i++) {
				var currentAssetDay = $(row[i]).find('.currentDay').text();
				if($(this).hasClass('monday')) {
					$(row[i]).hide();
					if((currentAssetDay === 'Monday')) {
                        if ($(row[i]).hasClass('hiddenByImpact') || $(row[i]).hasClass('hiddenByFlag')) {
                            break;
                        } else {
                            $(row[i]).show();
                        }
						sortingDay = 'monday';
					}
				} else if($(this).hasClass('tuesday')) {
					$(row[i]).hide();
					if((currentAssetDay === 'Tuesday')) {
						if ($(row[i]).hasClass('hiddenByImpact') || $(row[i]).hasClass('hiddenByFlag')) {
                            break;
                        } else {
                            $(row[i]).show();
                        }
						sortingDay = 'tuesday';
					}
				} else if($(this).hasClass('wednesday')) {
					$(row[i]).hide();
					if((currentAssetDay === 'Wednesday')) {
						if ($(row[i]).hasClass('hiddenByImpact') || $(row[i]).hasClass('hiddenByFlag')) {
                            break;
                        } else {
                            $(row[i]).show();
                        }
						sortingDay = 'wednesday';
					}
				} else if($(this).hasClass('thursday')) {
					$(row[i]).hide();
					if((currentAssetDay === '')) {
						if ($(row[i]).hasClass('hiddenByImpact') || $(row[i]).hasClass('hiddenByFlag')) {
                            break;
                        } else {
                            $(row[i]).show();
                        }
						sortingDay = 'thursday'; 
					}
				} else if($(this).hasClass('friday')) {
					$(row[i]).hide();
					if((currentAssetDay === 'Friday')) {
						if ($(row[i]).hasClass('hiddenByImpact') || $(row[i]).hasClass('hiddenByFlag')) {
                            break;
                        } else {
                            $(row[i]).show();
                        }
						sortingDay = 'friday';
					}
				} else if($(this).hasClass('all')) {
                    if ($(row[i]).hasClass('hiddenByImpact') || $(row[i]).hasClass('hiddenByFlag')) {
                        break;
                    } else {
                        $(row[i]).show();
                    }
				}
			}
		});
    },


    added: function(message) {
        //console.log("added", message);
    },
    changed: function(message) {
        //console.log("changed", message);
    },
    removed: function(message) {
        //console.log("removed", message);
    }
}, '#mte-calendar-traderoom');
// Real Forex filter the assets.
$(window).on('forexOptionsUpdated', function (event) {
	$('.cal-item .cal-trade-btn').bind('click', function (e) {
		var assetIdToTriggerClick = $(this).parent().find('.keep_id').text();
		var assetBoxToClick = $('.assetsview-boxes').find('.forex-asset-boxesrow[data-taid="'+ assetIdToTriggerClick + '"]');
		if (assetBoxToClick.hasClass('market-closed')) {
			$(assetBoxToClick).find('div[data-type="buyBox"]').trigger('click');
			$(assetBoxToClick).find('.market-closed-btn').trigger('click');
		} else {
			assetBoxToClick.find('div[data-type="buyBox"]').trigger('click');
		}
    });
});
// Real Forex filter the assets.
$(window).on('easyForexOptionsUpdated', function (event) { 
	$('.cal-item .cal-trade-btn').bind('click', function (e) {
		var assetIdToTriggerClick = $(this).parent().find('.keep_id').text();
		$('.tradewrapper').find(' .flip-container[data-taid="'+ assetIdToTriggerClick + '"] .invest-btn[data-type="invest-btn"]').trigger('click')
    });
});
// Importance Sorting
$('.choose-importance').click(function() {
	var imapactId = $(this).attr('id');
	var row = $('.page-traderoom .cal-item');
	if ($(this).hasClass('marked')) {
		$(row).find('.'+ imapactId +'').parent().parent().parent().hide().addClass('hiddenByImpact')
	} else {
        $(row).find('.'+ imapactId +'').parent().parent().parent().show().removeClass('hiddenByImpact')
    }
     $(this).toggleClass('marked')
});
// Flag Sorting
$('.flag_checkbox').click(function() {
	var flagId = $(this).attr('id');
	var row = $('.page-traderoom .cal-item');
	if ($(this).is(':checked')) {
		for (let i = 0; i < row.length; i++) {
			var currentAssetDay = $(row[i]).find('.currentDay').text();
			if (sortingDay === currentAssetDay.toLowerCase()) {
				$('.page-traderoom .cal-item').find('.'+ flagId +'').parent().parent().show().removeClass('hiddenByFlag')
				$(this).toggleClass('marked');
			} else if (sortingDay === 'all') {
				$('.page-traderoom .cal-item').find('.'+ flagId +'').parent().parent().show().removeClass('hiddenByFlag')
				$(this).toggleClass('marked');
			}  else {
				$(row[i]).hide().addClass('hiddenByFlag')
			}
		}
	} else {
		$('.page-traderoom .cal-item').find('.'+ flagId +'').parent().parent().hide().addClass('hiddenByFlag')
	}
});
</script>
