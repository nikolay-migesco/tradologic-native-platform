<script>
// Daily Video
Xclient.render({
    clientId: 'ab56181', // Client ID
    product: 'tecvid', // Product code (cal,news,sumr-d,sumr-w,sumr-m,tecan,tecvid,sgnl)
    lang: 'en',
    assetmode: 'forex', // binary or forex (sgnl only)
    asset: '$csib', //retreive data for specific assets ($csib)
    limit: 100, // data limit (news, sumr-d,sumr-w,sumr-m, tecan, tecvid only)

    getData: function(data) {
        var data = JSON.parse(data).data;
        if (data) {
            var content = '';
            for (var i = 0; i < data.length; i++) {
                var currentItem = data[i];
                content += '<div class="col-sm-12 tecvid-item">' +
                                '<div class="col-sm-12 tecvid-image">' +
                                    '<div class="play_img"></div>' + 
                                    '<img class="img-responsive" src="https://img.youtube.com/vi/'+currentItem.videoid+'/0.jpg">' + 
                                '</div>' +
                                '<span style="display: none;" class="videoid">' + currentItem.videoid + '</span>' +
                                '<div class="col-sm-12 title-details">' +
                                    '<div class="col-sm-6 title">' + currentItem.title + '</div>' +
                                    '<div class="col-sm-6 date">' + currentItem.postedat + '</div>' +
                                '</div>' +
                                '<div class="col-sm-12 tecvid-trade-btn">View' +
                                '<span class="keep_id" style="display: none;">Keep Asset ID</span>' +
                                '</div>' +
                            '</div>';
            }

            $("#daily-video-container").append(content);
        }
    },
    added: function(message) {
        // console.log("added", message);
    },
    changed: function(message) {
        // console.log("changed", message);
    },
    removed: function(message) {
        // console.log("removed", message);
    }
}, '#daily-video-container');

// Daily Video Popup
var videoId = $(this).find(".videoid").text();
$(document).on("click", ".tecvid-item", function() {
    var content = '';
    content += '<div class="ui-dialog-titlebar ui-corner-all ui-widget-header ui-helper-clearfix ui-draggable-handle">' +
                    '<span class="ui-dialog-title">Daily Videos</span>' +
                        '<button type="button" class="ui-button ui-corner-all ui-widget ui-button-icon-only ui-dialog-titlebar-close" data-dismiss="modal" title="Close"><span class="ui-button-icon ui-icon ui-icon-closethick"></span>' +
                        '<span class="ui-button-icon-space"> </span>' +
                        '</button>' +
                '</div>' +
                '<div class="news-title">' + $(this).find(".title").text() + '</div>' +
                '<div class=" ui-dialog-content ui-widget-content">' +
                    '<div class="feed-row-image">' +
                    // Video Modal Content
                    '<iframe id="youtube" width="100%" height="100%" src="https://www.youtube.com/embed/' + $(this).find(".videoid").text() + '?autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>' +
                    // Video Modal Content
                    '</a>' +
                    '<div class="date-container">' + $(this).find(".date").text() + '</div>' +
                    '<span class="keep_id_inner" style="display: none;">' + $(this).find(".keep_id").text() + '</span>' +
                    '<div class="col-sm-12 tecvid-trade-btn open-trade-btn">Trade</div>' +
                '</div>';


    $('#mte-dialog').html(content);
    $('#mte-dialog').modal('show');
    $('#mte-dialog.ui-dialog').on('hidden.bs.modal', function (e) { 
        event.preventDefault();
        $("#youtube").attr('src', '');
    });
    $('.tecvid-trade-btn.open-trade-btn').bind('click', function (e) {
        var assetIdToTriggerClick = $(this).parent().find('.keep_id_inner').text();
        <?php if (strpos($_SERVER['REQUEST_URI'], '/traderoom/?product=simplex') !== false) { ?>
            $('.tradearea').find(' .flip-container[data-taid="'+ assetIdToTriggerClick + '"] .invest-btn[data-type="invest-btn"]').trigger('click')
        <?php } else { ?>
            var assetBoxToClick = $('.assetsview-boxes').find('.forex-asset-boxesrow[data-taid="'+ assetIdToTriggerClick + '"]');
            if (assetBoxToClick.hasClass('market-closed')) {
                $(assetBoxToClick).find('div[data-type="buyBox"]').trigger('click');
                $(assetBoxToClick).find('.market-closed-btn').trigger('click');
            } else {
                assetBoxToClick.find('div[data-type="buyBox"]').trigger('click');
            }
        <?php } ?>
        $('#mte-dialog').modal('hide');
    });
});

// MTE MEdia - check available assets real forex
$(window).on('forexOptionsUpdated', function (event) {
    var currentAssetId;
    $('.tecvid-item .title').each(function () {
        var currentSignalAsset = $(this);
        var currentSignalAssetName = $(this).text();
        for (let i = 0; i < forexHelper.options.length; i++) {
            currentAssetId = forexHelper.options[i].id;
            if(currentSignalAssetName.toLowerCase() === forexHelper.options[i].name.toLowerCase()) {
                currentSignalAsset.addClass('matched');
                currentSignalAsset.parent().parent().find('.keep_id').text(currentAssetId)
                break;
            } else {
                currentSignalAsset.parent().parent().hide();
            }
        }
        if(currentSignalAsset.hasClass('matched')) {
            currentSignalAsset.parent().parent().show();
        }
    });
});
// Easy Forex filter the assets.
$(window).on('easyForexOptionsUpdated', function (event) { 
    $('.tecvid-item .title').each(function () {
        var currentSignalAsset = $(this);
        var currentSignalAssetName = $(this).text();
        for (let i = 0; i < easyForexHelper.options.length; i++) {
            currentAssetId = easyForexHelper.options[i].id;
            if(currentSignalAssetName.toLowerCase() === easyForexHelper.options[i].name.toLowerCase()) {
                currentSignalAsset.addClass('matched');
                currentSignalAsset.parent().parent().find('.keep_id').text(currentAssetId)
                break;
            } else {
                currentSignalAsset.parent().parent().hide();
            }
        }
        if(currentSignalAsset.hasClass('matched')) {
            currentSignalAsset.parent().parent().show();
        }
    });
});
</script>