<script>
// Market Analysis
Xclient.render({
    clientId: 'ab56181', // Client ID
    product: 'tecan', // Product code (cal,news,sumr-d,sumr-w,sumr-m,tecan,tecvid,sgnl)
    lang: 'en',
    assetmode: 'forex', // binary or forex (sgnl only)
    asset: '$csib', //retreive data for specific assets ($csib)
    limit: 100, // data limit (news, sumr-d,sumr-w,sumr-m, tecan, tecvid only)

    getData: function(data) {
        var data = JSON.parse(data).data;
        if (data) {
            var content = '';
            for (var i = 0; i < data.length; i++) {
                var currentItem = data[i];
                content += '<div class="col-sm-12 tecan-item">' +
								'<div class="col-sm-12 tecan-image">' +
									'<img class="img-responsive" src="' + currentItem.imageurl + '">' + 
								'</div>' +
								'<div class="col-sm-12 title-details">' +
									'<div class="col-sm-6 title">' + currentItem.title + '</div>' +
									'<div class="col-sm-6 date">' + currentItem.postedat + '</div>' +
								'</div>' +
								'<div class="col-sm-12 tecan-trade-btn">View' +
                                '</div>' +
                                '<span class="keep_id" style="display: none;">Keep Asset ID</span>' +
							'</div>';
            }

            $("#market-analysis-container").append(content);
        }
    },

    added: function(message) {
        // console.log("added", message);
    },
    changed: function(message) {
        //console.log("changed", message);
    },
    removed: function(message) {
        //console.log("removed", message);
    }
}, '#market-analysis-container');

$(document).on("click", ".tecan-item", function() {
    var content = '';
    content += '<div class="ui-dialog-titlebar ui-corner-all ui-widget-header ui-helper-clearfix ui-draggable-handle">' +
					'<span class="ui-dialog-title">Chart Analysis</span>' +
						'<button type="button" class="ui-button ui-corner-all ui-widget ui-button-icon-only ui-dialog-titlebar-close" data-dismiss="modal" title="Close"><span class="ui-button-icon ui-icon ui-icon-closethick"></span>' +
						'<span class="ui-button-icon-space"> </span>' +
						'</button>' +
				'</div>' +
				'<div class="news-title">' + $(this).find(".title").text() + '</div>' +
				'<div class=" ui-dialog-content ui-widget-content">' +
					'<div class="feed-row-image"><img src=' + $(this).find("img").attr('src') + '></div>' +
					'<div class="date-container">' + $(this).find(".date").text() + '</div>' +
                    '<div class="col-sm-12 tecan-trade-btn-inner">Trade</div>' +
                    '<span class="keep_id_inner" style="display: none;">' + $(this).find(".keep_id").text() + '</span>' +
				'</div>';


    $('#mte-dialog').html(content);
    $('#mte-dialog').modal('show');
    $('.tecan-trade-btn-inner').bind('click', function (e) {
        var assetIdToTriggerClick = $(this).parent().find('.keep_id_inner').text();
        $('.assetsview-boxes').find('.forex-asset-boxesrow[data-taid="'+ assetIdToTriggerClick + '"] div[data-type="buyBox"]').trigger('click');
        $('#mte-dialog').modal('hide');
    });
    $('.tecan-trade-btn-inner').bind('click', function (e) {
        var assetIdToTriggerClick = $(this).parent().find('.keep_id_inner').text();
        <?php if (strpos($_SERVER['REQUEST_URI'], '/traderoom/?product=simplex') !== false) { ?>
            $('.tradearea').find(' .flip-container[data-taid="'+ assetIdToTriggerClick + '"] .invest-btn[data-type="invest-btn"]').trigger('click')
        <?php } else { ?>
            var assetBoxToClick = $('.assetsview-boxes').find('.forex-asset-boxesrow[data-taid="'+ assetIdToTriggerClick + '"]');
            if (assetBoxToClick.hasClass('market-closed')) {
                $(assetBoxToClick).find('div[data-type="buyBox"]').trigger('click');
                $(assetBoxToClick).find('.market-closed-btn').trigger('click');
            } else {
                assetBoxToClick.find('div[data-type="buyBox"]').trigger('click');
            }
            
        <?php } ?>
        $('#mte-dialog').modal('hide');
    });
});
// MTE MEdia - check available assets
$(window).on('forexOptionsUpdated', function (event) {
    var currentAssetId;
    $('.tecan-item .title').each(function () {
        var currentSignalAsset = $(this);
        var currentSignalAssetName = $(this).text();
        for (let i = 0; i < forexHelper.options.length; i++) {
            currentAssetId = forexHelper.options[i].id;
            if(currentSignalAssetName.toLowerCase() === forexHelper.options[i].name.toLowerCase()) {
                currentSignalAsset.addClass('matched');
                currentSignalAsset.parent().parent().find('.keep_id').text(currentAssetId)
                break;
            } else {
                currentSignalAsset.parent().parent().hide();
            }
        }
        if(currentSignalAsset.hasClass('matched')) {
            currentSignalAsset.parent().parent().show();
        }
    });
});
// Easy Forex filter the assets.
$(window).on('easyForexOptionsUpdated', function (event) { 
    $('.tecan-item .title').each(function () {
        var currentSignalAsset = $(this);
        var currentSignalAssetName = $(this).text();
        for (let i = 0; i < easyForexHelper.options.length; i++) {
            currentAssetId = easyForexHelper.options[i].id;
            if(currentSignalAssetName.toLowerCase() === easyForexHelper.options[i].name.toLowerCase()) {
                currentSignalAsset.addClass('matched');
                currentSignalAsset.parent().parent().find('.keep_id').text(currentAssetId)
                break;
            } else {
                currentSignalAsset.parent().parent().hide();
            }
        }
        if(currentSignalAsset.hasClass('matched')) {
            currentSignalAsset.parent().parent().show();
        }
    });
});
</script>