<script>
// Signals
Xclient.render({
    clientId: 'ab56181', // Client ID
    product: 'sgnl', // Product code (cal,news,sumr-d,sumr-w,sumr-m,tecan,tecvid,sgnl)
    lang: 'en',
    assetmode: 'forex', // binary or forex (sgnl only)
    asset: '$csib', //retreive data for specific assets ($csib)
    limit: 100, // data limit (news, sumr-d,sumr-w,sumr-m, tecan, tecvid only)

    getData: function(data) {
        var data = JSON.parse(data).data.notifications;

        if (data) {
            var content = '';
            for (var i = 0; i < data.length; i++) {
                var currentItem = data[i];

				
                content += '<div class="col-sm-12 signals-item">' +
								'<div class="col-sm-12 signals-details">' +
									'<div class="col-sm-4 asset">' +  currentItem.asset + '</div>' +
									'<div class="col-sm-4 rate"><span  class="direction ' + currentItem.direction.trim().toLowerCase() + '"></span>' + currentItem.rate + '</div>' +
									'<div class="col-sm-4 prices">' + 
										'<div class="col-sm-12 ">' + 
											'<div class="col-sm-12 tp">TP ' + currentItem.takeprofit + '</div>' +
										'</div>' +
										'<div class="col-sm-12 ">' + 
											'<div class="col-sm-12 sl">SL ' + currentItem.stoploss + '</div>' +
										'</div>' +
									'</div>' +
								'</div>' +
								'<div class="col-sm-12 signals-trade-btn">Trade' +
								'</div>' +
								'<span class="keep_id" style="display: none;">Keep Asset ID</span>' +
							'</div>';
            }

            $("#signals-container").append(content);
        }
    },

    added: function(message) {
        // console.log("added", message);
    },
    changed: function(message) {
        //console.log("changed", message);
    },
    removed: function(message) {
        //console.log("removed", message);
    }
}, '#signals-container');
// MTE MEdia - check available assets real forex
$(window).on('forexOptionsUpdated', function (event) {
    var currentAssetId;
    $('.signals-item .asset').each(function () {
        var currentSignalAsset = $(this);
        var currentSignalAssetName = $(this).text();
        for (let i = 0; i < forexHelper.options.length; i++) {
            currentAssetId = forexHelper.options[i].id;
            if(currentSignalAssetName.toLowerCase() === forexHelper.options[i].name.toLowerCase()) {
                currentSignalAsset.addClass('matched');
                currentSignalAsset.parent().parent().find('.keep_id').text(currentAssetId)
                break;
            } else {
                currentSignalAsset.parent().parent().hide();
            }
        }
        if(currentSignalAsset.hasClass('matched')) {
            currentSignalAsset.parent().parent().show();
        }
    });
    $('.signals-item .signals-trade-btn').bind('click', function (e) {
        var assetIdToTriggerClick = $(this).parent().find('.keep_id').text();
        var assetBoxToClick = $('.assetsview-boxes').find('.forex-asset-boxesrow[data-taid="'+ assetIdToTriggerClick + '"]');
        if (assetBoxToClick.hasClass('market-closed')) {
            $(assetBoxToClick).find('div[data-type="buyBox"]').trigger('click');
            $(assetBoxToClick).find('.market-closed-btn').trigger('click');
        } else {
            assetBoxToClick.find('div[data-type="buyBox"]').trigger('click');
        }
    });
});
// Easy Forex filter the assets.
$(window).on('easyForexOptionsUpdated', function (event) { 
    $('.signals-item .asset').each(function () {
        var currentSignalAsset = $(this);
        var currentSignalAssetName = $(this).text();
        for (let i = 0; i < easyForexHelper.options.length; i++) {
            currentAssetId = easyForexHelper.options[i].id;
            if(currentSignalAssetName.toLowerCase() === easyForexHelper.options[i].name.toLowerCase()) {
                currentSignalAsset.addClass('matched');
                currentSignalAsset.parent().parent().find('.keep_id').text(currentAssetId)
                break;
            } else {
                currentSignalAsset.parent().parent().hide();
            }
        }
        if(currentSignalAsset.hasClass('matched')) {
            currentSignalAsset.parent().parent().show();
        }
    });
    $('.signals-item .signals-trade-btn').bind('click', function (e) {
        var assetIdToTriggerClick = $(this).parent().find('.keep_id').text();
        $('.tradearea').find(' .flip-container[data-taid="'+ assetIdToTriggerClick + '"] .invest-btn[data-type="invest-btn"]').trigger('click')
    });
});
</script>