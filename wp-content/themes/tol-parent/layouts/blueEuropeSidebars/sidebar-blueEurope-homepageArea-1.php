<?php if (is_active_sidebar('sidebar-blueEurope-homepageArea-1')) : // widgets  ?>

	<div class="sidebar-homepage-2-widgets">
		<?php dynamic_sidebar('sidebar-blueEurope-homepageArea-1'); ?>
	</div><!-- sidebar-blueEurope-homepageArea-1-widgets -->

<?php else: // no widgets ?>

	<div class="sidebar-blueEurope-homepageArea-1-widgets">
         <h1 data-translate="blueEuropeTextOneHomepageArea1"></h1>
         <div class="blueEuropeRandomText" data-translate="blueEuropeRandomText"></div>
         <div class="tol-blueEuropeLearnMore">
            <a class="tol-blueEuropeLearnMoreLink" href="./about-us" alt="More" data-translate="LearnMore"></a>
         </div>
	</div><!-- sidebar-blueEurope-homepageArea-1-widgets -->

<?php endif; ?>