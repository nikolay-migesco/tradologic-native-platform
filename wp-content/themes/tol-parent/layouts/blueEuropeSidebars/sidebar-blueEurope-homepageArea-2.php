<?php if (is_active_sidebar('sidebar-blueEurope-homepageArea-2')) : // widgets  ?>

	<div class="sidebar-homepage-2-widgets">
		<?php dynamic_sidebar('sidebar-blueEurope-homepageArea-2'); ?>
	</div><!-- sidebar-blueEurope-homepageArea-2-widgets -->

<?php else: // no widgets ?>

	<div class="sidebar-blueEurope-homepageArea-1-widgets">
         <h1 data-translate="blueEuropeTextOneHomepageArea2"></h1>
         <div class="blueEuropeRandomText" data-translate="blueEuropeRandomText"></div>
         <div class="tol-blueEuropeLearnMore">
            <a class="tol-blueEuropeLearnMoreLink" href="./contact-us" alt="More" data-translate="LearnMore"></a>
         </div>
	</div><!-- sidebar-blueEurope-homepageArea-2-widgets -->

<?php endif; ?>