<!DOCTYPE html <?php language_attributes(); ?>>
<!--[if lt IE 7 ]>
<html class="ie ie6 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>
<html class="ie ie7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>
<html class="ie ie9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js lang-<?php echo ICL_LANGUAGE_CODE; ?>">
<!--<![endif]-->
<head>
    <?php require_once 'header.php'; ?>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <title><?php wp_title(' | ', true, 'right'); ?><?php bloginfo('name'); ?></title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="/wp-content/themes/tol-child/images/favicon.ico">
    <script src="/wp-content/themes/tol-parent/scripts/modernizr.custom.js"></script>
    <script src="/wp-content/themes/tol-parent/scripts/classie.js"></script>
    <script src="/wp-content/themes/tol-parent/scripts/mlpushmenu.js"></script>
    <!-- <link rel="stylesheet" type="text/css" href="/wp-content/themes/tol-parent/styles/normalize.css" /> -->
    <!-- <link rel="stylesheet" type="text/css" href="css/demo.css" /> -->
    <!-- <link rel="stylesheet" type="text/css" href="css/icons.css" /> -->
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/tol-parent/styles/component.css" />
</head>

<body id="top" <?php body_class(); ?>>
    <!-- Push Wrapper -->
    <div class="menu-shadow"></div>
    <div class="mp-pusher" id="mp-pusher">
        <!-- mp-menu -->
        <nav id="mp-menu" class="mp-menu">
            <div class="mp-level">
            <span class="close-menu-btn float-right"></span>
            <ul>
                <!-- not logged buttons -->
                <li class="icon icon-arrow-left headers notlogged">
                    <a class="icon icon-display bold" id="open-account-header" href="../registration"><span class="inner-icon"> <img src="/wp-content/themes/tol-child/images/new-template-99/account.png" alt="" class="img-reponsive "></span><span data-translate="open_account"></span></a>
                </li>
                <li class="icon icon-arrow-left headers notlogged">
                    <a class="icon icon-display bold" id="log-in-header" href="../login"><span class="inner-icon"> <img src="/wp-content/themes/tol-child/images/new-template-99/login.png" alt="" class="img-reponsive"></span><span data-translate="Login_LogIn"></span></a>
                </li>
                <!-- logged in buttons -->
                <li class="icon icon-arrow-left headers logged-in">
                    <a class="icon icon-display bold"  href="../traderoom"><span class="inner-icon"> <img src="/wp-content/themes/tol-child/images/new-template-99/trade_now.png" alt="" class="img-reponsive "></span><span data-translate="tradeNow"></span></a>
                </li>
                <li class="icon icon-arrow-left headers logged-in">
                    <a class="icon icon-display bold" id="my-profile" href="../my-profile"><span class="inner-icon"> <img src="/wp-content/themes/tol-child/images/new-template-99/account.png" alt="" class="img-reponsive"></span><span data-translate="profile"></span></a>
                    <div class="mp-level inner">
                        <ul>
                            <li class="icon icon-arrow-left mp-back-li">
                                <a class="icon icon-phone bold mp-back" href="#"><span class="float-left backIcon"> <img src="/wp-content/themes/tol-child/images/new-template-99/back.png" alt="" class="img-reponsive"></span><span data-translate="menu"></span></a>
                            </li>
                            <li class="icon icon-arrow-left header-container">
                                <h6 class="bold tab-header profile-header" data-translate="profile"></h6>
                            </li>
                            <li class="icon icon-arrow-left">
                                <a class="icon icon-phone" href="../my-profile" data-translate="UserDataProfile"></a>
                            </li>
                            <li class="icon icon-arrow-left">
                                <a class="icon icon-display" href="../cashier" data-translate="Deposit"></a>
                            </li>
                            <li class="icon icon-arrow-left">
                            <a class="icon icon-display" href="../cashier" data-translate="withdrawPageTitle"></a>
                            </li>
                            <li class="icon icon-arrow-left">
                            <a class="icon icon-display" href="../my-trades" data-translate="userDataMyTrades"></a>
                            </li>
                            <li class="icon icon-arrow-left">
                            <a class="icon icon-display" href="../assets" data-translate="asset-settings"></a>
                            </li>
                            </li>
                            <li class="icon icon-arrow-left sign-out">
                                <a class="icon icon-display bold"  href="#" id="logout-btn"><span class="inner-icon"> <img src="/wp-content/themes/tol-child/images/new-template-99/sign_out.png" alt="" class="img-reponsive "></span><span data-translate="userDataLogout"></span></a>
                            </li>
                        </ul>
                    </div>
                </li>
                <!-- language-switcher for mobile devices  -->
                <li class="icon icon-arrow-left headers language-switcher-container">
                    <div class="language-switcher-mobile">
                        <?php do_action('wpml_add_language_selector'); ?>
                    </div>               
                 </li>
                <li class="icon icon-arrow-left inner-menu">
                    <a class="icon icon-display" href="#"><span data-translate="products"></span><span class="float-right"> <img src="/wp-content/themes/tol-child/images/new-template-99/more.png" alt="" class="img-reponsive"></span></a> 
                    <div class="mp-level inner">
                        <ul>
                            <li class="icon icon-arrow-left mp-back-li">
                                <a class="icon icon-phone bold mp-back" href="#"><span class="float-left backIcon"> <img src="/wp-content/themes/tol-child/images/new-template-99/back.png" alt="" class="img-reponsive"></span><span data-translate="menu"></span></a>
                            </li>
                            <li class="icon icon-arrow-left header-container">
                                <h6 class="bold tab-header" data-translate="products"></h6>
                            </li>
                            <li class="icon icon-arrow-left">
                                <a class="icon icon-phone" href="#" data-translate="binary-options"></a>
                            </li>
                            <li class="icon icon-arrow-left">
                                <a class="icon icon-display" href="#" data-translate="binary-exchange"></a>
                            </li>
                            <li class="icon icon-arrow-left">
                            <a class="icon icon-display" href="#" data-translate="simplex-or-simplexcrypto"></a>
                            </li>
                            <li class="icon icon-arrow-left">
                            <a class="icon icon-display" href="#" data-translate="forex-or-forexcrypto"></a>
                            </li>
                            <li class="icon icon-arrow-left">
                            <a class="icon icon-display" href="#" data-translate="mt5"></a>
                            </li>
                            <li class="icon icon-arrow-left">
                            <a class="icon icon-display" href="#" data-translate="portfolio-management"></a>
                            </li>
                            <li class="icon icon-arrow-left">
                            <a class="icon icon-display" href="#" data-translate="crypto-invest"></a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="icon icon-arrow-left inner-menu">
                    <a class="icon icon-display" href="#"><span data-translate="conditions"></span><span class="float-right"> <img src="/wp-content/themes/tol-child/images/new-template-99/more.png" alt="" class="img-reponsive"></span></a> 
                <div class="mp-level inner">
                    <ul>
                        <li class="icon icon-arrow-left mp-back-li">
                            <a class="icon icon-phone bold goBack mp-back" href="#"><span class="float-left backIcon"> <img src="/wp-content/themes/tol-child/images/new-template-99/back.png" alt="" class="img-reponsive"></span><span data-translate="menu"></span></a>
                        </li>
                        <li class="icon icon-arrow-left header-container">
                            <h6 class="bold tab-header"  data-translate="conditions"></h6>
                        </li>
                        <li class="icon icon-arrow-left">
                            <a class="icon icon-phone" href="#" data-translate="binary-options"></a>
                        </li>
                        <li class="icon icon-arrow-left">
                            <a class="icon icon-display" href="#" data-translate="binary-exchange"></a>
                        </li>
                        <li class="icon icon-arrow-left">
                        <a class="icon icon-display" href="#" data-translate="simplex-or-simplexcrypto"></a>
                        </li>
                        <li class="icon icon-arrow-left">
                        <a class="icon icon-display" href="#" data-translate="forex-or-forexcrypto"></a>
                        </li>
                        <li class="icon icon-arrow-left">
                        <a class="icon icon-display" href="#" data-translate="mt5"></a>
                        </li>
                        <li class="icon icon-arrow-left">
                        <a class="icon icon-display" href="#" data-translate="portfolio-management"></a>
                        </li>
                        <li class="icon icon-arrow-left">
                        <a class="icon icon-display" href="#" data-translate="crypto-invest"></a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="icon icon-arrow-left">
                <a class="icon icon-display" href="../education" data-translate="education"></a>
            </li>
            <li class="icon icon-arrow-left">
                <a class="icon icon-display" href="../faq" data-translate="faq"></a>
            </li>
            <li class="icon icon-arrow-left">
                <a class="icon icon-display" href="../contact-us" data-translate="contact-us"></a>
            </li>	
            <li class="icon icon-arrow-left">
                <a class="icon icon-display legal-link" href="#" data-translate="homeAboutUs"></a>
            </li>	
            <li class="icon icon-arrow-left inner-menu">
            <a class="icon icon-display" href="#"><span data-translate="legal"></span><span class="float-right align-self-end"> <img src="/wp-content/themes/tol-child/images/new-template-99/more.png" alt="" class="img-reponsive align-self-end"></span></a> 
            <div class="mp-level inner">
                <ul>
                    <li class="icon icon-arrow-left mp-back-li">
                        <a class="icon icon-phone bold goBack mp-back " href="#"><span class="float-left backIcon"> <img src="/wp-content/themes/tol-child/images/new-template-99/back.png" alt="" class="img-reponsive"></span><span data-translate="menu"></span></a>
                    </li>
                    <li class="icon icon-arrow-left header-container">
                        <h6 class="bold tab-header" data-translate="legal"></h6>
                    </li>
                    <li class="icon icon-arrow-left">
                        <a class="icon icon-phone legal-link" data-target="legal-docs" href="#" data-translate="legal-documents"></a>
                    </li>
                    <li class="icon icon-arrow-left">
                        <a class="icon icon-phone legal-link" href="#" data-target="terms-and-conditions" data-translate="termsConditionsLong"></a>
                    </li>
                    <li class="icon icon-arrow-left">
                        <a class="icon icon-display legal-link" href="#" data-target="privacy-policy" data-translate="ShortPolicy"></a>
                    </li>
                    <li class="icon icon-arrow-left">
                    <a class="icon icon-display legal-link" href="#" data-target="risk-disclosure" data-translate="ShortRisk"></a>
                    </li>
                    </li>
                </ul>
            </div>
        </li>
		</ul>
	</div>
</nav>
        <!-- /mp-menu -->
    <div class="container-fluid navbar-container">
        <nav id="tol-header-nav" class="navbar-header navbar fixed-top" role="navigation">
            <button href="/" id="trigger" class="menu-trigger navbar-toggler hamburger" data-toggle="collapse" data-target="collapseElements"><span data-translate="menu-hamburger"></span></button>
            <div>
                <a href="../" class="navbar-brand"><img src="/wp-content/themes/tol-child/images/new-template-99/logo.png" alt="logo" class="img-responsive "></a>
            </div>
            <div class="language-switcher">
                <?php do_action('wpml_add_language_selector'); ?>
            </div>
        </nav>
    </div>
</div>

<script stylesheet="text/javascript">
new mlPushMenu(document.getElementById('mp-menu'), document.getElementById('trigger'));
</script>
