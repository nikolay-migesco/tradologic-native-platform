<!-- Footer -->
<footer id="tol-footer">
    <div class="statusbar cf _focusable" id="statusbar" tabindex="-1">
        <div class="fleft">
            <div class="statusbar-item account-type demo-user" data-translate="PracticeAccount"></div>
            <div class="statusbar-item account-type real-user" data-translate="RealAccount"></div>
            <div class="statusbar-item account-type tolUserIsNotLogged" data-translate="GuestAccount"></div>
            <div class="statusbar-item current-time">
                <span data-widget="currentDateTime" data-type="time" class="currentTimeWidget value"></span>
                <span class="utctime label">(<span data-widget="currentDateTime" data-type="timeUTC" class="currentTimeUtcWidget"></span>&nbsp;UTC)</span>
            </div>
        </div>
        <div class="equity-container">
            <div class="equity-content tolUserIsLogged">
                <div class="equity-item balance-item" id="equity-ppl">
                    <span class="label" data-translate="userDataBalance"></span>
                    <span data-widget="balancev2" data-type="balance" class="value blue"></span>
                </div>
                <?php
                $isCurrProductExists = array_key_exists('curr_product',$GLOBALS);
                $isCurrProductEqualForex = false;
                if($isCurrProductExists){
                    $isCurrProductEqualForex = $GLOBALS['curr_product'] == 'forex';
                }
                if($isCurrProductEqualForex){
                    ?>
                    <div class="equity-item profit-item" id="equity-ppl">
                        <span class="label" data-translate="profit"></span>
                        <span data-widget="forex-forex" data-type="profit" class="value"></span>
                    </div>
                    <div class="equity-item equity-item" id="equity-total">
                        <span class="label" data-translate="equity"></span>
                        <span data-widget="forex-forex" data-type="equity" class="value"></span>
                    </div>
                    <div class="equity-item margin-item" id="equity-indicator">
                        <span class="label" data-translate="footer_margin"></span>
                        <span data-widget="forex-forex" data-type="forexMargin" class="value"></span>
                        <div data-widget="forex-forex" data-type="marginPercentBar" class="margin-indicator"></div>
                        <span data-widget="forex-forex" data-type="marginPercentValue" class="value margin-percent-value blue"></span>
                        <div class="marginLevel-wrapper">
                            <span data-widget="forex-forex" data-type="marginLevelLabel" class="label" data-translate="footer_marginLevel"></span>
                            <span data-widget="forex-forex" data-type="marginLevelValue" class="value blue"></span>
                        </div>
                    </div>
                    <div class="equity-item avbalance-item" id="equity-total">
                        <span class="label" data-translate="availableBalance"></span>
                        <span data-widget="forex-forex" data-type="availableBalance" class="value"></span>
                    </div>
                <?php } ?>
                <span class="footer-settings-icon column-settings">
                    <div class="settings"></div>
                    <div class="contextmenu multiple-selection datatable-contextmenu _focusable leftCenter">
                        <div class="multiple-selection-content">
                            <div class="item item-datatable-contextmenu-name selected" data-column="1">
                                <span data-translate="settings_balance"></span>
                                <div class="list-checkbox"></div>
                            </div>
                            <div class="item item-datatable-contextmenu-name selected" data-column="2">
                                <span data-translate="settings_profit"></span>
                                <div class="list-checkbox"></div>
                            </div>
                            <div class="item item-datatable-contextmenu-quantity selected" data-column="3">
                                <span data-translate="settings_equity"></span>
                                <div class="list-checkbox"></div>
                            </div>
                            <div class="item item-datatable-contextmenu-direction selected" data-column="4">
                                <span data-translate="settings_margin"></span>
                                <div class="list-checkbox"></div>
                            </div>
                            <div class="item item-datatable-contextmenu-averagePrice selected" data-column="5">
                                <span data-translate="settings_avbalance"></span>
                                <div class="list-checkbox"></div>
                            </div>
                            <div class="btn-wrapper navigation">
                                <div class="nav-button defaultSettings" data-translate="defaultSettings"></div>
                            </div>
                        </div>
                    </div>
                </span>
            </div>
        </div>
    </div>
    <div id="tol-footer-layout-1">
        <div id="tol-footer-inside">
			<div class="footer__wrapper container" >
			   <div  class="footer__categories">
				  <div class="footer__column"><img src="/wp-content/themes/tol-parent/images/forex/logo-dark-theme.png" alt="logo"></div>
				  <div class="footer__column_nav">
					</div>
					<div class="footer__column footer__column--login">
						<div class="start-trading btn-container"  >
							<a href="/traderoom" data-translate="startTradeNow"></a>
						</div>
						<div class="footer-login btn-container" data-toggle="modal" data-target="#tol-login-popup">
							<a data-translate="orLoginNow"></a>
						</div>
                        <div class="social-links-wrapper social-media-links">
                            <a href="https://www.youtube.com/channel/UCsjgHJUS1Hkk68SnI3FDZTQ" target="_blank">
                                <img src="/wp-content/themes/tol-child/images/broker/youtube.png" alt="youtube">
                            </a>
                            <a href="https://twitter.com/migescocom" target="_blank">
                                <img src="/wp-content/themes/tol-child/images/broker/twitter.png" alt="twitter">
                            </a>
                            <a href="https://www.facebook.com/migesco" target="_blank">
                                <img src="/wp-content/themes/tol-child/images/broker/facebook.png" alt="facebook">
                            </a>
                        </div>
					</div>
			   </div>
			    <hr  class="footer__hr">
			   <div class="footer__copyright">
				  <div  class="footer__payment-images">
					 <div class="footer-images-wrapper">
						<img src="/wp-content/themes/tol-child/images/broker/cards.png" alt="Cards">
					</div>
				  </div>
				</div>
			</div>
		</div>
    </div>
</footer>
<?php if (ENABLE_DEMO_FLOW) { ?>
    <div id="unverified-account-modal" tabindex="-1" role="dialog" class="ui-dialog ui-corner-all ui-widget ui-widget-content ui-front ui-draggable forex-neworder-dialog forex-change-modes-popup unverified-account-modal" aria-labelledby="tol-change-modes-result" style="display: none;">
        <div class="ui-dialog-titlebar ui-corner-all ui-widget-header ui-helper-clearfix ui-draggable-handle">
            <span id="ui-id-2" class="ui-dialog-title" data-translate="warning"></span>
            <span class="warning-header-text" data-translate="unverifiedAccount"></span>
            <button type="button" class="ui-button ui-corner-all ui-widget ui-button-icon-only ui-dialog-titlebar-close" data-dismiss="modal" title="Close">
                <span class="ui-button-icon ui-icon ui-icon-closethick"></span>
                <span class="ui-button-icon-space"> </span>
                </button>
            </div>
        <div class="forex-new-order ui-dialog-content ui-widget-content ">
            <div class="body-text" data-translate="completeVerification"></div>
            <div class="modes-btn-row">
                <div class="button btn-cancel btn-uppercase" data-dismiss="modal" data-translate="be_cancel"></div>
                <div class="button forex-confirm-btn btn-uppercase tol-userbar-my-profile" data-translate="verify"></div>
            </div>
        </div>
    </div>
<?php } ?>
<!-- LOGIN POP UP -->
<div id="tol-login-popup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="tol-login-label">
    <div class="modal-dialog modal-sm modal-default" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="tol-login-label" data-translate="login-title"></h4>
            </div>
            <?php if (ENABLE_DEMO_FLOW) { ?>
                <div class="account-switch-toggle">
                    <div class="active" data-translate="real"></div>
                    <div data-translate="demo"></div>
                </div>
            <?php } ?>
            <div data-widget="login" data-type="container" class="modal-body login-container">
                <div class="form-group">
                    <?php echo do_shortcode('[loginUsername class="form-control"]'); ?>
                </div>
                <div class="form-group popup-password">
                    <?php echo do_shortcode('[loginPassword class="form-control"]'); ?>
                </div>
                <div class="row form-group popup-password">
                    <div class="checkbox savePassWrapper col-xs-6 col-sm-6 nopadding-right">
                        <label>
                            <input type="checkbox" name="savePass" id="savePass" class="savePass"><span for="savePass" data-translate="Login_RememberPass"></span>
                        </label>
                    </div>
                    <div class="col-xs-6 col-sm-6 nopadding-left text-right">
                        <a href="<?php echo home_url('/'); ?>forgot-password" data-translate="forgotPassword"></a>
                    </div>
                </div>
            </div>
            <div class="login2fa-container" style="display: none;">
                <div class="auth-subtitle"><p>To login, open the Two-Factor Authentication app on your device, view your authentication code and enter it below.</p>If you do not have your device with you, you can use one of the Backup codes, generated automatically during the activation of the feature.</div>
                <div class="float-container">
                    <label for="autoCode">Enter Code</label>
                    <input data-widget="login" data-type="auth-code" type="text" id="autoCode" class="auth-input-code" />
                    <span data-widget="login" data-type="auth-error" class="auth-error"></span>
                </div>
            </div>
            <div class="modal-footer">
                <?php echo do_shortcode('[loginButton class="nav-button button open-real-account"]'); ?>
                <a href="#" data-widget="login" data-type="auth-submit" class="nav-button button open-real-account auth-btn" style="display: none;" data-translate="verify"></a>
                <div data-widget="login" data-type="error" style="display: none" class="widgetPlaceholder error-message"></div>
                <div class="login-success-message success-message" style="display:none;" data-translate="Login_Successful"></div>
            </div>
        </div>
    </div>
</div>
<!-- END LOGIN POP UP -->
<!-- START Change modes popup -->
<div id="tol-change-modes" tabindex="-1" role="dialog" class="ui-dialog ui-corner-all ui-widget ui-widget-content ui-front ui-draggable forex-neworder-dialog forex-change-modes-popup" aria-labelledby="tol-change-modes" style="display: none;">
    <div class="ui-dialog-titlebar ui-corner-all ui-widget-header ui-helper-clearfix ui-draggable-handle">
        <span id="ui-id-2" class="ui-dialog-title" data-translate="Settings"></span>
        <button type="button" class="ui-button ui-corner-all ui-widget ui-button-icon-only ui-dialog-titlebar-close" data-dismiss="modal" title="Close">
            <span class="ui-button-icon ui-icon ui-icon-closethick"></span>
            <span class="ui-button-icon-space"> </span>
            Close
        </button>
    </div>
    <div class="forex-new-order ui-dialog-content ui-widget-content">
        <div class="asset-orderid">
            <div class="forex-asset-name" data-translate="TradingSettings"></div>
        </div>
        <div id="changeCtrls" class="body-text">
            <button type="button" class="settings-row" data-toggle="collapse" data-target="#modes" data-translate="TradingModes"></button>
            <div id="modes" class="collapse">
                <div class="modes-wrapper">
                    <div class="modes-row">
                        <input type="radio" name="tradingmode" id="aggregate" value="aggregate" class="modes-radiobtn" checked />
                        <label for="aggregate" class="modes-row-label" data-translate="AggregatingMode"></label>
                        <div class="check"></div>
                        <div class="modes-info" data-translate="AggregatingModeInfo"></div>
                    </div>
                    <div class="modes-row">
                        <input type="radio" name="tradingmode" id="hedging" value="hedging" class="modes-radiobtn" />
                        <label for="hedging" class="modes-row-label" data-translate="HedgingMode"></label>
                        <div class="check"></div>
                        <div class="modes-info" data-translate="HedgingModeInfo"></div>
                    </div>
                    <div class="modes-btn-row">
                        <div class="button forex-change-mode" data-translate="Accept"></div>
                    </div>
                </div>
            </div>
            <button type="button" class="settings-row" data-toggle="collapse" data-target="#themes" data-translate="Themes"></button>
            <div id="themes" class="collapse">
                <div class="themes-wrapper">
                    <div class="themes-row themes-row-one">
                        <input type="radio" name="thememode" id="darktheme" value="dark" class="themes-radiobtn" checked />
                        <label for="darktheme" class="modes-row-label" data-translate="DarkTheme"></label>
                        <div class="check"></div>
                    </div>
                    <div class="themes-row themes-row-two">
                        <input type="radio" name="thememode" id="whitetheme" value="white" class="themes-radiobtn" />
                        <label for="whitetheme" class="modes-row-label" data-translate="WhiteTheme"></label>
                        <div class="check"></div>
                    </div>
                    <div class="modes-btn-row">
                        <div class="button forex-confirm-btn forex-change-theme" data-translate="Accept"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Change modes popup -->

<!-- START Change modes result popup -->
<div id="tol-change-modes-result" tabindex="-1" role="dialog" class="ui-dialog ui-corner-all ui-widget ui-widget-content ui-front ui-draggable forex-neworder-dialog forex-change-modes-popup" aria-labelledby="tol-change-modes-result" style="display: none;">
    <div class="ui-dialog-titlebar ui-corner-all ui-widget-header ui-helper-clearfix ui-draggable-handle">
        <span id="ui-id-2" class="ui-dialog-title">Highlight</span>
        <button type="button" class="ui-button ui-corner-all ui-widget ui-button-icon-only ui-dialog-titlebar-close" data-dismiss="modal" title="Close">
            <span class="ui-button-icon ui-icon ui-icon-closethick"></span>
            <span class="ui-button-icon-space"> </span>
            Close
        </button>
    </div>
    <div class="forex-new-order ui-dialog-content ui-widget-content">
        <div class="asset-orderid">
            <div class="forex-asset-name"></div>
        </div>
        <div class="body-text"></div>
        <div class="modes-btn-row">
            <div class="button forex-confirm-btn" data-dismiss="modal">OK</div>
        </div>
    </div>
</div>
<!-- END Change modes result popup -->
<!-- PLATFORM INFO POP UP -->
<div id="tol-platform-info-popup" class="modal fade" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-sm modal-default" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button> 
            </div>
            <div data-type="container" class="modal-body platform-info-container">
				Dear customer, we have upgraded our platform to a new version for enhancements and improved service.<br /><br />
				Should you experience any issue while logging in, kindly change your password by following <a href="<?php echo home_url('/'); ?>forgot-password" >this</a> link.
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
<!-- END PLATFORM INFO POP UP -->

</body>
</html>
