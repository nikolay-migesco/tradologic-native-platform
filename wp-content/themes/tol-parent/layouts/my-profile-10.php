<div id="tol-myProfile-layout-10">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div id="form-wrapper" class="container compliance-widget-wrapper">

            <div class="entry-header pageHeadTitle row">
                <div class="col-sm-6">
                    <span class="entry-title "><?php the_title(); ?></span>
                </div>
                <div class="col-sm-6 no-padd-left text-right">
                    <a href="<?php echo home_url('/'); ?>my-profile" class="per-details" data-translate="Personal_Details"></a>
                    <a href="<?php echo home_url('/change-password') ?>" class="change-pass" data-translate="Change_Password"></a>
                </div>
            </div>
            <header class="entry-header">
                <h1 class="entry-title"><?php the_title(); ?></h1>
            </header>
            <ul class="nav nav-tabs">
                <li class="active"><a class="mypTabs" data-translate="myProfilePageTitle" href="<?php echo home_url('/my-profile'); ?>"></a></li>
                <li><a class="mypTabs" data-translate="Change_Password" href="<?php echo home_url('/change-password') ?>"></a></li>
            </ul>
            <div data-widget="compliance" data-type="main">
                <div class="row">
                    <ul class="nav tabs nav-wizard no-ne">
                        <li class="col-sm-4">
                            <span class="span-step-compliance col-sm-2">1</span>
                            <span class="col-sm-1">&nbsp;</span>
                            <a href="javascript:;" class="compliance-tab col-sm-8 active" data-widget="compliance" data-type="menu" data-target="userdetails" data-translate="Registration_PersonalDetails"></a>
                        </li>
                        <li class="col-sm-4 ">
                            <span class="span-step-compliance col-sm-2">2</span>
                            <span class="col-sm-1">&nbsp;</span>
                            <a href="javascript:;" class="col-sm-8 compliance-tab quest" data-widget="compliance" data-type="menu" data-target="questionnaires" data-translate="Registration_Questionaries"></a>
                        </li>
                        <li class="col-sm-4">
                            <span class="span-step-compliance col-sm-2">3</span>
                            <span class="col-sm-1">&nbsp;</span>
                            <a href="javascript:;" class="col-sm-8 compliance-tab" data-widget="compliance" data-type="menu" data-target="documents" data-translate="Registration_Upload"></a>
                        </li>
                    </ul>
                </div>
                <div data-widget="compliance" data-type="section" data-item="userdetails"
                     class="compliance-userdetails compliance-container row">
                    <form class="form col-md-6">
                        <div class="myprofile-successmsg"></div>
                        <?php echo do_shortcode('[userDetailsError]'); ?>
                        <div class="row tol-my-profile-column">
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group col-xs-12 col-sm-12">
                                    <a id="mobilescroll"></a>
                                    <label class="col-xs-12 col-sm-12 col-md-4 control-label text-left no-padd"
                                           data-translate="Registration_Email"></label>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <?php echo do_shortcode('[userDetailsEmail class="form-control"]'); ?>
                                    </div>
                                </div>
                                <?php if (MY_PROFILE_AVATAR) { ?>
                                    <div class="form-group col-xs-12 col-sm-12">
                                        <label class="col-xs-12 col-sm-12 col-md-4 control-label text-left no-padd"
                                               data-translate="Registration_Avatar"></label>
                                        <div type="text" data-widget="userDetails" data-type="avatar"
                                             data-isRequired="false"
                                             class="widgetPlaceholder col-xs-3 col-sm-3 col-md-4"
                                             style="display: none"></div>
                                    </div>

                                <?php } ?>
                                <div class="form-group col-xs-12 col-sm-12">
                                    <label class="col-xs-12 col-sm-12 col-md-4 control-label text-left no-padd"
                                           data-translate="Registration_Title"></label>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <?php echo do_shortcode('[userDetailsETitle class="form-control"]'); ?>
                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-12">
                                    <label class="col-xs-12 col-sm-12 col-md-4 control-label text-left no-padd"
                                           data-translate="Registration_FirstName"></label>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <?php echo do_shortcode('[userDetailsFirstName class="form-control"]'); ?>
                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-12">
                                    <label class="col-xs-12 col-sm-12 col-md-4 control-label text-left no-padd"
                                           data-translate="Registration_LastName"></label>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <?php echo do_shortcode('[userDetailsLastName class="form-control"]'); ?>
                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-12">
                                    <label class="col-xs-12 col-sm-12 col-md-4 control-label text-left no-padd"
                                           data-translate="Registration_Country"></label>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <?php echo do_shortcode('[userDetailsCountryCode class="form-control"]'); ?>
                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-12">
                                    <label class="col-xs-12 col-sm-12 col-md-4 control-label text-left no-padd"
                                           data-translate="Registration_Nationality"></label>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <?php echo do_shortcode('[userDetailsNationality class="form-control"]'); ?>
                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-12">
                                    <label
                                        class="col-xs-12 col-xs-12 col-sm-12 col-md-4 control-label text-left no-padd"
                                        data-translate="Registration_DateOfBirth"></label>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <div class="col-xs-3 col-sm-3 nopadding">
                                            <?php echo do_shortcode('[userDetailsBirthDay class="form-control my-profile-birth-day"]'); ?>
                                        </div>
                                        <div class="col-xs-5 col-sm-5 nopadding">
                                            <?php echo do_shortcode('[userDetailsBirthMonth class="form-control"]'); ?>
                                        </div>
                                        <div class="col-xs-4 col-sm-4 nopadding">
                                            <?php echo do_shortcode('[userDetailsBirthYear class="form-control"]'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-12">
                                    <label class="col-xs-12 col-sm-12 col-md-4 control-label text-left no-padd"
                                           data-translate="Registration_Street"></label>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <?php echo do_shortcode('[userDetailsAddressLine1 class="form-control"]'); ?>
                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-12">
                                    <label class="col-xs-12 col-sm-12 col-md-4 control-label text-left no-padd"
                                           data-translate="Registration_HouseNum"></label>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <?php echo do_shortcode('[userDetailsAddressLine2 class="form-control"]'); ?>
                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-12">
                                    <label class="col-xs-12 col-sm-12 col-md-4 control-label text-left no-padd"
                                           data-translate="Registration_City"></label>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <?php echo do_shortcode('[userDetailsCity class="form-control"]'); ?>
                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-12">
                                    <label class="col-xs-12 col-sm-12 col-md-4 control-label text-left no-padd"
                                           data-translate="Registration_Phone1"></label>
                                    <div class="col-sm-12 col-md-8">
                                        <div class="col-xs-2 col-sm-2 nopadding">
                                            <?php echo do_shortcode('[userDetailsCountryPhoneCode class="form-control"]'); ?>
                                        </div>
                                        <div class="col-xs-4 col-sm-4">
                                            <?php echo do_shortcode('[userDetailsAreaPhoneCode class="form-control"]'); ?>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 nopadding">
                                            <?php echo do_shortcode('[userDetailsPhone class="form-control"]'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-12">
                                    <label class="col-xs-12 col-sm-12 col-md-4 control-label text-left no-padd"
                                           data-translate="Registration_Phone2"></label>
                                    <div class="col-sm-12 col-md-8">
                                        <?php echo do_shortcode('[userDetailsSecondaryPhone class="form-control"]'); ?>
                                    </div>
                                </div>
                                <div class="col-xs-12 ">
                                    <div class="hidden-xs col-sm-4"></div>
                                    <div class="col-xs-8 ">
                                        <button
                                            class="tol-quick-submit btn  orange-button-styles col-xs-12"
                                            data-widget="userDetails" data-type="submit"
                                            data-translate="Button_Submit"></button>
                                    </div>

                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group col-xs-12 col-sm-12">
                                    <label class="col-xs-12 col-sm-12 col-md-4 control-label text-left">&nbsp;</label>
                                    <div class="col-sm-12 col-md-8">
                                        <div class="row social-buttons no-ne">
                                            <div class="btn-group form-group col-xs-12">
                                                <a href="#" class="btn btn-primary btn-connect col-xs-2">
                                                    <svg class="social-login">
                                                        <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                                             xlink:href="#gl"></use>
                                                    </svg>
                                                </a>
                                                <div data-widget="userDetails" data-type="googleLoginBtn"
                                                     class="widgetPlaceholder loginGoogle btn btn-primary col-xs-10"
                                                     style="display:none;"></div>
                                            </div>
                                        </div>
                                        <div class="row no-ne">
                                            <div class="btn-group form-group col-xs-12">
                                                <a href="#" class="btn btn-primary btn-connect col-xs-2">
                                                    <svg class="social-login">
                                                        <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                                             xlink:href="#fb"></use>
                                                    </svg>
                                                </a>
                                                <div data-widget="userDetails" data-type="fbLoginBtn"
                                                     class="widgetPlaceholder linkFacebook btn btn-primary col-xs-10"></div>
                                            </div>
                                        </div>

                                        <div data-widget="userDetails" data-type="fbLoginErrorMessage"
                                             class="fbLoginErrorMessage"></div>
                                        <div data-widget="userDetails" data-type="googleLoginErrorMessage"
                                             class="fbLoginErrorMessage"></div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>

                <div data-widget="compliance" data-type="section" data-item="questionnaires"
                     class="compliance-questionnaire compliance-container row" style="display: none;">
                    <div class="questionnaireguide" data-translate="questionnareGuide"></div>
                    <div class="questionnaireerror" data-type="questionnaireerror" style="display: none"></div>
                    <div data-widget="compliance" data-type="questions" class="compliance-questions"></div>
                </div>
                <div class="compliance-documents compliance-container row" data-widget="compliance" data-type="section"
                     data-item="documents" style="display: none;">
                    <div class="documentsguide" data-translate="missingdocuments"></div>
                    <div id="docsguide-upload" class="documentsguide">Upload Document</div>
                    <div class="documenthint"></div>
                    <div data-widget="compliance" data-type="documentsupload"></div>
                    <table class="table table-striped table-responsive">
                        <thead class="doctable">
                        <tr>
                            <th data-translate="documentName"></th>
                            <th data-translate="documentType"></th>
                            <th data-translate="documentState"></th>
                        </tr>
                        </thead>
                        <tbody class="uploadeddocs" data-widget="compliance" data-item="uploadedDocs"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </article>
</div>
