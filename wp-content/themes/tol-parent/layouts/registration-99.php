<div id="tol-registration-layout-99"> 
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-2 form-container justify-content-center">
                <form name="foo">
                    <div class="form-group header">
                        <h3 class="text-left" data-translate="registration-title"></h3>
                    </div>
                    <?php if (defined('ENABLE_SOCIAL_IN_REGISTRATION') && ENABLE_SOCIAL_IN_REGISTRATION != false) { ?>
                        <div class="form-group social-logins">
                            <div data-widget="loginFacebook" onclick="$('#tol-login-popup').modal('hide');" data-type="fbLoginBtn" class="btn fb-login social" data-translate="loginFb"></div>
                            <div data-widget="loginFacebook" data-type="popUpElements" data-registration-widgets="title,firstName,lastName,birth,email,primaryPhone,languageCode,countryCode,currency,password,tos" class="loginFacebookWidget registrationPopUp" style="display:none"></div>

                            <div data-widget="loginGoogle" data-type="googleLoginBtn" onclick="$('#tol-login-popup').modal('hide');" class="btn google-login social" data-translate="loginGplus"></div>
                            <div data-widget="loginGoogle" data-type="popUpElements" data-registration-widgets="email,password,title,firstName,lastName,languageCode,countryCode,birth,primaryPhone,currency,tos" class="loginGoogleWidget registrationPopUp" style="display:none"></div>
                        </div>
                        <div class="form-group dashes">
                            <strong class="line-thru" data-translate="or"></strong>
                        </div>
                    <?php } ?>
                    <div class="form-group first-name-group input-container">
                        <input type="text"  data-widget="registration" data-type="firstName" data-isRequired="true" data-minLength="3" class="form-control" id="first-name" required/>
                        <label for="first-name" class="lfl" data-translate="Registration_FirstName"></label>
                        </div>
                    <div class="form-group last-name-group input-container">
                        <input type="text"  data-widget="registration" data-type="lastName" data-isRequired="true" data-minLength="3" class="form-control" id="last-name" required/>
                        <label for="last-name" class="lfl" data-translate="Registration_LastName"></label>
                    </div>
                    <div class="form-group email-group input-container">
                        <input type="text" id="email" data-widget="registration" data-type="email" data-isRequired="true" data-minLength="3" class="form-control" required/>
                        <label for="email" class="lfl" data-translate="Registration_Email"></label>    
                    </div>
                    <div class="form-group password-group input-container">
                        <input type="password" id="password" data-widget="registration" data-type="password" data-minLength="6" data-isRequired="true" data-regexpMessage="The password should contain at least one uppercase character" class="form-control" required/>
                        <label for="password" class="lfl" name="password" data-translate="Registration_Password_"></label>
                        <div id="showPassword" class="glyphicon glyphicon-eye-open"></div>
                    </div>
                    <!-- // collapse button // -->
                    <button class="btn btn-primary btn-show-more btn-main" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                        Show more
                    </button>
                    <!-- // collapse container // -->
                    <div class="collapse" id="collapseExample">
                        <div class="form-group country-group input-container">
                            <label for="choose-country" class="lfl" data-translate="Registration_Country"></label>
                            <select data-widget="registration" data-type="countryCode" placeholder="Please Choose Country" data-isRequired="true" class="form-control" id="choose-country"></select>
                        </div>
                        <div class="form-group language-group input-container">
                            <label for="choose-language" class="lfl" data-translate="Registration_Language"></label>
                            <select data-widget="registration" data-type="languageCode" data-isRequired="true" placeholder="Select Your Language" class="form-control" id="choose-language"></select>
                        </div>
                        <div class="form-group input-container">
                            <label for="birth-date" class="lfl" data-translate="Registration_DateOfBirth"></label>
                            <input type="text" class="form-control input-container" id="birth-date" placeholder="DD/MM/YY">
                        </div>
                        <div class="form-group country-code-group input-container">
                            <label for="phone" class="lfl" id="phoneNumber" data-translate="Registration_PhoneNumber"></label>
                            <?php echo do_shortcode('[registrationCountryPhoneCode ]'); ?>
                            <?php echo do_shortcode('[registrationAreaPhoneCode ]'); ?>
                            <?php echo do_shortcode('[registrationPhone class="form-control" ]'); ?>
                        </div>
                    </div> <!-- // collapse container // -->
                    <div class="form-group checkbox savePassWrapper">
                        <input id="c4" type="checkbox"  name="cc" data-widget="registration" data-type="tos"  data-isRequired="true"/>
                        <label for="c4" class="os"><span ></span></label>
                        <div class="tc os" data-translate="registrationTermsAndConditions">
                        </div>
                    </div>
                    <div class="form-group checkbox not-us">
                        <input type="checkbox" id="c5" name="cc" />
                        <label for="c5" class="os"><span></span></label>
                    </div>
                    <div class="pp os" data-translate="not_us">
                    </div>
                    <div data-widget="registration" data-type="error" style="display: none" class="tos-error"></div>
                    <div class="form-group">
                    <div data-widget="registration" data-type="submit" data-translate="register" class="btn btn-success text-center btn-main"></div>
                    </div>
                    <div class="form-group">
                        <label class="os" data-translate="already-have-account"></label><a href="../login" class="log-in os link" id="login" data-translate="Login_LogIn"></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>