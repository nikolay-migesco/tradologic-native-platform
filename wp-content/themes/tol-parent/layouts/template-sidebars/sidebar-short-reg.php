<?php
/**
 * The sidebar containing the main widget area.
 *
 * If no active widgets in sidebar, let's hide it completely.
 *
 * @package XPTheme
 */
?>

<?php if (is_active_sidebar('sidebar-short-reg')) : // widgets    ?>

    <div class="sidebar-homepage-2-widgets">
        <?php dynamic_sidebar('sidebar-short-reg'); ?>
    </div><!-- sidebar-short-reg -->

<?php else: // no widgets ?>

    <div class="loginFormSidebar">
        <div class="col-md-12 quick-form-wrapSidebar">
            <?php if (QUICK_REGISTRATION) { ?>
                <div class="panel panel-default quick-formSidebar">
                    <div class="text-center h2 quickSignUpHeader" data-translate="quick_signup_title"></div>
                    <?php echo do_shortcode('[registrationError]'); ?>
                    <div class="container-fluid loginFormContainer">
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <i class="glyphicon glyphicon-user shortRegIcons"></i>
                                <?php echo do_shortcode('[registrationFirstName placeholder="First Name" class="form-control shortRegForm"]'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <i class="glyphicon glyphicon-user shortRegIcons"></i>
                                <?php echo do_shortcode('[registrationLastName class="form-control shortRegForm"]'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <i class="glyphicon glyphicon-envelope shortRegIcons"></i>
                                <?php echo do_shortcode('[registrationEmail class="form-control shortRegForm"]'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <i class="glyphicon glyphicon-earphone shortRegIcons" aria-hidden="true"></i>
                                <?php echo do_shortcode('[registrationPhone class="form-control shortRegForm"]'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <i class="glyphicon glyphicon-lock shortRegIcons"></i>
                                <?php echo do_shortcode('[registrationPassword class="form-control shortRegForm"]'); ?>
                            </div>
                        </div>
                        <!--                                        <div class="row">
                                                                    <div class="form-group col-sm-12">
                        <?php // echo do_shortcode('[registrationConfirmPassword class="form-control"]'); ?>
                                                                    </div>
                                                                </div>-->
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <?php echo do_shortcode('[registrationTos]'); ?> I have read and agreed to <a
                                    href="./terms-and-conditions" target="_blank"  data-translate="termsConditions">Terms &amp;
                                    Conditions</a> and <a href="./privacy-policy" target="_blank" class="pPolicyL"
                                                      data-translate="privacyPolicy">Privacy policy</a><span
                                                      class="red">*</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12 submit1">
                                <?php echo do_shortcode('[registrationCountryCode isrequired="false" class="countryCodeWrapperHidden" style="display:none;"]') ?>
                                <?php echo do_shortcode('[registrationSubmit class="btn btn-primary cust-submit"]'); ?>
                                <span class="subm-pic"></span>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>




<?php endif; ?>