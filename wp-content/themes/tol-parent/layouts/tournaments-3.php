<?php
wp_enqueue_style('tournaments', get_stylesheet_directory_uri() . '/styles/layouts/tournaments-3.css', array(), NULL, false);
?>

<script type="text/javascript">
    $(window).on('loadingIsCompleted', function () {
        $('.tour-menu-item.active').click();
        $('.tournamentInfo, .introAdditionalText, .tournamentNameSpan').show();
    });
</script>

<div class="container-fluid exchange-layout">
    <div class="row active-tour-header">
        <h1>Ultra Fast Demo Contest</h1>
        <h4>Win $100 in just one hour. No deposit required.</h4>
        <div class="col-xs-12 col-sm-3">
            <div class="step-image-one"></div>
            <div class="step-header">Sign up</div>
            <div class="step-info">It only takes a few minutes. All you need is to personalize your contest profile.</div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="step-image-two"></div>
            <div class="step-header">Wait for the contest round to start</div>
            <div class="step-info">You will be alerted about the Contest start 15 minutes in advance. You will be ushered into the contest automatically.</div>
        </div>
        <div class="col-xs-12 col-sm-3">
            <div class="step-image-three"></div>
            <div class="step-header">Trade to win</div>
            <div class="step-info">Your money stay in your account all the process</div>
        </div>
    </div>
    <div class="row second-article-header">
        <div class="row tournaments-menu">
            <div data-widget="tournaments" data-type="menu" data-target="active" class="tour-menu-item active">Trading Tournaments In Progress</div>
            <div data-widget="tournaments" data-type="menu" data-target="future" class="tour-menu-item">Upcoming Online Trading Tournaments</div>
            <div data-widget="tournaments" data-type="menu" data-target="past" class="tour-menu-item">Past Trading Tournaments</div>
        </div>
        <div data-widget="tournaments" data-type="content" data-target="active" class="tour-content active-tours active">
            <div class="row" data-type="contentRow" data-target="active">
            </div>
            <div class="row active-tour-table">
                <h1 class="leaderboard-title" data-translate="Leaderboard">Leaderboard</h1>
                <div data-widget="tournaments" data-type="leaderboard" class="leaderboard">
                    <div class="row leaderboard-header">
                        <div data-column="rank" class="col-xs-12 col-sm-1 leaderboard-column">Rank</div>
                        <div data-column="avatar" class="col-xs-12 col-sm-1 leaderboard-column">&nbsp</div>
                        <div data-column="nickname" class="col-xs-12 col-sm-4 leaderboard-column">Nickname</div>
                        <div data-column="balance"class="col-xs-8 col-sm-4 leaderboard-column">Tournament balance</div>
                        <div data-column="prize" class="col-xs-4 col-sm-4 leaderboard-column">Prize</div>
                    </div>
                    <div data-type="leaderboardcontainer" class="leaderboard-container"></div>
                </div>
            </div>
        </div>
        <div data-widget="tournaments" data-type="content" data-target="future" class="tour-content future-tours" style="display: none;">
            <h1>Future Tournaments</h1>
            <div class="row" data-type="contentRow" data-target="future">
            </div>
        </div>
        <div data-widget="tournaments" data-type="content" id="pastTournamentsContent" data-target="past" class="tour-content past-tours" style="display: none;">
            <h1>Past Tournaments</h1>
            <div class="row" data-type="contentRow" data-target="past">
            </div>
        </div>
    </div>
</div>
<div class="row collapse" id="tournamentDetails" data-type="tournamentDetails">
    <div data-widget="tournaments" data-type="name" class="tournament-name"></div>
    <a href="#" class="btnback" data-toggle="collapse" data-target="#tournamentDetails">< Back</a>
    <div data-widget="tournaments" data-type="countdown" class="row countdown-wrapper" style=""></div>
    <div class="well info-tournaments-wrapper">
        <div class="tournamentDetailsInfo"></div>
    </div>
    <div data-widget="tournaments" data-type="leaderboard" class="leaderboard">
        <div class="row leaderboard-header">
            <div data-column="rank" class="leaderboard-column header-rank-column">Rank</div>
            <div data-column="nickname" class="col-xs-12 col-sm-4 leaderboard-column header-nickname-column">Nickname</div>
            <div data-column="balance"class="col-xs-8 col-sm-4 leaderboard-column header-balance-column">Tournament balance</div>
            <div data-column="prize" class="col-xs-4 col-sm-4 leaderboard-column header-prize-column">Prize</div>
        </div>
        <div data-type="leaderboardcontainer" class="leaderboard-container"></div>
    </div>
</div>
<div data-widget="tournaments" data-type="paging" data-rowsperpage="10" data-paginglimit="10" class="pagination pagination-sm"></div>
<div class="modal fade portfolioModal" id="rebuy-modal" data-widget="tournaments" data-type="portfolioModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel" data-widget="tournaments" data-type="portfolioModalTitle">Modal title</h4>
            </div>
            <div class="modal-body" data-widget="tournaments" data-type="portfolioModalContent"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary portfolioActionButton" data-widget="tournaments" data-type="portfolioModalButton">Save changes</button>
            </div>
        </div>
    </div>
</div>