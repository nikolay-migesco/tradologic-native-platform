<div id="tol-deposit-newCC-wrapper" class="well well-sm">
    <fieldset class="wrapper">
        <div class="col-sm-6">
            <div class="row">
                <div data-widget="cashierCreditCardDeposit" data-type="error" style="display: none"></div>
                <div class="form-group col-xs-12 col-sm-12">
                    <label class="col-xs-4 col-sm-12 col-md-4 control-label text-left" data-translate="cashierLabelAmount"></label>
                    <div class="col-xs-8 col-sm-12 col-md-8">
                        <label data-widget="cashierCreditCardDeposit" data-type="amount"></label>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-12">
                    <label class="col-sm-12 col-md-4 control-label text-left" data-translate="cashierLabelCardType"></label>
                    <div class="col-sm-12 col-md-8">
                        <select data-widget="cashierCreditCardDeposit" data-type="cardType" style="display:none" class="form-control"></select>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-12">
                    <label class="col-xs-12 col-sm-12 col-md-4 control-label text-left" data-translate="cashierLabelCardNumber"></label>
                    <div class="col-xs-12 col-sm-12 col-md-8">
                        <input type="text" data-widget="cashierCreditCardDeposit" data-type="number" maxlength="20" data-isRequired="true" data-regexpmessage="Invalid credit card number" data-regexp="^5[1-5]\d{2}-?\d{4}-?\d{4}-?\d{4}$" class="form-control" style="display:none" />
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-12">
                    <label class="col-xs-12 col-sm-12 col-md-4 control-label text-left" data-translate="cashierLabelExpirationDate"></label>
                    <div class="col-xs-6 col-sm-6 col-md-4">
                        <select data-widget="cashierCreditCardDeposit" data-type="expirationMonth" style="display:none" class="form-control"></select>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-4">
                        <select data-widget="cashierCreditCardDeposit" data-type="expirationYear" style="display:none" class="form-control"></select>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-12">
                    <label class="col-xs-4 col-sm-12 col-md-4 control-label text-left" data-translate="cashierLabelCVV"></label>
                    <div class="col-xs-12 col-sm-12 col-md-8">
                        <input type="text" data-widget="cashierCreditCardDeposit" data-type="cvv" data-minLength="3" maxlength="4" data-isRequired="true" style="display:none" class="form-control" />
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-12">
                    <label class="col-xs-12 col-sm-12 col-md-4 control-label text-left" data-translate="cashierLabelFirstName"></label>
                    <div class="col-xs-12 col-sm-12 col-md-8">
                        <input type="text" data-widget="cashierCreditCardDeposit" data-type="firstName" data-regexp="[A-Za-z]" maxlength="20" data-isRequired="true" style="display:none" class="form-control" />
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-12">
                    <label class="col-xs-12 col-sm-12 col-md-4 control-label text-left" data-translate="cashierLabelLastName"></label>
                    <div class="col-xs-12 col-sm-12 col-md-8">
                        <input type="text" data-widget="cashierCreditCardDeposit" data-type="lastName" data-regexp="[A-Za-z]" maxlength="20" data-isRequired="true" style="display:none" class="form-control" />
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="row">
            <div class="form-group col-xs-12 col-sm-12">
                <label class="col-xs-12 col-sm-12 col-md-4 control-label text-left" data-translate="cashierLabelAddress1"></label>
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <input type="text" data-widget="cashierCreditCardDeposit" data-type="address1" maxlength="40" data-isRequired="true" style="display:none" class="form-control" />
                </div>
            </div>
            <div class="form-group col-xs-12 col-sm-12">
                <label class="col-xs-12 col-sm-12 col-md-4 control-label text-left" data-translate="cashierLabelAddress2"></label>
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <input type="text" data-widget="cashierCreditCardDeposit" data-type="address2" maxlength="40" style="display:none" class="form-control" />
                </div>
            </div>
            <div class="form-group col-xs-12 col-sm-12">
                <label class="col-xs-12 col-sm-12 col-md-4 control-label text-left" data-translate="cashierLabelCountry"></label>
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <select data-widget="cashierCreditCardDeposit" data-type="country" style="display:none" class="form-control"></select>
                </div>
            </div>
            <div class="form-group col-xs-12 col-sm-12">
                <label class="col-xs-12 col-sm-12 col-md-4 control-label text-left" data-translate="cashierLabelCity"></label>
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <input type="text" data-widget="cashierCreditCardDeposit" data-type="city" maxlength="20" data-isRequired="true" style="display:none" class="form-control" />
                </div>
            </div>
            <div class="form-group col-xs-12 col-sm-12">
                <label class="col-xs-12 col-sm-12 col-md-4 control-label text-left" data-translate="cashierLabelPostalCode"></label>
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <input type="text" data-widget="cashierCreditCardDeposit" data-type="zip" maxlength="15" data-isRequired="true" style="display:none" class="form-control" />
                </div>
            </div>
            <div class="form-group col-xs-12 col-sm-12">
                <label class="col-xs-12 col-sm-12 col-md-4 control-label text-left" style="display: none;" data-translate="cashierLabelProvince"></label>
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <select data-widget="cashierCreditCardDeposit" data-type="province" style="display:none" class="form-control cashierProvinceField"></select>
                </div>
            </div>
            <div class="clearfix hidden-xs">&nbsp;</div>
            <div class="col-sm-6 hidden-xs">&nbsp;</div>
            </div>
        </div>
        <div class="form-group ccDepositButtons col-sm-12 text-right">
            <input action="action" type="button" value="Back" onclick="history.go(-1);" class="btn btn-primary" />
            <input type="button" data-widget="cashierCreditCardDeposit" data-type="submit" value="Deposit" style="display:none" class="ccdepositBTN btn btn-primary" />
        </div>
    </fieldset>
</div>