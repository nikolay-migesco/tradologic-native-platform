<div id="tol-leaderBoard-layout-1">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="container-fluid social-wrapper">
            <div class="col-sm-10 col-sm-offset-1 no-padding-all">
                <div class="pageHeadTitle">
                    <?php require_once(ABSPATH.'wp-content/themes/tol-parent/layouts/'.BREADCRUMB.'.php' ); ?>
                </div>
                <div class="well clearfix">
                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-5">
                            <div class="row">
                                <label data-translate="socialTrading_AssetType" class="col-sm-4 nopadding-right tol-labels"></label>
                                <div class="col-sm-8">
                                    <?php echo do_shortcode('[socialLeaderboardAssetFilter class="form-control form-control-md"]'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-xs-12 col-sm-5">
                            <div class="row">
                                <label data-translate="socialTrading_Period" class="col-sm-4 nopadding-right tol-labels"></label>
                                <div class="col-sm-8">
                                    <?php echo do_shortcode('[socialLeaderboardPeriodFilter class="form-control form-control-md"]'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-xs-12 col-sm-2">
                            <?php echo do_shortcode('[socialLeaderboardSearch class="btn btn-primary col-xs-12"]'); ?>
                        </div>
                    </div>
                </div>

                <?php echo do_shortcode('[socialLeaderboardTable class="table table-striped"]'); ?>
            </div>
        </div>
        <div class="entry-content">
            <?php echo $post->post_content; ?>

        </div>
    </article>
</div>