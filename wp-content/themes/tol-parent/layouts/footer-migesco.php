<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package XPTheme
 */

if (!is_front_page()) {
    echo <<<HTML
 </div>
HTML;
}
?>
<?php if (strpos($_SERVER['REQUEST_URI'], '/cashier') !== false) {
    echo '</div>';
}
if (!(basename(get_permalink()) == 'traderoom')) {

    /* close tags from wp-content/themes/tol-parent/layouts/header-migesco.php */
    echo '</td></tr></tbody></table></div>';

    if (prior_lang() == 'ru'): //RUSSIAN CONTACT
        ?>
        <div id="footer_m">
            <div class="padding">
                <table width="100%" cellpadding="0" cellspacing="0" border="0" id="menu_box">
                    <tr>
                        <td valign="top" width="252">
                            <h3 class="menu_title">С чего начать</h3>
                            <div class="menu_item op-users-only"><a href="/deposit-account">Пополнить счет</a></div>
                            <div class="menu_item op-guests-only"><a href="#register">Открыть счет</a></div>
                            <div class="menu_item"><a href="/sposoby-popolneniya-i-snyatiya#deposit">Как пополнить
                                    счет</a></div>
                            <div class="menu_item"><a href="/kak-nachat-torgovat">Как начать торговать</a></div>
                            <div class="menu_item"><a href="/kak-nauchitsya-torgovat-binarnymi-opcionami">Как научиться
                                    торговать</a></div>
                            <div class="menu_item"><a href="/kak-prognozirovat">Как прогнозировать рынок</a></div>
                            <div class="menu_item"><a href="/sposoby-popolneniya-i-snyatiya#withdraw">Как снять
                                    прибыль</a></div>
                        </td>
                        <td valign="top" width="253">
                            <h3 class="menu_title">Нужна помощь</h3>
                            <div class="menu_item op-guests-only"><a href="#login">Войти в личный кабинет</a></div>
                            <div class="menu_item op-users-only"><a href="/deposit-account">Войти в личный кабинет</a>
                            </div>
                            <div class="menu_item"><a href="/traderoom">Войти в торговый терминал</a></div>
                            <div class="menu_item"><a href="/usloviya-torgovli">Условия торговли</a></div>
                            <div class="menu_item"><a href="/aktivy-dlya-torgovli">Активы для торговли</a></div>
                            <div class="menu_item"><a href="/poshagovoe-rukovodstvo">Пошаговое руководство</a></div>
                            <div class="menu_item"><a href="/svyazatsya-s-nami">Связаться с компанией</a></div>
                        </td>
                        <td valign="top" width="253">
                            <h3 class="menu_title">О компании</h3>
                            <div class="menu_item"><a href="/o-kompanii">О Migesco</a></div>
                            <div class="menu_item"><a href="/preimushestva">Наши преимущества</a></div>
                            <div class="menu_item"><a href="/regulirovanie">Регулирование</a></div>
                            <div class="menu_item"><a href="/sohrannost-klientskih-sredstv">Сохранность средств</a>
                            </div>
                            <div class="menu_item"><a href="/category/novosti-kompanii">Новости</a></div>
                            <div class="menu_item"><a href="/partnerskaja-programma-migesco">Партнерские программы</a>
                            </div>
                        </td>
                        <td valign="top" style="border-left:1px solid #cad3dd;padding-left:25px !important;">
                            <h3 class="menu_title" style="padding-left:0 !important;">Оставайтесь на связи:</h3>
                            <div id="social_box">
                                <div>
                                    <a class="facebook" href="https://www.facebook.com/migesco" target="_blank"></a>
                                    <a class="vk" href="https://vk.com/migesco" target="_blank"></a>
                                    <a class="telegram" href="http://tele.gg/migesco" target="_blank"></a>
                                    <a class="youtube" href="https://www.youtube.com/channel/UCsjgHJUS1Hkk68SnI3FDZTQ"
                                       target="_blank"></a>
                                </div>
                                <div>
                                    <a class="google" href="https://plus.google.com/u/0/+Migesco/posts"
                                       target="_blank"></a>
                                    <a class="odnoklassniki" href="http://www.odnoklassniki.ru/group/53119392350433"
                                       target="_blank"></a>
                                    <a class="twitter" href="https://twitter.com/migescocom" target="_blank"></a>
                                </div>
                            </div>
                            <h3 class="menu_title" style="padding-left:0 !important;">Мобильные приложения:</h3>
                            <div id="playgoogle"><a href="https://play.google.com/store/apps/details?id=org.TOL.Migesco"
                                                    target="_blank"></a></div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    <?php elseif (prior_lang() == 'en'): //ENGLISH CONTACT?>
        <div id="footer_m">
            <div class="padding">
                <table width="100%" cellpadding="0" cellspacing="0" border="0" id="menu_box">
                    <tr>
                        <td valign="top" width="252">
                            <h3 class="menu_title">How to start trading</h3>
                            <div class="menu_item op-users-only"><a href="/en/deposit-account">Deposit account</a></div>
                            <div class="menu_item op-guests-only"><a href="/en/#register">Open account</a></div>
                            <div class="menu_item"><a href="/en/deposits-and-withdrawals#deposit">How to deposit
                                    money</a></div>
                            <div class="menu_item"><a href="/en/how-to-start-trading">How to start trading</a></div>
                            <div class="menu_item"><a href="/en/learn-how-to-trade">Learn how to trade</a></div>
                            <div class="menu_item"><a href="/en/how-to-forecast-markets">How to forecast markets</a>
                            </div>
                            <div class="menu_item"><a href="/en/deposits-and-withdrawals#withdraw">How to withdraw
                                    money</a></div>
                        </td>
                        <td valign="top" width="253">
                            <h3 class="menu_title">Need help?</h3>
                            <div class="menu_item op-guests-only"><a href="/en/#login">MyMigesco</a></div>
                            <div class="menu_item op-users-only"><a href="/en/deposit-account">MyMigesco</a></div>
                            <div class="menu_item"><a href="/en/traderoom">Trade now</a></div>
                            <div class="menu_item"><a href="/en/trading-terms">Terms and conditions</a></div>
                            <div class="menu_item"><a href="/en/assets">Assets for trading</a></div>
                            <div class="menu_item"><a href="/en/binary-options-types">Binary options types</a></div>
                            <div class="menu_item"><a href="/en/contact-us">Contact us</a></div>
                        </td>
                        <td valign="top" width="253">
                            <h3 class="menu_title">About us</h3>
                            <div class="menu_item"><a href="/en/about-migesco">About Migesco</a></div>
                            <div class="menu_item"><a href="/en/why-migesco">Why Migesco</a></div>
                            <div class="menu_item"><a href="/en/regulation">Regulation</a></div>
                            <div class="menu_item"><a href="/en/safe-trading">Safe trading</a></div>
                            <div class="menu_item"><a href="/en/our-awards">Our awrads</a></div>
                            <div class="menu_item"><a href="/en/affiliate-network">Affiliate network</a></div>
                        </td>
                        <td valign="top" style="border-left:1px solid #cad3dd;padding-left:25px !important;">
                            <h3 class="menu_title" style="padding-left:0 !important;">Stay online:</h3>
                            <div id="social_box">
                                <div>
                                    <a class="facebook" href="https://www.facebook.com/migesco" target="_blank"></a>
                                    <a class="vk" href="https://vk.com/migesco" target="_blank"></a>
                                    <a class="telegram" href="http://tele.gg/migesco" target="_blank"></a>
                                    <a class="youtube" href="https://www.youtube.com/channel/UCsjgHJUS1Hkk68SnI3FDZTQ"
                                       target="_blank"></a>
                                </div>
                                <div>
                                    <a class="google" href="https://plus.google.com/u/0/+Migesco/posts"
                                       target="_blank"></a>
                                    <a class="odnoklassniki" href="http://www.odnoklassniki.ru/group/53119392350433"
                                       target="_blank"></a>
                                    <a class="twitter" href="https://twitter.com/migescocom" target="_blank"></a>
                                </div>
                            </div>
                            <h3 class="menu_title" style="padding-left:0 !important;">Mobile apps:</h3>
                            <div id="playgoogle"><a href="https://play.google.com/store/apps/details?id=org.TOL.Migesco"
                                                    target="_blank"
                                                    style="background: url(/wp-content/themes/tol-parent/images/home/google.png) top left no-repeat !important;"></a>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    <?php endif; ?>


    <?php if (is_front_page()): //CHECK HOME PAGE?>
        <?php if (prior_lang() == 'ru'): //RUSSIAN SEOTEXT?>
            <div id="seo_box">
                <div class="padding">
                    <h2 class="seo_title">ЧТО ТАКОЕ ТОРГОВЛЯ НА ФИНАНСОВЫХ РЫНКАХ?</h2>
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <style>
                            p.text{
                                padding: 7px;
                            }
                        </style>
                        <tr>
                            <td width="50%" valign="top">
                                <p class = "text">
                                    Начнем с определения: финансовые рынки – это самый современный инструмент для
                                    инвестирования, который подразумевает заключение сделки, по которой вы получаете
                                    прибыль, если правильно определили направление изменения курса какого-либо актива
                                    (товара, акции, валюты или индекса).
                                </p>
                                <p class = "text">
                                    Торговля на финансовых рынках позволяет зарабатывать на колебаниях курсов отдельных
                                    активов. Трейдер выбирает актив – валюту, золото, нефть, серебро, акцию крупной
                                    компании, определяет направление изменения курса - то есть, будет ли курс актива
                                    выше или ниже, чем курс, установленный на текущую минуту, и заключает сделку.
                                </p>
                                <p class = "text">
                                    Наша компания предлагает торговать 250 активами, которые торгуются на всех Мировых
                                    биржах. Что это значит для вас? Это значит, что вы всегда можете найти подходящий
                                    актив для торговли, где бы вы не находились, ведь мировые рынки не спят!
                                </p>
                            </td>
                            <td width="50%" valign="top" style="padding-left:28px;">
                                <p class = "text">
                                    Migesco заботится о своих трейдерах и предлагает бесплатные программы-помощники,
                                    которые позволяют сделать торговлю более прибыльной.  Торговые сигналы,
                                    экономический календарь, поддержка персонального аналитика, индивидуальный курс
                                    обучения, торговый план на неделю – вот лишь часть тех программ, которые специалисты
                                    компании разработали для вас! Не теряйте драгоценное время и не упускайте
                                    возможность заработать с брокером Migesco!
                                </p>
                                <p class = "text">
                                    Мы ежедневно сотни раз отвечаем на главный вопрос «Как научиться торговать на
                                    финансовых рынках?» Именно поэтому особое внимание компания Migesco уделяет обучению
                                    торговле и инвестициям. Специалистами компании были разработаны специальные
                                    пошаговые инструкции, позволяющие освоить торговлю в кратчайшие сроки.
                                </p>
                                <p class = "text">
                                    Migesco гордится тем, что с нами торговля на финансовых рынка превращаются в
                                    надежный, достойный и прибыльный способ заработка.
                                </p>
                            </td>
                        </tr>
                    </table>
                    <h2 class="seo_title">Почему торговля на ProfEx и SimplEx получают все большую популярность среди
                        трейдеров?</h2>
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td width="50%" valign="top">
                                <div class="seo_li">
                                    <p>
                                        Во-первых, потому что два разных вида исполнения сделок позволяют торговать
                                        трейдерам с разным уровнем знаний и разными торговыми стратегиями. Если ProfEx
                                        больше подходит для трейдеров с большим опытом работы, то SimplEх во многом
                                        напоминает традиционных бинарные опционы, так как позволяет заранее соотнести
                                        возможности для заработка и вероятные риски.
                                    </p>
                                </div>
                                <div class="seo_li">
                                    <p>
                                        ProfEx и SimplEx это выгодно, так как в моменты повышенной волатильности, курс
                                        выбранного актива может уйти достаточно далеко от круса открытия, что поможет
                                        вам увеличить депозит в несколько раз.
                                    </p>
                                </div>
                            </td>
                            <td width="50%" valign="top" style="padding-left:28px;">
                                <div class="seo_li">
                                    <p>
                                        Торговля на ProfEx и SimplEx это быстро - открыть счет можно за две минуты,
                                        достаточно лишь заполнить небольшую анкету. Время истечения сделки вы
                                        определяете сами!
                                    </p>
                                </div>
                                <div class="seo_li">
                                    <p>
                                        Заранее прописанные условия сделки - маржа, своп и стоимость пункта - позволяют
                                        вам самостоятельно контролировать свои инвестиции, ваш успех в ваших руках, а
                                        Migesco это ваш надежный партнер в современном финансовом мире!
                                    </p>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        <?php elseif (prior_lang() == 'en'): //ENGLISH SEOTEXT?>
            <div id="seo_box">
                <div class="padding">
                    <h2 class="seo_title">WHAT IS BINARY OPTIONS TRADING?</h2>
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td width="50%" valign="top">
                                Binary options - are the most popular financial tool, that allows to get a
                                pre-determined profit if you make a correct trading decision – where the asset's
                                (commodity, stock, currency or index) rate will move - above or below after a certain
                                period of time (from 60 seconds up to six months).<br/><br/>
                                Binary options allow to earn money on a rate fluctuations of different assets. How
                                binary options trading works?<br/><br/>
                                Trader chooses asset - currency, oil, gold, silver, share of a world famous company,
                                determines the direction of rate movement - whether the rate of the asset will go above
                                or below than the rate fixed at the current moment, and makes a deal.<br/><br/>
                                Our company offers more than 250 assets, that are traded on all the world's stock
                                exchanges. What does that mean for you? It means that you can always find a best asset
                                for trading, no matter where you are at the moment, because world markets never sleep!
                            </td>
                            <td width="50%" valign="top" style="padding-left:28px;">
                                Migesco cares about all traders and offers free special programs that helps to make
                                binary options trading even more profitable. Trading signals, economic calendar, support
                                of personal analyst, an individual educational course, a trading plan for the upcoming
                                week - that's only a small part of the programs that our company has developed for you!
                                Do not waste your precious time and do not miss the opportunity to earn money on binary
                                options trading with Migesco!
                                <br/><br/>
                                Hundred times a day we answer one question: "How to learn binary options trading?" That
                                is why Migesco pays special attention to binary options trading educational materials.
                                Specialists of the company developed "binary options trading course", which is popular
                                among beginners and experienced traders. Migesco proud that binary options trading is
                                converted into a reliable, decent and profitable way to make money in the financial
                                markets.
                            </td>
                        </tr>
                    </table>
                    <h2 class="seo_title">Why binary options have become so popular among traders?</h2>
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td width="50%" valign="top">
                                <div class="seo_li"><span>First, because binary options are simple: all you only need is to select the asset, determine where the asset's rate will go - above or below in a certain time (from one to five minutes), enter the amount of the deal and get the profit.</span>
                                </div>
                                <div class="seo_li"><span>Binary options are profitable — we offer up to 240% payout from one deal, while it is enough to get one point of rate change only to make a profit! It means that if you invest $500, you can earn $1200 per one deal!</span>
                                </div>
                            </td>
                            <td width="50%" valign="top" style="padding-left:28px;">
                                <div class="seo_li"><span>Binary options are fast — it will take you two minutes only to open a trading account. Deal expiration time starts from one minute, it means that you can make a profit after 60 seconds!</span>
                                </div>
                                <div class="seo_li"><span>Binary options trading is a trading with a limited risk, the trader risk is an amount of the deal, which trader can choose himself. Trader can control his investments, your success is in your hands, and Migesco is your reliable partner in the modern financial world!</span>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>


    <?php if (prior_lang() == 'ru'): //RUSSIAN FOOTER?>
        <div id="footer">
            <div class="padding">
                <div class="pad">
                    <div id="copyr">Copyright © 2012-<?php echo date('Y'); ?> Migesco</div>
                    <div id="sub_foot">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td valign="top" width="526">
                                    <div style="padding-right:47px !important;">
                                        Все материалы размешенные на сайте охраняются авторским правом.<br/>
                                        Использование информации, размещённой на сайте, в том числе копирование,<br/>
                                        распространение, передача третьим лицам, опубликование или иные действия<br/>
                                        без письменного согласия Migesco не допускается.
                                    </div>
                                </td>
                                <td valign="top">
                                    Деятельность компании Optima Capital Ltd. осуществляется на St. Vincent.<br/>
                                    Осуществление платежных операций обеспечивает компания Migesco Services,<br/>
                                    адрес: Klimentos 41-43, Klimentos Tower, Office 25, 1061, Nicosia, Cyprus.
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <?php elseif (prior_lang() == 'en'): //ENGLISH FOOTER?>
        <div id="footer">
            <div class="padding">
                <div class="pad">
                    <div id="sub_link">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td width="339">
                                    <div><a href="/en/easy-fast-and-profitable">Binary options</a> - is easy, fast and
                                        profitable way to earn money on the financial markets.
                                    </div>
                                </td>
                                <td width="337">
                                    <div><a href="/en/binary-options-trading">Trade binary options </a> on currencies,
                                        commodities, shares and indices.
                                    </div>
                                </td>
                                <td>
                                    <div><a href="https://www.migesco.com/en/learn-how-to-trade">Learn how to trade
                                            binary options</a>.
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="copyr">Copyright © 2012-<?php echo date('Y'); ?> Migesco</div>
                    <div id="sub_foot">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td valign="top" width="526">
                                    <div style="padding-right:47px !important;">
                                        All materials posted on the site are protected by copyright.<br/>
                                        Use of the information posted on this website, including copy,
                                        distribution, transfer to third parties, publication or other actions
                                        without the prior written consent of Migesco is not allowed.
                                    </div>
                                </td>
                                <td valign="top">
                                    Optima Capital Ltd. operates on St. Vincent.<br/>
                                    Payment services are carried by Migesco Services,<br/>
                                    address: Klimentos 41-43, Klimentos Tower, Office 25, 1061, Nicosia, Cyprus.
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

    </div><!-- #page -->

<?php } else { ?>

    <!-- Footer -->
    <footer id="tol-footer">
        <div class="statusbar cf _focusable" id="statusbar" tabindex="-1">
            <div class="fleft">
                <div class="statusbar-item account-type demo-user" data-translate="PracticeAccount">Practice Account
                </div>
                <div class="statusbar-item account-type real-user" data-translate="RealAccount">Real Account</div>
                <div class="statusbar-item current-time">
                    <span data-widget="currentDateTime" data-type="time" class="currentTimeWidget value"></span>
                    <span class="utctime label">(<span data-widget="currentDateTime" data-type="timeUTC"
                                                       class="currentTimeUtcWidget"></span>&nbsp;UTC)</span>
                </div>
            </div>
            <div class="equity-container">
                <div class="equity-content">
                    <div class="equity-item balance-item" id="equity-ppl">
                        <span class="label" data-translate="userDataBalance"></span>
                        <span data-widget="balancev2" data-type="balance" class="value blue"></span>
                    </div>
                    <?php if (defined('ISFOREX') && ISFOREX == true) { ?>
                        <div class="equity-item profit-item" id="equity-ppl">
                            <span class="label" data-translate="profit"></span>
                            <span data-widget="balancev2" data-type="profit" class="value"></span>
                        </div>
                        <div class="equity-item equity-item" id="equity-total">
                            <span class="label" data-translate="equity"></span>
                            <span data-widget="balancev2" data-type="equity" class="value"></span>
                        </div>
                        <div class="equity-item margin-item" id="equity-indicator">
                            <span class="label" data-translate="footer_margin"></span>
                            <span data-widget="balancev2" data-type="forexMargin" class="value"></span>
                            <div class="margin-indicator" data-widget="balancev2" data-type="marginPercentBar"></div>
                            <span data-widget="balancev2" data-type="marginPercentValue"
                                  class="value margin-percent-value blue"></span>
                        </div>
                        <div class="equity-item avbalance-item" id="equity-total">
                            <span class="label" data-translate="availableBalance"></span>
                            <span data-widget="balancev2" data-type="availableBalance" class="value"></span>
                        </div>
                    <?php } ?>
                    <span class="footer-settings-icon column-settings">
                            <div class="settings"></div>
                            <div class="contextmenu multiple-selection datatable-contextmenu _focusable leftCenter">
                                <div class="multiple-selection-content">
                                    <div class="item item-datatable-contextmenu-name selected" data-column="1">
                                        <span data-translate="settings_balance"></span>
                                        <div class="list-checkbox"></div>
                                    </div>
                                    <div class="item item-datatable-contextmenu-name selected" data-column="2">
                                        <span data-translate="settings_profit"></span>
                                        <div class="list-checkbox"></div>
                                    </div>
                                    <div class="item item-datatable-contextmenu-quantity selected" data-column="3">
                                        <span data-translate="settings_equity"></span>
                                        <div class="list-checkbox"></div>
                                    </div>
                                    <div class="item item-datatable-contextmenu-direction selected" data-column="4">
                                        <span data-translate="settings_margin"></span>
                                        <div class="list-checkbox"></div>
                                    </div>
                                    <div class="item item-datatable-contextmenu-averagePrice selected" data-column="5">
                                        <span data-translate="settings_avbalance"></span>
                                        <div class="list-checkbox"></div>
                                    </div>
                                    <div class="btn-wrapper navigation">
                                        <div class="nav-button defaultSettings" data-translate="defaultSettings"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                </div>
            </div>
        </div>
        <div id="tol-footer-layout-1">
            <div class="container payment-icons">
                <div id="footer-logos-wrapper">
                    <div class="footer-logo col-xs-6 col-sm-2 visa"></div>
                    <div class="footer-logo col-xs-6 col-sm-2 mastercard"></div>
                    <div class="footer-logo col-xs-6 col-sm-2 maestro"></div>
                    <div class="footer-logo col-xs-6 col-sm-2 neteller"></div>
                    <div class="footer-logo col-xs-6 col-sm-2 paysafe"></div>
                    <div class="footer-logo col-xs-6 col-sm-2 wiretransfer"></div>
                </div>
            </div>
            <div id="tol-footer-inside">
                <div class="container" style="padding: 0 !important;">
                    <nav class="col-sm-8 nopadding">
                        <?php
                        // footer hor menu
                        if (has_nav_menu('footer_hor'))
                            wp_nav_menu(array('theme_location' => 'footer_hor', 'menu_class' => 'hor-menu nav nav-pills')); ?>
                    </nav>
                    <nav class="col-sm-offset-10">
                        <ul class="nav nav-pills social-nav text-right">
                            <li>
                                <a href="#" target="_blank" class="fb-social"></a>
                            </li>
                            <li>
                                <a href="#" target="_blank" class="tw-social"></a>
                            </li>
                            <li>
                                <a href="#" target="_blank" class="li-social"></a>
                            </li>
                        </ul>
                    </nav>
                    <hr>
                    <aside>
                        <small><?php dynamic_sidebar('tol-disclosure'); ?></small>
                    </aside>
                </div>
            </div>
        </div>
    </footer>

<?php } ?>

<?php if (basename(get_permalink()) == 'traderoom') { ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#pop_close').click(function () {
                $('#pop_overlay').remove();
            });

            $('#pop_overlay .pop_box').live("click", function (event) {
                if (!$(event.target).is('#pop_close')) location.href = "/deposit-account?superbonus";
            });

//demo
            if (window.location.hash === '#registration-success') {
                $('#popup_overlay22').show();
                $.cookie("show_reg", 1);
            }

            if ($.cookie("show_reg") === 1) {
                $('#popup_overlay22').show();
            }

            setTimeout(function () {
                $('#popup_close22').show();
            }, 3000);

            $('#popup_overlay2').click(function (event) {
                if ($(event.target).is("#popup_overlay22")) $('#popup_close22').show();
            });

            $('#popup_close22, #popup_overlay22 .demo_but a').click(function () {
                $('#popup_overlay22').hide();
                $.removeCookie("show_reg");
                $(".clearfix.flat-top-borders:not(#rss-feeds)").append('<div style="margin-bottom:5px !important;"><a href="javascript://"><img src="/wp-content/themes/tol-child/images/banner1small.png"></a></div>');
            });

            $('#popup_overlay22 .real_but a').click(function () {
                $.removeCookie("show_reg");
            });

        });
    </script>
    <style type="text/css">
        #pop_overlay {
            top: 0;
            left: 0;
            position: fixed;
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, .5);
            z-index: 9999999;
        }

        #pop_overlay .pop_box {
            width: 860px;
            height: 584px;
            margin: auto;
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            background: url('/wp-content/themes/tol-parent/images/weekend/big_popup.jpg') no-repeat;
            cursor: pointer;
        }

        #pop_close {
            width: 120px;
            height: 60px;
            position: absolute;
            right: 2px;
            top: 2px;
            background: url('/wp-content/themes/tol-parent/images/weekend/close_popup.png') top right no-repeat;
            cursor: pointer;
        }

        /* DEMO */
        #popup_overlay22 {
            top: 0;
            left: 0;
            position: fixed;
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, .5);
            z-index: 9999999;
        }

        #popup_overlay22 .popup_box {
            width: 600px;
            height: 400px;
            margin: auto;
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            background: url('/wp-content/themes/tol-parent/images/demo/big_popup.jpg');
        }

        #popup_overlay22 .popup_header {
            color: white;
            text-align: center;
            font: bold 24px 'PT Sans';
            padding-top: 20px;
        }

        #popup_overlay22 .popup_content td {
            text-align: center;
            padding-top: 30px;
        }

        #popup_overlay22 .box_title {
            font: bold 19px 'PT Sans';
            padding-bottom: 22px;
            color: white;
            padding-top: 10px;
        }

        #popup_overlay22 .demo_price {
            font: bold 24px 'PT Sans';
            color: white;
            padding-bottom: 15px;
        }

        #popup_overlay22 .real_price {
            font: bold 24px 'PT Sans';
            color: white;
            padding-bottom: 15px;
        }

        #popup_overlay22 .demo_li, #popup_overlay22 .real_li {
            text-align: left;
            padding-left: 13px;
            font: 15px 'PT Sans';
            color: white;
            margin-top: 15px;
        }

        #popup_overlay22 .demo_li div:first-child {
            padding-left: 29px;
            background: url('/wp-content/themes/tol-parent/images/demo/no_1.png') left top no-repeat;
            min-height: 22px;
            margin-bottom: 3px;
        }

        #popup_overlay22 .demo_li div:last-child {
            padding-left: 29px;
            background: url('/wp-content/themes/tol-parent/images/demo/no_2.png') left top no-repeat;
            min-height: 27px;
        }

        #popup_overlay22 .real_li div:first-child {
            padding-left: 26px;
            background: url('/wp-content/themes/tol-parent/images/demo/yes_1.png') left top no-repeat;
            min-height: 22px;
            margin-bottom: 3px;
        }

        #popup_overlay22 .real_li div:last-child {
            padding-left: 26px;
            background: url('/wp-content/themes/tol-parent/images/demo/yes_2.png') left top no-repeat;
            min-height: 27px;
        }

        #popup_overlay22 .demo_but a {
            margin-top: 5px;
            height: 41px;
            display: inline-block;
            background-color: #69BC36;
            font: 14px 'PT Sans';
            color: white;
            padding-left: 35px;
            padding-right: 35px;
            text-decoration: none;
            padding-top: 3px;
            cursor: pointer;
        }

        #popup_overlay22 .demo_but a span {
            font-size: 14px;
            font-weight: normal;
        }

        #popup_overlay22 .real_but a {
            margin-top: 5px;
            height: 44px;
            display: inline-block;
            background-color: #69BC36;
            font: 19px/42px 'PT Sans';
            color: white;
            padding-left: 35px;
            padding-right: 35px;
            text-decoration: none;
            padding-top: 3px;
            cursor: pointer;
        }

        #popup_overlay22 .real_but a span {
            font-size: 14px;
            font-weight: normal;
        }

        #popup_overlay22 .li_title {
            text-align: center;
            font: bold 19px 'PT Sans';
            color: white;
            margin-top: 22px;
        }

        #popup_close22 {
            width: 18px;
            height: 17px;
            position: absolute;
            right: 1px;
            top: 1px;
            background: url('/wp-content/themes/tol-parent/images/demo/close.png') no-repeat;
            cursor: pointer;
        }
    </style>
    <?php
    $day = gmdate('D');
    $time = gmdate('H');
    $daysArray = array('Sat', 'Sun');

    if ((($day == 'Fri' && $time >= '21') || in_array($day, $daysArray) || ($day == 'Mon' && $time <= '02')) && (gmdate('Y-m-d H:i:s') > $_COOKIE['next_date'])) {
        setcookie("pop_show", '', 1);
        $_COOKIE['pop_show'] = '';
    }

    if ((($day == 'Fri' && $time >= '21') || in_array($day, $daysArray) || ($day == 'Mon' && $time <= '02')) && $_COOKIE['pop_show'] != '1') {
        setcookie("pop_show", '1');
        setcookie("next_date", gmdate('Y-m-d H:i:s', strtotime('next friday, 09:00pm')));
        ?>
        <div id="pop_overlay">
            <div class="pop_box">
                <div id="pop_close"></div>
            </div>
        </div>
        <?php
    }
    ?>

    <div id="popup_overlay22" style="display:none;">
        <div class="popup_box">
            <div class="popup_header"><?php echo(prior_lang() == 'en' ? 'Your trading account is registered' : 'Ваш торговый счет зарегистрирован') ?></div>
            <div class="popup_content">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td valign="top" width="50%">
                            <div class="box_title"><?php echo(prior_lang() == 'en' ? 'Your demo account<br /> is credited' : 'На ваш демо-счет в Migesco<br /> зачислено') ?></div>
                            <div class="demo_price">₽ 15 000</div>
                            <div class="demo_but">
                                <a><?php echo(prior_lang() == 'en' ? 'Start trading on a demo account' : 'Начать торговать на<br /> демо-счете') ?></a>
                            </div>
                            <div class="li_title"><?php echo(prior_lang() == 'en' ? 'Demo trading:' : 'Торговля на демо-счете:') ?></div>
                            <div class="demo_li">
                                <div><?php echo(prior_lang() == 'en' ? 'Does not require to deposit real<br /> money' : 'Не требует пополнения реальными<br /> деньгами') ?></div>
                                <div><?php echo(prior_lang() == 'en' ? 'It is not possible to withdraw money' : 'Невозможно вывести прибыль') ?></div>
                            </div>
                        </td>
                        <td valign="top" width="50%">
                            <div class="box_title"><?php echo(prior_lang() == 'en' ? 'On your real account<br /> now is' : 'На вашем реальном счете<br /> сейчас') ?></div>
                            <div class="real_price">₽ 0</div>
                            <div class="real_but"><a
                                        href="cashier?cpage=deposit"><?php echo(prior_lang() == 'en' ? 'Deposit' : 'Пополнить счет') ?></a>
                            </div>
                            <div class="li_title"><?php echo(prior_lang() == 'en' ? 'Real money trading:' : 'Торговля на реальном счете:') ?></div>
                            <div class="real_li">
                                <div><?php echo(prior_lang() == 'en' ? 'Minimum deposit $5' : 'Минимальный депозит 250 рублей') ?></div>
                                <div><?php echo(prior_lang() == 'en' ? 'Allows to earn real money' : 'Позволяет зарабатывать реальные<br /> деньги') ?></div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="popup_close22" style="display:none;"></div>
        </div>
    </div>

<?php } ?>
<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700&;subset=cyrillic,cyrillic-ext,latin-ext"
      rel="stylesheet"/>
<?php include(dirname(__FILE__) . '/../widget-call.php'); ?>
<?php include(dirname(__FILE__) . '/../widget-x2.php'); ?>
<?php include(dirname(__FILE__) . '/../widget-vebinar.php'); ?>
<?php if (get_post_field('post_name', get_post()) != 'deposit-account' && get_post_field('post_name', get_post()) != 'cashier') { ?>
    <div id="aktiv_day"></div>
    <?php
    if ($_SERVER['REQUEST_URI'] == '/category/puls-rynka') $hide = '?hide=1';
    ?>
<?php } ?>

<link rel='stylesheet' id='ptsans-font' href='<?php echo get_template_directory_uri(); ?>/ptsan.css' type='text/css'
      media='all'/>

<script type="text/javascript">
    function popup_strategy(pageURL, title) {
        var left = (screen.width / 2) - (1000 / 2);
        var top = (screen.height / 2) - (700 / 2);
        var targetWin = window.open(pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width=1000, height=700, top=' + top + ', left=' + left);
    }

    var thousandSeparator = function (str) {
        var parts = (str + '').split('.'),
            main = parts[0],
            len = main.length,
            output = '',
            i = len - 1;

        while (i >= 0) {
            output = main.charAt(i) + output;
            if ((len - i) % 3 === 0 && i > 0) {
                output = ' ' + output;
            }
            --i;
        }

        if (parts.length > 1) {
            output += '.' + parts[1];
        }
        return output;
    };


    function createCookie(name, value, days) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            var expires = "; expires=" + date.toGMTString();
        }
        else {
            var expires = "";
        }
        document.cookie = name + "=" + value + expires + "; path=/";
    }

    function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    }

    $(document).ready(function () {

        $(window).on('loadingIsCompleted', function (event, response) {


            $('li[data-value="custom"]').live("click", function () {
                if ($('form.assetFilterCustomPopup').css('display') === 'none') {
                    $('div[data-type="assetTypeFilter"]').click();
                }
            });


            $('#popup_close22, #popup_overlay22 a').click(function () {
                var widgetUserData = JSON.parse($.cookie('widgetUserData'));
                var user_id = widgetUserData.id + '_';
                $.cookie("temp_cookie", user_id);
                user_id = $.cookie("temp_cookie");
                $.cookie(user_id + "popup_timeout22", 60000); //popup 1
                setInterval(function () {
                    var widgetUserData = JSON.parse($.cookie('widgetUserData'));
                    var user_id = widgetUserData.id + '_';
                    $.cookie(user_id + "popup_timeout22", parseInt($.cookie(user_id + "popup_timeout22")) - 1000);
                    if ($.cookie(user_id + "popup_timeout22") === 0) {
                        $.removeCookie(user_id + "popup_timeout22");
                        $('#popup_overlay').show();
                    }
                }, 1000);
            });

        });

        if ($.cookie(user_id + "popup_timeout22")) {
            setInterval(function () {
                $.cookie(user_id + "popup_timeout22", parseInt($.cookie(user_id + "popup_timeout22")) - 1000);
                if ($.cookie(user_id + "popup_timeout22") === 0) {
                    $.removeCookie(user_id + "popup_timeout22");
                    $('#popup_overlay').show();
                }
            }, 1000);
        }


        $('.navigationContainer li').click(function () {
            $('.navigationContainer').find('li').css('display', 'none');
            $('.navigationContainer').css('overflow', 'hidden');
        });

        $('.navigationContainer').mouseover(function () {
            $(this).find('li').css('display', 'block');
        });

        $(window).click(function () {
            $('.navigationContainer li').css('display', 'none');
            $('.navigationContainer').css('overflow', 'hidden');
            $('.tooltip').hide();
        });

        $(window).on('hashchange', function () {
            if (window.location.hash === '#registration-success' || window.location.hash === '#login-success') {
                location.reload();
            }
        });

        $('#popup_close').live("click", function () {
            var widgetUserData = JSON.parse($.cookie('widgetUserData'));
            var user_id = widgetUserData.id + '_';
            $.cookie("temp_cookie", user_id);
            var user_id = $.cookie("temp_cookie");
            window.location.hash = '';
            $('#popup_overlay').remove();
            $.cookie(user_id + "popup_timeout2", 60000); //popup час
            setInterval(function () {
                var widgetUserData = JSON.parse($.cookie('widgetUserData'));
                var user_id = widgetUserData.id + '_';
                $.cookie(user_id + "popup_timeout2", parseInt($.cookie(user_id + "popup_timeout2")) - 1000);
                if ($.cookie(user_id + "popup_timeout2") === 0) {
                    $.removeCookie(user_id + "popup_timeout2");
                    $('#popup_overlay3').show();
                    $.cookie(user_id + "close_time", $.now() + (60 * 60 * 1000), {path: '/'}); //360000
                    $.cookie(user_id + "popup_hide", 0, {path: '/'});
                    $('#popup_overlay3').show();
                    $('#ban_nobonus').hide();
                    $('#ban_bonus').show();
                    $.cookie(user_id + "countdown", parseInt(($.cookie(user_id + "close_time") - $.now()) / 10));
                    counterTime.resetCountdown();
                    setTimeout(function () {
                        $('#popup_close3').show();
                    }, 3000);
                }
            }, 1000);

            $(".clearfix.flat-top-borders:not(#rss-feeds)").append('<div style="margin-bottom:5px !important;"><a href="javascript://"><img src="/wp-content/themes/tol-child/images/banner2small.png"></a></div>');
        });

        $('#popup_overlay .popup_box').click(function (event) {
            $('#popup_overlay .popup_box .popup_title, #popup_overlay .popup_box .popup_button').hide();
            if (!$(event.target).hasClass('show_ppt') && !$(event.target).hasClass('show_ppt_en')) {
                if ($('#popup_overlay .popup_box .popup_img iframe#video').length === 0) {
                    $('#popup_overlay .popup_box').css('height', '364px');
                    $('#popup_overlay .popup_box .popup_img').html('<iframe id="video" width="611" height="344" src="https://www.youtube.com/embed/C0f-Vw_0hEA" frameborder="0" allowfullscreen></iframe>');
                }
            } else {
                if ($('#popup_overlay .popup_box .popup_img iframe#ppt').length === 0) {
                    $('#popup_overlay .popup_box').css('height', '484px');
                    if ($(event.target).hasClass('show_ppt_en')) {
                        $('#popup_overlay .popup_box .popup_img').html('<iframe id="ppt" width="611" height="464" src="https://www.migesco.com/wp-content/themes/xptheme-child/strategy-videos/trading/index.html" frameborder="0" allowfullscreen></iframe>');
                    } else {
                        $('#popup_overlay .popup_box .popup_img').html('<iframe id="ppt" width="611" height="464" src="/wp-content/themes/xptheme-child/strategy-videos/new/index.html" frameborder="0" allowfullscreen></iframe>');
                    }
                }
            }
        });

        $('#popup_close2').click(function () {
            $('#popup_overlay2').hide();

            $(".clearfix.flat-top-borders:not(#rss-feeds)").append('<div style="margin-bottom:5px !important;"><a href="javascript://"><img src="/wp-content/themes/tol-child/images/banner3small.png"></a></div>');

        });

        $('#popup_overlay2 .popup_box').click(function (event) {
            if ($('#popup_overlay2 .popup_box .popup_img iframe#ppt2').length === 0) {
                $('#popup_overlay2 .popup_box .show_ppt2').hide();
                $('#popup_overlay2 .popup_box').css('height', '494px');
                <?php if (prior_lang() == 'ru') { ?>
                $('#popup_overlay2 .popup_box .popup_img').html('<iframe id="ppt2" width="611" height="474" src="/wp-content/themes/xptheme-child/strategy-videos/1/index.html" frameborder="0" allowfullscreen></iframe>');
                <?php } else { ?>
                $('#popup_overlay2 .popup_box').css('width', '611px');
                $('#popup_overlay2 .popup_box .popup_img').html('<iframe id="ppt2" width="611" height="474" src="https://www.migesco.com/wp-content/themes/xptheme-child/strategy-videos/candlesticks/index.html" frameborder="0" allowfullscreen></iframe>');
                <?php } ?>

            }
        });
    });
</script>

<style type="text/css">
    .widgetPlaceholder.showButton {
        background-color: #69bb1b !important;
        border: 0 !important;
    }

    .widgetPlaceholder.showButton:hover {
        background-color: #458E00 !important;
    }

    #popup_overlay, #popup_overlay2 {
        top: 0;
        left: 0;
        position: fixed;
        width: 100%;
        height: 100%;
        background: rgba(0, 0, 0, .5);
        z-index: 9999999;
    }

    #popup_overlay .popup_box, #popup_overlay2 .popup_box {
        width: 611px;
        height: 430px;
        margin: auto;
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        background-color: white;
        border: 1px solid grey;
    }

    #popup_overlay .popup_box .popup_title {
        text-align: center;
        font: bold 21px Arial;
        color: #4b6e9b;
        margin-top: 20px;
    }

    #popup_overlay .popup_box .popup_img {
        text-align: center;
        margin-top: 20px;
    }

    #popup_overlay2 .popup_box .popup_img {
        text-align: center;
        margin-top: 20px;
    }

    #popup_overlay .popup_box .popup_button {
        margin-top: 20px;
    }

    #popup_overlay .popup_box .popup_img img, #popup_overlay2 .popup_box .popup_img img {
        cursor: pointer;
    }

    #popup_close, #popup_close2 {
        width: 11px;
        height: 11px;
        position: absolute;
        right: 5px;
        top: 5px;
        background: url('/wp-content/themes/tol-parent/images/xp-theme/close.png') no-repeat;
        cursor: pointer;
    }

    #popup_overlay .popup_box .show_video {
        cursor: pointer;
        background-color: #4b6e9b;
        display: inline-block;
        color: white;
        padding: 6px 30px;
        border: 1px solid #4b6e9b;
        font: bold 17px Arial;
        margin-right: 10px;
    }

    #popup_overlay .popup_box .show_ppt {
        cursor: pointer;
        background-color: white;
        display: inline-block;
        color: #4b6e9b;
        padding: 6px 30px;
        border: 1px solid black;
        font: bold 17px Arial;
    }

    #popup_overlay2 .popup_box .show_ppt2 {
        position: absolute;
        bottom: 35px;
        text-align: center;
        width: 100%;
    }

    #popup_overlay2 .popup_box .show_ppt2 a {
        cursor: pointer;
        background-color: #4b6e9b;
        display: inline-block;
        color: white;
        padding: 15px 7px;
        font: bold 17px Arial;
    }

    #content {
        background-color: white;
    }

    /* checkbox slide */
    .onoffswitch {
        position: relative;
        width: 44px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
    }

    .onoffswitch-checkbox {
        display: none;
    }

    .onoffswitch-label {
        display: block;
        overflow: hidden;
        cursor: pointer;
        border: 0;
        border-radius: 22px;
    }

    .onoffswitch-inner {
        display: block;
        width: 200%;
        margin-left: -100%;
        transition: margin 0.3s ease-in 0s;
    }

    .onoffswitch-inner:before, .onoffswitch-inner:after {
        display: block;
        float: left;
        width: 50%;
        height: 24px;
        padding: 0;
        line-height: 24px;
        font-size: 10px;
        color: white;
        font-family: Trebuchet, Arial, sans-serif;
        font-weight: bold;
        box-sizing: border-box;
    }

    .onoffswitch-inner:before {
        content: "";
        padding-left: 5px;
        background-color: #3D5B7F;
        color: #FFFFFF;
    }

    .onoffswitch-inner:after {
        content: "";
        padding-right: 5px;
        background-color: #E7EAEE;
        color: #666666;
        text-align: right;
    }

    .onoffswitch-switch {
        display: block;
        width: 24px;
        margin: 0;
        background: #FFFFFF;
        position: absolute;
        top: 0;
        bottom: 0;
        right: 20px;
        border: 1px solid #E7EAEE;
        border-radius: 22px;
        transition: all 0.3s ease-in 0s;
    }

    .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
        margin-left: 0;
    }

    .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
        right: 0;
    }

    .onoffswitch2 {
        position: relative;
        width: 30px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
    }

    .onoffswitch2-checkbox {
        display: none;
    }

    .onoffswitch2-label {
        display: block;
        overflow: hidden;
        cursor: pointer;
        border: 0;
        border-radius: 22px;
    }

    .onoffswitch2-inner {
        display: block;
        width: 200%;
        margin-left: -100%;
        transition: margin 0.3s ease-in 0s;
    }

    .onoffswitch2-inner:before, .onoffswitch2-inner:after {
        display: block;
        float: left;
        width: 50%;
        height: 16px;
        padding: 0;
        line-height: 16px;
        font-size: 10px;
        color: white;
        font-family: Trebuchet, Arial, sans-serif;
        font-weight: bold;
        box-sizing: border-box;
    }

    .onoffswitch2-inner:before {
        content: "";
        padding-left: 5px;
        background-color: #3D5B7F;
        color: #FFFFFF;
    }

    .onoffswitch2-inner:after {
        content: "";
        padding-right: 5px;
        background-color: #E7EAEE;
        color: #666666;
        text-align: right;
    }

    .onoffswitch2-switch {
        display: block;
        width: 16px;
        margin: 0;
        background: #FFFFFF;
        position: absolute;
        top: 0;
        bottom: 0;
        right: 14px;
        border: 1px solid #E7EAEE;
        border-radius: 22px;
        transition: all 0.3s ease-in 0s;
    }

    .onoffswitch2-checkbox:checked + .onoffswitch2-label .onoffswitch2-inner {
        margin-left: 0;
    }

    .onoffswitch2-checkbox:checked + .onoffswitch2-label .onoffswitch2-switch {
        right: 0;
    }
</style>


<div id="popup_overlay" class="<?php echo prior_lang('ru') ? '' : 'show_ppt_en'; ?>" style="display:none; ">
    <div class="popup_box <?php echo prior_lang('ru') ? '' : 'show_ppt_en'; ?>">
        <div class="popup_title <?php echo prior_lang('ru') ? '' : 'show_ppt_en'; ?>"><?php echo prior_lang('ru') ? 'Как заработать на бинарных опционах<br /> в торговой платформе Migesco' : 'How to trade binary options'; ?></div>
        <div class="popup_img <?php echo prior_lang('ru') ? '' : 'show_ppt_en'; ?>"><img
                    class="<?php echo prior_lang('ru') ? '' : 'show_ppt_en'; ?>"
                    src="/wp-content/themes/tol-parent/images/xp-theme/euro.jpg"/></div>
        <div class="popup_button <?php echo prior_lang('ru') ? '' : 'show_ppt_en'; ?>"
             style="text-align:center;"><?php echo prior_lang('ru') ? '<a class="show_video">Смотреть видео</a><a class="show_ppt">Смотреть презентацию</a>' : '<a class="show_ppt show_ppt_en">Learn More</a>'; ?></div>
        <div id="popup_close"></div>
    </div>
</div>
<div id="popup_overlay2" style="display:none;">
    <div class="popup_box" <?php echo prior_lang('ru') ? '' : 'style=""'; ?>>
        <div class="popup_img"><img width="611"
                                    src="/wp-content/themes/tol-parent/images/xp-theme/<?php echo prior_lang('ru') ? 'ppt.jpg' : 'strategy_ppt.png'; ?>"/>
        </div>
        <div class="show_ppt2"><?php echo prior_lang('ru') ? '<a>Смотреть презентацию</a>' : '<a>Learn More</a>'; ?></div>
        <div id="popup_close2"></div>
    </div>
</div>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter22015681 = new Ya.Metrika({
                    id: 22015681,
                    webvisor: true,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true
                });
            } catch (e) {
            }
        });

        const n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () {
                n.parentNode.insertBefore(s, n);
            };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol === "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera === "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript>
    <div><img src="//mc.yandex.ru/watch/22015681" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->

<!-- begin of Top100 code -->
<div style="display:none;">
    <script id="top100Counter" type="text/javascript" src="https://scounter.rambler.ru/top100.jcn?3035822"></script>
    <noscript>
        <a href="http://top100.rambler.ru/navi/3035822/">
            <img src="https://scounter.rambler.ru/top100.cnt?3035822" alt="Rambler's Top100" border="0"/>
        </a>
    </noscript>
</div>
<!-- end of Top100 code -->

<!-- Rating@Mail.ru counter -->
<script type="text/javascript">
    const _tmr = _tmr || [];
    _tmr.push({id: "2543721", type: "pageView", start: (new Date()).getTime()});
    (function (d, w) {
        const ts = d.createElement("script");
        ts.type = "text/javascript";
        ts.async = true;
        ts.src = (d.location.protocol === "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
        var f = function () {
            var s = d.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(ts, s);
        };
        if (w.opera === "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window);
</script>
<noscript>
    <div style="position:absolute;left:-10000px;">
        <img src="//top-fwz1.mail.ru/counter?id=2543721;js=na" style="border:0;" height="1" width="1"
             alt="Рейтинг@Mail.ru"/>
    </div>
</noscript>
<!-- //Rating@Mail.ru counter -->

<?php wp_footer();

$valueGetConfig = '';
$isValid = array_key_exists('config', $_GET);
if ($isValid) {
    $valueGetConfig = $_GET['config'];
}
?>
<script type="text/javascript">
    document.write(ssxdom('b13075cb792e333de3ec360539d5bda2c3b20d3d'));
</script>
<!-- Код тега ремаркетинга Google -->
<!--
С помощью тега ремаркетинга запрещается собирать информацию, по которой можно идентифицировать личность пользователя. Также запрещается размещать тег на страницах с контентом деликатного характера. Подробнее об этих требованиях и о настройке тега читайте на странице http://google.com/ads/remarketingsetup.
-->
<div style="display:none;">
    <script type="text/javascript">
        /* <![CDATA[ */
        const google_conversion_id = 986918291;
        const google_custom_params = window.google_tag_params;
        const google_remarketing_only = true;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt=""
                 src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/986918291/?value=0&;guid=ON&;script=0"/>
        </div>
    </noscript>
</div>

<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
    (function () {
        const widget_id = 'OoDfYa80AO';
        const d = document;
        const w = window;

        function l() {
            const s = document.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = '//code.jivosite.com/script/widget/' + widget_id;
            const ss = document.getElementsByTagName('script')[0];
            ss.parentNode.insertBefore(s, ss);
        }

        if (d.readyState === 'complete') {
            l();
        } else {
            if (w.attachEvent) {
                w.attachEvent('onload', l);
            } else {
                w.addEventListener('load', l, false);
            }
        }
    })();
</script>
<!-- {/literal} END JIVOSITE CODE -->

<?php
/* close tags from wp-content/themes/tol-parent/layouts/header-migesco.php */
echo '</body></html>';
