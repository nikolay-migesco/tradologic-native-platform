<?php
add_action('wp_enqueue_scripts','dequeue_simplex');
add_action('wp_enqueue_scripts','dequeue_binary');

setcookie('game', 'forex', 0, '/');
?>
<script type="text/javascript">
    $(document).ready(function () {
        $.cookie('tradersTrend', 'on', {path: "/"});
    });
</script>
<script>
    jQuery(function ($) {
        $(".ui-draggable").draggable();
        migesco.attachDraggableToNewNodes();
    });
</script>
<div data-widget="forex-forex" data-type="toolsmenu" class="tradepanel" id="tradePanel">
    <div class="tradepanel-control-bar" data-dojo-attach-point="headNode">
        <div data-widget="forex-forex" data-link="boxes" data-isactive="true" class="assets-boxes active"><span data-translate="box-view"></span></div>
        <div data-widget="forex-forex" data-link="list" data-isactive="false" class="assets-list"><span data-translate="list-view"></span></div>
    </div>
    <div class="tradepanel-control-bar assetslist-tools" >
        <div data-widget="forex-forex" data-type="assetstype" class="tradepanel-title assetstype">
            <span data-translate="All"></span>
        </div>
        <div data-type="searchcontainer" class="tradepanel-control-bar asset-search-bar">
            <input data-widget="forex-forex" data-type="searchinput" type="text" class="input-assetssearch" placeholder="" data-translate="search"/>
            <span class="list-icon has-tooltip svg-icon-holder" no-blur-id="instruments-list" data-tooltip-message="label.add-instrument" data-tooltip-orientation="rightCenter"></span>
        </div>
        <span class="asset-table-settings-icon column-settings" data-widget="forex-forex" data-type="widgetsettings">
            <div class="settings">
                <div class="asset-table-settings-outer">
                    <div class="asset-table-settings-content">
                        <div class="asset-table-settings-title" data-translate="asset-settings"></div>
                        <div class="row-settings row-settings-oneclick">
                            <div class="one-click-trading-title">
                                <span data-translate="asset-settings-one"></span>
                                <span class="info-icon" data-toggle="tooltip" data-placement="bottom" title="One-Click Trading allows you to create orders with the click of a single button in Box View mode, without the need of additional confirmation."></span>
                            </div>
                            <button class="close-button-tick"></button>
                            <div class="onoffswitch">
                                <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" data-widget="forex-forex" data-type="oneclicktrade" id="myonoffswitch">
                                <label class="onoffswitch-label" for="myonoffswitch">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                </label>
                            </div>
                        </div>
                        <div class="row-settings">
                            <div class="one-click-trading-title">
                                <span data-translate="asset-settings-two"></span>
                                <span class="info-icon" data-toggle="tooltip" data-placement="bottom" title="Traders Trend represents the Sell and Buy sentiment among the investors."></span>
                            </div>
                            <button class="close-button-tick"></button>
                            <div class="onoffswitch">
                                <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" data-widget="forex-forex" data-type="traderstrend" id="traderstrend">
                                <label class="onoffswitch-label" for="traderstrend">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                </label>
                            </div>
                        </div>
                        <div class="row-settings">
                            <div class="one-click-trading-title">
                                <span data-translate="asset-settings-three"></span>
                                <span class="info-icon" data-toggle="tooltip" data-placement="bottom" title="Shows the current spread(the difference between Ask and Bid price) of the instrument. When disabled(off), shows the daily change of the instrument."></span>
                            </div>
                            <button class="close-button-tick"></button>
                            <div class="onoffswitch">
                                <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" data-widget="forex-forex" data-type="dailypercent" id="dailypercent">
                                <label class="onoffswitch-label" for="dailypercent">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </span>
    </div>
    <div data-type="optionscontainer" class="tradepanel-control-bar"  style="display: none">
        <div data-type="searchoptions" class="col-xs-12 col-sm-12 assetssearch-options-container" data-translate="All"></div>
    </div>
    <div class="row assetslist-optionscontainer" data-type="optionscontainer" style="display: none"></div>
    <div class="tradepanel-header-bar" >
        <span class="tradepanel-instrument-label" data-translate="instrument"></span>
        <span class="tradepanel-sell-label" data-translate="sell"></span>
        <span class="tradepanel-buy-label" data-translate="buy"></span>
    </div>
    <div class="scrollable-area scrollable-area-at-top scrollable-area-active asset-table-left">
        <div class="scrollable-area-body">
            <div class="scrollable-area-content">
                <div  data-widget="forex-forex" data-type="assetsBoxes" class="assetsview-boxes" style="display: none;"></div>
                <div data-widget="forex-forex" data-type="assetsList" class="assetsview-list"></div>
            </div>
        </div>
    </div>
    <div class="separator vertical-separator trade-separator ui-resizable-handle ui-resizable-e" id="tradeSeparator" style="left: 320px; height: 100%;">
        <span class="separator-collapse">
            <span class="separator-arrow"></span>
        </span>
    </div>
</div>
<div class="panel chartpanel disable_remove tabs-mode">
    <div id="tv_chart_container"></div>
</div>
<div class="account panel orders-trades-table-wrapper positions-active" id="accountPanel">
    <div class="tabHolder">
        <a href="#trades" aria-controls="orders" role="tab" data-toggle="tab">
            <span class="tab-item tabpositions has-tooltip tab-active">
                <span data-translate="openPositions"></span>
               <div id="forex-orders-count" class="counter active">0</div>
            </span>
        </a>
        <a href="#orders" aria-controls="orders" role="tab" data-toggle="tab">
            <span class="tab-item taborders has-tooltip">
                <span data-translate="pendingOrders"></span>
                <span class="counter">0</span>
            </span>
        </a>
        <div data-widget="forex-openPositions" data-type="closeAllBtn" class="closeAll positions button disabled" data-translate="closeAllOpenPositions"></div>
        <div data-widget="forex-pendingOrders" data-type="closeAllBtn" class="closeAll orders button disabled" data-translate="closeAllPendingOrders" style="display: none"></div>
        <span class="column-settings tabpositions">
            <div class="settings"></div>
            <div class="contextmenu multiple-selection datatable-contextmenu _focusable leftCenter" tabindex="-1">
                <div class="multiple-selection-content">
                    <div class="item item-datatable-contextmenu-name selected" data-column="1">
                        <span data-translate="positionId"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-quantity selected" data-column="3">
                        <span data-translate="quantity"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-averagePrice selected" data-column="5">
                        <span data-translate="forexTable_rate"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-currentPrice selected" data-column="6">
                        <span data-translate="marketRate"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-limitPrice selected" data-column="7">
                        <span data-translate="takeProfit-short"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-stopPrice selected" data-column="8">
                        <span data-translate="stopLoss-short"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-margin selected" data-column="9">
                        <span data-translate="margin"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-swap" data-column="10">
                        <span data-translate="swap"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-ppl selected" data-column="11">
                        <span data-translate="commission"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-created" data-column="12">
                        <span data-translate="forexTable_created"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-ppl selected" data-column="13">
                        <span data-translate="copiedFrom"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-ppl selected" data-column="14">
                        <span data-translate="result"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="btn-wrapper navigation">
                        <div class="nav-button defaultSettings" data-translate="defaultSettings"></div>
                    </div>
                </div>
            </div>
        </span>
        <span class="column-settings taborders">
            <div class="settings"></div>
            <div class="contextmenu multiple-selection datatable-contextmenu _focusable leftCenter" tabindex="-1">
                <div class="multiple-selection-content">
                    <div class="item item-datatable-contextmenu-name" data-column="1">
                        <span data-translate="orderId"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-direction selected" data-column="3">
                        <span data-translate="direction"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-quantity selected" data-column="5">
                        <span data-translate="quantity"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-averagePrice selected" data-column="6">
                        <span data-translate="rate"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-marketrate selected" data-column="7">
                        <span data-translate="marketrate"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-orderDate selected" data-column="8">
                        <span data-translate="forexTable_created"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="btn-wrapper navigation">
                        <div class="nav-button defaultSettings" data-translate="defaultSettings"></div>
                    </div>
                </div>
            </div>
        </span>
    </div>
    <div class="tab-content" id="real-forex-tab-content">
        <div role="tabpanel" class="tab-pane active" id="trades">
            <div class="dataTable">
                <table class="fake-table">
                    <thead>
                    <tr>
                        <th data-column="positionId" data-sort="int" class="name canSort canResize canDrag">
                            <div class="cellWrapper">
                                <span class="content" data-translate="positionid"></span>
                            </div>
                        </th>
                        <th data-column="asset" data-sort="string" class="name canSort canResize canDrag">
                            <div class="cellWrapper">
                                <span class="content" data-translate="asset"></span>
                            </div>
                        </th>
                        <th data-column="quantity" data-sort="int" class="name canSort canResize canDrag">
                            <div class="cellWrapper">
                                <span class="content" data-translate="quantity"></span>
                            </div>
                        </th>
                        <th data-column="direction" data-sort="string" class="name canSort canResize canDrag">
                            <div class="cellWrapper">
                                <span class="content" data-translate="direction"></span>
                            </div>
                        </th>
                        <th data-column="rate" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper">
                                <span class="content" data-translate="forexTable_rate"></span>
                            </div>
                        </th>
                        <th data-column="marketRate" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper">
                                <span class="content" data-translate="marketRate"></span>
                            </div>
                        </th>
                        <th data-column="takeProfit" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper">
                                <span class="content" data-translate="takeProfit-short"></span>
                            </div>
                        </th>
                        <th data-column="stopLoss" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper">
                                <span class="content" data-translate="stopLoss-short"></span>
                            </div>
                        </th>
                        <th data-column="margin" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper">
                                <span class="content" data-translate="margin"></span>
                            </div>
                        </th>
                        <th data-column="swap" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper">
                                <span class="content" data-translate="swap"></span>
                            </div>
                        </th>
                        <th data-column="commission" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper">
                                <span class="content" data-translate="commission"></span>
                            </div>
                        </th>
                        <th data-column="orderDate" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper">
                                <span class="content" data-translate="forexTable_created"></span>
                            </div>
                        </th>
                        <th data-column="copied-from" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper">
                                <span class="content" data-translate="copiedFrom"></span>
                            </div>
                        </th>
                        <th data-column="result" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper">
                                <span class="content" data-translate="result"></span>
                            </div>
                        </th>
                        <th data-column="actions" data-sort="string" class="name canSort canResize canDrag">
                            <div class="cellWrapper">
                                <span class="content"></span>
                            </div>
                        </th>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="scrollable-area scrollable-area-at-top">
                <div class="scrollable-area-body">
                    <div class="scrollable-area-content">
                        <div class="dataTable">
                            <table id="forexProOpenTradesTable" class="hasArrows" data-widget="forex-openPositions">
                                <thead data-widget="forex-openPositions" data-type="header" class="hide-thead">
                                <tr>
                                    <th data-column="positionId" data-sort="int" class="name canSort canResize canDrag">
                                        <div class="cellWrapper">
                                            <span class="content" data-translate="positionId"></span>
                                        </div>
                                    </th>
                                    <th data-column="asset" data-sort="string" class="name canSort canResize canDrag">
                                        <div class="cellWrapper">
                                            <span class="content" data-translate="instrument"></span>
                                        </div>
                                    </th>
                                    <th data-column="quantity" data-sort="int" class="name canSort canResize canDrag">
                                        <div class="cellWrapper">
                                            <span class="content" data-translate="quantity"></span>
                                        </div>
                                    </th>
                                    <th data-column="direction" data-sort="string" class="name canSort canResize canDrag">
                                        <div class="cellWrapper">
                                            <span class="content" data-translate="direction"></span>
                                        </div>
                                    </th>
                                    <th data-column="rate" data-sort="float" class="name canSort canResize canDrag">
                                        <div class="cellWrapper">
                                            <span class="content" data-translate="rate"></span>
                                        </div>
                                    </th>
                                    <th data-column="marketRate" data-sort="float" class="name canSort canResize canDrag">
                                        <div class="cellWrapper">
                                            <span class="content" data-translate="marketRate"></span>
                                        </div>
                                    </th>
                                    <th data-column="takeProfit" data-sort="float" class="name canSort canResize canDrag">
                                        <div class="cellWrapper">
                                            <span class="content" data-translate="takeProfit-short"></span>
                                        </div>
                                    </th>
                                    <th data-column="stopLoss" data-sort="float" class="name canSort canResize canDrag">
                                        <div class="cellWrapper">
                                            <span class="content" data-translate="stopLoss-short"></span>
                                        </div>
                                    </th>
                                    <th data-column="margin" data-sort="float" class="name canSort canResize canDrag">
                                        <div class="cellWrapper">
                                            <span class="content" data-translate="margin"></span>
                                        </div>
                                    </th>
                                    <th data-column="swap" data-sort="float" class="name canSort canResize canDrag">
                                        <div class="cellWrapper">
                                            <span class="content" data-translate="swap"></span>
                                        </div>
                                    </th>
                                    <th data-column="commission" data-sort="float" class="name canSort canResize canDrag">
                                        <div class="cellWrapper">
                                            <span class="content" data-translate="commission"></span>
                                        </div>
                                    </th>
                                    <th data-column="orderDate" data-sort="float" class="name canSort canResize canDrag">
                                        <div class="cellWrapper">
                                            <span class="content" data-translate="orderDate"></span>
                                        </div>
                                    </th>
                                    <th data-column="copied-from" data-sort="float" class="name canSort canResize canDrag">
                                        <div class="cellWrapper">
                                            <span class="content" data-translate="copiedFrom"></span>
                                        </div>
                                    </th>
                                    <th data-column="result" data-sort="float" class="name canSort canResize canDrag">
                                        <div class="cellWrapper">
                                            <span class="content" data-translate="result"></span>
                                        </div>
                                    </th>
                                    <th data-column="actions" data-sort="string" class="name canSort canResize canDrag">
                                        <div class="cellWrapper">
                                            <span class="content" data-translate="actions"></span>
                                        </div>
                                    </th>
                                </tr>
                                </thead>
                                <tbody data-widget="forex-openPositions" data-type="table"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="orders">
            <div class="dataTable">
                <table class="fake-table">
                    <thead>
                    <tr data-type="header" >
                        <th data-column="orderId" data-sort="int" class="name canSort canResize canDrag">
                            <div class="cellWrapper">
                                <span class="content" data-translate="orderId"></span>
                            </div>
                        </th>
                        <th data-column="asset" data-sort="string" class="name canSort canResize canDrag">
                            <div class="cellWrapper">
                                <span class="content" data-translate="asset"></span>
                            </div>
                        </th>
                        <th data-column="direction" data-sort="string" class="name canSort canResize canDrag">
                            <div class="cellWrapper">
                                <span class="content" data-translate="direction"></span>
                            </div>
                        </th>
                        <th data-column="type" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper">
                                <span class="content" data-translate="type"></span>
                            </div>
                        </th>
                        <th data-column="quantity" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper">
                                <span class="content" data-translate="quantity"></span>
                            </div>
                        </th>
                        <th data-column="rate" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper">
                                <span class="content" data-translate="rate"></span>
                            </div>
                        </th>
                        <th data-column="marketrate" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper">
                                <span class="content" data-translate="marketrate"></span>
                            </div>
                        </th>
                        <th data-column="orderDate" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper">
                                <span class="content" data-translate="forexTable_created"></span>
                            </div>
                        </th>
                        <th data-column="actions" data-sort="string" class="name canSort canResize canDrag">
                            <div class="cellWrapper">
                                <span class="content"></span>
                            </div>
                        </th>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="scrollable-area scrollable-area-at-top">
                <div class="scrollable-area-body">
                    <div class="scrollable-area-content">
                        <div class="dataTable">
                            <div class="forexPendingOrders">
                                <table id="real-forex-orders-table" class="hasArrows" data-widget="forex-pendingOrders">
                                    <thead class="hide-thead">
                                    <tr data-type="header" >
                                        <th data-column="orderId" data-sort="int" class="name canSort canResize canDrag">
                                            <div class="cellWrapper">
                                                <span class="content" data-translate="orderId"></span>
                                            </div>
                                        </th>
                                        <th data-column="asset" data-sort="string" class="name canSort canResize canDrag">
                                            <div class="cellWrapper">
                                                <span class="content" data-translate="asset"></span>
                                            </div>
                                        </th>
                                        <th data-column="direction" data-sort="string" class="name canSort canResize canDrag">
                                            <div class="cellWrapper">
                                                <span class="content" data-translate="direction"></span>
                                            </div>
                                        </th>
                                        <th data-column="type" data-sort="float" class="name canSort canResize canDrag">
                                            <div class="cellWrapper">
                                                <span class="content" data-translate="type"></span>
                                            </div>
                                        </th>
                                        <th data-column="quantity" data-sort="float" class="name canSort canResize canDrag">
                                            <div class="cellWrapper">
                                                <span class="content" data-translate="quantity"></span>
                                            </div>
                                        </th>
                                        <th data-column="rate" data-sort="float" class="name canSort canResize canDrag">
                                            <div class="cellWrapper">
                                                <span class="content" data-translate="rate"></span>
                                            </div>
                                        </th>
                                        <th data-column="marketrate" data-sort="float" class="name canSort canResize canDrag">
                                            <div class="cellWrapper">
                                                <span class="content" data-translate="marketrate"></span>
                                            </div>
                                        </th>
                                        <th data-column="orderDate" data-sort="float" class="name canSort canResize canDrag">
                                            <div class="cellWrapper">
                                                <span class="content" data-translate="forexTable_created"></span>
                                            </div>
                                        </th>
                                        <th data-column="actions" data-sort="string" class="name canSort canResize canDrag">
                                            <div class="cellWrapper">
                                                <span class="content"></span>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody data-widget="forex-pendingOrders" data-type="table"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="sidepanel-tabs focusable" id="sidePanel" tabindex="-1">
    <ul class="nav right-sidebar-nav">
        <?php if (ENABLE_FOREX_SOCIAL) { ?>
            <li class="social-tab active">
                <a href="#tab1" data-toggle="tab" class="active"></a>
            </li>
        <?php } ?>
        <li class="messages-tab <?php echo(ENABLE_FOREX_SOCIAL ? '' : 'active'); ?>">
            <a href="#tab3" data-toggle="tab" class="<?php echo(ENABLE_FOREX_SOCIAL ? '' : 'active'); ?>"></a>
        </li>
        <?php if (ENABLE_MTE) { ?>
            <li class="daily-video-tab">
                <a href="#daily-video" data-toggle="tab" class=""></a>
            </li>
            <li class="signals-tab">
                <a href="#signals" data-toggle="tab" class=""></a>
            </li>
            <li class="economic-calendar-tab">
                <a href="#economic-calendar" data-toggle="tab" class=""></a>
            </li>
            <li class="market-analysis-tab">
                <a href="#market-analysis" data-toggle="tab" class=""></a>
            </li>
        <?php } ?>
    </ul>
    <div class="tab-content">
        <?php if (ENABLE_FOREX_SOCIAL) { ?>
            <div class="tab-pane active in" id="tab1">
                <div class="tol-social-sidebar" data-widget="forex-forexTraders">
                    <h1 class="social-title" data-translate="userDataSocial"></h1>
                    <div class="search-part-social">
                        <input data-widget="forex-forexTraderSearch" data-type="search" type="text" placeholder="Trader Name" class="input-tradersearch" />
                        <div data-widget="forex-forexTraderSearch" data-type="options" class="tradersearch-dropdown" style="display:none"></div>
                        <span class="tradersearch-button" data-toggle="modal" data-target="#tol-explore-popup">Explore</span>
                    </div>
                    <div class="content-part-social">
                        <ul class="nav content-part-social">
                            <li class="most-profit-tab active">
                                <a href="#tab11" data-toggle="tab" class="active">Most Profitable</a>
                            </li>
                            <li class="most-copied-tab">
                                <a href="#tab12" data-toggle="tab">Most Copied</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane dataTable active in" id="tab11">
                                <table class="traderoom-social table-mostprofitable hasArrows">
                                    <thead class="fake-thead">
                                    <tr>
                                        <th data-column="avatar" data-sort="string" class="canSort canResize canDrag tarder-avatar"><div class="cellWrapper"><span class="content">Name</span></span></div></th>
                                        <th data-column="name" data-sort="string" class="canSort canResize canDrag tarder-name"><div class="cellWrapper"><span class="content"></span></div></th>
                                        <th data-column="plperc" data-sort="float" class="name canSort canResize canDrag" aria-sort="descending"><div class="cellWrapper"><span class="content">P/L(%)</span></div></th>
                                        <th data-column="copies" data-sort="int" class="name canSort canResize canDrag"><div class="cellWrapper"><span class="content">Copiers</span></div></th>
                                        <th data-column="action" data-sort="string" class="name canSort canResize canDrag"><div class="cellWrapper"><span class="content">Copy</span></div></th>
                                    </tr>
                                    </thead>
                                </table>
                                <div class="scrollable-social">
                                    <div class="scrollable-area-body">
                                        <div class="scrollable-area-content">
                                            <div class="clearfix flat-top-borders">
                                                <table class="traderoom-social table-mostprofitable hasArrows traders-head" data-widget="forex-forexTraders" data-type="mostprofitable-table">
                                                    <thead data-type="mostprofitable-header">
                                                    <tr>
                                                        <th data-column="avatar" data-sort="string" class="canSort canResize canDrag tarder-avatar">
                                                            <div class="cellWrapper">
                                                                <span class="content">Name</span>
                                                                <span class="sortArrow"></span>
                                                                <span class="sortBackground"></span>
                                                            </div>
                                                        </th>
                                                        <th data-column="name" data-sort="string" class="canSort canResize canDrag tarder-name">
                                                            <div class="cellWrapper">
                                                                <span class="content"></span>
                                                                <span class="sortArrow"></span>
                                                                <span class="sortBackground"></span>
                                                            </div>
                                                        </th>
                                                        <th data-column="plperc" data-sort="float" class="name canSort canResize canDrag" aria-sort="descending">
                                                            <div class="cellWrapper">
                                                                <span class="content">P/L(%)</span>
                                                                <span class="sortArrow"></span>
                                                                <span class="sortBackground"></span>
                                                            </div>
                                                        </th>
                                                        <th data-column="copies" data-sort="int" class="name canSort canResize canDrag">
                                                            <div class="cellWrapper">
                                                                <span class="content">Copies</span>
                                                                <span class="sortArrow"></span>
                                                                <span class="sortBackground"></span>
                                                            </div>
                                                        </th>
                                                        <th data-column="action" data-sort="string" class="name canSort canResize canDrag">
                                                            <div class="cellWrapper">
                                                                <span class="content">Copy</span>
                                                                <span class="sortArrow"></span>
                                                                <span class="sortBackground"></span>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane dataTable fade" id="tab12">
                                <table class="traderoom-social table-mostprofitable hasArrows">
                                    <thead class="fake-thead">
                                    <tr>
                                        <th data-column="avatar" data-sort="string" class="canSort canResize canDrag tarder-avatar"><div class="cellWrapper"><span class="content">Name</span></span></div></th>
                                        <th data-column="name" data-sort="string" class="canSort canResize canDrag tarder-name"><div class="cellWrapper"><span class="content"></span></div></th>
                                        <th data-column="plperc" data-sort="float" class="name canSort canResize canDrag" aria-sort="descending"><div class="cellWrapper"><span class="content">P/L(%)</span></div></th>
                                        <th data-column="copies" data-sort="int" class="name canSort canResize canDrag"><div class="cellWrapper"><span class="content">Copiers</span></div></th>
                                        <th data-column="action" data-sort="string" class="name canSort canResize canDrag"><div class="cellWrapper"><span class="content">Copy</span></div></th>
                                    </tr>
                                    </thead>
                                </table>
                                <div class="scrollable-social">
                                    <div class="scrollable-area-body">
                                        <div class="scrollable-area-content">
                                            <div class="clearfix flat-top-borders">
                                                <table class="traderoom-social table-mostcopied hasArrows traders-head" data-widget="forex-forexTraders" data-type="mostcopied-table">
                                                    <thead data-type="mostcopied-header">
                                                    <tr>
                                                        <th data-column="avatar" data-sort="string" class="canSort canResize canDrag trader-avatar">
                                                            <div class="cellWrapper">
                                                                <span class="content">Name</span>
                                                                <span class="sortArrow"></span>
                                                                <span class="sortBackground"></span>
                                                            </div>
                                                        </th>
                                                        <th data-column="name" data-sort="string" class="canSort canResize canDrag">
                                                            <div class="cellWrapper">
                                                                <span class="content"></span>
                                                                <span class="sortArrow"></span>
                                                                <span class="sortBackground"></span>
                                                            </div>
                                                        </th>
                                                        <th data-column="plperc" data-sort="float" class="name canSort canResize canDrag">
                                                            <div class="cellWrapper">
                                                                <span class="content">P/L(%)</span>
                                                                <span class="sortArrow"></span>
                                                                <span class="sortBackground"></span>
                                                            </div>
                                                        </th>
                                                        <th data-column="copies" data-sort="int" class="name canSort canResize canDrag" aria-sort="descending">
                                                            <div class="cellWrapper">
                                                                <span class="content">Copies</span>
                                                                <span class="sortArrow"></span>
                                                                <span class="sortBackground"></span>
                                                            </div>
                                                        </th>
                                                        <th data-column="action" data-sort="string" class="name canSort canResize canDrag">
                                                            <div class="cellWrapper">
                                                                <span class="content">Copy</span>
                                                                <span class="sortArrow"></span>
                                                                <span class="sortBackground"></span>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>

        <div class="tab-pane fade" id="tab3">
            <div class="tol-messages-sidebar">
                <h1 class="messages-title" data-translate="userDataMessages"></h1>
                <div class="scrollable-area scrollable-area-at-top">
                    <div class="scrollable-area-body">
                        <div class="scrollable-area-content">
                            <div class="clearfix flat-top-borders">
                                <?php echo do_shortcode('[messageInboxAllMessages]'); ?>
                                <?php echo do_shortcode('[messageInboxContent]'); ?>
                                <div class="pagination-wrapper">
                                    <?php echo do_shortcode('[messageInboxPaging]'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if (ENABLE_MTE) { ?>
            <script src="//mte-media.com/slmloaders/apilib\/xmteapi.js"></script>
            <div id="tol-mte-news" class="tab-pane fade">
                <header class="entry-header">
                    <h1 class="entry-title news-title">News</h1>
                </header>
                <div class="scrollable-area scrollable-area-at-top">
                    <div class="scrollable-area-body">
                        <div class="scrollable-area-content">
                            <div id="mte-news-container">
                                <?php include( ABSPATH . 'wp-content/themes/tol-parent/layouts/MTE/traderoom-mte/mte-news.php' ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="daily-video" class="tab-pane fade">
                <header class="entry-header">
                    <h1 class="entry-title news-title">Daily Videos</h1>
                </header>
                <div class="scrollable-area scrollable-area-at-top">
                    <div class="scrollable-area-body">
                        <div class="scrollable-area-content">
                            <div id="daily-video-container">
                                <?php include( ABSPATH . 'wp-content/themes/tol-parent/layouts/MTE/traderoom-mte/daily-video.php' ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="signals" class="tab-pane fade">
                <header class="entry-header">
                    <h1 class="entry-title news-title">Signals</h1>
                </header>
                <div class="scrollable-area scrollable-area-at-top">
                    <div class="scrollable-area-body">
                        <div class="scrollable-area-content">
                            <div id="signals-container">
                                <?php include( ABSPATH . 'wp-content/themes/tol-parent/layouts/MTE/traderoom-mte/signals.php' ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="economic-calendar" class="tab-pane fade">
                <header class="entry-header">
                    <h1 class="entry-title news-title">Economic calendar</h1>
                </header>
                <div id="economic-calendar-container">
                    <!-- Economic Calendar-->
                    <!-- Day tabs -->
                    <ul class="nav nav-tabs nav-justified" id="sortingByDays" role="tablist">
                        <li class="nav-item all active">
                            <a class="nav-link active" id="all-tab" data-toggle="tab" href="#all" role="tab" aria-controls="all" aria-selected="true">all</a>
                        </li>
                        <li class="nav-item monday">
                            <a class="nav-link" id="monday-tab" data-toggle="tab" href="#monday" role="tab" aria-controls="monday" aria-selected="false">mon</a>
                        </li>
                        <li class="nav-item tuesday">
                            <a class="nav-link" id="tuesday-tab" data-toggle="tab" href="#tuesday" role="tab" aria-controls="tuesday" aria-selected="false">tue</a>
                        </li>
                        <li class="nav-item wednesday">
                            <a class="nav-link" id="wednesday-tab" data-toggle="tab" href="#wednesday" role="tab" aria-controls="wednesday" aria-selected="false">wed</a>
                        </li>
                        <li class="nav-item thursday">
                            <a class="nav-link" id="thursday-tab" data-toggle="tab" href="#thursday" role="tab" aria-controls="thursday" aria-selected="false">thu</a>
                        </li>
                        <li class="nav-item friday">
                            <a class="nav-link" id="friday-tab" data-toggle="tab" href="#friday" role="tab" aria-controls="friday" aria-selected="false">fri</a>
                        </li>
                    </ul>
                    <span class="calendar_btn" type="button" data-toggle="collapse" data-target="#infoCalendarTraderoom"></span>
                    <div id="infoCalendarTraderoom" class="collapse">
                        <div class="impact-container">
                            <div id="impact3" class="choose-importance marked col-importace"><iron-icon class="importance-icon style-scope mte-calendar-event x-scope iron-icon-1" id="importanceIcon"><svg viewBox="0 0 24 24" preserveAspectRatio="xMidYMid meet" class="style-scope iron-icon" style="pointer-events: none; display: block; width: 100%; height: 100%;"><g class="style-scope iron-icon"><path d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z" class="style-scope iron-icon"></path></g></svg></iron-icon></div>
                            <div id="impact2" class="choose-importance marked col-importace"><iron-icon class="importance-icon style-scope mte-calendar-event x-scope iron-icon-1" id="importanceIcon"><svg viewBox="0 0 24 24" preserveAspectRatio="xMidYMid meet" class="style-scope iron-icon" style="pointer-events: none; display: block; width: 100%; height: 100%;"><g class="style-scope iron-icon"><path d="M22 9.74l-7.19-.62L12 2.5 9.19 9.13 2 9.74l5.46 4.73-1.64 7.03L12 17.77l6.18 3.73-1.63-7.03L22 9.74zM12 15.9V6.6l1.71 4.04 4.38.38-3.32 2.88 1 4.28L12 15.9z" class="style-scope iron-icon"></path></g></svg></iron-icon></div>
                            <div id="impact1" class="choose-importance marked col-importace"><iron-icon class="importance-icon style-scope mte-calendar-event x-scope iron-icon-1" id="importanceIcon"><svg viewBox="0 0 24 24" preserveAspectRatio="xMidYMid meet" class="style-scope iron-icon" style="pointer-events: none; display: block; width: 100%; height: 100%;"><g class="style-scope iron-icon"><path d="M22 9.24l-7.19-.62L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21 12 17.27 18.18 21l-1.63-7.03L22 9.24zM12 15.4l-3.76 2.27 1-4.28-3.32-2.88 4.38-.38L12 6.1l1.71 4.04 4.38.38-3.32 2.88 1 4.28L12 15.4z" class="style-scope iron-icon"></path></g></svg></iron-icon></div>
                        </div>
                        <div class="flag-row">
                            <div class="flag-item">
                                <label class="container-checkbox">
                                    <input type="checkbox" name="chooseFlagAud" id="flag-icon-aud" class="flag_checkbox" checked="checked"><span class="checkmark"></span>
                                </label>
                                <div class="flag-image aud"></div>
                                <span class="currency-name">Aud</span>
                            </div>
                            <div class="flag-item">
                                <label class="container-checkbox">
                                    <input type="checkbox" name="chooseFlagCad" id="flag-icon-cad" class="flag_checkbox" checked="checked"><span class="checkmark"></span>
                                </label>
                                <div class="flag-image cad"></div>
                                <span class="currency-name">Cad</span>
                            </div>
                            <div class="flag-item">
                                <label class="container-checkbox">
                                    <input type="checkbox" name="chooseFlagCny" id="flag-icon-cny" class="flag_checkbox" checked="checked"><span class="checkmark"></span>
                                </label>
                                <div class="flag-image cny"></div>
                                <span class="currency-name">CNY</span>
                            </div>
                            <div class="flag-item">
                                <label class="container-checkbox">
                                    <input type="checkbox" name="chooseFlagJpy" id="flag-icon-jpy" class="flag_checkbox" checked="checked"><span class="checkmark"></span>
                                </label>
                                <div class="flag-image jpy"></div>
                                <span class="currency-name">JPY</span>
                            </div>
                            <div class="flag-item">
                                <label class="container-checkbox">
                                    <input type="checkbox" name="chooseFlagNzd" id="flag-icon-nzd" class="flag_checkbox" checked="checked"><span class="checkmark"></span>
                                </label>
                                <div class="flag-image nzd"></div>
                                <span class="currency-name">NZD</span>
                            </div>
                            <div class="flag-item">
                                <label class="container-checkbox">
                                    <input type="checkbox" name="chooseFlagChf" id="flag-icon-chf" class="flag_checkbox" checked="checked"><span class="checkmark"></span>
                                </label>
                                <div class="flag-image chf"></div>
                                <span class="currency-name">CHF</span>
                            </div>
                            <div class="flag-item">
                                <label class="container-checkbox">
                                    <input type="checkbox" name="chooseFlagEur" id="flag-icon-eur" class="flag_checkbox" checked="checked"><span class="checkmark"></span>
                                </label>
                                <div class="flag-image eur"></div>
                                <span class="currency-name">EUR</span>
                            </div>
                            <div class="flag-item">
                                <label class="container-checkbox">
                                    <input type="checkbox" name="chooseFlagGbp" id="flag-icon-gbp" class="flag_checkbox" checked="checked"><span class="checkmark"></span>
                                </label>
                                <div class="flag-image gbp"></div>
                                <span class="currency-name">GBP</span>
                            </div>
                            <div class="flag-item">
                                <label class="container-checkbox">
                                    <input type="checkbox" name="chooseFlagUsd" id="flag-icon-usd" class="flag_checkbox" checked="checked"><span class="checkmark"></span>
                                </label>
                                <div class="flag-image usd"></div>
                                <span class="currency-name">USD</span>
                            </div>
                        </div>
                    </div>
                    <!-- Day Tab panes -->
                    <div class="scrollable-area scrollable-area-at-top">
                        <div class="scrollable-area-body">
                            <div class="scrollable-area-content">
                                <div id="mte-calendar-traderoom">
                                    <?php include( ABSPATH . 'wp-content/themes/tol-parent/layouts/MTE/traderoom-mte/economic-calendar.php' ); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Economic Calendar -->
                </div>
            </div>
            <div id="market-analysis" class="tab-pane fade">
                <header class="entry-header">
                    <h1 class="entry-title news-title">Chart Analysis</h1>
                </header>
                <div class="scrollable-area scrollable-area-at-top">
                    <div class="scrollable-area-body">
                        <div class="scrollable-area-content">
                            <div id="market-analysis-container">
                                <?php include( ABSPATH . 'wp-content/themes/tol-parent/layouts/MTE/traderoom-mte/market-analysis.php' ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<div class="separator horizontal-separator chart-separator" style="top: 693px; left: 325px; width: 1290px;">
    <span class="separator-collapse">
        <span class="separator-arrow"></span>
    </span>
</div>
<div class="separator vertical-separator side-separator">
    <span class="separator-collapse">
        <span class="separator-arrow"></span>
    </span>
</div>
<div class="table-overlay-one" style="display: none;"></div>
<div class="table-overlay-two" style="display: none;"></div>
<!-- START Hub to push the chart prices -->


<!-- Trading View chart library starts here -->
<script data-cfasync="false" type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/tradingview/charting_library/charting_library.min.js"></script>
<script data-cfasync="false" type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/tradingview/datafeeds/datafeed.js"></script>
<script type="text/javascript">
    function initializeChart() {
        $(window).on('loadingIsCompleted', function (event) {
            const waitingForOptions = 100;
            let checkOptions = setTimeout(function waitForOptions (){
                const tradeOptions = forexHelper.options;
                const stillWaiting = tradeOptions.length === 0;
                if (stillWaiting) {
                    checkOptions = setTimeout(waitForOptions,waitingForOptions);
                }
                if (!stillWaiting) {
                    const chartTheme = migesco.chooseTheme();
                    tradingViewWidget = window.tvWidget = new TradingView.widget({
                        theme: chartTheme,
                        autosize: true,
                        symbol: tradeOptions[0].name,
                        interval: 1,
                        container_id: "tv_chart_container",
                        datafeed: new Datafeeds.UDFCompatibleDatafeed("https://advfeed.finte.co/tradingviewservices.asmx", 1000),
                        library_path: "<?php echo get_template_directory_uri(); ?>/tradingview/charting_library/",
                        locale: "<?php echo prior_lang() ?>",
                        drawings_access: {
                            type: 'black',
                            tools: [
                                {
                                    name: "Regression Trend"
                                }
                            ]
                        },
                        disabled_features: [
                            "create_volume_indicator_by_default",
                            "footer_screenshot",
                            "header_screenshot",
                            "header_interval_dialog_button",
                            "header_symbol_search",
                            "symbol_search_hot_key",
                            "border_around_the_chart",
                            "uppercase_instrument_names",
                            "constraint_dialogs_movement",
                        ],
                        enabled_features: [
                            "side_toolbar_in_fullscreen_mode",
                            "caption_buttons_text_if_possible",
                            "lock_visible_time_range_on_resize",
                            "study_templates",
                            "no_min_chart_width",
                            "timeframes_toolbar",
                            "same_data_requery",
                        ],
                        charts_storage_url: 'https://saveload.tradingview.com',
                        charts_storage_api_version: "1.1",
                        supports_search: false,
                        client_id: 'tradingview.com',
                        user_id: 'public_user',
                        overrides: {
                            "timezone": "Europe/Moscow"
                        },
                    });

                    $("#tv_chart_container").parent().addClass('loaded');
                }
            }, waitingForOptions);
        });
    }
    window.addEventListener('DOMContentLoaded', initializeChart, false);

    $(document).ready(function () {
        $('#tol-change-modes').on('hidden.bs.modal', function () {
            $('.body-text #themes, .body-text #modes').removeClass('in').attr('aria-expanded', false);
            $('.settings-row').removeClass('collapsed').attr('aria-expanded', false)
        });

        $('#changeCtrls').on('show.bs.collapse','.collapse', function() {
            $('#changeCtrls').find('.collapse.in').collapse('hide');
        });

        $('.forex-change-mode').on('click', function(){
            if ((document.getElementById("aggregate").checked && widgets.user.forexModeId === 2) || (document.getElementById("hedging").checked && widgets.user.forexModeId == 3)) {
                $('#tol-change-modes').modal('hide');
            } else {
                widgets.api.changeForexMode((document.getElementById("aggregate").checked ? '2' : '3'), function(response){
                    if (response.code === 200) {
                        widgets.api.getUserData([widgets.processUser, function () {
                            $('#tol-change-modes').modal('hide');
                            $('#tol-change-modes-result .forex-asset-name').text(document.getElementById("aggregate").checked ? widgetMessage['change-mode-to-agg-title'] : widgetMessage['change-mode-to-hedg-title']);
                            $('#tol-change-modes-result .body-text').text(helper.getTranslation('change-mode-success','Trading mode has been switched successfully.'));

                            if (bringBackSocialProfile) {
                                forexSocialProfileWidget.initPopup(currentTraderID, 'copy', false);
                                bringBackSocialProfile = false;
                                currentTraderID = null;
                            }
                        }]);
                    } else {
                        $('#tol-change-modes').modal('hide');
                        $('#tol-change-modes-result .forex-asset-name').text(document.getElementById("aggregate").checked ? widgetMessage['change-mode-to-agg-title'] : widgetMessage['change-mode-to-hedg-title']);
                        $('#tol-change-modes-result .body-text').text(document.getElementById("aggregate").checked ? widgetMessage['change-mode-to-agg-fail'] : widgetMessage['change-mode-to-hedg-fail']);
                        $('#tol-change-modes-result').modal('show');
                        if (widgets.user.forexModeId === 3) {
                            document.getElementById("aggregate").checked = false;
                            document.getElementById("hedging").checked = true;
                        } else {
                            document.getElementById("aggregate").checked = true;
                            document.getElementById("hedging").checked = false;
                        }
                    }
                });
            }
        });

        $('.forex-change-theme').on('click', function() {
            $('#themes').removeClass('in');
            $('.settings-row[data-target=#themes]').attr('aria-expanded',false);

            if (document.getElementById("whitetheme").checked) {
                $('body').addClass('white-theme');
                $('label[for=darktheme]').text('Switch to Dark theme');
                $('label[for=whitetheme]').text('White theme');

                migesco.becomeLight(tradingViewWidget);

            } else {
                $('body').removeClass('white-theme');
                $('label[for=darktheme]').text('Dark theme');
                $('label[for=whitetheme]').text('Switch to White theme');

                migesco.becomeDark(tradingViewWidget);
            }
        });


        $('.copy-diff-mode').on('click', function() {
            bringBackSocialProfile = true;
            currentTraderID = $('.copy-diff-mode').parent().data('traderid');
            $('#tol-social-popup').modal('hide');
            $('#tol-change-modes').modal('show');
            $('.settings-row[data-target="#modes"]').click();
        });
    });

    $(window).on('loadingIsCompleted', function(){
        bringBackSocialProfile = false;

        if (widgets.isLogged()) {
            if (widgets.user.forexModeId === 3) {
                document.getElementById("aggregate").checked = false;
                document.getElementById("hedging").checked = true;
            } else {
                document.getElementById("aggregate").checked = true;
                document.getElementById("hedging").checked = false;
            }
        }

        if ($.cookie('theme') === 'light') {
            document.getElementById("darktheme").checked = false;
            document.getElementById("whitetheme").checked = true;
            $('label[for=darktheme]').text('Switch to Dark theme');
            $('label[for=whitetheme]').text('White theme');
        } else {
            $('label[for=darktheme]').text('Dark theme');
            $('label[for=whitetheme]').text('Switch to White theme');
            document.getElementById("darktheme").checked = true;
            document.getElementById("whitetheme").checked = false;
        }
});
</script>
<style>
    #tv_chart_container{visibility:hidden;height:100%;}
    #tv_chart_container iframe {height:100% !important;}
    .panel.loaded{border:0;}
    .panel.loaded #tv_chart_container{visibility:visible}
</style>
<div class="ui-widget-overlay"></div>
