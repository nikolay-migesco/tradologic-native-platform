<div id="tol-myTrades-layout-12" class="container">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <header class="entry-header">
            <h1 class="entry-title"><?php the_title(); ?></h1>
        </header>
        <form action="" class="my-trades-form tol-form-inline">
            <div class="search-bar">
                <div class="row">
                    <div class="form-group serach-bar-el col-xs-12 col-sm-3">
                        <label data-translate="TradeRoom_Assets"></label>
                        <?php echo do_shortcode('[myTradesOptions class="form-control btn-default form-control-md"]'); ?>
                    </div>
                    <div class="form-group serach-bar-el col-xs-6 col-sm-2">
                        <label data-translate="MyOptions_From"></label>
                        <?php echo do_shortcode('[myTradesFromDate  data-past="30" class="form-control nopadding-right"]'); ?>
                    </div>
                    <div class="form-group serach-bar-el col-xs-6 col-sm-2">
                        <label data-translate="MyOptions_To"></label>
                        <?php echo do_shortcode('[myTradesToDate class="form-control nopadding-right"]'); ?>
                    </div>
                    <div class="form-group serach-bar-el col-xs-12 col-sm-3">
                        <label data-translate="copyBoardtrades"></label>
                        <?php echo do_shortcode('[myTradesSocialFilter class="form-control btn-default form-control-md"]'); ?>
                    </div>
                    <div class="form-group serach-bar-el col-xs-12 col-sm-3">
                        <label data-translate="Trades_Volume"></label>
                        <?php echo do_shortcode('[myTradesVolume class="form-control"]'); ?>
                    </div>
                    <div class="form-group serach-bar-el col-xs-12 col-sm-3">
                        <label data-translate="Trades_Profit"></label>
                        <?php echo do_shortcode('[myTradesProfit class="form-control"]'); ?>
                    </div>
                    <div class="form-group serach-bar-el col-xs-12 col-sm-3">
                        <label data-translate="Trades_Total"></label>
                        <?php echo do_shortcode('[myTradesTotalTrades class="form-control"]'); ?>
                    </div>
                    <?php echo do_shortcode('[myTradesSearchButton translate="MyTrades_SearchBtn" class="btn btn-primary col-xs-12"]'); ?>
                </div>
            </div>
            <?php echo do_shortcode('[myTradesTable class="table table-striped my-trades-table"]'); ?>
            <div class="pagination-wrapper text-right">
                <?php echo do_shortcode('[myTradesPaging paginglimit="10" class="pagination pagination-sm"]'); ?>
            </div>
        </form>
    </article>
</div>