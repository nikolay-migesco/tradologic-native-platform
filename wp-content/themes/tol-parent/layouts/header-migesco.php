<?php session_start();

include_once get_template_directory() . '/classes/themehelper.class.php';
include_once(ABSPATH . '/wp-content/plugins/migesco-menu/init.php');
?>
<!DOCTYPE html <?php language_attributes(); ?>>
<?php
echo '<html class="no-js lang-'.ICL_LANGUAGE_CODE.'">';
?>


<head>
    <?php require_once 'header.php'; ?>
    <style>
        div#lang_sel {
            height: 38px !important;
            margin: 1px 19px 1px 20px !important;
        }

        #toggle-menu #menu-item-12500, #toggle-menu #menu-item-5088, #toggle-menu #menu-item-5090 {
            display: none;
        }
    </style>

    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <!-- Metas Page details-->
    <title><?php wp_title(' | ', true, 'right'); ?><?php bloginfo('name'); ?></title>
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!--google font style-->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>

    <!-- Favicons -->
    <link rel="shortcut icon" href="/wp-content/themes/tol-child/images/favicon.ico">
    <link rel="apple-touch-icon" href="/wp-content/themes/tol-child/images/favicon.ico">
    <link rel="apple-touch-icon" sizes="72x72" href="/wp-content/themes/tol-child/images/favicon.ico">
    <link rel="apple-touch-icon" sizes="114x114" href="/wp-content/themes/tol-child/images/favicon.ico">

    <!-- from migesco67 -->
    <?php

    $loader = new ResourceLoader(get_template_directory(),get_template_directory_uri());
    $styleMigesco = $loader->withFileTime('/styles/style-migesco.css');
    $leftMenu = $loader->withFileTime('/styles/left_menu.css');
    $fonts = $loader->withFileTime('/styles/fonts.css');
    $ptsan = $loader->withFileTime('/ptsan.css');

    ?>

    <link rel='stylesheet' id='new-css' href='<?php echo $styleMigesco; ?>' type='text/css' media='all'/>
    <link rel='stylesheet' id='left-menu-css' href='<?php echo $leftMenu; ?>' type='text/css' media='all'/>
    <link rel='stylesheet' id='fonts-css' href='<?php echo $fonts; ?>' type='text/css' media='all'/>
    <link rel='stylesheet' id='ptsans-font' href='<?php echo $ptsan; ?>' type='text/css'
          media='all'/>
    <?php
    unset($loader,$styleMigesco,$leftMenu,$fonts,$ptsan);
    ?>
    <style type="text/css">
        img.alignright {
            float: right;
            margin: 0 0 1em 1em
        }

        img.alignleft {
            float: left;
            margin: 0 1em 1em 0
        }

        img.aligncenter {
            display: block;
            margin-left: auto;
            margin-right: auto
        }

        a img.alignright {
            float: right;
            margin: 0 0 1em 1em
        }

        a img.alignleft {
            float: left;
            margin: 0 1em 1em 0
        }

        a img.aligncenter {
            display: block;
            margin-left: auto;
            margin-right: auto
        }

        .appriseOuter {
            background: #536e96 !important;
            border: 0 !important;
            border-radius: 0 !important;
            box-shadow: none !important;
            border-radius: 5px !important;
        }

        .appriseInner {
            text-shadow: none !important;
            color: white !important;
            font: 15px/20px Tahoma !important;
        }

        .appriseInner a {
            text-shadow: none !important;
            color: white !important;
            font: 15px/20px Tahoma !important;
        }

        .appriseInner span {
            text-shadow: none !important;
            color: white !important;
            font: 15px/20px Tahoma !important;
        }

        .appriseInner button {
            background: white !important;
            color: #406692 !important;
            border: 1px solid #406692 !important;
            box-shadow: none !important;
        }
    </style>
    <?php
    if (ICL_LANGUAGE_CODE == 'en') {
        echo '
<style type="text/css">
.search-form .selectContainer {
    margin-right:40px !important;
}
</style>
';
    }
    wp_head();
    prior_header();
    ?>

    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-43105391-3', 'migesco.com');
        ga('require', 'displayfeatures');
        ga('send', 'pageview');

    </script>

    <script>(function () {
            var _fbq = window._fbq || (window._fbq = []);
            if (!_fbq.loaded) {
                var fbds = document.createElement('script');
                fbds.async = true;
                fbds.src = '//connect.facebook.net/en_US/fbds.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(fbds, s);
                _fbq.loaded = true;
            }
            _fbq.push(['addPixelId', '1549023895310507']);
        })();
        window._fbq = window._fbq || [];
        window._fbq.push(['track', 'PixelInitialized', {}]);
    </script>
    <noscript><img height="1" width="1" alt="" style="display:none"
                   src="https://www.facebook.com/tr?id=1549023895310507&amp;ev=PixelInitialized"/></noscript>

    <script type='text/javascript' src='/wp-content/themes/tol-parent/scripts/jquery.cookie.js'></script>


    <script type="text/javascript">
        $(document).ready(function () {
            $('.header_menu_list li').hover(
                function () {
                    $(this).children('.header_submenu').fadeIn(0);
                    $(this).children('span.header_submenu_connector').css('display', 'block');
                }, function () {
                    $(this).children('.header_submenu').fadeOut(0);
                    $(this).children('span.header_submenu_connector').css('display', 'none');
                }
            );
        });
    </script>
    <script type="text/javascript">
        $(window).on('loadingIsCompleted', function (event, response) {
            if ($.cookie('widgetUserData')) {
                var widgetUserData = JSON.parse($.cookie('widgetUserData'));
                var user_id = widgetUserData.id + '_';
            }

            if ($.cookie('widgetUserData')) {
                if (widgetUserData.isReal) {
                    $('.op-real-account').attr('style', 'display:inline-block !important');
                    $('.op-demo-account').attr('style', 'display:none !important');
                } else {
                    $('.op-demo-account').attr('style', 'display:inline-block !important');
                    $('.op-real-account').attr('style', 'display:none !important');
                }
            }
        });

        $(document).ready(function () {
            if ($.cookie('widgetUserData')) {
                var widgetUserData = JSON.parse($.cookie('widgetUserData'));
                var user_id = widgetUserData.id + '_';
            }

            if ($.cookie('widgetUserData')) {
                if (widgetUserData.isReal) {
                    $('.op-real-account').attr('style', 'display:inline-block !important');
                    $('.op-demo-account').attr('style', 'display:none !important');
                } else {
                    $('.op-demo-account').attr('style', 'display:inline-block !important');
                    $('.op-real-account').attr('style', 'display:none !important');
                }
            }
        });

        $(document).ready(function () {
            $(window).scroll(function (e) {
                var scrolled_int = $(window).scrollLeft();
                $('#masthead').css('left', '-' + scrolled_int + 'px');
            });

            $(document).scroll(function () {
                if ($('body').scrollTop() == 0) {
                    $('#page_header').css('border-bottom', '0');
                } else {
                    $('#page_header').css('border-bottom', '5px solid #D6DCDF');
                }
            });
        });
    </script>

    <script type="text/javascript" src="/wp-content/themes/tol-parent/scripts/jquery.timer.js"></script>
    <script type="text/javascript" src="/wp-content/themes/tol-parent/scripts/countdown.js"></script>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700&subset=cyrillic,cyrillic-ext,latin-ext"
          rel="stylesheet"/>
    <!-- eof from migesco67 -->

</head>
<body id="top" <?php body_class(); ?>>
<div id='div_session_write' style="display:none;"></div>

<?php
echo '<div id="page" class="hfeed site">';
?>

    <header id="masthead" class="site-header" role="banner">
        <div id="page_header">
            <?php get_template_part('element', 'sitehead'); ?>

            <?php migesco_menu(); ?>

            <div class="op-breadcumb-wrapper">

                <?php prior_breadcrumbs(); ?>
            </div>

        </div>
    </header><!-- #masthead -->

    <!-- Prior.lv Fixline -->
    <?php
    if ($prior_submenu = prior_submenu()) {
        echo '
<div style="margin:183px auto 0;width:980px;">
<table width="100%">
	<tbody>
		<tr>
			<td valign="top" style="width:228px; vertical-align: top">'.$prior_submenu.'</td>
			<td valign="top" class="right_content">
				<div class="op-content">
';
    } elseif (get_post_field('post_name', get_post()) == 'vip-usloviya' || get_post_field('post_name', get_post()) == 'become-a-vip' || get_post_field('post_name', get_post()) == 'migesco-priglashaet-na-vebinar' || get_post_field('post_name', get_post()) == 'kak-vybrat-depozit-dlja-torgovli' || get_post_field('post_name', get_post()) == 'how-much-to-deposit-for-trading' || get_post_field('post_name', get_post()) == 'privetstvennyi-bonus' || get_post_field('post_name', get_post()) == 'welcome-bonus' || get_post_field('post_name', get_post()) == 'strahovanie-depozita-sdelki-bez-riska' || get_post_field('post_name', get_post()) == 'insure-your-deposit' || get_post_field('post_name', get_post()) == 'povyshennye-vyplaty' || get_post_field('post_name', get_post()) == 'increased-payout' || get_post_field('post_name', get_post()) == 'kak-zarabatyvat-bolshe-na-binarnyh-opcionah' || get_post_field('post_name', get_post()) == 'how-to-earn-more') {
        echo '<div style="margin:183px auto 0;width:100%;">';
    } elseif ($prior_submenu != prior_submenu() || !is_front_page()) {
        echo '<div style="margin:183px auto 0;width:980px;">';
    }
    ?>

    <?php if (strpos($_SERVER['REQUEST_URI'], '/cashier') !== false) {
        echo '<div style="margin:183px auto 0;width:980px;">';
    }
