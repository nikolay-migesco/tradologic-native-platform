<!DOCTYPE html <?php language_attributes(); ?>>
<!--[if lt IE 7 ]>
<html class="ie ie6 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>
<html class="ie ie7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>
<html class="ie ie9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js lang-<?php echo ICL_LANGUAGE_CODE; ?>">
    <!--<![endif]-->

    <head>
        <?php require_once 'header.php'; ?>

        <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
        <!-- Metas Page details-->
        <title><?php wp_title(' | ', true, 'right'); ?><?php bloginfo('name'); ?></title>
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!--google font style-->
        <link href='//fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>

        <!-- Favicons -->
        <link rel="shortcut icon" href="../images/favicon.ico">
        <link rel="apple-touch-icon" href="../images/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="../images/apple-touch-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="../images/apple-touch-114x114.png">
    </head>
    <body id="top" <?php body_class(); ?>>
        <div id="tol-header-layout-1">
            <!-- LOGIN POP UP -->
            <div id="tol-login-popup" class="modal fade quicklogin" tabindex="-1" role="dialog" aria-labelledby="tol-login-label">
                <div class="modal-dialog modal-sm modal-default" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="tol-login-label">Sign In</h4>
                        </div>
                        <div data-widget="login" data-type="container" data-quick="true" class="modal-body widgetPlaceholder">
                            <div class="form-group">
                                <?php echo do_shortcode('[loginUsername class="form-control"]'); ?>
                            </div>
                            <div class="form-group">
                                <?php echo do_shortcode('[loginPassword class="form-control"]'); ?>
                            </div>

                            <div class="row form-group popup-password">
                                <div class="checkbox savePassWrapper col-xs-12 col-sm-6 nopadding-right">
                                    <label><input type="checkbox" class="savePass"> Remember me</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 nopadding-left text-right">
                                    <a href="<?php echo home_url('/'); ?>forgot-password" data-translate="forgotPassword"></a>
                                </div>
                            </div>                   

                            <div class="form-group">
                                <?php echo do_shortcode('[loginButton class="btn btn-primary form-control"]'); ?>
                            </div>
                            <div class="form-group">
                                <a class="newAccount-submit-link btn btn-default form-control tol-create-account" href="<?php echo home_url('/'); ?>registration">Create an Account</a>
                            </div>
                            <div data-widget="login" data-type="error" style="display: none" class="widgetPlaceholder error-message"></div>
                            <div class="login-success-message success-message" style="display:none;" data-translate="Login_Successful"></div>

                            <div class="row">
                                <div class="col-xs-12 tol-social-logins">
                                    <div data-widget="loginFacebook" onclick="$('#tol-login-popup').modal('hide');" data-type="fbLoginBtn" data-flow="quick" class="loginFacebookWidget tol-social-widgets">
                                        <a href="#" class="btn btn-default"></a>
                                    </div>
                                    <div data-widget="loginFacebook" data-type="popUpElements" data-registration-widgets="title,firstName,lastName,birth,email,primaryPhone,languageCode,countryCode,currency,password,tos" class="loginFacebookWidget registrationPopUp" style="display:none"></div>

                                    <div data-widget="loginGoogle" data-type="googleLoginBtn" data-flow="quick" onclick="$('#tol-login-popup').modal('hide');" id="loginbtn233" class="loginGoogleWidget tol-social-widgets" style="display:none;">
                                        <a href="#" class="btn btn-default"></a>
                                    </div>
                                    <div data-widget="loginGoogle" data-type="popUpElements" data-registration-widgets="email,password,title,firstName,lastName,languageCode,countryCode,birth,primaryPhone,currency,tos" class="loginGoogleWidget registrationPopUp" style="display:none"></div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END LOGIN POP UP -->
             <!-- QUICK DEPOSIT POP UP -->
            <div id="tol-quick-deposit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="tol-quick-deposit-label">
                <div class="modal-dialog modal-md modal-default" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="tol-quick-deposit-label">Deposit</h4>
                        </div>
                        <div class="modal-body widgetPlaceholder">
                            <fieldset class="cashierCardDepositQuick">
                                <div class="bg-danger cc-deposit text-center" data-translate="quickDepositInfo"></div>
                                <div data-widget="cashierCardDepositQuick" data-type="error" class="quickDepositInfo" style="display: none"></div>
                                <div class="cashierCardDepositQuickActiveCC" style="display: none;">
                                    <div class="form-group">
                                        <label data-widget="cashierCardDepositQuick" class="col-sm-4 cashierCardDepositQuickActiveCC" data-translate="cashierLabelChooseAmount"></label>
                                        <input type="text" data-widget="cashierCardDepositQuick" class="form-control cashierCardDepositQuickActiveCC" data-type="amount" data-regexpmessage="Enter valid amount" style="display: none" />
                                    </div>
                                    <div class="form-group">
                                        <label data-widget="cashierCardDepositQuick" class="col-sm-4 cashierCardDepositQuickActiveCC" data-translate="cashierLabelActiveCC"></label>
                                        <select data-widget="cashierCardDepositQuick" data-type="activeCreditCards" class="form-control cashierCardDepositQuickActiveCC" style="display: none"></select>
                                    </div>
                                    <div class="form-group">
                                        <label data-widget="cashierCardDepositQuick" class="col-sm-4" data-translate="cashierLabelCardCvv"></label>
                                        <input type="text" data-widget="cashierCardDepositQuick" class="form-control" data-type="cardCvv" maxlength="4" data-isRequired="true" style="display: none" />
                                    </div>
                                    <div class="form-group text-right">
                                        <span class="quick-cancel-BTN btn btn-danger"><span data-translate="cashierWithdrawCancel"></span></span>
                                        <input type="button" data-widget="cashierCardDepositQuick" data-type="submit" value="Submit Deposit" style="display: none" class="btn btn-primary cashierCardDepositQuickButton" />
                                        <span data-widget="cashierCardDepositQuick" class="bg-warning cc-deposit text-center" data-translate="cashierLabelTryAgain" style="display: none"></span>
                                        <span data-widget="cashierCardDepositQuick" class="bg-success cc-deposit text-center" data-translate="cashierDepositSuccessful" style="display: none"></span><br />
                                        <input type="button" data-widget="cashierCardDepositQuick" data-type="tryagain" value="Try Again" style="display: none" class="btn btn-primary" />
                                    </div>
                                    <div data-widget="cashierCardDepositQuick" data-type="iframeHolder" style="display: none"></div>
                                    <div data-widget="cashierCardDepositQuick" data-type="promoCodeValidation" style="display: none">
                                        <span data-widget="cashierCardDepositQuick" data-translate="cashierLabelPromoCodeValidation"></span><br />
                                        <span data-widget="cashierCardDepositQuick" class="label" data-translate="cashierLabelPromoCode"></span>
                                        <input type="text" data-widget="cashierCardDepositQuick" data-type="promotionalCode" class="promoCode"/>
                                    </div>
                                    <div class="form-group text-center quick-cancel-BTN endDeposit" style="display: none;"><div class="btn btn-primary">OK</div></div>
                                    <div class="goToCashierLink"><label data-translate="wantMoreOptions"></label> <a href="<?php echo home_url('/'); ?>cashier"><span data-translate="goToCashier"></span></a></div>
                                </div>
                                <div class="cashierCardDepositQuickNoCC" style="display: none;">
                                    <span class="quick-cancel-BTN"><div data-translate="cashierWithdrawCancel" class="btn btn-primary btn-danger"></div></span>
                                    <span class="quick-deposit-BTN"><a href="<?php echo home_url('/'); ?>cashier"><div class="btn btn-primary">Go To Cashier</div></a></span>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END QUICK DEPOSIT POP UP -->
            <header id="tol-header">
                <nav id="tol-main-nav" class="navbar navbar-default">
                    <div class="container header">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#slidemenu">
                                <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
                                <span class="icon-bar"></span> <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#">Tradologic</a>

                            <div class="tol-language-switcher">
                                <?php do_action('wpml_add_language_selector'); ?>
                            </div>

                            <!-- SIGN IN NAVIGATION -->
                            <div id="tol-login-area" class="nav navbar-nav navbar-right navbar-btn user-nav tolUserIsNotLogged" style="display:none;">
                                <div class="btn-group" role="group">
                                    <button type="button" class="btn btn-primary" data-translate="UserData_userBlock_Login" data-toggle="modal" data-target="#tol-login-popup"></button>
                                    <button type="button" class="btn btn-default tol-create-account" data-translate="UserData_userBlock_CreateAccount"></button>
                                </div>
                            </div>
                            <!-- END SIGN IN NAVIGATION -->

                            <!-- USER BAR - FOR LOGGED USER -->
                            <ul id="tol-userbar" class="nav navbar-nav user-nav tolUserIsLogged" style="display:none">
                                <li>
                                    <!--<a href="#" class="icon-link"><span class="icon icon-icon1"></span><span class="badge badge-red">6</span></a>-->
                                    <span class="icon-link tol-userbar-inbox">
                                        <span class="icon icon-icon1"></span>
                                        <?php echo do_shortcode('[userBarMessages inboxhref="#"]'); ?>
                                    </span>
                                </li>
                                <li>
                                    <a href="#" class="icon-link dropdown-toggle bonus-icon" data-toggle="dropdown" data-widget="userBar" data-type="bonuses" data-cashback="false" aria-expanded="false">
                                        <span class="icon icon-icon2"></span>
                                        <?php echo do_shortcode('[userBarBonuses data-cashback="true"]'); ?>
                                        <?php echo do_shortcode('[userBarPendingBonuses]'); ?>
                                    </a>
                                    <ul class="dropdown-menu dropdown-bonus">
                                        <li>
                                            <?php echo do_shortcode('[bonusesPopUp class="tol-bonuses" showitexplicitly="true"]'); ?>
                                        </li>
                                    </ul>

                                </li>
                                <li>
                                    <button type="button" class="btn dropdown-toggle dropdown-user" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                        <?php echo do_shortcode('[userBarUserName]'); ?>
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a class="tol-userbar-my-trades" href="#">
                                                <span class="icon icon-icon4"></span>
                                                <span data-translate="userDataMyTrades"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="tol-userbar-my-statistics" href="#">
                                                <span class="icon icon-icon5"></span>
                                                <span data-translate="userDataMyStatistics"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="tol-userbar-my-profile" href="#">
                                                <span class="icon icon-icon6"></span>
                                                <span  data-translate="userDataMyProfile"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="tol-userbar-change-password" href="#">
                                                <span class="icon icon-icon7"></span>
                                                <span  data-translate="userDataChangePassword"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="icon icon-icon8"></span>
                                                <?php echo do_shortcode('[userBarLogout]'); ?>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#" class="icon-link">
                                        <span class="icon icon-icon3"></span>
                                        <?php echo do_shortcode('[userBarBalance]'); ?>
                                    </a>
                                </li>
                                <li>
                                    <button type="button" class="btn btn-primary tol-quick-deposit">Deposit</button>
                                </li>
                            </ul>
                            <!-- END USER BAR- FOR LOGGED USER -->
                        </div>
                    </div>
                    <div class="tol-nav navbar-inverse">
                        <div class="container">
                            <div class="row">
                                <div class="collapse navbar-collapse navbar-collapse" id="slidemenu">
                                    <div class="tolUserIsNotLogged" style="display:none;">
                                        <?php
                                        wp_nav_menu(
                                                array(
                                                    'menu' => 'Primary Menu',
                                                    'theme_location' => 'primary',
                                                    'container' => 'false',
                                                    'menu_class' => 'nav navbar-nav main-nav'
                                                )
                                        );
                                        ?>
                                    </div>

                                    <div class="tolUserIsLogged" style="display:none;">
                                        <?php
                                        wp_nav_menu(
                                                array(
                                                    'menu' => 'Primary Menu (Logged-in)',
                                                    'theme_location' => 'primary_logged',
                                                    'container' => 'false',
                                                    'menu_class' => 'nav navbar-nav main-nav'
                                                )
                                        );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
                <div class="asset-line">
                    <div class="container">
                        <?php echo do_shortcode('[slider]'); ?>
                    </div>
                </div>
            </header>
        </div>
