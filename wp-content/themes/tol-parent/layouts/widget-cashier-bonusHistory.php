<script type="text/javascript">
    $(window).on('cashierLoaded', function () {
        $('.cashier-bonus-history thead th:nth-child(3),.cashier-bonus-history thead th:nth-child(5),.cashier-bonus-history thead th:nth-child(7),.cashier-bonus-history thead th:nth-child(8)').addClass('hidden-xs');
        setTimeout(function(){
            $('.cashier-bonus-history tr td:nth-child(3),.cashier-bonus-history tr td:nth-child(5),.cashier-bonus-history tr td:nth-child(7),.cashier-bonus-history tr td:nth-child(8)').addClass('hidden-xs');
            }, 2000);
    });
</script>
<div id="tol-bonus-history">
    <div class="well well-sm clearfix flat-top-borders">
        <?php echo do_shortcode('[cashierBonusHistory]'); ?>
        <?php echo do_shortcode('[cashierBonusHistoryPaging]'); ?>
    </div>
</div>