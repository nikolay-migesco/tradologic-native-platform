<?php
/**
 * Created by PhpStorm.
 * User: OBenov
 * Date: 24-Oct-17
 * Time: 10:26 AM
 */
 $lang = ICL_LANGUAGE_CODE;
 ?>
 <div class="tol-homepage-layout-99">
    <section class="video-section text-center">
        <div class="container-fluid"> 
                <div class="row justify-content-center">
                    <div class="col-md-12 trade-header text-center">
                        <h1 class="main-header"  data-translate="hp-header" ></h1>
                        <div class="col-md-8 p-container">
                            <p class="header-descriptions text-center" data-translate="hp-header-descriptions"></p>
                        </div>
                    </div>
                    <div class="header-buttons text-center">
                        <a href="../registration" class="btn btn-full notlogged" data-translate="Homepage_Open_Account"></a>
                        <a href="../login" class="btn  btn-half log-in notlogged" data-translate="Login_LogIn"></a>
                        <a href="#" class="btn btn-half logged-in start-trading" data-translate="tradeNow"></a>
                    </div>
                </div>
                <div class="counters row">
                    <div class="col">
                    <h6 class="counter-header">$<span class="count">250</span></h6>
                    <p class="counter-text" data-translate="minimum-deposit"></p>
                    </div>
                    <div class="col">
                    <h6 class="counter-header"><span class="count">150</span>+</h6>
                    <p class="counter-text" data-translate="assets-to-trade"></p>
                    </div>
                    <div class="col">
                    <h6 class="counter-header"><span class="count">100</span>%</h6>
                    <p class="counter-text" data-translate="safe-and-secure"></p>
                    </div>
                    <div class="col">
                    <h6 class="counter-header"><span class="count">85</span>%</h6>
                    <p class="counter-text" data-translate="up-to-profit"></p>
                    </div>
                    <div class="col">
                    <h6 class="counter-header"><span class="count">100</span>%</h6>
                    <p class="counter-text" data-translate="up-to-bonus"></p>
                    </div>
                </div>
                    <!-- <div class="text-center cookies">
                        <p class="pt-4 float-left" data-translate="cookies-msg"></p>
                        <span class="close-btn-cookies"></span>
                    </div> -->
        </div> <!-- Container-fluid -->
    </section> <!-- Navigation -->
    <section class="nav-tab-section"> 
        <div class="container-fluid nav-wrap">
            <!-- Navigation -->
            <ul class="nav nav-tabs nav-justified"  id="nav-tabs-options"> 
                <li class="col text-center">
                    <a  href="#binary-tab" data-toggle="tab"  data-translate="binary-options"></a>
                </li>
                <li class="col text-center">
                    <a  href="#exchange-tab" data-toggle="tab" data-translate="exchange"></a>
                </li>
                <li  class="col text-center current_page_item">
                    <a href="#simplex-tab" data-toggle="tab"  class="active" data-translate="simplex"></a>
                </li>
                <li  class="col text-center">
                    <a  href="#forex-tab" data-toggle="tab" data-translate="forex"></a>
                </li>
                <li  class="col text-center">
                    <a href="#meta-trader-tab" data-toggle="tab" data-translate="meta-trader-5"></a>
                </li>
                <li  class="col text-center">
                    <a  href="#portfolio-tab" data-toggle="tab" data-translate="portfolio"></a>
                </li>
                <li  class="col text-center">
                    <a  href="#crypto-invest-tab" data-toggle="tab" data-translate="crypto-invest"></a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade" id="binary-tab">
                    <div class="row">
                         <div class="col-lg-6 col-md-10 products">
                            <div class="vertical-text" data-translate="our-products"></div>
                            <h3 data-translate="binary-options"></h3>
                            <div class="col-sm-10 decription-container">
                            <p class="descriptions" data-translate="binary-options-description"></p>
                            </div>
                            <a href="#" class="btn start-trading" data-translate="start-trading"></a>
                            </div>
                         <div class="col-lg-6 col-md-12 our-products">
                                <div class="text-in-picture">
                                    <p class="p-first" data-translate="user-friendly"></p>
                                    <p class="p-second" data-translate="user-integrated"></p>
                                    <p class="p-third" data-translate="safe-and-secure"></p>
                                </div>
                            <img src="/wp-content/themes/tol-child/images/new-template-99/simplex_back.png" alt="Simplex" class="img-responsive green-img">
                            <img src="/wp-content/themes/tol-child/images/new-template-99/simplex.png" alt="Simplex" class="img-responsive simplex-img">
                        </div>
                        <div class="col-sm-6"></div>
                    </div>
                </div>
                <div class="tab-pane fade" id="exchange-tab">
                    <div class="row">
                         <div class="col-lg-6 col-md-10 products">
                            <div class="vertical-text" data-translate="our-products"></div>
                            <h3 data-translate="exchange"></h3>
                            <div class="col-sm-10 decription-container">
                            <p class="descriptions" data-translate="exchange-description"></p>
                            </div>
                            <a href="#" class="btn start-trading" data-translate="start-trading"></a>
                            </div>
                         <div class="col-lg-6 col-md-12 our-products">
                            <div class="text-in-picture">
                                <p class="p-first" data-translate="user-friendly"></p>
                                <p class="p-second" data-translate="user-integrated"></p>
                                <p class="p-third" data-translate="safe-and-secure"></p>
                            </div>
                            <img src="/wp-content/themes/tol-child/images/new-template-99/simplex_back.png" alt="Simplex" class="img-responsive green-img">
                            <img src="/wp-content/themes/tol-child/images/new-template-99/simplex.png" alt="Simplex" class="img-responsive simplex-img">
                        </div>
                    </div>
                </div>
               <div class="tab-pane active" id="simplex-tab">
                    <div class="row">
                         <div class="col-lg-6 col-md-10 products">
                            <div class="vertical-text" data-translate="our-products"></div>
                            <h3 data-translate="simplex"></h3>
                            <div class="col-sm-10 decription-container">
                            <p class="descriptions" data-translate="simplex-description"></p>
                            </div>
                            <a href="#" class="btn start-trading" data-translate="start-trading"></a>
                            </div>
                        <div class="col-lg-6 col-md-12 our-products">
                            <div class="text-in-picture">
                                <p class="p-first" data-translate="user-friendly"></p>
                                <p class="p-second" data-translate="user-integrated"></p>
                                <p class="p-third" data-translate="safe-and-secure"></p>
                            </div>
                            <img src="/wp-content/themes/tol-child/images/new-template-99/simplex_back.png" alt="Simplex" class="img-responsive green-img">
                            <img src="/wp-content/themes/tol-child/images/new-template-99/simplex.png" alt="Simplex" class="img-responsive simplex-img">
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="forex-tab">
                    <div class="row">
                         <div class="col-lg-6 col-md-10 products">
                            <div class="vertical-text" data-translate="our-products"></div>
                            <h3 data-translate="forex"></h3>
                            <div class="col-sm-10 decription-container">
                            <p class="descriptions" data-translate="forex-description"></p>
                            </div>
                            <a href="#" class="btn start-trading" data-translate="start-trading"></a>
                            </div>
                         <div class="col-lg-6 col-md-12 our-products">
                            <div class="text-in-picture">
                                <p class="p-first" data-translate="user-friendly"></p>
                                <p class="p-second" data-translate="user-integrated"></p>
                                <p class="p-third" data-translate="safe-and-secure"></p>
                            </div>
                            <img src="/wp-content/themes/tol-child/images/new-template-99/simplex_back.png" alt="Simplex" class="img-responsive green-img">
                            <img src="/wp-content/themes/tol-child/images/new-template-99/simplex.png" alt="Simplex" class="img-responsive simplex-img">
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="meta-trader-tab">
                    <div class="row">
                         <div class="col-lg-6 col-md-10 products">
                            <div class="vertical-text" data-translate="our-products"></div>
                            <h3 data-translate="meta-trader-5"></h3>
                            <div class="col-sm-10 decription-container">
                            <p class="descriptions" data-translate="meta-trader-5-description"></p>
                            </div>
                            <a href="#" class="btn start-trading" data-translate="start-trading"></a>
                            </div>
                        <div class="col-lg-6 col-md-12 our-products">
                            <div class="text-in-picture">
                                <p class="p-first" data-translate="user-friendly"></p>
                                <p class="p-second" data-translate="user-integrated"></p>
                                <p class="p-third" data-translate="safe-and-secure"></p>
                            </div>
                            <img src="/wp-content/themes/tol-child/images/new-template-99/simplex_back.png" alt="Simplex" class="img-responsive green-img">
                            <img src="/wp-content/themes/tol-child/images/new-template-99/simplex.png" alt="Simplex" class="img-responsive simplex-img">
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="portfolio-tab">
                    <div class="row">
                         <div class="col-lg-6 col-md-10 products">
                            <div class="vertical-text" data-translate="our-products"></div>
                            <h3 data-translate="portfolio"></h3>
                            <div class="col-sm-10 decription-container">
                            <p class="descriptions" data-translate="portfolio-description"></p>
                            </div>
                            <a href="#" class="btn start-trading" data-translate="start-trading"></a>
                            </div>
                         <div class="col-lg-6 col-md-12 our-products">
                            <div class="text-in-picture">
                                <p class="p-first" data-translate="user-friendly"></p>
                                <p class="p-second" data-translate="user-integrated"></p>
                                <p class="p-third" data-translate="safe-and-secure"></p>
                            </div>
                            <img src="/wp-content/themes/tol-child/images/new-template-99/simplex_back.png" alt="Simplex" class="img-responsive green-img">
                            <img src="/wp-content/themes/tol-child/images/new-template-99/simplex.png" alt="Simplex" class="img-responsive simplex-img">
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="crypto-invest-tab">
                    <div class="row">
                         <div class="col-lg-6 col-md-10 products">
                            <div class="vertical-text" data-translate="our-products"></div>
                            <h3 data-translate="crypto-invest"></h3>
                            <div class="col-sm-10 decription-container">
                            <p class="descriptions" data-translate="crypto-invest-description"></p>
                            </div>
                            <a href="#" class="btn start-trading" data-translate="start-trading"></a>
                            </div>
                         <div class="col-lg-6 col-md-12 our-products">
                                <div class="text-in-picture">
                                <p class="p-first" data-translate="user-friendly"></p>
                                <p class="p-second" data-translate="user-integrated"></p>
                                <p class="p-third" data-translate="safe-and-secure"></p>
                                </div>
                            <img src="/wp-content/themes/tol-child/images/new-template-99/simplex_back.png" alt="Simplex" class="img-responsive green-img">
                            <img src="/wp-content/themes/tol-child/images/new-template-99/simplex.png" alt="Simplex" class="img-responsive simplex-img">
                        </div>
                    </div>
                </div> 
            </div> <!-- Navigation -->
        </div> <!-- Container-fluid -->
    </section>
    <section class="device-section">
        <div class="container-fluid">
            <div class="col-lg-6 col-md-8 col-sm-12">
                <h1 data-translate="trade-on-any-device"></h1>
                <a href="https://www.apple.com/lae/ios/app-store/"><img src="/wp-content/themes/tol-child/images/new-template-99/app_store.png" alt="" class="img-responsive"></a>
                <a href="https://play.google.com/store?hl=en"><img src="/wp-content/themes/tol-child/images/new-template-99/play_store.png" alt="" class="img-responsive"></a>
            </div>
        </div> <!-- Container-fluid -->
    </section>
    <section class="open-account-section">
        <div class="container-fluid">
            <ul class="nav nav-tabs nav-justified" id="nav-tabs-account">
                <li class="col text-center active">
                    <a  class="active" href="#open-account-tab" data-toggle="tab" data-translate="hp-open-account"></a>
                </li>
                <li class="col text-center">
                    <a  href="#deposit-tab"  data-toggle="tab" data-translate="hp-deposit"></a>
                </li>
                <li  class="col text-center">
                    <a  href="#trade-tab" data-toggle="tab" data-translate="hp-trade"></a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="open-account-tab">
                    <div class="row"> 
                        <div class="col-lg-6 col-md-10 left-one">
                            <div class="vertical-text">01.</div>
                            <h3 data-translate="open-account-tab"></h3>
                            <div class="col-sm-10 description-container">
                                <p class="descriptions" data-translate="open-account-description"></p>
                            </div>
                            <a href="../registration" class="btn " data-translate="open_account"></a>
                        </div>
                        <div class="col-lg-6 col-md-12 right-one">
                            <img src="/wp-content/themes/tol-child/images/new-template-99/open_account.png" alt="Simplex" class="img-responsive open-account-img">
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="deposit-tab">
                    <div class="row">
                        <div class="col-lg-6 col-md-10 left-one">
                            <div class="vertical-text">02.</div>
                            <h3 data-translate="Deposit"></h3>
                            <div class="col-sm-10 description-container">
                                <p class="descriptions" data-translate="deposit-description"></p>
                            </div>
                            <a href="#" class="btn deposit" data-translate="Deposit"></a>
                        </div>
                        <div class="col-lg-6 col-md-12 right-one">
                                <img src="/wp-content/themes/tol-child/images/new-template-99/open_account.png" alt="Simplex" class="img-responsive open-account-img">
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="trade-tab">
                    <div class="row">
                        <div class="col-lg-6 col-md-10 left-one">
                            <div class="vertical-text">03.</div>
                            <h3 data-translate="trade"></h3>
                            <div class="col-sm-10 description-container">
                                <p class="descriptions" data-translate="trade-description"></p>
                            </div>
                            <a href="#" class="btn start-trading" data-translate="tradeNow"></a>
                        </div>
                        <div class="col-lg-6 col-md-12 right-one">
                            <img src="/wp-content/themes/tol-child/images/new-template-99/open_account.png" alt="Simplex" class="img-responsive open-account-img">
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- Container-fluid -->
    </section>
        <section class="contactus-section">
            <div class="container-fluid">
                <div class="col-lg-6 col-sm-10 any-questions">
                    <h1 data-translate="contact-us-header"></h1>
                    <p class="text-justify" data-translate="contact-us-description"></p>
                    <a href="../contact-us" class="btn bg-white white-button" data-translate="Homepage_Contact_Us"></a>
                </div>
            </div>
        </section> 
    </div>
 </div>  <!-- tol-homepage-layout-99 -->

