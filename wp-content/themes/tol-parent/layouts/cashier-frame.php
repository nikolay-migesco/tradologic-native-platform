<script type="text/javascript" src="/wp-content/themes/tol-parent/javascript/jquery.min.js"></script>
<script type="text/javascript" src="/wp-content/themes/tol-parent/scripts/jquery.md5.js"></script>

<form action="https://cashier-test.praxispay.com/Login.asp"
      method="post"
      name="CashierLogin"
      id="CashierLogin">
    <input type="hidden" name="FrontEnd" id="frontend" value="">
    <input type="hidden" name="PIN" id="pin" value="">
    <input type="hidden" name="Password" id="password" value="">
    <input type="hidden" name="Secret" id="secret" value="">
    <input type="hidden" name="Action" id="action" value="DEPOSIT"> 		<!-- optional -->
    <input type="hidden" name="Lang" id="lang" value="en">			<!-- optional -->
    <input type="hidden" name="PID" id="pid" value=""><!-- optional -->
    <input type="hidden" name="BlackBox" id="blackbox" value="">			<!-- optional -->
</form>

<script type="text/javascript">
    function getUrlParam(name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };

    $(document).ready(function() {
        $('#pin').val(getUrlParam('user'));
        $('#password').val(getUrlParam('sessionid'));
        $('#lang').val(getUrlParam('locale') || 'en');
        $('#secret').val($.md5(getUrlParam('user') + '-' + getUrlParam('sessionid') + '-testapi123'));
        $('#frontend').val('Praxis TEST_3 GBP');
        CashierLogin.submit();
    });
</script>
