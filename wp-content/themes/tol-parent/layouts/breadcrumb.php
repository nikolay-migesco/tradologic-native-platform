<div class="breadcrumb">

    <?php
    $blogName = get_option( 'blogname' );
    echo '<a href="' . get_home_url() . '" title="'.$blogName.'" class="breadcrumbs__link"> '.$blogName.'</a> <span class="breadcrumbs__divider"> > </span> ';
    // if there is a parent, display the link
    $parent_title = get_the_title( $post->post_parent );
    if ( $parent_title != the_title( '', '', false ) ) {
        echo '<a href="' . get_permalink( $post->post_parent ) . '" title="' . $parent_title . '">' . $parent_title . '</a> <span class="breadcrumbs__divider"> > </span>  ';
    }
    // then go on to the current page link
    ?>
    <span class="active"><?php the_title(); ?></span>
</div>
