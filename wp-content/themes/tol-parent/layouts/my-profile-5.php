 <div id="tol-myProfile-layout-1">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="pageHeadTitle">
            <?php require_once( ABSPATH . 'wp-content/themes/tol-parent/layouts/breadcrumb.php' ); ?>
        </div>

        <div class="myprofile-successmsg"></div>

        <div class="widgetContent myprofile-wrapper">
            <form class="myProfileContents">    
                <div data-widget="userDetails" data-type="error" style="display: none"></div>

                <fieldset class="wrapper userDetails">

                    <div class="label-field-wrapper text-container email-container">
                        <label data-translate="Registration_Email" class="label required"></label>
                        <input type="text" data-widget="userDetails" data-type="email" data-isRequired="true" data-regexpMessage="Please enter a valid email address" class="widgetPlaceholder input" disabled/>
                    </div>
                    <div class="label-field-wrapper text-container email-container">
                        <label data-widget="userDetails" data-translate="myProfileNickname" data-type="nicknameLabel" class="label required"  style="display: none;"></label>
                        <input type="text" data-widget="userDetails" data-type="nickname" data-isRequired="false"  class="widgetPlaceholder" style="display: none"/>
                    </div>            
                    <div class="label-field-wrapper text-container avatar-container">
                        <label data-widget="userDetails" data-type="avatarLabel" data-translate="myProfileAvatar" class="label required" style="display: none;"></label>
                        <div type="text" data-widget="userDetails" data-type="avatar" data-isRequired="false"  class="widgetPlaceholder" style="display: none"></div>
                    </div>

                    <div class="label-field-wrapper text-container title-container">
                        <label data-translate="Registration_Title" class="label"></label>
                        <span class="select-container">
                            <select data-widget="userDetails" data-type="title" class="widgetPlaceholder select selectContainer  styled"></select>
                        </span>
                    </div>

                    <div class="label-field-wrapper text-container firstName-container">
                        <label data-translate="Registration_FirstName" class="label required"></label>
                        <input type="text" data-widget="userDetails" data-type="firstName" data-isRequired="true" data-minLength="3" class="widgetPlaceholder input" />
                    </div>

                    <div class="label-field-wrapper text-container lastName-container">
                        <label data-translate="Registration_LastName" class="label required"></label>
                        <input type="text" data-widget="userDetails" data-type="lastName" data-isRequired="true" data-minLength="3" class="widgetPlaceholder input" />
                    </div>

                    <div class="label-field-wrapper text-container countryCode-container">
                        <label data-translate="Registration_Country" class="label required"></label>
                        <span class="select-container">
                            <select data-widget="userDetails" data-type="countryCode" data-isRequired="true" class="widgetPlaceholder selectContainer  select styled"></select>
                        </span>
                    </div>

                    <div class="label-field-wrapper text-container birth-container">
                        <label data-translate="Registration_DateOfBirth" class="label required"></label>
                        <span class="select-container birthDay">
                            <select class="styled selectContainer " data-widget="userDetails" data-type="birthDay" class="widgetPlaceholder"></select>
                        </span>
                        <span class="select-container birthMonth">
                            <select class="styled selectContainer " data-widget="userDetails" data-type="birthMonth" class="widgetPlaceholder"></select>
                        </span>
                        <span class="select-container birthYear">
                            <select class="styled selectContainer " data-widget="userDetails" data-type="birthYear" data-regexpMessage="Please enter your date of birth" class="widgetPlaceholder"></select>
                        </span>
                    </div>

                    <div class="label-field-wrapper text-container addressLine1-container">
                        <label data-translate="Registration_Street" class="label"></label>
                        <input type="text" data-widget="userDetails" data-type="addressLine1"  class="widgetPlaceholder input" />
                    </div>

                    <div class="label-field-wrapper text-container addressLine2-container">
                        <label data-translate="Registration_HouseNum" class="label"></label>
                        <input type="text" data-widget="userDetails" data-type="addressLine2"  class="widgetPlaceholder input" />
                    </div>

                    <div class="label-field-wrapper text-container city-container">
                        <label data-translate="Registration_City" class="label"></label>
                        <input type="text" data-widget="userDetails" data-type="city"  class="widgetPlaceholder input" />
                    </div>

                    <div class="label-field-wrapper text-container phone-container">
                        <label data-translate="Registration_Phone1" class="label required"></label>
                        <input type="text" data-widget="userDetails" data-type="countryPhoneCode" class="widgetPlaceholder phoneCountryCode" />
                        <input type="text" data-widget="userDetails" data-type="areaPhoneCode" class="widgetPlaceholder phoneAreaCode" />
                        <input type="text" data-widget="userDetails" data-type="phone" data-regexpMessage="Please enter your Primary Phone" class="widgetPlaceholder phone" />
                    </div>

                    <div class="label-field-wrapper text-container secondaryPhone-container">
                        <label data-translate="Registration_Phone2" class="label"></label>
                        <input type="text" data-widget="userDetails" data-type="secondaryPhone" data-regexpMessage="Please enter your Secondary phone/Mobile" name="secondaryPhone" class="widgetPlaceholder input" />
                    </div>

                    <div class="label-field-wrapper submit-container">
                        <label class="label empty"></label>
                        <div data-widget="userDetails" data-type="googleLoginBtn" class="widgetPlaceholder loginGoogle" style="display:none;"></div>
                        <div data-widget="userDetails" data-type="fbLoginBtn" class="widgetPlaceholder linkFacebook"></div>
                        <div data-widget="userDetails" data-type="fbLoginErrorMessage" class="fbLoginErrorMessage"></div>
                        <div data-widget="userDetails" data-type="googleLoginErrorMessage" class="fbLoginErrorMessage"></div>
                    </div>

                    <div class="label-field-wrapper submit-container">
                        <label class="label empty"></label>
                        <input class="submitBtn" type="button" data-widget="userDetails" data-type="submit" data-translate="MyProfile_Submit" class="widgetPlaceholder" />
                    </div>

                </fieldset>
            </form>
        </div>
        
        <div class="entry-content">
            <?php echo $post->post_content; ?>
        </div>
    </article>
</div>