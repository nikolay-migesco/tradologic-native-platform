<div id="tol-leaderBoard-layout-1">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="container social-wrapper">
            <div class="pageHeadTitle">
                <?php require_once(ABSPATH.'wp-content/themes/tol-parent/layouts/'.BREADCRUMB.'.php' ); ?>
            </div>
            <header class="entry-header">
                <h1 class="entry-title"><?php the_title(); ?></h1>
            </header>
            <div class="well clearfix">
                <div class="row">
                    <div class="hidden-xs col-sm-4 col-md-2">
                        <div class="row">
                            <label data-translate="socialTrading_AssetType" class="col-sm-12 nopadding-right tol-labels"></label>
                        </div>
                    </div>
                    <div class="hidden-xs col-sm-4 col-md-2">
                        <div class="row">
                            <label data-translate="socialTrading_Period" class="col-sm-12 nopadding-right tol-labels"></label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-xs-12 col-sm-4 col-md-2">
                        <div class="row">
                            <div class="col-sm-12">
                                <?php echo do_shortcode('[socialLeaderboardAssetTypeFilter class="form-control form-control-md"]'); ?>
                                <?php echo do_shortcode('[socialLeaderboardAssetFilter class="form-control form-control-md"]'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-4 col-md-2">
                        <div class="row">
                            <div class="col-sm-12">
                                <?php echo do_shortcode('[socialLeaderboardPeriodFilter class="form-control form-control-md"]'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-4 col-md-2">
                        <?php echo do_shortcode('[socialLeaderboardSearch class="btn btn-primary col-xs-12 search-button"]'); ?>
                    </div>
                </div>
            </div>
            <?php echo do_shortcode('[socialLeaderboardTable class="table table-striped"]'); ?>
        </div>
    </article>
</div>