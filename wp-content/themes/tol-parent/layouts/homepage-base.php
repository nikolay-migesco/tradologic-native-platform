<?php
$lang = ICL_LANGUAGE_CODE;
?>
<div class="tol-homepage-layout-33">
    <section class="paddingTop">
        <div class="main-banner">
			<div class="lion-man-section__content">
			   <div class="container">
				  <h1 class="lion-man-section__title">
					 <p>Innovative</p>
					 <p>Online Trading</p>
					 <p><strong>for Everyone</strong></p>
				  </h1>
				  <div class="lion-man-section__start-trading-btn btn-container btn-size-large btn-background-gold btn-border-none btn-text-white text-uppercase tol-trade" style="border-radius: 0px; background: rgb(249, 161, 52); border-color: rgb(249, 161, 52);"><a>Start trading now
					 </a>
				  </div>
			   </div>
			   <div class="lion-man-section__video-button-wrapper">
				  <div class="container">
					 <div class="lion-man-section__video-button" onclick="toggleVideo();">
						<img src="/wp-content/themes/tol-child/images/wilkinsfinance/wilkins-video-preview.png" alt="Video Preview">
					 </div>
						<div id="video-popup">
							<div id="close-icon" onClick="toggleVideo('hide');"></div>
							<iframe id="youtube-player" width="1000" height="700" src="https://www.youtube.com/embed/eco4eImMcgc?enablejsapi=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					 
					 <div class="lion-man-section__preview-text-wrapper">
						<p class="lion-man-section__preview-text lion-man-section__preview-text--heading">Migesco News</p>
						<p class="lion-man-section__preview-text">Daily Financial news - for traders, by traders</p>
					 </div>
				  </div>
			   </div>
			</div>
		</div>
		
    </section>
    <section class="text-section">
        <div class="slider-container">
			<?php echo do_shortcode('[slider]'); ?>
		</div>
    </section>
    <section class="why-trade-section container">
	   <div class="why-trade-section__content">
		  <h2 class="why-trade-section__title">CFD and Cryptocurrency Trading</h2>
		  <p class="why-trade-section__subtitle">            Welcome to Migesco - innovative CFD and Cryptocurrency online trading. Our team of dedicated financial services professionals and support staff
			 work closely with each of our clients to help them perfect their unique trading style.
		  </p>
	   </div>
	   <div class="why-trade-section__advantages">
		  <a href="/registration/" class="why-trade-section__fast why-trade-section__content-direction--row">
			 <div class="why-trade-section__img-wrapper">
				<div class="why-trade-section__fast-img why-trade-section__img"></div>
			 </div>
			 <div class="why-trade-section__fast-content why-trade-section__advantages-item">
				<h3 class="why-trade-section__fast-title why-trade-section__advantages-title">Fast</h3>
				<p class="why-trade-section__fast-subtitle why-trade-section__advantages-subtitle">Open an account with our firm today and start trading almost instantly. Take advantage of our platform’s wide selection of options which offer a new opportunity to profit every minute.</p>
			 </div>
		  </a>
		  <a href="/education/" class="why-trade-section__clear why-trade-section__content-direction--row">
			 <div class="why-trade-section__img-wrapper">
				<div class="why-trade-section__clear-img why-trade-section__img"></div>
			 </div>
			 <div class="why-trade-section__clear-content why-trade-section__advantages-item">
				<h3 class="why-trade-section__clear-title why-trade-section__advantages-title">Clear</h3>
				<p class="why-trade-section__clear-subtitle why-trade-section__advantages-subtitle">Our education team has created a step-by-step guide which walks you through choosing assets and placing orders. By following our instructions, it’s easy to become a successful trader.</p>
			 </div>
		  </a>
		  <a href="/trading-tools/" class="why-trade-section__complete why-trade-section__content-direction--row">
			 <div class="why-trade-section__img-wrapper">
				<div class="why-trade-section__complete-img why-trade-section__img"></div>
			 </div>
			 <div class="why-trade-section__complete-content why-trade-section__advantages-item">
				<h3 class="why-trade-section__complete-title why-trade-section__advantages-title">Variety</h3>
				<p class="why-trade-section__complete-subtitle why-trade-section__advantages-subtitle">Our firm provides access to the most popular assets and trading methods all in one place. Choose from stocks, indices, commodities, and other CFDs, and time frames from one minute up to one year.</p>
			 </div>
		  </a>
	   </div>
	</section>
	<section class="experience-platform-section">
	   <div class="experience-platform-section__content">
		  <h2 class="experience-platform-section__title">An Advanced Platform for Intuitive Trading</h2>
		  <p class="experience-platform-section__subtitle">Our technology puts successful trading at your fingertips. Gain access to the most up-to-date pricing information via a user-friendly interface designed to reduce your learning curve.</p>
		  <div class="experience-platform-section__buttons">
			 <div class="experience-platform-section__learn-btn experience-platform-section__btn btn-container btn-size-small btn-background-transparent btn-border-white btn-text-white text-uppercase" style="border-radius: 18px;"><a href="/education">How it Works
				</a>
			 </div>
			 <div class="experience-platform-section__start-trading-btn experience-platform-section__btn btn-container btn-size-small btn-background-gold btn-border-gold btn-text-white text-uppercase" style="border-radius: 18px; background: rgb(249, 161, 52); border-color: rgb(249, 161, 52);"><a class="tol-trade-experience-platform">Get Started
				</a>
			 </div>
		  </div>
	   </div>
	   <div class="experience-platform-section__advantages">
		  <div class="experience-platform-section__advantages-first-column">
			 <div class="experience-platform-section__range experience-platform-section__advantages-item">
				<div class="experience-img-wrapper"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAcCAQAAAAUnS30AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQfiAhUNLh5zj0P4AAAA+klEQVQoz3WTv04CQRCHf2ckmHDXQUO0416AUEMiiSHa2kjhA+hb0FDSkEC8pyChorPRzkYLEhLQxNheQ0MBH4XRu93bnW1m9pvM7M4focKpZ/qJbLnMGzY+1Z1+/PhBK8M2stZIifM3Jk74MJ+ZN1rsGfhwwCvQ9OE+sLFr8KeEfAMjHx4C0LFxgCTFeldZW3W1N36d/nrNccmBKyFucEuCRImlE34RoYBzXRv5+upIQj0t7JqLgA0AU1fNRRuATyI3fgIOdN0dOyMFJr6G3gJrQh+e5QPbuMqOsX8cHs3ANn4pdiyb1Ibe9FyY+n/PeyrFjcnUC8c66QiKGpyFfU4GJgAAAABJRU5ErkJggg==" class="experience-platform-section__advantages-img"></div>
				<div class="experience-platform-section__range-content">
				   <h3 class="experience-platform-section__range-title experience-platform-section__content-title">Easy-To-Use</h3>
				   <p class="experience-platform-section__range-subtitle experience-platform-section__content-subtitle">Intuitive use of the platform lets you react quickly and stay focused.</p>
				</div>
			 </div>
			 <div class="experience-platform-section__range experience-platform-section__advantages-item">
				<div class="experience-img-wrapper"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAcCAQAAAAUnS30AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQfiAhUNLh5zj0P4AAAA+klEQVQoz3WTv04CQRCHf2ckmHDXQUO0416AUEMiiSHa2kjhA+hb0FDSkEC8pyChorPRzkYLEhLQxNheQ0MBH4XRu93bnW1m9pvM7M4focKpZ/qJbLnMGzY+1Z1+/PhBK8M2stZIifM3Jk74MJ+ZN1rsGfhwwCvQ9OE+sLFr8KeEfAMjHx4C0LFxgCTFeldZW3W1N36d/nrNccmBKyFucEuCRImlE34RoYBzXRv5+upIQj0t7JqLgA0AU1fNRRuATyI3fgIOdN0dOyMFJr6G3gJrQh+e5QPbuMqOsX8cHs3ANn4pdiyb1Ibe9FyY+n/PeyrFjcnUC8c66QiKGpyFfU4GJgAAAABJRU5ErkJggg==" class="experience-platform-section__advantages-img"></div>
				<div class="experience-platform-section__range-content">
				   <h3 class="experience-platform-section__range-title experience-platform-section__content-title">Traditional</h3>
				   <p class="experience-platform-section__range-subtitle experience-platform-section__content-subtitle">Simplify your winning trading strategy by focusing solely on future price movements.</p>
				</div>
			 </div>
			 <div class="experience-platform-section__range experience-platform-section__advantages-item">
				<div class="experience-img-wrapper"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAcCAQAAAAUnS30AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQfiAhUNLh5zj0P4AAAA+klEQVQoz3WTv04CQRCHf2ckmHDXQUO0416AUEMiiSHa2kjhA+hb0FDSkEC8pyChorPRzkYLEhLQxNheQ0MBH4XRu93bnW1m9pvM7M4focKpZ/qJbLnMGzY+1Z1+/PhBK8M2stZIifM3Jk74MJ+ZN1rsGfhwwCvQ9OE+sLFr8KeEfAMjHx4C0LFxgCTFeldZW3W1N36d/nrNccmBKyFucEuCRImlE34RoYBzXRv5+upIQj0t7JqLgA0AU1fNRRuATyI3fgIOdN0dOyMFJr6G3gJrQh+e5QPbuMqOsX8cHs3ANn4pdiyb1Ibe9FyY+n/PeyrFjcnUC8c66QiKGpyFfU4GJgAAAABJRU5ErkJggg==" class="experience-platform-section__advantages-img"></div>
				<div class="experience-platform-section__range-content">
				   <h3 class="experience-platform-section__range-title experience-platform-section__content-title">Cryptocurrencies</h3>
				   <p class="experience-platform-section__range-subtitle experience-platform-section__content-subtitle">Trade the latest cryptocurrencies CFDs and make the most of today's highly volatile and potentially profitable market.</p>
				</div>
			 </div>
		  </div>
		  <div class="experience-platform-section__tablet-img-wrapper"></div>
	   </div>
	</section>
	<section class="education-section">
	   <div class="education-section__content">
		  <h2 class="education-section__title">Explore Our Educational Library</h2>
		  <p class="education-section__subtitle">We’ve created a series of E-Books, videos, and tutorials intended to give you the tools you need to become an expert trader.</p>
	   </div>
	   <div class="education-section__advantages education-section__advantages-mobile">
		  <div class="education-section__container">
			 <a href="" class="education-section__link">
				<div class="education-section__tips-for-creating education-section__advantages-item active  education-section__content-traning">
				   <h3 class="education-section__tips-for-creating-title education-section__advantage-title education-section__training-title">Online trading guide</h3>
				   <div class="education-section__img-container"><img src="/wp-content/themes/tol-child/images/wilkinsfinance/icon-trading.png" alt=""></div>
				</div>
			 </a>
		  </div>
		  <div class="education-section__container">
			 <a href="/cryptocurrency" class="education-section__link">
				<div class="education-section__how-create-form education-section__advantages-item">
				   <h3 class="education-section__how-create-form-title education-section__advantage-title">Expert articles</h3>
				   <div class="education-section__img-container"><img src="/wp-content/themes/tol-child/images/wilkinsfinance/icon-person.png" alt=""></div>
				</div>
			 </a>
			 <a href="" class="education-section__link">
				<div class="education-section__web-assembly education-section__advantages-item">
				   <h3 class="education-section__web-assembly-title education-section__advantage-title">Training courses</h3>
				   <div class="education-section__img-container"><img src="/wp-content/themes/tol-child/images/wilkinsfinance/icon-speaker.png" alt="" class="education-section__icon-speaker"></div>
				</div>
			 </a>
		  </div>
		  <div class="education-section__container">
			 <a href="/education/glossary" class="education-section__link">
				<div class="education-section__adobe-introduces education-section__advantages-item">
				   <h3 class="education-section__adobe-introduces-title education-section__advantage-title">Glossary</h3>
				   <div class="education-section__img-container"><img src="/wp-content/themes/tol-child/images/wilkinsfinance/icon-www.png" alt=""></div>
				</div>
			 </a>
			 <a href="/about-us/faq-page/" class="education-section__link">
				<div class="education-section__design-workflow education-section__advantages-item">
				   <h3 class="education-section__design-workflow-title education-section__advantage-title">FAQ</h3>
				   <div class="education-section__img-container"><img src="/wp-content/themes/tol-child/images/wilkinsfinance/icon-globe.png" alt=""></div>
				</div>
			 </a>
		  </div>
	   </div>
	   <div class="education-section__advantages education-section__advantages-tablet">
		  <div class="education-section__container">
			 <a href="" class="education-section__link">
				<div class="education-section__tips-for-creating education-section__advantages-item active  education-section__content-traning">
				   <h3 class="education-section__tips-for-creating-title education-section__advantage-title education-section__training-title">Online trading guide</h3>
				   <div class="education-section__img-container"><img src="/wp-content/themes/tol-child/images/wilkinsfinance/icon-trading.png" alt=""></div>
				</div>
			 </a>
			 <a href="/cryptocurrency" class="education-section__link">
				<div class="education-section__how-create-form education-section__advantages-item">
				   <h3 class="education-section__how-create-form-title education-section__advantage-title">Expert articles</h3>
				   <div class="education-section__img-container"><img src="/wp-content/themes/tol-child/images/wilkinsfinance/icon-person.png" alt=""></div>
				</div>
			 </a>
		  </div>
		  <div class="education-section__container">
			 <a href="" class="education-section__link">
				<div class="education-section__web-assembly education-section__advantages-item">
				   <h3 class="education-section__web-assembly-title education-section__advantage-title">Training courses</h3>
				   <div class="education-section__img-container"><img src="/wp-content/themes/tol-child/images/wilkinsfinance/icon-speaker.png" alt="" class="education-section__icon-speaker"></div>
				</div>
			 </a>
			 <a href="/glossary/" class="education-section__link">
				<div class="education-section__adobe-introduces education-section__advantages-item">
				   <h3 class="education-section__adobe-introduces-title education-section__advantage-title">Glossary</h3>
				   <div class="education-section__img-container"><img src="/wp-content/themes/tol-child/images/wilkinsfinance/icon-www.png" alt=""></div>
				</div>
			 </a>
			 <a href="/about-us/faq-page/" class="education-section__link">
				<div class="education-section__design-workflow education-section__advantages-item">
				   <h3 class="education-section__design-workflow-title education-section__advantage-title">FAQ</h3>
				   <div class="education-section__img-container"><img src="/wp-content/themes/tol-child/images/wilkinsfinance/icon-globe.png" alt=""></div>
				</div>
			 </a>
		  </div>
	   </div>
	</section>
	<section class="under-hood-section">
	   <div class="container">
		  <div class="under-hood-section__content">
			 <h2 class="under-hood-section__title">Under the Hood</h2>
			 <p class="under-hood-section__subtitle">The Migesco platform also includes a collection of investment tools which will make trading even more convenient.</p>
		  </div>
		  <div class="swiper-box">
			 <div class="contacts">
				<div class="contact" style="background-image:url(/wp-content/themes/tol-child/images/wilkinsfinance/slider-1.png);">
				   <div></div>
				</div>
				<div class="contact" style="background-image:url(/wp-content/themes/tol-child/images/wilkinsfinance/slider-2.png);">
				   <div></div>
				</div>
				<div class="contact active" style="background-image:url(/wp-content/themes/tol-child/images/wilkinsfinance/slider-3.png);">
				   <div></div>
				</div>
				<div class="contact" style="background-image:url(/wp-content/themes/tol-child/images/wilkinsfinance/slider-4.png);">
				   <div></div>
				</div>
			 </div>
			 <div class="slider">
				<?php echo do_shortcode('[print_vertical_thumbnail_slider]'); ?>

			 </div>
		  </div>
	   </div>
	</section>
</div>
<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script>
<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js'></script>
<script>
   jQuery(document).ready(function($){
        toggleVideo = function(state) {
            var div = document.getElementById("video-popup"),
                iframe = div.getElementsByTagName("iframe")[0].contentWindow;

            $(div).css('display', state == 'hide' ? 'none' : 'block');
            iframe.postMessage('{"event":"command","func":"' + (state == "hide" ? "pauseVideo" : "playVideo") + '","args":""}','*');
        }
    });
	
	$(window).on('loadingIsCompleted', function () {
		if(!widgets.isLogged()) {
			$('#tol-platform-info-popup').modal('show');
		}
	 });
</script>
