<div id="tol-registration-layout-12" class="widgetContent registration-wrapper">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <header class="entry-header" style="background:url(/wp-content/themes/tol-parent/images/reg-header.jpg);background-size:cover;background-position:center center;">
            <h1 class="entry-title site" data-translate="register-new-account"></h1>
        </header>
        <div class="entry-content container">
            <div class="site">
                <div class="reg-steps">
                    <h2 class="reg-step reg-step-1" data-translate="SignUpHeading" class="widgetPlaceholder"></h2>
                    <h3 class="reg-step reg-step-1" style="float: right;"><img class="registration-process"/></h3>
                    <div class="clearfix"></div>
                </div>
                <div class="inner-pages-wrap">
                    <div class="registration-page">
                        <div class="my-profile-menu"></div>
                        <div class="widgetContent registration-wrapper">
                            <form>
                                <fieldset class="wrapper registration">
                                    <div class="left-side">
                                        <div class="label-field-wrapper text-container email-container">
                                            <label class="label required" data-translate="Registration_Email"></label>
                                            <?php echo do_shortcode('[registrationEmail class="form-control"]'); ?>
                                        </div>

                                        <div class="label-field-wrapper text-container password-container">
                                            <label class="label required" data-translate="Registration_Password_:"></label>
                                            <?php echo do_shortcode('[registrationPassword class="form-control"]'); ?>
                                        </div>

                                        <div class="label-field-wrapper text-container confirmPassword-container">
                                            <label class="label required" data-translate="Registration_Password2_:"></label>
                                            <?php echo do_shortcode('[registrationConfirmPassword class="form-control"]'); ?>
                                        </div>

                                        <div class="label-field-wrapper text-container title-container">
                                            <label class="label required" data-translate="Registration_Title"></label>
                                            <?php echo do_shortcode('[registrationTitle class="form-control"]'); ?>
                                        </div>

                                        <div class="label-field-wrapper text-container firstName-container">
                                            <label class="label required" data-translate="Registration_FirstName"></label>
                                            <?php echo do_shortcode('[registrationFirstName class="form-control"]'); ?>
                                        </div>

                                        <div class="label-field-wrapper text-container lastName-container">
                                            <label class="label required" data-translate="Registration_LastName"></label>
                                            <?php echo do_shortcode('[registrationLastName class="form-control"]'); ?>
                                        </div>

                                        <div class="label-field-wrapper text-container languageCode-container">
                                            <label class="label required" data-translate="Registration_Language"></label>
                                            <?php echo do_shortcode('[registrationLanguageCode class="form-control"]'); ?>
                                        </div>
                                    </div>
                                    <div class="right-side">
<!--                                        <div class="label-field-wrapper text-container countryCode-container">-->
<!--                                            <label class="label required" data-translate="Registration_Country"></label>-->
<!--                                            --><?php //echo do_shortcode('[registrationCountryCode class="form-control"]'); ?>
<!--                                        </div>-->
                                        <div class="label-field-wrapper text-container birth-container">
                                            <label class="label required" data-translate="Registration_DateOfBirth"></label>
                                            <span class="select-container birthDay">
                                                <?php echo do_shortcode('[registrationBirthDay class="form-control"]'); ?>
                                            </span>
                                            <span class="select-container birthMonth">
                                                <?php echo do_shortcode('[registrationBirthMonth class="form-control"]'); ?>
                                            </span>
                                            <span class="select-container birthYear">
                                                <?php echo do_shortcode('[registrationBirthYear class="form-control"]'); ?>
                                            </span>
                                        </div>
                                        <div class="label-field-wrapper text-container phone-container">
                                            <label class="label required" data-translate="Registration_Phone1"></label>
                                            <?php echo do_shortcode('[registrationCountryPhoneCode class="phoneCountryCode"]'); ?>
                                            <?php echo do_shortcode('[registrationAreaPhoneCode class="phoneAreaCode"]'); ?>
                                            <?php echo do_shortcode('[registrationPhone class="phone"]'); ?>
                                        </div>
                                        <div class="label-field-wrapper text-container secondaryPhone-container">
                                            <label class="label" data-translate="Registration_Phone2"></label>
                                            <?php echo do_shortcode('[registrationSecondaryPhone class="form-control"]'); ?>
                                        </div>
                                        
                                        <div class="label-field-wrapper text-container idExpiryDate-container" style="display:none">
                                            <label class="label" data-translate="Registration_idExpiryDate"></label>
                                            <?php echo do_shortcode('[registrationidExpiryDate class="form-control"]'); ?>
                                        </div>
                                        
                                        <div class="label-field-wrapper text-container idIssuer-container" style="display:none">
                                            <label class="label idIssuerLabel" data-translate="Registration_idIssuer"></label>
                                            <?php echo do_shortcode('[registrationidIssuer class="form-control"]'); ?>
                                        </div>

                                        <div class="label-field-wrapper text-container currencyCode-container">
                                            <label class="label required" data-translate="Registration_BillingCurrency"></label>
                                            <span class="select-container">
                                                <?php echo do_shortcode('[registrationCurrencyCode class="form-control"]'); ?>
                                            </span>
                                        </div>

                                        <div class="label-field-wrapper checkbox-container subscribeToPromoEmail-container">
                                            <label class="checkbox">
                                                <input type="checkbox">
                                                <label data-translate="subscribeToPromoEmail"></label>
                                            </label>
                                        </div>

                                        <div class="label-field-wrapper checkbox-container subscribeToPromoSms-container">
                                            <label class="checkbox">
                                                <input type="checkbox">
                                                <label data-translate="subscribeToPromoSms" class="widgetPlaceholder"></label>
                                            </label>
                                        </div>

                                        <div class="label-field-wrapper checkbox-container tos-container">
                                            <label class="checkbox">
                                                <input data-widget="registration" data-type="tos" data-isrequired="true" type="checkbox" />
                                                <label data-translate="registrationTos" class="widgetPlaceholder"></label>
                                                <div data-widget="registration" data-type="error" style="display: none" class="error-message"></div>
                                            </label>
                                            <br>
                                        </div>
                                    </div><div class="clearfix"></div>
                                    <div class="label-field-wrapper submit-container">
                                        <label class="label empty"></label>
                                        <?php echo do_shortcode('[registrationSubmit class="btn btn-primary btn-yellow col-xs-12 col-sm-12"]'); ?>
                                        <div class="loading-animation" style="display:none;"></div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="right-sidebar-security">
                    <div class="safesecure">    
                        <h1 data-translate="safe-and-secure-trading"></h1>
                        <ul>
                            <li data-translate="SegregatedAccounts" class="widgetPlaceholder"></li>
                            <li data-translate="AllFunds" class="widgetPlaceholder"></li>
                            <li data-translate="128Bit" class="widgetPlaceholder"></li>
                        </ul>
                    </div>
                    <div class="transparency">
                        <h1 data-translate="SecurityHeading" class="widgetPlaceholder"></h1>
                        <p><img class="size-full wp-image-8414" src="/wp-content/themes/tol-parent/images/secure2.png" alt="secure"></p>
                    </div>
                    <div class="regulated">
                        <h1 data-translate="CustomerSupportHeading" class="widgetPlaceholder"></h1>
                        <p><!-- <img src="../wp-content/uploads/247-Customer-Support.png" alt="regulated" class="size-full wp-image-8945" /> --><br>
                        </p>
                        <p class="regulated-contacts">
                            <img src="/wp-content/themes/tol-parent/images/mail.png">&nbsp;&nbsp;support@xmarkets.com<br>
                            <img src="/wp-content/themes/tol-parent/images/uk_flag.png">&nbsp;&nbsp;+441515414060<br>
                            <img src="/wp-content/themes/tol-parent/images/nl_flag.png">&nbsp;&nbsp;+31858881665<br>
                            <img src="/wp-content/themes/tol-parent/images/de_flag.png">&nbsp;&nbsp;+4969120066065<br>
                            <img src="/wp-content/themes/tol-parent/images/ca_flag.png">&nbsp;&nbsp;+14378001867<br>
                            <img src="/wp-content/themes/tol-parent/images/at_flag.png">&nbsp;&nbsp;+43720883854</p>
                    </div>
                </div>
            </div><!-- .site -->
        </div><!-- .entry-content -->
        <footer class="entry-meta">
        </footer><!-- .entry-meta -->
    </article><!-- #post -->
</div>