<script src='//mte-media.com/admin2/js/mte.js'></script>
<?php
get_header();
switch (ICL_LANGUAGE_CODE) {

    case "ar":
        $language = "ar";
        break;
    case "ru":
        $language = "ru";
        break;
    default:
        $language = "en";
}
?>

<div id="content" role="main">
    <div class="container-fluid">
        <div class="col-sm-offset-1 col-sm-10">
            <script>
                put_course({target_id: 'mte_course', ref: '<?php echo MTE_CLIENT_ID ?>', settings: 'Default', language: '<?php echo $language ?>', product: 'cal', lsns: '', startWith: undefined})
            </script>
            <div id='mte_course'></div>
            <div id='players'>
                <div id='mfp_player'></div>
                <div id='vid_player_container'></div>
                <div id='ebk_player'></div>
                <div id='cal'></div>
            </div>
        </div>
    </div>
</div>