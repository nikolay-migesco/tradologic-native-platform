<script type="text/javascript">
    $(window).on('loadingIsCompleted', function () {
        $('.my-trades-table thead th:nth-child(1),.my-trades-table thead th:nth-child(3),.my-trades-table thead th:nth-child(5),.my-trades-table thead th:nth-child(7)').addClass('hidden-xs');
        setTimeout(function () {
            $('.my-trades-table tr td:nth-child(1),.my-trades-table tr td:nth-child(3),.my-trades-table tr td:nth-child(5),.my-trades-table tr td:nth-child(7)').addClass('hidden-xs');
        }, 2000);
    });
</script>
<div id="tol-myTrades-layout-1" class="container">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="container">
            <div class="pageHeadTitle">
                <?php require_once(ABSPATH . 'wp-content/themes/tol-parent/layouts/' . BREADCRUMB . '.php' ); ?>
            </div>
            <header class="entry-header">
                <h1 class="entry-title"><?php the_title(); ?></h1>
            </header>
            <script>
                $(window).on('loadingIsCompleted', function () {
                    $(window).trigger(widgets.events.expireOptions);
                });
            </script>

            <div class="widgetContent mytrades-wrapper">

                <div class="search-form">
                    <label data-translate="TradeRoom_Assets"></label>
                    <span class="select-container">
                        <select data-widget="myTradesOptions" class="widgetPlaceholder selectContainer styled" style="display:none">all</select>
                    </span>

                    <label class="fromLabel" data-translate="MyOptions_From"></label>
                    <input data-widget="myTradesFromDate" data-past="30" class="widgetPlaceholder" type="text" style="display:none" />

                    <label class="toLabel" data-translate="MyOptions_To"></label>
                    <input data-widget="myTradesToDate" class="widgetPlaceholder" type="text" style="display:none" />

                    <label data-translate="copyBoardtrades" class="myTradesSocial"></label>
                    <select data-widget="myTradesSocialFilter" class="widgetPlaceholder myTradesSocialFilterDDL" style="display: none"></select>			

                    <input data-widget="myTradesSearchButton" class="widgetPlaceholder myTradesSearchButton searchButton" type="button" data-translate="MyTrades_SearchBtn" style="display:none" />
                </div>

                <div class="info-wrapper">
                    <label data-translate="Trades_Volume"></label>
                    <span data-widget="myTradesVolume" class="widgetPlaceholder tradesVolume" style="display:none"></span>

                    <label data-translate="Trades_Profit"></label>
                    <span data-widget="myTradesProfit" class="widgetPlaceholder tradesProfit" style="display:none"></span>

                    <label data-translate="Trades_Total"></label>
                    <span data-widget="myTradesTotalTrades" class="widgetPlaceholder tradesTotal" style="display:none"></span>
                </div>

                <table data-widget="myTradesTable" class="widgetPlaceholder datagrid">
                    <thead>
                        <tr>
                            <th data-column="id" class="type-string" data-translate="MyOptions_OrderID"></th>
                            <th data-column="created" data-translate="MyOptions_OrderDate"></th>
                            <th data-column="name" class="type-float" data-translate="MyOptions_Assets"></th>
                            <th data-column="action" data-translate="MyOptions_Action"></th>
                            <th data-column="rate" data-translate="MyOptions_TradeRate"></th>
                            <th data-column="invested" class="type-int" data-translate="MyOptions_Amount"></th>
                            <th data-column="expired" data-translate="MyOptions_ExpireAt"></th>
                            <th data-column="currentRate" data-translate="MyOptions_ExpiryRate"></th>
                            <th data-column="payout" class="type-int" data-translate="MyOptions_Payout"></th>
                            <th data-column="profit" class="type-int" data-translate="MyOptions_PL"></th>				
                            <th data-column="social" class="type-int" data-translate="copyBoardtrades"></th>
                        </tr>
                    </thead>
                </table>
                <div data-widget="myTradesPaging" data-paginglimit="10" class="widgetPlaceholder datagrid-paging pastExpiriesPaging" style="display:none"></div>

            </div>

            <div class="entry-content">
                <?php echo $post->post_content; ?>
            </div>
    </article>
</div>