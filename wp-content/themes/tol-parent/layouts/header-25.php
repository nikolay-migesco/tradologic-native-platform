<!DOCTYPE html>
<html class="no-js lang-<?php echo ICL_LANGUAGE_CODE; ?>" <?php language_attributes(); ?>>
    <head>
        <?php
            $GLOBALS['desktopView'] = 'desktop';
            setcookie('config', 'forex', time()+3600*24, "/");
        require_once 'header.php';
        ?>
        <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
        <!-- Metas Page details-->
        <title><?php wp_title(' | ', true, 'right'); ?><?php bloginfo('name'); ?></title>
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!--google font style-->
        <link href='//fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
        <script src="/wp-content/themes/tol-parent/scripts/select2.min.js"></script>

        <?php if (strpos($_SERVER['REQUEST_URI'], '/traderoom/?product=simplex') !== false || strpos($_SERVER['REQUEST_URI'], '/traderoom/?product=newbinary') !== false) { ?>
            <!--chartingIQ-->
            <link rel="stylesheet" type="text/css" href="/wp-content/themes/tol-parent/chartIQ/simplex-IQ/css/perfect-scrollbar.css"  />
            <link rel="stylesheet" type="text/css" href="/wp-content/themes/tol-parent/chartIQ/simplex-IQ/css/stx-chart.css"  />
            <link rel="stylesheet" type="text/css" href="/wp-content/themes/tol-parent/chartIQ/simplex-IQ/sass/chartiq.css"  />
        <?php } else { ?>
            <!--chartingIQ-->
            <link rel="stylesheet" type="text/css" href="/wp-content/themes/tol-parent/chartIQ/css/perfect-scrollbar.css"  />
            <link rel="stylesheet" type="text/css" href="/wp-content/themes/tol-parent/chartIQ/css/stx-chart.css"  />
            <link rel="stylesheet" type="text/css" href="/wp-content/themes/tol-parent/chartIQ/sass/chartiq.css"  />
         <?php } ?>

    </head>
    <body id="top" <?php body_class(); ?>>
        <!-- START Asset Specification popup -->
        <div id="tol-asset-spec" tabindex="-1" role="dialog" class="ui-dialog ui-corner-all ui-widget ui-widget-content ui-front ui-draggable forex-neworder-dialog asset-specs-popup" aria-labelledby="tol-asset-specs-result" style="display: none;">
            <div class="ui-dialog-titlebar ui-corner-all ui-widget-header ui-helper-clearfix ui-draggable-handle">
                <span id="ui-id-2" class="ui-dialog-title" data-translate="AssetSpecification"></span>
                <button type="button" class="ui-button ui-corner-all ui-widget ui-button-icon-only ui-dialog-titlebar-close" data-dismiss="modal" title="Close"></button>
            </div>
            <div class="ui-dialog-content ui-widget-content">
                <div class="asset-orderid">
                    <div class="forex-asset-name"></div>
                </div>
                <div class="asset-specs-content ui-dialog-content ui-widget-content">
                    <div class="asset-specs-body"></div>
                </div>
                <div class="modes-btn-row">
                    <div class="button forex-confirm-btn" data-dismiss="modal" data-translate="Close"></div>
                </div>
            </div>
        </div>
        <!-- END Asset Specification popup -->

        <?php
        if (strpos($_SERVER['REQUEST_URI'], 'my-trades') !== false) {
            if (ENABLE_FOREX_SOCIAL) {
                    include(get_template_directory() . '/page-templates/forex-social.php');
            }
        }

        if (strpos($_SERVER['REQUEST_URI'], 'traderoom') !== false) {
            if (isset($_GET['product'])) {
                $GLOBALS['curr_product'] = $_GET['product'];
            } else {
                if (isset($_GET['config'])) {
                    $GLOBALS['curr_product'] = $_GET['config'];
                } else {
                    $GLOBALS['curr_product'] = getDefaultTraderoom();
                }
            }
            // Mobile header
            if ($GLOBALS['curr_product'] == 'realm' || $GLOBALS['curr_product'] == 'realil' || $GLOBALS['curr_product'] == 'simplexm' || $GLOBALS['curr_product'] == 'newbinarym') {
                $GLOBALS['desktopView'] = 'mobile';
            };
            // include newbinary mobile and landscape neworderpopup
            if ($GLOBALS['curr_product'] == 'newbinarym' || $GLOBALS['curr_product'] == 'newbinaryil') {
                include(get_template_directory() . '/page-templates/newbinarymobile-popup.php');
            }
            // include forex social popups if social is enabled
            if (($GLOBALS['curr_product'] == 'forex' || $GLOBALS['curr_product'] == 'realm' || $GLOBALS['curr_product'] == 'realil') && ENABLE_FOREX_SOCIAL) {
                include(get_template_directory() . '/page-templates/forex-social.php');
            }
            // Desktop header
            if ($GLOBALS['desktopView'] == 'desktop') {
                wp_dequeue_style('header');
                wp_enqueue_style('header', get_template_directory_uri() . "/styles/layouts/header-25.css", array(), md5_file(get_template_directory() . "/styles/layouts/header-25.css"), false);
                ?>
                <?php if (ENABLE_MTE) { ?>
                    <div id="mte-dialog" tabindex="-1" role="dialog"
                         style="height: auto; width: 430px; display: none;"
                         class="ui-dialog ui-corner-all ui-widget ui-widget-content ui-front ui-draggable"></div>
                <?php } ?>
        <!-- START PostionID history popup -->
        <div id="tol-position-history" tabindex="-1" role="dialog" style="height: auto; width: 320px; display: none;" class="ui-dialog ui-corner-all ui-widget ui-widget-content ui-front ui-draggable forex-poshistory-dialog">
            <div class="ui-dialog-titlebar ui-corner-all ui-widget-header ui-helper-clearfix ui-draggable-handle">
                <span class="ui-dialog-title" data-translate="PositionHistory"></span>
                <button type="button" class="ui-button ui-corner-all ui-widget ui-button-icon-only ui-dialog-titlebar-close" data-dismiss="modal" title="Close">
                    <span class="ui-button-icon ui-icon ui-icon-closethick"></span>
                    <span class="ui-button-icon-space"> </span>
                    Close
                </button>
            </div>
            <div class="asset-orderid">
                <div class="forex-position-id"></div>
                <div class="forex-posid-asset"></div>
                <div class="forex-profit-title two-lines" data-translate="profit"></div>
                <div class="forex-posid-profit"></div>
            </div>
            <div class="position-history-content ui-dialog-content ui-widget-content">
                <div class="position-history-body"></div>
            </div>
        </div>
        <!-- END PostionID history popup -->

        <div id="tol-header-layout-25">
            <header id="tol-header">
                <div class="navigation" id="navigation">
                    <div class="cf">
                        <div id="toggle-menu" class="nav-button has-icon main-menu">
                            <div class="main-menu-icon">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                            <div class="tol-wp-navigation">
                                <?php
                                wp_nav_menu(
                                    array(
                                        'menu' => 'Боковое меню в терминале',
                                        'container' => 'false',
                                        'menu_class' => 'nav navbar-nav main-nav'
                                    )
                                );
                                ?>
                            </div>
                        </div>
                        <div class="nav_logo">
                            <a class="navbar-brand" href="#"></a>
                        </div>
                        <div class="right-buttons">
                            <div class="tol-login-userbar-container">
                                <div class="tol-language-switcher">
                                    <?php do_action('wpml_add_language_selector'); ?>
                                </div>
                                <!-- SIGN IN NAVIGATION -->
                                <div id="tol-login-area" class="nav navbar-nav user-nav tolUserIsNotLogged" style="display:none;">
                                    <div class="nav-game-notlogged">
                                        <div type="button" class="nav-button button nav-game" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <?php
                                            $modeCode = 'game-name-easy';
                                            if ($GLOBALS['curr_product'] == 'forex') {
                                                $modeCode = 'game-name-real';
                                            } ;
                                            ?>
                                            <span class="userBarElem currgame" data-translate="<?php echo $modeCode ?>"></span>
                                            <?php unset($modeCode); ?>
                                            <span class="dropdown-arrow"></span>
                                        </div>
                                        <ul class="navgame dropdown-menu">
                                            <?php if ($GLOBALS['curr_product'] != 'simplex') { ?>
                                                <li>
                                                    <a class="tol-forex-game" href="../traderoom?product=simplex" data-translate="game-name-easy"></a>
                                                </li>
                                            <?php } ?>    
                                           
                                            <?php if ($GLOBALS['curr_product'] != 'forex') { ?>
                                                <li>
                                                    <a class="tol-forex-pro" href="../traderoom?product=forex" data-translate="game-name-real"></a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                    <div class="nav-button button tol-login-account" data-toggle="modal" data-target="#tol-login-popup"><span class="icon icon-card"></span><span data-translate="UserData_userBlock_Login"></span></div>
                                    <div class="nav-button button open-real-account tol-create-account" data-translate="UserData_userBlock_CreateAccount"></div>
                                </div>
                                <!-- END SIGN IN NAVIGATION -->
                                <!-- USER BAR - FOR LOGGED USER -->
                                <ul id="tol-userbar" class="nav navbar-nav user-nav tolUserIsLogged" style="display:none">
                                    <li>
                                        <div type="button" class="nav-button button nav-game" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <?php
                                            $modeCode = 'game-name-easy';
                                            if ($GLOBALS['curr_product'] == 'forex') {
                                                $modeCode = 'game-name-real';
                                            } ;
                                            ?>
                                            <span class="userBarElem currgame" data-translate="<?php echo $modeCode ?>"></span>
                                            <?php unset($modeCode); ?>
                                            <span class="dropdown-arrow"></span>
                                        </div>
                                        <ul class="navgame dropdown-menu">
                                            <?php if ($GLOBALS['curr_product'] != 'simplex') { ?>
                                                <li>
                                                    <a class="tol-forex-game" href="../traderoom?product=simplex" data-translate="game-name-easy"></a>
                                                </li>
                                            <?php }
                                           
                                                if ($GLOBALS['curr_product'] != 'forex') { ?>
                                                <li>
                                                    <a class="tol-forex-pro" href="../traderoom?product=forex" data-translate="game-name-real"></a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                    <li>
                                        <div class="nav-button button open-real-account tol-quick-deposit" data-translate="DepositBtn" ></div>
                                    </li>
                                    <li class="li-switch-account" style="display: none;">
                                        <div type="button" data-switch="" class="nav-button button nav-switch-account" aria-haspopup="true" aria-expanded="false">
                                            <span class="userBarElem user-switch-acc"></span>
                                        </div>
                                    </li>
                                    <li>
                                        <div type="button" class="nav-button button nav-user" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <?php echo do_shortcode('[userBarUserName]'); ?>
                                            <span class="dropdown-arrow"></span>
                                        </div>
                                        <ul class="dropdown-menu">
                                            <?php if (($GLOBALS['curr_product'] == 'forex' || $GLOBALS['curr_product'] == 'realm' || $GLOBALS['curr_product'] == 'realil')) { ?>
                                                <li>
                                                    <a class="tol-userbar-settings" href="#" data-toggle="modal" data-target="#tol-change-modes" data-translate="asset-settings"></a>
                                                </li>
                                            <?php } ?>
                                            <li>
                                                <a class="tol-userbar-my-trades" href="#" data-translate="userDataMyTrades"></a>
                                            </li>
                                            <li>
                                                <a class="tol-userbar-my-profile" href="#" data-translate="Personal_Details"></a>
                                            </li>
                                            <?php if (($GLOBALS['curr_product'] == 'forex' || $GLOBALS['curr_product'] == 'realm' || $GLOBALS['curr_product'] == 'realil') && ENABLE_FOREX_SOCIAL) { ?>
                                                <li>
                                                    <a class="tol-userbar-social-profile" href="#" data-translate="Social_Profile"></a>
                                                </li>
                                            <?php }
                                            if (ENABLE_DEMO_FLOW) { ?>
                                                <li>
                                                    <a class="tol-userbar-transaction-history" href="#" data-translate="cashierMenuTransactionHistory" style="display: none;"></a>
                                                </li>
                                            <?php } ?>
                                            <li>
                                                <a class="tol-userbar-logout-user" href="#" data-translate="logout"></a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                                <!-- END USER BAR- FOR LOGGED USER -->
                            </div>
                        </div>
                    </div>
            </header>
        </div>
                <?php
            } /* end of if ($GLOBALS['desktopView'] == 'desktop') { */
            else { /* else for if ($GLOBALS['desktopView'] == 'desktop') { */
             wp_dequeue_style('header');
             wp_enqueue_style('header', get_template_directory_uri() . "/styles/layouts/header-25m.css", array(), md5_file(get_template_directory() . "/styles/layouts/header-25m.css"), false);
             ?>
        <!-- MOBILE MENU -->
        <div id="mobilemenu-shadow"></div>
        <div id="mobilemenu" class="tol-wp-navigation">
            <?php
            wp_nav_menu(
                array(
                    'menu' => 'Боковое меню в терминале',
                    'container' => 'false',
                    'menu_class' => 'nav navbar-nav main-nav'
                )
            );
            ?>
            <div class="tol-language-switcher">
                <?php do_action('wpml_add_language_selector'); ?>
            </div>
        </div>
        <!-- END MOBILE MENU -->
        <!-- START PostionID history popup -->
        <div id="tol-position-history" tabindex="-1" role="dialog" style="height: auto; width: 320px; display: none;" class="ui-dialog ui-corner-all ui-widget ui-widget-content ui-front ui-draggable forex-poshistory-dialog">
            <div class="ui-dialog-titlebar ui-corner-all ui-widget-header ui-helper-clearfix ui-draggable-handle">
                <span class="ui-dialog-title">Position History</span>
                <button type="button" class="ui-button ui-corner-all ui-widget ui-button-icon-only ui-dialog-titlebar-close" data-dismiss="modal" title="Close">
                    <span class="ui-button-icon ui-icon ui-icon-closethick"></span>
                    <span class="ui-button-icon-space"> </span>
                    Close
                </button>
            </div>
            <div class="asset-orderid">
                <div class="forex-position-id"></div>
                <div class="forex-posid-asset"></div>
                <div class="forex-profit-title two-lines" data-translate="profit"></div>
                <div class="forex-posid-profit"></div>
            </div>
            <div class="position-history-content ui-dialog-content ui-widget-content">
                <div class="position-history-body"></div>
            </div>
        </div>
        <!-- END PostionID history popup -->
        <div id="tol-header-layout-25">
            <header id="tol-header">
                <div class="navigation" id="navigation">
                    <div class="cf">
                        <div class="nav-button has-icon main-menu">
                            <div id="toggle-menu" class="main-menu-icon">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                        <div class="nav_logo">
                            <a class="navbar-brand" href="#"></a>
                        </div>
                        <div class="right-buttons">
                            <div class="tol-login-userbar-container">
                                <!-- SIGN IN NAVIGATION -->
                                <div id="tol-login-area" class="nav navbar-nav user-nav tolUserIsNotLogged" style="display:none;">
                                    <div class="nav-button button tol-login-account" data-toggle="modal" data-target="#tol-login-popup"><span class="icon icon-card"></span><span data-translate="UserData_userBlock_Login"></span></div>
                                    <div class="nav-button button open-real-account tol-create-account" data-translate="UserData_userBlock_CreateAccount"></div>
                                </div>
                                <!-- END SIGN IN NAVIGATION -->
                                <!-- USER BAR - FOR LOGGED USER -->
                                <ul id="tol-userbar" class="nav navbar-nav user-nav tolUserIsLogged" style="display:none">
                                    <li>
                                        <?php if (strpos($_SERVER['REQUEST_URI'],'cashier') == false) { ?>
                                            <div class="nav-button button open-real-account tol-quick-deposit" data-translate="deposit-BTN"></div>
                                        <?php } else { ?>
                                            <div class="nav-button button open-real-account tol-trade-button" data-translate="trade"></div>
                                        <?php } ?>
                                    </li>
                                    <li>
                                        <div id="userbar-mobilemenu" type="button" class="nav-button button nav-user" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="userBarElem userName" data-widget="userBar"></span>
                                            <span class="dropdown-arrow"></span>
                                        </div>
                                    </li>
                                </ul>
                                <!-- END USER BAR- FOR LOGGED USER -->
                            </div>
                        </div>
                        <div id="userbarmenu" class="dropdown-menu">
                            <ul>
                                <li class="usermenu-icon">
                                    <div id="toggle-usermenu">X</div>
                                </li>
                                <?php if (($GLOBALS['curr_product'] == 'forex' || $GLOBALS['curr_product'] == 'realm' || $GLOBALS['curr_product'] == 'realil')) { ?>
                                    <li>
                                        <a class="tol-userbar-settings" href="#" data-toggle="modal" data-target="#tol-change-modes" data-translate="asset-settings"></a>
                                    </li>
                                <?php } ?>
                                <!--li>
                                    <a class="tol-userbar-my-portfolios" href="#">My Portfolios</a>
                                </li-->
                                <li>
                                    <a class="tol-userbar-my-trades" href="#" data-toggle="modal" data-type="myTradesButton" data-target="#tol-trades-popup" data-translate="userDataMyTrades"></a>
                                </li>
                                <li>
                                    <a class="tol-userbar-my-profile" href="#" data-translate="Personal_Details"></a>
                                </li>
                                <?php if (($GLOBALS['curr_product'] == 'forex' || $GLOBALS['curr_product'] == 'realm' || $GLOBALS['curr_product'] == 'realil') && ENABLE_FOREX_SOCIAL)  { ?>
                                    <li>
                                        <a class="tol-userbar-social-profile" href="#" data-translate="Social_Profile"></a>
                                    </li>
                                <?php } ?>
                                <li>
                                    <a class="tol-userbar-logout-user" href="#" data-translate="logout"></a>
                                </li>
                            </ul>
                        </div>
                    </div>
            </header>
        </div>

            <?php } /* end of else for if ($GLOBALS['desktopView'] == 'desktop') { */
        } /* end of if (strpos($_SERVER['REQUEST_URI'], 'traderoom') !== false) { */
        else { /* else for if (strpos($_SERVER['REQUEST_URI'], 'traderoom') !== false) { */
        wp_dequeue_style('header');
        wp_enqueue_style('header', get_template_directory_uri() . "/styles/layouts/header-26.css", array(), md5_file(get_template_directory() . "/styles/layouts/header-26.css"), false);
        ?>
        <div id="tol-header-layout-26">
        <?php if ( ENABLE_MTE )  { ?>
            <script src="//mte-media.com/slmloaders/apilib\/xmteapi.js"></script>
            <div id="mte-dialog" tabindex="-1" role="dialog" style="height: auto; width: 430px; display: none;" class="ui-dialog ui-corner-all ui-widget ui-widget-content ui-front ui-draggable"> </div>
        <?php } ?>
         <!-- START PostionID history popup -->
         <div id="tol-position-history" tabindex="-1" role="dialog" style="height: auto; width: 320px; display: none;" class="ui-dialog ui-corner-all ui-widget ui-widget-content ui-front ui-draggable forex-poshistory-dialog">
                <div class="ui-dialog-titlebar ui-corner-all ui-widget-header ui-helper-clearfix ui-draggable-handle">
                    <span class="ui-dialog-title" data-translate="PositionHistory"></span>
                    <button type="button" class="ui-button ui-corner-all ui-widget ui-button-icon-only ui-dialog-titlebar-close" data-dismiss="modal" title="Close">
                        <span class="ui-button-icon ui-icon ui-icon-closethick"></span>
                        <span class="ui-button-icon-space"> </span>
                        Close
                    </button>
                </div>
                <div class="asset-orderid">
                    <div class="forex-position-id"></div>
                    <div class="forex-posid-asset"></div>
                    <div class="forex-profit-title two-lines" data-translate="profit"></div>
                    <div class="forex-posid-profit"></div>
                </div>
                <div class="position-history-content ui-dialog-content ui-widget-content">
                    <div class="position-history-body"></div>
                </div>
            </div>
            <!-- END PostionID history popup -->
            <header id="tol-header" class="wrap">
                <div class="navigation" id="navigation">
                    <!-- clock -->
                    <div class=" time-multiselect">
                        <div class="js-TimeLocal time-multiselect__time"></div>
                        <div class="arrow-down">
                        </div>
                        <div class="time-multiselect__content">
                            <div class="time-multiselect__content">
                                <ul class="time-multiselect__list js-Selector">
                                    <li value="-12" class="time-multiselect__list-item">GMT -12</li>
                                    <li value="-11" class="time-multiselect__list-item">GMT -11</li>
                                    <li value="-10" class="time-multiselect__list-item">GMT -10</li>
                                    <li value="-9" class="time-multiselect__list-item">GMT -9</li>
                                    <li value="-8" class="time-multiselect__list-item">GMT -8</li>
                                    <li value="-7" class="time-multiselect__list-item">GMT -7</li>
                                    <li value="-6" class="time-multiselect__list-item">GMT -6</li>
                                    <li value="-5" class="time-multiselect__list-item">GMT -5</li>
                                    <li value="-4" class="time-multiselect__list-item">GMT -4</li>
                                    <li value="-3" class="time-multiselect__list-item">GMT -3</li>
                                    <li value="-2" class="time-multiselect__list-item">GMT -2</li>
                                    <li value="-1" class="time-multiselect__list-item">GMT -1</li>
                                    <li value="0" class="time-multiselect__list-item">GMT +0</li>
                                    <li value="1" class="time-multiselect__list-item">GMT +1</li>
                                    <li value="2" class="time-multiselect__list-item">GMT +2</li>
                                    <li value="3" class="time-multiselect__list-item">GMT +3</li>
                                    <li value="4" class="time-multiselect__list-item">GMT +4</li>
                                    <li value="5" class="time-multiselect__list-item">GMT +5</li>
                                    <li value="6" class="time-multiselect__list-item">GMT +6</li>
                                    <li value="7" class="time-multiselect__list-item">GMT +7</li>
                                    <li value="8" class="time-multiselect__list-item">GMT +8</li>
                                    <li value="9" class="time-multiselect__list-item">GMT +9</li>
                                    <li value="10" class="time-multiselect__list-item">GMT +10</li>
                                    <li value="11" class="time-multiselect__list-item">GMT +11</li>
                                    <li value="12" class="time-multiselect__list-item">GMT +12</li>
                                </ul>
                                <div class="force-overflow"></div>
                            </div>
                        </div>
                    </div>
                    <script type='text/javascript'
                            src='//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js'></script>
                    <script>
                        if ($.cookie('userTimeZoneValue') == null) {
                            $('.time-multiselect__list.js-Selector').find('.time-multiselect__list-item[value=0]').addClass('active');
                            $('.time-multiselect__list.js-Selector').find('.time-multiselect__list-item[value=0]').append('<span class="time-multiselect__user-time">User time</span>');
                            $.cookie("userTimeZoneValue", 0, {path: "/"});
                        } else {
                            $.each($('.time-multiselect__list.js-Selector').find('.time-multiselect__list-item'), function (i, block) {
                                $(this).removeClass('active');
                                $(this).html().split('<span class="time-multiselect__user-time">User time</span>')[0];
                                if ($(this).attr('value') == $.cookie('userTimeZoneValue')) {
                                    $(this).addClass('active');
                                    $(this).append('<span class="time-multiselect__user-time">User time</span>');
                                }
                            });
                        }
                        var userTimeZoneText = $('.time-multiselect__list-item.active').html().split('<span class="time-multiselect__user-time">User time</span>')[0];


                        $('.time-multiselect').click(function () {
                            if ($('.time-multiselect__content').is(':visible')) {
                                $('.time-multiselect__content').hide();
                            } else {
                                $('.time-multiselect__content').show();
                            }
                        });

                        $('.time-multiselect__list.js-Selector > .time-multiselect__list-item').click(function () {
                            $.each($('.time-multiselect__list.js-Selector').find('.time-multiselect__list-item'), function (i, block) {
                                $(this).removeClass('active');
                                $(this).html($(this).html().split('<span class="time-multiselect__user-time">User time</span>')[0]);
                            });
                            $(this).addClass('active');
                            $(this).append('<span class="time-multiselect__user-time">User time</span>');
                            $.cookie('userTimeZoneValue', $(this).attr('value'), {path: "/"});
                            userTimeZoneText = $(this).html().split('<span class="time-multiselect__user-time">User time</span>')[0];
                        })

                        function GetClock() {
                            const dateTimeUtc = new Date();
                            const dateTimeLocal = new Date(dateTimeUtc.getTime() + $.cookie('userTimeZoneValue') * 3600 * 1000);
                            document.querySelector(".js-TimeLocal").innerHTML = dateTimeLocal.toUTCString().split(' 2019')[1].split(' GMT')[0] + ' ' + userTimeZoneText;
                        }

                        GetClock();
                        setInterval(GetClock, 1000);
                    </script>
                    <!-- clock -->
                    <div class="cf">
                        <div class="nav_logo">
                            <a class="navbar-brand" href="#"></a>
                        </div>

                        <div class="navbar-default">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#slidemenu">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="right-buttons">
                            <div class="tol-wp-navigation">
                                <div class="collapse navbar-collapse navbar-collapse" id="slidemenu">
                                    <div class="nav_logo">
                                        <a class="navbar-brand" href="#"></a>
                                    </div>
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#slidemenu" aria-expanded="true">
                                    <span aria-hidden="true">×</span>
                                    </button>
                                    <?php
                                    wp_nav_menu(
                                        array(
                                            'menu' => 'Main Menu',
                                            'theme_location' => 'primary',
                                            'container' => 'false',
                                            'menu_class' => 'nav navbar-nav main-nav'
                                        )
                                    );
                                    ?>
                                </div>
                            </div>
                            <div class="tol-language-switcher">
                                <?php do_action('wpml_add_language_selector'); ?>
                            </div>
                            <div class="tol-login-userbar-container">
                                <!-- SIGN IN NAVIGATION -->
                                <div id="tol-login-area" class="nav navbar-nav user-nav tolUserIsNotLogged" style="display:none;">
                                    <div class="nav-button button tol-login-account" data-toggle="modal" data-target="#tol-login-popup"><span class="icon icon-card"></span><span data-translate="UserData_userBlock_Login"></span></div>
                                    <div class="nav-button button open-real-account tol-create-account" data-translate="UserData_userBlock_CreateAccount"></div>
                                </div>
                                <!-- END SIGN IN NAVIGATION -->
                                <!-- USER BAR - FOR LOGGED USER -->
                                <ul id="tol-userbar" class="nav navbar-nav user-nav tolUserIsLogged" style="display:none">
                                    <li>
                                        <div class="nav-button button open-real-account tol-trade" data-translate="MainMenu_Trade"></div>
                                    </li>
                                    <li>
                                        <div type="button" class="nav-button button nav-user" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <?php echo do_shortcode('[userBarUserName]'); ?>
                                            <span class="dropdown-arrow"></span>
                                        </div>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a class="tol-userbar-my-trades" href="#" data-toggle="modal" data-target="#tol-trades-popup" data-translate="userDataMyTrades"></a>
                                            </li>
                                            <li>
                                                <a class="tol-userbar-my-profile" href="#" data-translate="Personal_Details"></a>
                                            </li>
                                            <li>
                                                <a class="tol-userbar-logout-user" href="#" data-translate="logout"></a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                                <!-- END USER BAR- FOR LOGGED USER -->
                            </div>
                        </div>
                    </div>
            </header>
        </div>
        <?php } /* end of else for if (strpos($_SERVER['REQUEST_URI'], 'traderoom') !== false) { */
