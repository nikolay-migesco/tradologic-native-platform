<div id="tol-transaction-history">
    <div class="well well-sm clearfix flat-top-borders">
        <?php echo do_shortcode('[cashierTransactionHistory]'); ?>
        <?php echo do_shortcode('[cashierTransactionHistoryPaging]'); ?>
    </div>
</div>