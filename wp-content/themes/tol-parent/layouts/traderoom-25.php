<script>
    $(document).ready(function(){
        $.cookie("game", 'simplex', {path: "/"});
        $('body').removeClass('white-theme');
    });
</script>
<!-- BEGIN TRADE POP UP -->
<script>
    jQuery(function ($) {
        $(".modal-dialog, .ui-draggable").draggable();
        migesco.attachDraggableToNewNodes();
    });
</script>
<div id="tol-trade-popup" data-widget="easyForex-easyForex" data-type="popupcontainer" data-taid="" data-keyboard="false" data-backdrop="static" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="tol-trade-label">
    <div class="modal-dialog modal-sm modal-default" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-widget="easyForex-easyForex" data-type="closepopup">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body widgetPlaceholder">
                <div class="front">
                    <div data-widget="easyForex-easyForex" data-type="topinfo" class="left-part">
                        <div class="flag-container">
                            <div class="left-flag"></div>
                            <div class="right-flag"></div>
                        </div>
                        <div class="tradearea-box-title"></div>
                    </div>
                    <div class="right-part">
                        <div class="tradearea-box-price">
                            <span data-widget="easyForex-easyForex" data-type="popupPrice" class="tradearea-box-price-title" data-translate="easyForexPrice"></span>
                            <span data-type="averageprice" data-direction="down" class="tradearea-box-price-value down"></span>
                        </div>
                        <div class="tradearea-box-change">
                            <span class="tradearea-box-change-title" data-translate="dailyChange"></span>
                            <span data-type="dailychange" class="tradearea-box-change-value"></span>
                        </div>
                    </div>
                </div>
                <div class="header-text" data-widget="easyForex-easyForex" data-type="compare-text">
                </div>
                <div class="btn-wrapper">
                    <div data-widget="easyForex-easyForex" data-type="upbtn" data-direction="up" class="upbtn" data-translate="up"></div>
                    <div data-widget="easyForex-easyForex" data-type="downbtn" data-direction="down" class="downbtn" data-translate="down"></div>
                </div>
                <div data-widget="easyForex-easyForex" data-type="popupcontrollerscontainer" class="controllers-wrapper inactive">
                    <div class="cover-controllers"></div>
                    <div class="ddl-container">
                        <label data-translate="investment-amount"></label>
                        <div data-widget="easyForex-easyForex" data-type="investment-element" type="button" class="nav-button button ddl-ctrl" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span data-widget="easyForex-easyForex" data-type="investment-selected"></span>
                            <span class="dropdown-arrow"></span>
                        </div>
                        <input type="number" onkeypress="return event.charCode == 0 || event.charCode == 46 || (event.charCode >= 48 && event.charCode <= 57)" data-widget="easyForex-easyForex" data-type="investmentInput" class="ddl-ctrl" style="display: none;" />
                        <ul data-widget="easyForex-easyForex" data-type="investment-list" class="dropdown-menu"></ul>
                    </div>
                    <label><span data-translate="risk-sensitivity"></span> <a href="#" class="info-icon" data-toggle="tooltip" data-placement="top" data-widget="easyForex-easyForex" data-type="tooltip-riskSensitivity" ></a></label>
                    <div class="risk-sens-btns">
                        <div data-widget="easyForex-easyForex" data-type="riskBtn" data-risk="0" class="lowbtn" data-translate="low"></div>
                        <div data-widget="easyForex-easyForex" data-type="riskBtn" data-risk="1" class="medbtn" data-translate="medium"></div>
                        <div data-widget="easyForex-easyForex" data-type="riskBtn" data-risk="2" class="highbtn" data-translate="high"></div>
                    </div>

                    <div class="ddl-container">
                        <label><span data-translate="takeProfit"></span> <a href="#" class="info-icon" data-toggle="tooltip" data-placement="top" data-widget="easyForex-easyForex" data-type="tooltip-takeProfit"></a></label>
                        <div data-widget="easyForex-easyForex" data-type="takeprofit-element" type="button" class="nav-button button ddl-ctrl" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span data-widget="easyForex-easyForex" data-type="takeprofit-selected"></span>
                            <span class="dropdown-arrow"></span>
                        </div>
                        <input type="number" onkeypress="return event.charCode == 0 || event.charCode == 46 || (event.charCode >= 48 && event.charCode <= 57)" data-widget="easyForex-easyForex" data-type="takeProfitInput" class="ddl-ctrl" style="display: none;" />
                        <ul data-widget="easyForex-easyForex" data-type="takeprofit-list" class="dropdown-menu"></ul>
                    </div>
                    <div class="ddl-container">
                        <label><span data-translate="stopLoss"></span> <a href="#" class="info-icon" data-toggle="tooltip" data-placement="top" data-widget="easyForex-easyForex" data-type="tooltip-stopLoss"></a></label>
                        <div data-widget="easyForex-easyForex" data-type="stoploss-element" type="button" class="nav-button button ddl-ctrl" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span data-widget="easyForex-easyForex" data-type="stoploss-selected"></span>
                            <span class="dropdown-arrow"></span>
                        </div>
                        <input type="number" onkeypress="return event.charCode == 0 || event.charCode == 46 || (event.charCode >= 48 && event.charCode <= 57)" data-widget="easyForex-easyForex" data-type="stopLossInput" class="ddl-ctrl" style="display: none;" />
                        <ul data-widget="easyForex-easyForex" data-type="stoploss-list" class="dropdown-menu"></ul>
                    </div>
                    <div class="footer-info">
                        <div class="info-block">
                            <span><span data-translate="easy-trade-value"></span> <a href="#" style="margin-left: 18px;" class="info-icon" data-toggle="tooltip" data-placement="top" data-widget="easyForex-easyForex" data-type="tooltip-tradeValue"></a> <span data-widget="easyForex-easyForex" data-type="trade-value"></span></span>
                        </div>
                        <div class="info-block">
                            <span>
                                <span data-translate="easy-commission"></span>
                                <a href="#" data-widget="easyForex-easyForex" data-type="tooltip-commission" class="info-icon" data-toggle="tooltip" data-placement="top"></a>
                                <span data-widget="easyForex-easyForex" data-type="commission"></span>
                            </span>
                        </div>
                        <div class="info-block">
                            <span>
                                <span data-translate="easy-pointvalue"></span>
                                <a href="#" style="margin-left: 19px;" data-widget="easyForex-easyForex" data-type="tooltip-pointValue" class="info-icon" data-toggle="tooltip" data-placement="top"></a>
                                <span data-widget="easyForex-easyForex" data-type="pointValue"></span>
                            </span>
                        </div>
                    </div>
                    <div data-widget="easyForex-easyForex" data-type="invest" class="invest-btn" data-translate="invest"></div>
                </div>
            </div>
            <div class="modal-footer"></div>
            <div class="toggleChart" data-widget="easyForex-easyForex" data-type="chart-toggle" data-translate="easy-chart"></div>
        </div>
        <div class="chartwrapper">
            <div id="tv_chart_container"></div>
        </div>
    </div>
</div>
<!-- END TRADE POP UP -->
<div class="tradewrapper" data-widget="easyForex-easyForex" data-type="tradewrapper">
    <!-- Moved the boxfilter inside tradewrapper cause otherwise clicks are not detected -->
    <div class="boxfilter" data-widget="easyForex-easyForex" data-type="filter" <?php echo($GLOBALS['curr_product'] == 'crypto' ? 'data-categories="CryptoCoins,CryptoTokens"' : ''); ?>>
        <div class="filter-el active" data-filter="All" data-translate="filterAll"></div>
        <div class="filter-el" data-filter="Favourites">
            <span data-translate="traderoomFavourites"></span>
            <span class="fav-counter" data-widget="easyForex-easyForex" data-type="fav-counter" style="display: none;"></span>
        </div>
        <div class="filter-el" data-filter="Currencies" data-translate="traderoomCurrencies"></div>
        <div class="filter-el" data-filter="CryptoCoins" data-translate="traderoomCryptoCoins"></div>
        <div class="filter-el" data-filter="CryptoTokens" data-translate="traderoomCryptoTokens"></div>
        <div class="filter-el" data-filter="Commodities" data-translate="traderoomCommodities"></div>
        <div class="filter-el" data-filter="Futures" data-translate="traderoomIndices"></div>
        <div class="filter-el" data-filter="Stocks" data-translate="traderoomStocks"></div>
    </div>
    <div data-widget="easyForex-easyForex" data-type="assetsList" class="tradearea"></div>
    <div data-widget="easyForex-easyForex" data-type="paging" data-paginglimit="10" data-prevNextPage="true"  class="pagination pagination-sm"></div>
</div>
<div class="account panel easy-forex-table-wrapper positions-active" id="accountPanel">
    <div class="tabHolder">
        <a href="#trades" aria-controls="orders" role="tab" data-toggle="tab">
            <span class="tab-item tabpositions has-tooltip tab-active">
                <span data-translate="openPositions"></span>
               <div id="forex-orders-count" class="counter active">0</div>
            </span>
        </a>
        <a href="#pastInvestments" aria-controls="orders" role="tab" data-toggle="tab">
            <span class="tab-item taborders has-tooltip">
                <span data-translate="ClosedPositions"></span>
                <span id="easyforex-closed-count" class="counter">0</span>
            </span>
        </a>
        <a href="#pulse_market" class="simple_page" aria-controls="pulse_market" role="tab" data-toggle="tab" data-url="category/puls-rynka">
                <span class="tab-item has-tooltip">
                    <span data-translate="market-pulse"></span>
                </span>
        </a>
        <a href="#trading_signals" class="simple_page" aria-controls="trading_signals" role="tab" data-toggle="tab" data-url="category/torgovie-idei">
                <span class="tab-item has-tooltip">
                    <span data-translate="trading-ideas"></span>
                </span>
        </a>
        <a href="#calendars" aria-controls="calendars" role="tab" data-toggle="tab" >
            <span class="tab-item has-tooltip">
                <span data-translate="calendars"></span>
            </span>
        </a>
        <span class="column-settings tabeasypositions">
            <div class="settings"></div>
            <div class="contextmenu multiple-selection datatable-contextmenu _focusable leftCenter" tabindex="-1">
                <div class="multiple-selection-content">
                    <div class="item item-datatable-contextmenu-trade selected" data-column="1">
                        <span data-translate="tradeID"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-open-time selected" data-column="2">
                        <span data-translate="openTime"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-quantity selected" data-column="3">
                        <span data-translate="asset"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-direction selected" data-column="4">
                        <span data-translate="type"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-averagePrice selected" data-column="5">
                        <span data-translate="openPrice"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-currentPrice selected" data-column="6">
                        <span data-translate="investAmount"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-limitPrice selected" data-column="7">
                        <span data-translate="risk"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-stopPrice selected" data-column="8">
                        <span data-translate="stopLoss"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-margin selected" data-column="9">
                        <span data-translate="takeProfit"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-swap selected" data-column="10">
                        <span data-translate="marketPrice"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-created" data-column="11">
                        <span data-translate="swap"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-ppl selected" data-column="12">
                        <span data-translate="profit"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-ppl selected" data-column="13">
                        <span data-translate="Return"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="btn-wrapper navigation">
                        <div class="nav-button defaultSettings" data-translate="defaultSettings"></div>
                    </div>
                </div>
            </div>
        </span>
        <span class="column-settings tab-easy-closed-positions">
            <div class="settings"></div>
            <div class="contextmenu multiple-selection datatable-contextmenu _focusable leftCenter" tabindex="-1">
                <div class="multiple-selection-content">
                    <div class="item item-datatable-contextmenu-trade selected" data-column="1">
                        <span data-translate="tradeID"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-name selected" data-column="2">
                        <span data-translate="openTime"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-quantity selected" data-column="3">
                        <span data-translate="asset"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-direction selected" data-column="4">
                        <span data-translate="type"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-averagePrice selected" data-column="5">
                        <span data-translate="openPrice"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-currentPrice selected" data-column="6">
                        <span data-translate="investAmount"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-limitPrice selected" data-column="7">
                        <span data-translate="risk"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-stopPrice selected" data-column="8">
                        <span data-translate="stopLoss"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-margin selected" data-column="9">
                        <span data-translate="takeProfit"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-swap selected" data-column="10">
                        <span data-translate="closeTime"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-swap selected" data-column="11">
                        <span data-translate="closePrice"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-created selected" data-column="12">
                        <span data-translate="swap"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-created selected" data-column="13">
                        <span data-translate="commissions"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-ppl selected" data-column="14">
                        <span data-translate="Return"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="item item-datatable-contextmenu-ppl selected" data-column="15">
                        <span data-translate="profit"></span>
                        <div class="list-checkbox"></div>
                    </div>
                    <div class="btn-wrapper navigation">
                        <div class="nav-button defaultSettings" data-translate="defaultSettings"></div>
                    </div>
                </div>
            </div>
        </span>
    </div>
    <div class="tab-content" id="easy-forex-tab-content">
        <div role="tabpanel" class="tab-pane active" id="trades">
            <div class="dataTable">
                <table class="fake-table" data-widget="easyForex-easyForexOpenPositions">
                    <thead>
                    <tr data-type="easyForexOpenPositions-header">
                        <th data-column="tradeID" data-sort="int" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="tradeID"></span></div>
                        </th>
                        <th data-column="openTime" data-sort="int" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="openTime"></span></div>
                        </th>
                        <th data-column="asset" data-sort="string" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="asset"></span></div>
                        </th>
                        <th data-column="type" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="type"></span></div>
                        </th>
                        <th data-column="openPrice" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="openPrice"></span></div>
                        </th>
                        <th data-column="investAmount" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="investAmount"></span></div>
                        </th>
                        <th data-column="risk" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="risk"></span></div>
                        </th>
                        <th data-column="stopLoss" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="stopLoss"></span></div>
                        </th>
                        <th data-column="takeProfit" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="takeProfit"></span></div>
                        </th>
                        <th data-column="marketPrice" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="marketPrice"></span></div>
                        </th>
                        <th data-column="swap" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="swap"></span></div>
                        </th>
                        <th data-column="return" data-sort="string" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="profit"></span></div>
                        </th>
                        <th data-column="profit" data-sort="string" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="return"></span></div>
                        </th>
                        <th data-column="actions" data-sort="string" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="actions"></span></div>
                        </th>
                    </tr>
                    </thead>
                </table>
                <div class="scrollable-area scrollable-area-at-top">
                    <div class="scrollable-area-body">
                        <div class="scrollable-area-content">
                            <div class="dataTable">
                                <table id="forexProOpenTradesTable" class="hasArrows" data-widget="easyForex-easyForexOpenPositions">
                                    <tbody data-widget="easyForex-easyForexOpenPositions" data-type="table"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="pastInvestments">
            <div class="dataTable">
                <table class="fake-table" data-widget="easyForex-easyForexClosedPositions">
                    <thead>
                    <tr data-type="easyForexClosedPositions-header">
                        <th data-column="tradeID" data-sort="int" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="tradeID"></span></div>
                        </th>
                        <th data-column="openTime" data-sort="int" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="openTime"></span></div>
                        </th>
                        <th data-column="asset" data-sort="string" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="asset"></span></div>
                        </th>
                        <th data-column="type" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="type"></span></div>
                        </th>
                        <th data-column="openPrice" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="openPrice"></span></div>
                        </th>
                        <th data-column="investAmount" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="investAmount"></span></div>
                        </th>
                        <th data-column="risk" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="risk"></span></div>
                        </th>
                        <th data-column="stopLoss" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="stopLoss"></span></div>
                        </th>
                        <th data-column="takeProfit" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="takeProfit"></span></div>
                        </th>
                        <th data-column="closeTime" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="closeTime"></span></div>
                        </th>
                        <th data-column="closePrice" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="closePrice"></span></div>
                        </th>
                        <th data-column="swap" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="swap"></span></div>
                        </th>
                        <th data-column="commissions" data-sort="float" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="commissions"></span></div>
                        </th>
                        <th data-column="profit" data-sort="string" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="return"></span></div>
                        </th>
                        <th data-column="return" data-sort="string" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="profit"></span></div>
                        </th>
                        <th data-column="actions" data-sort="string" class="name canSort canResize canDrag">
                            <div class="cellWrapper"><span class="content" data-translate="actions"></span></div>
                        </th>
                    </tr>
                    </thead>
                </table>
                <div class="scrollable-area scrollable-area-at-top">
                    <div class="scrollable-area-body">
                        <div class="scrollable-area-content">
                            <div class="dataTable">
                                <table id="forexProOpenTradesTable" class="hasArrows" data-widget="easyForex-easyForexClosedPositions">
                                    <tbody data-widget="easyForex-easyForexClosedPositions" data-type="table"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="pulse_market">
            <iframe width="100%" height="256" scrolling="no" style="border:0;overflow-y:hidden;" id="pmi"></iframe>
            <div class="loading_iframe"><div></div></div>
        </div>
        <div role="tabpanel" class="tab-pane" id="trading_signals">
            <iframe width="100%" height="256" scrolling="no" style="border:0;overflow-y:hidden;" id="tsi"></iframe>
            <div class="loading_iframe"><div></div></div>
        </div>
        <div role="tabpanel" class="tab-pane" id="calendars">
            <div style="margin: 10px; height:200px">
                <!-- TradingView Widget BEGIN -->
                <div class="tradingview-widget-container">
                    <div class="tradingview-widget-container__widget"></div>
                    <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-events.js" >
                        {
                            "colorTheme": "dark",
                            "width": "100%",
                            "height": "100%",
                            "locale": "ru",
                            "importanceFilter": "-1,0,1"
                        }
                    </script>
                </div>
                <!-- TradingView Widget END -->
            </div>

        </div>
    </div>
</div>

<div class="sidepanel-tabs focusable" id="sidePanel" tabindex="-1">
    <ul class="nav right-sidebar-nav">
        <li class="messages-tab">
            <a href="#tab2" data-toggle="tab" class="active"></a>
        </li>
        <?php if (ENABLE_MTE) { ?>
        <li class="daily-video-tab">
            <a href="#daily-video" data-toggle="tab" class=""></a>
        </li>
        <li class="signals-tab">
            <a href="#signals" data-toggle="tab" class=""></a>
        </li>
        <li class="economic-calendar-tab">
            <a href="#economic-calendar" data-toggle="tab" class=""></a>
        </li>
		<li class="market-analysis-tab">
            <a href="#market-analysis" data-toggle="tab" class=""></a>
        </li>
		<?php } ?>
    </ul>
    <div class="tab-content">
        <?php if (ENABLE_MTE) { ?>
            <script src="//mte-media.com/slmloaders/apilib\/xmteapi.js"></script>
            <div class="tab-pane active in" id="tab2">
                <div class="tol-messages-sidebar">
                    <h1 class="messages-title" data-translate="userDataMessages"></h1>
                    <div class="scrollable-area scrollable-area-at-top">
                        <div class="scrollable-area-body">
                            <div class="scrollable-area-content">
                                <div class="clearfix flat-top-borders">
                                    <?php echo do_shortcode('[messageInboxAllMessages]'); ?>
                                    <?php echo do_shortcode('[messageInboxContent]'); ?>
                                    <div class="pagination-wrapper">
                                        <?php echo do_shortcode('[messageInboxPaging]'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="daily-video" class="tab-pane fade">
                <header class="entry-header">
                    <h1 class="entry-title news-title">Daily Videos</h1>
                </header>
                <div class="scrollable-area scrollable-area-at-top">
                    <div class="scrollable-area-body">
                        <div class="scrollable-area-content">
                            <div id="daily-video-container">
                                <?php include( ABSPATH . 'wp-content/themes/tol-parent/layouts/MTE/traderoom-mte/daily-video.php' ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="signals" class="tab-pane fade">
                <header class="entry-header">
                    <h1 class="entry-title news-title">Signals</h1>
                </header>
                <div class="scrollable-area scrollable-area-at-top">
                    <div class="scrollable-area-body">
                        <div class="scrollable-area-content">
                            <div id="signals-container">
                                <?php include( ABSPATH . 'wp-content/themes/tol-parent/layouts/MTE/traderoom-mte/signals.php' ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="economic-calendar" class="tab-pane fade">
                <header class="entry-header">
                    <h1 class="entry-title news-title">Economic calendar</h1>
                </header>
                        <div id="economic-calendar-container">
                        <!-- Economic Calendar-->
                            <!-- Day tabs -->
                            <ul class="nav nav-tabs nav-justified" id="sortingByDays" role="tablist">
                                <li class="nav-item all active">
                                    <a class="nav-link active" id="all-tab" data-toggle="tab" href="#all" role="tab" aria-controls="all" aria-selected="true">all</a>
                                </li>
                                <li class="nav-item monday">
                                    <a class="nav-link" id="monday-tab" data-toggle="tab" href="#monday" role="tab" aria-controls="monday" aria-selected="false">mon</a>
                                </li>
                                <li class="nav-item tuesday">
                                    <a class="nav-link" id="tuesday-tab" data-toggle="tab" href="#tuesday" role="tab" aria-controls="tuesday" aria-selected="false">tue</a>
                                </li>
                                <li class="nav-item wednesday">
                                    <a class="nav-link" id="wednesday-tab" data-toggle="tab" href="#wednesday" role="tab" aria-controls="wednesday" aria-selected="false">wed</a>
                                </li>
                                <li class="nav-item thursday">
                                    <a class="nav-link" id="thursday-tab" data-toggle="tab" href="#thursday" role="tab" aria-controls="thursday" aria-selected="false">thu</a>
                                </li>
                                <li class="nav-item friday">
                                    <a class="nav-link" id="friday-tab" data-toggle="tab" href="#friday" role="tab" aria-controls="friday" aria-selected="false">fri</a>
                                </li>
                            </ul>
                            <span class="calendar_btn" type="button" data-toggle="collapse" data-target="#infoCalendarTraderoom"></span>
                            <div id="infoCalendarTraderoom" class="collapse">
                                <div class="impact-container">
                                    <div id="impact3" class="choose-importance marked col-importace"><iron-icon class="importance-icon style-scope mte-calendar-event x-scope iron-icon-1" id="importanceIcon"><svg viewBox="0 0 24 24" preserveAspectRatio="xMidYMid meet" class="style-scope iron-icon" style="pointer-events: none; display: block; width: 100%; height: 100%;"><g class="style-scope iron-icon"><path d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z" class="style-scope iron-icon"></path></g></svg></iron-icon></div>
                                    <div id="impact2" class="choose-importance marked col-importace"><iron-icon class="importance-icon style-scope mte-calendar-event x-scope iron-icon-1" id="importanceIcon"><svg viewBox="0 0 24 24" preserveAspectRatio="xMidYMid meet" class="style-scope iron-icon" style="pointer-events: none; display: block; width: 100%; height: 100%;"><g class="style-scope iron-icon"><path d="M22 9.74l-7.19-.62L12 2.5 9.19 9.13 2 9.74l5.46 4.73-1.64 7.03L12 17.77l6.18 3.73-1.63-7.03L22 9.74zM12 15.9V6.6l1.71 4.04 4.38.38-3.32 2.88 1 4.28L12 15.9z" class="style-scope iron-icon"></path></g></svg></iron-icon></div>
                                    <div id="impact1" class="choose-importance marked col-importace"><iron-icon class="importance-icon style-scope mte-calendar-event x-scope iron-icon-1" id="importanceIcon"><svg viewBox="0 0 24 24" preserveAspectRatio="xMidYMid meet" class="style-scope iron-icon" style="pointer-events: none; display: block; width: 100%; height: 100%;"><g class="style-scope iron-icon"><path d="M22 9.24l-7.19-.62L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21 12 17.27 18.18 21l-1.63-7.03L22 9.24zM12 15.4l-3.76 2.27 1-4.28-3.32-2.88 4.38-.38L12 6.1l1.71 4.04 4.38.38-3.32 2.88 1 4.28L12 15.4z" class="style-scope iron-icon"></path></g></svg></iron-icon></div>
                                </div>
                            <div class="flag-row">
                                <div class="flag-item">
                                    <label class="container-checkbox">
                                        <input type="checkbox" name="chooseFlagAud" id="flag-icon-aud" class="flag_checkbox" checked="checked"><span class="checkmark"></span>
                                    </label>
                                    <div class="flag-image aud"></div>
                                    <span class="currency-name">Aud</span>
                                </div>
                                <div class="flag-item">
                                    <label class="container-checkbox">
                                        <input type="checkbox" name="chooseFlagCad" id="flag-icon-cad" class="flag_checkbox" checked="checked"><span class="checkmark"></span>
                                    </label>
                                    <div class="flag-image cad"></div>
                                    <span class="currency-name">Cad</span>
                                </div>
                                <div class="flag-item">
                                    <label class="container-checkbox">
                                        <input type="checkbox" name="chooseFlagCny" id="flag-icon-cny" class="flag_checkbox" checked="checked"><span class="checkmark"></span>
                                    </label>
                                    <div class="flag-image cny"></div>
                                    <span class="currency-name">CNY</span>
                                </div>
                                <div class="flag-item">
                                    <label class="container-checkbox">
                                        <input type="checkbox" name="chooseFlagJpy" id="flag-icon-jpy" class="flag_checkbox" checked="checked"><span class="checkmark"></span>
                                    </label>
                                    <div class="flag-image jpy"></div>
                                    <span class="currency-name">JPY</span>
                                </div>
                                <div class="flag-item">
                                    <label class="container-checkbox">
                                        <input type="checkbox" name="chooseFlagNzd" id="flag-icon-nzd" class="flag_checkbox" checked="checked"><span class="checkmark"></span>
                                    </label>
                                    <div class="flag-image nzd"></div>
                                    <span class="currency-name">NZD</span>
                                </div>
                                <div class="flag-item">
                                    <label class="container-checkbox">
                                        <input type="checkbox" name="chooseFlagChf" id="flag-icon-chf" class="flag_checkbox" checked="checked"><span class="checkmark"></span>
                                    </label>
                                    <div class="flag-image chf"></div>
                                    <span class="currency-name">CHF</span>
                                </div>
                                <div class="flag-item">
                                    <label class="container-checkbox">
                                        <input type="checkbox" name="chooseFlagEur" id="flag-icon-eur" class="flag_checkbox" checked="checked"><span class="checkmark"></span>
                                    </label>
                                    <div class="flag-image eur"></div>
                                    <span class="currency-name">EUR</span>
                                </div>
                                <div class="flag-item">
                                    <label class="container-checkbox">
                                        <input type="checkbox" name="chooseFlagGbp" id="flag-icon-gbp" class="flag_checkbox" checked="checked"><span class="checkmark"></span>
                                    </label>
                                    <div class="flag-image gbp"></div>
                                    <span class="currency-name">GBP</span>
                                </div>
                                <div class="flag-item">
                                    <label class="container-checkbox">
                                        <input type="checkbox" name="chooseFlagUsd" id="flag-icon-usd" class="flag_checkbox" checked="checked"><span class="checkmark"></span>
                                    </label>
                                    <div class="flag-image usd"></div>
                                    <span class="currency-name">USD</span>
                                </div>
                            </div>
                        </div>
                        <!-- Day Tab panes -->
                        <div class="scrollable-area scrollable-area-at-top">
                            <div class="scrollable-area-body">
                                <div class="scrollable-area-content">
                                    <div id="mte-calendar-traderoom">
                                        <?php include( ABSPATH . 'wp-content/themes/tol-parent/layouts/MTE/traderoom-mte/economic-calendar.php' ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- End Economic Calendar -->
                </div>
            </div>
            <div id="market-analysis" class="tab-pane fade">
            <header class="entry-header">
                <h1 class="entry-title news-title">Market Analysis</h1>
            </header>
            <div class="scrollable-area scrollable-area-at-top">
                <div class="scrollable-area-body">
                    <div class="scrollable-area-content">
                        <div id="market-analysis-container">
                        <?php include( ABSPATH . 'wp-content/themes/tol-parent/layouts/MTE/traderoom-mte/market-analysis.php' ); ?>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        <?php } else { ?>
        <div class="tab-pane active in" id="tab2">
            <div class="tol-messages-sidebar">
                <h1 class="messages-title" data-translate="userDataMessages"></h1>
                <div class="scrollable-area scrollable-area-at-top">
                    <div class="scrollable-area-body">
                        <div class="scrollable-area-content">
                            <div class="clearfix flat-top-borders">
                                <?php echo do_shortcode('[messageInboxAllMessages]'); ?>
                                <?php echo do_shortcode('[messageInboxContent]'); ?>
                                <div class="pagination-wrapper">
                                    <?php echo do_shortcode('[messageInboxPaging]'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
<div class="separator horizontal-separator middle-separator easyForex-middle-separator">
    <span class="separator-collapse">
        <span class="separator-arrow"></span>
    </span>
</div>
<div class="separator vertical-separator side-separator">
    <span class="separator-collapse">
        <span class="separator-arrow"></span>
    </span>
</div>

<script data-cfasync="false" type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/tradingview/charting_library/charting_library.min.js"></script>
<script data-cfasync="false" type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/tradingview/datafeeds/datafeed.js"></script>
<script type="text/javascript">

    const initializeChart = function() {
        $(window).on('loadingIsCompleted', function (event) {
            const waitingForOptions = 100;
            let checkOptions = setTimeout(function waitForOptions (){
                const tradeOptions = easyForexHelper.options;
                const stillWaiting = tradeOptions.length === 0;
                if (stillWaiting) {
                    checkOptions = setTimeout(waitForOptions,waitingForOptions);
                }
                if (!stillWaiting) {
                    tradingViewWidget = window.tvWidget = new TradingView.widget({
                        theme: migesco.tradingviewThemes.darkLayout,
                        autosize: true,
                        symbol: tradeOptions[0].name,
                        interval: 1,
                        container_id: "tv_chart_container",
                        datafeed: new Datafeeds.UDFCompatibleDatafeed("https://advfeed.finte.co/tradingviewservices.asmx", 1000),
                        library_path: "<?php echo get_template_directory_uri(); ?>/tradingview/charting_library/",
                        locale: "<?php echo prior_lang() ?>",
                        drawings_access: {
                            type: 'black',
                            tools: [
                                {
                                    name: "Regression Trend"
                                }
                            ]
                        },
                        disabled_features: [
                            "create_volume_indicator_by_default",
                            "footer_screenshot",
                            "header_screenshot",
                            "header_interval_dialog_button",
                            "header_symbol_search",
                            "symbol_search_hot_key",
                            "border_around_the_chart",
                            "uppercase_instrument_names",
                            "constraint_dialogs_movement",
                        ],
                        enabled_features: [
                            "side_toolbar_in_fullscreen_mode",
                            "caption_buttons_text_if_possible",
                            "lock_visible_time_range_on_resize",
                            "study_templates",
                            "no_min_chart_width",
                            "timeframes_toolbar",
                            "same_data_requery",
                        ],
                        charts_storage_url: 'https://saveload.tradingview.com',
                        charts_storage_api_version: "1.1",
                        supports_search: false,
                        client_id: 'tradingview.com',
                        user_id: 'public_user',
                        overrides: {
                            "timezone": "Europe/Moscow"
                        },
                    });

                    $("#tv_chart_container").parent().addClass('loaded');
                }
            }, waitingForOptions);
        });
    };
    window.addEventListener('DOMContentLoaded', initializeChart, false);


</script>
<div class="ui-widget-overlay"></div>
