<script type="text/javascript">
    $(document).ready(function(){
        $.cookie("game", 'simplex', {path: "/"});
        migesco.setSimplexPhone();

        $('body').removeClass('white-theme');

        $('#livechat-compact-container').remove();
		
		$('.tol-wp-navigation .menu-item').removeClass('current-menu-item');
        $('.mobile-simplex').addClass('current-menu-item');
		
		$('#userbarmenu').css('height', $(document).innerHeight());
        $('#mobilemenu').css('height', $(document).innerHeight());


        $('.boxfilter-mobile-wrapper .button').on('click touch', function(){
            if (!$('.boxfilter-mobile-wrapper').hasClass('open')) {
                $('.boxfilter-mobile-wrapper').removeClass('open');
                $('.boxfilter-mobile-wrapper .button').attr('aria-expanded', 'false');
            } else {
                $('.boxfilter-mobile-wrapper').addClass('open');
                $('.boxfilter-mobile-wrapper .button').attr('aria-expanded', 'true');
            };
        });

        $('.mfilter-el').click(function() {
            $('.mobile-currFilter').html($(this)[0].innerHTML);
        });

        if ($(document).width() < 768) {
            $('.boxfilter').remove();
        } else {
            $('.boxfilter-mobile-wrapper').remove();
        };

        // $('.tradearea').css('height', $('.tradewrapper').height() - ($(document).width() < 768 ? 44 : 59));

        // $(window).resize(function(){
        //     $('.tradearea').css('height', $('.tradewrapper').height() - ($(document).width() < 768 ? 44 : 59));
        // });

        $('.footer-icon-positions > a').click(function(e) {
            if (!widgets.isLogged()) {
                e.stopPropagation();
                $('#tol-login-popup').modal('toggle');
            };

            if ($(this).parent().hasClass('active')) {
                return;
            } else {
                if ($('#balance-positions').html() == '') {
                    $('#balance-positions').html($('#balance-orders').html());
                    $('#balance-orders').html('');
                };
            };

            setTimeout(function() {
                if ($('.balance-toggle[data-target="#balance-positions"]').hasClass('collapsed')) {
                    $('.positions-active .scrollable-area').css('height', $('#content').height() - 67 - $('#trades .fakeTableHead').height());
                } else {
                    $('.positions-active .scrollable-area').css('height', $('#content').height() - $('.equity-container').height() - 67 - $('#trades .fakeTableHead').height());
                };

                $(".positions-active .scrollable-area-content").mCustomScrollbar("destroy");
                $(".positions-active .scrollable-area-content").mCustomScrollbar({
                    scrollButtons: {
                        enable: false
                    },
                    horizontalScroll: false,
                    autoDraggerLength: true,
                    contentTouchScroll: true,
                    scrollInertia: 600,
                    autoHideScrollbar: true,
                    advanced: {
                        autoScrollOnFocus: false,
                        updateOnContentResize: true,
                        updateOnBrowserResize: true
                    }
                });
            }, 300);
        });

        $('.footer-icon-orders > a').click(function(e) {
            if (!widgets.isLogged()) {
                e.stopPropagation();
                $('#tol-login-popup').modal('toggle');
            };

            if ($(this).parent().hasClass('active')) {
                return;
            } else {
                if ($('#balance-orders').html() == '') {
                    $('#balance-orders').html($('#balance-positions').html());
                    $('#balance-positions').html('');
                };
            };

            setTimeout(function(){
                if ($('.balance-toggle[data-target="#balance-orders"]').hasClass('collapsed')) {
                    $('.orders-active .scrollable-area').css('height', $('#content').height() - $('.forexPendingOrders').height() - 67);
                } else {
                    $('.orders-active .scrollable-area').css('height', $('#content').height() - $('.equity-container').height() - $('.forexPendingOrders').height() - 67);
                };

                $(".orders-active .scrollable-area-content").mCustomScrollbar("destroy");
                $(".orders-active .scrollable-area-content").mCustomScrollbar({
                    scrollButtons: {
                        enable: false
                    },
                    horizontalScroll: false,
                    autoDraggerLength: true,
                    contentTouchScroll: true,
                    scrollInertia: 600,
                    autoHideScrollbar: true,
                    advanced: {
                        autoScrollOnFocus: false,
                        updateOnContentResize: true,
                        updateOnBrowserResize: true
                    }
                });
            }, 300);
        });

        $(".column-settings").click(function(e){
            e.stopPropagation();
            if ($(this).hasClass('show')) {
                $(this).removeClass('show');

                if ($(this).hasClass('tab-easy-closed-positions')) {
                    if ($('.balance-toggle[data-target="#balance-orders"]').hasClass('collapsed')) {
                        $('.orders-active .scrollable-area').css('height', $('#content').height() - $('.forexPendingOrders').height() - 67);
                    } else {
                        $('.orders-active .scrollable-area').css('height', $('#content').height() - $('.equity-container').height() - $('.forexPendingOrders').height() - 67);
                    };

                    $(".orders-active .scrollable-area-content").mCustomScrollbar("destroy");
                    $(".orders-active .scrollable-area-content").mCustomScrollbar({
                        scrollButtons: {
                            enable: false
                        },
                        horizontalScroll: false,
                        autoDraggerLength: true,
                        contentTouchScroll: true,
                        scrollInertia: 600,
                        autoHideScrollbar: true,
                        advanced: {
                            autoScrollOnFocus: false,
                            updateOnContentResize: true,
                            updateOnBrowserResize: true
                        }
                    });
                } else {
                    if ($('.balance-toggle[data-target="#balance-positions"]').hasClass('collapsed')) {
                        $('.positions-active .scrollable-area').css('height', $('#content').height() - 67 - $('#trades .fakeTableHead').height());
                    } else {
                        $('.positions-active .scrollable-area').css('height', $('#content').height() - $('.equity-container').height() - 67 - $('#trades .fakeTableHead').height());
                    };

                    $(".positions-active .scrollable-area-content").mCustomScrollbar("destroy");
                    $(".positions-active .scrollable-area-content").mCustomScrollbar({
                        scrollButtons: {
                            enable: false
                        },
                        horizontalScroll: false,
                        autoDraggerLength: true,
                        contentTouchScroll: true,
                        scrollInertia: 600,
                        autoHideScrollbar: true,
                        advanced: {
                            autoScrollOnFocus: false,
                            updateOnContentResize: true,
                            updateOnBrowserResize: true
                        }
                    });
                }
            } else {
                $(this).addClass('show');
            };
            return;
        });

        $(document).click(function(){
            if ($(".column-settings").hasClass('show')) {
                $(".column-settings").removeClass('show');

                if ($('.tab-pane.active .column-settings').hasClass('tab-easy-closed-positions')) {
                    if ($('.balance-toggle[data-target="#balance-orders"]').hasClass('collapsed')) {
                        $('.orders-active .scrollable-area').css('height', $('#content').height() - $('.forexPendingOrders').height() - 67);
                    } else {
                        $('.orders-active .scrollable-area').css('height', $('#content').height() - $('.equity-container').height() - $('.forexPendingOrders').height() - 67);
                    };

                    $(".orders-active .scrollable-area-content").mCustomScrollbar("destroy");
                    $(".orders-active .scrollable-area-content").mCustomScrollbar({
                        scrollButtons: {
                            enable: false
                        },
                        horizontalScroll: false,
                        autoDraggerLength: true,
                        contentTouchScroll: true,
                        scrollInertia: 600,
                        autoHideScrollbar: true,
                        advanced: {
                            autoScrollOnFocus: false,
                            updateOnContentResize: true,
                            updateOnBrowserResize: true
                        }
                    });
                } else {
                    if ($('.balance-toggle[data-target="#balance-positions"]').hasClass('collapsed')) {
                        $('.positions-active .scrollable-area').css('height', $('#content').height() - 67 - $('#trades .fakeTableHead').height());
                    } else {
                        $('.positions-active .scrollable-area').css('height', $('#content').height() - $('.equity-container').height() - 67 - $('#trades .fakeTableHead').height());
                    };

                    $(".positions-active .scrollable-area-content").mCustomScrollbar("destroy");
                    $(".positions-active .scrollable-area-content").mCustomScrollbar({
                        scrollButtons: {
                            enable: false
                        },
                        horizontalScroll: false,
                        autoDraggerLength: true,
                        contentTouchScroll: true,
                        scrollInertia: 600,
                        autoHideScrollbar: true,
                        advanced: {
                            autoScrollOnFocus: false,
                            updateOnContentResize: true,
                            updateOnBrowserResize: true
                        }
                    });
                }
            };
            $(".nav-button.has-icon.main-menu").removeClass('show');
        });

        $('.balance-toggle').click(function() {
            if ($(this).hasClass('collapsed')) {
                if ($(this).attr('data-target') == '#balance-positions') {
                    $('.positions-active .scrollable-area').css('height', $('#content').height() - $('.equity-container').height() - 67 - $('#trades .fakeTableHead').height());

                    $(".positions-active .scrollable-area-content").mCustomScrollbar("destroy");
                    $(".positions-active .scrollable-area-content").mCustomScrollbar({
                        scrollButtons: {
                            enable: false
                        },
                        horizontalScroll: false,
                        autoDraggerLength: true,
                        contentTouchScroll: true,
                        scrollInertia: 600,
                        autoHideScrollbar: true,
                        advanced: {
                            autoScrollOnFocus: false,
                            updateOnContentResize: true,
                            updateOnBrowserResize: true
                        }
                    });
                } else {
                    $('.orders-active .scrollable-area').css('height', $('#content').height() - $('.equity-container').height() - $('.forexPendingOrders').height() - 67);

                    $(".orders-active .scrollable-area-content").mCustomScrollbar("destroy");
                    $(".orders-active .scrollable-area-content").mCustomScrollbar({
                        scrollButtons: {
                            enable: false
                        },
                        horizontalScroll: false,
                        autoDraggerLength: true,
                        contentTouchScroll: true,
                        scrollInertia: 600,
                        autoHideScrollbar: true,
                        advanced: {
                            autoScrollOnFocus: false,
                            updateOnContentResize: true,
                            updateOnBrowserResize: true
                        }
                    });
                }
            } else {
                if ($(this).attr('data-target') == '#balance-positions') {
                    $('.positions-active .scrollable-area').css('height', $('#content').height() - 67 - $('#trades .fakeTableHead').height());

                    $(".positions-active .scrollable-area-content").mCustomScrollbar("destroy");
                    $(".positions-active .scrollable-area-content").mCustomScrollbar({
                        scrollButtons: {
                            enable: false
                        },
                        horizontalScroll: false,
                        autoDraggerLength: true,
                        contentTouchScroll: true,
                        scrollInertia: 600,
                        autoHideScrollbar: true,
                        advanced: {
                            autoScrollOnFocus: false,
                            updateOnContentResize: true,
                            updateOnBrowserResize: true
                        }
                    });
                } else {
                    $('.orders-active .scrollable-area').css('height', $('#content').height() - $('.forexPendingOrders').height() - 67);

                    $(".orders-active .scrollable-area-content").mCustomScrollbar("destroy");
                    $(".orders-active .scrollable-area-content").mCustomScrollbar({
                        scrollButtons: {
                            enable: false
                        },
                        horizontalScroll: false,
                        autoDraggerLength: true,
                        contentTouchScroll: true,
                        scrollInertia: 600,
                        autoHideScrollbar: true,
                        advanced: {
                            autoScrollOnFocus: false,
                            updateOnContentResize: true,
                            updateOnBrowserResize: true
                        }
                    });
                }
            }
        });

        $('.info-icon').tooltip();

       

        

        //defaultSettings btn on the easy trades tables
        $('#tol-trades-popup .tabeasytrades .defaultSettings').on('click', function (event) {

            $('#tol-trades-popup .tabeasytrades .multiple-selection-content .item').addClass('selected');
            $.each(myEasyForexTradesColumns, function (i, isVisible) {
                if (isVisible === true){
                    //hide header column
                    $("#tol-trades-popup .my-trades-table.easy-trades-table th:nth-child(" + i +")").show();
                    //hide rows
                    $("#tol-trades-popup .my-trades-table.easy-trades-table td:nth-child("+ i +")").show();
                } else{
                    //hide header column
                    $("#tol-trades-popup .my-trades-table.easy-trades-table th:nth-child(" + i +")").hide();
                    //hide rows
                    $("#tol-trades-popup .my-trades-table.easy-trades-table td:nth-child("+ i +")").hide();

                    $("#tol-trades-popup .tabeasytrades .multiple-selection-content .item:nth-child("+ i +")").removeClass('selected');
                };
            });
            widgets.myEasyForexTradesColumns = $.extend(true, {}, myEasyForexTradesColumnsDefault);
            if($.cookie('myEasyForexTradesColumns')){
                $.cookie('myEasyForexTradesColumns',JSON.stringify(widgets.myEasyForexTradesColumns));
            }else{
                $.cookie('myEasyForexTradesColumns',JSON.stringify(widgets.myEasyForexTradesColumns), {path: "/"});
            }
        });

        //defaultSettings btn on the trades tables
        $('.account .tabpositions .defaultSettings').on('click', function (event) {

            $('.account .tabpositions .multiple-selection-content .item').addClass('selected');
            $.each(openPositionsColumnsDefault, function (i, isVisible) {
                if (isVisible === true){
                    //hide header column
                    $("#trades .dataTable th:nth-child(" + i +")").show();
                    //hide rows
                    $("#trades .dataTable td:nth-child("+ i +")").show();
                } else{
                    $("#trades .dataTable th:nth-child(" + i +")").hide();
                    //hide rows
                    $("#trades .dataTable td:nth-child("+ i +")").hide();
                    $(".account .tabpositions .multiple-selection-content .item:nth-child("+ i +")").removeClass('selected');
                }
            });
            widgets.openPositionsColumns = $.extend(true, {}, openPositionsColumnsDefault);

            // set cookie
            if($.cookie('openPositionsColumns')){
                $.cookie('openPositionsColumns',JSON.stringify(widgets.openPositionsColumns));
            }else{
                $.cookie('openPositionsColumns',JSON.stringify(widgets.openPositionsColumns), {path: "/"});
            }

        });

        //easy Forex open positions default settings
        $('.account .tabeasypositions .defaultSettings').on('click', function (event) {

            $('.account .tabeasypositions .multiple-selection-content .item').addClass('selected');
            $.each(easyOpenPositionsColumnsDefault, function (i, isVisible) {
                if (isVisible === true){
                    $("#trades .dataTable th:nth-child(" + i +")").show();
                    if (widgets.isMobileDevice()) {
                        $("#trades .dataTable div[data-type='openPositionsRow'] > div:nth-child("+ i +")").show();
                    } else {
                        $("#trades .dataTable td:nth-child("+ i +")").show();
                    };
                } else{
                    $("#trades .dataTable th:nth-child(" + i +")").hide();
                    if (widgets.isMobileDevice()) {
                        $("#trades .dataTable div[data-type='openPositionsRow'] > div:nth-child("+ i +")").show();
                    } else {
                        $("#trades .dataTable td:nth-child("+ i +")").hide();
                    };
                    $(".account .tabeasypositions .multiple-selection-content .item:nth-child("+ i +")").removeClass('selected');
                }
            });
            widgets.easyOpenPositionsColumns = $.extend(true, {}, easyOpenPositionsColumnsDefault);

            // set cookie
            if($.cookie('easyOpenPositionsColumns')){
                $.cookie('easyOpenPositionsColumns',JSON.stringify(widgets.easyOpenPositionsColumns));
            }else{
                $.cookie('easyOpenPositionsColumns',JSON.stringify(widgets.easyOpenPositionsColumns), {path: "/"});
            };
        });

        //easy Forex closed positions default settings
        $('.account .tab-easy-closed-positions .defaultSettings').on('click', function (event) {

            $('.account .tab-easy-closed-positions .multiple-selection-content .item').addClass('selected');
            $.each(easyClosedPositionsColumnsDefault, function (i, isVisible) {
                if (isVisible === true){
                    $("#pastInvestments .dataTable th:nth-child(" + i +")").show();
                    $("#pastInvestments .dataTable td:nth-child("+ i +")").show();
                } else{
                    $("#pastInvestments .dataTable th:nth-child(" + i +")").hide();
                    $("#pastInvestments .dataTable td:nth-child("+ i +")").hide();
                    $(".account .tab-easy-closed-positions .multiple-selection-content .item:nth-child("+ i +")").removeClass('selected');
                }
            });
            widgets.easyClosedPositionsColumns = $.extend(true, {}, easyClosedPositionsColumnsDefault);

            // set cookie
            if($.cookie('easyClosedPositionsColumns')){
                $.cookie('easyClosedPositionsColumns',JSON.stringify(widgets.easyClosedPositionsColumns));
            }else{
                $.cookie('easyClosedPositionsColumns',JSON.stringify(widgets.easyClosedPositionsColumns), {path: "/"});
            };
        });

        //defaultSettings btn on the trades tables
        $('.account .taborders .defaultSettings').on('click', function (event) {

            $('.account .taborders .multiple-selection-content .item').addClass('selected');
            $.each(pendingOrdersColumnsDefault, function (i, isVisible) {
                if (isVisible === true){
                    //hide header column
                    $("#orders .dataTable th:nth-child(" + i +")").show();
                    //hide rows
                    $("#orders .dataTable td:nth-child("+ i +")").show();
                }
                else{
                    //hide header column
                    $("#orders .dataTable th:nth-child(" + i +")").hide();
                    //hide rows
                    $("#orders .dataTable td:nth-child("+ i +")").hide();

                    $(".account .taborders .multiple-selection-content .item:nth-child("+ i +")").removeClass('selected');
                }
            });
            widgets.pendingOrdersColumns = $.extend(true, {}, pendingOrdersColumnsDefault);
            if($.cookie('pendingOrdersColumns')){
                $.cookie('pendingOrdersColumns',JSON.stringify(widgets.pendingOrdersColumns));
            }else{
                $.cookie('pendingOrdersColumns',JSON.stringify(widgets.pendingOrdersColumns), {path: "/"});
            }
        });

        //Footer settings icon
        $('.footer-settings-icon .multiple-selection-content .item').on('click', function (event) {
            var elToHide = $(this).attr('data-column');
            event.stopPropagation();

            if(elToHide==1) return;
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
                if (widgets.footerSettings){
                    widgets.footerSettings[elToHide] = false;
                }
                $('.equity-content .equity-item:nth-child('+(elToHide)+')').hide();
            } else {
                $(this).addClass('selected');
                if (widgets.footerSettings){
                    widgets.footerSettings[elToHide] = true;
                }
                $('.equity-content .equity-item:nth-child('+(elToHide)+')').show();
            };

            // set cookie
            if($.cookie('footerSettings')){
                $.cookie('footerSettings',JSON.stringify(widgets.footerSettings));
            }else{
                $.cookie('footerSettings',JSON.stringify(widgets.footerSettings), {path: "/"});
            }
        });

        $('.footer-settings-icon .defaultSettings').on('click', function (event) {
            $('.equity-item').show();
            $('.footer-settings-icon .multiple-selection-content .item').addClass('selected');
            widgets.footerSettings = footerSettingsDefault;
            // set cookie
            if($.cookie('footerSettings')){
                $.cookie('footerSettings',JSON.stringify(footerSettingsDefault));
            }else{
                $.cookie('footerSettings',JSON.stringify(footerSettingsDefault), {path: "/"});
            }
        });
        //End footer settings icon

        $('.mobile-switch-btn').click(function() {
            if ($(this).hasClass('active')) {
                return;
            } else if ($(this).hasClass('inv-switch')) {
                $('.mobile-switch-btn').removeClass('active');
                $(this).addClass('active');
                $('.chartwrapper.loaded').removeClass('showmobile');
            } else {
                $('.mobile-switch-btn').removeClass('active');
                $(this).addClass('active');
                $('.chartwrapper.loaded').addClass('showmobile');
            };
        });
    });
</script>
<div class="layout" id="layout">
    <div class="traderoom-loader">
        <div class="loader">
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
        </div>
    </div>
    <ul class="footer-nav nav nav-tabs">
        <li class="footer-icon-quotes active">
            <a data-toggle="tab" href="#home" data-translate="Quotes"></a>
        </li>
        <li class="footer-icon-positions">
            <a data-toggle="tab" href="#menu1" data-translate="openPositions"></a>
        </li>
        <li class="footer-icon-orders">
            <a data-toggle="tab" href="#menu2" data-translate="ClosedPositions"></a>
        </li>
        <li class="footer-icon-more">
            <a data-toggle="tab" href="#menu3" data-translate="More"></a>
        </li>
    </ul>

    <div class="tab-content">
        <div id="home" class="tab-pane fade in active">
            <div class="tradewrapper" data-widget="easyForex-easyForex" data-type="tradewrapper">
                <div class="boxfilter" data-widget="easyForex-easyForex" data-type="filter" <?php echo($GLOBALS['curr_product'] == 'crypto' ? 'data-categories="CryptoCoins,CryptoTokens"' : ''); ?>>
                    <div class="filter-el active" data-filter="All" data-translate="filterAll"></div>
                    <div class="filter-el" data-filter="Favourites"><span data-translate="traderoomFavourites"></span> <span class="fav-counter" data-widget="easyForex-easyForex" data-type="fav-counter" style="display: none;"></span></div>
                    <div class="filter-el" data-filter="Currencies" data-translate="traderoomCurrencies"></div>
                    <div class="filter-el" data-filter="CryptoCoins" data-translate="traderoomCryptoCoins"></div>
                    <div class="filter-el" data-filter="CryptoTokens" data-translate="traderoomCryptoTokens"></div>
                    <div class="filter-el" data-filter="Commodities" data-translate="traderoomCommodities"></div>
                    <div class="filter-el" data-filter="Futures" data-translate="traderoomIndices"></div>
                    <div class="filter-el" data-filter="Stocks" data-translate="traderoomStocks"></div>
                </div>
                <div class="boxfilter-mobile-wrapper">
                    <div type="button" class="nav-button button less-than" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="mobile-currFilter"></span>
                        <span class="dropdown-arrow"></span>
                    </div>
                    <ul class="dropdown-menu" data-widget="easyForex-easyForex" data-type="filter">
                        <li class="mfilter-el" data-filter="All" data-translate="filterAll"></li>
                        <li class="mfilter-el" data-filter="Favourites"><span data-translate="traderoomFavourites"></span> <span class="fav-counter" data-widget="easyForex-easyForex" data-type="fav-counter" style="display: none;"></span></li>
                        <li class="mfilter-el" data-filter="Currencies" data-translate="traderoomCurrencies"></li>
                        <li class="mfilter-el" data-filter="CryptoCoins" data-translate="traderoomCryptoCoins"></li>
                        <li class="mfilter-el" data-filter="CryptoTokens" data-translate="traderoomCryptoTokens"></li>
                        <li class="mfilter-el" data-filter="Commodities" data-translate="traderoomCommodities"></li>
                        <li class="mfilter-el" data-filter="Futures"  data-translate="traderoomIndices"></li>
                        <li class="mfilter-el" data-filter="Stocks" data-translate="traderoomStocks"></li>
                    </ul>
                </div>
                <div data-widget="easyForex-easyForex" data-type="assetsList" class="tradearea"></div>
                <div data-widget="easyForex-easyForex" data-type="paging" data-paginglimit="10" data-prevNextPage="true" class="pagination pagination-sm"></div>
            </div>
        </div>
        <div id="menu1" class="tab-pane fade">
            <button type="button" class="balance-toggle" data-toggle="collapse" data-target="#balance-positions" aria-expanded="true" data-translate="userDataBalance"></button>
            <div id="balance-positions" class="collapse in" aria-expanded="true">
                <div class="equity-container">
                    <div class="equity-content">
                        <div class="equity-item balance-item" id="equity-ppl">
                            <span class="label" data-translate="userDataBalance"></span>
                            <span data-widget="balancev2" data-type="balance" class="value blue"></span>
                        </div>
                        <div class="equity-item avbalance-item" id="equity-total">
                            <span class="label" data-translate="availableBalance"></span>
                            <span data-widget="balancev2" data-type="availableBalance" class="value"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pending-orders-title" data-translate="openPositions"></div>
            <div class="account panel orders-trades-table-wrapper positions-active" id="accountPanel">
                <div class="tabHolder">
                    <span class="column-settings tabeasypositions">
                    <div class="settings"></div>
                    <div class="contextmenu multiple-selection datatable-contextmenu _focusable leftCenter" tabindex="-1">
                        <div class="multiple-selection-content">
                            <div class="item item-datatable-contextmenu-trade" data-column="1">
                                <span data-translate="tradeID"></span>
                                <div class="list-checkbox"></div>
                            </div>
                            <div class="item item-datatable-contextmenu-open-time" data-column="2">
                                <span data-translate="openTime"></span>
                                <div class="list-checkbox"></div>
                            </div>
                            <div class="item item-datatable-contextmenu-quantity selected" data-column="3">
                                <span data-translate="asset"></span>
                                <div class="list-checkbox"></div>
                            </div>
                            <div class="item item-datatable-contextmenu-direction selected" data-column="4">
                                <span data-translate="type"></span>
                                <div class="list-checkbox"></div>
                            </div>
                            <div class="item item-datatable-contextmenu-averagePrice selected" data-column="5">
                                <span data-translate="openPrice"></span>
                                <div class="list-checkbox"></div>
                            </div>
                            <div class="item item-datatable-contextmenu-currentPrice selected" data-column="6">
                                <span data-translate="investAmount"></span>
                                <div class="list-checkbox"></div>
                            </div>
                            <div class="item item-datatable-contextmenu-limitPrice selected" data-column="7">
                                <span data-translate="risk"></span>
                                <div class="list-checkbox"></div>
                            </div>
                            <div class="item item-datatable-contextmenu-stopPrice selected" data-column="8">
                                <span data-translate="stopLoss"></span>
                                <div class="list-checkbox"></div>
                            </div>
                            <div class="item item-datatable-contextmenu-margin selected" data-column="9">
                                <span data-translate="takeProfit"></span>
                                <div class="list-checkbox"></div>
                            </div>
                            <div class="item item-datatable-contextmenu-swap selected" data-column="10">
                                <span data-translate="marketPrice"></span>
                                <div class="list-checkbox"></div>
                            </div>
                            <div class="item item-datatable-contextmenu-created" data-column="11">
                                <span data-translate="swap"></span>
                                <div class="list-checkbox"></div>
                            </div>
                            <div class="item item-datatable-contextmenu-ppl selected" data-column="12">
                                <span data-translate="profit"></span>
                                <div class="list-checkbox"></div>
                            </div>
                            <div class="item item-datatable-contextmenu-ppl selected" data-column="13">
                                <span data-translate="Return"></span>
                                <div class="list-checkbox"></div>
                            </div>
                            <div class="btn-wrapper navigation">
                                <div class="nav-button defaultSettings" data-translate="defaultSettings"></div>
                            </div>
                        </div>
                    </div>
                </span>
            </div>
                <div id="trades">
                    <div class="dataTable">
                        <table class="fakeTableHead" data-widget="easyForex-easyForexOpenPositions">
                            <thead>
                            <tr data-type="easyForexOpenPositions-header">
                                <th data-column="tradeID" data-sort="int" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" data-translate="tradeID"></span></div>
                                </th>
                                <th data-column="openTime" data-sort="int" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" data-translate="openTime"></span></div>
                                </th>
                                <th data-column="asset" data-sort="string" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" data-translate="asset"></span></div>
                                </th>
                                <th data-column="type" data-sort="float" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" data-translate="type"></span></div>
                                </th>
                                <th data-column="openPrice" data-sort="float" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" data-translate="openPrice"></span></div>
                                </th>
                                <th data-column="investAmount" data-sort="float" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" data-translate="investAmount"></span></div>
                                </th>
                                <th data-column="risk" data-sort="float" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" data-translate="risk"></span></div>
                                </th>
                                <th data-column="stopLoss" data-sort="float" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" data-translate="stopLoss"></span></div>
                                </th>
                                <th data-column="takeProfit" data-sort="float" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" data-translate="takeProfit"></span></div>
                                </th>
                                <th data-column="marketPrice" data-sort="float" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" data-translate="marketPrice"></span></div>
                                </th>
                                <th data-column="swap" data-sort="float" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" data-translate="swap"></span></div>
                                </th>
                                <th data-column="return" data-sort="string" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" data-translate="profit"></span></div>
                                </th>
                                <th data-column="profit" data-sort="string" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" data-translate="return"></span></div>
                                </th>
                                <th data-column="actions" data-sort="string" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" data-translate="actions"></span></div>
                                </th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="scrollable-area scrollable-area-at-top">
                    <div class="scrollable-area-body">
                        <div class="scrollable-area-content">
                            <div id="trades">
                                <div class="dataTable">
                                    <table id="forexProOpenTradesTable" class="hasArrows" data-widget="easyForex-easyForexOpenPositions">
                                        <tbody data-widget="easyForex-easyForexOpenPositions" data-type="table"></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="menu2" class="tab-pane fade">
            <button type="button" class="balance-toggle" data-toggle="collapse" data-target="#balance-orders" aria-expanded="true" data-translate="userDataBalance"></button>
            <div id="balance-orders" class="collapse in" aria-expanded="true"></div>
            <div class="pending-orders-title" data-translate="ClosedPositions"></div>
            <div class="account panel orders-trades-table-wrapper orders-active">
                <div class="tabHolder">
                    <span class="column-settings tab-easy-closed-positions">
                        <div class="settings"></div>
                        <div class="contextmenu multiple-selection datatable-contextmenu _focusable leftCenter" tabindex="-1">
                            <div class="multiple-selection-content">
                                <div class="item item-datatable-contextmenu-trade selected" data-column="1">
                                    <span data-translate="tradeID"></span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="item item-datatable-contextmenu-name selected" data-column="2">
                                    <span data-translate="openTime"></span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="item item-datatable-contextmenu-quantity selected" data-column="3">
                                    <span data-translate="asset"></span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="item item-datatable-contextmenu-direction selected" data-column="4">
                                    <span data-translate="type"></span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="item item-datatable-contextmenu-averagePrice selected" data-column="5">
                                    <span data-translate="openPrice"></span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="item item-datatable-contextmenu-currentPrice selected" data-column="6">
                                    <span data-translate="investAmount"></span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="item item-datatable-contextmenu-limitPrice selected" data-column="7">
                                    <span data-translate="risk"></span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="item item-datatable-contextmenu-stopPrice selected" data-column="8">
                                    <span data-translate="stopLoss"></span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="item item-datatable-contextmenu-margin selected" data-column="9">
                                    <span data-translate="takeProfit"></span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="item item-datatable-contextmenu-swap selected" data-column="10">
                                    <span data-translate="closeTime"></span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="item item-datatable-contextmenu-swap selected" data-column="11">
                                    <span data-translate="closePrice"></span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="item item-datatable-contextmenu-created selected" data-column="12">
                                    <span data-translate="swap"></span>
                                    <div class="list-checkbox"></div>
                                </div>
                                <div class="item item-datatable-contextmenu-created selected" data-column="13">
                                    <span data-translate="commissions"></span>
                                    <div class="list-checkbox"></div>
                                </div>
                            <div class="item item-datatable-contextmenu-ppl selected" data-column="14">
                                <span data-translate="Return"></span>
                                <div class="list-checkbox"></div>
                            </div>
                            <div class="item item-datatable-contextmenu-ppl selected" data-column="15">
                                <span data-translate="profit"></span>
                                <div class="list-checkbox"></div>
                            </div>
                                <div class="btn-wrapper navigation">
                                    <div class="nav-button defaultSettings" data-translate="defaultSettings"></div>
                                </div>
                            </div>
                        </div>
                    </span>
                </div>
                <div id="pastInvestments">
                    <div class="dataTable">
                        <div class="forexPendingOrders">
                            <table class="fakeTableHead" data-widget="easyForex-easyForexClosedPositions">
                                <thead>
                                <tr data-type="easyForexClosedPositions-header">
                                    <th data-column="tradeID" data-sort="int" class="name canSort canResize canDrag">
                                        <div class="cellWrapper"><span class="content" data-translate="tradeID"></span></div>
                                    </th>
                                    <th data-column="openTime" data-sort="int" class="name canSort canResize canDrag">
                                        <div class="cellWrapper"><span class="content" data-translate="openTime"></span></div>
                                    </th>
                                    <th data-column="asset" data-sort="string" class="name canSort canResize canDrag">
                                        <div class="cellWrapper"><span class="content" data-translate="asset"></span></div>
                                    </th>
                                    <th data-column="type" data-sort="float" class="name canSort canResize canDrag">
                                        <div class="cellWrapper"><span class="content" data-translate="type"></span></div>
                                    </th>
                                    <th data-column="openPrice" data-sort="float" class="name canSort canResize canDrag">
                                        <div class="cellWrapper"><span class="content" data-translate="openPrice"></span></div>
                                    </th>
                                    <th data-column="investAmount" data-sort="float" class="name canSort canResize canDrag">
                                        <div class="cellWrapper"><span class="content" data-translate="investAmount"></span></div>
                                    </th>
                                    <th data-column="risk" data-sort="float" class="name canSort canResize canDrag">
                                        <div class="cellWrapper"><span class="content" data-translate="risk"></span></div>
                                    </th>
                                    <th data-column="stopLoss" data-sort="float" class="name canSort canResize canDrag">
                                        <div class="cellWrapper"><span class="content" data-translate="stopLoss"></span></div>
                                    </th>
                                    <th data-column="takeProfit" data-sort="float" class="name canSort canResize canDrag">
                                        <div class="cellWrapper"><span class="content" data-translate="takeProfit"></span></div>
                                    </th>
                                    <th data-column="closeTime" data-sort="float" class="name canSort canResize canDrag">
                                        <div class="cellWrapper"><span class="content" data-translate="closeTime"></span></div>
                                    </th>
                                    <th data-column="closePrice" data-sort="float" class="name canSort canResize canDrag">
                                        <div class="cellWrapper"><span class="content" data-translate="closePrice"></span></div>
                                    </th>
                                    <th data-column="swap" data-sort="float" class="name canSort canResize canDrag">
                                        <div class="cellWrapper"><span class="content" data-translate="swap"></span></div>
                                    </th>
                                    <th data-column="commissions" data-sort="float" class="name canSort canResize canDrag">
                                        <div class="cellWrapper"><span class="content" data-translate="commissions"></span></div>
                                    </th>
                                    <th data-column="profit" data-sort="string" class="name canSort canResize canDrag">
                                        <div class="cellWrapper"><span class="content" data-translate="return"></span></div>
                                    </th>
                                    <th data-column="return" data-sort="string" class="name canSort canResize canDrag">
                                        <div class="cellWrapper"><span class="content" data-translate="profit"></span></div>
                                    </th>
                                    <th data-column="actions" data-sort="string" class="name canSort canResize canDrag">
                                        <div class="cellWrapper"><span class="content" data-translate="actions"></span></div>
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="scrollable-area scrollable-area-at-top" style="height: 298px;">
                    <div class="scrollable-area-body">
                        <div class="scrollable-area-content">
                            <div id="orders">
                                <div class="dataTable">
                                    <div class="forexPendingOrders">
                                        <table id="easyForexClosedTradesTable" class="hasArrows" data-widget="easyForex-easyForexClosedPositions">
                                            <tbody data-widget="easyForex-easyForexClosedPositions" data-type="table"></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="menu3" class="tab-pane fade">
            <div class="sidepanel-tabs focusable" id="sidePanel" tabindex="-1">
                <ul class="nav right-sidebar-nav">
                    <li class="messages-tab active">
                        <a href="#tab2" data-toggle="tab" class="active">
                            <span data-translate="userDataMessages"></span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="tab2">
                        <div class="tol-messages-sidebar">
                            <div class="scrollable-area scrollable-area-at-top">
                                <div class="scrollable-area-body">
                                    <div class="scrollable-area-content">
                                        <div class="clearfix flat-top-borders">
                                            <?php echo do_shortcode('[messageInboxAllMessages]'); ?>
                                            <?php echo do_shortcode('[messageInboxContent]'); ?>
                                            <div class="pagination-wrapper"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="tol-trade-popup" data-widget="easyForex-easyForex" data-type="popupcontainer" data-taid="" data-keyboard="false" data-backdrop="static" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="tol-trade-label">
    <div class="modal-dialog modal-sm modal-default" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-widget="easyForex-easyForex" data-type="closepopup">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body widgetPlaceholder">
                <div class="front">
                    <div data-widget="easyForex-easyForex" data-type="topinfo" class="left-part">
                        <div class="flag-container">
                            <div class="left-flag"></div>
                            <div class="right-flag"></div>
                        </div>
                        <div class="tradearea-box-title"></div>
                    </div>
                    <div class="right-part">
                        <div class="tradearea-box-price">
                            <span class="tradearea-box-price-title" data-translate="easyForexPrice"></span>
                            <span data-type="averageprice" data-direction="down" class="tradearea-box-price-value down"></span>
                        </div>
                        <div class="tradearea-box-change">
                            <span class="tradearea-box-change-title" data-translate="dailyChange"></span>
                            <span data-type="dailychange" class="tradearea-box-change-value"></span>
                        </div>
                    </div>
                </div>
                <div class="mobile-switch" style="display: none;">
                    <div class="inv-switch mobile-switch-btn active" data-translate="investment"></div>
                    <div class="chart-switch mobile-switch-btn" data-translate="easy-chart"></div>
                </div>
                <div class="header-text" data-widget="easyForex-easyForex" data-type="compare-text">
                </div>
                <div class="btn-wrapper">
                    <div data-widget="easyForex-easyForex" data-type="upbtn" data-direction="up" class="upbtn" data-translate="up"></div>
                    <div data-widget="easyForex-easyForex" data-type="downbtn" data-direction="down" class="downbtn" data-translate="down"></div>
                </div>
                <div data-widget="easyForex-easyForex" data-type="popupcontrollerscontainer" class="controllers-wrapper inactive">
                    <div class="cover-controllers"></div>
                    <div class="ddl-container">
                        <label data-translate="investment-amount"></label>
                        <div data-widget="easyForex-easyForex" data-type="investment-element" type="button" class="nav-button button ddl-ctrl" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span data-widget="easyForex-easyForex" data-type="investment-selected"></span>
                            <span class="dropdown-arrow"></span>
                        </div>
                        <input type="number" onkeypress="return event.charCode >= 48 && event.charCode <= 57" data-widget="easyForex-easyForex" data-type="investmentInput" class="ddl-ctrl" style="display: none;" />
                        <ul data-widget="easyForex-easyForex" data-type="investment-list" class="dropdown-menu"></ul>
                    </div>
                    <label><span data-translate="risk-sensitivity"></span> <a href="#" class="info-icon" data-toggle="tooltip" data-placement="top" data-widget="easyForex-easyForex" data-type="tooltip-riskSensitivity"></a></label>
                    <div class="risk-sens-btns">
                        <div data-widget="easyForex-easyForex" data-type="riskBtn" data-risk="0" class="lowbtn" data-translate="low"></div>
                        <div data-widget="easyForex-easyForex" data-type="riskBtn" data-risk="1" class="medbtn" data-translate="medium"></div>
                        <div data-widget="easyForex-easyForex" data-type="riskBtn" data-risk="2" class="highbtn" data-translate="high"></div>
                    </div>

                    <div class="ddl-container">
                        <label><span data-translate="takeProfit"></span> <a href="#" class="info-icon" data-toggle="tooltip" data-placement="top"  data-widget="easyForex-easyForex" data-type="tooltip-takeProfit"></a></label>
                        <div data-widget="easyForex-easyForex" data-type="takeprofit-element" type="button" class="nav-button button ddl-ctrl" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span data-widget="easyForex-easyForex" data-type="takeprofit-selected"></span>
                            <span class="dropdown-arrow"></span>
                        </div>
                        <input type="number" onkeypress="return event.charCode >= 48 && event.charCode <= 57" data-widget="easyForex-easyForex" data-type="takeProfitInput" class="ddl-ctrl" style="display: none;" />
                        <ul data-widget="easyForex-easyForex" data-type="takeprofit-list" class="dropdown-menu"></ul>
                    </div>
                    <div class="ddl-container">
                        <label><span data-translate="stopLoss"></span> <a href="#" class="info-icon" data-toggle="tooltip" data-placement="top" data-widget="easyForex-easyForex" data-type="tooltip-stopLoss"></a></label>
                        <div data-widget="easyForex-easyForex" data-type="stoploss-element" type="button" class="nav-button button ddl-ctrl" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span data-widget="easyForex-easyForex" data-type="stoploss-selected"></span>
                            <span class="dropdown-arrow"></span>
                        </div>
                        <input type="number" onkeypress="return event.charCode >= 48 && event.charCode <= 57" data-widget="easyForex-easyForex" data-type="stopLossInput" class="ddl-ctrl" style="display: none;" />
                        <ul data-widget="easyForex-easyForex" data-type="stoploss-list" class="dropdown-menu"></ul>
                    </div>
                    <div class="footer-info">
                        <div class="info-block">
                            <span><span data-translate="easy-trade-value"></span> <a href="#" style="margin-left: 18px;" class="info-icon" data-toggle="tooltip" data-placement="top" data-widget="easyForex-easyForex" data-type="tooltip-tradeValue" ></a> <span data-widget="easyForex-easyForex" data-type="trade-value"></span></span>
                        </div>
                        <div class="info-block">
                            <span><span data-translate="easy-commission"></span>: <a href="#" data-widget="easyForex-easyForex" data-type="tooltip-commission" class="info-icon" data-toggle="tooltip" data-placement="top"></a> <span data-widget="easyForex-easyForex" data-type="commission"></span></span>
                        </div>
                        <div class="info-block">
                            <span>
                                <span data-translate="easy-pointvalue"></span>
                                <a href="#" style="margin-left: 19px;" data-widget="easyForex-easyForex" data-type="tooltip-pointValue" class="info-icon" data-toggle="tooltip" data-placement="top"></a>
                                <span data-widget="easyForex-easyForex" data-type="pointValue"></span>
                            </span>
                        </div>
                    </div>
                    <div data-widget="easyForex-easyForex" data-type="invest" class="invest-btn" data-translate="invest"></div>
                </div>
            </div>
            <div class="modal-footer"></div>
            <div class="toggleChart" data-widget="easyForex-easyForex" data-type="chart-toggle" data-translate="easy-chart"></div>
        </div>
        <div class="chartwrapper">
            <div id="tv_chart_container"></div>
        </div>
    </div>
</div>
<!-- END TRADE POP UP -->


<!-- Trading View chart library starts here -->
<script data-cfasync="false" type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/tradingview/charting_library/charting_library.min.js"></script>
<script data-cfasync="false" type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/tradingview/datafeeds/datafeed.js"></script>
<script type="text/javascript">
    const initializeChart = function() {
        $(window).on('loadingIsCompleted', function (event) {
            const waitingForOptions = 100;
            let checkOptions = setTimeout(function waitForOptions (){
                const tradeOptions = easyForexHelper.options;
                const stillWaiting = tradeOptions.length === 0;
                if (stillWaiting) {
                    checkOptions = setTimeout(waitForOptions,waitingForOptions);
                }
                if (!stillWaiting) {
                    tradingViewWidget = window.tvWidget = new TradingView.widget({
                        theme: migesco.tradingviewThemes.darkLayout,
                        autosize: true,
                        symbol: tradeOptions[0].name,
                        interval: 1,
                        container_id: "tv_chart_container",
                        datafeed: new Datafeeds.UDFCompatibleDatafeed("https://advfeed.finte.co/tradingviewservices.asmx", 1000),
                        library_path: "<?php echo get_template_directory_uri(); ?>/tradingview/charting_library/",
                        locale: "<?php echo prior_lang() ?>",
                        drawings_access: {
                            type: 'black',
                            tools: [
                                {
                                    name: "Regression Trend"
                                }
                            ]
                        },
                        disabled_features: [
                            "create_volume_indicator_by_default",
                            "footer_screenshot",
                            "header_screenshot",
                            "header_interval_dialog_button",
                            "header_symbol_search",
                            "symbol_search_hot_key",
                            "border_around_the_chart",
                            "uppercase_instrument_names",
                            "constraint_dialogs_movement",
                        ],
                        enabled_features: [
                            "side_toolbar_in_fullscreen_mode",
                            "caption_buttons_text_if_possible",
                            "lock_visible_time_range_on_resize",
                            "study_templates",
                            "no_min_chart_width",
                            "timeframes_toolbar",
                            "same_data_requery",
                        ],
                        charts_storage_url: 'https://saveload.tradingview.com',
                        charts_storage_api_version: "1.1",
                        supports_search: false,
                        client_id: 'tradingview.com',
                        user_id: 'public_user',
                        overrides: {
                            "timezone": "Europe/Moscow"
                        },
                    });

                    $("#tv_chart_container").parent().addClass('loaded');
                }
            }, waitingForOptions);
        });
    };
    window.addEventListener('DOMContentLoaded', initializeChart, false);
</script>
<style>
    #tv_chart_container{visibility:hidden;height:100%;}
    #tv_chart_container iframe {height:100% !important;}
    .panel.loaded{border:0;}
    .panel.loaded #tv_chart_container{visibility:visible}
</style>
<div class="ui-widget-overlay"></div>
