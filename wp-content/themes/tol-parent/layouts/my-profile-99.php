<div id="tol-myProfile-layout-99">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-2 form-container justify-content-center">
                <form name="foo">
                    <div class="form-group header">
                        <h3 class="text-left" data-translate="UserDataProfile"></h3>
                    </div>
                    <div class="avatar-container">
                        <label  class="lfl current-avatar" data-translate="current_avatar"></label>
                        <img src="/wp-content/themes/tol-child/images/new-template-99/avatars/male-1.png" alt="avatar" class="img-responsive">
                        <span class="edit-btn"></span>
                        <a href="#"><img src="/wp-content/themes/tol-child/images/new-template-99/edit.png" alt="edit-btn" class="img-responsive edit-btn"></a>
                    </div>
                    <div class="form-group input-container">
                        <label for="email" class="lfl" name="email"  data-translate="Registration_Email"></label>
                        <input type="text" data-widget="userDetails" data-type="email" data-isRequired="true" data-regexpMessage="' . $a['data-regexpMessage'] . '"class="form-control" id="email" disabled required/>
                    </div>
                    <div class="form-group input-container">
                        <input type="text" data-widget="userDetails" data-type="firstName" data-isRequired="true" data-minLength="' . $a['data-minLength'] . '" class="form-control" id="first-name" required/>
                        <label for="first-name" name="first-name" class="lfl" data-translate="Registration_FirstName"></label>
                    </div>
                    <div class="form-group input-container">
                        <input type="text" data-widget="userDetails" data-type="lastName" data-isRequired="true" data-minLength="' . $a['data-minLength'] . '" class="form-control" id="last-name" required/>
                        <label for="last-name" class="lfl" name="last-name" data-translate="Registration_LastName"></label>
                    </div>
                    <div class="form-group input-container">
                        <label for="about-me" class="lfl about" name="about-me" data-translate="about_me"></label>
                        <textarea name="about-me" id="about-me" class="form-control  form-rounded" cols="30" rows="4"></textarea>
                    </div>
                    <div class="tos-error">
                    <div data-widget="userDetails" data-type="error" style="display: none"></div>
                    </div>
                    <?php if (defined('ENABLE_SOCIAL_IN_REGISTRATION') && ENABLE_SOCIAL_IN_REGISTRATION != false) { ?>
                        <div class="form-group social-logins">
                            <div data-widget="loginFacebook" onclick="$('#tol-login-popup').modal('hide');" data-type="fbLoginBtn" class="btn fb-login social" data-translate="loginFb"></div>
                            <div data-widget="loginFacebook" data-type="popUpElements" data-registration-widgets="title,firstName,lastName,birth,email,primaryPhone,languageCode,countryCode,currency,password,tos" class="loginFacebookWidget registrationPopUp" style="display:none"></div>

                            <div data-widget="loginGoogle" data-type="googleLoginBtn" onclick="$('#tol-login-popup').modal('hide');" class="btn google-login social" data-translate="loginGplus"></div>
                            <div data-widget="loginGoogle" data-type="popUpElements" data-registration-widgets="email,password,title,firstName,lastName,languageCode,countryCode,birth,primaryPhone,currency,tos" class="loginGoogleWidget registrationPopUp" style="display:none"></div>
                        </div>
        
                    <?php } ?>
                    <div class="form-group change-password-con tainer">
                    <div class="btn btn-success text-center btn-main change-password"><a href="../change-password" data-translate="Change_Password"></a></div>
                    </div>
                    <div class="form-group  save-changes-container">
                    <input type="button" data-widget="userDetails" data-type="submit" data-translate="saveChanges" class="btn btn-success text-center btn-main save-changes" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>