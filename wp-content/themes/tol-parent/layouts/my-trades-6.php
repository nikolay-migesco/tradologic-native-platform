<script type="text/javascript">
    $(window).on('loadingIsCompleted', function () {
        $('.my-trades-table thead th:nth-child(1),.my-trades-table thead th:nth-child(3),.my-trades-table thead th:nth-child(5),.my-trades-table thead th:nth-child(7)').addClass('hidden-xs');
        setTimeout(function () {
            $('.my-trades-table tr td:nth-child(1),.my-trades-table tr td:nth-child(3),.my-trades-table tr td:nth-child(5),.my-trades-table tr td:nth-child(7)').addClass('hidden-xs');
        }, 2000);
    });
</script>
<div id="tol-myTrades-layout-1" class="container">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="container">
            <div class="pageHeadTitle">
                <?php require_once(ABSPATH.'wp-content/themes/tol-parent/layouts/'.BREADCRUMB.'.php' ); ?>
            </div>
            <header class="entry-header">
                <h1 class="entry-title"><?php the_title(); ?></h1>
            </header>
            <form action="" class="tol-form-inline">
                <div class="row">
                    <div class="well">
                        <div class="row mytrades-profit-row">
                             <div class="form-group col-xs-12 col-sm-2">
                                 <div class="row">
                                    <label class="mytrades-label gamefilter-label" data-translate="MyTrades_GameFilter"></label>
                                    <div class="col-sm-12 mytrades-gamefilter nopadding-left">
                                        <?php echo do_shortcode('[myTradesGameFilter class="form-control btn-default form-control-md"]'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-2">
                                <div class="row">
                                    <label class="mytrades-label" data-translate="TradeRoom_Assets"></label>
                                    <div class="col-xs-12 col-sm-12 nopadding-left">
                                        <?php echo do_shortcode('[myTradesOptions class="form-control btn-default form-control-md"]'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-xs-6 col-sm-2">
                                <div class="row">
                                    <label class="mytrades-label" data-translate="MyOptions_From"></label>
                                    <div class="col-sm-12 my-trades-from-date nopadding-left">
                                        <?php echo do_shortcode('[myTradesFromDate  data-past="30" class="form-control nopadding-right"]'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-xs-6 col-sm-2">
                                <div class="row">
                                    <label class="mytrades-label" data-translate="MyOptions_To"></label>
                                    <div class="col-sm-12 nopadding-left">
                                        <?php echo do_shortcode('[myTradesToDate class="form-control nopadding-right"]'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-2">
                                <div class="row">
                                    <label class="mytrades-label" data-translate="copyBoardtrades"></label>
                                    <div class="col-sm-12 nopadding-left">
                                        <?php echo do_shortcode('[myTradesSocialFilter class="form-control btn-default form-control-md"]'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-xs-8 col-sm-2 mytrades-search">
                                <?php echo do_shortcode('[myTradesSearchButton translate="MyTrades_SearchBtn" class="btn btn-primary col-xs-12"]'); ?>
                            </div>
                               <div class="form-group col-xs-8 col-sm-1">
                               &nbsp;
                            </div>
                        </div>
                        <hr class="mytrades-hr">
                        <div class="row mytrades-profit-row">
                            <div class="col-xs-12 col-sm-3">
                                <div class="row mytrades-profit-row">
                                    <label class="mytrades-label" data-translate="Trades_Volume"></label>
                                    <div class="col-sm-12 nopadding-left">
                                        <?php echo do_shortcode('[myTradesVolume class="form-control"]'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-3">
                                <div class="row mytrades-profit-row">
                                    <label class="mytrades-label" data-translate="Trades_Profit"></label>
                                    <div class="col-sm-12 nopadding-left">
                                        <?php echo do_shortcode('[myTradesProfit class="form-control"]'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-3">
                                <div class="row mytrades-profit-row">
                                    <label class="mytrades-label" data-translate="Trades_Volume"></label>
                                    <div class="col-sm-12 col-xs-12 nopadding-left">
                                        <?php echo do_shortcode('[myTradesTotalTrades class="form-control"]'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-3">
                                <div class="row mytrades-profit-row">
                                    <div class="col-sm-12 col-xs-12 nopadding-left">&nbsp;
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo do_shortcode('[myTradesTable class="table table-striped my-trades-table"]'); ?>
                    <div class="text-right">

                        <?php echo do_shortcode('[myTradesPaging paginglimit="10" class="pagination pagination-sm"]'); ?>
                    </div>
                </div>
            </form>
            <div class="entry-content">
                <?php echo $post->post_content; ?>

            </div>
    </article>
</div>