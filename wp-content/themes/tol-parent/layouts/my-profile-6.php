<div id="tol-myProfile-layout-1">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div id="form-wrapper" class="container-fluid compliance-widget-wrapper">
            <div class="col-sm-offset-1 col-sm-10 no-padding-all">
                <div class="pageHeadTitle">
                    <?php require_once(ABSPATH.'wp-content/themes/tol-parent/layouts/'.BREADCRUMB.'.php' ); ?>
                </div>
                <header class="entry-header">
                    <h1 class="entry-title"><?php the_title(); ?></h1>
                </header>
                <div data-widget="compliance" data-type="main">
                    <div class="row myProfileHeader">
                        <ul class="nav tabs nav-wizard">
                            <li class="col-sm-4 nopadding">
                                <a href="javascript:;" class="compliance-tab col-sm-9" data-widget="compliance" data-type="menu" data-target="userdetails">Personal Details</a>
                            </li>
                            <li class="col-sm-4 nopadding">
                                <a href="javascript:;" class="col-sm-9 compliance-tab quest" data-widget="compliance" data-type="menu" data-target="questionnaires">Questionnaires</a>
                            </li>
                            <li class="col-sm-4 nopadding">
                                <a href="javascript:;" class="compliance-tab" data-widget="compliance" data-type="menu" data-target="documents">Upload Documents</a>
                            </li>
                        </ul>
                    </div>
                    <div data-widget="compliance" data-type="section" data-item="userdetails" class="compliance-userdetails compliance-container row well">
                        <!-- Start My Profile -->
                        <!-- | #67794 | 02.08.16 | ptraycheva -->
                        <div class="my-profile-title"></div>
                        <div class="myprofile-successmsg"></div>
                        <?php echo do_shortcode('[userDetailsError]'); ?>
                        <div class="row tol-my-profile-column">
                            <div class="col-sm-6 firstClmn">
                                <div class="form-group col-sm-12">
                                    <a id="mobilescroll"></a>
                                    <label class="hidden-xs col-sm-12 col-md-4 control-label text-left" data-translate="Registration_Email"></label>
                                    <div class="col-sm-12 col-md-8">
                                        <?php echo do_shortcode('[userDetailsEmail class="form-control"]'); ?>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label class="hidden-xs col-sm-12 col-md-4 control-label text-left" data-translate="Registration_Title"></label>
                                    <div class="col-sm-12 col-md-8">
                                        <?php echo do_shortcode('[userDetailsETitle class="form-control"]'); ?>
                                    </div>
                                </div>
                                <div class="form-group col-xs-5 col-sm-12">
                                    <label class="hidden-xs col-sm-12 col-md-4 control-label text-left" data-translate="Registration_FirstName"></label>
                                    <div class="col-sm-12 col-md-8">
                                        <?php echo do_shortcode('[userDetailsFirstName class="form-control"]'); ?>
                                    </div>
                                </div>
                                <div class="form-group col-xs-7 col-sm-12">
                                    <label class="hidden-xs col-sm-12 col-md-4 control-label text-left" data-translate="Registration_LastName"></label>
                                    <div class="col-sm-12 col-md-8">
                                        <?php echo do_shortcode('[userDetailsLastName class="form-control"]'); ?>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label class="hidden-xs col-sm-12 col-md-4 control-label text-left" data-translate="Registration_Country"></label>
                                    <div class="col-sm-12 col-md-8">
                                        <?php echo do_shortcode('[userDetailsCountryCode class="form-control"]'); ?>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label class="hidden-xs col-sm-12 col-md-4 control-label text-left" data-translate="Registration_DateOfBirth"></label>
                                    <div class="col-sm-12 col-md-8">
                                        <div class="col-xs-3 col-sm-2 nopadding">
                                            <?php echo do_shortcode('[userDetailsBirthDay class="form-control my-profile-birth-day"]'); ?>
                                        </div>
                                        <div class="col-xs-6 col-sm-6">
                                            <?php echo do_shortcode('[userDetailsBirthMonth class="form-control"]'); ?>
                                        </div>
                                        <div class="col-xs-3 col-sm-4 nopadding">
                                            <?php echo do_shortcode('[userDetailsBirthYear class="form-control"]'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group col-sm-12">
                                    <label class="hidden-xs col-sm-12 col-md-4 control-label text-left" data-translate="Registration_Street"></label>
                                    <div class="col-sm-12 col-md-8">
                                        <?php echo do_shortcode('[userDetailsAddressLine1 class="form-control"]'); ?>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label class="hidden-xs col-sm-12 col-md-4 control-label text-left" data-translate="Registration_HouseNum"></label>
                                    <div class="col-sm-12 col-md-8">
                                        <?php echo do_shortcode('[userDetailsAddressLine2 class="form-control"]'); ?>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label class="hidden-xs col-sm-12 col-md-4 control-label text-left" data-translate="Registration_City"></label>
                                    <div class="col-sm-12 col-md-8">
                                        <?php echo do_shortcode('[userDetailsCity class="form-control"]'); ?>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label class="hidden-xs col-sm-12 col-md-4 control-label text-left" data-translate="Registration_Phone1"></label>
                                    <div class="col-sm-12 col-md-8">
                                        <div class="col-xs-2 col-sm-2 nopadding">
                                            <?php echo do_shortcode('[userDetailsCountryPhoneCode class="form-control"]'); ?>
                                        </div>
                                        <div class="col-xs-4 col-sm-4">
                                            <?php echo do_shortcode('[userDetailsAreaPhoneCode class="form-control"]'); ?>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 nopadding">
                                            <?php echo do_shortcode('[userDetailsPhone class="form-control"]'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label class="hidden-xs col-sm-6 col-md-4 control-label text-left" data-translate="Registration_Phone2"></label>
                                    <div class="col-sm-12 col-md-8">
                                        <?php echo do_shortcode('[userDetailsSecondaryPhone class="form-control"]'); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">&nbsp;</div>
                            <div class="col-sm-6">
                                <div class="form-group col-sm-12">
                                    <label class="hidden-xs col-sm-12 col-md-4 control-label text-left">&nbsp;</label>
                                    <div class="col-sm-12 col-md-8">
                                        <button class="tol-quick-submit col-sm-12 btn tol-submit-my-profile" data-widget="userDetails" data-type="submit" data-translate="Button_Submit"></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- End My Profile -->
                    </div>

                    <div data-widget="compliance" data-type="section" data-item="questionnaires" class="compliance-questionnaire compliance-container row" style="display: none;">
                        <div class="questionnaireguide" data-translate="questionnareGuide"></div>
                        <div class="questionnaireerror" data-type="questionnaireerror" style="display: none"></div>
                        <div data-widget="compliance" data-type="questions" class="compliance-questions"></div>
                    </div>

                    <div class="compliance-documents compliance-container row" data-widget="compliance" data-type="section" data-item="documents" style="display: none;">
                        <div class="documentsguide" data-translate="missingdocuments"></div>
                        <div id="docsguide-upload" class="documentsguide">Upload Document</div>

                        <div class="documenthint"></div>
                        <div data-widget="compliance" data-type="documentsupload"></div>

                        <table class="table table-striped table-responsive">
                            <thead class="doctable">
                            <tr>
                                <th data-translate="documentName"></th>
                                <th data-translate="documentType"></th>
                                <th data-translate="documentState"></th>
                            </tr>
                            </thead>
                            <tbody class="uploadeddocs" data-widget="compliance" data-item="uploadedDocs"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="entry-content">
            <?php echo $post->post_content; ?>
        </div>
    </article>
</div>