<div class="tab-pane" id="verification" role="tabpanel">
            <div class="container">
                <div class="row verification-wrapper">
                    <h1 data-translate="userDataMyProfile"></h1>
                    <div class="container compliance-widget-wrapper">
                        <div data-widget="compliance" data-type="main">
                            <ul class="nav my-profile-tabs">
                                <li class="col-sm-4 nopadding">
                                    <a href="javascript:;" class="compliance-tab active" data-widget="compliance" data-type="menu" data-target="userdetails" data-translate="Registration_PersonalDetails"></a>
                                </li>
                                <li class="col-sm-4 nopadding">
                                    <a href="javascript:;" class="compliance-tab quest" data-widget="compliance" data-type="menu" data-target="questionnaires" data-translate="Registration_Questionaries"></a>
                                </li>
                                <li class="col-sm-4 nopadding">
                                    <a href="javascript:;" class="compliance-tab" data-widget="compliance" data-type="menu" data-target="documents" data-translate="Registration_Upload"></a>
                                </li>
                            </ul>
                            <div data-widget="compliance" data-type="section" data-item="userdetails" class="compliance-userdetails compliance-container row justify-content-center">
                                <div class="col-sm-2 form-container justify-content-center">
                                    <?php echo do_shortcode('[userDetailsError]'); ?>
                                    <form name="foo">
                                        <div class="form-group header">
                                            <h3 class="text-left" data-translate="UserDataProfile"></h3>
                                        </div>
                                        <div class="form-group input-container">
                                            <label for="email" class="lfl" name="email"  data-translate="Registration_Email"></label>
                                            <input type="text" data-widget="userDetails" data-type="email" data-isRequired="true" data-regexpMessage="Please enter a valid email address" class="form-control" id="email" disabled/>
                                        </div>
                                        <div class="form-group input-container avatar-container" style="display: none;">
                                            <label class="lfl" data-translate="Registration_Avatar"></label>
                                            <div type="text" data-widget="userDetails" data-type="avatar" data-isRequired="false" class="avatar-wrapper" style="display: none"></div>
                                        </div>
                                        <div class="form-group input-container">
                                            <label class="lfl" for="userDetailsTitle" name="userDetailsTitle" data-translate="Registration_Title"></label>
                                            <select data-widget="userDetails" data-type="title" id="userDetailsTitle" class="form-control"></select>
                                        </div>
                                        <div class="form-group input-container">
                                            <label for="first-name" name="first-name" class="lfl" data-translate="Registration_FirstName"></label>
                                            <input type="text" data-widget="userDetails" data-type="firstName" data-isRequired="true" data-minLength="3" class="form-control" id="first-name" />
                                        </div>
                                        <div class="form-group input-container">
                                            <label for="last-name" class="lfl" name="last-name" data-translate="Registration_LastName"></label>
                                            <input type="text" data-widget="userDetails" data-type="lastName" data-isRequired="true" data-minLength="3" class="form-control" id="last-name" />
                                        </div>
                                        <div class="form-group input-container">
                                            <label class="lfl" for="userDetailsNation" name="userDetailsNation" data-translate="Registration_Nationality"></label>
                                            <select data-widget="userDetails" data-type="nationalityId" data-isrequired="false" id="userDetailsNation" class="form-control"></select>
                                        </div>
                                        <!--<div class="form-group input-container">
                                            <label class="lfl" for="jurisdictionCountryCode" name="jurisdictionCountryCode" data-translate="Registration_Jurisdiction"></label>
                                            <select data-widget="userDetails" data-type="JurisdictionCountryCode" data-isRequired="true" id="jurisdictionCountryCode" class="form-control"></select>
                                        </div>-->
                                        <div class="form-group input-container">
                                            <label class="lfl" for="birthDay" name="birthDay" data-translate="Registration_DateOfBirth"></label>
                                            <div id="birthDay" style="height: 38px;">
                                                <div style="width: 25%;float:left;">
                                                    <select data-widget="userDetails" data-type="birthDay" class="form-control my-profile-birth-day"></select>
                                                </div>
                                                <div style="width:41.66666666666667%;padding: 0 15px;float:left;">
                                                    <select data-widget="userDetails" data-type="birthMonth" class="form-control"></select>
                                                </div>
                                                <div style="width: 33.33333333333333%;float:left;">
                                                    <select data-widget="userDetails" data-type="birthYear" data-regexpmessage="Please enter your date of birth" class="form-control"></select>
                                                </div>
                                            </div>
                                        </div>
                                        <!--<div class="form-group input-container">
                                            <label class="lfl" data-translate="Registration_IDCard"></label>
                                            <input type="text" placeholder="" data-translate="Registration_IDCard" data-widget="userDetails" data-type="IDCard" data-isRequired="true" data-minLength="6" class="form-control" />
                                        </div>
                                        <div class="form-group input-container">
                                            <label class="lfl othercountry" name="identificationNumber" for="identificationNumber" data-translate="Registration_TIN"></label>
                                            <label class="lfl bgcountry" name="identificationNumber" for="identificationNumber" data-translate="Registration_EGN" style="display: none;"></label>
                                            <input type="text" id="identificationNumber" placeholder="" data-translate="Registration_TIN" data-widget="userDetails" data-type="IdentificationNumber" data-isRequired="true" data-minLength="6" class="form-control" />
                                        </div>-->
                                        <div class="form-group input-container">
                                            <label class="lfl" for="userDetailsAddress1" name="userDetailsAddress1" data-translate="Registration_Street"></label>
                                            <input type="text" data-widget="userDetails" data-type="addressLine1" id="userDetailsAddress1" class="form-control" />
                                        </div>
                                        <div class="form-group input-container">
                                            <label class="lfl" for="userDetailsAddress2" name="userDetailsAddress2" data-translate="Registration_HouseNum"></label>
                                            <input type="text" data-widget="userDetails" data-type="addressLine2" id="userDetailsAddress2" class="form-control" />
                                        </div>
                                        <div class="form-group input-container">
                                            <label class="lfl" for="userDetailsCity" name="userDetailsCity" data-translate="Registration_City"></label>
                                            <input type="text" data-widget="userDetails" data-type="city" id="userDetailsCity" class="form-control" />
                                        </div>
                                        <div class="form-group input-container">
                                            <label class="lfl" for="phone" name="phone" data-translate="Registration_Phone1"></label>
                                            <?php echo do_shortcode('[userDetailsCountryPhoneCode class="form-control"]'); ?>
                                            <?php echo do_shortcode('[userDetailsAreaPhoneCode class="form-control"]'); ?>
                                            <input id="phone" data-decorate="flags" type="text" data-widget="userDetails" data-type="phone" data-regexpmessage="Please enter your Primary Phone" class="form-control" autocomplete="off" />
                                        </div>
                                        <div class="form-group input-container">
                                            <label class="lfl" for="phone-2" name="phone-2" data-translate="Registration_Phone2"></label>
                                            <input id="phone-2" type="text" data-widget="userDetails" data-type="secondaryPhone" data-regexpmessage="Please enter your Secondary phone/Mobile" class="form-control" autocomplete="off">
                                        </div>
                                        <div class="form-group input-container">
                                            <label class="lfl" for="ccode" name="ccode" data-translate="Registration_Country"></label>
                                            <select id="ccode" data-widget="userDetails" data-type="countryCode" data-isrequired="" class="form-control"></select>
                                        </div>
                                        <div class="tos-error">
                                            <div data-widget="userDetails" data-type="error" style="display: none"></div>
                                        </div>
                                        <div class="form-group change-password-container">
                                            <div class="btn btn-success text-center btn-main change-password"><a href="../change-password" data-translate="Change_Password"></a></div>
                                        </div>
                                        <div class="form-group  save-changes-container">
                                            <input type="button" data-widget="userDetails" data-type="submit" data-translate="saveChanges" class="btn btn-success text-center btn-main save-changes" />
                                        </div>
                                        <div class="form-group social-logins">
                                            <div data-widget="loginFacebook" onclick="$('#tol-login-popup').modal('hide');" data-type="fbLoginBtn" class="btn fb-login social" data-translate="loginFb"></div>
                                            <div data-widget="loginFacebook" data-type="popUpElements" data-registration-widgets="title,firstName,lastName,birth,email,primaryPhone,languageCode,countryCode,currency,password,tos" class="loginFacebookWidget registrationPopUp" style="display:none"></div>

                                            <div data-widget="loginGoogle" data-type="googleLoginBtn" onclick="$('#tol-login-popup').modal('hide');" class="btn google-login social" data-translate="loginGplus"></div>
                                            <div data-widget="loginGoogle" data-type="popUpElements" data-registration-widgets="email,password,title,firstName,lastName,languageCode,countryCode,birth,primaryPhone,currency,tos" class="loginGoogleWidget registrationPopUp" style="display:none"></div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div data-widget="compliance" data-type="section" data-item="questionnaires" class="compliance-questionnaires compliance-container row" style="display: none;">
                                <div class="questionnaireguide" data-translate="questionnareGuide"></div>
                                <div class="questionnaireerror" data-type="questionnaireerror" style="display: none"></div>
                                <div data-widget="compliance" data-type="questions" class="compliance-questions"></div>
                            </div>
                            <div class="compliance-documents compliance-container row" data-widget="compliance" data-type="section" data-item="documents" style="display: none;">
                                <div class="documentsguide" data-translate="missingdocuments"></div>
                                <div id="docsguide-upload" class="documentsguide">Upload Document</div>
                                <div class="documenthint"></div>
                                <div data-widget="compliance" data-type="documentsupload"></div>
                                <table class="table table-striped table-responsive">
                                    <thead class="doctable">
                                    <tr>
                                        <th data-translate="documentName"></th>
                                        <th data-translate="documentType"></th>
                                        <th data-translate="documentState"></th>
                                    </tr>
                                    </thead>
                                    <tbody class="uploadeddocs" data-widget="compliance" data-item="uploadedDocs"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var url = document.location.toString();
        if (url.match('#')) {
            $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
        };

        $('.buysellassetddl').click(function(){
            $(this).toggleClass('open');
        });
        $(document).click(function(event){
            if (!$(event.target).parents('.buysellassetddl').length) {
                $('.buysellassetddl').removeClass('open');
            }
        });
        $('.view-all-activiy').click(function() {
            $('.dashboard-navs a[href="#transactions"]').tab('show');
        });
        $('.tol-userbar-my-profile').click(function() {
            $('.dashboard-navs a[href="#verification"]').tab('show');
        });
    });
</script>
<?php
    get_footer();
?>