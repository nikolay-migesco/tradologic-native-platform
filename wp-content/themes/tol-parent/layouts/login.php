<div id="tol-login-layout-99"> 
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-2 form-container justify-content-center">
                <!-- form 1 -->
                <form id="side-1" class="flip" data-widget="login" data-type="container">
                    <div class="form-group header">
                        <h3 class="text-left" data-translate="Login_LogIn">Log In</h3>
                    </div>
                    <?php if (defined('ENABLE_SOCIAL_IN_REGISTRATION') && ENABLE_SOCIAL_IN_REGISTRATION != false) { ?>
                        <div class="form-group social-logins">
                            <div data-widget="loginFacebook" onclick="$('#tol-login-popup').modal('hide');" data-type="fbLoginBtn" class="btn fb-login social" data-translate="loginFb"></div>
                            <div data-widget="loginFacebook" data-type="popUpElements" data-registration-widgets="title,firstName,lastName,birth,email,primaryPhone,languageCode,countryCode,currency,password,tos" class="loginFacebookWidget registrationPopUp" style="display:none"></div>

                            <div data-widget="loginGoogle" data-type="googleLoginBtn" onclick="$('#tol-login-popup').modal('hide');" class="btn google-login social" data-translate="loginGplus"></div>
                            <div data-widget="loginGoogle" data-type="popUpElements" data-registration-widgets="email,password,title,firstName,lastName,languageCode,countryCode,birth,primaryPhone,currency,tos" class="loginGoogleWidget registrationPopUp" style="display:none"></div>
                        </div>
                        <div class="form-group dashes">
                            <strong class="line-thru" data-translate="or">or</strong>
                        </div>
                    <?php } ?>
                    <div class="form-group input-container">
                        <input type="text" id="username" data-widget="login" tab-index="0"  data-type="username" class="form-control" required/>
                        <label for="username" class="lfl" data-translate="email">Email</label>
                    </div>
                    <div class="form-group input-container">
                        <input type="password" id="password" data-widget="login" data-type="password" class="form-control"  tab-index="1" required/>
                        <label for="password" class="lfl" data-translate="Login_Password">Password</label>
                        <div id="showPassword" class="glyphicon glyphicon-eye-open"></div>
                    </div>
                    <div class="error-msg lfl" data-widget="login" data-type="error" style="display:none"></div>
                    <div class="form-group checkbox savePassWrapper">
                        <input type="checkbox" name="savePass" id="savePass" class="savePass">
                        <label for="savePass" class="os"><span></span>Remember me  <a href="../forgot-password" class="forgot-password" tab-index="4" data-translate="forgotPassword"></a></label>
                    </div>
                    <div class="form-group">
                        <input type="button" data-widget="login" data-type="submit" value="" data-translate="Login_LogIn"  tab-index="3" class="btn btn-success text-center btn-main" />
                    </div>
                    <div class="form-group">
                        <label class="os" data-translate="dont-you-have-an-account"></label><a href="../registration" class="sign-up os" id="signup" tab-index="5" data-translate="sign-up"></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
