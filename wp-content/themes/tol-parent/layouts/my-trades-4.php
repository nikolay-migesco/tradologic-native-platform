<div id="tol-myTrades-layout-1">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="container-fluid">
            <div class="col-sm-10 col-sm-offset-1 no-padding-all">
            <div class="pageHeadTitle">
                 <?php require_once(ABSPATH . 'wp-content/themes/tol-parent/layouts/' . BREADCRUMB . '.php' ); ?>
            </div>
            <form action="" class="tol-form-inline">
                <div class="row">
                    <div class="well">
                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-3">
                                <div class="row">
                                    <label class="col-sm-3" data-translate="TradeRoom_Assets"></label>
                                    <div class="col-sm-8 nopadding-left">
                                        <?php echo do_shortcode('[myTradesOptions class="form-control btn-default form-control-md"]'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-xs-6 col-sm-2">
                                <div class="row">
                                    <label class="col-sm-4 hidden-xs text-right" data-translate="MyOptions_From"></label>
                                    <div class="col-sm-8 nopadding-left">
                                        <?php echo do_shortcode('[myTradesFromDate  data-past="30" class="form-control nopadding-right"]'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-xs-6 col-sm-2">
                                <div class="row">
                                    <label class="col-sm-4 hidden-xs text-right" data-translate="MyOptions_To"></label>
                                    <div class="col-sm-8 nopadding-left">
                                        <?php echo do_shortcode('[myTradesToDate class="form-control nopadding-right"]'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-3">
                                <div class="row">
                                    <label class="col-sm-4 hidden-xs" data-translate="copyBoardtrades"></label>
                                    <div class="col-sm-8">
                                        <?php echo do_shortcode('[myTradesSocialFilter class="form-control btn-default form-control-md"]'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-2">
                                <?php echo do_shortcode('[myTradesSearchButton translate="MyTrades_SearchBtn" class="btn btn-primary col-xs-12"]'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-5">
                                <div class="row">
                                    <label class="col-sm-4" data-translate="Trades_Volume"></label>
                                    <div class="col-sm-8">
                                        <?php echo do_shortcode('[myTradesVolume class="form-control"]'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-3">
                                <div class="row">
                                    <label class="col-sm-2" data-translate="Trades_Profit"></label>
                                    <div class="col-sm-10">
                                        <?php echo do_shortcode('[myTradesProfit class="form-control"]'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="row">
                                    <label class="col-sm-4 col-xs-5">Trades in Total</label>
                                    <div class="col-sm-8 col-xs-12">
                                        <?php echo do_shortcode('[myTradesTotalTrades class="form-control"]'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo do_shortcode('[myTradesTable class="table table-striped"]'); ?>
                    <div class="text-right">

                        <?php echo do_shortcode('[myTradesPaging paginglimit="10" class="pagination pagination-sm"]'); ?>
                    </div>
                </div>
            </form>
            <div class="entry-content">
                <?php echo $post->post_content; ?>

            </div>
            
                        </div>
                    </div>
    </article>
</div>