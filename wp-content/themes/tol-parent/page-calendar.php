<?php
/**
 * Template Name: MTE Calendar
 */
 ?>
<script>
// Economic Calendar
var sortingDay = 'all';

Xclient.render({
    clientId: 'ab56181', // Client ID
    product: 'cal', // Product code (cal,news,sumr-d,sumr-w,sumr-m,tecan,tecvid,sgnl)
    lang: 'en',
    assetmode: 'forex', // binary or forex (sgnl only)
    asset: '$csi', //retreive data for specific assets ($csib)
    limit: 100, // data limit (news, sumr-d,sumr-w,sumr-m, tecan, tecvid only)
    
    getData: function(data) {
        var data = JSON.parse(data).data.events;
        if (data) {
            var content = '';
            for (var i = 0; i < data.length; i++) {
                var currentItem = data[i],
                    allItemsArray = [];
                    // Date and Hours
                    currentItemDate = currentItem.date,
                    getDate = [];
                    getDate.push(currentItemDate);
                    var getDateAndHours = getDate[0].split(' '),
                        getDateSplitDate = getDateAndHours[0].split('-');
                    // Take the hours and minutes
                    var getDateSplitHours = getDateAndHours[1].split(':'),
                        currentItemHour = getDateSplitHours[0],
                        currentItemMinutes = getDateSplitHours[1];
                    // Take the date
                    var currentItemMounth = getDateSplitDate[1],
                        currentItemDay = getDateSplitDate[2],
                    // Date To print
                        currentItemDateToPrint= `${currentItemMounth}/${currentItemDay} ${currentItemHour}:${currentItemMinutes}`
                    // Help to sort assets by days
                    var currentItemDateDay = new Date(currentItem.date),
                        weekdays = new Array(7);
                            weekdays[0] = "Sunday";
                            weekdays[1] = "Monday";
                            weekdays[2] = "Tuesday";
                            weekdays[3] = "Wednesday";
                            weekdays[4] = "Thursday";
                            weekdays[5] = "Friday";
                            weekdays[6] = "Saturday";

                    var weekday_value = currentItemDateDay.getDay(),
                        currentDayAsString = '';
                    
                    
                    switch(weekdays[weekday_value]) {
						case 'Monday':
                            currentDayAsString = 'Monday';
							break;
                        case 'Tuesday':
                            currentDayAsString = 'Tuesday';
                            break;
                        case 'Wednesday':
                            currentDayAsString = 'Wednesday';
                            break;
                        case '':
                            currentDayAsString = '';
                            break;
                        case 'Friday':
                            currentDayAsString = 'Friday';
                            break;
						default:
							break;
                    }

                    currentItemCurrency = currentItem.currency.toLowerCase(),
                    currentItemImpact = currentItem.impact,
                    currentItemRateArrow = '',
                    calendarRow = '',
                    currentItemActual = currentItem.actual;
                    
                    // Impact Icon
                    if(currentItemImpact === 1) {
                        currentItemImpact = '<div class="impact1 col mte-calendar-event col-importace"><iron-icon class="importance-icon style-scope mte-calendar-event x-scope iron-icon-1" id="importanceIcon"><svg viewBox="0 0 24 24" preserveAspectRatio="xMidYMid meet" class="style-scope iron-icon" style="pointer-events: none; display: block; width: 100%; height: 100%;"><g class="style-scope iron-icon"><path d="M22 9.24l-7.19-.62L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21 12 17.27 18.18 21l-1.63-7.03L22 9.24zM12 15.4l-3.76 2.27 1-4.28-3.32-2.88 4.38-.38L12 6.1l1.71 4.04 4.38.38-3.32 2.88 1 4.28L12 15.4z" class="style-scope iron-icon"></path></g></svg></iron-icon></div>'
                    } else if (currentItemImpact === 2) {
                        currentItemImpact = '<div class="impact2 col mte-calendar-event col-importace"><iron-icon class="importance-icon style-scope mte-calendar-event x-scope iron-icon-1" id="importanceIcon"><svg viewBox="0 0 24 24" preserveAspectRatio="xMidYMid meet" class="style-scope iron-icon" style="pointer-events: none; display: block; width: 100%; height: 100%;"><g class="style-scope iron-icon"><path d="M22 9.74l-7.19-.62L12 2.5 9.19 9.13 2 9.74l5.46 4.73-1.64 7.03L12 17.77l6.18 3.73-1.63-7.03L22 9.74zM12 15.9V6.6l1.71 4.04 4.38.38-3.32 2.88 1 4.28L12 15.9z" class="style-scope iron-icon"></path></g></svg></iron-icon></div>'
                    } else {
                        currentItemImpact = '<div class="impact3 col mte-calendar-event col-importace"><iron-icon class="importance-icon style-scope mte-calendar-event x-scope iron-icon-1" id="importanceIcon"><svg viewBox="0 0 24 24" preserveAspectRatio="xMidYMid meet" class="style-scope iron-icon" style="pointer-events: none; display: block; width: 100%; height: 100%;"><g class="style-scope iron-icon"><path d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z" class="style-scope iron-icon"></path></g></svg></iron-icon></div>'
                    }

                     // Rate Arrow
                     if( (currentItem.previous) && (currentItem.actual) ) {
						if( Number(currentItem.actual) > Number(currentItem.previous) ) {
                            currentItemRateArrow = '<div class="col-sm-12 rateArrow up"><span></span></div>'
                            currentItemActual = '<div class="col-sm-12 bold_title"><b>Actual</b></div><div class="actualValue up values">' + (currentItem.actual == '' ? '-' : currentItem.actual + '%') + '</div>'
						} else if ( Number(currentItem.actual) < Number(currentItem.previous) ) {
                            currentItemRateArrow = '<div class="col-sm-12 rateArrow down"><span></span></div>'
                            currentItemActual = '<div class="col-sm-12 bold_title"><b>Actual</b></div><div class="actualValue down values">' + (currentItem.actual == '' ? '-' : currentItem.actual + '%') + '</div>'
						} else {
                            currentItemRateArrow = '<div class="col-sm-12 rateArrow" style="display:none;"><span></span></div>'
                            currentItemActual = '<div class="col-sm-12 bold_title"><b>Actual</b></div><div class="actualValue values">' + (currentItem.actual == '' ? '-' : currentItem.actual) + '</div>'
						}
                    } else {
                        currentItemRateArrow = '<div class="col-sm-12 rateArrow" style="display:none;"><span></span></div>'
                        currentItemActual = '<div class="col-sm-12 bold_title"><b>Actual</b></div><div class="actualValue values">' + (currentItem.actual == '' ? '-' : currentItem.actual) + '</div>'
                    }
                    
                    if(i % 2 === 0) {
                        calendarRow = '<div class="calendar-row gray" type="button" data-toggle="collapse" data-target="#info'+ i + '">'
                    } else {
                        calendarRow = '<div class="calendar-row" type="button" data-toggle="collapse" data-target="#info'+ i + '">'
                    }
                    

                    content +=  calendarRow +
                                    '<div class="col mte-calendar-event col-date">'+ currentItemDateToPrint +'</div>' +
                                    '<span class="currentDay" style="display: none;">'+ currentDayAsString +'</span>' +
                                    '<div class="col-sm-2 flag-icon flag-icon-' + currentItem.currency.toLowerCase() + '"></div>' +
                                    '<div class="col mte-calendar-event col-currency">' + currentItem.currency + '</div>' +
                                    '<div class="col mte-calendar-event col-event">' + currentItem.msg + '</div>' +
                                    currentItemImpact +
                                    '<div class="col mte-calendar-event col-actual"> <span class="style-scope">' + (currentItem.actual == '' ? '-' : currentItem.actual) + '</span> </div>' +
                                    '<div class="col mte-calendar-event col-forcast">' + (currentItem.forcast == '' ? '-' : currentItem.forcast) + '</div>' +
                                    '<div class="col mte-calendar-event col-previous">' + (currentItem.previous == '' ? '-' : currentItem.previous) + '</div>' +
                                '</div>' +
                                '<div id="info'+ i + '" class="collapse info-container">' + 
                                    currentItemRateArrow +
                                    currentItemActual +
                                    '<div class="col-sm-12 bold_title"><b>Forecast</b>></div><div class="values">' + (currentItem.forcast == '' ? '-' : currentItem.forcast +'%')  + '</div>' +
                                    '<div class="col-sm-12 bold_title"><b>Previous</b></div><div class="values">' + (currentItem.previous == '' ? '-' : currentItem.previous +'%') + '</div>' +
                                    '<div class="col-sm-12 bold_title"><b>Measures</b></div><div class="values">' + (currentItem.measures == '' ? '-' : currentItem.measures) + '</div>' +
									'<div class="col-sm-12 bold_title"><b>Ususal effect</b></div><div class="values">' + (currentItem.usualeffect == '' ? '-' : currentItem.usualeffect) + '</div>' +
									'<div class="col-sm-12 bold_title"><b>Frequency</b></div><div class="values">' + (currentItem.frequency == '' ? '-' : currentItem.frequency)  + '</div>' +
                                    '<div class="col-sm-12 bold_title"><b>Note</b></div><div class="values">' + (currentItem.notes == '' ? '-' : currentItem.notes)  + '</div>' +
                                    '<div class="col-sm-12 trade_button_news">Trade</div>' +  
                                '</div>';
            }
            $("#mte-calendar-container").append(content);
        }

        var row = $('.calendar-row');
		for (let i = 0; i < row.length; i++) {
			allItemsArray.push($(row[i]));
		}
		
		$(document).on("click", "#sortingByDays .nav-item", function() {
			for (let i = 0; i < row.length; i++) {
				var currentAssetDay = $(row[i]).find('.currentDay').text();
				if($(this).hasClass('monday')) {
					$(row[i]).hide();
					if((currentAssetDay === 'Monday')) {
                        if ($(row[i]).hasClass('hiddenByImpact') || $(row[i]).hasClass('hiddenByFlag')) {
                            break;
                        } else {
                            $(row[i]).show();
                        }
						sortingDay = 'monday';
					}
				} else if($(this).hasClass('tuesday')) {
					$(row[i]).hide();
					if((currentAssetDay === 'Tuesday')) {
						if ($(row[i]).hasClass('hiddenByImpact') || $(row[i]).hasClass('hiddenByFlag')) {
                            break;
                        } else {
                            $(row[i]).show();
                        }
						sortingDay = 'tuesday';
					}
				} else if($(this).hasClass('wednesday')) {
					$(row[i]).hide();
					if((currentAssetDay === 'Wednesday')) {
						if ($(row[i]).hasClass('hiddenByImpact') || $(row[i]).hasClass('hiddenByFlag')) {
                            break;
                        } else {
                            $(row[i]).show();
                        }
						sortingDay = 'wednesday';
					}
				} else if($(this).hasClass('thursday')) {
					$(row[i]).hide();
					if((currentAssetDay === '')) {
						if ($(row[i]).hasClass('hiddenByImpact') || $(row[i]).hasClass('hiddenByFlag')) {
                            break;
                        } else {
                            $(row[i]).show();
                        }
						sortingDay = 'thursday'; 
					}
				} else if($(this).hasClass('friday')) {
					$(row[i]).hide();
					if((currentAssetDay === 'Friday')) {
						if ($(row[i]).hasClass('hiddenByImpact') || $(row[i]).hasClass('hiddenByFlag')) {
                            break;
                        } else {
                            $(row[i]).show();
                        }
						sortingDay = 'friday';
					}
				} else if($(this).hasClass('all')) {
                    if ($(row[i]).hasClass('hiddenByImpact') || $(row[i]).hasClass('hiddenByFlag')) {
                        break;
                    } else {
                        $(row[i]).show();
                    }
				}
			}
		});
    },

    added: function(message) {
        // console.log("added", message);
    },
    changed: function(message) {
        //console.log("changed", message);
    },
    removed: function(message) {
        //console.log("removed", message);
    }
}, '#mte-calendar-container');

$(document).on("click", ".trade_button_news", function() {
    if (widgets.isLogged()) {
        if (widgets.isMobileDevice()) {
            if($(window).width() == 1024) {
                window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'traderoom?product=realil';
            } else {
                window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'traderoom?product=realm';
            }
        } else {
            window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'traderoom';
        }
    } else {
        window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'registration';
    };
    return;
});
// Sort Assets by Day
// Sort Assets by Impact (Star)
$('.choose-importance').click(function() {
	var imapactId = $(this).attr('id');
	var row = $('.calendar-row');
    if ($(this).hasClass('marked')) {
        $(row).find('.'+ imapactId +'').parent().hide().addClass('hiddenByImpact')
    } else {
        $(row).find('.'+ imapactId +'').parent().show().removeClass('hiddenByImpact')
    }
     $(this).toggleClass('marked')
});
$('.flag_checkbox').click(function() {
	var flagId = $(this).attr('id');
	var row = $('.calendar-row');
	if ($(this).is(':checked')) {
		for (let i = 0; i < row.length; i++) {
			var currentAssetDay = $(row[i]).find('.currentDay').text();
			if (sortingDay === currentAssetDay.toLowerCase()) {
				$('.calendar-row').find('.'+ flagId +'').parent().show().removeClass('hiddenByFlag')
				$(this).toggleClass('marked');
			} else if (sortingDay === 'all') {
				$('.calendar-row').find('.'+ flagId +'').parent().show().removeClass('hiddenByFlag')
				$(this).toggleClass('marked');
			}  else {
				$(row[i]).hide().addClass('hiddenByFlag')
			}
		}
	} else {
		$('.calendar-row').find('.'+ flagId +'').parent().hide().addClass('hiddenByFlag')
	}
});

</script>