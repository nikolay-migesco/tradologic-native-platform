/* get urls from plugin config */
var baseUrl = widgetsSettings.baseUrl + 'v6';
var apiUrl = widgetsSettings.restApiUrl;
var feedApiUrl = widgetsSettings.feedApiUrl;
var loggerUrl = widgetsSettings.loggerUrl;
var facebookAppID = widgetsSettings.facebookAppID;
var googleClientId = widgetsSettings.googleClientId;
var edgeUrl = '/api';
var qqClientId = widgetsSettings.qqId;
var advfeedApiUrl = widgetsSettings.advfeedApiUrl;
var webId = widgetsSettings.webId;
var avatarStorageURL = 'https://images.finte.co/Gamification/Avatars/';
var automaticLogout = true;
var enableQuickRegistration = true;
var isRegistrationPage = (window.location.href.indexOf('registration') > 0); //add correct URL string where is full registration page
var enableUserProfileFieldsOnCashier = true;
firstOrdersUpdate = true;
firstPositionsUpdate = true;

// Forex Open Postion orders columns which are visible/hidden by default
var openPositionsColumnsDefault = {
    1: false,    //PositionID
    2: true,   //Asset
    3: true,    //Quantity
    4: true,    //Direction
    5: true,    //Average Rate
    6: true,    //Market Rate
    7: true,    //Take Profit
    8: true,    //Stop Loss
    9: true,    //Margin
    10: false,  //Swap
    11: false,  //Commisison
    12: false,    //Date Created
    13: false,    //Copied From
    14: true,    //Result
    15: true    //Actions
};

// Forex Open Postion orders columns which are visible/hidden by default
var openPositionsColumns = ($.cookie('openPositionsColumns') ? $.parseJSON($.cookie('openPositionsColumns')) : openPositionsColumnsDefault);

//My Trades Table New Binary default columns
var myTradesTableNewBinaryColumnsDefault = {
    1: true,    //Order ID
    2: true,   //Order  Date
    3: true,    //Asset
    4: true,    //Action
    5: true,    //Trade Rate
    6: true,    //Amount
    7: true,    //Expiry
    8: true,    //Close Rate
    9: true    //Profit
};

if($.cookie('myTradesTableNewBinaryColumns')){
    var myTradesTableNewBinaryColumns = $.parseJSON($.cookie('myTradesTableNewBinaryColumns'));
}else{
    var myTradesTableNewBinaryColumns = myTradesTableNewBinaryColumnsDefault;
}

//My Trades Table New Exchange default columns
var myTradesTableBinaryExchangeNewColumnsDefault = {
    1: true,    //Trade ID
    2: true,   //Timestamp
    3: true,    //Asset
    4: true,    //Strike
    5: true,    //Expiry
    6: true,    //Direction
    7: true,    //Lots
    8: true,    //Price
    9: true,    //Rate
	10: true,    //Fee
    11: true    //PL
};

if($.cookie('myTradesTableBinaryExchangeNewColumns')){
    var myTradesTableBinaryExchangeNewColumns = $.parseJSON($.cookie('myTradesTableBinaryExchangeNewColumns'));
}else{
    var myTradesTableBinaryExchangeNewColumns = myTradesTableBinaryExchangeNewColumnsDefault;
}

//My Trades Table New SimpleForex default columns
var myTradesTableSimpleForexNewColumnsDefault = {
    1: true,    //Asset
    2: true,   //Type
    3: true,    //OpenDate
    4: true,    //TradeRate
    5: true,    //InvestAmount
    6: true,    //Leverage
    7: false,    //StopLoss
    8: false,    //TakeProfit
	9: true,    //CloseRate
    10: true,    //CloseDate
	11: false,    //Swap
    12: true    //Profit
};

if($.cookie('myTradesTableSimpleForexNewColumns')){
    var myTradesTableSimpleForexNewColumns = $.parseJSON($.cookie('myTradesTableSimpleForexNewColumns'));
}else{
    var myTradesTableSimpleForexNewColumns = myTradesTableSimpleForexNewColumnsDefault;
}

//My Trades Table New Simplex default columns
var myTradesTableSimplexNewColumnsDefault = {
    1: true,    //TradeId
    2: true,   //OpenTime
    3: true,    //Asset
    4: true,    //Type
    5: true,    //OpenPrice
    6: true,    //InvestAmount
    7: true,    //Risk
    8: false,    //StopLoss
    9: false,    //TakeProfit
	10: true,    //CloseTime
    11: true,    //ClosePrice
	12: true,    //Swap
	13: true,    //Commission
    14: true   //Profit
};

if($.cookie('myTradesTableSimplexNewColumns')){
    var myTradesTableSimplexNewColumns = $.parseJSON($.cookie('myTradesTableSimplexNewColumns'));
}else{
    var myTradesTableSimplexNewColumns = myTradesTableSimplexNewColumnsDefault;
}

//My Trades Table New RealForex default columns
var myTradesTableRealForexNewColumnsDefault = {
    1: false,    //Position ID
    2: true,   //TradeId
    3: true,    //Asset
    4: true,    //Quantity
    5: true,    //Direction
    6: true,    //Rate
    7: true,    //Close rate
    8: true,    //Date
    9: true,    //Close Date
    10: false,    //Swap
    11: false,  //Commission
    12: true,    //Result
    13: false    //Copied From
};

if($.cookie('myTradesTableRealForexNewColumns')){
    var myTradesTableRealForexNewColumns = $.parseJSON($.cookie('myTradesTableRealForexNewColumns'));
}else{
    var myTradesTableRealForexNewColumns = myTradesTableRealForexNewColumnsDefault;
}

var easyOpenPositionsColumnsDefault = {
    1: false,
    2: false,
    3: true,
    4: true,
    5: true,
    6: true,
    7: false,
    8: true,
    9: true,
    10: true,
    11: false,
    12: true,
    13: true
};
// Forex Open Postion orders columns which are visible/hidden by default
if($.cookie('easyOpenPositionsColumns')){
    var easyOpenPositionsColumns = $.parseJSON($.cookie('easyOpenPositionsColumns'));
}else{
    var easyOpenPositionsColumns = easyOpenPositionsColumnsDefault;
}

var easyClosedPositionsColumnsDefault = {
    1: false,
    2: true,
    3: true,
    4: true,
    5: true,
    6: true,
    7: true,
    8: false,
    9: false,
    10: true,
    11: true,
    12: false,
    13: true,
    14: true,
    15: true
};
// easy forex closed postions columns - visible/hidden by default
if($.cookie('easyClosedPositionsColumns')){
    var easyClosedPositionsColumns = $.parseJSON($.cookie('easyClosedPositionsColumns'));
}else{
    var easyClosedPositionsColumns = easyClosedPositionsColumnsDefault;
}

var pendingOrdersColumnsDefault = {
    1: false,    //OrderID
    2: true,     //Asset
    3: true,     //Direction
    4: true,     //Type
    5: true,     //Qunatity
    6: true,     //Rate
    7: true,     //Market Rate
    8: false,    //Date Created
    9: true      //Actions
};

// Forex Pending Orders columns which are visible/hidden by default
var pendingOrdersColumns = ($.cookie('pendingOrdersColumns') ? $.parseJSON($.cookie('pendingOrdersColumns')) : pendingOrdersColumnsDefault);

var footerSettingsDefault = {
    1: true,
    2: true,
    3: true,
    4: true,
    5: true
};
// Footer icons
var footerSettings = ($.cookie('footerSettings') ? $.parseJSON($.cookie('footerSettings')) : footerSettingsDefault);

// My trades colums
var myTradesColumnsDefault = {
    1: false,    //Position ID
    2: true,   //TradeId
    3: true,    //Asset
    4: true,    //Quantity
    5: true,    //Direction
    6: true,    //Rate
    7: true,    //Close rate
    8: true,    //Date
    9: true,    //Close Date
    10: false,    //Swap
    11: false,  //Commission
    12: true,    //Result
    13: false    //Copied From
};

var myTradesColumns = $.cookie('myTradesColumns') ? $.parseJSON($.cookie('myTradesColumns')) : myTradesColumnsDefault;

// My trades colums
var myEasyForexTradesColumnsDefault = {
    1: false,  //Trade ID
    2: true,   //Open Time
    3: true,   //Asset
    4: true,   //Type
    5: true,   //Open Price
    6: true,   //Invest Amount
    7: true,   //Risk
    8: false,   //Stop Loss
    9: false,   //Take Profit
    10: true, //Close Time
    11: true, //Close Price
    12: false,  //Swap
    13: true, //Commissions
    14: false,   //Return
    15: true   //Profit
};

var myEasyForexTradesColumns = $.cookie('myEasyForexTradesColumns') ? $.parseJSON($.cookie('myEasyForexTradesColumns')) : myEasyForexTradesColumnsDefault;

function getServerResource() { };

/** DO NOT EDIT BELOW THIS LINE **/
/** Fight Apathy... or don't **/

//*****************************************************************************************************************************************************************//
//************************************************************************** initWidgets() ************************************************************************//
//*****************************************************************************************************************************************************************//

var initWidgets = function () {
    widgets.traderoomloader = '#loadingElement'; // Loading element for the widgets traderoom
    widgets.themedevloader = '.themedevloader'; //Loading element for themedev
    widgets.webId = webId;
    widgets.baseUrl = baseUrl;
    widgets.apiUrl = apiUrl;
    widgets.edgeUrl = apiUrl;
    widgets.feedApiUrl = feedApiUrl;
    widgets.loggerUrl = loggerUrl;
    widgets.candlesUrl = 'https://candles-stage.superdev.com/candles.stg/_search';
    widgets.facebookAppID = facebookAppID;
    widgets.qqClientId = qqClientId;
    widgets.googleClientId = googleClientId;
    widgets.filterByAssetType = ['currencies', 'commodities', 'indices', 'futures', 'stocks', 'cryptotokens', 'cryptocoins'];
    widgets.supportedGames = ['EasyForex','RealForex'];
    widgets.tooltipIcon = true;
    widgets.tooltipIcon = ($(window).width() < 1025);
    widgets.clientDateTimeOffset = null;
    widgets.advfeedApiUrl = advfeedApiUrl;
    widgets.avatarStorageURL = avatarStorageURL;
    widgets.automaticLogout = automaticLogout;
    widgets.enableQuickRegistration = enableQuickRegistration;
    widgets.isRegistrationPage = isRegistrationPage;
    widgets.enableUserProfileFieldsOnCashier = enableUserProfileFieldsOnCashier;
    widgets.openPositionsColumns = $.extend(true, {}, openPositionsColumns);
    widgets.easyOpenPositionsColumns = $.extend(true, {}, easyOpenPositionsColumns);
    widgets.easyClosedPositionsColumns = $.extend(true, {}, easyClosedPositionsColumns);
    widgets.pendingOrdersColumns = $.extend(true, {}, pendingOrdersColumns);
    widgets.footerSettings = $.extend(true, {}, footerSettings);
    widgets.myTradesColumns = $.extend(true, {}, myTradesColumns);
    widgets.myEasyForexTradesColumns = $.extend(true, {}, myEasyForexTradesColumns);
	widgets.myTradesTableNewBinaryColumns = $.extend(true, {}, myTradesTableNewBinaryColumns);
	widgets.myTradesTableBinaryExchangeNewColumns = $.extend(true, {}, myTradesTableBinaryExchangeNewColumns);
	widgets.myTradesTableSimpleForexNewColumns = $.extend(true, {}, myTradesTableSimpleForexNewColumns);
	widgets.myTradesTableSimplexNewColumns = $.extend(true, {}, myTradesTableSimplexNewColumns);
	widgets.myTradesTableRealForexNewColumns = $.extend(true, {}, myTradesTableRealForexNewColumns);
    widgets.footerSettingsDefault = footerSettingsDefault;
    widgets.enableForexConfirmationClosePopup = true;
    widgets.tournamentRedirectUrl = (window.location.hostname.indexOf("stage") > -1 ? "https://demotournaments-stage.tradologic.com/" : "https://demotournaments-qa.tradologic.com/")  + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'traderoom';
    widgets.enableRebuy = true;
    widgets.tooltipIcon = true;
    widgets.forexNewAssetList = true;
    widgets.forexInfoPopupEnabled = true;
    widgets.simplexCryptoTraderoom = false;

    if ($(window).width() < 1025) {
        widgets.tooltipIcon = false;
    }
    
    // LOADING IS STARTED
    $(window).on('loadingIsStarted', loadingIsStarted);

    // LOADING IS COMPLETED
    $(window).on('loadingIsCompleted', loadingIsCompleted);

    $(window).on('assetSpecificationsLoaded', function(event, response){
        if (response.code == 200) {
            var tradingHours = '',
                weekDaysDefault = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];


            for (i = 0; i < 7; i++) {
                var foundDay = false;

                for (j = 0; j < response.data[0].CalendarInfo.length; j++) {
                    if (response.data[0].CalendarInfo[j].DayOfTheWeek == i) {
                        tradingHours += '<div class="asset-spec-row"><label>' + weekDaysDefault[response.data[0].CalendarInfo[j].DayOfTheWeek] + '</label><span>' + response.data[0].CalendarInfo[j].StartTimeOfDay + '-' + response.data[0].CalendarInfo[j].EndTimeOfDay + '</span></div>';
                        foundDay = true;
                    }
                }

                if (!foundDay) {
                    tradingHours += '<div class="asset-spec-row"><label>' + weekDaysDefault[i] + '</label><span>None</span>';
                }
            };

            tradingHours += '</div>';

            $('#tol-asset-spec .forex-asset-name').html(response.data[0].Asset);

            if (widgets.game == 'EasyForex') {
                $('.asset-specs-body').html('<div class="asset-specs-part asset-specs-part-header"><h3>Base Information</h3><div class="asset-spec-row"><label>Base Asset</label><span>' + response.data[0].Asset1 + '</span></div><div class="asset-spec-row"><label>Quote Asset</label><span>' + response.data[0].Asset2 + '</span></div><div class="asset-spec-row"><label>Digits</label><span>' + response.data[0].Accuracy + '</span></div><div class="asset-spec-row"><label>Margin Currency</label><span>' + response.data[0].Asset2 + '</span></div><div class="asset-spec-row"><label>Profit Currency</label><span>' + response.data[0].Asset2 + '</span></div><div class="asset-spec-row"><label>Leverage Low</label><span>' + response.data[0].Leverage.split(',')[0] + '</span></div><div class="asset-spec-row"><label>Leverage Medium</label><span>' + response.data[0].Leverage.split(',')[1] + '</span></div><div class="asset-spec-row"><label>Leverage High</label><span>' + response.data[0].Leverage.split(',')[2] + '</span></div></div><div class="asset-specs-part"><h3>Margin and Commissions</h3><div class="asset-spec-row"><label>Swap Type</label><span>Daily % using the open price</span></div><div class="asset-spec-row"><label>Swap Long</label><span>' + response.data[0].SwapLong + '%</span></div><div class="asset-spec-row"><label>Swap Short</label><span>' + response.data[0].SwapShort + '%</span></div><div class="asset-spec-row"><label>3-day Swap</label><span>' + response.data[0].ThreeDaysSwap + '</span></div><div class="asset-spec-row"><label>Commission</label><span>' + response.data[0].Commission + '%</span></div><div class="asset-spec-row"><label>Minimum Commission</label><span>' + helper.formatCurrency(helper.getCurrencySymbol(), response.data[0].MinCommission) + '</span></div><div class="asset-spec-row"><label>Maximum Commission</label><span>' + helper.formatCurrency(helper.getCurrencySymbol(), response.data[0].MaxCommission) + '</span></div></div><div class="asset-specs-part asset-specs-part-bottom"><h3>Trading Sessions</h3>' + tradingHours + '</div>');
            } else {
                $('.asset-specs-body').html('<div class="asset-specs-part asset-specs-part-header"><h3>Base Information</h3><div class="asset-spec-row"><label>Base Asset</label><span>' + response.data[0].Asset1 + '</span></div><div class="asset-spec-row"><label>Quote Asset</label><span>' + response.data[0].Asset2 + '</span></div><div class="asset-spec-row"><label>Digits</label><span>' + response.data[0].Accuracy + '</span></div><div class="asset-spec-row"><label>Spread</label><span>' + response.data[0].SpreadType + '</span></div><div class="asset-spec-row"><label>Target Spread</label><span>' + response.data[0].TargetSpread + ' points</span></div><div class="asset-spec-row"><label>Margin Currency</label><span>' + response.data[0].MarginCurrency + '</span></div><div class="asset-spec-row"><label>Profit Currency</label><span>' + response.data[0].ProfitCurrency + '</span></div><div class="asset-spec-row"><label>Leverage</label><span>1:' + response.data[0].Leverage + '</span></div><div class="asset-spec-row"><label>Minimal Volume</label><span>' + response.data[0].MinQuantity + ' units</span></div><div class="asset-spec-row"><label>Maximal Volume</label><span>' + response.data[0].MaxQuantity + ' units</span></div></div><div class="asset-specs-part"><h3>Margin and Commissions</h3><div class="asset-spec-row"><label>Margin Buy</label><span>' + response.data[0].Margin.toFixed(2) + '%</span></div><div class="asset-spec-row"><label>Margin Sell</label><span>' + response.data[0].Margin.toFixed(2) + '%</span></div><div class="asset-spec-row"><label>Swap Type</label><span>Daily % using the open price</span></div><div class="asset-spec-row"><label>Swap Long</label><span>' + response.data[0].SwapLong + '%</span></div><div class="asset-spec-row"><label>Swap Short</label><span>' + response.data[0].SwapShort + '%</span></div><div class="asset-spec-row"><label>3-day Swap</label><span>' + response.data[0].ThreeDaysSwap + '</span></div><div class="asset-spec-row"><label>CFD Commission</label><span>' + response.data[0].Commission + '%</span></div><div class="asset-spec-row"><label>Minimum Commission</label><span>' + helper.formatCurrency(helper.getCurrencySymbol(), response.data[0].MinCommission) + '</span></div></div><div class="asset-specs-part asset-specs-part-bottom"><h3>Trading Sessions</h3>' + tradingHours + '</div>');
            }

            $('#tol-asset-spec').modal('show');
            $('.asset-specs-content').mCustomScrollbar({theme:"minimal-dark"});
        }
    });

    $(window).on('easyForexOptionsDisplayed', assignMobileHighlightEvent);

    // REGISTER EVENTS
    $(window).on('widgetsRegisterEvents', function () {

        $(window).on(widgets.events.copyBoardEditUnSuccessful, function () {
            apprise(helper.getTranslation("copyBoardEditUnSuccessful", "Edit was unsuccessful."));
        });

        $(window).on(widgets.events.copyBoardUnfollowUnSuccessful, function () {
            apprise(helper.getTranslation("copyBoardUnfollowUnSuccessful", "Unfollow was unsuccessful."));
        });

        $(window).on(widgets.events.copyBoardNoChange, function () {
            apprise(helper.getTranslation("copyBoardNoChange", "No change has been made"));
        });

        $(window).on(widgets.events.copyBoardSuccessfullEdit, function () {
            apprise(helper.getTranslation("copyBoardSuccessfullEdit", "Followee edit was successful!"));
        });


        $('#forexProOpenTradesTable').on({
            mouseenter: function() {
                var strikeLineID =  $(this).data('orderid');
                $('.trade' + strikeLineID ).find('.strike-rate-line-Put-type').css('display', 'inline-block')
                $('.trade' + strikeLineID ).find('.strike-rate-line-Put-taid').css('display', 'inline-block')
                $('.trade' + strikeLineID ).find('.strike-rate-line-Call-type').css('display', 'inline-block')
                $('.trade' + strikeLineID ).find('.strike-rate-line-Call-taid').css('display', 'inline-block')
            },
            mouseleave: function() {
                var strikeLineID = $(this).data('orderid');
                $('.trade' + strikeLineID ).find('.strike-rate-line-Put-type').css('display', 'none')
                $('.trade' + strikeLineID ).find('.strike-rate-line-Put-taid').css('display', 'none')
                $('.trade' + strikeLineID ).find('.strike-rate-line-Call-type').css('display', 'none')
                $('.trade' + strikeLineID ).find('.strike-rate-line-Call-taid').css('display', 'none')
            }
        }, 'tr');

        //CUSTOM FILTER - NO OPTIONS
        $(window).on(widgets.events.noOptionsForCustomPreset, noOptionsForCustomPreset);

        // BINARY EXCHANGE TRADE IS COMPLETED
        $(window).on(widgets.events.binaryExchangeTradeIsCompleted, binaryExchangeTradeIsCompleted);

        $(window).on(widgets.events.twoFactorAuthLoginCodeRequired, function(event){
            $('#tol-login-popup [data-type=auth-code]').val('').parent().removeClass('error');
            $('#tol-login-popup [data-type=auth-error]').html('');
            $('#tol-login-popup .login-container, #tol-login-popup .open-real-account').hide();
            $('#tol-login-popup .login2fa-container, #tol-login-popup .auth-btn').show();
        });

        $(window).on(widgets.events.twoFactorAuthStatusChanged, function(event, response){
            $('.profile2fa-container').hide();
            $('.profile2faresult-container span').html(response ? helper.getTranslation('profile2fa-disabled', 'Two-Factor Authentication was successfully disabled.') : helper.getTranslation('profile2fa-enabled', 'Two-Factor Authentication was successfully enabled.'));
            $('.profile2faresult-container').show();

            widgets.api.getUserData([widgets.processUser, function () {
                $(window).trigger(widgets.events.loadingIsCompleted);
            }]);
        });

        // Update the pending orders counter
        $(window).on(widgets.events.realForexOrdersCount, function(event, counter) {
            $('.taborders .counter').html(counter);
            $.each(widgets.pendingOrdersColumns, function (i, isVisible) {
                if (isVisible === false){
                    $("#orders .dataTable th:nth-child(" + i +")").hide();
                    $("#orders .dataTable td:nth-child("+ i +")").hide();
                };
            });

            if( $('.account .taborders .multiple-selection-content .item').length>0){
                $('.account .taborders .multiple-selection-content .item').each(function (el) {
                    if(widgets.pendingOrdersColumns[el+1]===true){
                        $(this).addClass('selected');
                    }else if(widgets.pendingOrdersColumns[el+1]===false){
                        $(this).removeClass('selected');
                    }
                });
            }
        });

        $(window).on(widgets.events.realForexQuantityDLLClicked, function (event, scrollArea) {
            if (scrollArea == undefined){
                if ($('[data-type="amount"]:last').find('[data-type="amountcontainer"]').length > 0){
                    $('.assetsview-boxes > .forex-asset-boxesrow:last-of-type .amount-dropdown').css('margin-top',-Math.abs($('[data-type="amount"]:last').find('[data-type="amountcontainer"]').height()+28));
                };
            };
        });

        $(window).on(widgets.events.marginCallDepositRedirect, function () {
            depositQuickRedirect()
        });

        $(window).on(widgets.events.portfolioAddedSuccessfully, function (event, response) {
            $('#portfolio-modal').modal('toggle');
            apprise(response, {}, function (r) {
                if (r) {
                    window.location.href = getPathLanguagePrefix() + '/my-portfolios';
                    return;
                }
            });
        });

        $(window).on('binaryExchangeTradesCount', function(event, count){
            $('#be-trades-count').text(count);
        });

        $(window).on('binaryExchangeAssetSelected', function (event, data) {
            if (typeof tradingViewWidget != 'undefined') {
                tradingViewWidget.setSymbol(data.taID.toString(), tradingViewWidget.symbolInterval().interval);
            };
        });

        $(window).on('binaryExchangeOptionSelected', function (event, data) {
            if (typeof tradingViewWidget != 'undefined') {
                tradingViewWidget.chart().removeAllShapes();

                //top chart area
                tradingViewWidget.chart().createMultipointShape(
                    [{ time: 1289501500, price: data.strikeRateNum * 2 }, { time: tradingViewWidget.getVisibleRange().to * 1000, price: data.strikeRateNum }],
                    {
                        shape: "rectangle",
                        disableSelection: true,
                        overrides: {
                            backgroundColor: data.action == 2 ? "rgba( 174, 29, 46, 0.1)" : "rgba( 0, 147, 79, 0.1)",
                            color: data.action == 2 ? "rgba( 174, 29, 46, 0.1)" : "rgba( 0, 147, 79, 0.1)"
                        }
                    }
                );

                //bottom chart area
                tradingViewWidget.chart().createMultipointShape(
                    [{ time: 1289501500, price: data.strikeRateNum }, { time: tradingViewWidget.getVisibleRange().to * 1000, price: data.strikeRateNum / 2 }],
                    {
                        shape: "rectangle",
                        disableSelection: true,
                        overrides: {
                            backgroundColor: data.action == 2 ? "rgba( 0, 147, 79, 0.1)" : "rgba( 174, 29, 46, 0.1)",
                            color: data.action == 2 ? "rgba( 0, 147, 79, 0.1)" : "rgba( 174, 29, 46, 0.1)"
                        }
                    }
                );

                //current strike level
                tradingViewWidget.chart().createShape(
                    { price: data.strikeRateNum },
                    {
                        shape: "horizontal_line",
                        disableSelection: true,
                        overrides: {
                            linecolor: "rgba( 63, 191, 243, 1)",
                            textcolor: "rgba( 255, 255, 255, 1)"
                        }
                    });
            };
        });

        $(window).on('binaryExchangeExpirySelected', function (event, data, data2) {
            if (typeof tradingViewWidget != 'undefined') {
                tradingViewWidget.chart().removeAllShapes();
                tradingViewWidget.setSymbol(data.taID.toString(), tradingViewWidget.symbolInterval().interval);
            };
        });

        $(window).on(widgets.events.realForexTradesCount, function(event, counter) {
            $('.tabpositions .counter').html(counter);

            if (counter == 0 && typeof stxx != 'undefined') {
                clearStrikeRateLine();
            };

            if (widgets.game == 'RealForex') {
                $.each(widgets.openPositionsColumns, function (i, isVisible) {
                    if (isVisible === false){
                        $("#trades .dataTable th:nth-child(" + i +")").hide();

                        if (widgets.isMobileDevice()) {
                            $("#trades .dataTable div[data-type='openPositionsRow'] > div:nth-child("+ i +")").hide();
                        } else {
                            $("#trades .dataTable td:nth-child("+ i +")").hide();
                        };
                    }
                });

                $('.account .tabpositions .multiple-selection-content .item').each(function (index, el) {
                    if(widgets.openPositionsColumns[$(el).attr('data-column')]===true){
                        $(this).addClass('selected');
                    } else {
                        $(this).removeClass('selected');
                    }
                });
            } else {
                $.each(widgets.easyOpenPositionsColumns, function (i, isVisible) {
                    if (isVisible === false){
                        $("#trades .dataTable th:nth-child(" + i +")").hide();

                        if (widgets.isMobileDevice()) {
                            $("#trades .dataTable div[data-type='openPositionsRow'] > div:nth-child("+ i +")").hide();
                        } else {
                            $("#trades .dataTable td:nth-child("+ i +")").hide();
                        };
                    }
                });

                if( $('.account .tabeasypositions .multiple-selection-content .item').length>0){
                    $('.account .tabeasypositions .multiple-selection-content .item').each(function (el) {
                        if(widgets.easyOpenPositionsColumns[el+1]===true){
                            $(this).addClass('selected');
                        }else if(widgets.easyOpenPositionsColumns[el+1]===false){
                            $(this).removeClass('selected');
                        }
                    });
                };
            };

            if( $('.footer-settings-icon .multiple-selection-content .item').length>0){
                $('.footer-settings-icon .multiple-selection-content .item').each(function (el) {
                    if(widgets.footerSettings[el+1]===true){
                        $(this).addClass('selected');
                        $('.equity-content .equity-item:nth-child('+(el+1)+')').show();
                    }else if(widgets.footerSettings[el+1]===false){
                        $(this).removeClass('selected');
                        $('.equity-content .equity-item:nth-child('+(el+1)+')').hide();
                    }
                });
            }
        });

        $(window).on(widgets.events.easyForexReinvest, function (event, orderId) {
            var order;

            for (i = 0; i < easyForexHelper.closedPositions.length; i++) {
                if (easyForexHelper.closedPositions[i].orderId == orderId) {
                    order = easyForexHelper.closedPositions[i];
                    break;
                };
            };

            var assetData ={
                assetId: order.tradableAssetId.toString(),
                assetSymbolName: order.description
            };

            $(window).trigger('tradeFromChartAssetSelectedSimplex', assetData);
        });

        //closedTradesCount
        $(window).on(widgets.events.closedTradesCount, function(event, counter) {
            $('.taborders .counter').html(counter);

            if (helper.getParameterByName('reinvestorderid') != '') {
                $(window).trigger(widgets.events.easyForexReinvest, helper.getParameterByName('reinvestorderid'));

                var haveReinvestOrderID = false;

                if (window.location.href.split("?")[1] != undefined) {
                    var paramsURL = (window.location.href.split("?")[1]).split("&"),
                        stack = '';

                    for (i = 0; i < paramsURL.length; i++) {
                        if (paramsURL[i].indexOf('reinvestorderid=') > -1) {
                            haveReinvestOrderID = true;
                        } else {
                            if (stack == '')
                                stack += '?' + paramsURL[i];
                            else
                                stack += '&' + paramsURL[i];
                        }
                    }

                    stack = window.location.href.split("?")[0] + stack;

                    if (haveReinvestOrderID) {
                        if ($.browser.msie && $.browser.version < 10)
                            window.location.href = stack;
                        else
                            window.history.pushState("string", "Title", stack);
                    };
                };
            };

            $.each(widgets.easyClosedPositionsColumns, function (i, isVisible) {
                if (isVisible === false){
                    $("#pastInvestments .dataTable th:nth-child(" + i +")").hide();

                    if (widgets.isMobileDevice()) {
                        $("#easyForexClosedTradesTable div[data-type='openPositionsRow'] > div:nth-child("+ i +")").hide();
                    } else {
                        $("#pastInvestments .dataTable td:nth-child("+ i +")").hide();
                    };
                }
            });

            if( $('.account .tab-easy-closed-positions .multiple-selection-content .item').length>0){
                $('.account .tab-easy-closed-positions .multiple-selection-content .item').each(function (el) {
                    if(widgets.easyClosedPositionsColumns[el+1]===true){
                        $(this).addClass('selected');
                    }else if(widgets.easyClosedPositionsColumns[el+1]===false){
                        $(this).removeClass('selected');
                    }
                });
            };
        });

        // Update the pending orders counter
        $(window).on(widgets.events.realForexOrdersCount, function(event, counter) {
            $('.taborders .counter').html(counter);

            $.each(widgets.pendingOrdersColumns, function (i, isVisible) {
                if (isVisible === false){
                    $("#orders .dataTable th:nth-child(" + i + "), #orders .dataTable td:nth-child(" + i + ")").hide();
                };
            });

            $('.account .taborders .multiple-selection-content .item').each(function (index, el) {
                if(widgets.pendingOrdersColumns[$(el).attr('data-column')]===true){
                    $(this).addClass('selected');
                } else {
                    $(this).removeClass('selected');
                }
            });
        });

        // show ModifyOrder history popup
        $(window).on(widgets.events.realForexEditTrade, function(event, orderId) { 
            if ($(window).width() > 1024 && window.location.pathname.indexOf('traderoom') > -1 && $('.orders-trades-table-wrapper').hasClass('positions-active')) {
                $('table[data-widget="forex-openPositions"]').addClass('table-active-row');
                // Active Row
                var blurredRow =  $('table[data-widget="forex-openPositions"] tr[data-orderid="' + orderId + '"]');
                var blurredRowPos = blurredRow.offset();

                // Table Overlay One
                var ordersTable = $('.orders-trades-table-wrapper');
                var ordersTablePos = ordersTable.position();
                var overlayMaxHeight = (blurredRowPos.top - $('#tol-header-layout-25').height() - (ordersTablePos.top));
                $('.table-overlay-one').css('top', ordersTablePos.top);
                $('.table-overlay-one').css('left', ordersTablePos.left);
                $('.table-overlay-one').css('height', '100%');
                $('.table-overlay-one').css('max-height', overlayMaxHeight);
                $('.table-overlay-one').css('width', ordersTable.outerWidth());

                // Table Overlay Two
                var tableOverlayHeight = $('.orders-trades-table-wrapper').outerHeight() - blurredRow.outerHeight() - overlayMaxHeight;
                var tableOverlayTwoTop = (blurredRowPos.top - $('#tol-header-layout-25').height()) + blurredRow.outerHeight();
                $('.table-overlay-two').css('top', tableOverlayTwoTop);
                $('.table-overlay-two').css('height', tableOverlayHeight);
                $('.table-overlay-two').css('left', ordersTablePos.left);
                $('.table-overlay-two').css('width', ordersTable.outerWidth());
            
                // Show Overlays
                $('.table-overlay-one').show();
                $('.table-overlay-two').show();
            }
        });

        $(window).on(widgets.events.forexCloseButtonClicked, function (event, target) {
            $(target).parent().html('<div class="loader"><i></i><i></i><i></i><i></i></div>');
        });

        $(window).on(widgets.events.forexOpenPosition, function (event, position) {
            var currName = '';

            for (i=0; i < forexHelper.options.length; i++) {
                if (position.tradableAssetId == forexHelper.options[i].id) {
                    currName = forexHelper.options[i].name;
                    break;
                }
            }

            if(currName == $('#asset-label').text() && typeof stxx != 'undefined'){
                chartIQHitRateLineHoverAction(stxx, position);
            };
        });

        $(window).on(widgets.events.forexClosePosition, function (event, asset) {
            if(asset == $('#asset-label').text() && typeof stxx != 'undefined'){
                clearStrikeRateLine();
            };
        });

        // show positionID history popup
        $(window).on(widgets.events.forexPostionIdHistory, function(event, response){
            if (response.code == 200) {
                if (response.data.length > 0) {
                    var posId = response.data[0].PositionId,
                        assetName = response.data[0].Description,
                        html = '';

                    $('.position-history-content').mCustomScrollbar('destroy');
                    $('table[data-widget="forex-openPositions"]').addClass('table-active-row');
                    $('table[data-widget="forex-openPositions"] tr[data-orderid="' + response.data[0].PositionId + '"]').addClass('blur');
                    
                    if ($(window).width() > 1024 && window.location.pathname.indexOf('traderoom') > -1) {
                        // Active Row
                        var blurredRow =  $('table[data-widget="forex-openPositions"] tr[data-orderid="' + response.data[0].PositionId + '"]');
                        var blurredRowPos = blurredRow.offset();

                        // Table Overlay One
                        var ordersTable = $('.orders-trades-table-wrapper');
                        var ordersTablePos = ordersTable.position();
                        var overlayMaxHeight = (blurredRowPos.top - $('#tol-header-layout-25').height() - (ordersTablePos.top));
                        $('.table-overlay-one').css('top', ordersTablePos.top);
                        $('.table-overlay-one').css('left', ordersTablePos.left);
                        $('.table-overlay-one').css('height', '100%');
                        $('.table-overlay-one').css('max-height', overlayMaxHeight);
                        $('.table-overlay-one').css('width', ordersTable.outerWidth());

                        // Table Overlay Two
                        var tableOverlayHeight = $('.orders-trades-table-wrapper').outerHeight() - blurredRow.outerHeight() - overlayMaxHeight;
                        var tableOverlayTwoTop = (blurredRowPos.top - $('#tol-header-layout-25').height()) + blurredRow.outerHeight();
                        $('.table-overlay-two').css('top', tableOverlayTwoTop);
                        $('.table-overlay-two').css('height', tableOverlayHeight);
                        $('.table-overlay-two').css('left', ordersTablePos.left);
                        $('.table-overlay-two').css('width', ordersTable.outerWidth());
                    
                        // Show Overlays
                        $('.table-overlay-one').show();
                        $('.table-overlay-two').show();
                    }

                    for (i=0; i < response.data.length; i++) {
                        html += '<div class="posid-history-row ' + (response.data[i].Action).toLowerCase() + '"><div class="posidhistory-left"><div class="posidhistory-left-title ' + (response.data[i].Action).toLowerCase() + '">' + (response.data[i].Action).toUpperCase() + '</div><div class="posidhistory-left-date">' + helper.formatDate(helper.convertUTCDateToLocalDate(new Date(response.data[i].DateCreated), helper.dateFormats.masks.normalDateTime), helper.dateFormats.masks.normalDateTime) + '</div></div><div class="posidhistory-right"><div class="posidhistory-right-title">' + (response.data[i].OrderType == "MO" ? 'Market Order: ' : 'Pending Order: ') + response.data[i].OrderType + response.data[i].OrderId + '</div><div class="posidhistory-right-data"><div><span>Execution price:</span><div>' + response.data[i].ExecutionPrice + '</div></div><div><span>' + response.data[i].Direction + '</span><div>' + response.data[i].Quantity + '</div></div></div></div>' + ((response.data[i].Action).toLowerCase() == 'modified' ? '<div class="modified-add-row"><div><span>Average Price:</span><div>' + response.data[i].AveragePrice + '</div></div><div><span>' + response.data[i].PositionDirection + ':</span><div>' + response.data[i].NewQuantity + '</div></div></div>' : '') + '</div>';
                    };

                    $('#tol-position-history .forex-position-id').html('POS' + posId);
                    $('#tol-position-history .forex-posid-asset').html(assetName);
                    if (widgets.isMobileDevice()) {
                        $('#tol-position-history .forex-posid-profit').html(window.location.pathname.indexOf('my-trades') > -1 ? helper.formatCurrency(helper.getCurrencySymbol(), response.data[0].Profit) : $('#forexProOpenTradesTable div[data-type="openPositionsRow"][data-orderid=' + response.data[0].PositionId + '] div.result-td').html());
                    } else {
                        $('#tol-position-history .forex-posid-profit').html(window.location.pathname.indexOf('my-trades') > -1 ? helper.formatCurrency(helper.getCurrencySymbol(), response.data[0].Profit) : $('#forexProOpenTradesTable tr[data-orderid=' + response.data[0].PositionId + '] td.result-td').html());
                    };

                    $('.position-history-body').html(html);
                    $('#tol-position-history').modal('show');
                    $('.position-history-content').mCustomScrollbar({theme:"minimal-dark"});
                }
            }
        });

        // Remove Open Positions Table Classes when PositionID popup is closed and hide Overlays
        if ($(window).width() > 1024 && window.location.pathname.indexOf('traderoom') > -1) {
            $('.ui-dialog.forex-poshistory-dialog').on('hidden.bs.modal', function (e) {
                $('table[data-widget="forex-openPositions"]').removeClass('table-active-row');
                $('table[data-widget="forex-openPositions"] tr').removeClass('blur');
                $('.table-overlay-one').hide();
                $('.table-overlay-two').hide();
            });
            // Remove Open Positions Table Classes when Modify popup is closed and hide Overlays
            $('.ui-dialog.forex-neworder-dialog').live("dialogclose", function(){
                $('table[data-widget="forex-openPositions"]').removeClass('table-active-row');
                $('table[data-widget="forex-openPositions"] tr').removeClass('blur');
                $('.table-overlay-one').hide();
                $('.table-overlay-two').hide();
            });
        }

        $(window).on('realForexRenameQuantity', function () {
            $('#trades [data-column="quantity"] span, #orders [data-column="quantity"] span').html(helper.getTranslation('lots', 'Lots'));
            $('.multiple-selection-content span[data-translate="quantity"]').html(helper.getTranslation('lots', 'Lots'));
            $('#tab5 [data-column="volume"] div, #tab5 [data-column="volume"] span, .multiple-selection-content span[data-translate="forexTable_quantity"]').html(helper.getTranslation('lots', 'Lots')); // my trades, real forex
        });

        //Trade is started - we must take the amount and the direction
        $(window).on(widgets.events.tradeIsStarted, tradeIsStarted);

        // TRADE IS COMPLETED
        $(window).on(widgets.events.tradeIsCompleted, tradeIsCompleted);

        $(window).on(widgets.events.forexTradeIsCompleted, forexTradeIsCompleted);

        $(window).on(widgets.events.realForexTradeIsCompleted, realForexTradeIsCompleted);

        // No Options Message - Global Milk message
        $(window).on(widgets.events.noOptions, noOptions);

        // Show Milk Message - MULTY VIEW for each box
        $(window).on(widgets.events.noAvailableOptions, noAvailableOptions);

        // Hide Milk Message - MULTY VIEW for each box
        $(window).on(widgets.events.unhideMilkMessage, unhideMilkMessage);

        // Hide Global Milk message
        $(window).on(widgets.events.optionsUpdated, optionsUpdated);

        // Same asset message for multy view
        $(window).on(widgets.events.assetAlreadyUsed, assetAlreadyUsed);

        // LOG IN STARTED
        $(window).on(widgets.events.loginIsStarted, loginIsStarted);

        // LOG IN
        $(window).on(widgets.events.loginIsCompleted, loginIsCompleted);

        //User is not logged
        $(window).on(widgets.events.notLogged, function(event) {
            $('#tol-login-popup').modal('toggle');
        });

        // Triggers when user is logged and checks if it stays logged in every couple of seconds
        $(window).on(widgets.events.userLogin, userLogin);

        // Triggers when user is logged out
        $(window).on(widgets.events.userLogout, userLogout);

        //Updates assets ddl - customSelect when new asset from assets table is selected
        $(window).on(widgets.events.linkOptionUpdated, linkOptionUpdated);

        // REGISTRATION
        $(window).on('registrationCompleted', registrationCompleted);

        // RESET PASSWORD
        $(window).on('resetPassword', resetPassword);

        // RESET PASSWORD
        $(window).on('tradingToolsAreDisabled', tradingToolsAreDisabled);

        $(window).on('languageUpdate', function (event, response) {
            if (typeof widgets.selectedLang === 'object') {
                if (widgets.selectedIso != '') {
                    $.cookie("_icl_current_language", widgets.selectedIso, {path: "/"});
                    $.cookie("current_language", widgets.selectedIso, {path: "/"});
                };
                if (Object.keys(widgets.selectedLang).length > 0 && widgets.selectedLang.attr('href') != '#') {
                    if (window.location.pathname.indexOf('traderoom') > -1) {
                        window.location.href = widgets.selectedLang.attr('href') + '?game=' + $.cookie('game');
                    } else {
                        window.location.href = widgets.selectedLang.attr('href');
                    };
                };
            };
        });

        // Start Cashier

        //triggers when a deposit limitations is met using the quick deposit widget
        $(window).on(widgets.events.cashierDepositQuickLimitationFail, cashierDepositQuickLimitationFail);

        // triggers when a successful deposit is made
        $(window).on(widgets.events.cashierDepositSuccessful, cashierDepositSuccessful);

        // triggers when a pending deposit is made
        $(window).on(widgets.events.cashierDepositPending, cashierDepositPending);

        // triggers when promo code validation popup opens (initially a wrong promo code is entered)
        $(window).on(widgets.events.cashierPromocodePopupOpen, cashierPromocodePopupOpen);

        // triggers when 'Enter promo code' has been clicked in promo code validation popup
        $(window).on(widgets.events.cashierPromocodeEnter, cashierPromocodeEnter);

        // triggers when the deposit failed for some reason
        $(window).on(widgets.events.cashierDepositUnsuccessful, cashierDepositUnsuccessful);

        // triggeres when the deposit is started, here stays the code to start displaying "processing" animations or effects on deposit forms
        $(window).on(widgets.events.cashierStartDepositProcessing, cashierStartDepositProcessing);

        // triggeres when the deposit ends, here stays the code to end displaying "processing" animations or effects on deposit forms
        $(window).on(widgets.events.cashierEndDepositProcessing, cashierEndDepositProcessing);

        // triggers when iFrame is being prepared for loading
        $(window).on(widgets.events.cashierDepositLoadIframe, cashierDepositLoadIframe);

        // triggers when cashier widget needs to be disabled
        $(window).on(widgets.events.cashierWidgetDisabled, cashierWidgetDisabled);

        // triggers when suspended user is detected
        $(window).on(widgets.events.cashierUserSuspended, cashierUserSuspended);

        // triggers when cashier widget throws an exception during operation, unknown response code or error
        $(window).on(widgets.events.cashierWidgetError, cashierWidgetError);

        //   End Cashier

        // CHANGE PASSWORD
        $(window).on('changePassword', changePassword);

        // PASSWORD MISMATCH
        $(window).on('passwordMissmatch', passwordMissmatch);

        // Triggers when user details has been updated
        $(window).on('userDetailsUpdated', userDetailsUpdated);

        //triggered when a contact us message has been submitted
        $(window).on('contactUsSubmited', function(){
            $('.wrapper-contacts, .contact-us-wrapper-btns').hide();
        });

        // AFTER WAGER POPUP
        $(window).on(widgets.events.afterWagerBonusPopUpShow, afterWagerBonusPopUpShow);

        //BALANCE UPDATED
        $(window).on(widgets.events.balanceUpdated, balanceUpdated);

        //COPY TRADER
        $(window).on(widgets.events.copyTrader, copyTrader);

        //GET SOCIAL TRADER DATA
        $(window).on(widgets.events.socialTraderIdInfo, getSocialTraderId);


        $(window).on(widgets.events.inboxMessagesAvailable, function (event, inboxMessagesCount) {
            $('.messages .messagingNumber').hide();
            if (inboxMessagesCount > 0) {
                $('.messages .messagingNumber').show();
            };
        });

        $(window).on("inboxNoNewMessages", function () {
            $('.messages .messagingNumber').hide();
        });
        $(window).on(widgets.events.inactiveUserLoggedOut, displayLogOutNotification)

    });

    //-------------------------------------------
    // END REGISTER EVENTS
    //-------------------------------------------


    //----------------------------------
    // CONTACT US
    //----------------------------------

    widgets.postAddLead = function (response) {
        if (response.code == 201) {
            apprise(l10n.ContactUs_ThankYou);
        } else if (response.code === 400 && response.data.error !== null) {
            if (widgetMessage[response.data.type] == undefined) {
                apprise(response.data.error);
                return;
            } else {
                apprise(widgetMessage[response.data.type]);
                return;
            };
        } else {
            apprise(l10n.ContactUs_Failure);
            return;
        }
    };

    widgets.postContact = function (response) {
        if (response.code == 201) {
            apprise(l10n.ContactUs_ThankYou);
            $('.wrapper-contacts, .contact-us-wrapper-btns').hide();
        }
    };
    //----------------------------------
    // END CONTACT US
    //----------------------------------

    widgets.init();
    getEncryptedCookie();
};

//*****************************************************************************************************************************************************************//
//************************************************************************** END initWidgets() ********************************************************************//
//*****************************************************************************************************************************************************************//


clientDateTimeOffset: 50000;

//*****************************************************************************************************************************************************************//
//************************************************************************** FUNCTIONS ****************************************************************************//
//*****************************************************************************************************************************************************************//


//---------------------------------------------------
// UPDATE USER DATA
//-------------------------------------------------

function updateData() {
    $.cookie('widgetUserData', JSON.stringify(widgets.user), { path: '/' });

    if (widgetsSettings.enable_demo_flow == "1") {
        applyDemoUserFlow();
    };
}

//---------------------------------------------------
// UPDATE USER DATA
//-------------------------------------------------

//----------------------------------
// LOG OUT
//-------------------------------------------


function LogoutUser() {
    $.removeCookie('widgetSession', { path: '/' });
    $.removeCookie('token', { path: '/' });
    $.removeCookie('widgetUserData', { path: '/' });
    $.removeCookie('favouriteOptions', { path: '/' });

    $('.tolUserIsLogged').hide();
    $('.tolUserIsNotLogged').show();


    window.location.href = getPathLanguagePrefix() + widgetsSettings.logoutSuccessUrl;
    
};


//----------------------------------
// END LOG OUT
//-------------------------------------------


function framer_callParent() {
    var json = JSON.stringify({ type: 'function', data: arguments });
    window.top.postMessage(json, '*');
};

function redirectToURL() {
    window.location = arguments[0];
};

//======================================
//Functions wich load on every page
//==============================================


jQuery(function ($) {
    //-----------------------------------
    // Disable BUY button  or Prompt to log in
    //-----------------------------------------------
    if ($.cookie('widgetSession') == null) {
        $('.tradeButton').on("click", function () {
            apprise(widgetMessage.SaveTradeOrder_NotLoggedIn);
        });
    } else {
        $('.tradeButton').on("click", function () {
            $(this).attr('disabled', 'disabled');
        });
    };
    //-----------------------------------
    //END  Disable BUY button
    //-----------------

    widgets.selectedLang = {};
    widgets.selectedIso = '';
    $('#lang_sel_click ul ul li a').click(function (e) {
        e.preventDefault();
        widgets.selectedLang = $(this);

        var isoCode = (typeof $(this).parent().attr('class') !== 'undefined' ? $(this).parent().attr('class') : $(this).attr('class'));
        isoCode = isoCode.replace('icl-', '').replace('lang_sel_click_sel', '').replace(' ', '');

        widgets.selectedIso = isoCode;
        var lcid = widgetsSettings.languagesISO[isoCode];
        //widgets.setLanguage(lcid)

        if (typeof widgets.selectedLang === 'object') {
            if (widgets.selectedIso != '') {
                $.cookie("_icl_current_language", widgets.selectedIso, {path: "/"});
                $.cookie("current_language", widgets.selectedIso, {path: "/"});
            };
            if (Object.keys(widgets.selectedLang).length > 0 && widgets.selectedLang.attr('href') != '#') {
                widgetsSettings.lcid = lcid;
                if (window.location.pathname.indexOf('traderoom') > -1) {
                    window.location.href = widgets.selectedLang.attr('href');
                } else {
                    window.location.href = widgets.selectedLang.attr('href');
                };
            };
        };
    });

    //Side social panel
    $('.toggle-contacts-social').click(function() {
        if ($(this).hasClass('open')) {
            $( ".contacts-social" ).animate({
                left: "-=109"
            }, 500, function() {
                $('.toggle-contacts-social').removeClass('open');
                $('.toggle-contacts-social').addClass('close');
            });
        }else {
            $( ".contacts-social" ).animate({
                left: "+=109"
            }, 500, function() {
                $('.toggle-contacts-social').removeClass('close');
                $('.toggle-contacts-social').addClass('open');
            });
        };
    });
});

//----------------------------------------------
//----------------------- IS USER LOGGED ?
//----------------------------------------------

var tolIsLogged = function () {

    if (widgets.isLogged()) {
        $('.tolUserIsLogged').show(); // Show User Data container
        $('.tolUserIsNotLogged').hide(); // Hide Longin container

        $('body, .pageWrapper').removeClass('notlogged');
        $('body, .pageWrapper').addClass('logged');
        $('#traderoom-secondary').hide();

        $('.first-name').html(widgets.user.firstName);

    } else {
        $('body, .tolUserIsLogged').hide();
        $('body, .tolUserIsNotLogged').show();

        $('.pageWrapper').removeClass('logged');
        $('.pageWrapper').addClass('notlogged');
        $('#traderoom-secondary').show();
    };
};

//----------------------------------------------
//--------------------END IS USER LOGGED ?
//----------------------------------------------

var showClosedTrades = function () {
    $(window).trigger(widgets.events.expireOptions);
};

//----------------------------------------------
//------------ REMOVE HASH PARAMETER FROM URL
//----------------------------------------------

function removeHash() {
    var haveHash = false;

    if (window.location.href.split("?")[1] != undefined) {
        var paramsURL = (window.location.href.split("?")[1]).split("&");
        var stack = '';

        if (helper.getParameterByName('gclid') != '') {
            $.cookie('externalUsername', helper.getParameterByName('gclid'), { path: '/' });
            $('.externalUsername').val(helper.getParameterByName('gclid'));
            window.history.pushState("string", "Title", location.href.replace(/&?gclid=([^&]$|[^&]*)/i, ""));
        };

        for (i = 0; i < paramsURL.length; i++) {
            if (paramsURL[i].indexOf('hash=') > -1) {
                haveHash = true;
            } else {
                if (stack == '')
                    stack += '?' + paramsURL[i];
                else
                    stack += '&' + paramsURL[i];
            }
        }

        stack = window.location.href.split("?")[0] + stack;

        if (haveHash) {
            if ($.browser.msie && $.browser.version < 10)
                window.location.href = stack;
            else
                window.history.pushState("string", "Title", stack);
        };
    };
};

//----------------------------------------------
//------------ END REMOVE HASH PARAMETER FROM URL
//----------------------------------------------

//----------------------------------------------
//----------- ENCRYPT USER/PASSWORD AND SET COOKIES
//----------------------------------------------

function setEncryptedCookie() {
    var days = 30;
    var uName = $('#username').val();
    var pVal = $('#password').val();
    var _uName = '_uCookie';
    var _pVal = '_pCookie';
    var pass = uName;

    $.cookie("_saveChecked", "checked", { expires: days });
    $.cookie(_uName, uName, { expires: days });
    setSecureCookie(pass, _pVal, pVal, days);
}
//----------------------------------------------
//----------- END ENCRYPT USER/PASSWORD AND SET COOKIES
//----------------------------------------------

//----------------------------------------------
//----------- GET USER/PASSWORD FROM COOKIE
//----------------------------------------------

function getEncryptedCookie() {

    var _uName = '_uCookie';
    var uName;
    if ($.cookie(_uName) != undefined && $.cookie(_uName) != 'null') {
        uName = $.cookie(_uName);
        $('#username').val(uName);
    }

    var pass;
    var _pVal = '_pCookie';
    if ($.cookie(_pVal) != undefined && $.cookie(_pVal) != 'null') {
        pass = uName;
        $('#password').val(getSecureCookie(pass, _pVal));
    }

    var saveChecked
    if ($.cookie("_saveChecked") != undefined && $.cookie("_saveChecked") != 'null') {
        saveChecked = $.cookie("_saveChecked");
    }
    if (saveChecked == "checked")
        $(".savePass").attr('checked', true);
}

//----------------------------------------------
//----------- GET USER/PASSWORD FROM COOKIE
//----------------------------------------------

//----------------------------------------------
//----------- DELELTE USER/PASSWORD FROM COOKIE
//----------------------------------------------

function delEncryptedCookie() {

    var _uName = '_uCookie';
    var _pVal = '_pCookie';

    $.cookie(_uName, null);
    $.cookie(_pVal, null);
    $.cookie("_saveChecked", null);
}

//----------------------------------------------
//----------- DELELTE USER/PASSWORD FROM COOKIE
//----------------------------------------------

//----------------------------------------------
//------ Functions associated with quick deposit
//----------------------------------------------
function depositQuickRedirect() {
    var t = setTimeout(function () {
        window.top.location = "../cashier";
    }, 500);
};


function showPopUpAfterWagerBonus() {
    var msgPopup = $('#divAfterWagerBonus').find('div.afterWagerBonusPopup').find('span').text();
    var numPopup = document.getElementsByClassName("afterWagerBonusPopup");

    if (numPopup.length > 0) {
        document.getElementsByClassName("afterWagerBonusPopup")[0].remove();
        apprise(msgPopup);
    };
};
//----------------------------------------------
//------ END Functions associated with quick deposit
//----------------------------------------------

//*****************************************************************************************************************************************************************//
//************************************************************************** END FUNCTIONS ************************************************************************//
//*****************************************************************************************************************************************************************//

//*****************************************************************************************************************************************************************//
//************************************************************************** EVENTS FUNCTIONS *********************************************************************//
//*****************************************************************************************************************************************************************//
$(window).on('forexOptionsUpdated', function (event) {
    if (widgets.isFirstLoad) {
        iqChartAssetData = {
            assetId: forexHelper.options[0].id != 'undefined' ? forexHelper.options[0].id : forexHelper.options[1].id,
            assetSymbolName: forexHelper.options[0].name
        };
        $(window).trigger('tradeFromChartAssetSelectedForex', iqChartAssetData);
    };
});

/*********************************************/
/************* FOREX IQ-CHART *************/
/*********************************************/
$(window).on('tradeFromChartAssetSelectedForex', function (event, assetData) {
    if (typeof stxx == 'undefined') {
        return;
    }

    var chartSymbolLabel = document.getElementById('asset-label'),
        qfeedForex = {},
        iqChartAssetData = {
            assetId: assetData.assetId,
            assetSymbolName: assetData.assetSymbolName
        };

    function updateAssetLabel() {
        chartSymbolLabel.innerHTML = iqChartAssetData.assetSymbolName;
    };

    var resolutionElasticSearch =  function (resolutionLabel){
        if(resolutionLabel  == '1m'){
            return 1
        }else if(resolutionLabel  == '5m'){
            return 5
        } else if(resolutionLabel  == '15m'){
            return 15
        } else if(resolutionLabel  == '30m'){
            return 30
        } else if(resolutionLabel  == '1H'){
            return 60
        }else if(resolutionLabel  == '4H'){
            return 240
        }else if(resolutionLabel  == '1D'){
            return 1440
        }else if(resolutionLabel  == '1W'){
            return 10080
        }
    };

    qfeedForex.url = "https://candles-stage.superdev.com/candles.qa/_search";
    qfeedForex.translate = function (response, cb) {
        var newQuotes = [];

        if (!response.length) {
            return;
        };

        for (var i = 0; i < response.length; i++) {
            newQuotes[i] = {};
            newQuotes[i].DT = response[i][0];
            newQuotes[i].Open = response[i][1];
            newQuotes[i].High = response[i][2];
            newQuotes[i].Low = response[i][3];
            newQuotes[i].Close = response[i][4];
        }

        return newQuotes;
    };

    qfeedForex.fetchInitialData = function (symbol, suggestedStartDate, suggestedEndDate, params , cb) {
        var resolution  =  $('#period-val').text().replace(' ','');

        widgets.api.getElasticSearchPriceHistory(symbol, resolutionElasticSearch(resolution), function(response) {
            var newQuotes = qfeedForex.translate(response.data.history);
            cb({quotes: newQuotes});
        })
    };

    qfeedForex.fetchUpdateData = function (symbol, startdate, params, cb) {
        var resolution  = $('#period-val').text().replace(' ','');

        getElasticSearchData(this.url, {
            symbol: symbol.toString(),
            resolution: resolutionElasticSearch(resolution),
            from: Date.parse(startdate),
            to: Date.parse(new Date())
        }).done(function(response) {
            var newQuotes = qfeedForex.translate(response);

            if(typeof newQuotes == 'undefined'){
                return;
            }

            cb({quotes: newQuotes});
            updateAssetLabel()
        }).fail(function (status) {
            throw "Fail to update Elastic-Search (DATA) !";
        });
    };

    CIQ.ChartEngine.prototype.append("draw", updateAssetLabel);

    if (typeof stxx != 'undefined') {
        stxx.attachQuoteFeed(qfeedForex, {refreshInterval: 1});
        stxx.newChart(iqChartAssetData.assetId, null, null, function(){
            stxx.chart.yAxis.initialMarginTop = 80;
            stxx.chart.yAxis.initialMarginBottom = 150;
            stxx.yTolerance = 500;
            stxx.calculateYAxisMargins(stxx.chart.panel.yAxis);
            stxx.chart.yAxis.printDecimalPlaces = forexHelper.optionsByType["All"][iqChartAssetData.assetId].accuracy;
            stxx.chart.yAxis.maxDecimalPlaces = forexHelper.optionsByType["All"][iqChartAssetData.assetId].accuracy;
            stxx.chart.yAxis.decimalPlaces = forexHelper.optionsByType["All"][iqChartAssetData.assetId].accuracy;
            stxx.draw();
        });
    };

    setTimeout(function () {
        clearStrikeRateLine();
        generateExistStrikeLine();
    },500)
});

/*********************************************/
/************* SIMPLEX IQ-CHART *************/
/*********************************************/

// $(window).on('tradeFromChartAssetSelectedSimplex', function (event, assetData) {
//     var quotefeed = {},
//         chartSymbolLabel = document.getElementById('asset-label'),
//         iqChartAssetData = {
//             assetId: assetData.assetId,
//             assetSymbolName: assetData.assetSymbolName
//         };

//     quotefeed.url = "https://candles-stage.superdev.com/candles.qa/_search";

//     quotefeed.translate = function (response, cb) {
//         var newQuotes = [];

//         if (!response.length) {
//             return;
//         };

//         for (var i = 0; i < response.length; i++) {
//             newQuotes[i] = {};
//             newQuotes[i].DT = response[i][0];
//             newQuotes[i].Open = response[i][1];
//             newQuotes[i].High = response[i][2];
//             newQuotes[i].Low = response[i][3];
//             newQuotes[i].Close = response[i][4];
//         }

//         return newQuotes;
//     };

//     quotefeed.fetchInitialData = function (symbol, suggestedStartDate, suggestedEndDate, params , cb) {
//         var resolution  =  $('#period-val').text().replace(' ','');

//         widgets.api.getElasticSearchPriceHistory(symbol, 1, function(response) {
//             var newQuotes = quotefeed.translate(response.data.history);
//             cb({quotes: newQuotes});
//         })
//     };

//     stxx.attachQuoteFeed(quotefeed);
//     CIQ.Animation(stxx,  {tension:0.3});

//     stxx.newChart(iqChartAssetData.assetId,null,null,function () {
//         chartSymbolLabel.innerHTML = iqChartAssetData.assetSymbolName;
//         stxx.chart.yAxis.initialMarginTop = 180;
//         stxx.chart.yAxis.initialMarginBottom = 150;
//         stxx.yTolerance = 100000;
//         stxx.calculateYAxisMargins(stxx.chart.panel.yAxis);
//         stxx.draw();
//         setTimeout(streamSimplexPrices, 60000);
//     });

//     function streamSimplexPrices() {
//         var newData = {};
//         newData.DT     = new Date() ;
//         newData.Close  = Number(easyForexHelper.assetsPrices[iqChartAssetData.assetId].rate);
//         stxx.updateChartData(newData, stxx, {fillGaps: true});
//         setTimeout(streamSimplexPrices, 60000);
//     }

//     CIQ.ChartEngine.prototype.append("draw", function(){
//         chartSymbolLabel.innerHTML = iqChartAssetData.assetSymbolName;
//     });
// });


//-------------------------------------------
// LOADING IS STARTED
//-------------------------------------------
function loadingIsStarted() {
    if ($.cookie('widgetSession')) {
        $('.tolUserIsLogged').show(); // Show User Data container
        $('.tolUserIsNotLogged').hide(); // Hide Longin container

        if ($.cookie('widgetUserData')) {
            window.widgetsUser = $.parseJSON($.cookie('widgetUserData'));
            $('.first-name').html(widgetsUser.firstName);
            $('.balance-amount').html(widgetsUser.currencySymbol + ' ' + widgetsUser.balance);
        }
    } else {
        $('.tolUserIsLogged').hide(); // Hide User Data container
        $('.tolUserIsNotLogged').show(); // Show Longin container
    };

    $(widgets.traderoomloader).show();
    $('#loadingElementCashier, .cashier-content-wrap, .traderoom-loader').show();

    /*if (location.pathname.search('traderoom') > -1) {
        if (stxx) {
            initModule(stxx);
        }
    }*/
}

//-------------------------------------------
// END LOADING IS STARTED
//-------------------------------------------

//-------------------------------------------
// LOADING IS COMPLETED
//-------------------------------------------
function loadingIsCompleted() {
    $('.assetssearch-options-container').click(function(){
        $('.scrollable-area.asset-table-left').css('height', $('.asset-table-left').height() + 75);
    });

    $('.assets-boxes').click(function() {
        $('.row-settings-oneclick').show();

        if (screen.width == 768) {
            setTimeout(function() {
                $('.assetsview-boxes').css('height', Math.ceil($(".assetsview-boxes > div").length/2)*145);
                $('.scrollable-area.asset-table-left').css('height', $(window.top).height() - 198);
            }, 200);
        };

        if ($(this).hasClass('active')) {
            $('.scrollable-area.asset-table-left').css('height', $(window.top).height() - 229).mCustomScrollbar("destroy").mCustomScrollbar({
                scrollButtons: {
                    enable: false
                },
                theme: 'minimal-dark',
                horizontalScroll: false,
                autoDraggerLength: true,
                autoHideScrollbar: true,
                advanced: {
                    autoScrollOnFocus: false,
                    updateOnContentResize: true,
                    updateOnBrowserResize: true
                }
            });
        }
    });

    $('.assets-list').click(function() {
        $('.row-settings-oneclick').hide();

        if (screen.width == 768) {
            setTimeout(function() {
                $('.scrollable-area.asset-table-left').css('height', $(window.top).height() - 231);
            }, 200);
        };

        if ($(this).hasClass('active')) {
            $('.scrollable-area.asset-table-left').css('height', $(window.top).height() - 279).mCustomScrollbar("destroy").mCustomScrollbar({
                scrollButtons: {
                    enable: false
                },
                theme: 'minimal-dark',
                horizontalScroll: false,
                autoDraggerLength: true,
                autoHideScrollbar: true,
                advanced: {
                    autoScrollOnFocus: false,
                    updateOnContentResize: true,
                    updateOnBrowserResize: true
                }
            });

            $(".tradebox-compact-title").on('click', function (event) {
                var assetData ={
                    assetId: $(this).parent().parent().data('taid').toString(),
                    assetSymbolName: $(this).find('.tradebox-compact-instrument-name').text()
                };
                $(window).trigger('tradeFromChartAssetSelectedForex', assetData);
            });

            $(".open-chart-button-list").on('click', function (event) {
                var assetData ={
                    assetId: $(this).parent().parent().data('taid').toString(),
                    assetSymbolName: $(this).parent().find('.tradebox-compact-instrument-name').text()
                };
                $(window).trigger('tradeFromChartAssetSelectedForex', assetData);
            });
        }
    });

    $('#loadingElement, #loadingElementCashier, .traderoom-loader').hide();
    $('.mainMenu, .bodyWrapper, .widgetContent, .cashierContent, .cashierDepositFields, .tol-withdraw, .cashier-content-wrap, .userdata-wrapper, #content').show();
    $('.themedevloader').addClass('loaded');
    $('.highcharts-tooltip span').addClass('hover');
    $('.list-radio-wrap ul li:nth-child(1)').addClass('active');

    $('.list-radio-wrap ul li').click(function(){
        $('.list-radio-wrap ul li').removeClass('active');
        $(this).addClass('active');
    });

    if (widgets.isLogged()) {
        if (widgets.user.isReal) {
            $('.account-type.real-user').show();
            $('.account-type.demo-user').hide();
        } else {
            $('.account-type.demo-user').show();
            $('.account-type.real-user').hide();
        }
    } else {
        $.removeCookie('widgetSession', { path: '/' });
        $.removeCookie('token', { path: '/' });
        $.removeCookie('widgetUserData', { path: '/' });
        $('.tolUserIsLogged').hide();
        $('.tolUserIsNotLogged').show();
    };

    if(!$.cookie("current_language")) {
        $.cookie("current_language", (widgets.selectedIso!='' ? widgets.selectedIso : "en"), {path: "/"});
    } else {
        if ($.cookie("current_language").length > 7){
            $.cookie("current_language", widgets.selectedIso, {path: "/"});
        }
    };

    if(helper.getParameterByName('product') != '') {
        $.cookie("config", helper.getParameterByName('config'), {path: "/"});
        $.cookie("configFile", helper.getParameterByName('config'), {path: "/"});
    };

    if($.cookie("externalUsername"))
        $('.externalUsername').val($.cookie("externalUsername"));

    removeHash();

    if (location.pathname.search('traderoom') > -1) {
        $(".scrollable-area, .scrollable-social").mCustomScrollbar("destroy");
        $(".scrollable-area, .scrollable-social").mCustomScrollbar({
            theme: 'minimal-dark',
            scrollButtons: {
                enable: false
            },
            horizontalScroll: false,
            autoDraggerLength: true,
            autoHideScrollbar: true,
            advanced: {
                autoScrollOnFocus: true,
                updateOnContentResize: true,
                updateOnBrowserResize: true
            }

        });

        assignMobileHighlightEvent();

        if (widgets.game == 'RealForex' && typeof stxx != 'undefined') {
            setTimeout(function () {
                clearStrikeRateLine();
                generateExistStrikeLine();
            },2000);
        };

        $("div[data-type='assetsBoxes']").on('click', '[data-type="forexCubsesWrapper"]', function (event) {
            var assetData ={
                assetId:$(this).parent().data('taid').toString(),
                assetSymbolName:$(this).find('[data-type="name"]').text()
            };
            $(window).trigger('tradeFromChartAssetSelectedForex', assetData);
        });

        $("#period-dropDown").on('click', '.period-item', function (event) {
            stxx.setCandleWidth(135.1);
            stxx.draw();
        });
    };

    checkMT5Acc();
}

//-------------------------------------------
//END LOADING IS COMPLETED
//-------------------------------------------

//---------------------------------------------
// NO OPTION MESSAGE FOR CUSTOM FILTER
//---------------------------------------------

function noOptionsForCustomPreset(event, callBack) {
    if ($('.appriseOuter').size() == 0) {
        var msg = widgetMessage.noOptionsCustomFilter || 'There are no active option for this asset. Click "OK" to reset the filter to all.';
        apprise(msg, { 'alert': true }, function (r) {
            if (r) {
                callBack.call();
            }
        });
    }
};

//---------------------------------------------
// END NO OPTION MESSAGE FOR CUSTOM FILTER
//---------------------------------------------

function binaryExchangeTradeIsCompleted(event, response) {
    $('.close-button-row .close-button .close').click();
};


function tradeIsStarted(event, response) {
    currentOrder = response;
};


//---------------------------------------------
// TRADE IS COMPLETED
//---------------------------------------------

function tradeIsCompleted(event, response) {

    if (response.code == 200) {
        // resetTradeBox();
        apprise(widgetMessage.TradeRoom_SuccessTrade, {}, function (r) { if (r) { showPopUpAfterWagerBonus(); } });
        $('.tradeButton').attr("disabled", "true");
    } else {
        // resetTradeBox();
        var resultMessage = '';
        if (typeof widgetMessage[response.data.type] === 'string') {
            resultMessage = widgetMessage[response.data.type];
            if (typeof response.data.parameters === 'object') {
                for (var key in response.data.parameters) {
                    if (response.data.parameters.hasOwnProperty(key))
                        resultMessage = resultMessage.replace('{' + key + '}', response.data.parameters[key]);
                }
            }
            apprise(resultMessage);
        } else if (typeof response.data.text === 'string') {
            apprise(response.data.text);
        }

        $('.tradeButton').removeAttr('disabled', 'disabled'); //Enables the BuyButton
    }
};

function forexTradeIsCompleted(event, response) {
    if (response.code == 200) {

        $('.ui-widget-overlay, .forex-neworder-dialog').hide();
    }
};

function realForexTradeIsCompleted(event, response) {
    $('#forexProOpenTradesTable').removeClass('table-active-row');
    $('table[data-widget="forex-openPositions"] tr').removeClass('blur');
    $('.table-overlay-one').hide();
    $('.table-overlay-two').hide();
};
//---------------------------------------------
// END TRADE IS COMPLETED
//---------------------------------------------

//-----------------------------------------------
// No Options Message - Global Milk message
//-----------------------------------------------

function noOptions(event, response) {
    $('.noOptions, .tol-no-options').show();

    $(window).trigger('loadingIsCompleted');
    $(window).trigger('linkOptionUpdated');
};

//-----------------------------------------------
// END No Options Message
//-----------------------------------------------

//-----------------------------------------------
// Show Milk Message - MULTY VIEW for each box
//-----------------------------------------------

function noAvailableOptions(event, linkName) {

    $('.noOptionsContainer' + linkName.substr(linkName.length - 1)).show();
};

//----------------------------------------------------
// END Show Milk Message - MULTY VIEW for each box
//----------------------------------------------------

//-----------------------------------------------
// Hide Milk Message - MULTY VIEW for each box
//-----------------------------------------------

function unhideMilkMessage(event, linkName) {
    var i = linkName.substr(linkName.length - 1);
    $('.noOptionsContainer' + i).hide();

    try {
        $(['.tradearea' + i] + ' .optionsDDLWrapper .customSelectInner').html(widgets.links[linkName].option.name);
        $(['.tradearea' + i] + ' .expireDropDownWidget .customSelectInner').html(helper.formatExpiration(new Date(widgets.links[linkName].option.expirationTimestamp)));
    } catch (e) { };


};

//----------------------------------------------------
// END Hide Milk Message - MULTY VIEW for each box
//----------------------------------------------------

//-----------------------------------------------
// Hide Global Milk message
//-----------------------------------------------

function optionsUpdated() {
    $('.noOptions, .tol-no-options').hide();
    $(window).trigger('linkOptionUpdated');
};

//-----------------------------------------------
// END Hide Global Milk message
//-----------------------------------------------

//-----------------------------------------------
// Same asset message for multy view
//-----------------------------------------------

function assetAlreadyUsed() {
    apprise(widgetMessage.Asset_already_selected);
};

//-----------------------------------------------
// END Same asset message for multy view
//-----------------------------------------------

//-----------------------------------
// LOG IN STARTED
//-----------------------------------

function loginIsStarted(event) {

    $('.loginPreloader').show();

};

//-----------------------------------
// END LOG IN STARTED
//-----------------------------------


//-----------------------------------
// LOG IN
//-----------------------------------

function loginIsCompleted(event, response) {
    if (response.code === 201) {
        $('.login-success-message').show();
        $.cookie('widgetSession', response.data.sessionId, { path: '/' });
        if ($(".savePass").is(':checked'))
            setEncryptedCookie();
        else
            delEncryptedCookie();

        widgets.api.getResources({ riskFactor: { game: helper.ucfirst(widgets.game.toLowerCase()) } }, widgets.processResources);

        if (widgetsSettings.enable_demo_flow === true) {
            widgets.api.getUserData([widgets.processUser, function (response) {
                if (response.data) {
                    var user = response.data;
                    window.location.href = getPathLanguagePrefix() + (!user.isVerified && !user.Package ? '/education' : getTraderoomLink(traderoomMode.easyForex));
                 }
            }]);
        } else {
            const redirect = getTraderoomLink(traderoomMode.easyForex);
            window.location.href = getPathLanguagePrefix() + redirect;
        }
    } else {
        $('.loginPreloader').hide();
        $('.login-widget-wrapper').show();
    }

}

//-------------------------------------------
// END LOGIN
//-------------------------------------------

//----------------------------------------------------------------------------------------
// Triggers when user is logged and checks if it stays logged in every couple of seconds
//----------------------------------------------------------------------------------------

function userLogin() {
    tolIsLogged();
    updateData();

};

//-------------------------------------------
// END User is Logged
//-------------------------------------------

//----------------------------------------------------------------------------------------
// Triggers when user is logged out
//----------------------------------------------------------------------------------------

function userLogout() {
    LogoutUser();
};

//-------------------------------------------
// END User is Logged Out
//-------------------------------------------

//-------------------------------------------------------------------------------
//Updates assets ddl - customSelect when new asset from assets table is selected
//-------------------------------------------------------------------------------

function linkOptionUpdated() {
    // resetTradeBox();
    if ($.cookie('view') == 'multi') {
        try {
            for (var i = 1; i < 5; i++) {
                $(['.tradearea' + i] + ' .expireDropDownWidget .customSelectInner').html(helper.formatExpiration(new Date(widgets.links['tradologic' + i].option.expirationTimestamp)));
                $(['.tradearea' + i] + ' .optionsDDLWrapper .customSelectInner').html(widgets.links['tradologic' + i].option.name);
            }
            var t = setTimeout(function () {
                $('.widgetDynamicPayout').trigger('change');
            }, 100);
        } catch (e) { };
    }
    else {
        try {
            $('.expireDropDownWidget .customSelectInner').html(helper.formatExpiration(new Date(widgets.links.tradologic.option.expirationTimestamp)));
            $('.optionsDDLWrapper .customSelectInner').html(widgets.links.tradologic.option.name);
            var t = setTimeout(function () {
                $('.widgetDynamicPayout').trigger('change');
            }, 100);

        } catch (e) { };
    }
};

//-------------------------------------------------------------------------------------
//END - Updates assets ddl - customSelect when new asset from assets table is selected
//-------------------------------------------------------------------------------------

//----------------------------------
// REGISTRATION
//----------------------------------
function registrationCompleted(event, response) {
    if (response.code === 201) {
        $.cookie('widgetSession', response.data.sessionId, { path: '/' });

        const toTraderoom = widgetsSettings.registrationSuccessUrl === "/traderoom";

        if(!toTraderoom){
            window.location.href = getPathLanguagePrefix()
                + (widgetsSettings.enable_demo_flow === true ? '/education' : widgetsSettings.registrationSuccessUrl);
        }
        if(toTraderoom){
            const redirect = getTraderoomLink(traderoomMode.easyForex);
            window.location.href = getPathLanguagePrefix() + redirect;
        }

    } else {
        if ( typeof widgetMessage[response.data.type] === typeof undefined) {
            apprise(response.data.text);
        } else {
            apprise(widgetMessage[response.data.type]);
        }
    }
}
//----------------------------------
// END REGISTRATION
//----------------------------------

//----------------------------------
// RESET PASSWORD
//----------------------------------
function resetPassword(event, response) {
    if (response.code == 200) {
        $('.forgot-password-success').show();
    } else {
        $('.forgot-password-fail').show();
    };

    $('.forgot-password-container .forgot-outcome-text').html(helper.getTranslation(widgetMessage[response.data.type], response.data.text));
};
//----------------------------------
// END RESET PASSWORD
//----------------------------------

//----------------------------------
// TRADING TOOLS DISABLED
//----------------------------------

function tradingToolsAreDisabled(event) {
    $('.toolBackground').hide();
};

//----------------------------------
// TRADING TOOLS DISABLED
//----------------------------------

//----------------------------------
//   Start Cashier
//----------------------------------

//triggers when a deposit limitations is met using the quick deposit widget
function cashierDepositQuickLimitationFail(event, params) {
    $('[data-widget=cashierCardDepositQuick][data-type=error]').text(widgetMessage.cashierDepositLimitationFail.replace(/\{MinimumDepositSum}/g, params.result.data.parameters.MinimumDepositSum).replace(/\{MaximumDepositSum}/g, params.result.data.parameters.MaximumDepositSum));
    $('[data-widget=cashierCardDepositQuick][data-type=error]').css('padding', '10px 0 5px 0');
    $('[data-widget=cashierCardDepositQuick][data-type=error]').show()
};

// triggers when a successful deposit is made
function cashierDepositSuccessful(event, params) {
    if (params.widgetName != "cashierCardDepositQuick") {
        $('.cashierDepositFields').hide();
        window.location.href = './deposit-successful';
    } else {
        $('[data-widget=cashierCardDepositQuick], .quickDepositInfo, .quick-cancel-BTN, .goToCashierLink').hide();
        $('[data-widget=cashierCardDepositQuick][data-translate=cashierDepositSuccessful], .quick-cancel-BTN.endDeposit').show();
    };
};

// triggers when a pending deposit is made
function cashierDepositPending(event, params) {
    $('.cashierDepositFields').hide();
    window.location.href = './deposit-pending';
};

// triggers when promo code validation popup opens (initially a wrong promo code is entered)
function cashierPromocodePopupOpen(event) {
    $('#loadingElementCashier').hide();
};

// triggers when 'Enter promo code' has been clicked in promo code validation popup
function cashierPromocodeEnter(event) {
    $('.cashierFiedsets').show();
};

// triggers when the deposit failed for some reason
function cashierDepositUnsuccessful(event, params) {
    if (params.widgetName != "cashierCardDepositQuick") {
        window.location.href = './deposit-failed';
    } else {
        $('[data-widget=cashierCardDepositQuick], .quickDepositInfo, .quick-cancel-BTN, .goToCashierLink').hide();
        $('#tryAgainQuick').show();
        $('[data-widget=cashierCardDepositQuick][data-translate=cashierLabelTryAgain], .quick-cancel-BTN.endDeposit').show();
    };
};

// triggeres when the deposit is started, here stays the code to start displaying "processing" animations or effects on deposit forms
function cashierStartDepositProcessing(event) {
    $('.cashierFiedsets').hide();
    $('.ccDepositButtons').hide();
    $('#loadingElementCashier').show();
};

// triggeres when the deposit ends, here stays the code to end displaying "processing" animations or effects on deposit forms
function cashierEndDepositProcessing(event) {
    $('.cashierFiedsets').show();
    $('#loadingElementCashier').hide();
    $('.depositSummary').hide();
    $('.promoCodeLink').hide();
    $('.cashierField').hide();
};

// triggers when iFrame is being prepared for loading
function cashierDepositLoadIframe(event) {
    $('#loadingElementProcessing').hide();
    $('.cashierFiedsets').hide();
};

// triggers when cashier widget needs to be disabled
function cashierWidgetDisabled(event, widgetName, messageType, params) {
    // Code to display the message and disable the widget
    if (messageType == "cashierWithdrawalNotAvailableCreditBalance" || messageType == "cashierWithdrawDemoUser") {
        apprise(widgetMessage[messageType], {}, function (r) { if (r) { window.location.href = './cashier?cpage=deposit'; } });
    } else if (messageType = "cashierCountryNotAllowed" || messageType == "cashierIPNotAllowed") {
        if (widgetName != "cashierCardDepositQuick") {
            apprise(widgetMessage[messageType], {}, function (r) {
                if (r) {
                    window.location.href = './traderoom?product=forex';
                };
            });
        };
    } else {
        // code to display when messageType -> cashierOpenTradesForCreditBalance
        apprise(widgetMessage[messageType]);
    }
};

// triggers when suspended user is detected
function cashierUserSuspended(event) {
    if (typeof widgetMessage["cashierUserSuspended"] == 'undefined') { //checks whether such a widgetMessage is defined
        apprise('This user is suspended.');
        return;
    } else {
        apprise(widgetMessage["cashierUserSuspended"]);
        return;
    };
};

// triggers when cashier widget throws an exception during operation, unknown response code or error
function cashierWidgetError(event, widgetName, messageType, params) { // params contains the full response object as received from the REST API
    // Code to display an error message
    switch (widgetName) { // widget name
        case 'cashierDeposit':
            alert(widgetMessage[messageType]); // message type, for example cashierPendingWithdrawalForCreditBalance, could be used to get the proper translated message via widgetMessage
            // Put code here to display an error on cashierDeposit widget
            break;
        case 'cashierWithdraw':
            // Put code here to display an error on the cashierWithdraw widget
            break;
    };
};

//----------------------------------
//   End Cashier
//----------------------------------


//----------------------------------
//  CHECK USER IS  REAL OR NOT
//----------------------------------

 function checkRealUser() {
     var userStatus = widgets.user != null ? widgets.user.isReal : null;
 }

//----------------------------------
// END CHECK USER IS  REAL OR NOT
//----------------------------------

// --------------------------------------
// CHANGE PASSWORD
// --------------------------------------

function changePassword(event, response) {
    if (response.code == 200) {

        apprise(widgetMessage['successfulPasswordChange']);

    } else if (widgetMessage[response.data.type] == undefined) {
        apprise(response.data.text);
        return;
    } else {
        apprise(widgetMessage[response.data.type]);
        return;
    }
    return;
};

// ----------------------------------------
// END CHANGE PASSWORD
// ----------------------------------------

// --------------------------------------
// PASSWORD MISMATCH
// --------------------------------------

function passwordMissmatch(event) {
    apprise(widgetMessage[event.type]);
};

// --------------------------------------
// END PASSWORD MISMATCH
// --------------------------------------

//---------------------------------------------
// Triggers when user details has been updated
//---------------------------------------------

function userDetailsUpdated(response) {
    var myProfileSuccessMsg = widgetMessage['myProfileSuccessMsg'];
    $(".myprofile-successmsg").html(myProfileSuccessMsg);
};

//-------------------------------------------------
// END triggers when user details has been updated
//-------------------------------------------------

//---------------------------------------------
// After Wager Bonus
//---------------------------------------------

function afterWagerBonusPopUpShow(event) {
    $('#divAfterWagerBonus').find('div.afterWagerBonusPopup').hide();
};

//-------------------------------------------------
// END After Wager Bonus
//-------------------------------------------------

function balanceUpdated() {
    $.cookie('widgetUserData', JSON.stringify(widgets.user), { path: '/' });
};

function copyTrader(event, params) {
    apprise(params.text);
};

//*****************************************************************************************************************************************************************//
//************************************************************************** END EVENTS FUNCTIONS *****************************************************************//
//*****************************************************************************************************************************************************************//

function getSocialTraderId(event, traderId) {
    if (traderId) {
        window.location.href = getPathLanguagePrefix() + '/trader-info/?traderId=' + traderId; // Redirect to social-traderinfo page
        return;
    }
};

function displayLogOutNotification(event) {
    if ($(".appriseOuter").length > 0) {
        return;
    }

    apprise('<p class="inactiveuser-popup">' + helper.getTranslation('inactiveMessage', 'You have been logged out due to inactivity.') + '</p>', { 'alert': true }, function (r) {
        userLogout();
    });

    var inactiveApprise = $('.inactiveuser-popup');
    if (inactiveApprise.length > 0) {
        inactiveApprise.parent().addClass('inactiveUserApprise')
    }
};

$(document).on('click', '.ui-dialog-titlebar-close, .loginFacebookWidget, .loginQQWidget, .loginGoogleWidget', function (event) {
    $('.login-popup-bg').hide();
    $('.login-wrap').hide();
    $('.loginPreloader').hide();
});

function assignMobileHighlightEvent() {
    if(!widgets.isMobileDevice()){
        $('.flip-container').addClass('desktop');
    } else {
        $('.flip-container').unbind('click');
        $('.flip-container').click(function () {
            if ($(this).hasClass("tapped"))
                $(this).removeClass("tapped");
            else
            {
                $.each($('.tapped'), function (i, tappedBox) {
                    $(tappedBox).removeClass("tapped");
                });
                $(this).addClass("tapped");
                $.cookie("highlightedAssetId", parseInt($(this).attr('data-taid')));
            }
        });
    }
}

function getElasticSearchData(url, params) {
    if (params.resolution == 1) {
        return $.ajax({
            url: url,
            dataType: "text",
            type: 'POST',
            contentType: "application/json; charset=UTF-8",
            data: JSON.stringify({
                "query": {
                    "bool": {
                        "must": [
                            {"term": {"assetId": params.symbol}}
                        ],
                        "must_not": [
                            {
                                "exists" : { "field" : "length" }
                            }
                        ],
                        "filter": {
                            "range": {
                                "tS": {
                                    "gte": params.from,
                                    "lt": params.to
                                }
                            }
                        }
                    }
                },
                "size":10000,
                "sort" : [{ "tS" : {"order" : "asc"}}]
            })
        });
    } else {
        return $.ajax({
            url: url,
            dataType: "text",
            type: 'POST',
            contentType: "application/json; charset=UTF-8",
            data: JSON.stringify({
                "query": {
                    "bool": {
                        "must": [
                            {"term": {"assetId": params.symbol}},
                            {"term": {"length": params.resolution}}
                        ],
                        "filter": {
                            "range": {
                                "tS": {
                                    "gte": params.from,
                                    "lt": params.to
                                }
                            }
                        }
                    }
                },
                "size":10000,
                "sort" : [{ "tS" : {"order" : "asc"}}]
            })
        });
    }
};

function checkMT5Acc() {
    if(helper.getGlobalSetting('MT5Integration')) {
        $('.transfer-funds').text('Transfer Funds');
        $('.transfer-from-field').show();
        $('[data-widget=cashierWithdraw][data-type=transferToField]').text('Transfer to')  ;
    }
}

function generateExistStrikeLine (){
    var availableStrikeLine = [];

    if (typeof forexHelper.forexOpenPositions != 'undefined') {
        $.each(forexHelper.forexOpenPositions, function (i, item) {
            if (item.optionType == 'HARealForex') {
                var currName = '';

                for (i=0; i < forexHelper.options.length; i++) {
                    if (item.tradableAssetId ==  forexHelper.options[i].id) {
                        currName = forexHelper.options[i].name;
                        break;
                    }
                }

                if(currName == $('#asset-label').text()){
                    availableStrikeLine.push(item)
                };
            }
        });
    };

    $.each(availableStrikeLine , function (i ,item) {
        var strikeLineRateNode;
        strikeLineRateNode   = $$$("#strike-rate-line").cloneNode(true);
        strikeLineRateNode.id  = null;

        CIQ.appendClassName(strikeLineRateNode  , item.actionType == 'Buy' ? 'strike-rate-line strike-rate-line-Call trade'+item.orderID+''   : 'strike-rate-line strike-rate-line-Put trade'+item.orderID+'');

        if(item.actionType == 'Buy'){
            $(strikeLineRateNode).append(
                '<div class="strike-line-element strike-rate-line-Call-action" >BUY</div>'+
                '<div class="strike-line-element strike-rate-line-Call-taid">'+item.orderID+'</div>'+
                '<div class="strike-line-element strike-rate-line-Call-rate">'+item.rate+'</div>'
            );
        } else {
            $(strikeLineRateNode).append(
                '<div class="strike-line-element strike-rate-line-Put-action" >SELL</div>'+
                '<div class="strike-line-element strike-rate-line-Put-taid">'+item.orderID+'</div>'+
                '<div class="strike-line-element strike-rate-line-Put-rate">'+item.rate+'</div>'
            );
        };

        strikeLineRateNode.addEventListener('mouseover',function () {
            $(this).find('.strike-rate-line-Put-taid').css('display', 'inline-block');
            $(this).find('.strike-rate-line-Call-taid').css('display', 'inline-block');
        });

        strikeLineRateNode.addEventListener('mouseout',function () {
            $(this).find('.strike-rate-line-Put-taid').css('display', 'none');
            $(this).find('.strike-rate-line-Call-taid').css('display', 'none');
        });

        var dateTrade = new Date(item.orderDate.timestamp);

        var strikeLine = {
            stx: stxx,
            xPositioner: "date",
            x: new Date(dateTrade),
            label: "strikeLineRate",
            node: strikeLineRateNode
        };

        new CIQ.Marker(strikeLine);
        stxx.draw();
    })
}

function chartIQHitRateLineHoverAction (stxx,currentOrder){
    var strikeLineRateNode;

    strikeLineRateNode   = $$$("#strike-rate-line").cloneNode(true);
    strikeLineRateNode.id  = null;

    CIQ.appendClassName(strikeLineRateNode, currentOrder.actionType == 'Buy' ? 'strike-rate-line strike-rate-line-Call trade' + currentOrder.orderID : 'strike-rate-line strike-rate-line-Put trade'+currentOrder.orderID);

    if(currentOrder.actionType == 'Buy'){
        $(strikeLineRateNode).append(
            '<div class="strike-line-element strike-rate-line-Call-action" >BUY</div>'+
            '<div class="strike-line-element strike-rate-line-Call-taid">'+currentOrder.orderID+'</div>'+
            '<div class="strike-line-element strike-rate-line-Call-rate">'+currentOrder.rate+'</div>'
        );
    } else {
        $(strikeLineRateNode).append(
            '<div class="strike-line-element strike-rate-line-Put-action" >SELL</div>'+
            '<div class="strike-line-element strike-rate-line-Put-taid">'+currentOrder.orderID+'</div>'+
            '<div class="strike-line-element strike-rate-line-Put-rate">'+currentOrder.rate+'</div>'
        );
    }

    strikeLineRateNode.addEventListener('mouseover',function () {
        $(this).find('.strike-rate-line-Put-taid').css('display', 'inline-block');
        $(this).find('.strike-rate-line-Call-taid').css('display', 'inline-block');
    });

    strikeLineRateNode.addEventListener('mouseout',function () {
        $(this).find('.strike-rate-line-Put-taid').css('display', 'none');
        $(this).find('.strike-rate-line-Call-taid').css('display', 'none');
    });

    if (!stxx.masterData) {
        return;
    }

    var strikeLine = {
        stx: stxx,
        xPositioner: "date",
        x: stxx.masterData[stxx.masterData.length-1].DT,
        label: "strikeLineRate",
        node: strikeLineRateNode
    };

    new CIQ.Marker(strikeLine);
    stxx.draw();
}

function clearStrikeRateLine() {
    var strikeRateLine = $('.strike-rate-line');
    stxx.markers.strikeLineRate = null;
    $(strikeRateLine).remove();
}
