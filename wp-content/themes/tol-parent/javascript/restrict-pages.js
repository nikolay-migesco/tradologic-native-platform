function getCookie(name) {
    var value = '; ' + document.cookie;
    var parts = value.split('; ' + name + '=');
    if (parts.length == 2) return parts.pop().split(';').shift();
}

function isUserLogged() {
    if (getCookie('widgetSession') && getCookie('widgetSession')  !== '00000000-0000-0000-0000-000000000000') {
        return true;
    } else {
        return false;
    }
}

function tolWidgetsRedirect(i, url) {
    if (i == 1 && !isUserLogged()) {
        window.location = url;
    } else if (i == 2 && isUserLogged() && getURLParameterByName('hash') == '') {
        window.location = url;
    };
};

function getURLParameterByName (name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
};