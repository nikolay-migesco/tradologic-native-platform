(function () {
    $(document).ready(function () {
        var animateChart, clearPath, corectReset, corectResult, end_line_margin, end_num, movePin, movedLine, numsArr1,
            paper, path, path_position_x, path_position_y, randomInt, rateLoss, rateReset, rateResult, rateWin,
            renderPath, startGame, start_game, start_num, style, time, updateCount, updatePath, updateTimer;
        paper = Snap('#chart');
        time = 10;
        end_line_margin = 0;
        path_position_x = -10;
        path_position_y = 200;
        start_num = -10;
        end_num = 10;
        start_game = false;
        updateCount = 0;
        style = {
            fill: '#00406E',
            stroke: '#fff',
            strokeWidth: 0
        };
        path = paper.path('').attr({
            stroke: 'green',
            fill: 'l(0, 0, 1, 1)rgba(0, 128, 0, 0.74)-transparent',
            strokeWidth: 2.5
        });
        numsArr1 = [226, 200, 190, 210, 195, 180, 177, 170, 172, 168, 160, 170, 163, 157, 156, 150, 140, 145, 138, 120, 124, 116, 136, 133, 138, 149, 132, 110, 90, 95, 80, 120, 123, 115, 110, 125, 129, 150, 156, 157, 163, 174, 176, 189, 200, 203, 207, 199, 196, 200, 190, 193, 187, 200];
        renderPath = function (pathArr) {
            var i, j, len, pathString;
            pathString = "M0,450";
            for (j = 0, len = pathArr.length; j < len; j++) {
                i = pathArr[j];
                path_position_x += 10;
                pathString += "L" + path_position_x + "," + i;
            }
            pathString += "L" + (path_position_x + 10) + ",450";
            path.attr({
                d: pathString
            });
            return path_position_x = -10;
        };
        updatePath = function () {
            movePin(path_position_y);
            updateTimer();
            movedLine();
            renderPath(numsArr1);
            path_position_y = path_position_y + randomInt(start_num, end_num);
            if (path_position_y > 250) {
                path_position_y -= 30;
            }
            if (path_position_y < 80) {
                path_position_y += 30;
            }
            numsArr1.push(path_position_y);
            if (numsArr1.length > 200) {
                $('.loader_wrap').fadeIn(100);
                $('.chart__wrapper').fadeOut(10);
                setTimeout((function () {
                    return clearPath();
                }), 100);
                setTimeout((function () {
                    return $('.chart__wrapper').fadeIn(100);
                }), 1000);
                setTimeout((function () {
                    return $('.loader_wrap').fadeOut(100);
                }), 1000);
            }
            numsArr1 = numsArr1.slice(0);
            updateCount -= 10;
            return $('#chart').css('margin-left', updateCount);
        };
        movePin = function (path_position_y) {
            var pin_pos;
            pin_pos = path_position_y - 35;
            $('#chart_pin').css('top', pin_pos);
            return $('#chart_arr').css('top', pin_pos + 5);
        };
        updateTimer = function () {
            var times;
            time -= 1;
            times = 10;
            if (start_game) {
                if (time < 10) {
                    times = '0' + time;
                } else {
                    times = time;
                }
                if (time < 0) {
                    time = 11;
                }
                return $('.timer__count').text(times);
            } else {
                if (time < 10) {
                    times = '0' + time;
                } else {
                    times = time;
                }
                if (time < 6) {
                    time = 11;
                }
                return $('.timer__count').text(times);
            }
        };
        randomInt = function (lower, upper) {
            var ref, ref1;
            if (upper == null) {
                ref = [0, lower], lower = ref[0], upper = ref[1];
            }
            if (lower > upper) {
                ref1 = [upper, lower], lower = ref1[0], upper = ref1[1];
            }
            return Math.floor(Math.random() * (upper - lower + 1) + lower);
        };
        animateChart = function () {
            var i, j, results;
            results = [];
            for (i = j = 1; j <= 36000; i = ++j) {
                results.push(setTimeout((function () {
                    return updatePath();
                }), i * 1000));
            }
            return results;
        };
        clearPath = function () {
            var step;
            if (numsArr1.length > 70) {
                step = numsArr1.length - 60;
                numsArr1 = numsArr1.slice(step);
                updateCount += step * 10;
                return $('#chart').css('margin-left', updateCount);
            }
        };
        startGame = function (rate) {
            var setPos;
            corectResult(rate);
            start_game = true;
            $('.chart__btn').fadeOut(100);
            $('.chart__timer--big').fadeIn(100);
            $('#arrdown,#arrup').css('opacity', 0);
            $('#arr' + rate).css('opacity', 1);
            setPos = numsArr1[numsArr1.length - 1];
            $('#rate_line').css('top', setPos + 19).show();
            console.log(time);
            setTimeout((function () {
                return rateResult(rate, setPos, path_position_y);
            }), time * 1000);
            return setTimeout((function () {
                return rateReset();
            }), time * 1000);
        };
        corectResult = function (rate) {
            if (rate === 'up') {
                return start_num -= 10;
            } else {
                return end_num += 10;
            }
        };
        corectReset = function (rate) {
            if (rate === 'up') {
                return start_num += 10;
            } else {
                return end_num -= 10;
            }
        };
        rateResult = function (rate, setPos, path_position_y) {
            if (rate === 'up') {
                if (setPos > path_position_y) {
                    return rateWin(rate);
                } else {
                    return rateLoss(rate);
                }
            } else {
                if (setPos < path_position_y) {
                    return rateWin(rate);
                } else {
                    return rateLoss(rate);
                }
            }
        };
        rateReset = function (rate) {
            $('.chart__btn').fadeIn(1000);
            $('.chart__timer--big').fadeOut(100);
            start_game = false;
            $('#rate_line').hide();
            $('#chart_endline').css('margin-left', '0');
            end_line_margin = 0;
            return $('#arrdown,#arrup').css('opacity', .3);
        };
        rateWin = function (rate) {
            $('#chart_win').fadeIn();
            setTimeout((function () {
                return corectReset(rate);
            }), 500);
            return setTimeout((function () {
                return clearPath();
            }), 500);
        };
        rateLoss = function () {
            $('#chart_loss').fadeIn();
            setTimeout((function () {
                return corectReset(rate);
            }), 500);
            return setTimeout((function () {
                return clearPath();
            }), 500);
        };
        movedLine = function () {
            return $('#chart_endline').css('margin-left', time + '0px');
        };
        animateChart();
        $('#chart_up').hover((function () {
            $('#arrup').css('opacity', 1);
        }), function () {
            if (start_game) {
                $('#arrup').css('opacity', 1);
            } else {
                $('#arrup').css('opacity', .6);
            }
        });
        $('#chart_down').hover((function () {
            $('#arrdown').css('opacity', 1);
        }), function () {
            if (start_game) {
                $('#arrdown').css('opacity', 1);
            } else {
                $('#arrdown').css('opacity', .6);
            }
        });
        $('#chart_up').click(function () {
            return startGame('up');
        });
        $('#chart_down').click(function () {
            return startGame('down');
        });
        return $('.chart__block__btn.btn.gray ').click(function () {
            return $('.chart__block').fadeOut();
        });
    });

}).call(this);
