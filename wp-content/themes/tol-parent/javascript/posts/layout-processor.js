"use strict";

jQuery(function ($) {
    Object.getOwnPropertyNames(categoryPosts).forEach((postIndex) => {
        const layout = "html-layout";
        let post = categoryPosts[postIndex];
        const layoutExists = typeof post[layout] !== typeof undefined;
        if (layoutExists) {
            let content = post[layout];
            Object.getOwnPropertyNames(post).forEach((placeholder)=>{
                content = content.replace(new RegExp(`%${placeholder}%`,"g"), post[placeholder]);
            });
            const markup = $(`[data-post-id="${postIndex}"]`)[0];
            markup.innerHTML = content.replace("%content%", markup.innerHTML);
        }
    });
});
