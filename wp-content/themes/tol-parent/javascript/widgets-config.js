/* get urls from plugin config */
var baseUrl = widgetsSettings.baseUrl + '/' + widgetsSettings.widgets_version;
var apiUrl = widgetsSettings.restApiUrl;
var feedApiUrl = widgetsSettings.feedApiUrl;
var loggerUrl = widgetsSettings.loggerUrl;
var facebookAppID = widgetsSettings.facebookAppID;
var googleClientId = widgetsSettings.googleClientId;
var edgeUrl = '/api';
var qqClientId = widgetsSettings.qqId;
var advfeedApiUrl = widgetsSettings.advfeedApiUrl;
var webId = widgetsSettings.webId;
var avatarStorageURL = 'https://images.finte.co/Gamification/Avatars/';
var availableAvatars = ['f_01.jpg', 'm_01.jpg', 'f_02.jpg', 'm_02.jpg','f_03.jpg', 'm_03.jpg','f_04.jpg', 'm_04.jpg', 'f_05.jpg', 'm_05.jpg', 'f_06.jpg', 'm_06.jpg','f_07.jpg', 'm_07.jpg', 'f_08.jpg', 'm_08.jpg', 'f_09.jpg', 'm_09.jpg'];
var automaticLogout = true;
var enableQuickRegistration = true;
var isRegistrationPage = (window.location.href.indexOf('registration') > 0); //add correct URL string where is full registration page
var enableUserProfileFieldsOnCashier = true;
var enableAllOpenTrades = false;

var easyOpenPositionsColumnsDefault = {
    1: false,
    2: false,
    3: true,
    4: true,
    5: true,
    6: true,
    7: false,
    8: true,
    9: true,
    10: true,
    11: false,
    12: true
};
// Forex Open Postion orders columns which are visible/hidden by default
if($.cookie('easyOpenPositionsColumns')){
    var easyOpenPositionsColumns = $.parseJSON($.cookie('easyOpenPositionsColumns'));
}else{
    var easyOpenPositionsColumns = easyOpenPositionsColumnsDefault;
}

var easyClosedPositionsColumnsDefault = {
    1: false,
    2: true,
    3: true,
    4: true,
    5: true,
    6: true,
    7: true,
    8: false,
    9: false,
    10: true,
    11: true,
    12: false,
    13: true,
    14: true
};
// easy forex closed postions columns - visible/hidden by default
if($.cookie('easyClosedPositionsColumns')){
    var easyClosedPositionsColumns = $.parseJSON($.cookie('easyClosedPositionsColumns'));
}else{
    var easyClosedPositionsColumns = easyClosedPositionsColumnsDefault;
}

var binaryOpenTradesColumnsDefault = {
    1: true,    //Trade ID
    2: false,   //Entry Date
    3: true,    //Type
    4: true,    //Asset name
    5: true,    //Invest
    6: true,    //Action
    7: true,    //Strike
    8: true,    //Expiry
    9: false,    //Payout
    10: true,  //Return
    11: false,  //Profit
    12: true  //Actions
};

if($.cookie('binaryOpenTradesColumns')){
    var binaryOpenTradesColumns = $.parseJSON($.cookie('binaryOpenTradesColumns'));
}else{
    var binaryOpenTradesColumns = binaryOpenTradesColumnsDefault;
}
//My Trades Table New Binary default columns
var myTradesTableNewBinaryColumnsDefault = {
    1: true,    //Order ID
    2: true,   //Order  Date
    3: true,    //Asset
    4: true,    //Action
    5: true,    //Trade Rate
    6: true,    //Amount
    7: true,    //Expiry
    8: true,    //Close Rate
    9: true    //Profit
};

if($.cookie('myTradesTableNewBinaryColumns')){
    var myTradesTableNewBinaryColumns = $.parseJSON($.cookie('myTradesTableNewBinaryColumns'));
}else{
    var myTradesTableNewBinaryColumns = myTradesTableNewBinaryColumnsDefault;
}

//My Trades Table New Exchange default columns
var myTradesTableBinaryExchangeNewColumnsDefault = {
    1: true,    //Trade ID
    2: true,   //Timestamp
    3: true,    //Asset
    4: true,    //Strike
    5: true,    //Expiry
    6: true,    //Direction
    7: true,    //Lots
    8: true,    //Price
    9: true,    //Rate
	10: true,    //Fee
    11: true    //PL
};

if($.cookie('myTradesTableBinaryExchangeNewColumns')){
    var myTradesTableBinaryExchangeNewColumns = $.parseJSON($.cookie('myTradesTableBinaryExchangeNewColumns'));
}else{
    var myTradesTableBinaryExchangeNewColumns = myTradesTableBinaryExchangeNewColumnsDefault;
}

//My Trades Table New SimpleForex default columns
var myTradesTableSimpleForexNewColumnsDefault = {
    1: true,    //Asset
    2: true,   //Type
    3: true,    //OpenDate
    4: true,    //TradeRate
    5: true,    //InvestAmount
    6: true,    //Leverage
    7: false,    //StopLoss
    8: false,    //TakeProfit
	9: true,    //CloseRate
    10: true,    //CloseDate
	11: false,    //Swap
    12: true    //Profit
};

if($.cookie('myTradesTableSimpleForexNewColumns')){
    var myTradesTableSimpleForexNewColumns = $.parseJSON($.cookie('myTradesTableSimpleForexNewColumns'));
}else{
    var myTradesTableSimpleForexNewColumns = myTradesTableSimpleForexNewColumnsDefault;
}

//My Trades Table New Simplex default columns
var myTradesTableSimplexNewColumnsDefault = {
    1: true,    //TradeId
    2: true,   //OpenTime
    3: true,    //Asset
    4: true,    //Type
    5: true,    //OpenPrice
    6: true,    //InvestAmount
    7: true,    //Risk
    8: false,    //StopLoss
    9: false,    //TakeProfit
	10: true,    //CloseTime
    11: true,    //ClosePrice
	12: true,    //Swap
	13: true,    //Commission
    14: true   //Profit
};

if($.cookie('myTradesTableSimplexNewColumns')){
    var myTradesTableSimplexNewColumns = $.parseJSON($.cookie('myTradesTableSimplexNewColumns'));
}else{
    var myTradesTableSimplexNewColumns = myTradesTableSimplexNewColumnsDefault;
}

//My Trades Table New RealForex default columns
var myTradesTableRealForexNewColumnsDefault = {
    1: false,    //Position ID
    2: true,   //TradeId
    3: true,    //Asset
    4: true,    //Quantity
    5: true,    //Direction
    6: true,    //Rate
    7: true,    //Close rate
    8: true,    //Date
    9: true,    //Close Date
    10: false,    //Swap
    11: false,  //Commission
    12: true,    //Result
    13: false    //Copied From
};

if($.cookie('myTradesTableRealForexNewColumns')){
    var myTradesTableRealForexNewColumns = $.parseJSON($.cookie('myTradesTableRealForexNewColumns'));
}else{
    var myTradesTableRealForexNewColumns = myTradesTableRealForexNewColumnsDefault;
}


var footerSettingsDefault = {
    1: true,
    2: true,
    3: true,
    4: true,
    5: true
};
// Footer icons
if($.cookie('footerSettings')){
    var footerSettings = $.parseJSON($.cookie('footerSettings'));
}else{
    var footerSettings = footerSettingsDefault;
}

//New Binary closed trades default columns
var binaryClosedTradesColumnsDefault = {
	1: false,  //TradeId
    2: false,  //Entry Date
    3: true,   //Type
    4: true,   //Asset
	5: true,   //Invest
    6: true,   //Action
    7: true,   //Strike
    8: true,   //Expiry time
    9: true,   //Expiry rate
    10: false, //Payout
    11: false  //Profit

};
if($.cookie('binaryClosedTradesColumns')){
    var binaryClosedTradesColumns = $.parseJSON($.cookie('binaryClosedTradesColumns'));
}else{
    var binaryClosedTradesColumns = binaryClosedTradesColumnsDefault;
}

//New Binary signals default columns
var binarySignalsColumnsDefault = {
    1: true,  //Asset
    2: true,  //Validity
    3: true,   //Expiry Time
    4: true,   //Payout
    5: true,   //Signal
    6: true,   //Strength
    7: true,   //Amount
    8: true   //Trade

};
if($.cookie('binarySignalsColumns')){
    var binarySignalsColumns = $.parseJSON($.cookie('binarySignalsColumns'));
}else{
    var binarySignalsColumns = binarySignalsColumnsDefault;
}

function getServerResource() { };

/** DO NOT EDIT BELOW THIS LINE **/
/** Fight Apathy... or don't **/

//*****************************************************************************************************************************************************************//
//************************************************************************** initWidgets() ************************************************************************//
//*****************************************************************************************************************************************************************//

var initWidgets = function () {

    widgets.traderoomloader = '#loadingElement'; // Loading element for the widgets traderoom
    widgets.themedevloader = '.themedevloader'; //Loading element for themedev
    widgets.webId = webId;
    widgets.baseUrl = baseUrl;
    widgets.apiUrl = apiUrl;
    widgets.edgeUrl = edgeUrl;
    widgets.feedApiUrl = feedApiUrl;
    widgets.loggerUrl = loggerUrl;
    widgets.facebookAppID = facebookAppID;
    widgets.qqClientId = qqClientId;
    widgets.googleClientId = googleClientId;
    widgets.filterByAssetType = ['currencies', 'commodities', 'indices', 'futures', 'stocks', 'cryptotokens', 'cryptocoins'];
    widgets.supportedGames = ["digital", "turboBinary", "touch", "range", "advanced"];
    widgets.investAmountMaxLength = 5;
    widgets.clientDateTimeOffset = null;
    widgets.advfeedApiUrl = advfeedApiUrl;
    widgets.avatarStorageURL = avatarStorageURL;
    widgets.automaticLogout = automaticLogout;
    widgets.enableQuickRegistration = enableQuickRegistration;
    widgets.isRegistrationPage = isRegistrationPage;
    widgets.enableUserProfileFieldsOnCashier = enableUserProfileFieldsOnCashier;
    widgets.enableAllOpenTrades = enableAllOpenTrades;
    widgets.footerSettings = $.extend(true, {}, footerSettings);
	widgets.binaryOpenTradesColumns = $.extend(true, {}, binaryOpenTradesColumns);
    widgets.binaryClosedTradesColumns = $.extend(true, {}, binaryClosedTradesColumns);
    widgets.easyOpenPositionsColumns = $.extend(true, {}, easyOpenPositionsColumns);
    widgets.easyClosedPositionsColumns = $.extend(true, {}, easyClosedPositionsColumns);
    widgets.binarySignalsColumns = $.extend(true, {}, binarySignalsColumns);
	widgets.myTradesTableNewBinaryColumns = $.extend(true, {}, myTradesTableNewBinaryColumns);
	widgets.myTradesTableBinaryExchangeNewColumns = $.extend(true, {}, myTradesTableBinaryExchangeNewColumns);
	widgets.myTradesTableSimpleForexNewColumns = $.extend(true, {}, myTradesTableSimpleForexNewColumns);
	widgets.myTradesTableSimplexNewColumns = $.extend(true, {}, myTradesTableSimplexNewColumns);
	widgets.myTradesTableRealForexNewColumns = $.extend(true, {}, myTradesTableRealForexNewColumns);
	widgets.game = "";
    widgets.footerSettings = $.extend( true, {}, footerSettings );
    widgets.binaryOpenTradesColumns = $.extend( true, {}, binaryOpenTradesColumns );
    widgets.binaryClosedTradesColumns = $.extend( true, {}, binaryClosedTradesColumns );
    widgets.binarySignalsColumns = $.extend( true, {}, binarySignalsColumns );
    widgets.game = "";
    widgets.tournamentRedirectUrl = (window.location.hostname.includes("stage") ? "https://demotournaments-stage.tradologic.com/" : "https://demotournaments-qa.tradologic.com/")  + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'traderoom';
    widgets.availableAvatars = availableAvatars;
    widgets.enableRebuy = true;
    widgets.simplexCryptoTraderoom = true;

	$(window).on('notLogged', function() {
        $('#tol-login-popup').modal('toggle');
    });

    // LOADING IS STARTED
    $(window).on('loadingIsStarted', loadingIsStarted);

    // LOADING IS COMPLETED
    $(window).on('loadingIsCompleted', loadingIsCompleted);

    // REGISTER EVENTS
    $(window).on('widgetsRegisterEvents', function () {
        $( window ).on( widgets.events.buttonAlphaClick, function ( event ) {
            if (helper.getParameterByName('product') == 'newbinary') {
                $( '.widgetButtonAlpha' ).addClass( 'isClicked' );
                $( '.widgetButtonBeta' ).removeClass( 'isClicked' );
                $( '.selectionStateLabel' ).html( 'High' );
                $( 'label.selectionStateLabel' ).removeClass( 'call put' ).addClass( 'call' );

                if ($('.widgetDynamicPayout').hasClass('select2-hidden-accessible')) {
                    $('.widgetDynamicPayout, .tol-widgetContracts').select2('destroy');
                };

                $('.widgetDynamicPayout, .tol-widgetContracts').select2({
                    minimumResultsForSearch: Infinity,
                    containerCssClass : "popup-select-wrapper",
                    dropdownCssClass: "popup-select-dropdown"
                });

                if (widgets.game == "touch")
                    $('label.selectionStateLabel').text(widgetMessage.touch_selection_up_short);
                else
                    $('label.selectionStateLabel').text($('.widgetButtonAlpha').text());
            }
        });

        $(window).on( widgets.events.buttonBetaClick, function ( event ) {
            if (helper.getParameterByName('product') == 'newbinary') {
                $( '.widgetButtonBeta' ).addClass( 'isClicked' );
                $( '.widgetButtonAlpha' ).removeClass( 'isClicked' );
                $( '.selectionStateLabel' ).html( 'Low' );
                $( 'label.selectionStateLabel' ).removeClass( 'call put' ).addClass( 'put' );

                if ($('.widgetDynamicPayout').hasClass('select2-hidden-accessible')) {
                    $('.widgetDynamicPayout, .tol-widgetContracts').select2('destroy');
                };

                $('.widgetDynamicPayout, .tol-widgetContracts').select2({
                    minimumResultsForSearch: Infinity,
                    containerCssClass : "popup-select-wrapper",
                    dropdownCssClass: "popup-select-dropdown"
                });

                if ( widgets.game == "touch" )
                    $( 'label.selectionStateLabel' ).text( widgetMessage.touch_selection_down_short );
                else
                    $('label.selectionStateLabel').text($('.widgetButtonBeta').text());
            }
        });

        $(window).on(widgets.events.closedTradesCount, function(event, counter) {
            $('.taborders .counter').html(counter);

            $.each(widgets.easyClosedPositionsColumns, function (i, isVisible) {
                if (isVisible === false){
                    $("#pastInvestments .dataTable th:nth-child(" + i +")").hide();

                    if (widgets.isMobileDevice()) {
                        $("#easyForexClosedTradesTable div[data-type='openPositionsRow'] > div:nth-child("+ i +")").hide();
                    } else {
                        $("#pastInvestments .dataTable td:nth-child("+ i +")").hide();
                    };
                } else {
                    $("#pastInvestments .dataTable th:nth-child(" + i +")").show();

                    if (widgets.isMobileDevice()) {
                        $("#easyForexClosedTradesTable div[data-type='openPositionsRow'] > div:nth-child("+ i +")").show();
                    } else {
                        $("#pastInvestments .dataTable td:nth-child("+ i +")").show();
                    };
                };
            });

            if( $('.account .tab-easy-closed-positions .multiple-selection-content .item').length>0){
                $('.account .tab-easy-closed-positions .multiple-selection-content .item').each(function (el) {
                    if(widgets.easyClosedPositionsColumns[el+1]===true){
                        $(this).addClass('selected');
                    }else if(widgets.easyClosedPositionsColumns[el+1]===false){
                        $(this).removeClass('selected');
                    }
                });
            };
        });

        $(window).on(widgets.events.realForexTradesCount, function(event, counter) {
            $('.tabpositions .counter').html(counter);

            if (widgets.game == 'RealForex') {

                $.each(widgets.openPositionsColumns, function (i, isVisible) {
                    if (isVisible === false){
                        $("#trades .dataTable th:nth-child(" + i +")").hide();

                        if (widgets.isMobileDevice()) {
                            $("#trades .dataTable div[data-type='openPositionsRow'] > div:nth-child("+ i +")").hide();
                        } else {
                            $("#trades .dataTable td:nth-child("+ i +")").hide();
                        };
                    }
                });

                $.each(widgets.pendingOrdersColumns, function (i, isVisible) {
                    if (isVisible === false){
                        $("#orders .dataTable th:nth-child(" + i +")").hide();

                        if (widgets.isMobileDevice()) {
                            $("#orders .dataTable div[data-type='pendingOrderRow'] > div:nth-child("+ i +")").hide();
                        } else {
                            $("#orders .dataTable td:nth-child("+ i +")").hide();
                        };
                    }
                });

                if( $('.account .tabpositions .multiple-selection-content .item').length>0){
                    $('.account .tabpositions .multiple-selection-content .item').each(function (el) {
                        if(widgets.openPositionsColumns[el+1]===true){
                            $(this).addClass('selected');
                        }else if(widgets.openPositionsColumns[el+1]===false){
                            $(this).removeClass('selected');
                        }
                    });
                };

                if( $('.account .taborders .multiple-selection-content .item').length>0){
                    $('.account .taborders .multiple-selection-content .item').each(function (el) {
                        if(widgets.pendingOrdersColumns[el+1]===true){
                            $(this).addClass('selected');
                        }else if(widgets.pendingOrdersColumns[el+1]===false){
                            $(this).removeClass('selected');
                        }
                    });
                };
            } else {
                $.each(widgets.easyOpenPositionsColumns, function (i, isVisible) {
                    if (isVisible === false){
                        $("#trades .dataTable th:nth-child(" + i +")").hide();

                        if (widgets.isMobileDevice()) {
                            $("#trades .dataTable div[data-type='openPositionsRow'] > div:nth-child("+ i +")").hide();
                        } else {
                            $("#trades .dataTable td:nth-child("+ i +")").hide();
                        };
                    } else {
                        $("#trades .dataTable th:nth-child(" + i +")").show();

                        if (widgets.isMobileDevice()) {
                            $("#trades .dataTable div[data-type='openPositionsRow'] > div:nth-child("+ i +")").show();
                        } else {
                            $("#trades .dataTable td:nth-child("+ i +")").show();
                        };
                    };
                });

                if( $('.account .tabeasypositions .multiple-selection-content .item').length>0){
                    $('.account .tabeasypositions .multiple-selection-content .item').each(function (el) {
                        if(widgets.easyOpenPositionsColumns[el+1]===true){
                            $(this).addClass('selected');
                        }else if(widgets.easyOpenPositionsColumns[el+1]===false){
                            $(this).removeClass('selected');
                        }
                    });
                };
            };
            if( $('.footer-settings-icon .multiple-selection-content .item').length>0){
                $('.footer-settings-icon .multiple-selection-content .item').each(function (el) {
                    if(widgets.footerSettings[el+1]===true){
                        $(this).addClass('selected');
                        $('.equity-content .equity-item:nth-child('+(el+1)+')').show();
                    }else if(widgets.footerSettings[el+1]===false){
                        $(this).removeClass('selected');
                        $('.equity-content .equity-item:nth-child('+(el+1)+')').hide();
                    }
                });
            }
        });

        $(window).on(widgets.events.copyBoardEditUnSuccessful, function () {
            apprise(helper.getTranslation("copyBoardEditUnSuccessful", "Edit was unsuccessful."));
        });

        $(window).on(widgets.events.copyBoardUnfollowUnSuccessful, function () {
            apprise(helper.getTranslation("copyBoardUnfollowUnSuccessful", "Unfollow was unsuccessful."));
        });

        $(window).on(widgets.events.copyBoardNoChange, function () {
            apprise(helper.getTranslation("copyBoardNoChange", "No change has been made"));
        });

        $(window).on(widgets.events.copyBoardSuccessfullEdit, function () {
            apprise(helper.getTranslation("copyBoardSuccessfullEdit", "Followee edit was successful!"));
        });

        $(window).on(widgets.events.portfolioAddedSuccessfully, function (event, response) {
            $('#portfolio-modal').modal('toggle');
            apprise(response, {}, function (r) {
                if (r) {
                    window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'my-portfolios';
                    return;
                }
            });
        });

        $(window).on(widgets.events.tradingViewActiveOption, function (event, response) {
            if (typeof tradingViewWidget != 'undefined') {
                setTimeout(function() {
                    tradingViewWidget.setSymbol((typeof response.tradableAssetId != 'undefined' ? response.tradableAssetId.toString() : response.id.toString() ), tradingViewWidget.symbolInterval().interval);
                }, 800);
            };
        });

        $(window).on(widgets.events.assetFilterChanged, function (event) {
            if (helper.getParameterByName('product') == 'newbinary') {
                setTimeout(function(){
                    try {
                        $('.expire-select').select2({
                            minimumResultsForSearch: Infinity,
                            containerCssClass: "expiry-select-wrapper",
                            dropdownCssClass: "expiry-select-dropdown"
                        });
                    } catch (e) {
                    };
                }, 200);
            }
        });

        $(window).on('qestionarySubmittedSuccessfull', function(event, response){
            if (response.data.parameters.showAcceptNotification == 'true') {
                apprise('Some text here when provided');
            }
        });

        $(window).on('questionnaireNotification', function(){
            if ($('.appriseOuter').size() == 0) {
                apprise('Some text here when provided');
            }
        });



        $('#openTradesTable').on({

            mouseenter: function() {
                var strikeLineID =  $(this).attr('id');
                $('.' + strikeLineID ).find('.strike-rate-line-Put-type').css('display', 'inline-block')
                $('.' + strikeLineID ).find('.strike-rate-line-Put-taid').css('display', 'inline-block')
                $('.' + strikeLineID ).find('.strike-rate-line-Call-type').css('display', 'inline-block')
                $('.' + strikeLineID ).find('.strike-rate-line-Call-taid').css('display', 'inline-block')
            },
            mouseleave: function() {
                var strikeLineID = $(this).attr('id');
                $('.' + strikeLineID ).find('.strike-rate-line-Put-type').css('display', 'none')
                $('.' + strikeLineID ).find('.strike-rate-line-Put-taid').css('display', 'none')
                $('.' + strikeLineID ).find('.strike-rate-line-Call-type').css('display', 'none')
                $('.' + strikeLineID ).find('.strike-rate-line-Call-taid').css('display', 'none')
            }
        }, 'tr');


        //CUSTOM FILTER - NO OPTIONS
        $(window).on(widgets.events.noOptionsForCustomPreset, noOptionsForCustomPreset);

        //Trade is started - we must take the amount and the direction
        $(window).on(widgets.events.tradeIsStarted, tradeIsStarted);

        // TRADE IS COMPLETED
        $(window).on(widgets.events.tradeIsCompleted, tradeIsCompleted);

        $(window).on(widgets.events.newBinarySignalTradeCompleted, tradeIsCompleted);

        // No Options Message - Global Milk message
        $(window).on(widgets.events.noOptions, noOptions);

        // Show Milk Message - MULTY VIEW for each box
        $(window).on(widgets.events.noAvailableOptions, noAvailableOptions);

        // Hide Milk Message - MULTY VIEW for each box
        $(window).on(widgets.events.unhideMilkMessage, unhideMilkMessage);

        // Hide Global Milk message
        $(window).on(widgets.events.optionsUpdated, optionsUpdated);

        // Same asset message for multy view
        $(window).on(widgets.events.assetAlreadyUsed, assetAlreadyUsed);

        // LOG IN STARTED
        $(window).on(widgets.events.loginIsStarted, loginIsStarted);

        // LOG IN
        $(window).on(widgets.events.loginIsCompleted, loginIsCompleted);

        // Triggers when user is logged and checks if it stays logged in every couple of seconds
        $(window).on(widgets.events.userLogin, userLogin);

        // Triggers when user is logged out
        $(window).on(widgets.events.userLogout, userLogout);

        //Updates assets ddl - customSelect when new asset from assets table is selected
        $(window).on(widgets.events.linkOptionUpdated, linkOptionUpdated);

        // REGISTRATION
        $(window).on('registrationCompleted', registrationCompleted);

        // RESET PASSWORD
        $(window).on('resetPassword', resetPassword);

        // DISABLE TRADING TOOLS
        $(window).on('tradingToolsAreDisabled', tradingToolsAreDisabled);

        // RESET WIDGET
        $(window).on('resetWidget', resetWidget);

        $(window).on(widgets.events.twoFactorAuthLoginCodeRequired, function(event){
            $('#tol-login-popup [data-type=auth-code]').val('').parent().removeClass('error');
            $('#tol-login-popup [data-type=auth-error]').html('');
            $('#tol-login-popup .login-container, #tol-login-popup .open-real-account').hide();
            $('#tol-login-popup .login2fa-container, #tol-login-popup .auth-btn').show();
        });

        $(window).on(widgets.events.twoFactorAuthStatusChanged, function(event, response){
            $('.profile2fa-container').hide();
            $('.profile2faresult-container span').html(response ? helper.getTranslation('profile2fa-disabled', 'Two-Factor Authentication was successfully disabled.') : helper.getTranslation('profile2fa-enabled', 'Two-Factor Authentication was successfully enabled.'));
            $('.profile2faresult-container').show();

            widgets.api.getUserData([widgets.processUser, function () {
                $(window).trigger(widgets.events.loadingIsCompleted);
            }]);
        });

        $(window).on(widgets.events.realForexQuantityDLLClicked, function (event, scrollArea) {
            if (scrollArea != undefined){
                if ($('[data-type="amount"]:last').find('[data-type="amountcontainer"]').length > 0){
                    var currentHeight = parseInt($(scrollArea).css('height'));
                    $(scrollArea).css('height', parseInt(currentHeight * 1.08));
                    var height = scrollArea.scrollHeight;
                    $('#scrollTable').scrollTop(height);
                }
                else{
                    $(scrollArea).css('height', '100%');
                }
            }
        });

        // show positionID history popup
        $(window).on(widgets.events.forexPostionIdHistory, function(event, response){
            if (response.code == 200) {
                if (response.data.length > 0) {
                    var posId = response.data[0].PositionId,
                        assetName = response.data[0].Description,
                        html = '';

                    $('.position-history-content').mCustomScrollbar('destroy');

                    for (i=0; i < response.data.length; i++) {
                        html += '<div class="posid-history-row ' + (response.data[i].Action).toLowerCase() + '"><div class="posidhistory-left"><div class="posidhistory-left-title ' + (response.data[i].Action).toLowerCase() + '">' + (response.data[i].Action).toUpperCase() + '</div><div class="posidhistory-left-date">' + helper.formatDate(helper.convertUTCDateToLocalDate(new Date(response.data[i].DateCreated), helper.dateFormats.masks.normalDateTime), helper.dateFormats.masks.normalDateTime) + '</div></div><div class="posidhistory-right"><div class="posidhistory-right-title">' + (response.data[i].OrderType == "MO" ? 'Market Order: ' : 'Pending Order: ') + response.data[i].OrderType + response.data[i].OrderId + '</div><div class="posidhistory-right-data"><div><span>Execution price:</span><div>' + response.data[i].ExecutionPrice + '</div></div><div><span>' + response.data[i].Direction + '</span><div>' + response.data[i].Quantity + '</div></div></div></div>' + ((response.data[i].Action).toLowerCase() == 'modified' ? '<div class="modified-add-row"><div><span>Average Price:</span><div>' + response.data[i].AveragePrice + '</div></div><div><span>' + response.data[i].PositionDirection + ':</span><div>' + response.data[i].NewQuantity + '</div></div></div>' : '') + '</div>';
                    };

                    $('#tol-position-history .forex-position-id').html('POS' + posId);
                    $('#tol-position-history .forex-posid-asset').html(assetName);
                    $('#tol-position-history .forex-posid-profit').html(helper.formatCurrency(helper.getCurrencySymbol(), response.data[0].Profit.toFixed(2)));

                    $('.position-history-body').html(html);
                    $('#tol-position-history').modal('show');
                    $('.position-history-content').mCustomScrollbar({theme:"minimal-dark"});
                }
            }
        });

        $(window).on(widgets.events.myTradesTableIsLoaded, function(event, counter) {
            if(widgets.isMobileDevice()){
				$.each(widgets.binaryClosedTradesColumns, function (i, isVisible) {
					if (isVisible === false){
						$("#orders table th:nth-child(" + i +")").hide();
						$("#closedTradesTable tbody div:nth-child("+ i +")").hide();
					} else {
						$("#orders table th:nth-child(" + i +")").show();
						$("#closedTradesTable tbody div:nth-child("+ i +")").show();
					}
				});
			} else {
				$.each(widgets.binaryClosedTradesColumns, function (i, isVisible) {
					if (isVisible === false){
						$("#orders table th:nth-child(" + i +")").hide();
						$("#orders table td:nth-child("+ i +")").hide();
					} else {
						$("#orders table th:nth-child(" + i +")").show();
						$("#orders table td:nth-child("+ i +")").show();
					}
				});
			}


            if( $('.account .taborders .multiple-selection-content .item').length>0){
                $('.account .taborders .multiple-selection-content .item').each(function (el) {
                    if(widgets.binaryClosedTradesColumns[el+1]===true){
                        $(this).addClass('selected');
                    }else if(widgets.binaryClosedTradesColumns[el+1]===false){
                        $(this).removeClass('selected');
                    }
                });
            };
        });

        $(window).on('openTradesUpdated', function() {

            setTimeout(function() {
                $.each(widgets.binaryOpenTradesColumns, function (i, isVisible) {
                    if (isVisible === false){
                        $(".positions-active table th:nth-child(" + i +")").hide();
                        $(widgets.isMobileDevice() ? ".positions-active table tbody div:nth-child("+ i +")" : ".positions-active table tbody td:nth-child("+ i +")").hide();
                    } else {
                        $(".positions-active table th:nth-child(" + i +")").show();
                        $(widgets.isMobileDevice() ? ".positions-active table tbody div:nth-child("+ i +")" : ".positions-active table tbody td:nth-child("+ i +")").show();
                    }
                });

                if( $('.account .tabpositions .multiple-selection-content .item').length>0){
                    $('.account .tabpositions .multiple-selection-content .item').each(function (el) {
							if(widgets.binaryOpenTradesColumns[el+1]===true){
								$(this).addClass('selected');
							}else if(widgets.binaryOpenTradesColumns[el+1]===false){
								$(this).removeClass('selected');
							}
                    });
                };
            }, 300)
        });

        // LANGUAGE UPDATE
        $(window).on('languageUpdate', function (event, response) {
            if (typeof widgets.selectedLang === 'object') {
                if (widgets.selectedIso != '') {
                    $.cookie("_icl_current_language", widgets.selectedIso, {path: "/"});
                    $.cookie("current_language", widgets.selectedIso, {path: "/"});
                }
                if (Object.keys(widgets.selectedLang).length > 0) {
                    // Get param if exist and add it to a new location
                    var query = window.location.search.substring(1);
                    var vars = query.split("&");
                    var getParam = '';
                    for (var i=0;i<vars.length;i++) {
                        var pair = vars[i].split("=");
                        if(pair[0] == 'cpage'){
                            getParam = '?' + vars[i];
                        }
                    }
                    if(widgets.selectedLang.attr('href') != 'javascript:;'){
                        window.location.href = widgets.selectedLang.attr('href') + getParam;
                    }else{
                        window.location.href = widgets.selectedLang.attr('href');
                    }
                }
            }
        });

        // Start Cashier

            //triggers when a deposit limitations is met using the quick deposit widget
                $(window).on(widgets.events.cashierDepositQuickLimitationFail, cashierDepositQuickLimitationFail);

            // triggers when a successful deposit is made
                $(window).on(widgets.events.cashierDepositSuccessful, cashierDepositSuccessful);

            // triggers when a pending deposit is made
                $(window).on(widgets.events.cashierDepositPending, cashierDepositPending);

            // triggers when promo code validation popup opens (initially a wrong promo code is entered)
                $(window).on(widgets.events.cashierPromocodePopupOpen, cashierPromocodePopupOpen);

            // triggers when 'Enter promo code' has been clicked in promo code validation popup
                $(window).on(widgets.events.cashierPromocodeEnter, cashierPromocodeEnter);

            // triggers when the deposit failed for some reason
                $(window).on(widgets.events.cashierDepositUnsuccessful, cashierDepositUnsuccessful);

            // triggeres when the deposit is started, here stays the code to start displaying "processing" animations or effects on deposit forms
                $(window).on(widgets.events.cashierStartDepositProcessing, cashierStartDepositProcessing);

            // triggeres when the deposit ends, here stays the code to end displaying "processing" animations or effects on deposit forms
                $(window).on(widgets.events.cashierEndDepositProcessing, cashierEndDepositProcessing);

            // triggers when iFrame is being prepared for loading
                $(window).on(widgets.events.cashierDepositLoadIframe, cashierDepositLoadIframe);

            // triggers when cashier widget needs to be disabled
                $(window).on(widgets.events.cashierWidgetDisabled, cashierWidgetDisabled);

            // triggers when suspended user is detected
                $(window).on(widgets.events.cashierUserSuspended, cashierUserSuspended);

            // triggers when cashier widget throws an exception during operation, unknown response code or error
                $(window).on(widgets.events.cashierWidgetError, cashierWidgetError);

        //   End Cashier

        //asset filter popup loaded
        $(window).on('assetFilterLoaded', function(){
            $(".scrollable-area-asset-filter .scrollable-area-content").mCustomScrollbar({
                scrollButtons: {
                    enable: false
                },
                horizontalScroll: false,
                autoDraggerLength: true,
                contentTouchScroll: true,
                scrollInertia: 600,
                autoHideScrollbar: true,
                advanced: {
                    autoScrollOnFocus: false,
                    updateOnContentResize: true,
                    updateOnBrowserResize: true
                }
            });

            $('.legend-wrapper span').click(function(){
                $(this).parent().parent().parent().toggleClass('expanded');
                $('.columns fieldset#' + $(this).data('container')).toggle();
            });
        });

        // CHANGE PASSWORD
        $(window).on('changePassword', changePassword);

        // PASSWORD MISMATCH
        $(window).on('passwordMissmatch', passwordMissmatch);

        // Triggers when user details has been updated
        $(window).on('userDetailsUpdated', userDetailsUpdated);

        // AFTER WAGER WON
        $(window).on(widgets.events.afterWagerBonusWon, afterWagerBonusWon);

        // AFTER WAGER POPUP
        $(window).on(widgets.events.afterWagerBonusPopUpShow, afterWagerBonusPopUpShow);

        //BALANCE UPDATED
        $(window).on(widgets.events.balanceUpdated, balanceUpdated);

        //COPY TRADER
        $(window).on(widgets.events.copyTrader, copyTrader);

        //GET SOCIAL TRADER DATA
        $(window).on(widgets.events.socialTraderIdInfo, getSocialTraderId);


        $(window).on(widgets.events.inboxMessagesAvailable, function (event, inboxMessagesCount) {
            $('.messages .messagingNumber').hide();
            if (inboxMessagesCount > 0) {
                $('.messages .messagingNumber').show();
            };
        });

        $(window).on("inboxNoNewMessages", function () {
            $('.messages .messagingNumber').hide();
        });
        $(window).on(widgets.events.inactiveUserLoggedOut, displayLogOutNotification)

        $(window).on(widgets.events.newSignalAdded, function (event) {
            showSignals();
        });

    });

    //-------------------------------------------
    // END REGISTER EVENTS
    //-------------------------------------------


    //----------------------------------
    // CONTACT US
    //----------------------------------

    widgets.postAddLead = function (response) {

        //console.log(response);
        if (response.code == 201) {

            // error with text description
            apprise(l10n.ContactUs_ThankYou);

        } else if (response.code === 400 && response.data.error !== null) {
            // error with text description
            if (widgetMessage[response.data.type] == undefined) {
                apprise(response.data.error);
                return;
            } else {
                apprise(widgetMessage[response.data.type]);
                return;
            }

        } else {
            // general error fallback
            apprise(l10n.ContactUs_Failure);
            return;
        }

    };

    widgets.postContact = function (response) {

        if (response.code == 201) {
            apprise(l10n.ContactUs_ThankYou);
            $('.wrapper-contacts').hide();
        }

    };


    //----------------------------------
    // END CONTACT US
    //----------------------------------

    widgets.init();
    getEncryptedCookie();
};

//*****************************************************************************************************************************************************************//
//************************************************************************** END initWidgets() ********************************************************************//
//*****************************************************************************************************************************************************************//


clientDateTimeOffset: 50000;

//*****************************************************************************************************************************************************************//
//************************************************************************** FUNCTIONS ****************************************************************************//
//*****************************************************************************************************************************************************************//


//---------------------------------------------------
// UPDATE USER DATA
//-------------------------------------------------

function updateData() {
    $.cookie('widgetUserData', JSON.stringify(widgets.user), { path: '/' });

    if (widgetsSettings.enable_demo_flow == "1") {
        applyDemoUserFlow();
    };
}

//---------------------------------------------------
// UPDATE USER DATA
//-------------------------------------------------

function resetWidget() {
    //-----------------------------------
    // Disable BUY button  or Prompt to log in
    //-----------------------------------------------
    if ($.cookie('widgetSession') == null && widgets.isLogged() == false) {
        $('.tradeButton').on("click", function () {
            apprise(widgetMessage.SaveTradeOrder_NotLoggedIn);
        });
    };

    $('.cancelButton').click(function () {
        $('.cancelButton-wrapper, .countdown-wrapper').hide();
        $('.buy-button-fake').show();
        resetTradeBox();
    });

    $('.buy-button-fake').click(function () {
        $('.cancelButton-wrapper, .countdown-wrapper').show();
        $('.tol-tradebox-close-button, .investAmountDDLButton').hide(); // X button
        $('.rate-down, .rate-up, .tol-widgetInvestAmount, .tol-widgetContracts').prop('disabled', true); //disable amount, autotrade, rate
        $('.buy-button-fake').hide();
        $('.countdown-timer').text(3);
        startTimer();
    });
    $( ".button.help").unbind( "click" );
    $('button.help').click(function () {
        if (!$(this).hasClass('active')) {
            $(this).addClass('active');
            $('.helpWrapper').show();
            if (widgets.game == "overUnder") {
                $('.overUnder .scrollContent tr.overUnder').prev().click();
                $('.helpContainerLabels2').show();
                $('.overUnder .scrollContent tr:nth-child(3)').trigger("click");
                var test = $('.overUnder .scrollContent tr.overUnder .payoutOverWrap')[0];
                $(test).click();
            } else {
                if ($('.tol-tradebox-left').is(":visible")) {
                    $('.helpContainerLabels1').show();
                    $('.helpContainerLabels2').hide();
                } else {
                    $('.helpContainerLabels2').show();
                    $('.helpContainerLabels1').hide();
                }
            }
        } else {
            navClose();
            $('.overUnder .scrollContent tr.overUnder').prev().click();
        }
    });

    $('.navClose').click(function () {
        navClose();
    });

    $('.navNext').click(function () {
        showRight(true, 'tradearea1', true, true);
        $('.helpContainerLabels2').show();
        $('.helpContainerLabels1').hide();
    });

    widgets.selectedLang = {};
    widgets.selectedIso = '';
    $('#lang_sel_click ul li ul a').click(function (e) {
        e.preventDefault();
        widgets.selectedLang = $(this);

        var isoCode = (typeof $(this).parent().attr('class') !== 'undefined' ? $(this).parent().attr('class') : $(this).attr('class'));
        isoCode = isoCode.replace('icl-', '').replace('lang_sel_sel', '').replace(' ', '');

        widgets.selectedIso = isoCode;
        var lcid = widgetsSettings.languagesISO[isoCode];
        widgets.setLanguage(lcid);
    });
}

//----------------------------------
// LOG OUT
//-------------------------------------------


function LogoutUser() {
    $.removeCookie('widgetSession', { path: '/' });
    $.removeCookie('token', { path: '/' });
    $.removeCookie('widgetUserData', { path: '/' });

    if (window.location.href !== window.location.href.split("?")[0]) {
        window.location.href = window.location.href.split("?")[0];
    } else {
        if(widgetsSettings.logoutSuccessUrl){
            var url = widgetsSettings.logoutSuccessUrl;
            if(widgetsSettings.lang!='en')
                url = "/"+widgetsSettings.lang+widgetsSettings.logoutSuccessUrl;
            window.location.href = url;
        } else {
            location.reload(true);
        }
    }
};


//----------------------------------
// END LOG OUT
//-------------------------------------------


function framer_callParent() {
    var json = JSON.stringify({ type: 'function', data: arguments });
    window.top.postMessage(json, '*');
}

function redirectToURL() {
    window.location = arguments[0];
}


//======================================
//Functions wich load on every page
//==============================================


jQuery(function ($) {
    //-----------------------------------
    // Disable BUY button  or Prompt to log in
    //-----------------------------------------------
    if ($.cookie('widgetSession') == null && widgets.isLogged() == false) {
        $('.tradeButton').on("click", function () {
            apprise(widgetMessage.SaveTradeOrder_NotLoggedIn);
        });
    };
    //-----------------------------------
    //END  Disable BUY button
    //-----------------

    widgets.selectedLang = {};
    widgets.selectedIso = '';
    $('#lang_sel_click ul li ul a').click(function (e) {
        e.preventDefault();
        widgets.selectedLang = $(this);

        var isoCode = (typeof $(this).parent().attr('class') !== 'undefined' ? $(this).parent().attr('class') : $(this).attr('class'));
        isoCode = isoCode.replace('icl-', '').replace('lang_sel_sel', '').replace(' ', '');

        widgets.selectedIso = isoCode;
        var lcid = widgetsSettings.languagesISO[isoCode];
        widgets.setLanguage(lcid);
    });

    //browser detect
    if (typeof jQuery.browser != 'undefined') {
        if (jQuery.browser.chrome) {
            if (navigator.appVersion.indexOf('Edge') > -1) {
                $('body').addClass('browser-edge');
            } else {
                $('body').addClass('browser-chrome');
            }
        } else if (jQuery.browser.mozilla) {
            $('body').addClass('browser-firefox');
        } else if (jQuery.browser.msie) {
            $('body').addClass('browser-msie');
        } else if (jQuery.browser.safari) {
            $('body').addClass('browser-safari');
        } else if (jQuery.browser.opera) {
            $('body').addClass('browser-opera');
        } else if (jQuery.browser.netscape) {
            $('body').addClass('browser-netscape');
        };
    };
});
//----------------------------------------------
//----------------------- IS USER LOGGED ?
//----------------------------------------------

var tolIsLogged = function () {

        if (widgets.isLogged()) {
            $('.tolUserIsLogged').show(); // Show User Data container
            $('.tolUserIsNotLogged').hide(); // Hide Longin container

            $('body, .pageWrapper').removeClass('notlogged');
            $('body, .pageWrapper').addClass('logged');
            $('#traderoom-secondary').hide();

            $('.first-name').html(widgets.user.firstName);
            userStatusObservable();

        } else {
            $('body, .tolUserIsLogged').hide();
            $('body, .tolUserIsNotLogged').show();

            $('.pageWrapper').removeClass('logged');
            $('.pageWrapper').addClass('notlogged');
            $('#traderoom-secondary').show();
        };
};

//----------------------------------------------
//--------------------END IS USER LOGGED ?
//----------------------------------------------

//----------------------------------------------
//------------------- USER STATUS OBSERVABLE
//----------------------------------------------

var userStatusObservable = function (  ) {

            setInterval( function (  ) {
                var userStatus = widgets.user.status;
                if ( widgets.user.status === 'Suspicious' ) {
                    alert( 'Your Account is being suspended. Please contact us in order to proceed with the verification.' );
                    window.location.href = '/' + ($.cookie( 'current_language' ) == 'en' ? '' : $.cookie( 'current_language' ) + '/') + 'contact-us'; // Redirect after successfull login
                } else if ( userStatus === 'Duplicated phone' ) {
                    alert( 'Your Account is being suspended. The phone number that you provide is already in use. Please navigate to your personal details page in order to change your phone number and you will receive SMS shortly. Submit the code so your Account will be verified.' );
                    window.location.href = '/' + ($.cookie( 'current_language' ) == 'en' ? '' : $.cookie( 'current_language' ) + '/') + 'my-profile'; // Redirect after successfull login
                }
            },
            // Every 5-minutes check for user status
            300000);
};
//------------------------------------------------
//------------------END USER STATUS OBSERVABLE
//------------------------------------------------

var showClosedTrades = function () {
    $(window).trigger(widgets.events.expireOptions);

	$.each(widgets.binaryClosedTradesColumns, function (i, isVisible) {
		if (isVisible === false){
			$("#orders table th:nth-child(" + i +")").hide();
			$(widgets.isMobileDevice() ? "#closedTradesTable tbody div:nth-child("+ i +")" : "#orders table td:nth-child("+ i +")").hide();
		} else {
			$("#orders table th:nth-child(" + i +")").show();
			$(widgets.isMobileDevice() ? "#closedTradesTable tbody div:nth-child("+ i +")" : "#orders table td:nth-child("+ i +")").show();
		}
	});


    if( $('.account .taborders .multiple-selection-content .item').length>0){
        $('.account .taborders .multiple-selection-content .item').each(function (el) {
            if(widgets.binaryClosedTradesColumns[el+1]===true){
                $(this).addClass('selected');
            }else if(widgets.binaryClosedTradesColumns[el+1]===false){
                $(this).removeClass('selected');
            }
        });
    };
};

var showSignals = function () {
    // $(window).trigger(widgets.events.expireOptions);

    $.each(widgets.binarySignalsColumns, function (i, isVisible) {
        if (isVisible === false){
            $("#signalsHeaderTable thead tr th:nth-child(" + i +")").hide();
            if (widgets.isMobileDevice())
                $("#signalsTable tbody tr div.column-div:nth-child(" + i +")").hide();
            else
                $("#signalsTable tbody tr td:nth-child(" + i +")").hide();
            // $(widgets.isMobileDevice() ? "#signalsHeaderTable tbody div:nth-child("+ i +")" : "#signalsTable td:nth-child("+ i +")").hide();
        } else {
            $("#signalsHeaderTable thead tr th:nth-child(" + i +")").show();
            if (widgets.isMobileDevice())
                $("#signalsTable tbody tr div.column-div:nth-child(" + i +")").show();
            else
                $("#signalsTable tbody tr td:nth-child(" + i +")").show()
            // $(widgets.isMobileDevice() ? "#signalsHeaderTable tbody div:nth-child("+ i +")" : "#signalsTable td:nth-child("+ i +")").show();
        }
    });


    if( $('.account .tabsignals .multiple-selection-content .item').length>0){
        $('.account .tabsignals .multiple-selection-content .item').each(function (el) {
            if(widgets.binarySignalsColumns[el+1]===true){
                $(this).addClass('selected');
            }else if(widgets.binarySignalsColumns[el+1]===false){
                $(this).removeClass('selected');
            }
        });
    };

    if (widgets.isMobileDevice()) {
        adjustMobileSignalsColumns();
    }
};

var adjustMobileSignalsColumns = function () {
    var selected = [];
    $.each(widgets.binarySignalsColumns, function (i, value) {
        if (value) selected.push(value);
    });
    var signalsColumn = widgets.isMobileDevice() ? 'div' : 'td';
    if (window.innerWidth > 414)
    {
        if (selected.length < 8) {
            $.each($('#signalsHeaderTable thead tr th, #signalsTable tbody tr ' + signalsColumn), function (i, element) {
                $(element).css('width', 100 / selected.length + '%');
                $(element).find('.cellWrapper').css('padding-right', 0);
                $(element).find('.cellWrapper').css('float', 'left');
                $(element).find('.trade').css('float', 'left');
            });
        } else {
            $.each($('#signalsHeaderTable thead tr th, #signalsTable tbody tr ' + signalsColumn), function (i, element) {
                $(element).css('width', '16.6%');
                // $(element).find('.cellWrapper').css('padding-right', 0);
                // $(element).find('.cellWrapper').css('float', 'right');
                $(element).find('.amount').css('width', '50%');
                $(element).find('.amount').css('float', 'left');
                $(element).find('.trade').css('width', '50%');
                $(element).find('.trade').css('float', 'left');
                if ($(element).is('th') && $(element).hasClass('trade')) {
                    $(element).css('float', 'right');
                    $(element).find('.cellWrapper').css('padding-right', 0);
                    $(element).find('.cellWrapper').css('float', 'left');
                }
                // $(element).find('.trade').css('float', 'left');
            });
        }
    } else {
        if (selected.length < 8) {
            $('.amount').filter('th').css('width', '33.33%');
            $('.trade').filter('th').css('width', '33.33%');
            $('.trade').filter('th').css('text-align', 'left');
            $('.trade').filter('th').css('float', 'left');
            $('.trade').filter('div').parent().css('width', '33.33%');
            $('.trade').filter('div').parent().css('float', 'left');
            $('.trade').filter('div').css('text-align', 'left');
        } else {
            $('.trade').filter('th').css('float', 'right');
            $('.trade').filter('div').parent().css('float', 'right');
        }
    }

    var height = 0;
    $.each($('.tableRowDiv'), function (i, element) {
        height += 100;
    });

    $('.signalsTable .tableBodyDiv').css('height', height + 'px');
}

//----------------------------------------------
//------------ REMOVE HASH PARAMETER FROM URL
//----------------------------------------------

function removeHash() {
    var haveHash = false;

    if (window.location.href.split("?")[1] != undefined) {
        var paramsURL = (window.location.href.split("?")[1]).split("&"),
            stack = '';

        for (i = 0; i < paramsURL.length; i++) {
            if (paramsURL[i].indexOf('hash=') > -1 || paramsURL[i].indexOf('config=') > -1) {
                haveHash = true;
            } else {
                if (stack == '')
                    stack += '?' + paramsURL[i]
                else
                    stack += '&' + paramsURL[i]
            }
        }

        stack = window.location.href.split("?")[0] + '/' + stack;

        if (haveHash) {
            window.location.href = stack;
        };
    };

};

//----------------------------------------------
//------------ END REMOVE HASH PARAMETER FROM URL
//----------------------------------------------

//----------------------------------------------
//----------- ENCRYPT USER/PASSWORD AND SET COOKIES
//----------------------------------------------

function setEncryptedCookie() {
    var days = 30;
    var uName = $('#username').val();
    var pVal = $('#password').val();
    var _uName = '_uCookie';
    var _pVal = '_pCookie';
    var pass = uName;

    $.cookie("_saveChecked", "checked", { expires: days });
    $.cookie(_uName, uName, { expires: days });
    setSecureCookie(pass, _pVal, pVal, days);
}
//----------------------------------------------
//----------- END ENCRYPT USER/PASSWORD AND SET COOKIES
//----------------------------------------------

//----------------------------------------------
//----------- GET USER/PASSWORD FROM COOKIE
//----------------------------------------------

function getEncryptedCookie() {

    var _uName = '_uCookie';
    var uName;
    if ($.cookie(_uName) != undefined && $.cookie(_uName) != 'null') {
        uName = $.cookie(_uName);
        $('#username').val(uName);
    }

    var pass;
    var _pVal = '_pCookie';
    if ($.cookie(_pVal) != undefined && $.cookie(_pVal) != 'null') {
        pass = uName;
        $('#password').val(getSecureCookie(pass, _pVal));
    }

    var saveChecked;
    if ($.cookie("_saveChecked") != undefined && $.cookie("_saveChecked") != 'null') {
        saveChecked = $.cookie("_saveChecked");
    }
    if (saveChecked == "checked")
        $(".savePass").attr('checked', true);
}

//----------------------------------------------
//----------- GET USER/PASSWORD FROM COOKIE
//----------------------------------------------

//----------------------------------------------
//----------- DELELTE USER/PASSWORD FROM COOKIE
//----------------------------------------------

function delEncryptedCookie() {

    var _uName = '_uCookie';
    var _pVal = '_pCookie';

    $.cookie(_uName, null);
    $.cookie(_pVal, null);
    $.cookie("_saveChecked", null);
}

//----------------------------------------------
//----------- DELELTE USER/PASSWORD FROM COOKIE
//----------------------------------------------

//----------------------------------------------
//------ Functions associated with quick deposit
//----------------------------------------------

function depositQuickRedirect() {
    var t = setTimeout(function () {
        window.top.location = "../cashier";
    }, 500);
};


function showPopUpAfterWagerBonus() {
    var msgPopup = $('#divAfterWagerBonus').find('div.afterWagerBonusPopup').find('span').text();
    var numPopup = document.getElementsByClassName("afterWagerBonusPopup");

    if (numPopup.length > 0) {
        document.getElementsByClassName("afterWagerBonusPopup")[0].remove();
        apprise(msgPopup);
    };
};
//----------------------------------------------
//------ END Functions associated with quick deposit
//----------------------------------------------

//*****************************************************************************************************************************************************************//
//************************************************************************** END FUNCTIONS ************************************************************************//
//*****************************************************************************************************************************************************************//

//*****************************************************************************************************************************************************************//
//************************************************************************** EVENTS FUNCTIONS *********************************************************************//
//*****************************************************************************************************************************************************************//

//-------------------------------------------
// LOADING IS STARTED
//-------------------------------------------

function loadingIsStarted(){
    if ($.cookie('widgetSession')) {
        $('.tolUserIsLogged').show(); // Show User Data container
        $('.tolUserIsNotLogged').hide(); // Hide Longin container

        if ($.cookie('widgetUserData')) {
            window.widgetsUser = $.parseJSON($.cookie('widgetUserData'));
            $('.first-name').html(widgetsUser.firstName);
            $('.balance-amount').html(widgetsUser.currencySymbol + ' ' + widgetsUser.balance);
        }

    } else {
        $('.tolUserIsLogged').hide(); // Hide User Data container
        $('.tolUserIsNotLogged').show(); // Show Longin container
    }

    $(widgets.traderoomloader).show();
    $('#loadingElementCashier, .traderoom-loader, .cashier-content-wrap').show();

    /*if (location.pathname.search('traderoom') > -1) {
        if (stxx) {
            initModule(stxx);
        }
    }*/
}

//-------------------------------------------
// END LOADING IS STARTED
//-------------------------------------------


/*********************************************/
/************* BINARY-OPTION IQ-CHART *************/
/*********************************************/

$(window).on('tradeFromChartAssetSelectedBinary', function (event, assetData) {
    var quotefeed = {},
        chartSymbolLabel = document.getElementById('asset-label'),
        iqChartAssetData = {
            assetId: assetData.assetId,
            assetSymbolName: assetData.assetSymbolName
        };

    function toTimestamp(strDate) {
        return Date.parse(strDate);
    }

    quotefeed.url = "https://candles-stage.superdev.com/candles.qa/_search";

    quotefeed.translate = function (response, cb) {
        var lines = response.split('\n'),
            lineToJSON = JSON.parse(lines[0]),
            newQuotes = [],
            data = {},
            status = null,
            errorData = null;

        if (lineToJSON.hits.total) {
            status = "ok";
            data.t = [];

            for (var i = 0; i < lineToJSON.hits.total; i++) {
                errorData =  lineToJSON.hits.hits[i]
                if( typeof errorData == 'undefined'){
                    break;
                }
                data.t.push(lineToJSON.hits.hits[i].sort[0]);
            }
        } else {
            status = "no_data";
        }

        var nodata = status == 'no_data';

        if ( status != 'ok' && !nodata) {
            return;
        };

        if(typeof data.t == 'undefined'){
            console.log('Empty data stream for -->' +  quotefeed.url);
            return
        }

        for (var i = 0; i < data.t.length; i++) {
            newQuotes[i] = {};
            newQuotes[i].DT = lineToJSON.hits.hits[i].sort[0]; // Or set newQuotes[i].DT if you have a JS Date
            newQuotes[i].Open = lineToJSON.hits.hits[i]._source.open;
            newQuotes[i].High = lineToJSON.hits.hits[i]._source.max;
            newQuotes[i].Low = lineToJSON.hits.hits[i]._source.min;
            newQuotes[i].Close = lineToJSON.hits.hits[i]._source.close;
        }

        return newQuotes;
    };

    quotefeed.fetchInitialData = function (symbol, suggestedStartDate, suggestedEndDate, params , cb) {
        getElasticSearchData(this.url, {
            symbol: symbol.toString(),
            resolution: 1,
            from: toTimestamp(suggestedStartDate),
            to: toTimestamp(suggestedEndDate)
        }).done(function (response) {
            var newQuotes = quotefeed.translate(response);
            cb({quotes: newQuotes});
        }).fail(function (status) {
            cb({error: status});
        });
    };

    stxx.attachQuoteFeed(quotefeed);
    CIQ.Animation(stxx,  {tension:0.3});

    stxx.newChart(iqChartAssetData.assetId,null,null,function () {
        chartSymbolLabel.innerHTML = iqChartAssetData.assetSymbolName;
        stxx.chart.yAxis.initialMarginTop = 180;
        stxx.chart.yAxis.initialMarginBottom = 150;
        stxx.yTolerance = 100000;
        stxx.calculateYAxisMargins(stxx.chart.panel.yAxis);
        stxx.draw();
        setTimeout(streamBinaryPrices, 60000);
    });

    function streamBinaryPrices() {
        var newData = {},
            newPrice ;

        if(!widgets.links.tradologic.option){
            console.log('Missing Stream Data for this asset !');
            return
        } else {
            newPrice = widgets.assetsPrices[widgets.links.tradologic.option.tradableAssetId].rate;
        }

        newData.DT = new Date() ;
        newData.Close = Number(newPrice);
        stxx.updateChartData(newData, stxx, {fillGaps: true});
        setTimeout(streamBinaryPrices, 60000);
    }

    CIQ.ChartEngine.prototype.append("draw", function(){
        chartSymbolLabel.innerHTML = iqChartAssetData.assetSymbolName;
    });
    
    setTimeout(function () {
        clearStrikeRateLine();
        generateExistStrikeLine();
    },500)
});


//-------------------------------------------
// LOADING IS COMPLETED
//-------------------------------------------

function loadingIsCompleted() {
	$.each(widgets.binaryOpenTradesColumns, function (i, isVisible) {
		if (isVisible === false){
			$(".positions-active table th:nth-child(" + i +")").hide();
			$(widgets.isMobileDevice() ? ".positions-active table tbody div:nth-child("+ i +")" : ".positions-active table td:nth-child("+ i +")").hide();
		} else {
			$(".positions-active table th:nth-child(" + i +")").show();
			$(widgets.isMobileDevice() ? ".positions-active table tbody div:nth-child("+ i +")" : ".positions-active table td:nth-child("+ i +")").show();
		}
	});

	if( $('.account .tabpositions .multiple-selection-content .item').length>0){
		$('.account .tabpositions .multiple-selection-content .item').each(function (el) {
			if(widgets.binaryOpenTradesColumns[el+1]===true){
				$(this).addClass('selected');
			}else if(widgets.binaryOpenTradesColumns[el+1]===false){
				$(this).removeClass('selected');
			}
		});


    };

    // fix cookie this is because of fast game switching
    $.cookie('_icl_current_language', widgetsSettings.lang, { path: '/' });

	if (!$.cookie('widgetSession') && widgets.isLogged()) {
		$.cookie('widgetSession', widgets.token.sessionId, { path: '/' });
	};

    $('#loadingElement, #loadingElementCashier, .traderoom-loader').hide();
    $('.mainMenu, .bodyWrapper, .widgetContent, .cashierContent, .cashierDepositFields, .tol-withdraw, .cashier-content-wrap, .userdata-wrapper, #content').show();
    $('.themedevloader').addClass('loaded');
    $('.highcharts-tooltip span').addClass('hover');

    if (helper.getParameterByName('product') == 'newbinary') {
        try {
            $('.expire-select').select2({
                minimumResultsForSearch: Infinity,
                containerCssClass: "expiry-select-wrapper",
                dropdownCssClass: "expiry-select-dropdown"
            });
        } catch (e) {
        };
    }

    try {
        $(".scrollable-area-content, .tol-opentrades-table,.tol-assets-table,.tol-pendingorders-table,.tol-closedtrades-table,.tol-signals-table,.dropdown-bonus .tol-body.active, .tol-bonuses .tol-body.pending").mCustomScrollbar("destroy");
        $(".scrollable-area-content, .tol-opentrades-table,.tol-assets-table,.tol-pendingorders-table,.tol-closedtrades-table,.tol-signals-table,.dropdown-bonus .tol-body.active, .tol-bonuses .tol-body.pending").mCustomScrollbar({
            scrollButtons: {
                enable: false
            },
            horizontalScroll: false,
            autoDraggerLength: true,
            autoHideScrollbar: true,
            advanced: {
                autoScrollOnFocus: false,
                updateOnContentResize: true,
                updateOnBrowserResize: true
            }
        });
    } catch (e) { };

    // this is added because of prod cache
    if(!$.cookie("current_language")){
        $.cookie("current_language", (widgets.selectedIso!='' ? widgets.selectedIso : "en"), {path: "/"});
    }

    if (!widgets.isLogged()) {
        $.removeCookie('widgetSession', { path: '/' });
        $.removeCookie('token', { path: '/' });
        $.removeCookie('widgetUserData', { path: '/' });
        $('.tolUserIsLogged').hide();
        $('.tolUserIsNotLogged').show();
    };

    // new binary row click should update the chart with the new asset
    $('.asset-row-element').unbind('click').on('click', function () {
        if (typeof tradingViewWidget != 'undefined') {
            tradingViewWidget.setSymbol($(this).attr('data-id').toString(), tradingViewWidget.symbolInterval().interval);
        }
    });

    if (widgets.isMobileDevice()) {
        $('.assetName, .assetPayout').on('click touch', function () {
            if (typeof $(this).parent().parent().data('taid') != 'undefined') {
                tradingViewWidget.setSymbol($(this).parent().parent().data('taid').toString(), tradingViewWidget.symbolInterval().interval);
                $('.footer-icon-chart a').click();
            };
        });
    };

    $('.touch .put-btn').on('click', function () {
        $('.touchrateBeta').show();
        $('.touchrateAlpha').hide();
    });
    $('.touch .call-btn').on('click', function () {
        $('.touchrateBeta').hide();
        $('.touchrateAlpha').show();
    });


	if (widgets.settings.application.TaxJurisdiction.Value.toLowerCase() == 'true') {
	    $('.jurisdiction').show();
        $('[data-type=JurisdictionCountryCode]').attr('data-isRequired', true);
    }
    if (widgets.settings.application.PassportNumber.Value.toLowerCase() == 'true') {
        $('.passportID').show();
        $('[data-type=IDCard]').attr('data-isRequired', true);
    }
    if (widgets.settings.application.TaxIdentificationNumber.Value.toLowerCase() == 'true') {
        $('.taxIdNum').show();
        $('[data-type=IdentificationNumber]').attr('data-isRequired', true);
    }

    if(helper.getParameterByName('config') != '') {
        $.cookie("config", helper.getParameterByName('config'), {path: "/"});
        $.cookie("configFile", helper.getParameterByName('config'), {path: "/"});
    };



    removeHash();

    setTimeout(function () {
        //LOAD FIRST ASSET-OPTION IN CHART
        var iqChartAssetData = {
            assetId: assetListWidget.data.assetsOptions.id != 'undefined' ? Object.keys(assetListWidget.data.assetsOptions)[0] : Object.keys(assetListWidget.data.assetsOptions)[1],
            assetSymbolName:  Object.values(Object.values(assetListWidget.data.assetsOptions)[0])[0].name
        };
        $(window).trigger('tradeFromChartAssetSelectedBinary', iqChartAssetData);
    },500)
}

//-------------------------------------------
//END LOADING IS COMPLETED
//-------------------------------------------

//---------------------------------------------
// NO OPTION MESSAGE FOR CUSTOM FILTER
//---------------------------------------------

function noOptionsForCustomPreset (event, callBack) {
    if ($('.appriseOuter').size() == 0) {
        var msg = widgetMessage.noOptionsCustomFilter || 'There are no active option for this asset. Click "OK" to reset the filter to all.';
        apprise(msg, { 'alert': true }, function (r) {
            if (r) {
                callBack.call();
            }
        });
    }
};

//---------------------------------------------
// END NO OPTION MESSAGE FOR CUSTOM FILTER
//---------------------------------------------

function tradeIsStarted(event, response) {
    currentOrder = response;
};

//---------------------------------------------
// TRADE IS COMPLETED
//---------------------------------------------

function tradeIsCompleted(event, response) {

    $('.tradeButton').removeAttr('disabled', 'disabled'); //Enables the BuyButton
    $('.tradeButton').attr('disabled', false);
    if (helper.getParameterByName('product') == 'newbinary') {
        widgets.initNotificationStyles();
        if (response.code == 200)
           // CIQ.ChartEngine.prototype.append("draw", chartIQHitRateLine);

            chartIQHitRateLineHoverAction(stxx,currentOrder)
            $('#tol-order-popup').modal('hide');
        return;
    };

    if (response.code == 200) {
        if (helper.getParameterByName('product') != 'newbinary') {
            resetTradeBox();
        } else {
            $('#tol-order-popup').modal('hide');
        };
        apprise(widgetMessage.TradeRoom_SuccessTrade, {}, function (r) { if (r) { showPopUpAfterWagerBonus(); } });
    } else {
        if (helper.getParameterByName('product') != 'newbinary') {
            resetTradeBox();
        };
        var resultMessage = '';
        if (typeof widgetMessage[response.data.type] === 'string') {
            resultMessage = widgetMessage[response.data.type];
            if (typeof response.data.parameters === 'object') {
                for (var key in response.data.parameters) {
                    if (response.data.parameters.hasOwnProperty(key))
                        resultMessage = resultMessage.replace('{' + key + '}', response.data.parameters[key]);
                }
            }
            apprise(resultMessage);
        } else if (typeof response.data.text === 'string') {
            apprise(response.data.text);
        }
    }

};

//---------------------------------------------
// END TRADE IS COMPLETED
//---------------------------------------------

//-----------------------------------------------
// No Options Message - Global Milk message
//-----------------------------------------------

function noOptions (event, response) {
    // stupid fix for hidde the chart if there are no options
    $('.tol-traderoom-wrapper > div').css('z-index','-1');
    $('.noOptions, .tol-no-options').show();

    $(window).trigger('loadingIsCompleted');
    $(window).trigger('linkOptionUpdated');
};

//-----------------------------------------------
// END No Options Message
//-----------------------------------------------

//-----------------------------------------------
// Show Milk Message - MULTY VIEW for each box
//-----------------------------------------------

function noAvailableOptions(event, linkName) {

    $('.noOptionsContainer' + linkName.substr(linkName.length - 1)).show();
};

//----------------------------------------------------
// END Show Milk Message - MULTY VIEW for each box
//----------------------------------------------------

//-----------------------------------------------
// Hide Milk Message - MULTY VIEW for each box
//-----------------------------------------------

function unhideMilkMessage(event, linkName) {
    var i = linkName.substr(linkName.length - 1);
    $('.noOptionsContainer' + i).hide();

    try {
        $(['.tradearea' + i] + ' .optionsDDLWrapper .customSelectInner').html(widgets.links[linkName].option.name);
        $(['.tradearea' + i] + ' .expireDropDownWidget .customSelectInner').html(helper.formatExpiration(new Date(widgets.links[linkName].option.expirationTimestamp)));
    } catch (e) { };


};

//----------------------------------------------------
// END Hide Milk Message - MULTY VIEW for each box
//----------------------------------------------------

//-----------------------------------------------
// Hide Global Milk message
//-----------------------------------------------

function optionsUpdated() {
    $('.tol-traderoom-wrapper > div').css('z-index','unset');
    $('.noOptions, .tol-no-options').hide();
    $(window).trigger('linkOptionUpdated');
};

//-----------------------------------------------
// END Hide Global Milk message
//-----------------------------------------------

//-----------------------------------------------
// Same asset message for multy view
//-----------------------------------------------

function assetAlreadyUsed() {
    apprise(widgetMessage.Asset_already_selected);
};

//-----------------------------------------------
// END Same asset message for multy view
//-----------------------------------------------

//-----------------------------------
// LOG IN STARTED
//-----------------------------------
function loginIsStarted(event) {
    $('.loginPreloader').show();

    if (helper.getParameterByName('config') != '') {
        $.cookie("config", helper.getParameterByName('config'), { path: '/' } );
        $.cookie("configFile", helper.getParameterByName('config'), { path: '/' } );
    }
};
//-----------------------------------
// END LOG IN STARTED
//-----------------------------------


//-----------------------------------
// LOG IN
//-----------------------------------

function loginIsCompleted(event, response) {
    if (response.code == 201) {
        $('.login-success-message').show();

        var sessionId = response.data.sessionId;
        $.cookie('widgetSession', sessionId, { path: '/' });

        if ($(".savePass").is(':checked'))
            setEncryptedCookie();
        else
            delEncryptedCookie();

        widgets.api.getResources({ riskFactor: { game: helper.ucfirst(widgets.game.toLowerCase()) } }, widgets.processResources);
        widgets.api.getUserData([widgets.processUser, function () {
            //widgets.user.id - do whatever you want here
        }]);

        if (widgetsSettings.enable_demo_flow == "1") {
            widgets.api.getUserData([widgets.processUser, function (response) {
                if (response.data) {
                    var user = response.data;
                    if (!user.isVerified && !user.Package)
                        window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'education';
                    else
                        window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'traderoom' + (widgets.isMobileDevice() ? '?product=realm' : '');
                }
            }]);

            return;
        };

        if($.cookie('basicPrice')) {
            window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'cashier' + "?basicPrice=" + $.cookie('basicPrice');
            $.removeCookie("basicPrice", { path: '/' });
        } else if (widgets.isMobileDevice()) {
            window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'traderoom?config=realm';
        } else {
            window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'traderoom?config=forex';
        }

        if(!$.cookie('firstLoginAction')) {
            $.cookie('firstLoginAction', 'true', { path: '/' });
            if (widgetsSettings.enable_portfolio == '1') {
                window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'portfolio';
            } else {
                window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + widgetsSettings.loginSuccessUrl + (widgets.isTraderoom ? ('?game=' + widgets.game) : '');
            }
        }

        return;
    } else {
        $('.loginPreloader').hide();
        $('.login-widget-wrapper').show();
    }
};

//-------------------------------------------
// END LOGIN
//-------------------------------------------

//----------------------------------------------------------------------------------------
// Triggers when user is logged and checks if it stays logged in every couple of seconds
//----------------------------------------------------------------------------------------

function userLogin() {
    tolIsLogged();
    updateData();

};

//-------------------------------------------
// END User is Logged
//-------------------------------------------

//----------------------------------------------------------------------------------------
// Triggers when user is logged out
//----------------------------------------------------------------------------------------

function userLogout() {
    $.removeCookie("firstLoginAction", { path: '/' });
    LogoutUser();
};

//-------------------------------------------
// END User is Logged Out
//-------------------------------------------

//-------------------------------------------------------------------------------
//Updates assets ddl - customSelect when new asset from assets table is selected
//-------------------------------------------------------------------------------

function linkOptionUpdated() {
    if (helper.getParameterByName('product') != 'newbinary') {
        resetTradeBox();
    };
    if ($.cookie('view') == 'multi') {
        try {
            for (var i = 1; i < 5; i++) {
                $(['.tradearea' + i] + ' .expireDropDownWidget .customSelectInner').html(helper.formatExpiration(new Date(widgets.links['tradologic' + i].option.expirationTimestamp)));
                $(['.tradearea' + i] + ' .optionsDDLWrapper .customSelectInner').html(widgets.links['tradologic' + i].option.name);
            }
            var t = setTimeout(function () {
                $('.widgetDynamicPayout').trigger('change');
            }, 100);
        } catch (e) { };
    }
    else {
        try {
            $('.expireDropDownWidget .customSelectInner').html(helper.formatExpiration(new Date(widgets.links.tradologic.option.expirationTimestamp)));
            $('.optionsDDLWrapper .customSelectInner').html(widgets.links.tradologic.option.name);
            var t = setTimeout(function () {
                $('.widgetDynamicPayout').trigger('change');
            }, 100);

        } catch (e) { };
    }
};

//-------------------------------------------------------------------------------------
//END - Updates assets ddl - customSelect when new asset from assets table is selected
//-------------------------------------------------------------------------------------

//----------------------------------
// REGISTRATION
//----------------------------------

function registrationCompleted(event, response) {
    if (response.code == 201) {

        $.cookie('widgetSession', response.data.sessionId, { path: '/' });

        if($.cookie('basicPrice')) {
            window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'cashier' + "?basicPrice=" + $.cookie('basicPrice');
            $.removeCookie("basicPrice", { path: '/' });
        } else {

            if (widgetsSettings.enable_demo_flow == "1") {
                window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'education';
                return;
            }

            window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + widgetsSettings.registrationSuccessUrl;// Redirect after successfull login
            return;
        }

        widgets.api.getUserData([widgets.processUser, function () {
            var userStatus = widgets.user.status;
            if( userStatus === 'Suspicious'){
                $(window).trigger(widgets.events.registrationVerification);
            }else if( userStatus === 'Duplicated phone'){
                alert('The phone number that you provide is already in use. Please enter a new one in order to validate your account.');
                window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'my-profile'; // Redirect after successfull login
            }else {
                window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'traderoom';
            }
        }]);
        return;
    } else {
        // general error fallback
        if (widgetMessage[response.data.type] == undefined) {
            apprise(response.data.text);
            return;
        } else {
            apprise(widgetMessage[response.data.type]);
            return;
        }
    }

};

//----------------------------------
// END REGISTRATION
//----------------------------------

//----------------------------------
// RESET PASSWORD
//----------------------------------

function resetPassword(event, response) {
    if (response.code == 200) {
        $('.forgot-password-success').show();
    } else {
        $('.forgot-password-fail').show();
    };

    $('.forgot-password-container .forgot-outcome-text').html(helper.getTranslation(widgetMessage[response.data.type], response.data.text));
};

//----------------------------------
// END RESET PASSWORD
//----------------------------------

//----------------------------------
// TRADING TOOLS DISABLED
//----------------------------------

function tradingToolsAreDisabled(event) {
    $('.toolBackground').hide();
};

//----------------------------------
// TRADING TOOLS DISABLED
//----------------------------------

//----------------------------------
//   Start Cashier
//----------------------------------

//triggers when a deposit limitations is met using the quick deposit widget
function cashierDepositQuickLimitationFail(event, params) {
    $('[data-widget=cashierCardDepositQuick][data-type=error]').text(widgetMessage.cashierDepositLimitationFail.replace(/\{MinimumDepositSum}/g, params.result.data.parameters.MinimumDepositSum).replace(/\{MaximumDepositSum}/g, params.result.data.parameters.MaximumDepositSum));
    $('[data-widget=cashierCardDepositQuick][data-type=error]').css('padding', '10px 0 5px 0');
    $('[data-widget=cashierCardDepositQuick][data-type=error]').show()
};

// triggers when a successful deposit is made
function cashierDepositSuccessful(event, params) {
    if (params.widgetName != "cashierCardDepositQuick") {
        $('.cashierDepositFields').hide();
        window.location.href = './deposit-successful';
    } else {
        $('[data-widget=cashierCardDepositQuick], .quickDepositInfo, .quick-cancel-BTN, .goToCashierLink').hide();
        $('[data-widget=cashierCardDepositQuick][data-translate=cashierDepositSuccessful], .quick-cancel-BTN.endDeposit').show();
    };
};

// triggers when a pending deposit is made
function cashierDepositPending(event, params) {
    $('.cashierDepositFields').hide();
    window.location.href = './deposit-pending';
};

// triggers when promo code validation popup opens (initially a wrong promo code is entered)
function cashierPromocodePopupOpen(event) {
    $('#loadingElementCashier').hide();
};

// triggers when 'Enter promo code' has been clicked in promo code validation popup
function cashierPromocodeEnter(event) {
    $('.cashierFiedsets').show();
};

// triggers when the deposit failed for some reason
function cashierDepositUnsuccessful(event, params) {
    if (params.widgetName != "cashierCardDepositQuick") {
        window.location.href = './deposit-failed';
    } else {
        $('[data-widget=cashierCardDepositQuick], .quickDepositInfo, .quick-cancel-BTN, .goToCashierLink').hide();
        $('#tryAgainQuick').show();
        $('[data-widget=cashierCardDepositQuick][data-translate=cashierLabelTryAgain], .quick-cancel-BTN.endDeposit').show();
    };
};

// triggeres when the deposit is started, here stays the code to start displaying "processing" animations or effects on deposit forms
function cashierStartDepositProcessing(event) {
    $('.cashierFiedsets').hide();
    $('.ccDepositButtons').hide();
    $('#loadingElementCashier').show();
};

// triggeres when the deposit ends, here stays the code to end displaying "processing" animations or effects on deposit forms
function cashierEndDepositProcessing(event) {
    $('.cashierFiedsets').show();
    $('#loadingElementCashier').hide();
    $('.depositSummary').hide();
    $('.promoCodeLink').hide();
    $('.cashierField').hide();
};

// triggers when iFrame is being prepared for loading
function cashierDepositLoadIframe(event) {
    $('#loadingElementProcessing').hide();
    $('.cashierFiedsets').hide();
};

// triggers when cashier widget needs to be disabled
function cashierWidgetDisabled(event, widgetName, messageType, params) {
    // Code to display the message and disable the widget
    if (messageType == "cashierWithdrawalNotAvailableCreditBalance" || messageType == "cashierWithdrawDemoUser") {
        apprise(widgetMessage[messageType], {}, function (r) { if (r) { window.location.href = './cashier?cpage=deposit'; } });
    } else if (messageType = "cashierCountryNotAllowed" || messageType == "cashierIPNotAllowed") {
        if (widgetName != "cashierCardDepositQuick") {
            apprise(widgetMessage[messageType], {}, function (r) {
                if (r) {
                    window.location.href = './traderoom';
                };
            });
        };
    } else {
        // code to display when messageType -> cashierOpenTradesForCreditBalance
        apprise(widgetMessage[messageType]);
    }
};

// triggers when suspended user is detected
function cashierUserSuspended(event) {
    if (typeof widgetMessage["cashierUserSuspended"] == 'undefined') { //checks whether such a widgetMessage is defined
        apprise('This user is suspended.');
        return;
    } else {
        apprise(widgetMessage["cashierUserSuspended"]);
        return;
    };
};

// triggers when cashier widget throws an exception during operation, unknown response code or error
function cashierWidgetError(event, widgetName, messageType, params) { // params contains the full response object as received from the REST API
    // Code to display an error message
    switch (widgetName) { // widget name
        case 'cashierDeposit':
            alert(widgetMessage[messageType]); // message type, for example cashierPendingWithdrawalForCreditBalance, could be used to get the proper translated message via widgetMessage
            // Put code here to display an error on cashierDeposit widget
            break;
        case 'cashierWithdraw':
            // Put code here to display an error on the cashierWithdraw widget
            break;
    };
};

//----------------------------------
//   End Cashier
//----------------------------------

// --------------------------------------
// CHANGE PASSWORD
// --------------------------------------

function changePassword(event, response) {
    if (response.code == 200) {
        $('.change-password-success').show();
    } else {
        $('.change-password-fail').show();
    };

    $('.change-password-container .change-outcome-text').html(helper.getTranslation(widgetMessage[response.data.type], response.data.text));

    return;
};

// ----------------------------------------
// END CHANGE PASSWORD
// ----------------------------------------

// --------------------------------------
// PASSWORD MISMATCH
// --------------------------------------

function passwordMissmatch(event) {
    apprise(widgetMessage[event.type]);
};

// --------------------------------------
// END PASSWORD MISMATCH
// --------------------------------------

//---------------------------------------------
// Triggers when user details has been updated
//---------------------------------------------

function userDetailsUpdated(response) {
    var myProfileSuccessMsg = widgetMessage['myProfileSuccessMsg'];
    $(".myprofile-successmsg").html(myProfileSuccessMsg);
    widgets.api.getUserData([widgets.processUser, function () {
        var userStatus = widgets.user.status;
        if( userStatus === 'Suspicious' ){
            $(window).trigger(widgets.events.registrationVerificationPhone);
        }else {
            console.log('Status User is Verified');
        }
    }]);
};

//-------------------------------------------------
// END triggers when user details has been updated
//-------------------------------------------------

//---------------------------------------------
// After Wager Bonus
//---------------------------------------------

function afterWagerBonusWon(event) {

};

function afterWagerBonusPopUpShow(event) {
    $('#divAfterWagerBonus').find('div.afterWagerBonusPopup').hide();
};

//-------------------------------------------------
// END After Wager Bonus
//-------------------------------------------------

function balanceUpdated() {
    $.cookie('widgetUserData', JSON.stringify(widgets.user), { path: '/' });
};

function copyTrader(event, params) {
    apprise(params.text);
};

//*****************************************************************************************************************************************************************//
//************************************************************************** END EVENTS FUNCTIONS *****************************************************************//
//*****************************************************************************************************************************************************************//

function getSocialTraderId(event, traderId) {
    if (traderId) {
        window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'trader-info/?traderId=' + traderId; // Redirect to social-traderinfo page
    }
};

function displayLogOutNotification(event) {

    if ($(".appriseOuter").length > 0) {
        return;
    }

    apprise('<p class="inactiveuser-popup">' + helper.getTranslation('inactiveMessage', 'You have been logged out due to inactivity.') + '</p>', { 'alert': true }, function (r) {
       userLogout();
    });

    var inactiveApprise = $('.inactiveuser-popup');
    if (inactiveApprise.length > 0) {
        inactiveApprise.parent().addClass('inactiveUserApprise')
    }

};

$(document).on('click', '.ui-dialog-titlebar-close, .loginFacebookWidget, .loginQQWidget, .loginGoogleWidget', function (event) {
    $('.login-popup-bg').hide();
    $('.login-wrap').hide();
    $('.loginPreloader').hide();
});

function getElasticSearchData(url, params) {
    if (params.resolution == 1) {
        return $.ajax({
            url: url,
            dataType: "text",
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({
                "query": {
                    "bool": {
                        "must": [
                            {"term": {"assetId": params.symbol}}
                        ],
                        "must_not": [
                            {
                                "exists" : { "field" : "length" }
                            }
                        ],
                        "filter": {
                            "range": {
                                "tS": {
                                    "gte": params.from,
                                    "lt": params.to
                                }
                            }
                        }
                    }
                },
                "size":10000,
                "sort" : [{ "tS" : {"order" : "asc"}}]
            })
        });
    } else {
        return $.ajax({
            url: url,
            dataType: "text",
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({
                "query": {
                    "bool": {
                        "must": [
                            {"term": {"assetId": params.symbol}},
                            {"term": {"length": params.resolution}}
                        ],
                        "filter": {
                            "range": {
                                "tS": {
                                    "gte": params.from,
                                    "lt": params.to
                                }
                            }
                        }
                    }
                },
                "size":10000,
                "sort" : [{ "tS" : {"order" : "asc"}}]
            })
        });
    }
};



function generateExistStrikeLine (){
    var availableStrikeLine = [];

    $.each(widgets.openTrades, function (i, item) {
        if(item.description == $('#asset-label').text()){
            availableStrikeLine.push(item)
        }
    });

    $.each(availableStrikeLine , function (i ,item) {
        var strikeLineRateNode;

        strikeLineRateNode   = $$$("#strike-rate-line").cloneNode(true);
        strikeLineRateNode.id  = null;

        CIQ.appendClassName(strikeLineRateNode  , item.actionType == 'Call' ? 'strike-rate-line  strike-rate-line-Call  trade'+item.optionId+''   : 'strike-rate-line strike-rate-line-Put trade'+item.optionId+'');

        if(item.actionType == 'Call'){
            $(strikeLineRateNode).append(
                '<div class="strike-line-element strike-rate-line-Call-action" >CALL</div>'+
                '<div class="strike-line-element strike-rate-line-Call-type">'+widgets.game+'</div>'+
                '<div class="strike-line-element strike-rate-line-Call-taid">'+item.optionId+'</div>'+
                '<div class="strike-line-element strike-rate-line-Call-rate">'+item.rate+'</div>'
            );
        }else {
            $(strikeLineRateNode).append(
                '<div class="strike-line-element strike-rate-line-Put-action" >PUT</div>'+
                '<div class="strike-line-element strike-rate-line-Put-type">'+widgets.game+'</div>'+
                '<div class="strike-line-element strike-rate-line-Put-taid">'+item.optionId+'</div>'+
                '<div class="strike-line-element strike-rate-line-Put-rate">'+item.rate+'</div>'
            );
        }

        strikeLineRateNode.addEventListener('mouseover',function () {
            $(this).find('.strike-rate-line-Put-type').css('display', 'inline-block');
            $(this).find('.strike-rate-line-Put-taid').css('display', 'inline-block');
            $(this).find('.strike-rate-line-Call-type').css('display', 'inline-block');
            $(this).find('.strike-rate-line-Call-taid').css('display', 'inline-block');
        });

        strikeLineRateNode.addEventListener('mouseout',function () {
            $(this).find('.strike-rate-line-Put-type').css('display', 'none');
            $(this).find('.strike-rate-line-Put-taid').css('display', 'none');
            $(this).find('.strike-rate-line-Call-type').css('display', 'none');
            $(this).find('.strike-rate-line-Call-taid').css('display', 'none');
        });

        var dateTrade = new Date(item.orderDate.timestamp);

        var strikeLine = {
            stx: stxx,
            xPositioner: "date",
            x: new Date(dateTrade),
            label: "strikeLineRate",
            node: strikeLineRateNode
        };


        new CIQ.Marker(strikeLine);
        stxx.draw();

    })


}

function chartIQHitRateLineHoverAction (stxx,currentOrder){
    console.log(currentOrder)
    var strikeLineRateNode;
    strikeLineRateNode   = $$$("#strike-rate-line").cloneNode(true);
    strikeLineRateNode.id  = null;

    CIQ.appendClassName(strikeLineRateNode  , currentOrder.isCall ? 'strike-rate-line  strike-rate-line-Call  trade'+currentOrder.optionId+''   : 'strike-rate-line strike-rate-line-Put trade'+currentOrder.optionId+'');

    if(currentOrder.isCall){
        $(strikeLineRateNode).append(
            '<div class="strike-line-element strike-rate-line-Call-action" >CALL</div>'+
            '<div class="strike-line-element strike-rate-line-Call-type">'+widgets.game+'</div>'+
            '<div class="strike-line-element strike-rate-line-Call-taid">'+currentOrder.optionId+'</div>'+
            '<div class="strike-line-element strike-rate-line-Call-rate">'+currentOrder.rate+'</div>'
        );
    }else {
        $(strikeLineRateNode).append(
            '<div class="strike-line-element strike-rate-line-Put-action" >PUT</div>'+
            '<div class="strike-line-element strike-rate-line-Put-type">'+widgets.game+'</div>'+
            '<div class="strike-line-element strike-rate-line-Put-taid">'+currentOrder.optionId+'</div>'+
            '<div class="strike-line-element strike-rate-line-Put-rate">'+currentOrder.rate+'</div>'
        );
    }

    strikeLineRateNode.addEventListener('mouseover',function () {
        $(this).find('.strike-rate-line-Put-type').css('display', 'inline-block')
        $(this).find('.strike-rate-line-Put-taid').css('display', 'inline-block')
        $(this).find('.strike-rate-line-Call-type').css('display', 'inline-block')
        $(this).find('.strike-rate-line-Call-taid').css('display', 'inline-block')
    })

    strikeLineRateNode.addEventListener('mouseout',function () {
        $(this).find('.strike-rate-line-Put-type').css('display', 'none')
        $(this).find('.strike-rate-line-Put-taid').css('display', 'none')
        $(this).find('.strike-rate-line-Call-type').css('display', 'none')
        $(this).find('.strike-rate-line-Call-taid').css('display', 'none')
    })

    var strikeLine = {
        stx: stxx,
        xPositioner: "date",
        x: stxx.masterData[stxx.masterData.length-1].DT,
        label: "strikeLineRate",
        node: strikeLineRateNode
    };


    new CIQ.Marker(strikeLine);
    stxx.draw();
}

function clearStrikeRateLine() {

    var strikeRateLine = $('.strike-rate-line');

    stxx.markers.strikeLineRate = null;

    $(strikeRateLine).remove();

}
