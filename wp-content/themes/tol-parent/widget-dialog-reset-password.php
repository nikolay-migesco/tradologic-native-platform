<?php //if(!TolWidgets::isUserLogged()):?>
<div class="resetpassword-wrap" style="display:none;">
    <div class="box_title"><span>Восстановить пароль</span></div>
    <div class="reset_text">Чтобы восстановить пароль от Личного кабинета, введите адрес электронной почты,
        указанный при регистрации, и нажмите кнопку “Восстановить пароль”. Новый пароль будет выслан на электронную
        почту в течение нескольких минут. <br/>Если у вас остались вопросы, то задайте их нашим специалистам через
        форму <br/><a href="/svyazatsya-s-nami">обратной связи.</a></div>
    <div id="resetpassword_form">
        <form>
            <div class="form_input" style="height:68px !important;">
                <input class="widgetPlaceholder input" type="text" placeholder="Электронная почта"
                       data-widget="resetPassword" data-type="email"
                       data-regexpmessage="Неправильно заполнено поле Эл. почта" data-isrequired="true"/>
            </div>
            <div><input type="button" data-widget="resetPassword" data-type="submit" value="Восстановить пароль"/>
            </div>
            <div class="reset-link"><a href="#login">Войти в личный кабинет</a></div>
        </form>
    </div>
    <div id="close_popup3"><span class="resetpassword-close"></span></div>
</div>
<?php //endif;?>
