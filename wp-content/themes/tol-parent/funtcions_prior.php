<?php
function prior_lang($check = null)
{

    if (!defined('prior_getCurrentIso')) {
        $lang = ICL_LANGUAGE_CODE;
        define('prior_getCurrentIso', $lang ? $lang : 'ru');
    }
    if (!empty($check))
        return prior_getCurrentIso == $check;
    return prior_getCurrentIso;

}

function prior_pageaccept($onpages)
{
    $url = parse_url($_SERVER['REQUEST_URI']);

    if (in_array($url['path'], $onpages) || in_array(substr($url['path'], 1), $onpages))
        return true;

    return false;
}

function TolGetInfo($fieldname)
{
    switch ($fieldname) {

        case 'addressLine1':
        case 'addressLine2':
        case 'affiliateInfo':
        case 'balance':
        case 'birthDay':
        case 'birthMonth':
        case 'birthYear':
        case 'city':
        case 'countryCode':
        case 'cpf':
        case 'currencyCode':
        case 'currencySymbol':
        case 'email':
        case 'firstName':
        case 'gender':
        case 'id':
        case 'isAnonymous':
        case 'isReal':
        case 'languageCode':
        case 'lastName':
        case 'phone':
        case 'refId':
        case 'secondaryPhone':
        case 'subscribeToPromoEmail':
        case 'subscribeToPromoSms':
        case 'title':
        case 'type':
        case 'zip':
            // user data from cookie

            if (!isset($_COOKIE['widgetUserData'])) return false;    // no user data in cookie (not logged-in?)

            $json = stripslashes($_COOKIE['widgetUserData']);
            $userdata = json_decode($json, true);
            return $userdata[$fieldname];

            break;

        default:
            break;
    }

}


function prior_header()
{
    $extrCss = '';
    if (prior_pageaccept(array(/*'traderoom', 'en/traderoom', */
        'cashier', 'en/cashier')))
        $extrCss .= ".site-content {width: 1000px; margin: 0px auto;}\n";
    else {
        echo '<link rel="stylesheet" href="/wp-content/themes/tol-parent/prior.css" type="text/css" media="all" />';
        $extrCss .= ".site-content {width: auto !important;}\n";
    }

    if (prior_pageaccept(array('otkrytie-scheta', 'en/open-account')))
        $extrCss .= ".entry-header .entry-title {visibility:hidden; display: none !important;}\n";

    echo '<style>' . $extrCss . (isset($_COOKIE['widgetSession']) ? '.op-guests-only' : '.op-users-only') . ' {display: none !important;}</style>';
    echo '<style>' . $extrCss . (ICL_LANGUAGE_CODE == 'ru' ? '.op-ru-locale {display:inline-block;} .op-en-locale {display:none;}' : '.op-en-locale {display:inline-block;} .op-ru-locale {display:none;}') . '</style>';
    echo '<style>' . $extrCss . (TolGetInfo('isReal') ? '.op-demo-account' : '.op-real-account') . ' {display: none !important;}</style>';

    if (prior_pageaccept(array('vremya-torgovli-akciyami-kompaniy')) || prior_pageaccept(array('en/company-stocks-trading-time'))) {
        $left_second = 3600 - (intval(gmdate("i")) * 60) - intval(gmdate("s"));
        echo '
<script type="text/javascript">
$(document).ready(function() { 
$(\'#hour_' . intval(gmdate("H")) . ', #line_' . intval(gmdate("H")) . '\').addClass(\'active\');

setTimeout(function() {
var current_hour = $(\'#hours td.active\').attr(\'id\').replace(\'hour_\', \'\');
$(\'#hours td, #lines td\').removeClass(\'active\');
$(\'#hours td#hour_\'+(parseInt(current_hour)+1)+\', #lines td#line_\'+(parseInt(current_hour)+1)).addClass(\'active\');

setInterval(function() {
    var current_hour = $(\'#hours td.active\').attr(\'id\').replace(\'hour_\', \'\');
    if(current_hour==\'23\') current_hour = -1;
    $(\'#hours td, #lines td\').removeClass(\'active\');
    $(\'#hours td#hour_\'+(parseInt(current_hour)+1)+\', #lines td#line_\'+(parseInt(current_hour)+1)).addClass(\'active\'); 
}, 3600000);
    
}, ' . ($left_second * 1000) . ');
});
</script>
';
    }

}

const STRUCTURE_SOURCE = 'wp-content/themes/tol-parent/page-structure/page_structure.xml';

function prior_breadcrumbs()
{
    global $wp;
    $page_id = get_post()->ID;
    $page_alias = (prior_lang() == 'en' ? 'en/' : NULL) . trim(str_replace(home_url('/'), '', home_url($wp->request)));
    //$page_alias = get_post()->post_name;
    if ($page_alias == 'cashier' && !empty($_SERVER['QUERY_STRING'])) $page_alias = $page_alias . '?' . $_SERVER['QUERY_STRING'];
    $page_alias = preg_replace("/\/page\/[0-9]+$/", "/*", $page_alias);
    $delimiter = '&nbsp;&nbsp;&nbsp;<img alt="" src="' . get_stylesheet_directory_uri() . '/images/arrow.gif" style="position: relative; top: -2px;">&nbsp;&nbsp;&nbsp;'; // разделить между "крошками"
    $xml = file_get_contents(STRUCTURE_SOURCE);
    $breadcrumbs = array();
    $paths = array();
    $sxe = new SimpleXMLElement($xml);
    $nodes = $sxe->xpath('//*[@loggedin="' . (isUserLogged() ? 'true' : 'false') . '" and @localization="' . prior_lang() . '"]//*[@page_url="/' . $page_alias . '" or @page_url="/' . $page_alias . '/*"]');
    $nodeCount = 0;

    foreach ($nodes as $node) {
        $breadcrumbs[$nodeCount] = array(($node->attributes()->page_desc ? $node->attributes()->page_desc : $node->attributes()->page_name));
        while ($node = $node->xpath('parent::*')) {
            if (!empty($node[0])) {
                $node = $node[0];
                if ($node->getName() != 'page_structure') {
                    array_unshift($breadcrumbs[$nodeCount], '<a href="' . $node->attributes()->page_url . '">' . ($node->attributes()->page_desc ? $node->attributes()->page_desc : $node->attributes()->page_name) . '</a>');
                }
            } else {
                break;
            }
        }
        $nodeCount++;
    }

    if (count($breadcrumbs) > 0) {
        $paths = array();
        foreach ($breadcrumbs as $breadcrumb) {
            $paths[] = join($delimiter, $breadcrumb);
        }

        $isValid = isset($paths[1]);
        if ($isValid) {
            $isValid = $paths[1] instanceof Countable;
        }
        $pathCont = 0;
        if ($isValid) {
            $pathCont = count($paths[1]);
        }
        echo '<div class="op-breadcumb">' . ($pathCont > 0 ? $paths[1] : $paths[0]) . '</div>';
    }
}

function prior_submenu()
{
    global $wp;
    $left_menu = '';
    $page_id = get_post()->ID;
    $page_name = '/' . (prior_lang() == 'en' ? 'en/' : NULL) . trim(str_replace(home_url('/'), '', home_url($wp->request)));
    if ($page_name == 'cashier' && !empty($_SERVER['QUERY_STRING'])) $page_name = $page_name . '?' . $_SERVER['QUERY_STRING'];
    $page_name = preg_replace("/\/page\/[0-9]+$/", "", $page_name);
    $xml = file_get_contents(STRUCTURE_SOURCE);
    $sxe = new SimpleXMLElement($xml);

    $isLoggedAsString = (isUserLogged() ? 'true' : 'false');
    $nodes = $sxe->xpath('//*[@loggedin="' . $isLoggedAsString . '" and @localization="' . prior_lang() . '"]//*[@page_url="' . $page_name . '" or @page_url="' . $page_name . '/*"]');

    $isValid = isset($nodes[1]);
    $firstNode = null;
    if ($isValid) {
        $firstNode = $nodes[1];
        $isValid = $firstNode instanceof Countable;
    }

    $nodesCount = 0;
    if ($isValid) {
        $nodesCount = count($firstNode);
    }

    $i = 0;
    if ($nodesCount > 0) {
        $i = 1;
    }


    if (isset($nodes[$i])) {
        if ($nodes[$i]->getName() == 'group') $i = 1;
    }
    if (isset($nodes[$i]) && $nodes[$i]->attributes()->show_left_menu == 'true') {
        $item_type = $nodes[$i]->getName();

        if ($item_type == 'item') {
            foreach ($nodes as $node) $parent = $node->xpath('parent::*');
            $parent_url = $parent[0]->attributes()->page_url;
        } elseif ($item_type == 'subitem') {
            foreach ($nodes as $node) $parent = $node->xpath('parent::*');
            foreach ($parent as $node2) $parent2 = $node2->xpath('parent::*');
            $parent_url = $parent2[0]->attributes()->page_url;
            $page_name2 = $parent[0]->attributes()->page_url;
        }

        foreach ($nodes as $item) {
            foreach ($item as $child) {

                if (str_replace('/*', '', $child->attributes()->page_url) == $page_name) $active = 'class="active"';
                else $active = '';
                if (str_replace('/*', '', $child->attributes()->page_url) == $page_name2) $active2 = ' active2';
                else $active2 = '';
                $left_menu .= '<div class="level1' . $active2 . '"><a href="' . str_replace('/*', '', $child->attributes()->page_url) . '" ' . $active . '>' . ($child->attributes()->page_desc ? $child->attributes()->page_desc : $child->attributes()->page_name) . '</a></div>';


                if ($child->attributes()->page_url == $page_name || $child->attributes()->page_url == $page_name2) {
                    foreach ($child->children() as $childs) {
                        if (str_replace('/*', '', $childs->attributes()->page_url) == $page_name) $active2 = 'class="active"';
                        else $active2 = '';
                        $left_menu .= '<div class="level2"><a href="' . str_replace('/*', '', $childs->attributes()->page_url) . '" ' . $active2 . '>' . ($childs->attributes()->page_desc ? $childs->attributes()->page_desc : $childs->attributes()->page_name) . '</a></div>';
                    }
                }

            }
        }

        /* TOP BANNERS */
        $top_banners = $nodes[$i]->attributes()->show_left_top_banners;
        if ($top_banners && $nodes[$i]->attributes()->show_left_menu == 'true') {
            $array_ban = explode('|', $top_banners, 2);
            preg_match_all('|CNT=\[(.*)\]|U', $array_ban[0], $out, PREG_PATTERN_ORDER);
            $count_ban = $out[1][0];
            $array_img = explode('|', $array_ban[1]);
            $count_img = count($array_img);

            $show_top_banners = '';
            if ($count_ban == $count_img && $count_ban > 0 && $count_img > 0) {
                $show_top_banners .= '<div>';
                foreach ($array_img AS $value) {
                    preg_match_all('|IMG=\[(.*)\]|U', $value, $img, PREG_PATTERN_ORDER);
                    $img_params = ' ' . $img[1][0];
                    preg_match_all('|A=\[(.*)\]|U', $value, $link, PREG_PATTERN_ORDER);
                    $link_params = ' ' . $link[1][0];

                    $show_top_banners .= '<div style="padding-bottom:16px;"><a' . $link_params . '><img' . $img_params . '></a></div>';
                }
                $show_top_banners .= '</div>';
            }
        }

        /* BOTTOM BANNERS */
        $bottom_banners = $nodes[$i]->attributes()->show_left_bottom_banners;
        if ($bottom_banners && $nodes[$i]->attributes()->show_left_menu == 'true') {
            $array_ban = explode('|', $bottom_banners, 2);
            preg_match_all('|CNT=\[(.*)\]|U', $array_ban[0], $out, PREG_PATTERN_ORDER);
            $count_ban = $out[1][0];
            $array_img = explode('|', $array_ban[1]);
            $count_img = count($array_img);

            $show_bottom_banners = '';
            if ($count_ban == $count_img && $count_ban > 0 && $count_img > 0) {
                $show_bottom_banners .= '<div style="padding-top:16px;">';
                foreach ($array_img AS $value) {
                    preg_match_all('|IMG=\[(.*)\]|U', $value, $img, PREG_PATTERN_ORDER);
                    $img_params = ' ' . $img[1][0];
                    preg_match_all('|A=\[(.*)\]|U', $value, $link, PREG_PATTERN_ORDER);
                    $link_params = ' ' . $link[1][0];

                    $show_bottom_banners .= '<div style="padding-bottom:16px;"><a' . $link_params . '><img' . $img_params . '></a></div>';
                }
                $show_bottom_banners .= '</div>';
            }
        }


        return '<div id="left_menu">' . $show_top_banners . $left_menu . $show_bottom_banners . '</div>';

    }

}

function getPostDate($date, $lang)
{
    if ($lang == 'en') $months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
    else $months = array("1" => "Января", "2" => "Февраля", "3" => "Марта", "4" => "Апреля", "5" => "Мая", "6" => "Июня", "7" => "Июля", "8" => "Августа", "9" => "Сентября", "10" => "Октября", "11" => "Ноября", "12" => "Декабря");
    $d = date('j', strtotime($date));
    $m = $months[date('n', strtotime($date))];
    $y = date('Y', strtotime($date));
    return $d . ' ' . $m . ' ' . $y;
}
