<?php
/**
 *
 * Template Name: Change Password
 *
 */

get_header(); ?>

<div id="content" role="main">
    <div id="tol-changePassword-layout-99">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-sm-2 form-container justify-content-center">
                    <form>
                        <div class="form-group header">
                            <h3 class="text-left" data-translate="changePassowordPageTitle"></h3>
                        </div>
                        <div class="form-group input-container">
                            <input type="password" data-widget="changePassword" data-type="currentPassword" data-isRequired="true" class="form-control" id="current-password" required/>
                            <label for="current-password" class="lfl" name="current-password"  data-translate="ChangePassword_OldPassword" ></label>
                        </div>
                        <div class="form-group input-container">
                            <input type="password" data-widget="changePassword" data-type="newPassword" data-isRequired="true" class="form-control" id="newPassword" required />
                            <label for="newPassword" class="lfl" name="current-password"  data-translate="ChangePassword_NewPassword"></label>
                        </div>
                        <div class="form-group input-container">
                            <input type="password" data-widget="changePassword" data-type="confirmPassword" data-isRequired="true" class="form-control" id="confirmPassword"  required/>
                            <label for="confirmPassword" class="lfl" name="current-password"  data-translate="ChangePassword_NewPassword2"></label>
                        </div>
                        <div class="form-group">
                            <input type="button" data-widget="changePassword" data-type="submit" data-translate="saveChanges" class="btn btn-success text-center btn-main" />
                        </div>
                        <div class="change-password-success text-center">
                            <img src="/wp-content/themes/tol-child/images/new-template-99/email_success.png" alt="">
                            <p class="text-center change-outcome-text"></p>
                            <div class="btn btn-success text-center btn-main "><a href="../"  data-translate="back_profile"></a></div>
                        </div>
                        <div class="change-password-fail text-center">
                            <img src="/wp-content/themes/tol-child/images/new-template-99/email_unsuccess.png" alt="">
                            <p class="text-center change-outcome-text"></p>
                            <div class="btn btn-success text-center btn-main btn-back-forgotpass" data-translate="back"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    get_footer();
?>