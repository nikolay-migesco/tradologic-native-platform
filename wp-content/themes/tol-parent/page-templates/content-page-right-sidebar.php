<?php
/**
 * 
 *
 * Template Name: Content Right sidebar
 * 
 */

get_header(); ?>
	
	<div id="content" role="main">
        <div class="container">
			<div class="row">
				<div class="col-sm-8">
					<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'content', 'page' ); ?>
						<?php //comments_template( '', true ); ?>
					<?php endwhile; // end of the loop. ?>
				</div>
				<div class="col-sm-2">
					<?php #todo sidebar widget from appearance widgets for content ?>
				</div>
			</div>
        </div>

	</div><!-- #content -->	

<?php get_footer(); ?>