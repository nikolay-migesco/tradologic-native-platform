<?php
/**
 *
 * Template Name: Verification
 *
 */

get_header(); ?>

<div id="content" role="main">
    <div id="tol-verification">
        <section class="verification-section" data-widget="compliance" data-type="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12 text-center header-container">
                        <h2 data-translate="title_verification"></h2>
                    </div>
                </div>
                <ul class="nav nav-tabs nav-justified">
                    <li class="col text-center active">
                        <a class="active" href="#personal-details-tab" data-toggle="tab" data-widget="compliance" data-type="menu" data-target="userdetails"><img src="/wp-content/themes/tol-child/images/new-template-99/verification_unsuccess.png" alt="" class="img-responsive verification-img"><span data-translate="Personal_Details"></span></a>
                    </li>
                    <li class="col text-center">
                        <a href="#questionnaire-tab" data-toggle="tab" data-widget="compliance" data-type="menu" data-target="questionnaires"><img src="/wp-content/themes/tol-child/images/new-template-99/verification_unsuccess.png" alt="" class="img-responsive verification-img"><span data-translate="questionaire"></span></a>
                    </li>
                    <li class="col text-center">
                        <a href="#documents-tab" data-toggle="tab" data-widget="compliance" data-type="menu" data-target="documents"><img src="/wp-content/themes/tol-child/images/new-template-99/verification_unsuccess.png" alt="" class="img-responsive verification-img"><span data-translate="documents"></span></a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="personal-details-tab" data-widget="compliance" data-type="section" data-item="userdetails">
                        <div class="row justify-content-center">
                            <div class="col-sm-2 form-container justify-content-center">
                                <form name="foo">
                                    <div class="form-group header">
                                        <h3 class="text-left" data-translate="Personal_Details"></h3>
                                    </div>
                                    <div class="form-group country-group input-container">
                                        <label for="country" class="lfl" data-translate="Registration_Country"></label>
                                        <select id="country" data-widget="userDetails" data-type="countryCode" data-isRequired="true" class="form-control" ></select>         
                                    </div>
                                    <div class="form-group nationality-group input-container">
                                        <label for="nationality" class="lfl" data-translate="Registration_Nationality"></label>
                                        <select style="'.$a['style'].'" data-widget="userDetails" data-type="nationalityId" data-isRequired="' . $atts['isrequired'] . '" class="form-control" id="nationality" ></select>
                                    </div>
                                    <div class="form-group  input-container">
                                        <label for="" class="lfl" data-translate="Registration_DateOfBirth"></label>
                                        <select data-widget="userDetails" data-type="birthDay" class="form-control"></select>
                                        <select data-widget="userDetails" data-type="birthMonth" class="form-control"></select>
                                        <select data-widget="userDetails" data-type="birthYear" data-regexpMessage="Please enter your date of birth" class="form-control"></select>
                                    </div>
                                    <div class="form-group  input-container">
                                        <input type="text" data-widget="userDetails" data-type="addressLine1"  class="form-control" id="street-address" required/>
                                        <label for="street-address" class="lfl" name="street-address" data-translate="street_adress"></label>
                                    </div>
                                    <div class="form-group  input-container">
                                        <input type="text" data-widget="userDetails" data-type="city" id="city" class="form-control" required/>
                                        <label for="city" class="lfl" name="city" data-translate="Registration_City"></label>
                                    </div>
                                    <div class="form-group country-code-group input-container">
                                        <label for="phone" class="lfl" id="phoneNumber" data-translate="Registration_PhoneNumber"></label>
                                        <?php echo do_shortcode('[userDetailsCountryPhoneCode class="form-control"]'); ?>
                                        <?php echo do_shortcode('[userDetailsAreaPhoneCode class="form-control"]'); ?>
                                        <?php echo do_shortcode('[userDetailsPhone class="form-control"]'); ?>
                                    </div>
                                    <div class="form-group  input-container sendSMS">
                                        <input type="button" data-widget="userDetails" data-type="submit" data-translate="verify" class="btn btn-success text-center btn-main send-sms" />
                                    </div>
                                    <div class="form-group  input-container verification-code">
                                        <label for="verification-code" class="lfl" name="verification-code" data-translate="verification_code"></label>
                                        <input type="number" data-widget="registration" data-type="subscribeToPromoSms" class="form-control" id="verification-code"/>
                                        <input type="button" data-widget="userDetails" data-type="submit" data-translate="submit" class="btn btn-success text-center btn-main verify-sms"/>
                                    </div>
                                    <div class="form-group  input-container">
                                        <input type="button" data-widget="userDetails" data-type="submit" data-translate="next" class="btn btn-success text-center btn-main next" disabled/>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>            
                    <div data-widget="compliance" data-type="section" data-item="questionnaires" class="tab-pane compliance-questionnaire compliance-container row" style="display: none;">
                        <div class="container-fluid">
                            <div class="questionnaireguide" data-translate="questionnareGuide"></div>
                            <div class="questionnaireerror" data-type="questionnaireerror" style="display: none"></div>
                            <div data-widget="compliance" data-type="questions" class="compliance-questions"></div>
                        </div>
                    </div>
                    <div class="tab-pane compliance-documents compliance-container row" data-widget="compliance" data-type="section" data-item="documents" style="display: none;">
                        <div class="documentsguide" data-translate="missingdocuments"></div>
                        <div id="docsguide-upload" class="documentsguide">Upload Document</div>
                        <div class="documenthint"></div>
                        <div data-widget="compliance" data-type="documentsupload"></div>
                        <table class="table table-striped table-responsive">
                            <thead class="doctable">
                                <tr>
                                    <th data-translate="documentName"></th>
                                    <th data-translate="documentType"></th>
                                    <th data-translate="documentState"></th>
                                </tr>
                            </thead>
                            <tbody class="uploadeddocs" data-widget="compliance" data-item="uploadedDocs"></tbody>
                        </table>
                    </div>
                </div>
            </div> <!-- Container-fluid -->
        </section>
    </div>
</div>

<?php
    get_footer();
?>