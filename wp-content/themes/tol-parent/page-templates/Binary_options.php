<?php
/**
 * Template Name: binary options page 
 */

get_header(); ?>
<div class="dis_block clearfix2 padT45 ">
  <div class="container">
    <div class="full text-center">
      <h1><?php echo get_field('section_1_heading');?></h1>
      <p class="font2"><?php echo get_field('serction_text');?></p>
    </div>
    <div class="binary dis_block clearfix2 padB75" >
      <div class="row">
        <div class="col-sm-6 hide-mobile"></div>
        <div class="col-sm-6"><span class="font1 pad1 full"><?php echo get_field('subheading_text');?></span></div>
      </div>
      <div class="row">
        <div class="col-sm-4">
          <div class="padL1 full text-center"><img src=" <?php echo get_field('image_section_1');?>" alt=""></div>
        </div>
        <?php echo get_field('text_right_to_image');?>
      </div>
    </div>
  </div>
  <div class="binary-options trade greybg text-center dis_block clearfix2 padT115 padB90">
    <div class="container">
      <div class="full text-center">
        <h2><?php echo get_field('sectio_2_heading');?></h2>
        <p class="font2"><?php echo get_field('section_2_tex');?></p>
      </div>
      <div class="row">
	  
		<?php $trades = get_field('trades');
				foreach($trades  as $trade ){
		?>	
        <div class="col-sm-3"> <i><img src="<?php echo $trade['image'];?>" alt=""></i> <span><?php echo $trade['heading'];?></span>
          <p><?php echo $trade['trade_text'];?></p>
        </div>
		
		<?php } ?>
        
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>
