<?php
/**
 *
 * Template Name: Markets Binarex page
 *
 */
get_header();
?>


<div id="content" role="main" class="container-fluid markets-binarex-layout">
    <?php $section = get_field('sections'); ?>
    <?php $field_repeter = get_field('markets_item');
    ?>


    <div class="row col-sm-offset-1">
        <div class="col-sm-6">
            <p class="helvetica-bold-head"><?php echo $section[1]['heading']; ?></p>
            <p class="helvetica-bold-dark"><?php echo $section[1]['subheading']; ?></p>
        </div>
        <div class="col-sm-6">
            <span class="breadrumb-line"></span><span class="index-top"><?php echo $section[1]['hint']; ?></span>
        </div>
    </div>
    <div class="row col-sm-offset-1">
        <div class="col-sm-6">
            <p><?php echo $section[1]['text']; ?> </p>
        </div>

    </div>
    <section id="markets-overview">
        <div class="row col-sm-offset-1">
            <div class="col-sm-6">
                <p class="helvetica-bold-head"><?php echo $section[0]['heading']; ?></p>
                <p class="helvetica-bold-dark"><?php echo $section[0]['subheading']; ?></p>
            </div>
            <div class="col-sm-6">
                <span class="breadrumb-line"></span><span class="index-top"><?php echo $section[0]['hint']; ?></span>
            </div>
        </div>
        <div class="row col-sm-offset-1">
            <div class="col-sm-6">
                <div id="videoSection">
                    <img src="<?php echo $section[0]['image']; ?>" alt="">
                </div>
            </div>
            <div class="col-sm-6">
                <p><?php echo $section[0]['text']; ?> </p>
            </div>

        </div>
    </section>
    <section id="markets-items-section">
        <div class="row col-sm-offset-1" >
            <div class=" row col-sm-10 col-sm-offset-1">
                <ul id="markets-navigation" class="nav nav-bar">
                    <a>
                        <li class="col-sm-2 indices-tab" data-translate="filterIndices"></li>
                    </a>
                    <a>
                        <li  class="col-sm-2 forex-tab active" data-translate="menu_forex"></li>
                    </a>
                    <a>
                        <li class="col-sm-2 stocks-tab" data-translate="filterStocks"></li>
                    </a>
                    <a>
                        <li  class="col-sm-2 binary-tab" data-translate="filterBinary"></li>
                    </a>
                    <a>
                        <li class="col-sm-2 commodities-tab" data-translate="filterCommodities"></li>
                    </a>
                </ul>
            </div>
        </div>
        <div class="row col-sm-10 col-sm-offset-1 market-items-container">
            <div class="col-sm-2 markets-item"><a href="<?php echo $field_repeter[0]['link_url']; ?>"><img src="<?php echo $field_repeter[0]['icon_image']; ?>" alt=""> <p class="market-item-heading"><?php echo $field_repeter[0]['heading']; ?></p>
                    <p class="market-item-info"><?php echo $field_repeter[0]['content']; ?></p><img class="market-item-arrow" src="<?php echo $field_repeter[0]['arrow_image']; ?>" alt=""></a>
            </div>
            <div class="col-sm-2 markets-item"> <a href="<?php echo $field_repeter[1]['link_url']; ?>"><img src="<?php echo $field_repeter[1]['icon_image']; ?>" alt=""> <p class="market-item-heading"><?php echo $field_repeter[1]['heading']; ?></p>
                    <p class="market-item-info"><?php echo $field_repeter[1]['content']; ?></p><img class="market-item-arrow" src="<?php echo $field_repeter[1]['arrow_image']; ?>" alt=""></a>
            </div>
            <div class="col-sm-2 markets-item"> <a href="<?php echo $field_repeter[2]['link_url']; ?>"><img src="<?php echo $field_repeter[2]['icon_image']; ?>" alt=""> <p class="market-item-heading"><?php echo $field_repeter[2]['heading']; ?></p>
                    <p class="market-item-info"><?php echo $field_repeter[2]['content']; ?></p><img class="market-item-arrow" src="<?php echo $field_repeter[2]['arrow_image']; ?>" alt=""></a>
            </div>
            <div class="col-sm-2 markets-item"> <a href="<?php echo $field_repeter[3]['link_url']; ?>"><img src="<?php echo $field_repeter[3]['icon_image']; ?>" alt=""><p class="market-item-heading"><?php echo $field_repeter[3]['heading']; ?></p>
                    <p class="market-item-info"><?php echo $field_repeter[3]['content']; ?></p><img class="market-item-arrow" src="<?php echo $field_repeter[3]['arrow_image']; ?>" alt=""></a>
            </div>
        </div>
        <div class="row col-sm-10 col-sm-offset-1 market-items-container">
            <div class="col-sm-2 markets-item"><a href="<?php echo $field_repeter[0]['link_url']; ?>"><img src="<?php echo $field_repeter[0]['icon_image']; ?>" alt=""> <p class="market-item-heading"><?php echo $field_repeter[0]['heading']; ?></p>
                    <p class="market-item-info"><?php echo $field_repeter[0]['content']; ?></p><img class="market-item-arrow" src="<?php echo $field_repeter[0]['arrow_image']; ?>" alt=""></a>
            </div>
            <div class="col-sm-2 markets-item"> <a href="<?php echo $field_repeter[1]['link_url']; ?>"><img src="<?php echo $field_repeter[1]['icon_image']; ?>" alt=""> <p class="market-item-heading"><?php echo $field_repeter[1]['heading']; ?></p>
                    <p class="market-item-info"><?php echo $field_repeter[1]['content']; ?></p><img class="market-item-arrow" src="<?php echo $field_repeter[1]['arrow_image']; ?>" alt=""></a>
            </div>
            <div class="col-sm-2 markets-item"> <a href="<?php echo $field_repeter[2]['link_url']; ?>"><img src="<?php echo $field_repeter[2]['icon_image']; ?>" alt=""> <p class="market-item-heading"><?php echo $field_repeter[2]['heading']; ?></p>
                    <p class="market-item-info"><?php echo $field_repeter[2]['content']; ?></p><img class="market-item-arrow" src="<?php echo $field_repeter[2]['arrow_image']; ?>" alt=""></a>
            </div>
            <div class="col-sm-2 markets-item"> <a href="<?php echo $field_repeter[3]['link_url']; ?>"><img src="<?php echo $field_repeter[3]['icon_image']; ?>" alt=""><p class="market-item-heading"><?php echo $field_repeter[3]['heading']; ?></p>
                    <p class="market-item-info"><?php echo $field_repeter[3]['content']; ?></p><img class="market-item-arrow" src="<?php echo $field_repeter[3]['arrow_image']; ?>" alt=""></a>
            </div>
        </div>
    </section>
</div>
<div class="col-sm-10 col-sm-offset-1">
    <h3 class="bottom_text"><?php echo get_field('bottom_text'); ?></h3>
</div>
    
    <section class="markets-get-started">
        <div class="row col-sm-offset-1 get-started">
            <div class="col-sm-10">
                <p class="roboto-dark"><?php echo $section[2]['heading']; ?></p>
                <?php echo $section[2]['text']; ?>
            </div>
            <div class="col-sm-2">
                <button type="button" class="btn btn-primary trade-now-btn" data-translate="homepageBeginnerOpenAccoutLabel">Open account</button>
            </div>
        </div>
    </section>


</div><!-- #content -->
<?php
get_footer();
?>