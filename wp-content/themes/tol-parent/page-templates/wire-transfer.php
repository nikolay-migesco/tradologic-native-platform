<?php
/**
 * Template Name: Cashier Page no Header and Footer (widget area)
 *
 */

get_template_part( 'iframe', 'header' ); ?>
<script>


$(window).on('loadingIsCompleted', function () {
    setTimeout(function(){
        $('.depositBTN').hide();        
    }, 200);
});

</script>
 <div id="primary" class="site-content has-secondary cashier">
  <div id="content" role="main">

   <?php while ( have_posts() ) : the_post(); ?>
    <?php if ( has_post_thumbnail() ) : ?>
     <div class="entry-page-image">
      <?php the_post_thumbnail(); ?>
     </div><!-- .entry-page-image -->
    <?php endif; ?>

    <?php get_template_part( 'content', 'page' ); ?>

   <?php endwhile; // end of the loop. ?>

  </div><!-- #content -->
 </div><!-- #primary -->

 <?php// get_sidebar( 'cashier' ); ?>

 <div class="clearleft"></div>