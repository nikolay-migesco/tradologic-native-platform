<?php
/**
 *
 * Template Name: Tournaments
 *
 */

get_header(); ?>

<div id="content" role="main">

    <?php load_template(ABSPATH.'wp-content/themes/tol-parent/layouts/' . TOURNAMENTS_LAYOUT . '.php' ); ?>

</div>

<?php
    get_footer();
?>