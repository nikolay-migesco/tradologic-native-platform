<?php
/**
 *
 * Template Name: Forgot Password
 *
 */

get_header(); ?>

<div id="content" role="main">
    <div id="tol-forgotPassword-layout-99">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-sm-2 form-container forgot-password-container justify-content-center">
                    <form>
                        <div class="form-group header">
                            <h3 class="text-left" data-translate="login_forgotpassword"></h3>
                        </div>
                        <div class="form-group input-container">
                            <input data-widget="resetPassword" data-type="email" placeholder="Email" class="form-control" id="email" type="text" data-regexpMessage="Please enter a valid email address" data-isrequired="true" required value="" />
                        </div>
                        <div class="form-group">
                            <input class="btn btn-primary text-center btn-main" type="button" data-translate="sendButton" data-widget="resetPassword" data-type="submit" />
                        </div>
                        <div class="forgot-password-success text-center">
                            <img src="/wp-content/themes/tol-child/images/new-template-99/email_success.png" alt="">
                            <p class="text-center forgot-outcome-text"></p>
                            <div class="btn btn-primary text-center btn-main "><a href="../" data-translate="back_home"></a></div>
                        </div>
                        <div class="forgot-password-fail text-center">
                            <img src="/wp-content/themes/tol-child/images/new-template-99/email_unsuccess.png" alt="">
                            <p class="text-center forgot-outcome-text"></p>
                            <div class="btn btn-primary text-center btn-main btn-back-forgotpass" data-translate="back"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(event){
        $('.btn-back-forgotpass').unbind('click').bind('click', function(){
            $('.forgot-password-fail').hide()
        });
    });
</script>
<?php
    get_footer();
?>