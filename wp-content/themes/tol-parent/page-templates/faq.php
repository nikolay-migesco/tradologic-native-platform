<?php
/**
 * Template Name: FAQ PAGE
 */

 ?>
<?php
		$id = get_the_id();
 ?>
<div class="dis_block clearfix2 padT45 padB75 ">
  <div class="container">
    <div class="full text-center">
      <h1><?php the_title(); ?></h1>
      
    </div>
    
<div class="dis_block clearfix2 padT45">
<p> <?php  $post_object = get_post( $post_id );
			echo $post_object->post_content;
?></p>
</div>
    
    
<div class="faq">
            <div class="panel-group" id="accordion">
			
			<?PHP 
			$arg = array('post_type'=>'FAQ','posts_per_page' => -1);	
			$faqs = get_posts($arg);
			/* echo "<pre>";
			print_r($faqs);
			die(); */
			$i = 1;
			foreach($faqs as $faq){ ?>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <a class="accordion-toggle " data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?php echo $i; ?>">
                  <h4 class="panel-title">
                    <div class="question-heading">
                   
                      <p><?php echo $faq->post_title;?></p>
                    </div>
                    <div class="faqicon"><i class="indicator glyphicon glyphicon-triangle-bottom  pull-right"></i></div>
                    
                    
                  </h4>
                  </a>
                </div>
                <div id="collapseOne<?php echo $i; ?>" class="panel-collapse collapse <?php if( $i == 1){ echo 'in';} ?>">
                  <div class="panel-body">
                    
                      <p><?php echo $faq->post_content;?></p>
                    
                    <div class="clearfix2"></div>
                  </div>
                </div>
              </div>
			  
			  <?php $i++; } ?>
			</div>
          </div> 
  </div>
</div>
<script>
 function toggleChevron(e) {
    $(e.target)
        .prev('.panel-heading')
        .find("i.indicator")
        .toggleClass('glyphicon-triangle-bottom glyphicon-triangle-right');
}
$('#accordion').on('hidden.bs.collapse', toggleChevron);
$('#accordion').on('shown.bs.collapse', toggleChevron);


$('#accordion .panel.panel-default > .panel-heading a').addClass('collapsed');
$('#accordion .panel.panel-default:first > .panel-heading a').removeClass('collapsed');
 $('#accordion .panel.panel-default:first > .panel-heading').addClass('active');
    $('#accordion .panel.panel-default > .panel-heading').on('click',function(e){
      $('#accordion .panel.panel-default > .panel-heading').removeClass('active');      
      if($(this).children('a').hasClass('collapsed'))
      {
      $(this).addClass('active');
    }
    });   
   
</script>