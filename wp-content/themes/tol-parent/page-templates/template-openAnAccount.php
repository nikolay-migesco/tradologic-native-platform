<?php
/**
 *
 * Template Name: Open an account page (with short registration)
 *
 */
get_header();
?>


<div id="content" role="main">



    <div class="container-fluid no-padding-leftRight">
        <div class="col-sm-10 col-sm-offset-1 no-padding-leftRight">  
            <?php if (is_active_sidebar('sidebar-b4binary-short-reg')) : // widgets    ?>

                <div class="sidebar-homepage-2-widgets">
                    <?php dynamic_sidebar('sidebar-b4binary-short-reg'); ?>
                </div><!-- sidebar-b4binary-homepageArea-1-widgets -->

            <?php else: // no widgets ?>

                <div class="loginForm">
                    <div class="col-md-12 quick-form-wrapSidebar openAnAccountForm nopadding-right nopadding-left">
                        <?php if (QUICK_REGISTRATION) { ?>
                            <div class="quick-formSidebar nopadding-right nopadding-left">
                                <div class="row">
                                    <div class="h2 pull-left openAnAccountTitle" data-translate="openAnAccountTitle"></div>
                                </div>
                                <div class="col-lg-6 col-md-12 loginFormOpenAnAccount">

                                    <?php echo do_shortcode('[registrationError]'); ?>
                                    <div class="container-fluid loginFormContainer nopadding-left">
                                        <div class="row">
                                            <div class="form-group col-sm-12">
                    <!--                                <i class="glyphicon glyphicon-user shortRegIcons"></i>-->
                                                <?php echo do_shortcode('[registrationFirstName placeholder="First Name" class="form-control"]'); ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-sm-12">
                    <!--                                <i class="glyphicon glyphicon-user shortRegIcons"></i>-->
                                                <?php echo do_shortcode('[registrationLastName class="form-control"]'); ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-sm-12">
                    <!--                                <i class="glyphicon glyphicon-envelope shortRegIcons"></i>-->
                                                <?php echo do_shortcode('[registrationEmail class="form-control"]'); ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-sm-12">
                    <!--                                <i class="glyphicon glyphicon-earphone shortRegIcons" aria-hidden="true"></i>-->
                                                <?php echo do_shortcode('[registrationPhone class="form-control"]'); ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-sm-12">
                    <!--                                <i class="glyphicon glyphicon-lock shortRegIcons"></i>-->
                                                <?php echo do_shortcode('[registrationPassword class="form-control"]'); ?>
                                            </div>
                                        </div>
                                        <!--                                        <div class="row">
                                                                                    <div class="form-group col-sm-12">
                                        <?php // echo do_shortcode('[registrationConfirmPassword class="form-control"]'); ?>
                                                                                    </div>
                                                                                </div>-->

                                        <div class="row">
                                            <div class="form-group col-sm-12 submit1">
                                                <?php echo do_shortcode('[registrationCountryCode isrequired="false" class="countryCodeWrapperHidden" style="display:none;"]') ?>
                                                <?php echo do_shortcode('[registrationSubmit class="btn btn-primary cust-submit"]'); ?>
                                                <span class="subm-pic"></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-sm-12 tosTextOpenAnAccount">
                                                <?php echo do_shortcode('[registrationTos]'); ?> I have read and agreed to <br> <a
                                                    href="./terms-and-conditions" target="_blank" class="tosLinkopenAcc termsCond"  data-translate="termsConditions">Terms &amp;
                                                    Conditions</a> and <a href="./privacy-policy" target="_blank" class="pPolicyL tosLinkopenAcc"
                                                                      data-translate="privacyPolicy">Privacy policy</a><span
                                                                      class="moreInfoOpenAcc">*</span>
                                            </div>
                                        </div>
                                    </div>         
                                </div>
                                <div class="col-lg-6 col-md-12 openAnAccInfo">
                                    <div class="row">
                                        <div class="openAnAccountStatement firstStatement text-center" data-translate="firstStatementOpenAcc"></div>
                                    </div>
                                    <div class="row">
                                        <div class="openAnAccountText text-center" data-translate="firstTextOpenAcc"></div>
                                    </div>
                                    <div class="row">
                                        <div class="openAnAccountStatement secondStatement text-center" data-translate="secondStatementOpenAcc"></div>
                                    </div>
                                    <div class="row">
                                        <div class="openAnAccountText secondText text-center" data-translate="secondTextOpenAcc"></div>
                                    </div>

                                    <div class="row">
                                        <img class="img-responsive center-block openAccLogo" src="<?php echo get_template_directory_uri() ?>/images/b4binary/logo.png"/> 
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>




            <?php endif; ?>
        </div>
    </div>



</div>

<?php
get_footer();
?>

