<?php
/**
 *
 * Template Name: Traderoom page
 *
 */
require_once "../../../../wp-config.php";

$games = array('digital', 'range', 'touch', 'oneTouch', 'turboBinary', 'advanced', 'forex', 'ticks', 'knockin');
$result = array();

foreach ($games as $game) {
    $_GET['game'] = $game;
    
    ob_start();
    if(isset($game) && defined(strtoupper($game).'_TR')){
        if(constant(strtoupper($game).'_TR')!=NULL)
            load_template(ABSPATH.'wp-content/themes/tol-parent/layouts/'.constant(strtoupper($game).'_TR').'.php', false );  
    }else{
        load_template(ABSPATH.'wp-content/themes/tol-parent/layouts/'.TRADEROOM_LAYOUT.'.php', false );  
    }
    $result[$game]['single'] = ob_get_contents();
    ob_end_clean();
}

echo json_encode($result);