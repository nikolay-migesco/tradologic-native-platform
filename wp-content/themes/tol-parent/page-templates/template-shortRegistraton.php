<?php
/**
 *
 * Template Name: Inner Pages  (with right sidebar registration)
 *
 */
get_header();
?>



<div id="content" <?php echo (CUSTOM_CLASS) ? 'class="content-86"' : '' ?> role="main">
    <div class="container-fluid">
        <div class="pageHeadTitle">
            <?php require_once(ABSPATH . 'wp-content/themes/tol-parent/layouts/' . BREADCRUMB . '.php' ); ?>
        </div>
        <header class=" entry-header">
            <h1 class="entry-title blue-heading"><?php the_title(); ?></h1>
        </header>

        <div class="tolUserIsLogged">
            <div class="col-sm-12 nopadding">

                <div class="entry-content">
                    <?php the_content(); ?>
                </div><!-- .entry-content -->
            </div>
        </div>
    </div>

    <div class="tolUserIsNotLogged">
        <div class="container-fluid no-padding-all">
            <div class="col-sm-10 <?php echo(ADD_CLASS_REMOVE_CLASS) ? 'width-100 padding-left-right-30' : 'col-sm-offset-1 no-padding-all' ?>">
                <div class="col-sm-8 no-padding-all">
                    <div class="entry-content">
                        <?php the_content(); ?>
                    </div><!-- .entry-content -->
                </div>

                <div class="col-sm-4">
                    <?php
                    if (CUSTOM_SIDEBAR_REG) {
                        if (is_active_sidebar('sidebar-b4binary-short-reg')) { // widgets    
                            ?>

                            <div class="sidebar-homepage-2-widgets">
                                <?php dynamic_sidebar('sidebar-b4binary-short-reg'); ?>
                            </div><!-- sidebar-b4binary-homepageArea-1-widgets -->

                        <?php } else { // no widgets  ?>

                            <div class="loginForm">
                                <div class="col-md-12 quick-form-wrapSidebar nopadding-rightAtAll">
                                    <?php if (QUICK_REGISTRATION) { ?>
                                        <div class="panel panel-default quick-formSidebar">
                                            <div class="text-center h2 quickSignUpHeader" data-translate="quick_signup_title"></div>
                                            <?php echo do_shortcode('[registrationError]'); ?>
                                            <div class="container-fluid loginFormContainer">
                                                <div class="row">
                                                    <div class="form-group col-sm-12">
                                                        <i class="glyphicon glyphicon-user shortRegIcons"></i>
                                                        <?php echo do_shortcode('[registrationFirstName placeholder="First Name" class="form-control"]'); ?>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-sm-12">
                                                        <i class="glyphicon glyphicon-user shortRegIcons"></i>
                                                        <?php echo do_shortcode('[registrationLastName class="form-control"]'); ?>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-sm-12">
                                                        <i class="glyphicon glyphicon-envelope shortRegIcons"></i>
                                                        <?php echo do_shortcode('[registrationEmail class="form-control"]'); ?>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-sm-12">
                                                        <i class="glyphicon glyphicon-earphone shortRegIcons" aria-hidden="true"></i>
                                                        <?php echo do_shortcode('[registrationPhone class="form-control"]'); ?>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-sm-12">
                                                        <i class="glyphicon glyphicon-lock shortRegIcons"></i>
                                                        <?php echo do_shortcode('[registrationPassword class="form-control"]'); ?>
                                                    </div>
                                                </div>
                                                <!--                                        <div class="row">
                                                                                            <div class="form-group col-sm-12">
                                                <?php // echo do_shortcode('[registrationConfirmPassword class="form-control"]');  ?>
                                                                                            </div>
                                                                                        </div>-->
                                                <div class="row">
                                                    <div class="form-group col-sm-12">
                                                        <?php echo do_shortcode('[registrationTos]'); ?> I have read and agreed to <a
                                                            href="./terms-and-conditions" target="_blank"  data-translate="termsConditions">Terms &amp;
                                                            Conditions</a> and <a href="./privacy-policy" target="_blank" class="pPolicyL"
                                                                              data-translate="privacyPolicy">Privacy policy</a><span
                                                                              class="red">*</span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-sm-12 submit1">
                                                        <?php echo do_shortcode('[registrationCountryCode isrequired="false" class="countryCodeWrapperHidden" style="display:none;"]') ?>
                                                        <?php echo do_shortcode('[registrationSubmit class="btn btn-primary cust-submit"]'); ?>
                                                        <span class="subm-pic"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>




                            <?php
                        }
                    } else {
                        load_template(ABSPATH . 'wp-content/themes/tol-parent/layouts/template-sidebars/sidebar-short-reg.php');
                    }
                    ?>

                </div> 
            </div>
        </div>

    </div>


</div>

<?php
get_footer();
?>

