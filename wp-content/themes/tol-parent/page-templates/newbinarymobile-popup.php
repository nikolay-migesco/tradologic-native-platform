 
 <?php 
    setcookie('view', 'single', time()+3600*24 );

    $game = "digital";
    $digital_active = 'active';
    $range_active = '';
    $touch_active = '';
    $turbo_active = '';
    $oneTouch_active = '';
    $advanced_active = '';
    $forex_active = '';
    $ticks_active = '';

    switch (@$_GET['game']) {
        case "digital":
            $game = "digital";
            $digital_active = 'active';
            $range_active = '';
            break;
        case "range":
            $game = "range";
            $digital_active = '';
            $turbo_active = '';
            $range_active = 'active';
            break;
        case "touch":
            $game = "touch";
            $digital_active = '';
            $touch_active = 'active';
            $range_active = '';
            break;
        case "oneTouch":
            $game = "oneTouch";
            $digital_active = '';
            $range_active = '';
            $oneTouch_active = 'active';
            break;
        case "turboBinary":
            $game = "turboBinary";
            $digital_active = '';
            $turbo_active = 'active';
            break;
        case "advanced":
            $game = "advanced";
            $digital_active = '';
            $advanced_active = 'active';
            break;
        case "forex":
            $game = "forex";
            $digital_active = '';
            $forex_active = 'active';
            break;
        case "ticks":
            $game = "ticks";
            $digital_active = '';
            $ticks_active = 'active';
            break;
        default:
            $game = "digital";
    };

    setcookie('game', $game);
    setcookie('chartType', 'chartTypeCandle');
    ?>
 <!-- NEW ORDER POP UP -->
 <div id="tol-order-popup" class="modal fade" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="tol-order-label">
        <div class="modal-dialog modal-sm modal-default" role="document">
            <div class="modal-content <?php echo $game; ?>">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="tol-social-label">New Trade</h4>
                    <h4 class="modal-title"><?php  echo do_shortcode('[assetName game="' . $game . '"]'); ?></h4>
                </div>
                <div class="modal-body new-order-fields">
                    <div style="display: none;">
                        <?php echo do_shortcode('[buttonAlphaBeta game="' . $game . '" direction="Alpha" skiptouchrate="skip" translate="call_'.$game.'"]'); ?>
                        <?php if ($game == "range") { ?>
                            <span data-widget="<?php echo $game ?>_tradologic_rateAlpha" class="widgetRateAlpha col-sm-12 col-xs-4"></span>
                        <?php } ?>
                        <div class="tol-rate-label">
                            <?php echo do_shortcode('[rate game="' . $game . '" screen="true" class="form-control input-lg col-sm-12"]'); ?>
                        </div>
                        <?php if ($game == "range") { ?>
                            <span data-widget="range_tradologic_rateBeta" class="widgetRateBeta col-sm-12 col-xs-4"></span>
                        <?php } ?>
                        <?php echo do_shortcode('[buttonAlphaBeta game="' . $game . '" direction="Beta" skiptouchrate="skip" translate="put_'.$game.'"]'); ?>
                    </div>
                    <div class="row" style="margin:0;">
                        <div class="col-xs-12 nopadding new-order-row">
                            <div class="col-xs-5 nopadding">
                                <label data-translate="action"></label>
                            </div>
                            <div class="col-xs-7 text-primary nopadding">
                                <label class="selectionStateLabel"></label>
                            </div>
                        </div>
                        <div class="col-xs-12 nopadding new-order-row">
                            <div class="col-xs-5 nopadding">
                                <label data-translate="<?php echo ($game == 'range' ? 'menu_range' : ($game == 'touch' ? 'our_rate' : 'strike_level')) ?>"></label>
                            </div>
                            <div class="col-xs-7 rate-wrapper nopadding">
                                <?php echo do_shortcode('[rateRisk game="' . $game . '"]'); ?>
                            </div>
                        </div>
                        <?php if ($game == 'touch') { ?>
                            <div class="col-xs-12 nopadding new-order-row">
                                <div class="col-xs-5 nopadding">
                                    <label data-translate="touch_rate"></label>
                                </div>
                                <div class="col-xs-7 rate-wrapper nopadding">
                                    <span data-widget="touch_tradologic_rateAlpha" class="touchrateAlpha"></span>
                                    <span data-widget="touch_tradologic_rateBeta" class="touchrateBeta"></span>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if ($game != 'advanced' && $game != 'digital') { ?>
                            <div class="col-xs-12 nopadding new-order-row">
                                <div class="col-xs-5 nopadding">
                                    <label data-translate="payout"></label>
                                </div>
                                <div class="col-xs-7 rate-wrapper nopadding">
                                    <?php echo do_shortcode('[payout game="' . $game . '"]'); ?>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if ($game != 'advanced') { ?>
                            <div class="col-xs-12 nopadding new-order-row">
                                <div class="col-xs-5 nopadding">
                                    <label data-translate="option_expiry"></label>
                                </div>
                                <div class="col-xs-7 rate-wrapper nopadding">
                                    <?php echo do_shortcode('[expireDisplay game="' . $game . '"]'); ?>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="col-xs-12 nopadding new-order-row select-row first-margin">
                            <label class="ddl-label" data-translate="right-box-amount"></label>
                            <?php echo do_shortcode('[investAmount game="' . $game . '"]'); ?>
                        </div>
                        <?php if ($game == "digital") { ?>
                            <div class="col-xs-12 nopadding new-order-row select-row">
                                <label class="ddl-label" data-translate="payout"></label>
                                <select class="form-control btn-default form-control-sm" data-widget="digital_tradologic_payoutDynamic"></select>
                            </div>
                            <div class="col-xs-12 nopadding new-order-row select-row">
                                <label class="ddl-label" data-translate="contracts"></label>
                                <select  data-widget="digital_tradologic_contracts" class="form-control btn-default form-control-sm tol-widgetContracts"></select>
                            </div>
                        <?php } ?>
                        <?php if ($game == 'advanced') { ?>
                            <div class="col-xs-12 col-sm-12 nopadding new-order-row" style="margin-top: 10px !important;">
                                <div class="col-xs-5 nopadding">
                                    <label data-translate="duration"></label>
                                </div>
                            </div>
                            <div class="col-xs-12 nopadding new-order-row">
                                <div data-widget="advanced_tradologic_advancedExpire" data-settings='{"isSelect": false, "extraClass": "the-class-name"}' class="widgetPlaceholder widgetAdvancedDurationButton" style="display:inline-block;"></div>
                            </div>
                            <div class="col-xs-12 nopadding new-order-row" style="height: 60px;margin-top:10px !important;">
                                <span data-widget="advanced_tradologic_duration" class="spanExpiry" style="display:inline-block;"></span>
                                <div data-widget="advanced_tradologic_advancedLength" class="durationSlider"></div>
                                <input data-widget="advanced_tradologic_myTradesFromDate" class="widgetPlaceholder advancedDurationDatePicker" type="text" onkeypress="return false">
                            </div>

                            <div id="payout-return-holder" class="col-xs-12 durationSlider">
                                <div class="col-xs-6 nopadding">
                                    <span data-translate="returnLabelTradebox" class="payoutDesc"></span>
                                </div>
                                <div class="col-xs-6 nopadding text-right">
                                    <span data-translate="payoutLabel" class="returnDesc"></span>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="returnOnLossProfit"></div>
                                <div data-widget="advanced_tradologic_payoutAdvancedBinary" class="slider-payout"></div>
                                <div class="payoutProfit"></div>
                            </div>
                            <div class="col-xs-12" style="margin-top: 9px;padding: 0;">
                                <span class="returnHighAmount adv-game" data-widget="advanced_tradologic_amountBeta" data-hidesymbol="false" data-defaultvalueload='false' style="display: block;float: left;padding:0px;"></span>
                                <span class="returnLowAmount adv-game" data-widget="advanced_tradologic_amountAlpha" data-hidesymbol="false" data-defaultvalueload='false' style="display: block;float: right;padding:0px;"></span>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <?php if ($game != 'advanced') { ?>
                    <div class="new-order-summary col-sm-12">
                        <div class="col-xs-6 col-sm-6 small-text">
                            <small data-translate="inTheMoneyLabel"></small>
                        </div>
                        <div class="col-xs-6 col-sm-6">
                            <?php echo do_shortcode('[amount  direction="win" game="' . $game . '"]'); ?>
                        </div>
                        <div class="col-xs-6 col-sm-6 small-text">
                            <small data-translate="outTheMoneyLabel"></small>
                        </div>
                        <div class="col-xs-6 col-sm-6">
                            <?php echo do_shortcode('[amount direction="lose" game="' . $game . '"]'); ?>
                        </div>
                    </div>
                <?php } ?>
                <div class="new-order-btn">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php echo do_shortcode('[buttonBuy game="' . $game . '"]'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END NEW ORDER POP UP -->