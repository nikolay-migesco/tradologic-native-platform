<?php
/**
 *
 * Template Name: Binary Options Binarex page
 *
 */
get_header();
?>


<div id="content" role="main" class="container-fluid binary-options-binarex-layout">
    <?php $section = get_field('sections'); ?>
    <div class="row col-sm-offset-1">
        <div class="col-sm-6">
            <p class="helvetica-bold-head"><?php echo $section[0]['heading']; ?></p>
            <p class="helvetica-bold-dark"><?php echo $section[0]['subheading']; ?></p>
        </div>
        <div class="col-sm-6">
            <span class="breadrumb-line"></span><span class="index-top"><?php echo $section[0]['hint']; ?></span>
        </div>
    </div>

    <div class="row col-sm-offset-1">
        <div class="col-sm-4">
            <p><?php echo $section[0]['text']; ?> </p>
        </div>
        <div class="col-sm-8">
            <div id="videoSection">
                <img src="<?php echo $section[0]['image']; ?>" alt="">
            </div>
        </div>
    </div>
    
    <section class="benefits-trading col-sm-6" >
        <div class="row col-sm-offset-1">
            <div class="col-sm-offset-1" >
                <p class="helvetica-bold-head"><?php echo $section[1]['heading']; ?></p>
                <p class="helvetica-bold-dark"><?php echo $section[1]['subheading']; ?></p>
            </div>
        </div>
        <div class="row col-sm-offset-1">
            <div class="col-sm-8 col-sm-offset-1">
                <?php echo $section[1]['text']; ?>
            </div>
        </div>
    </section>
    
    
    <section class="difference-trading col-sm-6" >
        <div class="row col-sm-offset-1">
            <div class="row col-sm-offset-1">
                <p class="helvetica-bold-head"><?php echo $section[2]['heading']; ?></p>
                <p class="helvetica-bold-dark"><?php echo $section[2]['subheading']; ?></p>
            </div>
        </div>
        <div class="row ">
            <div class="col-sm-8 col-sm-offset-1">
                <?php echo $section[2]['text']; ?>
            </div>
           
        </div>
    </section>
</div>
    

    <section>
        <div class="row col-sm-offset-1 get-started">
            <div class="col-sm-10">
                <p class="roboto-dark"><?php echo $section[3]['heading']; ?></p>
                <?php echo $section[3]['text']; ?>
            </div>
            <div class="col-sm-2">
                <button type="button" class="btn btn-primary trade-now-btn" data-translate="homepageBeginnerOpenAccoutLabel">Open account</button>
            </div>
        </div>
    </section>
</div><!-- #content -->
<?php
get_footer();
?>