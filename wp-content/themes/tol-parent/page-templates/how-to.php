<?php
/**
 *
 * Template Name: How To Page
 *
 */

get_header(); ?>

<div id="content" role="main">

    <?php load_template(ABSPATH.'wp-content/themes/tol-parent/layouts/' . HOWTO_LAYOUT . '.php' ); ?>
	
</div>

<?php
    get_footer();
?>