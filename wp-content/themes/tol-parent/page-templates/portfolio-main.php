<?php
/**
 * Created by PhpStorm.
 * User: OBenov
 * Date: 10-Jul-17
 * Time: 9:10 AM
 * Template Name: Portfolio Home
 */

get_header(); ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('.userBarElem.currgame').text('Portfolio');
        $('.tol-forex-pro').show();
        $('.tol-portfolio').hide();

        $('.get-portfolio-btn').click(function () {
            window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'get-portfolio';
            return;
        });

        $('.tradefx-btn').click(function () {
            window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'traderoom';
            return;
        });

        $('.tradeeasy-btn').click(function () {
            window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'traderoom?config=simplex';
            return;
        });
    });
</script>
<div class="container-fluid">
    <div class="row portfolio-home-head">
        <div class="col-sm-6 portfolio-home-head-info">
            <div class="portfolio-home-title" data-translate="portfolio-home-title1"></div>
            <div class="portfolio-home-subtitle" data-translate="portfolio-home-subtitle1"></div>
            <div class="portfolio-home-btn get-portfolio-btn" data-translate="portfolio-get-portfolio"></div>
        </div>
        <div class="col-sm-6">
            <img class="img img-responsive" src="/wp-content/themes/tol-parent/images/home_laptop.png" />
        </div>
    </div>
</div>
<div id="content" role="main">
    <div id="tol-portfolio-home-1">
        <div class="container-fluid">
            <div class="row portfolio-home-body">
                <div class="row portfolio-home-forex">
                    <div class="col-sm-7">
                        <img class="img img-responsive" src="/wp-content/themes/tol-parent/images/forex_screen.png" />
                    </div>
                    <div class="col-sm-5 portfolio-home-forex-info">
                        <div class="portfolio-home-title" data-translate="portfolio-home-title2"></div>
                        <div class="portfolio-home-subtitle" data-translate="portfolio-home-subtitle2"></div>
                        <div class="portfolio-home-btn tradefx-btn" data-translate="MainMenu_HawToTrade"></div>
                    </div>
                </div>
                <div class="row portfolio-home-easyforex">
                    <div class="col-sm-5 portfolio-home-easy-info">
                        <div class="portfolio-home-title" data-translate="portfolio-home-title3"></div>
                        <div class="portfolio-home-subtitle" data-translate="portfolio-home-subtitle3"></div>
                        <div class="portfolio-home-btn tradeeasy-btn" data-translate="MainMenu_HawToTrade"></div>
                    </div>
                    <div class="col-sm-7">
                        <img class="img img-responsive" src="/wp-content/themes/tol-parent/images/easyforex_screen.png" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    get_footer();
?>
