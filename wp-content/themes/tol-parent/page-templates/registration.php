<?php
/**
 *
 * Template Name: Registration
 *
 */

get_header(); ?>

<div id="content" role="main">

    <?php load_template(ABSPATH.'wp-content/themes/tol-parent/layouts/' . REGISTRATION_LAYOUT . '.php' ); ?>
	
</div>

<?php
    get_footer();
?>