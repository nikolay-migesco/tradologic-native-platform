<?php
/**
 * Template Name: home page 
 */
 ?>
<div class="binary-options text-center dis_block clearfix2 padT45 padB75">
  <div class="container">
    <h1><?php echo get_field('section_first_heading'); ?></h1>
	<?php  $field_repeter = get_field('section_first'); 
			/* echo "<pre>";
			print_r($field_repeter);
			die(); */
	?>
	
    <div class="row">
      <div class="col-sm-4"><a href="<?php echo $field_repeter[0]['link_url']; ?>"><i><img src="<?php echo $field_repeter[0]['icon_image']; ?>" alt=""></i> <span><?php echo $field_repeter[0]['heading']; ?></span>
        <p><?php echo $field_repeter[0]['content']; ?></p></a>
      </div>
      <div class="col-sm-4"> <a href="<?php echo $field_repeter[1]['link_url']; ?>"><i><img src="<?php echo $field_repeter[1]['icon_image']; ?>" alt=""></i> <span><?php echo $field_repeter[1]['heading']; ?></span>
        <p><?php echo $field_repeter[1]['content']; ?></p></a>
      </div>
      <div class="col-sm-4"> <a href="<?php echo $field_repeter[2]['link_url']; ?>"><i><img src="<?php echo $field_repeter[2]['icon_image']; ?>" alt=""></i> <span><?php echo $field_repeter[2]['heading']; ?></span>
        <p><?php echo $field_repeter[2]['content']; ?></p></a>
      </div>
    </div>
  </div>
</div>
<div class="graybg white-txt dis_block clearfix2 padT35 padB50">
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
        <div class=" padL1 padT1 full line1"> <?php echo get_field('section_2'); ?>        </div>
      </div>
      <div class="col-sm-6 "> <i class="text-center full"><img src="<?php echo get_field('section_2_image'); ?>" alt=""></i> </div>
    </div>
  </div>
</div>
<div class="trader dis_block clearfix2 padT115 padB90">
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
        <div class="padL1 dis_block clearfix2 "> <?php echo get_field('section_3'); ?> 
          <div class="clearfix2"><a href="#" class="black-bt">Register</a></div>
        </div>
      </div>
      <div class="col-sm-6"> <i><img src="<?php echo get_field('section_3_image'); ?>" alt=""></i> </div>
    </div>
  </div>
</div>


<?php get_footer(); ?>
