<?php
/**
 *
 * Template Name: My Profile
 *
 */

get_header(); ?>

<div id="content" role="main">

    <?php 
    
    // if this is a demo user (aka has parent) then redirect back to traderoom
    $userId = json_decode(stripslashes($_COOKIE['widgetUserData']), true)["id"];
    $parentId = json_decode(stripslashes($_COOKIE['widgetUserData']), true)["ParentUserId"];
    $hasParent = ( isset(json_decode(stripslashes($_COOKIE['widgetUserData']), true)["ParentUserId"]) ? $userId != $parentId : false );
    if ($hasParent) {
        $url         =  "//{$_SERVER['HTTP_HOST']}";
        $escaped_url = htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' );
        header("Location: " . $escaped_url . "/traderoom");
    }
    
    load_template(ABSPATH.'wp-content/themes/tol-parent/layouts/' . MYPROFILE_LAYOUT . '.php' ); 
        
    ?>
	
</div>

<?php
    get_footer();
?>