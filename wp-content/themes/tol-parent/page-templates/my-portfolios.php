<?php
   /**
    * Created by PhpStorm.
    * User: OBenov
    * Date: 10-Jul-17
    * Time: 9:10 AM
    * Template Name: My Portfolios
    */
   
get_header();
?>

<script type="text/javascript">
    $(window).on('loadingIsCompleted', function() {
		setTimeout(function() {
			if($('[data-type="noPortfoliosMessage"]').is(':visible')) {
				$('.closed-portfolios-head-btns').hide();
			}
		}, 200);
		
		$(window).on(widgets.events.portfolioClosed, function () {
			setTimeout(function() {
				if($('[data-type="noPortfoliosMessage"]').is(':visible')) {
					$('.closed-portfolios-head-btns').hide();
				}
			}, 200);	
        });		
	});
   $(document).ready(function () {
       $('.userBarElem.currgame').text('Portfolio');
       $('.tol-forex-pro').show();
       $('.tol-portfolio').hide();
	   if ($(document).width() > 1024) {
		$('#tol-portfolios-1').css('height', $(document).height() - 120);
	   }

        $(window).on('portfolioDetailsOpen', function (event, portfolioId) {
           window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'portfolio-details/?portfolio=' + portfolioId;
        });
		
		$('.button.get-portfolio').click(function () {
			window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'get-portfolio';
			return;
		});

       $('.button.create-portfolio').click(function () {
           window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'portfolio-builder';
           return;
       });

        if ($(document).width() > 768) {
            $('.myportfolios-tables-content .scrollable-area').css('height', ($(window.top).height() - 437));
        };
   
       $(window).resize(function(){
           $('.myportfolios-tables-content .scrollable-area').css('height', ($(window.top).height() - 437));

       });

        setTimeout(function() {
            $('.active-portfolios-content .scrollable-area').css('height', $(window.top).height() - ($(document).width() > 768 ? 437 : $(document).width() > 320 ? 200: 100));
            $(".active-portfolios-content .scrollable-area-content").mCustomScrollbar("destroy");
            $(".active-portfolios-content .scrollable-area-content").mCustomScrollbar({
                scrollButtons: {
                    enable: false
                },
                horizontalScroll: false,
                autoDraggerLength: true,
                contentTouchScroll: true,
                scrollInertia: 600,
                autoHideScrollbar: true,
                advanced: {
                    autoScrollOnFocus: false,
                    updateOnContentResize: true,
                    updateOnBrowserResize: true
                }
            });
            $('.closed-portfolios-content .scrollable-area').css('height', $(window.top).height() - ($(document).width() > 768 ? 437 : $(document).width() > 320 ? 200 : 100) );
            $(".closed-portfolios-content .scrollable-area-content").mCustomScrollbar("destroy");
            $(".closed-portfolios-content .scrollable-area-content").mCustomScrollbar({
                scrollButtons: {
                    enable: false
                },
                horizontalScroll: false,
                autoDraggerLength: true,
                contentTouchScroll: true,
                scrollInertia: 600,
                autoHideScrollbar: true,
                advanced: {
                    autoScrollOnFocus: false,
                    updateOnContentResize: true,
                    updateOnBrowserResize: true
                }
            });
        }, 300);
   });	
</script>
<div id="content" role="main">
   <div id="tol-portfolios-1">
      <div class="container-fluid no-padding-all">
         <div class="portfolio-header-part">
            <h1 data-translate="myportfolios-title"></h1>
             <div class="closed-portfolios-head-btns">
                 <div type="button" class="nav-button button nav-game create-portfolio" data-translate="create-new"></div>
                 <div type="button" class="nav-button button nav-game get-portfolio" data-translate="get-new"></div>
             </div>
         </div>
         <div class="myportfolios-table-navigation">
            <ul class="nav">
               <li class="open-portfolios active">
                  <a href="#tab1" data-toggle="tab" class="active">Active Portfolios</a>
               </li>
               <li class="closed-portfolios"  data-type="tab" data-widget="closedPortfolios" >
                  <a href="#tab2" data-toggle="tab" >Closed Portfolios</a>
               </li>
            </ul>
         </div>
         <div class="myportfolios-tables-content">
            <div class="tab-content">
			   <div class="tab-pane fade active in" id="tab1" data-widget="activePortfolios">
                   <div data-type="noPortfoliosMessage" class="noPortfoliosMessage" style="display: none">
                       <div data-translate="no-active-portfolios" ></div>
                       <div data-type="noPortfoliosMessage" data-widget="activePortfolios">
                           <div type="button" class="nav-button button nav-game create-portfolio" data-translate="create-portfolio"></div>
                           <div type="button" class="nav-button button nav-game get-portfolio" data-translate="get-portfolio"></div>
                       </div>
                   </div>
                  <div class="active-portfolios-content" data-type="activePortfoliosContent" >
                     <div class="dataTable" >
                        <table class="fake-table">
                           <thead>
                              <tr>
                                 <th data-column="name" data-sort="string" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" data-translate="active-portfolio-name">NAME</span></div>
                                 </th>
                                 <th data-column="openDate" data-sort="string" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" data-translate="active-portfolio-opendate">OPEN DATE</span></div>
                                 </th>
                                 <th data-column="investedAmount" data-sort="string" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" data-translate="investment">INVESTMENT</span></div>
                                 </th>
                                 <th data-column="totalFees" data-sort="string" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" data-translate="active-portfolio-fees">FEES</span></div>
                                 </th>
                                 <th data-column="profit" data-sort="string" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" data-translate="profit">PROFIT</span></div>
                                 </th>
                                 <th data-column="value" data-sort="string" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" data-translate="active-portfolio-value">VALUE</span></div>
                                 </th>
                                 <th data-column="return" data-sort="string" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" data-translate="returnLabelTradebox">RETURN</span></div>
                                 </th>
                                 <th data-column="actions" data-sort="string" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" span></div>
                                 </th>
                              </tr>
                           </thead>
                        </table>
                     </div>
					
                    <div class="scrollable-area scrollable-area-at-top">
                        <div class="scrollable-area-body">
                           <div class="scrollable-area-content">
                              <div class="dataTable">
                                 <div class="activePortfoliosTable">
                                    <table class="table table-responsive" data-type="table" data-widget="activePortfolios">
                                       <thead data-type="header">
                                          <tr>
                                             <th data-column="name" data-sort="string" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="active-portfolio-name">NAME</span></div>
                                             </th>
                                             <th data-column="openDate" data-sort="string" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="active-portfolio-opendate">OPEN DATE</span></div>
                                             </th>
                                             <th data-column="investedAmount" data-sort="string" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="investment">INVESTMENT</span></div>
                                             </th>
                                             <th data-column="totalFees" data-sort="string" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="active-portfolio-fees">FEES</span></div>
                                             </th>
                                             <th data-column="profit" data-sort="string" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="profit">PROFIT</span></div>
                                             </th>
                                             <th data-column="value" data-sort="string" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="active-portfolio-value">VALUE</span></div>
                                             </th>
                                             <th data-column="return" data-sort="string" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="returnLabelTradebox">RETURN</span></div>
                                             </th>
                                             <th data-column="actions" data-sort="string" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" span></div>
                                             </th>
                                          </tr>
                                       </thead>
                                       <tbody></tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                    </div>
                    </div>
                </div>
               <div class="tab-pane fade" id="tab2" data-widget="closedPortfolios">
                   <div data-type="noPortfoliosMessage"  class="noPortfoliosMessage" style="display: none">
                       <div data-translate="no-closed-portfolios" ></div>
                       <div data-widget="activePortfolios">
                           <div type="button" class="nav-button button nav-game create-portfolio" data-translate="create-portfolio"></div>
                           <div type="button" class="nav-button button nav-game get-portfolio" data-translate="get-portfolio"></div>
                       </div>
                   </div>
                  <div class="closed-portfolios-content" data-type="closedPortfoliosContent">
                     <div class="dataTable">
                        <table class="fake-table">
                           <thead>
                              <tr>
                                 <th data-column="name" data-sort="string" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" data-translate="active-portfolio-name">NAME</span></div>
                                 </th>
                                 <th data-column="openDate" data-sort="string" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" data-translate="active-portfolio-opendate">OPEN DATE</span></div>
                                 </th>
                                 <th data-column="investedAmount" data-sort="string" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" data-translate="investment">INVESTMENT</span></div>
                                 </th>
                                 <th data-column="closeDate" data-sort="string" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" data-translate="closeDate">CLOSE DATE</span></div>
                                 </th>
                                 <th data-column="totalFees" data-sort="string" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" data-translate="active-portfolio-fees">FEES</span></div>
                                 </th>
                                 <th data-column="profit" data-sort="string" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" data-translate="profit">PROFIT</span></div>
                                 </th>
                                 <th data-column="value" data-sort="string" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" data-translate="active-portfolio-value">VALUE</span></div>
                                 </th>
                                 <th data-column="return" data-sort="string" class="name canSort canResize canDrag">
                                    <div class="cellWrapper"><span class="content" data-translate="returnLabelTradebox">RETURN</span></div>
                                 </th>
                              </tr>
                           </thead>
                        </table>
                     </div>
                     <div class="scrollable-area scrollable-area-at-top">
                        <div class="scrollable-area-body">
                           <div class="scrollable-area-content">
                              <div class="dataTable">
                                 <div class="closedPortfoliosTable">
                                    <table class="table table-responsive closedPortfoliosTable" data-type="table" data-widget="closedPortfolios">
                                       <thead data-type="header">
                                          <tr>
                                             <th data-column="name" data-sort="string" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="active-portfolio-name">NAME</span></div>
                                             </th>
                                             <th data-column="openDate" data-sort="string" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="active-portfolio-opendate">OPEN DATE</span></div>
                                             </th>
                                             <th data-column="investedAmount" data-sort="string" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="investment">INVESTMENT</span></div>
                                             </th>
                                             <th data-column="closeDate" data-sort="string" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="closeDate">CLOSE DATE</span></div>
                                             </th>
                                             <th data-column="totalFees" data-sort="string" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="active-portfolio-fees">FEES</span></div>
                                             </th>
                                             <th data-column="profit" data-sort="string" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="profit">PROFIT</span></div>
                                             </th>
                                             <th data-column="value" data-sort="string" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="active-portfolio-value">VALUE</span></div>
                                             </th>
                                             <th data-column="return" data-sort="string" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="returnLabelTradebox">RETURN</span></div>
                                             </th>
                                          </tr>
                                       </thead>
                                       <tbody></tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php
   get_footer();
   ?>
<div class="ui-widget-overlay"></div>