<?php
/**
 * Template Name: banking page 
 */

get_header(); ?>
<div class="whySTXoption">

<div class="full text-left paddingTopBottom_60">
<div class="container">

      <div class="text-center marginBottm_40"><h1><?php the_title(); ?></h1></div>
	  
		<?php 
		while ( have_posts() ) : the_post();
		the_content(); 
		endwhile;
		?>
         </div>
    </div>





<div class="optionRow greybg">

<h4 class="maginBottom_20 heading_text"><strong>Deposit Methods</strong></h4> 
	
<?php $sections = get_field('sections'); 
foreach($sections as $section ){ ?>

<div class=" full marginBottm_50">
<div class="container">
<div class="col-sm-3"> <i class="text-center full"><img src="<?php echo $section['image']; ?>" alt=""></i> </div>

<div class="col-sm-9">

<div class="optionTexBox">

<h4><strong><?php echo $section['heading']; ?></strong></h4>
<p>
<?php echo $section['text']; ?></p>

</div>
</div>

</div>
</div>
<?php } ?>


</div>
</div>

<?php get_footer(); ?>
