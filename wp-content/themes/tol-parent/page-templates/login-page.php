<?php
/**
 *
 * Template Name: Login page
 *
 */

get_header(); ?>

<div id="content" role="main">

    <?php load_template(ABSPATH.'wp-content/themes/tol-parent/layouts/login.php' ); ?>

</div>

<?php
    get_footer();
?>