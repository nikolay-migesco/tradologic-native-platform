<?php
/**
 * Created by PhpStorm.
 * User: Nikolay Ivanov
 * Date: 14-Jul-17
 * Time: 9:10 AM
 * Template Name: Portfolio Adviser
 */

get_header();
wp_enqueue_style('w3', "/wp-content/themes/tol-parent/styles/w3.css", array(), md5_file(get_template_directory() . "/styles/w3.css"), false);
?>
<script type="text/javascript">
    $(document).ready(function(){
        $('.userBarElem.currgame').text('Portfolio');
        $('.tol-forex-pro').show();
        $('.tol-portfolio, .portfolio-adviser-header').hide();

        $('#tol-portfolio-adviser-1').css('height', $(document).height() - 120);

        if (localStorage.getItem('risk') != null) {
            $('.portfolio-adviser-header' + localStorage.getItem('risk')).show();
        } else {
            $('.portfolio-adviser-header1').show();
        };

        $('.portfolio-investment-investbtn').click(function() {
            if (!widgets.isLogged()) {
                window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'registration';
                return;
            };
        });
    });
</script>
<div id="content" role="main">
    <div id="tol-portfolio-adviser-1">
        <div class="layout" id="layout">
            <div class="traderoom-loader" style="display: none;">
                <div class="loader">
                    <span class="loader-block"></span>
                    <span class="loader-block"></span>
                    <span class="loader-block"></span>
                    <span class="loader-block"></span>
                    <span class="loader-block"></span>
                    <span class="loader-block"></span>
                    <span class="loader-block"></span>
                    <span class="loader-block"></span>
                    <span class="loader-block"></span>
                </div>
            </div>
            <div class="portfolio-header-part">
                <h1 data-translate="portfolio-adviser">Your Investment Plan</h1>
                <h3 data-translate="portfolio-adviser-header" class="portfolio-adviser-header portfolio-adviser-header1"></h3>
                <h3 data-translate="portfolio-adviser-header2" class="portfolio-adviser-header portfolio-adviser-header2"></h3>
                <h3 data-translate="portfolio-adviser-header3" class="portfolio-adviser-header portfolio-adviser-header3"></h3>
            </div>
            <div class="portfolio-investment-content">
                <ul class="nav nav-tabs navigation" role="tablist">
                    <li role="presentation" class="active"><a href="#overview" aria-controls="overview" role="tab" data-toggle="tab">OVERVIEW</a></li>
                    <li role="presentation"><a href="#pastPerformance" aria-controls="pastPerformance" role="tab" data-toggle="tab">PAST PERFORMANCE</a></li>
                    <li role="presentation"><a href="#predictedPerformance" aria-controls="predictedPerformance" role="tab" data-toggle="tab">PREDICTED PERFORMANCE</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="row portfolio-investment-tabcontent" data-widget="portfolioAdviser">
                    <div class="col-md-3 portfolio-investment-tabcontent-left">
                        <div class="portfolio-name">
                            <span data-widget="portfolioAdviser" data-type="name">My Portfolio</span>
                            <input data-widget="portfolioAdviser" data-type="input-name" type="text" class="input-nickname" value="" style="display:none;" />
                        </div>
                        <div class="portfolio-investment-tabcontent-left-controls">
                            <span class="init-left">Initial investment</span>
                            <input class="init-right" data-widget="portfolioAdviser" data-type="investmentInput" type="number"/>
                            <input class="init-right" data-widget="portfolioAdviser" data-type="investmentCurrency" style="width: 10px" disabled/>
                            <div class="portfolio-range-holder investment">
                                <div class="portfolio-minus" data-widget="portfolioAdviser" data-type="rangeMinus"></div>
                                <input class="portfolio-investment-range" type="range" data-widget="portfolioAdviser" min="1" max="10000" data-type="investmentRange" value="1000">
                                <div class="portfolio-plus" data-widget="portfolioAdviser" data-type="rangePlus"></div>
                            </div>
                        </div>
                        <div class="portfolio-investment-tabcontent-left-controls">
                            <span class="init-left">Investment period</span>
                            <input class="init-right" data-widget="portfolioAdviser" data-type="periodInput" type="number"/>
                            <div class="portfolio-range-holder period">
                                <div class="portfolio-minus" data-widget="portfolioAdviser" data-type="rangeMinus"></div>
                                <input class="portfolio-investment-range" type="range" data-widget="portfolioAdviser" min="1" max="10" data-type="periodRange" value="5">
                                <div class="portfolio-plus" data-widget="portfolioAdviser" data-type="rangePlus"></div>
                            </div>
                        </div>
                        <div class="portfolio-investment-tabcontent-left-controls">
                            <span class="init-left">Risk level</span>
                            <ul class="nav nav-tabs risk-tabs" id="ul-risk" role="tablist" data-widget="portfolioAdviser" data-type="riskOptions">
                                <li role="presentation" value="1"><a href="#" role="tab" data-toggle="tab">Low</a></li>
                                <li role="presentation" value="2"><a href="#" role="tab" data-toggle="tab">Medium</a></li>
                                <li role="presentation" value="3"><a href="#" role="tab" data-toggle="tab">High</a></li>
                            </ul>
                        </div>
                        <div class="portfolio-investment-tabcontent-left-controls">
                            <div data-widget="portfolioAdviser" data-type="investBtn" class="nav-button button portfolio-investment-investbtn" data-translate="InvestBtn">INVEST</div>
                        </div>
                    </div>
                    <div class="col-md-9 portfolio-investment-tabcontent-right">
                        <div class="tab-content" data-widget="portfolioAdviser" data-type="portfolioTabContent">
                            <div role="tabpanel" class="tab-pane active portfolio-investment-overview" id="overview">
                                <div class="row">
                                    <div class="col-md-8" data-widget="portfolioAdviser" data-type="strategiesOverview"></div>
                                    <div class="col-md-4 portfolio-investment-donut" data-widget="portfolioAdviser" data-type="strategiesChart">
                                        <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="pastPerformance">
                                <div class="row">
                                    <div class="col-md-8 portfolio-investment-pastp-chart">
                                        <div id="pastPerformanceChart"></div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="portfolio-investment-pastp-right rec">
                                            <span class="value" data-widget="portfolioAdviser" data-type="pastPerformanceTotal"></span>
                                            <span class="title">Recommended portfolio</span>
                                        </div>
                                        <div class="portfolio-investment-pastp-right glob">
                                            <span class="value" data-widget="portfolioAdviser" data-type="globalIndexTotal"></span>
                                            <span class="title">Global index</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="predictedPerformance">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div id="predictedPerformanceChart"></div>
                                        <div id="predictedPerformanceLabels" data-widget="portfolioAdviser" data-type="predictedPerformanceLabels"></div>
                                    </div>
                                    <div class="col-md-4 portfolio-investment-predictedp-stats">
                                        <div class="row">
                                            <span class="title">INVESTMENT</span>
                                            <span class="value" data-widget="portfolioAdviser" data-type="summaryInvestment"></span>
                                        </div>
                                        <div class="row">
                                            <span class="title">PREDICTED VALUE</span>
                                            <span class="value" data-widget="portfolioAdviser" data-type="summaryValue"></span>
                                        </div>
                                        <div class="row">
                                            <span class="title">PROFIT</span>
                                            <span class="value blue" data-widget="portfolioAdviser" data-type="summaryProfit"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ui-widget-overlay"></div>
</div>

<div class="modal fade portfolioModal" id="portfolio-modal" data-widget="portfolioAdviser" data-type="portfolioModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel" data-widget="portfolioAdviser" data-type="portfolioModalTitle">Modal title</h4>
            </div>
            <div class="modal-body" data-widget="portfolioAdviser" data-type="portfolioModalContent">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary portfolioActionButton" data-widget="portfolioAdviser" data-type="portfolioModalButton">Save changes</button>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();
?>