<?php
/**
 *
 * Template Name: Asset List
 *
 */

get_header(); ?>

<div id="content" role="main">
    <div id="tol-assetList-layout-1">
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="hidden-xs">
                <div class="container">
                    <div class="pageHeadTitle">
                        <?php require_once(ABSPATH.'wp-content/themes/tol-parent/layouts/breadcrumb.php' ); ?>
                    </div>
                    <header class="entry-header">
                        <h1 class="entry-title"><?php the_title(); ?></h1>
                    </header>
                    <div class="row">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <?php echo do_shortcode('[assetIndexPerGameGamesMenu]'); ?>
                    <?php echo do_shortcode('[assetIndexPerGameAssetsContainer]'); ?>
                    <?php echo do_shortcode('[assetIndexPerGamePreLoader]'); ?>
                </div>
            </div>
        </article>
    </div>
</div>

<?php
    get_footer();
?>