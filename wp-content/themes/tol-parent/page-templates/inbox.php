<?php
/**
 *
 * Template Name: Inbox
 *
 */

get_header(); ?>

<div id="content" role="main">

    <?php load_template(ABSPATH.'wp-content/themes/tol-parent/layouts/inbox-1.php' ); ?>
	
</div>

<?php
    get_footer();
?>