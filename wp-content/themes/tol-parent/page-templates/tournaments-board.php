<?php
/**
 *
 * Template Name: Tournaments Board
 *
 */

get_header(); ?>

<div id="content" role="main">
    
    <?php load_template(ABSPATH.'wp-content/themes/tol-parent/layouts/'.TOURNAMENTS_TAB_LAYOUT.'.php' ); ?>

</div>

<?php
    get_footer();
?>