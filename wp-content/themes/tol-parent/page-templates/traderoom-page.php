<?php
/**
 *
 * Template Name: Traderoom page
 *
 */

get_header();

if (TRADEROOM_LAYOUT == 'traderoom-25m') { ?>
    <div id="tol-trade-popup" data-widget="easyForex-easyForex" data-type="popupcontainer" data-taid="" data-keyboard="false" data-backdrop="static" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="tol-trade-label">
        <div class="modal-dialog modal-sm modal-default" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-widget="easyForex-easyForex" data-type="closepopup">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body widgetPlaceholder">
                    <div class="front">
                        <div data-widget="easyForex-easyForex" data-type="topinfo" class="left-part">
                            <div class="flag-container">
                                <div class="left-flag"></div>
                                <div class="right-flag"></div>
                            </div>
                            <div class="tradearea-box-title">EUR/USD</div>
                        </div>
                        <div class="right-part">
                            <div class="tradearea-box-price">
                                <span class="tradearea-box-price-title">Price</span>
                                <span data-type="averageprice" data-direction="down" class="tradearea-box-price-value down">12802.438</span>
                            </div>
                            <div class="tradearea-box-change">
                                <span class="tradearea-box-change-title">Daily Change</span>
                                <span data-type="dailychange" class="tradearea-box-change-value">0.09</span>
                            </div>
                        </div>
                    </div>
                    <div class="mobile-switch" style="display: none;">
                        <div class="inv-switch mobile-switch-btn active">Investment</div>
                        <div class="chart-switch mobile-switch-btn">Chart</div>
                    </div>
                    <div class="header-text" data-widget="easyForex-easyForex" data-type="compare-text">
                        Will the price of the <span>Euro</span> go up or down against the <span>US Dollar</span>?
                    </div>
                    <div class="btn-wrapper">
                        <div data-widget="easyForex-easyForex" data-type="upbtn" data-direction="up" class="upbtn">UP</div>
                        <div data-widget="easyForex-easyForex" data-type="downbtn" data-direction="down" class="downbtn">DOWN</div>
                    </div>
                    <div data-widget="easyForex-easyForex" data-type="popupcontrollerscontainer" class="controllers-wrapper inactive">
                        <div class="cover-controllers"></div>
                        <div class="ddl-container">
                            <label>Investment amount</label>
                            <div data-widget="easyForex-easyForex" data-type="investment-element" type="button" class="nav-button button ddl-ctrl" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span data-widget="easyForex-easyForex" data-type="investment-selected"></span>
                                <span class="dropdown-arrow"></span>
                            </div>
                            <input type="number" onkeypress="return event.charCode >= 48 && event.charCode <= 57" data-widget="easyForex-easyForex" data-type="investmentInput" class="ddl-ctrl" style="display: none;" />
                            <ul data-widget="easyForex-easyForex" data-type="investment-list" class="dropdown-menu"></ul>
                        </div>
                        <label>Set risk sensitivity <a href="#" class="info-icon" data-toggle="tooltip" data-placement="top" title="Risk Sensitivity, also known as leverage, enables you to significantly increase profit potential. It can also result in greater loss."></a></label>
                        <div class="risk-sens-btns">
                            <div data-widget="easyForex-easyForex" data-type="riskBtn" data-risk="0" class="lowbtn">Low</div>
                            <div data-widget="easyForex-easyForex" data-type="riskBtn" data-risk="1" class="medbtn">Medium</div>
                            <div data-widget="easyForex-easyForex" data-type="riskBtn" data-risk="2" class="highbtn">High</div>
                        </div>

                        <div class="ddl-container">
                            <label>Take profit <a href="#" class="info-icon" data-toggle="tooltip" data-placement="top" title="The system will automatically close your trade if it makes the profit you set. This protects you from any loss which may incur after this profit is met."></a></label>
                            <div data-widget="easyForex-easyForex" data-type="takeprofit-element" type="button" class="nav-button button ddl-ctrl" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span data-widget="easyForex-easyForex" data-type="takeprofit-selected"></span>
                                <span class="dropdown-arrow"></span>
                            </div>
                            <input type="number" onkeypress="return event.charCode >= 48 && event.charCode <= 57" data-widget="easyForex-easyForex" data-type="takeProfitInput" class="ddl-ctrl" style="display: none;" />
                            <ul data-widget="easyForex-easyForex" data-type="takeprofit-list" class="dropdown-menu"></ul>
                        </div>
                        <div class="ddl-container">
                            <label>Stop loss <a href="#" class="info-icon" data-toggle="tooltip" data-placement="top" title="The system will automatically close your trade at the level of loss you set. This protects you from further loss."></a></label>
                            <div data-widget="easyForex-easyForex" data-type="stoploss-element" type="button" class="nav-button button ddl-ctrl" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span data-widget="easyForex-easyForex" data-type="stoploss-selected"></span>
                                <span class="dropdown-arrow"></span>
                            </div>
                            <input type="number" onkeypress="return event.charCode >= 48 && event.charCode <= 57" data-widget="easyForex-easyForex" data-type="stopLossInput" class="ddl-ctrl" style="display: none;" />
                            <ul data-widget="easyForex-easyForex" data-type="stoploss-list" class="dropdown-menu"></ul>
                        </div>
                        <div class="footer-info">
                            <div class="info-block">
                                <span>Trade value: <a href="#" style="margin-left: 18px;" class="info-icon" data-toggle="tooltip" data-placement="top" title="Represents the quantity of the trade, i.e. Investment amount*risk sensitivity"></a> <span data-widget="easyForex-easyForex" data-type="trade-value"></span></span>
                            </div>
                            <div class="info-block">
                                <span>Commission: <a href="#" data-widget="easyForex-easyForex" data-type="tooltip-commission" class="info-icon" data-toggle="tooltip" data-placement="top"
                                                     title="{percent} of trade size
Minimum {minAmount}
Maximum {maxAmount}"></a> <span data-widget="easyForex-easyForex" data-type="commission"></span></span>
                            </div>
                            <div class="info-block">
                                <span>
                                    <span data-translate="easy-pointvalue"></span>
                                    <a href="#" style="margin-left: 19px;" data-widget="easyForex-easyForex" data-type="tooltip-pointValue" class="info-icon" data-toggle="tooltip" data-placement="top" title="1 Point = {pointValue}"></a>
                                    <span data-widget="easyForex-easyForex" data-type="pointValue"></span>
                                </span>
                            </div>
                        </div>
                        <div data-widget="easyForex-easyForex" data-type="invest" class="invest-btn">Invest</div>
                    </div>
                </div>
                <div class="modal-footer"></div>
                <div class="toggleChart" data-widget="easyForex-easyForex" data-type="chart-toggle"><i></i>Chart<i></i></div>
            </div>
            <div class="chartwrapper">
                <div id="tv_chart_container"></div>
            </div>
        </div>
    </div>
    <!-- END TRADE POP UP -->
<?php } else if (TRADEROOM_LAYOUT == 'traderoom-28m' || TRADEROOM_LAYOUT == 'traderoom-28il') {
    setcookie('view', 'single', time()+3600*24 );

    $game = "digital";
    $digital_active = 'active';
    $range_active = '';
    $touch_active = '';
    $turbo_active = '';
    $oneTouch_active = '';
    $advanced_active = '';
    $forex_active = '';
    $ticks_active = '';

    switch (@$_GET['game']) {
        case "digital":
            $game = "digital";
            $digital_active = 'active';
            $range_active = '';
            break;
        case "range":
            $game = "range";
            $digital_active = '';
            $turbo_active = '';
            $range_active = 'active';
            break;
        case "touch":
            $game = "touch";
            $digital_active = '';
            $touch_active = 'active';
            $range_active = '';
            break;
        case "oneTouch":
            $game = "oneTouch";
            $digital_active = '';
            $range_active = '';
            $oneTouch_active = 'active';
            break;
        case "turboBinary":
            $game = "turboBinary";
            $digital_active = '';
            $turbo_active = 'active';
            break;
        case "advanced":
            $game = "advanced";
            $digital_active = '';
            $advanced_active = 'active';
            break;
        case "forex":
            $game = "forex";
            $digital_active = '';
            $forex_active = 'active';
            break;
        case "ticks":
            $game = "ticks";
            $digital_active = '';
            $ticks_active = 'active';
            break;
        default:
            $game = "digital";
    };

    setcookie('game', $game);
    setcookie('chartType', 'chartTypeCandle');
    ?>
    <!-- NEW ORDER POP UP -->
    <div id="tol-order-popup" class="modal fade" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="tol-order-label">
        <div class="modal-dialog modal-sm modal-default" role="document">
            <div class="modal-content <?php echo $game; ?>">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="tol-social-label">New Trade</h4>
                    <h4 class="modal-title"><?php  echo do_shortcode('[assetName game="' . $game . '"]'); ?></h4>
                </div>
                <div class="modal-body new-order-fields">
                    <div style="display: none;">
                        <?php echo do_shortcode('[buttonAlphaBeta game="' . $game . '" direction="Alpha" skiptouchrate="skip" translate="call_'.$game.'"]'); ?>
                        <?php if ($game == "range") { ?>
                            <span data-widget="<?php echo $game ?>_tradologic_rateAlpha" class="widgetRateAlpha col-sm-12 col-xs-4"></span>
                        <?php } ?>
                        <div class="tol-rate-label">
                            <?php echo do_shortcode('[rate game="' . $game . '" screen="true" class="form-control input-lg col-sm-12"]'); ?>
                        </div>
                        <?php if ($game == "range") { ?>
                            <span data-widget="range_tradologic_rateBeta" class="widgetRateBeta col-sm-12 col-xs-4"></span>
                        <?php } ?>
                        <?php echo do_shortcode('[buttonAlphaBeta game="' . $game . '" direction="Beta" skiptouchrate="skip" translate="put_'.$game.'"]'); ?>
                    </div>
                    <div class="row" style="margin:0;">
                        <div class="col-xs-12 nopadding new-order-row">
                            <div class="col-xs-5 nopadding">
                                <label data-translate="action"></label>
                            </div>
                            <div class="col-xs-7 text-primary nopadding">
                                <label class="selectionStateLabel"></label>
                            </div>
                        </div>
                        <div class="col-xs-12 nopadding new-order-row">
                            <div class="col-xs-5 nopadding">
                                <label data-translate="<?php echo ($game == 'range' ? 'menu_range' : ($game == 'touch' ? 'our_rate' : 'strike_level')) ?>"></label>
                            </div>
                            <div class="col-xs-7 rate-wrapper nopadding">
                                <?php echo do_shortcode('[rateRisk game="' . $game . '"]'); ?>
                            </div>
                        </div>
                        <?php if ($game == 'touch') { ?>
                            <div class="col-xs-12 nopadding new-order-row">
                                <div class="col-xs-5 nopadding">
                                    <label data-translate="touch_rate"></label>
                                </div>
                                <div class="col-xs-7 rate-wrapper nopadding">
                                    <span data-widget="touch_tradologic_rateAlpha" class="touchrateAlpha"></span>
                                    <span data-widget="touch_tradologic_rateBeta" class="touchrateBeta"></span>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if ($game != 'advanced' && $game != 'digital') { ?>
                            <div class="col-xs-12 nopadding new-order-row">
                                <div class="col-xs-5 nopadding">
                                    <label data-translate="payout"></label>
                                </div>
                                <div class="col-xs-7 rate-wrapper nopadding">
                                    <?php echo do_shortcode('[payout game="' . $game . '"]'); ?>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if ($game != 'advanced') { ?>
                            <div class="col-xs-12 nopadding new-order-row">
                                <div class="col-xs-5 nopadding">
                                    <label data-translate="option_expiry"></label>
                                </div>
                                <div class="col-xs-7 rate-wrapper nopadding">
                                    <?php echo do_shortcode('[expireDisplay game="' . $game . '"]'); ?>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="col-xs-12 nopadding new-order-row select-row first-margin">
                            <label class="ddl-label" data-translate="right-box-amount"></label>
                            <?php echo do_shortcode('[investAmount game="' . $game . '"]'); ?>
                        </div>
                        <?php if ($game == "digital") { ?>
                            <div class="col-xs-12 nopadding new-order-row select-row">
                                <label class="ddl-label" data-translate="payout"></label>
                                <select class="form-control btn-default form-control-sm" data-widget="digital_tradologic_payoutDynamic"></select>
                            </div>
                            <div class="col-xs-12 nopadding new-order-row select-row">
                                <label class="ddl-label" data-translate="contracts"></label>
                                <select  data-widget="digital_tradologic_contracts" class="form-control btn-default form-control-sm tol-widgetContracts"></select>
                            </div>
                        <?php } ?>
                        <?php if ($game == 'advanced') { ?>
                            <div class="col-xs-12 col-sm-12 nopadding new-order-row" style="margin-top: 10px !important;">
                                <div class="col-xs-5 nopadding">
                                    <label data-translate="duration"></label>
                                </div>
                            </div>
                            <div class="col-xs-12 nopadding new-order-row">
                                <div data-widget="advanced_tradologic_advancedExpire" data-settings='{"isSelect": false, "extraClass": "the-class-name"}' class="widgetPlaceholder widgetAdvancedDurationButton" style="display:inline-block;"></div>
                            </div>
                            <div class="col-xs-12 nopadding new-order-row" style="height: 60px;margin-top:10px !important;">
                                <span data-widget="advanced_tradologic_duration" class="spanExpiry" style="display:inline-block;"></span>
                                <div data-widget="advanced_tradologic_advancedLength" class="durationSlider"></div>
                                <input data-widget="advanced_tradologic_myTradesFromDate" class="widgetPlaceholder advancedDurationDatePicker" type="text" onkeypress="return false">
                            </div>

                            <div id="payout-return-holder" class="col-xs-12 durationSlider">
                                <div class="col-xs-6 nopadding">
                                    <span data-translate="returnLabelTradebox" class="payoutDesc"></span>
                                </div>
                                <div class="col-xs-6 nopadding text-right">
                                    <span data-translate="payoutLabel" class="returnDesc"></span>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="returnOnLossProfit"></div>
                                <div data-widget="advanced_tradologic_payoutAdvancedBinary" class="slider-payout"></div>
                                <div class="payoutProfit"></div>
                            </div>
                            <div class="col-xs-12" style="margin-top: 9px;padding: 0;">
                                <span class="returnHighAmount adv-game" data-widget="advanced_tradologic_amountBeta" data-hidesymbol="false" data-defaultvalueload='false' style="display: block;float: left;padding:0px;"></span>
                                <span class="returnLowAmount adv-game" data-widget="advanced_tradologic_amountAlpha" data-hidesymbol="false" data-defaultvalueload='false' style="display: block;float: right;padding:0px;"></span>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <?php if ($game != 'advanced') { ?>
                    <div class="new-order-summary col-sm-12">
                        <div class="col-xs-6 col-sm-6 small-text">
                            <small data-translate="inTheMoneyLabel"></small>
                        </div>
                        <div class="col-xs-6 col-sm-6">
                            <?php echo do_shortcode('[amount  direction="win" game="' . $game . '"]'); ?>
                        </div>
                        <div class="col-xs-6 col-sm-6 small-text">
                            <small data-translate="outTheMoneyLabel"></small>
                        </div>
                        <div class="col-xs-6 col-sm-6">
                            <?php echo do_shortcode('[amount direction="lose" game="' . $game . '"]'); ?>
                        </div>
                    </div>
                <?php } ?>
                <div class="new-order-btn">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php echo do_shortcode('[buttonBuy game="' . $game . '"]'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END NEW ORDER POP UP -->
<?php }

 if (ENABLE_DEMO_FLOW) { ?>
    <!-- MY PROFILE POP UP -->
    <div id="tol-myprofile-popup"  data-keyboard="false" data-backdrop="static" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="tol-social-label">
        <div class="modal-dialog modal-lg modal-default" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="tol-social-label">Personal Details</h4>
                </div>
                <div class="modal-body">
                    <div class="label-field-wrapper text-container birth-container">
                        <label class="label"><span class="required">*</span>Date of birth:</label>
                        <span class="select-container birthDay">
                            <select type="date" data-widget="userDetails" data-type="birthDay" class="widgetPlaceholder" data-isRequired="true" data-regexpMessage="Day Required"></select>
                        </span>
                        <span class="select-container birthMonth">
                            <select type="date" data-widget="userDetails" data-type="birthMonth" class="widgetPlaceholder" data-isRequired="true" data-regexpMessage="Month Required" ></select>
                        </span>
                        <span class="select-container birthYear">
                            <select type="date" data-widget="userDetails" data-type="birthYear" data-regexpMessage="Year Required" class="widgetPlaceholder" data-isRequired="true"></select>
                        </span>
                    </div>
                    <div class="label-field-wrapper text-container phone-container" style="display: none;">
                        <label class="label required">Primary Phone/Mobile:</label>
                        <input type="text" data-widget="userDetails" data-type="countryPhoneCode" class="widgetPlaceholder phoneCountryCode" />
                        <input type="text" data-widget="userDetails" data-type="areaPhoneCode" class="widgetPlaceholder phoneAreaCode" />
                        <input type="text" data-widget="userDetails" data-type="phone" data-regexpmessage="Please enter your Primary Phone" class="widgetPlaceholder phone" />
                    </div>
                    <div class="form-group col-xs-12 col-sm-12" style="display: none;">
                        <label class="col-xs-12 col-sm-12 col-md-4 control-label text-left no-padd"
                               data-translate="Registration_Country"></label>
                        <div class="col-xs-12 col-sm-12 col-md-8">
                            <?php echo do_shortcode('[userDetailsCountryCode class="form-control"]'); ?>
                        </div>
                    </div>
                    <div class="label-field-wrapper text-container ">
                        <label class="label"><span class="required">*</span>Address 1:</label>
                        <input type="text" data-widget="userDetails" data-type="addressLine1"  class="widgetPlaceholder input" data-isRequired="true" data-regexpMessage="Please enter your address"/>
                    </div>
                    <div class="label-field-wrapper text-container ">
                        <label class="label">Address 2:</label>
                        <input type="text" data-widget="userDetails" data-type="addressLine2"  class="input" data-isRequired="false" />
                    </div>
                    <div class="label-field-wrapper text-container city-container">
                        <label class="label"><span class="required">*</span>City:</label>
                        <input type="text" data-widget="userDetails" data-type="city" class="input" data-isRequired="true" data-regexpMessage="Please enter your city" />
                    </div>
                    <div class="label-field-wrapper text-container zip-container">
                        <label class="label"><span class="required">*</span>Zip Code:</label>
                        <input type="text" data-widget="userDetails" data-type="zip" class="input" data-isRequired="true" data-regexpMessage="Please enter your zip code"/>
                    </div>
                    <div class="label-field-wrapper text-container zip-container">
                        <label class="label"><span class="required">*</span>Tax Identification Number (TIN):</label>
                        <input type="text" placeholder="Tax Identification Number (TIN)" data-translate="Registration_TIN" data-widget="userDetails" data-type="IdentificationNumber" data-isrequired="true" data-minlength="6" class="input">
                    </div>
                    <div class="label-field-wrapper text-container zip-container">
                        <label class="label"><span class="required">*</span>Tax jurisdiction:</label>
                        <select data-widget="userDetails" data-type="JurisdictionCountryCode" data-isrequired="true" class="form-control" ></select>
                    </div>
                    <div class="label-field-wrapper text-container zip-container">
                        <label class="label"><span class="required">*</span>ID-Card/Passport number</label>
                        <input type="text" placeholder="ID-Card/Passport number" data-translate="Registration_IDCard" data-widget="userDetails" data-type="IDCard" data-isrequired="true" data-minlength="6" class="input">
                    </div>
                </div>
                <div class="modal-footer">
                    <button data-widget="userDetails"  data-type="submit" type="button" class="btn btn-primary">Next</button>
                </div>
            </div>
        </div>
    </div>
    <!-- MY PROFILE POP UP -->
    <!-- QUESTIONNAIRE POP UP -->
    <div id="tol-questionnaire-popup" data-keyboard="false" data-backdrop="static" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="tol-social-label">
        <div class="modal-dialog modal-lg modal-default" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="tol-social-label">Questionnaire</h4>
                </div>
                <div class="modal-body">

                    <div class="compliance-documents compliance-container row" data-widget="compliance" data-type="section" data-item="questionnaires" >
                        <div class="questionnaireguide" data-translate="questionnareGuide"></div>
                        <div class="questionnaireerror" data-type="questionnaireerror" style="display: none"></div>
                        <div data-widget="compliance" data-type="questions" class="compliance-questions"></div>
                    </div>


                </div>

            </div>
        </div>
    </div>
    <!-- QUESTIONNAIRE POP UP -->
    <!-- UPLOAD DOCUMENTS POP UP -->
    <div id="tol-upload-docs-popup" data-keyboard="false" data-backdrop="static" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="tol-social-label">
        <div class="modal-dialog modal-lg modal-default" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" data-widget="compliancePopup" data-type="title" id="tol-social-label"></h4>
                </div>
                <div class="modal-subheader" data-widget="compliancePopup" data-type="subtitle"></div>
                <div class="modal-body" data-widget="compliancePopup" data-type="upload"></div>
                <div class="modal-footer">
                    <input data-widget="compliancePopup" data-type="fileupload" id="fileUpload" style="display:none;" type="file" required="required" />
                    <button data-widget="compliancePopup" data-type="submit" data-group="" type="button" class="btn btn-primary" disabled>Submit</button>
                    <div data-widget="compliancePopup" data-type="dismiss" class="btn btn-primary" data-dismiss="modal" style="display:none;">OK</div>
                </div>
            </div>
        </div>
    </div>
    <!-- UPLOAD DOCUMENTS POP UP -->
<?php } ?>
<div id="content" role="main">
    <?php load_template(ABSPATH.'wp-content/themes/tol-parent/layouts/'.TRADEROOM_LAYOUT.'.php' ); ?>
</div>

<?php
    get_footer();
?>
