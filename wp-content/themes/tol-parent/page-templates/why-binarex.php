<?php
/**
 *
 * Template Name: Why Binarex page
 *
 */
get_header();
?>


<div id="content" role="main" class="container-fluid why-binarex-layout">
    <?php $section = get_field('sections'); ?>
    <div class="row col-sm-offset-1">
        <div class="col-sm-6">
            <p class="helvetica-bold-head"><?php echo $section[0]['heading']; ?></p>
            <p class="helvetica-bold-dark"><?php echo $section[0]['subheading']; ?></p>
        </div>
        <div class="col-sm-6">
            <span class="breadrumb-line"></span><span class="index-top"><?php echo $section[0]['hint']; ?></span>
        </div>
    </div>

    <div class="row col-sm-offset-1">
        <div class="col-sm-4">
            <p><?php echo $section[0]['text']; ?> </p>
        </div>
        <div class="col-sm-8">
            <div id="videoSection">
                <img src="<?php echo $section[0]['image']; ?>" alt="">
            </div>
        </div>
    </div>
    <div class="row col-sm-offset-1" >
        <div >
            <ul id="subsections-navigation" class=" nav nav-bar">
                <a>
                    <li class="col-sm-4 regulation-tab active" data-translate="regulation-tab-title"></li>
                </a>
                <a>
                    <li  class="col-sm-4 fees-tab" data-translate="fees-tab-title"></li>
                </a>
                <a>
                    <li class="col-sm-4 trading-hours-tab" data-translate="trading-hours-tab-title"></li>
                </a>
            </ul>
        </div>
    </div>

    <section class="regulations-trading" >
        <div class="row col-sm-offset-1">
            <div class="col-sm-6">
                <p class="helvetica-bold-head"><?php echo $section[1]['heading']; ?></p>
            </div>
            <div class="col-sm-6">
                <span class="breadrumb-line"></span><span class="index-top"><?php echo $section[1]['hint']; ?></span>
            </div>
        </div>
        <div class="row ">
            <div class="col-sm-4 col-sm-offset-1">
                <?php echo $section[1]['text']; ?>
            </div>
            <div class="col-sm-6">
                <div class="regulations-trading-rect">
                    <div class="regulations-trading-image">
                        <img src="<?php echo $section[1]['image']; ?>" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="low-fees-trading" style="display: none">
        <div class="row col-sm-offset-1">
            <div class="col-sm-6">
                <span class="index-top"><?php echo $section[3]['hint']; ?></span><span class="breadrumb-line"></span>
            </div>
            <div class="col-sm-6">
                <p class="helvetica-bold-head"><?php echo $section[3]['heading']; ?></p>
                <p class="helvetica-bold-dark"><?php echo $section[3]['subheading']; ?></p>
            </div>
        </div>
        <div class="row ">
            <div class="col-sm-4 col-sm-offset-1">
                <?php echo $section[3]['text']; ?>
            </div>
            <div class="col-sm-6">
                <div class="fees-trading-rect">
                    <p class="helvetica-bold-dark" data-translate="low-fees-trading-table-title"></p>
                    <table class="tailored-table table">
                        <thead>
                            <tr>
                                <th>Contracts Traded Per Order</th>
                                <th>Fees Per Side</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Trade fees</td>
                                <td>$0.90 per contract (capped at $9.00)</td>
                            </tr> 
                            <tr>
                                <td>Deposit fees</td>
                                <td>No additional fees</td>
                            </tr> 
                            <tr>
                                <td>Withdraw fees</td>
                                <td>$0.90 per contract (capped at $9.00)</td>
                            </tr>
                            <tr>
                                <td>Lorem Ipsum</td>
                                <td>No additional fees</td>
                            </tr> 
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <section class="trading-hours-trading" style="display: none">
        <div class="row col-sm-offset-1">
            <div class="col-sm-6">
                <span class="index-top"><?php echo $section[4]['hint']; ?></span><span class="breadrumb-line"></span>
            </div>
            <div class="col-sm-6">
                <p class="helvetica-bold-head"><?php echo $section[4]['heading']; ?></p>
                <p class="helvetica-bold-dark"><?php echo $section[4]['subheading']; ?></p>
            </div>
        </div>
        <div class="row ">
            <div class="col-sm-4 col-sm-offset-1">
                <?php echo $section[4]['text']; ?>
            </div>
            <div class="col-sm-6 trading-hours-table">
                <table class="tailored-table table">
                        <thead>
                            <tr>
                                <th>Holiday</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Friday, January 1</td>
                                <td>New Year's Day</td>
                            </tr> 
                            <tr>
                                <td>Friday, March 25</td>
                                <td>Good Friday</td>
                            </tr> 
                            <tr>
                                <td>Monday, March 28</td>
                                <td>Easter Monday</td>
                            </tr>
                            <tr>
                                <td>Sunday, May 1</td>
                                <td>May Day</td>
                            </tr> 
                            <tr>
                                <td>Sunday, December 25</td>
                                <td>Christmas Day</td>
                            </tr>
                            <tr>
                                <td>Monday, December 26</td>
                                <td>St.Stephen's Day</td>
                            </tr> 
                        </tbody>
                    </table>
            </div>
        </div>
    </section>
    <section>
        <div class="row col-sm-offset-1 get-started">
            <div class="col-sm-10">
                <p class="roboto-dark"><?php echo $section[2]['heading']; ?></p>
                <?php echo $section[2]['text']; ?>
            </div>
            <div class="col-sm-2">
                <button type="button" class="btn btn-primary trade-now-btn" data-translate="homepageBeginnerOpenAccoutLabel">Open account</button>
            </div>
        </div>
    </section>
</div><!-- #content -->
<?php
get_footer();
?>