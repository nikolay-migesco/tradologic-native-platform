<?php
   /**
    *
    * Template Name: FAQ 
    *
    */

   get_header();
?>
   <div id="content" role="main">
      <div class="container-fluid exchange-layout">
         <div class="row faq-section first-article-header">
            <div class="faq second-article-header">

            </div>
         </div>
      </div>
</div>
<style>
.panel-default > .panel-heading.active {
    background-color: #ce0f2c !important;
    border: 1px solid #ce0f2c;
    background: #ce0f2c;
    font-weight: normal;
    color: #ffffff;
}
</style>
<script type="text/javascript">
   function toggleChevron(e) {
       $(e.target)
               .prev('.panel-heading')
               .find("i.indicator")
               .toggleClass('glyphicon-triangle-bottom glyphicon-triangle-right');
   }
   $('#accordion').on('hidden.bs.collapse', toggleChevron);
   $('#accordion').on('shown.bs.collapse', toggleChevron);
   
   
   $('#accordion .panel.panel-default > .panel-heading a').addClass('collapsed');
   $('#accordion .panel.panel-default:first > .panel-heading a').removeClass('collapsed');
   $('#accordion .panel.panel-default:first > .panel-heading').addClass('active');
   $('#accordion .panel.panel-default > .panel-heading').on('click', function (e) {
       $('#accordion .panel.panel-default > .panel-heading').removeClass('active');
       if ($(this).children('a').hasClass('collapsed'))
       {
           $(this).addClass('active');
       }
   });
</script>

<?php get_footer(); ?>