<div class="tol-header-content">
    <?php
    /**
     *
     * Template Name: Content-Pages
     *
     */
    get_header();
    ?>
</div>



<div class="tol-body-content container">
    <?php
    if ( have_posts() ) while ( have_posts() )
        {
            the_post();
            the_content();
        }
    ?>
</div>



<div class="tol-footer-content">
    <?php
    get_footer();
    ?>
</div>
