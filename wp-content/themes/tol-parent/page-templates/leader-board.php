<?php
/**
 *
 * Template Name: Leader Board
 *
 */

get_header();
wp_enqueue_style('leaderboard-layout');
?>

<div id="content" role="main">

    <?php 
        if(!defined("LEADERBOARD_LAYOUT")) define("LEADERBOARD_LAYOUT", "leader-board-1");
        load_template(ABSPATH.'wp-content/themes/tol-parent/layouts/'.LEADERBOARD_LAYOUT.'.php' );
    ?>
	
</div>

<?php
    get_footer();
?>