<?php
/**
 *
 * Template Name: Trader Info
 *
 */

get_header(); 
wp_enqueue_style('leaderboard-layout');
?>

<div id="content" role="main">
    <div id="tol-traderInfo-layout-1">
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="container">
                <header class="entry-header">
                    <h1 class="entry-title"><?php the_title(); ?></h1>
                </header>
                <div class="pageHeadTitle">
                    <?php require_once(ABSPATH.'wp-content/themes/tol-parent/layouts/'.BREADCRUMB.'.php' ); ?>
                </div>
                <div data-widget="socialTraderInfo2" class="social-trader-info" data-translate="traderInfoLoading"></div>
            </div>
            <div class="entry-content">
                <?php echo $post->post_content; ?>
            </div>
        </article>
    </div>
</div>

<?php
    get_footer();
?>
