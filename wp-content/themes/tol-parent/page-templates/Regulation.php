<?php
/**
 * Template Name: regulation page 
 */

get_header(); ?>
<div class="dis_block clearfix2 padT45 padB75 ">
  <div class="container">
    <div class="full text-center">
      <h1><?php the_title(); ?></h1>
      
    </div>
    
<div class="dis_block clearfix2 padT45">
<?php 
		$id = get_the_id();
		$post_object = get_post( $post_id );
			echo $post_object->post_content;
?>

</div>
    
    
 
 <div class="regulation-block">
 <?php $flags = get_field('flag_and_country_detail');	
			foreach($flags as $flag){ ?>
 
 <div class="strip">
 <i><img src="<?php echo $flag['flag']; ?>" alt=""></i>
 <span><?php echo $flag['country_name']; ?></span>
 <p><?php echo $flag['related_detail']; ?></p>
 </div>
 
 
 <?php } ?>
 
 </div>   
    
    
    
    <div class="full padT35"><a href="#" class="yellow-bt">Open An Account</a></div>
    
    
    
    
    
    
    
    
    
  </div>
  
  
  
</div>

<?php get_footer(); ?>
