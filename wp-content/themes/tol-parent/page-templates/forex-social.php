 <!-- SOCIAL PROFILE POP UP -->
 <div id="tol-uncopy-popup" data-widget="forex-forexSocialProfile" data-type="profile-uncopy-popup" data-keyboard="false" data-backdrop="static" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="tol-social-label">
    <div class="modal-dialog modal-lg modal-default" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="switch-btns">
                    <div class="uncopy-header-title">
                        Uncopy from master trader <span data-widget="forex-forexSocialProfile" data-type="nickname" class="blue-text"></span>
                    </div>
                    <button data-widget="forex-forexSocialProfile" data-type="profile-uncopy-close" type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <h4 class="modal-title" id="tol-social-label">Confirm Uncopy</h4>
            </div>
            <div class="modal-body">
                Please note that all positions copied from <span data-widget="forex-forexSocialProfile" data-type="nickname" class="blue-text"></span> will be unlinked. Your positions will NOT close until you close them yourself.
            </div>
            <div class="modal-footer">
                <button data-widget="forex-forexSocialProfile"  data-type="profile-uncopy-close" type="button" class="btn btn-primary uncopyClose" data-dismiss="modal">Cancel</button>
                <button data-widget="forex-forexSocialProfile"  data-type="profile-uncopy-confirm" type="button" class="btn btn-primary">Uncopy</button>
            </div>
        </div>
    </div>
</div>
<div id="tol-social-popup" data-widget="forex-forexSocialProfile" data-type="container" data-keyboard="false" data-backdrop="static" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="tol-social-label">
    <div class="modal-dialog modal-lg modal-default" role="document">
        <div class="modal-content">
            <div class="profile-loader" data-widget="forex-forexSocialProfile" data-type="loader" style="display: none;">
                <div class="loader">
                    <span class="loader-block"></span>
                    <span class="loader-block"></span>
                    <span class="loader-block"></span>
                    <span class="loader-block"></span>
                    <span class="loader-block"></span>
                    <span class="loader-block"></span>
                    <span class="loader-block"></span>
                    <span class="loader-block"></span>
                    <span class="loader-block"></span>
                </div>
            </div>
            <div class="modal-header">
                <div class="switch-btns">
                    <div data-widget="forex-forexSocialProfile" data-type="previous-profile" data-id="" class="social-prev-profile disabled"><</div>
                    <div data-widget="forex-forexSocialProfile" data-type="next-profile" data-id="" class="social-next-profile disabled">></div>
                </div>
                <div data-widget="forex-forexSocialProfile" data-type="nickname-error" class="nickname-error"></div>
                <button data-widget="forex-forexSocialProfile" data-type="close" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 data-widget="forex-forexSocialProfile" data-type="container-title" class="modal-title" id="tol-social-label"></h4>
            </div>
            <div class="modal-body">
                <div class="leader-profile-left">
                    <div class="trader-name">
                        <span data-widget="forex-forexSocialProfile" data-type="nickname"></span>
                        <span data-widget="forex-forexSocialProfile" data-type="edit-nickname" class="edit-nickname"></span>
                        <input data-widget="forex-forexSocialProfile" data-type="input-nickname" type="text" class="input-nickname" value="" style="display:none;" />
                    </div>
                    <span data-widget="forex-forexSocialProfile" data-type="avatar" class="popup-avatar"></span>
                    <div data-widget="forex-forexSocialProfile" data-type="profile-copy-link-button" class="profile-copy-button">Copy</div>
                    <div data-widget="forex-forexSocialProfile" data-type="change-avatar" class="change-avatar-btn">Edit avatar</div>
                    <div class="trader-profile-copiers">
                        <span class="copiers-title">COPIERS</span>
                        <span data-widget="forex-forexSocialProfile" data-type="profile-trader-copiers" class="copiers-value"></span>
                    </div>
                    <div class="trader-profile-about">
                        <div class="trader-about-title">About Me</div>
                        <div data-widget="forex-forexSocialProfile" data-type="aboutme" title="" class="trader-about-content" ></div>
                        <span data-widget="forex-forexSocialProfile" data-type="edit-aboutme" class="edit-aboutme"></span>
                        <textarea data-widget="forex-forexSocialProfile" data-type="input-aboutme" type="text" class="input-aboutme" value="" style="display:none;"></textarea>
                    </div>
                </div>
                <div class="leader-profile-right">
                    <ul class="nav trader-popup-nav">
                        <li class="stats-tab active">
                            <a href="#tab31" data-widget="forex-forexSocialProfile" data-type="stats-tab" data-toggle="tab" class="active" aria-expanded="true"><span>Stats</span></a>
                        </li>
                        <li class="portfolio-tab">
                            <a href="#tab32" data-widget="forex-forexSocialProfile" data-type="portfolio-tab" data-toggle="tab"><span>Portfolio</span></a>
                        </li>
                        <li class="community-tab">
                            <a href="#tab33" data-widget="forex-forexSocialProfile" data-type="community-tab" data-toggle="tab"><span>Community</span></a>
                        </li>
                        <li class="copy-tab">
                            <a href="#tab34" data-widget="forex-forexSocialProfile" data-type="copy-tab" data-toggle="tab"><span>Copy</span></a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane stats-pane fade active in" id="tab31">
                            <div class="stats-wrapper">

                                <div class="stats-header">
                                    <div class="stats-pandl" data-widget="forex-forexSocialProfile" data-type="stats-header"></div>
                                    <div class="stats-time-frame">
                                        <span class="stats-time-title">Time Period</span>
                                        <span data-widget="forex-forexSocialProfile" data-type="stats-timeboxes" data-time="1" class="stats-time-box selected">1m</span>
                                        <span data-widget="forex-forexSocialProfile" data-type="stats-timeboxes" data-time="3" class="stats-time-box">3m</span>
                                        <span data-widget="forex-forexSocialProfile" data-type="stats-timeboxes" data-time="6" class="stats-time-box">6m</span>
                                        <span data-widget="forex-forexSocialProfile" data-type="stats-timeboxes" data-time="12" class="stats-time-box">1y</span>
                                    </div>
                                </div>

                                <div class="stats-body" data-widget="forex-forexSocialProfile" data-type="stats-chart"></div>

                                <div class="stats-footer">
                                    <div class="col-sm-4 stats-footer-btn stats-footer-btn1">
                                        <span class="footer-btn-title">Success Rate</span>
                                        <span data-widget="forex-forexSocialProfile" data-type="stats-footer-srate" class="footer-btn-value">+58.97%</span>
                                    </div>
                                    <div class="col-sm-4 stats-footer-btn stats-footer-btn2">
                                        <span class="footer-btn-title">Best Position</span>
                                        <span data-widget="forex-forexSocialProfile" data-type="stats-footer-best" class="footer-btn-value">+158.97%</span>
                                    </div>
                                    <div class="col-sm-4 stats-footer-btn stats-footer-btn3">
                                        <span class="footer-btn-title">Worst Position</span>
                                        <span data-widget="forex-forexSocialProfile" data-type="stats-footer-worst" class="footer-btn-value">-234.97%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab32">
                            <div class="portfolio-wrapper">
                                <div class="portfolio-header">
                                    <div class="portfolio-time-frame">
                                        <span class="stats-time-title">Time Period</span>
                                        <span data-widget="forex-forexSocialProfile" data-type="portfolio-timeboxes" data-time="1" class="stats-time-box selected">1m</span>
                                        <span data-widget="forex-forexSocialProfile" data-type="portfolio-timeboxes" data-time="3" class="stats-time-box">3m</span>
                                        <span data-widget="forex-forexSocialProfile" data-type="portfolio-timeboxes" data-time="6" class="stats-time-box">6m</span>
                                        <span data-widget="forex-forexSocialProfile" data-type="portfolio-timeboxes" data-time="12" class="stats-time-box">1y</span>
                                    </div>
                                </div>
                                <div class="portfolio-body" data-widget="forex-forexSocialProfile" data-type="portfolio-chart"></div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab33">
                            <div class="community-wrapper">
                                <ul class="nav trader-popup-nav">
                                    <li class="copiers-tab active">
                                        <a href="#tab21" data-toggle="tab" class="active" aria-expanded="true">
                                            <span>COPIERS</span>
                                            <span data-widget="forex-forexSocialProfile" data-type="copiers-count"></span>
                                        </a>
                                    </li>
                                    <li class="copying-tab">
                                        <a href="#tab22" data-toggle="tab">
                                            <span>COPYING</span>
                                            <span data-widget="forex-forexSocialProfile" data-type="copying-count"></span>
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane dataTable fade active in" id="tab21">
                                        <div class="scrollable-area scrollable-area-at-top">
                                            <div class="scrollable-area-body">
                                                <div class="scrollable-area-content">
                                                    <table class="popup-social copiers-copying hasArrows" data-widget="forex-forexSocialProfile" data-type="profile-copiers-table">
                                                        <thead data-type="profile-copiers-table-header">
                                                        <tr>
                                                            <th data-column="avatar" data-sort="string" class="canSort canResize canDrag tarder-avatar">
                                                                <div class="cellWrapper">
                                                                    <span class="content">Name</span>
                                                                </div>
                                                            </th>
                                                            <th data-column="name" data-sort="string" class="canSort canResize canDrag">
                                                                <div class="cellWrapper">
                                                                    <span class="content"></span>
                                                                </div>
                                                            </th>
                                                            <th data-column="plperc" data-sort="int" class="pandlcol canSort canResize canDrag">
                                                                <div class="cellWrapper">
                                                                    <span class="content">P/L(%)</span>
                                                                </div>
                                                            </th>
                                                            <th data-column="action" data-sort="string" class="copycol canSort canResize canDrag">
                                                                <div class="cellWrapper">
                                                                    <span class="content">Copy</span>
                                                                </div>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane dataTable fade" id="tab22">
                                        <div class="scrollable-area scrollable-area-at-top">
                                            <div class="scrollable-area-body">
                                                <div class="scrollable-area-content">
                                                    <table class="popup-social copiers-copying hasArrows" data-widget="forex-forexSocialProfile" data-type="profile-copying-table">
                                                        <thead>
                                                        <tr>
                                                            <th data-column="avatar" data-sort="string" class="canSort canResize canDrag tarder-avatar">
                                                                <div class="cellWrapper">
                                                                    <span class="content">Name</span>
                                                                </div>
                                                            </th>
                                                            <th data-column="name" data-sort="string" class="canSort canResize canDrag">
                                                                <div class="cellWrapper">
                                                                    <span class="content"></span>
                                                                </div>
                                                            </th>
                                                            <th data-column="plperc" data-sort="int" class="pandlcol canSort canResize canDrag">
                                                                <div class="cellWrapper">
                                                                    <span class="content">P/L(%)</span>
                                                                </div>
                                                            </th>
                                                            <th data-column="mypl" data-sort="int" class="mypl canSort canResize canDrag">
                                                                <div class="cellWrapper">
                                                                    <span class="content">My P/L</span>
                                                                </div>
                                                            </th>
                                                            <th data-column="action" data-sort="string" class="copycol canSort canResize canDrag">
                                                                <div class="cellWrapper">
                                                                    <span class="content">Copy</span>
                                                                </div>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane copy-pane fade" id="tab34">
                            <div class="copy-wrapper">
                                <a href="#" class="info-icon" data-toggle="tooltip" data-placement="right" title="The Percentage is the ration between the amount of the trade at the provider versus the amount that will open in your account"></a>
                                <div class="copy-header">
                                    <div class="copy-header-title">
                                        Copy <span data-widget="forex-forexSocialProfile" data-type="nickname" class="blue-text"></span> strategy proportinally on:
                                    </div>
                                    <div class="copy-header-body" data-type="profile-proportion-holder">
                                        <div class="asset-type-wrapper narrow">
                                            <div type="button" class="nav-button button asset-type narrow" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span data-widget="forex-forexSocialProfile" data-type="profile-proportion-selected"></span>
                                                <span class="dropdown-arrow"></span>
                                            </div>
                                            <ul class="dropdown-menu" data-type="profile-proportion-list" data-widget="forex-forexSocialProfile">
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="copy-body">
                                    <div class="copy-body-row">
                                        <span class="text-row">If <span data-widget="forex-forexSocialProfile" data-type="nickname" class="blue-text"></span> place 10,000 units trade in his account, then <span data-widget="forex-forexSocialProfile" data-type="example-units" class="blue-text">10000</span> units trade on the same asset will be executed in your account.</span>
                                    </div>
                                </div>
                            </div>
                            <div class="copy-footer">
                                <div class="copy-footer-title">Stop Copying if losses exceeded:</div>
                                <div class="copy-header-body">
                                    <input class="profile-popup-input" data-widget="forex-forexSocialProfile" data-type="profile-stoploss" type="text" value="" oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
                                </div>
                            </div>
                            <div data-widget="forex-forexSocialProfile" data-type="profile-different-mode" data-traderid="" class="profile-different-mode">
                                <span data-widget="forex-forexSocialProfile" data-type="nickname" class="blue-text"></span> <span class="different-mode-text" data-translate="diff-mode-text"></span>  <a href="#" class="copy-diff-mode" data-translate="change-mode">Change trading mode</a>
                            </div>
                            <div class="profile-copy-holder" data-widget="forex-forexSocialProfile" data-type="profile-copy-holder"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END SOCIAL PROFILE POP UP-->

<!-- SOCIAL SEARCH POP UP -->
<div id="tol-explore-popup" data-widget="forex-forexAdvancedSearch" data-type="container" class="modal fade" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="tol-explore-label">
    <div class="modal-dialog modal-sm modal-default" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="tol-social-label">Advanced Search</h4>
                <h4 class="modal-title">Search Tool</h4>
            </div>
            <div class="modal-body explore-search-fields">
                <div class="explore-search-row">
                    <span class="explore-search-title">Time Period</span>
                    <div class="time-box-wrapper">
                        <span data-widget="forex-forexAdvancedSearch" data-type="timeboxes" data-time="1" class="stats-time-box selected">1m</span>
                        <span data-widget="forex-forexAdvancedSearch" data-type="timeboxes" data-time="3"class="stats-time-box">3m</span>
                        <span data-widget="forex-forexAdvancedSearch" data-type="timeboxes" data-time="6" class="stats-time-box">6m</span>
                        <span data-widget="forex-forexAdvancedSearch" data-type="timeboxes" data-time="12" class="stats-time-box">1y</span>
                    </div>
                </div>
                <div class="explore-search-row">
                    <span class="explore-search-title">P/L</span>
                    <div class="less-than-wrapper">
                        <div type="button" class="nav-button button less-than" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span data-widget="forex-forexAdvancedSearch" data-type="pltext">More</span>
                            <span class="dropdown-arrow"></span>
                        </div>
                        <ul class="dropdown-menu">
                            <li data-widget="forex-forexAdvancedSearch" data-type="plDdl" direction="1">Less</li>
                            <li data-widget="forex-forexAdvancedSearch" data-type="plDdl" direction="2">More</li>
                        </ul>
                    </div>
                    <span class="explore-search-than">than</span>
                    <input data-widget="forex-forexAdvancedSearch" data-type="plVal" class="advanced-search-percent" type="text" oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" value ="0%"/>
                </div>
                <div class="explore-search-row">
                    <span class="explore-search-title">Copiers</span>
                    <div class="less-than-wrapper">
                        <div type="button" class="nav-button button less-than" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span data-widget="forex-forexAdvancedSearch" data-type="copierstext">More</span>
                            <span class="dropdown-arrow"></span>
                        </div>
                        <ul class="dropdown-menu">
                            <li data-widget="forex-forexAdvancedSearch" data-type="copiersDdl" direction="1">Less</li>
                            <li data-widget="forex-forexAdvancedSearch" data-type="copiersDdl" direction="2">More</li>
                        </ul>
                    </div>
                    <span class="explore-search-than">than</span>
                    <input data-widget="forex-forexAdvancedSearch" data-type="copiersVal" class="advanced-search-percent" type="number" min="0" step="1" value ="0"/>
                </div>
                <div class="explore-search-row">
                    <span class="explore-search-title">Success Rate</span>
                    <div class="less-than-wrapper">
                        <div type="button" class="nav-button button less-than" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span data-widget="forex-forexAdvancedSearch" data-type="ratetext">More</span>
                            <span class="dropdown-arrow"></span>
                        </div>
                        <ul class="dropdown-menu">
                            <li data-widget="forex-forexAdvancedSearch" data-type="sRateDdl" direction="1">Less</li>
                            <li data-widget="forex-forexAdvancedSearch" data-type="sRateDdl" direction="2">More</li>
                        </ul>
                    </div>
                    <span class="explore-search-than">than</span>
                    <input data-widget="forex-forexAdvancedSearch" data-type="sRateVal" class="advanced-search-percent" type="text" oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" value ="0%"/>
                </div>
                <div class="explore-search-row">
                    <span class="explore-search-title">Asset Type</span>
                    <div class="asset-type-wrapper">
                        <div type="button" class="nav-button button asset-type" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span data-widget="forex-forexAdvancedSearch" data-type="assettext">All</span>
                            <span class="dropdown-arrow"></span>
                        </div>
                        <ul class="dropdown-menu">
                            <li data-widget="forex-forexAdvancedSearch" data-type="assetType" astype="">All</li>
                            <li data-widget="forex-forexAdvancedSearch" data-type="assetType" astype="1">Currencies</li>
                            <li data-widget="forex-forexAdvancedSearch" data-type="assetType" astype="2">Indices</li>
                            <li data-widget="forex-forexAdvancedSearch" data-type="assetType" astype="3">Stocks</li>
                            <li data-widget="forex-forexAdvancedSearch" data-type="assetType" astype="4">Commodities</li>
                            <li data-widget="forex-forexAdvancedSearch" data-type="assetType" astype="5">Futures</li>
                        </ul>
                    </div>
                </div>
                <div data-widget="forex-forexAdvancedSearch" data-type="submit" class="advanced-search-btn">Search</div>
            </div>
            <div class="modal-header">
                <h4 class="modal-title">Search Results</h4>
            </div>
            <div class="modal-body explore-results dataTable">
                <table class="fake-search-thead traderoom-social hasArrows">
                    <thead>
                    <tr>
                        <th data-column="name" data-sort="string" class="trader-avatar canSort canResize canDrag"><span class="content">Name</span></th>
                        <th data-column="copiers" data-sort="string" class="copiers canSort canResize canDrag"><span class="content">Copiers</span></th>
                        <th data-column="pl" data-sort="int" class="pandl canSort canResize canDrag"><span class="content">P/L(%)</span></th>
                        <th data-column="srate" data-sort="int" class="success-rate canSort canResize canDrag"><span class="content">Success Rate</span></th>
                        <th data-column="copy" data-sort="string" class="copy canSort canResize canDrag"><span class="content">Copy</span></th>
                    </tr>
                    </thead>
                </table>
                <div class="scrollable-search-area">
                    <div class="scrollable-area-body">
                        <div class="scrollable-area-content">
                            <table data-widget="forex-forexAdvancedSearch" data-type="table" class="real-search-thead traderoom-social hasArrows">
                                <thead>
                                <tr>
                                    <th data-column="name" data-sort="string" class="trader-avatar canSort canResize canDrag">
                                        <span class="content">Name</span>
                                    </th>
                                    <th data-column="copiers" data-sort="string" class="copiers canSort canResize canDrag">
                                        <span class="content">Copiers</span>
                                    </th>
                                     <th data-column="pl" data-sort="int" class="pandl canSort canResize canDrag">
                                        <span class="content">P/L(%)</span>
                                    </th>
                                    <th data-column="srate" data-sort="int" class="success-rate canSort canResize canDrag">
                                        <span class="content">Success Rate</span>
                                    </th>
                                    <th data-column="copy" data-sort="string" class="copy canSort canResize canDrag">
                                        <span class="content">Copy</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END SOCIAL SEARCH POP UP -->