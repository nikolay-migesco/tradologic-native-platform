<?php
/**
 * Template Name: Cashier Fail (widget area)
 *
 */

get_header(); 
?>
<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script>
<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js'></script>
<script>
    jQuery(function ($) {
       // reload page w/ timeout
        var t=setTimeout(function(){
                window.location.href = '/' + (($.cookie('current_language') == 'en' || $.cookie('current_language') == undefined) ? '' : ($.cookie('current_language') + '/')) + "cashier";
        }, 5000);
    });
</script>
	
<div id="content" role="main">
    <div class="container">    
        <div class="row">  
            <div class="tol-cashier fail-page">   
                 <div class="depositSummary depositFail">
			        <?php while ( have_posts() ) : the_post(); ?>
				        <?php if ( has_post_thumbnail() ) : ?>
					        <div class="entry-page-image">
						        <?php the_post_thumbnail(); ?>
					        </div><!-- .entry-page-image -->
				        <?php endif; ?>

				        <?php get_template_part( 'content', 'page' ); ?>
                                    
			        <?php endwhile; // end of the loop. ?>
                </div>                                     
            </div>         
        </div> 
    </div>
</div>


<?php get_footer(); ?>