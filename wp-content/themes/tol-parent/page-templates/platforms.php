<?php
/**
 * Template Name: platform page 
 */

get_header(); ?>
<div class="dis_block clearfix2 padT45 range ">
  <div class="container">
    <div class="full text-center">
      <h1><?php the_title(); ?></h1>
    </div>
	
	<?php $section = get_field('sections'); ?>
	
    <div class="full padB90">
      <div class="row">
        <div class="col-sm-12">
          <div class="padL1 full padT1 line1"> <span class="font1"><?php echo  $section[0]['heading']; ?></span>
            <p><?php echo  $section[0]['text'];?> </p>
          </div>
        </div>
        
        <div class="col-sm-12">
        <div class="padL1 full relative">
         <div class="button-bottom"><a href="#" class="yellow-bt">Open an Account</a></div>
        <div class="computer2"><img src="<?php echo  $section[0]['image'];?>" alt=""></div>
        </div>
        
        </div>
        
      </div>
    </div>
  </div>
</div>





<div class=" greybg dis_block clearfix2 padT115 padB90 range">
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
        <div class="padL1 dis_block clearfix2 line1"> <span class="font1"><?php echo  $section[1]['heading']; ?></span>
         <?php echo  $section[1]['text'];?>          
          
          
          <div class="clearfix2 padT35"><a href="#" class="yellow-bt">Open An Account</a></div>
        </div>
      </div>
      <div class="col-sm-6"> <i class="text-right full"><img src="<?php echo  $section[1]['image'];?>" alt=""></i> </div>
    </div>
  </div>
</div>



<div class=" dis_block clearfix2 padT115 padB90 range">
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
        <div class="padL1 dis_block clearfix2 line1"> <span class="font1"><?php echo  $section[2]['heading']; ?></span>
       <?php echo  $section[2]['text'];?>           
          
          <div class="clearfix2 padT35"><a href="#" class="yellow-bt">Open An Account</a></div>
        </div>
      </div>
      <div class="col-sm-6"> <i class="full"><img src="<?php echo  $section[2]['image'];?>" alt=""></i> </div>
    </div>
  </div>
</div>



<?php get_footer(); ?>
