<?php
/**
 * Created by PhpStorm.
 * User: OBenov
 * Date: 10-Jul-17
 * Time: 9:10 AM
 * Template Name: Portfolio Builder
 */

get_header();
wp_enqueue_style('w3', "/wp-content/themes/tol-parent/styles/w3.css", array(), md5_file(get_template_directory() . "/styles/w3.css"), false);
?>
<script type="text/javascript">
    $(document).ready(function () {
        $('.userBarElem.currgame').text('Portfolio');
        $('.tol-forex-pro').show();
        $('.tol-portfolio').hide();
    });
</script>
<div id="content" role="main">
    <div id="tol-portfolio-builder-1">
        <div class="container-fluid no-padding-all">
            <div class="portfolio-header-part">
                <h1 data-translate="portfolio-builder"></h1>
                <h3 data-translate="portfolio-builder-header"></h3>
            </div>
            <div id="portfolioBuilderChart" class="pastExpiriesChart" data-widget="portfolioBuilder" data-type="chart-popup" style="display: none;">
                <a class="portfolioBuilderChartClose" data-widget="portfolioBuilder" data-type="chart-popup-close">x</a>
                <div class="portfolio-chart"></div>
            </div>
            <div class="portfolio-body-part">
                <div class="row">
                    <div class="col-xs-12 col-sm-7">
                        <div class="portofolio-builder-table" data-widget="portfolioBuilder" data-type="table"></div>
                    </div>
                    <div class="col-xs-12 col-sm-5">
                        <div class="portofolio-builder-stats">
                            <div class="portfolio-name">
                                <div data-widget="portfolioBuilder" data-type="name">My Portfolio</div>
                                <input data-widget="portfolioBuilder" data-type="input-name" type="text" class="input-nickname" value="" style="display:none;" />
                            </div>
                            <div class="portfolio-additional-info" data-widget="portfolioBuilder" data-type="portfolio-info">Create your <span>portfolio</span> by adding a strategy</div>
                            <div class="portfolio-details-wrapper" style="display: none;">
                                <div class="portfolio-details" data-widget="portfolioBuilder" data-type="details"></div>
                            </div>
                            <div>
                                <div id="pbuilder-chart" class="portfolio-nostrategies col-sm-6" data-widget="portfolioBuilder" data-type="portfolio-chart"></div>
                                <div class="col-sm-6">
                                    <div class="portfolio-builder-total" data-widget="portfolioBuilder" data-type="summary"></div>
                                    <div class="portfolio-invest-btn" data-widget="portfolioBuilder" data-type="invest" data-translate="invest" data-target="#portfolio-modal" data-toggle="modal" style="display: none;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-backdrop pbuilder-chart-back" style="display: none;"></div>

<div class="modal fade portfolioModal" id="portfolio-modal" data-widget="portfolioBuilder" data-type="portfolioModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel" data-widget="portfolioBuilder" data-type="portfolioModalTitle">Modal title</h4>
            </div>
            <div class="modal-body" data-widget="portfolioBuilder" data-type="portfolioModalContent">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary portfolioActionButton" data-widget="portfolioBuilder" data-type="portfolioModalButton">Save changes</button>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();
?>
