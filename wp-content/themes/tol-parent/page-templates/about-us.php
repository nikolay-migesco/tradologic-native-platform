<?php
/**
 *
 * Template Name: About Us Template
 *
 */

get_header(); ?>

<div id="content" role="main">
    <div id="tol-about-us-layout-1">
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="container">
                <div class="pageHeadTitle">
                    <?php require_once(ABSPATH.'wp-content/themes/tol-parent/layouts/'.BREADCRUMB.'.php' ); ?>
                </div>
                <header class="entry-header">
                    <h1 class="entry-title"><?php the_title(); ?></h1>
                </header>
                <div class="entry-content col-sm-12">
                    <?php echo $post->post_content; ?>
                </div>
            </div>
        </article>
    </div>
</div>

<?php
    get_footer();
?>