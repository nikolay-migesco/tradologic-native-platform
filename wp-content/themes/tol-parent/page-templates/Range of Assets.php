<?php
/**
 * Template Name: Range of assets 
 */

get_header(); ?>
<div class="dis_block clearfix2 padT45 range ">
  <div class="container">
    <div class="full text-center">
      <h1>Range of assets </h1>
    </div>
    <div class="full padT2 padB90">
      <div class="row">
        <div class="col-sm-6">
          <div class="padL1 dis_block clearfix2 line1"> <span class="font1"><?php the_field('forex_section_heading'); ?></span>
            <p><?php echo get_field('forex_section_text'); ?></p>
            <div class="clearfix2 padT35"><a href="/registration" class="yellow-bt">Open An Account</a></div>
          </div>
        </div>
		<?php $table_forex = get_field('table'); ?>
        <div class="col-sm-6">
          <div class="assets-tble">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr class="theadbg">
                <th>Currency</th>
                <th>Min Stake</th>
                <th>Tading hours</th>
              </tr>
			  <?php foreach($table_forex as $table_for){ ?>
              <tr>
                <td><?php echo $table_for['currency']; ?></td>
                <td><?php echo $table_for['min_stake']; ?></td>
                <td><?php echo $table_for['tading_hours']; ?></td>
              </tr>
			  <?php } ?>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class=" greybg dis_block clearfix2 padT115 padB90 range">
  <div class="container">
    <div class="row">
        <div class="col-sm-5">
          <div class="padL1 dis_block clearfix2 line1"> <span class="font1"><?php the_field('shares_heading'); ?></span>
            <p><?php echo get_field('shares_text'); ?></p>
            <div class="clearfix2 padT35"><a href="/registration" class="yellow-bt">Open An Account</a></div>
          </div>
        </div>
		<?php $table_shares = get_field('share_table'); ?>
        <div class="col-sm-7">
          <div class="assets-tble">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr class="theadbg">
              	<th>Country</th>
                <th>Shares</th>
                <th>Min Stake</th>
                <th>Tading hours</th>
              </tr>
			   <?php foreach($table_shares as $table_share){ ?>
              <tr>
               <td><?php echo $table_share['country']; ?></td>
                <td><?php echo $table_share['shares']; ?></td>
                <td><?php echo $table_share['min_stake']; ?></td>
                <td><?php echo $table_share['tading_hours']; ?></td>
              </tr>
               <?php } ?>
            </table>
          </div>
        </div>
      </div>
  </div>
</div>
<div class=" dis_block clearfix2 padT115 padB90 range">
  <div class="container">
    <div class="row">
        <div class="col-sm-6">
          <div class="padL1 dis_block clearfix2 line1"> <span class="font1"><?php the_field('indices_heading'); ?></span>
            <p><?php echo get_field('indices_text'); ?></p>
            <div class="clearfix2 padT35"><a href="/registration" class="yellow-bt">Open An Account</a></div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="assets-tble">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr class="theadbg">
                <th>Index</th>
                <th>Min Stake</th>
                <th>Tading hours</th>
              </tr>
			  <?php $table_indices = get_field('indices_table'); ?>
               <?php foreach($table_indices as $table_indice){ ?>
              <tr>
                <td><?php echo $table_indice['index']; ?></td>
                <td><?php echo $table_indice['min_stake']; ?></td>
                <td><?php echo $table_indice['tading_hours']; ?></td>
              </tr>
             <?php } ?>
            </table>
          </div>
        </div>
      </div>
  </div>
</div>


<div class=" greybg dis_block clearfix2 padT115 padB90 range">
  <div class="container">
    <div class="row">
        <div class="col-sm-6">
          <div class="padL1 dis_block clearfix2 line1"> <span class="font1"><?php the_field('commodities_heading'); ?></span>
            <p><?php echo get_field('commodities_text'); ?></p>
            <div class="clearfix2 padT35"><a href="/registration" class="yellow-bt">Open An Account</a></div>
          </div>
        </div>
		 <?php $table_commodities = get_field('commodities_table'); ?>
        <div class="col-sm-6">
          <div class="assets-tble">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr class="theadbg">
                <th>Index</th>
                <th>Min Stake</th>
                <th>Tading hours</th>
              </tr>
			  <?php foreach($table_commodities as $table_commoditie){ ?>
              <tr>
                 <td><?php echo $table_commoditie['index']; ?></td>
                <td><?php echo $table_commoditie['min_stake']; ?></td>
                <td><?php echo $table_commoditie['tading_hours']; ?></td>
              </tr>
			  <?php } ?>
			</table>
          </div>
        </div>
      </div>
  </div>
</div>






<?php get_footer(); ?>
