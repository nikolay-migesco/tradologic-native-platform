<?php
/*
 *
 * Template Name: Trading Instruments
 *
 */

get_header();
?>

<div id="content" role="main">
    <div class="container">
        <header class="entry-header">
            <h1 class="entry-title"><?php the_title(); ?></h1>
        </header>
        <div class="row traderinstruments-wrapper">
            <div data-widget="forexSwapRates" class="forexswaprates">
                <div class="forexswaprates-game-menu">
                    <ul class="nav">
                        <li data-widget="forexSwapRates" data-type="tab" data-game="EasyForex" class="active">Simplex</li>
                        <li data-widget="forexSwapRates" data-type="tab" data-game="RealForex">Real Forex</li>
                    </ul>
                </div>
                <input data-widget="forexSwapRates" data-target="search" class="forexswaprates-menu-search" placeholder="Search Instruments..."/>
                <div class="forexswaprates-menu">
                    <div data-widget="forexSwapRates" data-type="menu" data-target="all" class="forexswaprates-menu-currencies active">All</div>
                    <div data-widget="forexSwapRates" data-type="menu" data-target="currencies" class="forexswaprates-menu-currencies">Forex</div>
                    <div data-widget="forexSwapRates" data-type="menu" data-target="crypto" class="forexswaprates-menu-crypto">CryptoCurrencies</div>
                    <div data-widget="forexSwapRates" data-type="menu" data-target="indices" data-translate="Indices" class="forexswaprates-menu-indices"></div>
                    <div data-widget="forexSwapRates" data-type="menu" data-target="stocks" data-translate="Stocks" class="forexswaprates-menu-stocks"></div>
                    <div data-widget="forexSwapRates" data-type="menu" data-target="commodities" data-translate="Commodities" class="forexswaprates-menu-commodities"></div>
                </div>
                <div class="trading-conditions-table-container">
                    <div data-widget="forexSwapRates" data-type="loading" class="trading-conditions-loading"></div>
                    <div data-widget="forexSwapRates" data-type="fakeHead" class="trading-conditions-fake-head"></div>
                    <div class="scrollable-trading-conditions-table">
                        <div class="scrollable-area-body">
                            <div class="scrollable-area-content">
                                <div class="clearfix flat-top-borders">
                                    <div data-widget="forexSwapRates" data-type="table" class="trading-conditions-table"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="forexswaprates-export-container">
                    <div data-widget="forexSwapRates" data-type="export" data-file="pdf" class="forexswaprates-export-item exportPDF">Export to PDF</div>
                    <div data-widget="forexSwapRates" data-type="export" data-file="xls" class="forexswaprates-export-item exportXLS">Export to Excel</div>
                </div>
            </div>
        </div>
    </div>  
</div>
<script>
    $(document).ready(function () {
        $(window).on('tableIsLoaded', function(){
            $(".scrollable-trading-conditions-table").mCustomScrollbar("destroy");
            $(".scrollable-trading-conditions-table").mCustomScrollbar({
                scrollButtons: {
                    enable: false
                },
                theme: 'minimal-dark',
                horizontalScroll: false,
                autoDraggerLength: true,
                autoHideScrollbar: true,
                advanced: {
                    autoScrollOnFocus: false,
                    updateOnContentResize: true,
                    updateOnBrowserResize: true
                }
            });
        });
    });
</script>
<?php
    get_footer();
?>
