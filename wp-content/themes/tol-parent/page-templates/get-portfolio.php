<?php
/**
 * Created by PhpStorm.
 * User: OBenov
 * Date: 10-Jul-17
 * Time: 9:10 AM
 * Template Name: Get Portfolio
 */

get_header(); ?>
<script type="text/javascript">
    $(window).on("loadingIsCompleted", function() {
        var currElements = $('.seq-step2 .e-answer');

        for(var i = 0; i < currElements.length; i++) {
            currElements[i].innerHTML = currElements[i].innerHTML.replace(/\{currency}/g, helper.getCurrencySymbol())
        };
    });

    $(document).ready(function(){
        $('.userBarElem.currgame').text('Portfolio');
        $('.tol-portfolio, .seq-step').hide();
        $('.tol-forex-pro, .seq-step1').show();

        $('#tol-get-portfolio-1').css('height', $(document).height() - 120);

        $('.e-answer').click(function(){
            $('.' + $(this).parent()[0].className.replace(' ', '.') + ' .e-answer').removeClass('selected');
            $(this).addClass('selected');

            if (typeof currentQ != 'undefined') {
                if ($('.seq-current .answer__wrapper').data('step') == currentQ+1) {
                    $('.seq-next').hide();
                };
            };

            if ($('.seq-current .answer__wrapper').data('step') != 4) {
                $('.seq-next').click();
            };
        });

        $('.seq-step2 .e-answer').click(function(){
            localStorage.setItem('amount', $(this).data('value'));
        });

        $('.seq-step3 .e-answer').click(function(){
            localStorage.setItem('yearsRange', $(this).data('value'));
        });

        $('.seq-step4 .e-answer').click(function(){
            localStorage.setItem('risk', $(this).data('value'));
        });

        $('.seq-prev').click(function() {
            $('.seq-next').show();
            $('.seq-finish').hide();
            currentQ = $('.seq-current .answer__wrapper').data('step');

            if ($('.seq-current .answer__wrapper').data('step') == 2) {
                $('.seq-prev').hide();
            } else {
                $('.seq-prev').attr('disabled', false);
            };

            var currStep = $('.seq-current .answer__wrapper').data('step');

            $('.seq-step').removeClass('seq-current').hide();
            $('.seq-step' + (currStep-1)).addClass('seq-current').show();
            $('.progress-step' + currStep).removeClass('selected');
        });

        $('.seq-next').click(function() {
            $('.seq-prev').show();

            if (typeof currentQ != 'undefined') {
                if ($('.seq-current .answer__wrapper').data('step') == (currentQ-1)) {
                    $('.seq-next').hide();
                };
            };

            if ($('.seq-current .answer__wrapper').data('step') == 3) {
                $('.seq-next').hide();
                $('.seq-finish').show();
            };

            var currStep = $('.seq-current .answer__wrapper').data('step');

            $('.seq-step').removeClass('seq-current').hide();
            $('.seq-step' + (currStep+1)).addClass('seq-current').show();
            $('.progress-step' + (currStep+1)).addClass('selected');
        });

        $('.seq-finish').click(function() {
            if (!$('.seq-current .answer__wrapper .e-answer').hasClass('selected')) {
                apprise('Please select an answer!');
                return;
            };

            window.location.href = '/' + ($.cookie('current_language') == 'en' ? '' : $.cookie('current_language') + '/') + 'portfolio-adviser';
            return;
        });
    });
</script>
    <div class="traderoom-loader">
        <div class="loader">
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
            <span class="loader-block"></span>
        </div>
    </div>
<div id="content" role="main" style="display: none;">
    <div id="tol-get-portfolio-1">
        <div class="container-fluid no-padding-all">
            <div class="get-portfolio-content">
                <section id="sequence" class="seq">
                    <h1>Tell us about yourself</h1>
                    <h3>Answer 4 simple questions to get the portfolio for you</h3>
                    <ul class="seq-canvas">
                        <li class="seq-step seq-step1 seq-valign seq-current">
                            <div class="seq-vcenter">
                                <h2 class="seq-title">How old are you?</h2>
                                <div class="a-answer__wrapper answer__wrapper" data-step="1">
                                    <div class="e-answer" data-value="18-25"><div class='answer-back'></div><span>18-25</span></div>
                                    <div class="e-answer" data-value="26-35"><div class='answer-back'></div><span>26-35</span></div>
                                    <div class="e-answer" data-value="36-50"><div class='answer-back'></div><span>36-50</span></div>
                                    <div class="e-answer" data-value="51-60"><div class='answer-back'></div><span>51-60</span></div>
                                    <div class="e-answer" data-value="61+"><div class='answer-back'></div><span>60+</span></div>
                                </div>
                            </div>
                        </li>

                        <li class="seq-step seq-step2 seq-valign">
                            <div class="seq-vcenter">
                                <h2 class="seq-title">What is the total value of your liquid assets?</h2>
                                <div class="b-answer__wrapper answer__wrapper" data-step="2">
                                    <div class="e-answer" data-value="min-10000"><div class='answer-back'></div><span>Less than {currency}10,000</span></div>
                                    <div class="e-answer" data-value="10000-50000"><div class='answer-back'></div><span>{currency}10,000 to {currency}50,000</span></div>
                                    <div class="e-answer" data-value="50000-100000"><div class='answer-back'></div><span>{currency}50,000 to {currency}100,000</span></div>
                                    <div class="e-answer" data-value="100000-500000"><div class='answer-back'></div><span>{currency}100,000 to {currency}500,000</span></div>
                                    <div class="e-answer" data-value="500000-max"><div class='answer-back'></div><span>More than {currency}500,000</span></div>
                                </div>
                            </div>
                        </li>

                        <li class="seq-step seq-step3 seq-valign">
                            <div class="seq-vcenter">
                                <h2 class="seq-title">For how long you plan to invest?</h2>
                                <div class="c-answer__wrapper answer__wrapper" data-step="3">
                                    <div class="e-answer" data-value="min-2"><div class='answer-back'></div><span>2 years or less</span></div>
                                    <div class="e-answer" data-value="2-5"><div class='answer-back'></div><span>2-5 years</span></div>
                                    <div class="e-answer" data-value="5-8"><div class='answer-back'></div><span>5-8 years</span></div>
                                    <div class="e-answer" data-value="8-10"><div class='answer-back'></div><span>8-10 years</span></div>
                                    <div class="e-answer" data-value="10-max"><div class='answer-back'></div><span>10+ years</span></div>
                                </div>
                            </div>
                        </li>

                        <li class="seq-step seq-step4 seq-valign">
                            <div class="seq-vcenter">
                                <h2 class="seq-title">What is your risk tolerance?</h2>
                                <div class="d-answer__wrapper answer__wrapper" data-step="4">
                                    <div class="e-answer" data-value="1"><div class='answer-back'></div><span>Low</span></div>
                                    <div class="e-answer" data-value="2"><div class='answer-back'></div><span>Medium</span></div>
                                    <div class="e-answer" data-value="3"><div class='answer-back'></div><span>High</span></div>
                                </div>
                            </div>
                        </li>
                    </ul>

                    <fieldset class="seq-nav" aria-label="Slider buttons" aria-controls="sequence">
                        <div type="button" class="seq-prev" aria-label="Previous" style="display: none;">
                            <div class="img-arrow"></div>
                            <span class="portfolio-back-btn">Back</span>
                        </div>
                        <div type="button" class="seq-next" aria-label="Next" style="display: none;">
                            <span class="portfolio-next-btn">Continue</span>
                            <div class="img-arrow"></div>
                        </div>
                        <div type="button" class="seq-finish" aria-label="Next" style="display: none;">Finish</div>
                    </fieldset>
                </section>
            </div>
        </div>
        <div class="progress-section">
            <div class="progress-step first"></div>
            <div class="progress-step progress-step2"></div>
            <div class="progress-step progress-step3"></div>
            <div class="progress-step progress-step4"></div>
        </div>
    </div>
</div>

<?php
get_footer();
?>