<?php
/**
 *
 * Template Name: Contact us
 *
 */

get_header(); ?>

<div id="content" role="main">
    <div id="tol-contactUs-layout-1" class="container-fluid exchange-layout">
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <header class="entry-header">
                <h1 class="entry-title first-article-header"><?php the_title(); ?></h1>
            </header>
            <div class="row">
                <div class="well well-sm clearfix flat-top-borders">
                    <div class="col-sm-6">
                        <form action="" class=" form-horizontal">
                            <?php echo do_shortcode('[contactUsContainer]
                        <div class="form-group">
                            <label for="inputName" class="col-lg-2 control-label" data-translate="ContactUs_Name"></label>
                            <div class="col-lg-10">
                                [contactUsContactName class="form-control"]
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPhone" class="col-lg-2 control-label" data-translate="ContactUs_Phone">Phone</label>
                            <div class="col-lg-10">
                                [contactUsContactPhone class="form-control"]
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail" class="col-lg-2 control-label" data-translate="ContactUs_Email">Email</label>
                            <div class="col-lg-10">
                                [contactUsContactEmail class="form-control"]
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputSubject" class="col-lg-2 control-label" data-translate="ContactUs_Subject">Subject</label>
                            <div class="col-xs-12 col-lg-10">
                                [contactUsContactSubject]
                                [contactUsContactSubjectSelect class="subject-selector"]                        
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputMessage" class="col-lg-2 control-label" data-translate="ContactUs_Message">Message</label>
                            <div class="col-lg-10">
                                [contactUsContactContactMessage class="form-control"]         
                            </div>
                        </div>
                        <div>                                                             
                            [contactUsContactSubmit class="btn btn-primary"]
                        </div>
                        [/contactUsContainer]'); ?>
                        </form>
                    </div>
                    <div class="col-sm-6">
                        <div class="entry-content">
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </div>
</div>

<?php
    get_footer();
?>