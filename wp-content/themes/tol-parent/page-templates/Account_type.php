<?php
/**
 * Template Name: Account type page 
 */

get_header(); ?>
<div class="dis_block clearfix2 padT45 ">
  <div class="container">
    <div class="full text-center">
      <h1><?php the_title(); ?></h1>
      <p class="font1"><?php echo get_field('heading'); ?></p>
    </div>
    <div class="account-types dis_block clearfix2" >
	<?php $plans = get_field('account_types'); 
		  $i=1;
		  
		  foreach($plans as $plan){
		?>
	  <div class="col-width">
        <div class="header color<?php echo $i; ?>"><?php echo $plan['type_name'] ?></div>
        <div class="dis_block clearfix2">
				
			<?php 
			/* echo "<pre>";
			print_r($plan['features']); die(); */?>
		  <?php foreach($plan['features'] as $fetures){ ?>
		  
          <div class="options <?php  if($fetures['active_status'][0] == 'yes'){ echo "show";} else {echo "off";} ?>"><?php echo $fetures['name']; ?> </div>
		  
		  <?php } ?>
        </div>
        <div class="get-stared color<?php echo $i; ?>-footer"><a href="<?php echo $plan['link'];?>">Get Started</a></div>
      </div>
	  <?php  $i++; }
	?>
    </div>
	
    <?php
		$id = get_the_id();
		$post_object = get_post( $post_id );
			echo $post_object->post_content;
	?>
  </div>
</div>


<?php get_footer(); ?>
