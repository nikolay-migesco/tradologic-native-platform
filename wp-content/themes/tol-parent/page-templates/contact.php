<?php
/**
 * Template Name: contact page 
 */

get_header(); ?>
<div class="dis_block clearfix2 padT45 padB75 ">
  <div class="container">
    <div class="full text-center">
      <h1><?php the_title(); ?></h1>
      
    </div>
    
<div class="full padT45">    
<div class="row ">
<div class="col-sm-6 ">
<?php 
		$id = get_the_id();
		$post_object = get_post( $post_id );
			echo $post_object->post_content;
?>

<div class="email-telephone padT35">
<span><i><img src="<?php echo get_template_directory_uri(); ?>/images/mail-icon.png" alt=""></i><em class="semi-bold">T:</em> <?php echo get_field('phone_number'); ?></span>
<span><i><img src="<?php echo get_template_directory_uri(); ?>/images/phone-icon.png" alt=""></i><em class="semi-bold">E:</em> <?php echo get_field('email_address'); ?> </span>
</div>




</div>
<div class="col-sm-6">
<div class="contact-form">
<?php 
$short_contact = get_field('contact_form_short_code');

echo do_shortcode($short_contact);

?>



</div>






</div>
</div>   

</div> 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
  </div>
  
  
  
</div>

<?php get_footer(); ?>
