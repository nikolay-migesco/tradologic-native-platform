<?php
/**
 *
 * Template Name: Past Expiries
 *
 */

get_header(); ?>

<div id="content" role="main">
    <div id="tol-pastExpiries-layout-1">
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="container">
                <div class="pageHeadTitle">
                    <?php require_once(ABSPATH.'wp-content/themes/tol-parent/layouts/breadcrumb.php' ); ?>
                </div>
                <header class="entry-header">
                    <h1 class="entry-title"><?php the_title(); ?></h1>
                </header>
                <form action="" class="tol-form-inline">
                    <div class="row">
                        <div class="well">
                            <div class="row">
                                <div class="form-group col-xs-12 col-sm-4">
                                    <div class="row">
                                        <label class="col-xs-4 col-sm-2" data-translate="TradeRoom_Assets"></label>
                                        <div class="col-xs-8 col-sm-8 nopadding-right">
                                            <?php echo do_shortcode('[pastExpiriesOptions class="form-control btn-default form-control-md hasCustomSelect"]'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-xs-6 col-sm-3">
                                    <div class="row">
                                        <label class="col-sm-4 hidden-xs text-right" data-translate="MyOptions_From"></label>
                                        <div class="col-sm-8 past-expiries-from-date nopadding-left">
                                            <?php echo do_shortcode('[pastExpiriesFromTo widget="pastExpiriesFromDate" class="form-control hasCustomSelect"]'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-xs-6 col-sm-3">
                                    <div class="row">
                                        <label class="col-sm-4 hidden-xs text-right" data-translate="MyOptions_To"></label>
                                        <div class="col-sm-8 nopadding-left">
                                            <?php echo do_shortcode('[pastExpiriesFromTo widget="pastExpiriesToDate" class="form-control hasCustomSelect"]'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-2">
                                    <?php echo do_shortcode('[pastExpiriesSearch class="btn btn-primary col-xs-12"]'); ?>
                                </div>
                            </div>
                        </div>
                        <?php echo do_shortcode('[pastExpiriesTable class="table table-striped"]'); ?>
                        <div class="text-right">
                            <?php echo do_shortcode('[pastExpiriesPaging class="pagination pagination-sm"]'); ?>
                        </div>
                    </div>
                </form>
            </div>
            <div class="entry-content">
                <?php echo $post->post_content; ?>
            </div>
        </article>
    </div>
</div>

<?php
    get_footer();
?>