<?php
/**
 * Created by PhpStorm.
 * User: OBenov
 * Date: 10-Jul-17
 * Time: 9:10 AM
 * Template Name: Portfolio Details
 */

    get_header();
    wp_enqueue_style('w3', "/wp-content/themes/tol-parent/styles/w3.css", array(), md5_file(get_template_directory() . "/styles/w3.css"), false);
?>
<script type="text/javascript">
    $(document).ready(function () {
        $('.userBarElem.currgame').text('Portfolio');
        $('.tol-forex-pro').show();
        $('.tol-portfolio').hide();
    });
</script>
<div id="content" role="main">
    <div id="tol-portfolio-details-1">
        <div class="container-fluid no-padding-all">
            <div class="portfolio-header-part">
                <h1 data-translate="portfolio-details"></h1>
                <h3 class="portfolio-details-back" data-translate="portfolio-details-back"></h3>
            </div>
            <div class="portfolio-summary">
                <div class="portfolio-summary-field portfolio-summary-name">
                    <div class="portfolio-summary-field-title">Name</div>
                    <div data-widget="portfolioDetails" data-type="name" class="portfolio-summary-field-value"></div>
                </div>
                <div class="portfolio-summary-field portfolio-summary-started">
                    <div class="portfolio-summary-field-title">Started On</div>
                    <div data-widget="portfolioDetails" data-type="started" class="portfolio-summary-field-value"></div>
                </div>
                <div class="portfolio-summary-field portfolio-summary-investment">
                    <div class="portfolio-summary-field-title">Investment</div>
                    <div data-widget="portfolioDetails" data-type="investment" class="portfolio-summary-field-value"></div>
                </div>
                <div class="portfolio-summary-field portfolio-summary-fees">
                    <div class="portfolio-summary-field-title">Fees</div>
                    <div data-widget="portfolioDetails" data-type="fees" class="portfolio-summary-field-value"></div>
                </div>
                <div class="portfolio-summary-field portfolio-summary-profit">
                    <div class="portfolio-summary-field-title">Profit</div>
                    <div data-widget="portfolioDetails" data-type="profit" class="portfolio-summary-field-value"></div>
                </div>
                <div class="portfolio-summary-field portfolio-summary-value">
                    <div class="portfolio-summary-field-title">Value</div>
                    <div data-widget="portfolioDetails" data-type="value" class="portfolio-summary-field-value"></div>
                </div>
                <div class="portfolio-summary-field portfolio-summary-return">
                    <div class="portfolio-summary-field-title">Return</div>
                    <div data-widget="portfolioDetails" data-type="return" class="portfolio-summary-field-value"></div>
                </div>
            </div>
            <div class="portfolio-body-part">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 pdetails-chart" data-widget="portfolioDetails" data-type="chart-area-left"></div>
                    <div class="col-xs-12 col-sm-6 pdetails-overview" data-widget="portfolioDetails" data-type="chart-area-right"></div>
                </div>
            </div>
            <div class="portfolio-details-table-navigation">
                <ul class="nav">
                    <li class="open-trades active">
                        <a href="#tab1" data-toggle="tab" class="active">Open Trades</a>
                    </li>
                    <li class="closed-trades">
                        <a href="#tab2" data-toggle="tab">Closed Trades</a>
                    </li>
                </ul>
            </div>
            <div class="portfolio-details-tables-content">
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="tab1">
                        <div class="open-trades-content">
							<div class="dataTable" >
								<table class="fake-table">
									<thead>
										<tr>
											<th data-sort="string" class="name canSort canResize canDrag">
												<div class="cellWrapper"><span class="content">TRADE ID</span></div>
											</th>
											<th data-sort="string" class="name canSort canResize canDrag">
												<div class="cellWrapper"><span class="content">ASSET</span></div>
											</th>
											<th data-sort="string" class="name canSort canResize canDrag">
												<div class="cellWrapper"><span class="content">DIRECTION</span></div>
											</th>
											<th data-sort="string" class="name canSort canResize canDrag">
												<div class="cellWrapper"><span class="content">QUANTITY</span></div>
											</th>
											<th data-sort="string" class="name canSort canResize canDrag">
												<div class="cellWrapper"><span class="content">ORDER DATE</span></div>
											</th>
											<th data-sort="string" class="name canSort canResize canDrag">
												<div class="cellWrapper"><span class="content">RATE</span></div>
											</th>
											<th data-sort="string" class="name canSort canResize canDrag">
												<div class="cellWrapper"><span class="content">MARKET RATE</span></div>
											</th>
											<th data-sort="string" class="name canSort canResize canDrag">
												<div class="cellWrapper"><span class="content">RESULT</span></div>
											</th>
										</tr>
									</thead>
								</table>
							</div>
                            <table class="table table-responsive" data-widget="portfolioDetails" data-type="open-trades">
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="tab2">
                        <div class="closed-trades-content ">
							<div class="dataTable" >
								<table class="fake-table">
									<thead>
										<tr>
											<th data-sort="string" class="name canSort canResize canDrag">
												<div class="cellWrapper"><span class="content">TRADE ID</span></div>
											</th>
											<th data-sort="string" class="name canSort canResize canDrag">
												<div class="cellWrapper"><span class="content">ASSET</span></div>
											</th>
											<th data-sort="string" class="name canSort canResize canDrag">
												<div class="cellWrapper"><span class="content">DIRECTION</span></div>
											</th>
											<th data-sort="string" class="name canSort canResize canDrag">
												<div class="cellWrapper"><span class="content">QUANTITY</span></div>
											</th>
											<th data-sort="string" class="name canSort canResize canDrag">
												<div class="cellWrapper"><span class="content">ORDER DATE</span></div>
											</th>
											<th data-sort="string" class="name canSort canResize canDrag">
												<div class="cellWrapper"><span class="content">RATE</span></div>
											</th>
											<th data-sort="string" class="name canSort canResize canDrag">
												<div class="cellWrapper"><span class="content">CLOSE DATE</span></div>
											</th>
											<th data-sort="string" class="name canSort canResize canDrag">
												<div class="cellWrapper"><span class="content">CLOSE RATE</span></div>
											 </th>
											<th data-sort="string" class="name canSort canResize canDrag">
												<div class="cellWrapper"><span class="content">RESULT</span></div>
											</th>
										</tr>
									</thead>
								</table>
							</div>
                            <table class="table table-responsive" data-widget="portfolioDetails" data-type="closed-trades">
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    get_footer();
?>
