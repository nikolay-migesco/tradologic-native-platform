<?php
/**
 *
 * Template Name: Copy Board
 *
 */

get_header();
wp_enqueue_style('leaderboard-layout');
?>

<div id="content" role="main">
    <div id="tol-copyBoard-layout-1">
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="container social-wrapper">
                <div class="row copy-board-wrapper">
                    <?php echo do_shortcode('[socialCopyBoardTable]'); ?>
                    <?php echo do_shortcode('[socialCopyBoardPaging]'); ?>
                </div>
            </div>
            <div class="entry-content">
                <?php echo $post->post_content; ?>
            </div>
        </article>
    </div>
</div>

<?php
    get_footer();
?>