<?php
/**
 * Template Name: why STX options page 
 */

get_header(); ?>
<div class="whySTXoption padT45">

<div class="full text-center">
      <h1><?php the_title(); ?></h1>
    </div>

	
	
<?php $sections = get_field('sections'); 
		/* echo "<pre>";
		print_r($sections);
		die(); */
$i =1;
foreach($sections as $section ){ 
$mode = $i%2;
if ($mode == 0){
$greybg = 'greybg';
} else {
	$greybg = '';
}

?>
<div class="optionRow <?php echo $greybg; ?>">

<div class="container">
<div class="col-sm-3"> <i class="text-center full"><img src="<?php echo $section['image']; ?>" alt=""></i> </div>

<div class="col-sm-9">
<div class="optionTexBox"> 
<h4><strong><?php echo $section['heading']; ?></strong></h4>
<p>
<?php echo $section['text']; ?>
</p>
</div>
</div>


</div>

</div>






<?php $i++;} ?>	
	

</div>

<?php get_footer(); ?>
