<?php
/**
 *
 * Template Name: My Trades
 *
 */

get_header(); ?>

<div id="content" role="main">
<script>
    $(window).on('loadingIsCompleted', function() {	

        if ($(window).width() < 768) {
            $('#searchBarControls .fromDate, #searchBarControls .toDate').appendTo('.chooseDateRowMobile')
            $('#searchBarControls .positionIdFilter').insertAfter('#searchBarControls .form-group.assets');
            $('#searchBarControls .gameFilter').insertAfter('#searchBarControls .form-group.assets');
        }
        $('.mytrades-tabs').click(function () {
            if ($('.myTrades-tables-content .column-settings.taborders').hasClass('show')) {
                $('.myTrades-tables-content .column-settings.taborders').removeClass('show')
            }
        });
        if ($(window).width() >= 768) {
            $('#gamesMenu  ul  li a').css('width', (100 / $('#gamesMenu  ul  li').length )+'%');
        } else {
            $('.myTrades-table-navigation  ul  li a').css('width', '100%');
            $('div[data-widget="myTradesExportButtonPDF"]').appendTo('#exportButtons-mobile');
        }
        if(widgets.isMobileDevice()) {
            $('input[data-widget="myTradesFromDate"]').attr('focusOnShow' , 'false').attr('ignoreReadonly', 'true').attr('readonly', 'true');
            $('input[data-widget="myTradesToDate"]').attr('focusOnShow' , 'false').attr('ignoreReadonly', 'true').attr('readonly', 'true');
        }

        if($(window).width() <= 767) {
            $(".column-settings>.settings").unbind("click");
            $(".column-settings").click(function(e){
                e.stopPropagation();
                if ($(this).hasClass('show')) {
                    $(this).removeClass('show');
                    $('.ui-widget-overlay').hide();

                } else {
                    $('.ui-widget-overlay').show();
                    $(this).addClass('show');
                };
                return;
            });

            $('.ui-widget-overlay').click( function(){
                $('.ui-widget-overlay').hide();
            });
        }

        $('.myTrades-table-navigation ul li').click(function() {
            $('div[data-widget="myTradesExportButtonPDF"]').attr('data-gametype','');
            $('#positionIdFilter').val('');
            $('#summaryResults').hide();
            $('#searchBarInfo').show();

            if ($('#gamesMenu-mobile-active-only ul li').filter('.active').length > 0) {
                $('#gamesMenu-mobile-active-only ul li').filter('.active').removeClass('active');
            };

            if ($(this).hasClass('myTrades-binary')) {
                $('.form-group.orders-wrapper').hide();
                $("select[data-widget='myTradesGameFilter']").val('all');
                $('[data-widget=myTradesSearchButtonNew]').data('preselectedgame','all');
                $('.form-group.gameFilter').show();
                $('.form-group.positionIdFilter, #my-trades-tabs').hide();
                $("select[data-widget='myTradesGameFilter'] option[value='ticks']").remove();
                $("select[data-widget='myTradesGameFilter'] option[value='forex']").remove();
                $("select[data-widget='myTradesGameFilter'] option[value='knockin']").remove();
                $("select[data-widget='myTradesGameFilter'] option[value='oneTouch']").remove();
                $("select[data-widget='myTradesGameFilter'] option[value='binaryExchange']").remove();
                $("select[data-widget='myTradesGameFilter'] option[value='simplex']").remove();
                $("select[data-widget='myTradesGameFilter'] option[value='realForex']").remove();
                $('#gamesMenu-mobile-active-only li.myTrades-binary').addClass('active');
                $('div[data-widget="myTradesExportButtonPDF"]').attr('data-gametype','binary');
                $('div[data-widget="myTradesExportButtonXLS"]').attr('data-gametype','binary');
            } else if ($(this).hasClass('myTrades-exchange')) {
                $('.form-group.orders-wrapper').hide();
                $("select[data-widget='myTradesGameFilter']").append($("<option></option>").attr("value",value='binaryExchange').text(value));
                $("select[data-widget='myTradesGameFilter']").val('binaryExchange');
                $('[data-widget=myTradesSearchButtonNew]').data('preselectedgame','binaryExchange');
                $('.form-group.gameFilter, .form-group.positionIdFilter, #my-trades-tabs').hide();
                $('#gamesMenu-mobile-active-only li.myTrades-exchange').addClass('active');
                $('div[data-widget="myTradesExportButtonPDF"]').attr('data-gametype','exchange');
                $('div[data-widget="myTradesExportButtonXLS"]').attr('data-gametype','exchange');
                $('.tab-content .tab-pane').removeClass('active in');
                $('.tab-content .tab-pane.fade.exchange').addClass('active in');
            }  else if ($(this).hasClass('myTrades-simpleForex')) {
                $("select[data-widget='myTradesGameFilter']").append($("<option></option>").attr("value",value='forex').text(value));
                $("select[data-widget='myTradesGameFilter']").val('forex');
                $('[data-widget=myTradesSearchButtonNew]').data('preselectedgame','forex');
                $('.form-group.gameFilter, .form-group.positionIdFilter, #my-trades-tabs').hide();
                $('#gamesMenu-mobile-active-only li.myTrades-simpleForex').addClass('active');
                $('div[data-widget="myTradesExportButtonPDF"]').attr('data-gametype','forex');
                $('div[data-widget="myTradesExportButtonXLS"]').attr('data-gametype','forex');
                $('.tab-content .tab-pane').removeClass('active in');
                $('.tab-content .tab-pane.fade.simpleForex').addClass('active in');
            } else if ($(this).hasClass('myTrades-simplex')) {
                $('.form-group.orders-wrapper').hide();
                $("select[data-widget='myTradesGameFilter']").append($("<option></option>").attr("value",value='simplex').text(value));
                $("select[data-widget='myTradesGameFilter']").val('simplex');
                $('[data-widget=myTradesSearchButtonNew]').data('preselectedgame','simplex');
                $('.form-group.gameFilter, .form-group.positionIdFilter, #my-trades-tabs').hide();
                $('#gamesMenu-mobile-active-only li.myTrades-simplex').addClass('active');
                $('div[data-widget="myTradesExportButtonPDF"]').attr('data-gametype','forex');
                $('div[data-widget="myTradesExportButtonXLS"]').attr('data-gametype','forex');
                $('.tab-content .tab-pane').removeClass('active in');
                $('.tab-content .tab-pane.fade.simplex').addClass('active in');
            }  else if ($(this).hasClass('myTrades-realForex')) {
                $('.form-group.orders-wrapper').hide();
                $("select[data-widget='myTradesGameFilter']").append($("<option></option>").attr("value",value='realForex').text(value));
                $("select[data-widget='myTradesGameFilter']").val('realForex');
                $('[data-widget=myTradesSearchButtonNew]').data('preselectedgame','realForex');
                $('.form-group.gameFilter, #searchBarInfo').hide();
                $('.form-group.positionIdFilter, #my-trades-tabs, #summaryResults').show();
                $('#gamesMenu-mobile-active-only li.myTrades-realForex').addClass('active');
                $('div[data-widget="myTradesExportButtonPDF"]').attr('data-gametype','realforex');
                $('div[data-widget="myTradesExportButtonXLS"]').attr('data-gametype','realforex');
                $('.tab-content .tab-pane').removeClass('active in');
                $('.tab-content .tab-pane.fade.realForex').addClass('active in');
            }

            if ($('[data-widget=myTradesFromDate]').datepicker('getDate') != null && $('[data-widget=myTradesToDate]').datepicker('getDate') != null) {
                $('#myTradesSearchButton').click();
            }
        });


        if($('.myTrades-table-navigation  ul  li:first-of-type').hasClass('myTrades-binary')) {
            $('.form-group.gameFilter').show();
            $("select[data-widget='myTradesGameFilter'] option[value='ticks']").css('display', 'none');
            $("select[data-widget='myTradesGameFilter'] option[value='forex']").css('display', 'none');
            $("select[data-widget='myTradesGameFilter'] option[value='knockin']").css('display', 'none');
            $("select[data-widget='myTradesGameFilter'] option[value='oneTouch']").css('display', 'none');
            $("select[data-widget='myTradesGameFilter'] option[value='binaryExchange']").css('display', 'none');
            $("select[data-widget='myTradesGameFilter'] option[value='simplex']").css('display', 'none');
            $("select[data-widget='myTradesGameFilter'] option[value='realForex']").css('display', 'none');
            $('.tab-content .tab-pane.fade.binary').addClass('active in');
        }

        $('.myTrades-table-navigation ul li:first-of-type').click();

        $("select[data-widget='myTradesGameFilter']").on('change', function () {
            $('[data-widget=myTradesSearchButtonNew]').attr('data-preselectedgame',$(this).val());
        });

        //binary table settings
        $('.tab-pane.fade.binary .multiple-selection-content .item').on('click', function (event) {
            var columnIndex = $(this).attr('data-column');
            event.stopPropagation();

            if(columnIndex==3 || columnIndex==4 || columnIndex==9) return;
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
                if (widgets.myTradesTableNewBinaryColumns){
                    widgets.myTradesTableNewBinaryColumns[columnIndex] = false;
                }
                if(widgets.isMobileDevice()){
                    $('.tab-pane.fade.binary .dataTable thead th:nth-child('+(columnIndex)+'), .tab-pane.fade.binary .dataTable tbody div:nth-child('+(columnIndex)+')').hide();
                } else {
                    $('.tab-pane.fade.binary .dataTable thead th:nth-child('+(columnIndex)+'), .tab-pane.fade.binary .dataTable tbody td:nth-child('+(columnIndex)+')').hide();
                }
            } else {
                $(this).addClass('selected');
                if (widgets.myTradesTableNewBinaryColumns){
                    widgets.myTradesTableNewBinaryColumns[columnIndex] = true;
                }
                if(widgets.isMobileDevice()) {
                    $('.tab-pane.fade.binary .dataTable thead th:nth-child('+(columnIndex)+'), .tab-pane.fade.binary .dataTable tbody div:nth-child('+(columnIndex)+')').show();
                } else {
                    $('.tab-pane.fade.binary .dataTable thead th:nth-child('+(columnIndex)+'), .tab-pane.fade.binary .dataTable tbody td:nth-child('+(columnIndex)+')').show();
                }
            };

            // set cookie
            if ($.cookie('myTradesTableNewBinaryColumns')){
                $.cookie('myTradesTableNewBinaryColumns',JSON.stringify(widgets.myTradesTableNewBinaryColumns));
            } else {
                $.cookie('myTradesTableNewBinaryColumns',JSON.stringify(widgets.myTradesTableNewBinaryColumns), {path: "/"});
            }
        });

        //defaultSettings btn on the binary  table
        $('.tab-pane.fade.binary .defaultSettings').on('click', function (event) {

            $('.tab-pane.fade.binary .multiple-selection-content .item').addClass('selected');
            $.each(myTradesTableNewBinaryColumnsDefault, function (i, isVisible) {
                if (isVisible === true){
                    //hide header column
                    $('.tab-pane.fade.binary .dataTable th:nth-child(' + i +')').show();
                    //hide rows
                    if(widgets.isMobileDevice()) {
                        $('.tab-pane.fade.binary .dataTable div:nth-child('+ i +')').show();
                    } else {
                        $('.tab-pane.fade.binary .dataTable td:nth-child('+ i +')').show();
                    }
                }
                else{
                    //hide header column
                    $('.tab-pane.fade.binary .dataTable th:nth-child(' + i +')').hide();
                    //hide rows
                    if(widgets.isMobileDevice()) {
                        $('.tab-pane.fade.binary .dataTable div:nth-child('+ i +')').hide();
                    } else {
                        $('.tab-pane.fade.binary .dataTable td:nth-child('+ i +')').hide();
                    }

                    $('.tab-pane.fade.binary .multiple-selection-content .item:nth-child('+ i +')').removeClass('selected');
                }
            });
            widgets.myTradesTableNewBinaryColumns = $.extend(true, {}, myTradesTableNewBinaryColumnsDefault);
            if($.cookie('myTradesTableNewBinaryColumns')){
                $.cookie('myTradesTableNewBinaryColumns',JSON.stringify(widgets.myTradesTableNewBinaryColumns));
            }else{
                $.cookie('myTradesTableNewBinaryColumns',JSON.stringify(widgets.myTradesTableNewBinaryColumns), {path: "/"});
            }
            $('.page-my-trades .ui-widget-overlay').hide();
        });

        //exchange table settings
        $('.tab-pane.fade.exchange .multiple-selection-content .item').on('click', function (event) {
            var columnIndex = $(this).attr('data-column');
            event.stopPropagation();

            if(columnIndex==3 || columnIndex==6 || columnIndex==11) return;
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
                if (widgets.myTradesTableBinaryExchangeNewColumns){
                    widgets.myTradesTableBinaryExchangeNewColumns[columnIndex] = false;
                }
                if(widgets.isMobileDevice()){
                    $('.tab-pane.fade.exchange .dataTable thead th:nth-child('+(columnIndex)+'), .tab-pane.fade.exchange .dataTable tbody div:nth-child('+(columnIndex)+')').hide();
                } else {
                    $('.tab-pane.fade.exchange .dataTable thead th:nth-child('+(columnIndex)+'), .tab-pane.fade.exchange .dataTable tbody td:nth-child('+(columnIndex)+')').hide();
                }

            } else {
                $(this).addClass('selected');
                if (widgets.myTradesTableBinaryExchangeNewColumns){
                    widgets.myTradesTableBinaryExchangeNewColumns[columnIndex] = true;
                }
                if(widgets.isMobileDevice()){
                    $('.tab-pane.fade.exchange .dataTable thead th:nth-child('+(columnIndex)+'), .tab-pane.fade.exchange .dataTable tbody div:nth-child('+(columnIndex)+')').show();
                } else {
                    $('.tab-pane.fade.exchange .dataTable thead th:nth-child('+(columnIndex)+'), .tab-pane.fade.exchange .dataTable tbody td:nth-child('+(columnIndex)+')').show();
                }

            };

            // set cookie
            if($.cookie('myTradesTableBinaryExchangeNewColumns')){
                $.cookie('myTradesTableBinaryExchangeNewColumns',JSON.stringify(widgets.myTradesTableBinaryExchangeNewColumns));
            }else{
                $.cookie('myTradesTableBinaryExchangeNewColumns',JSON.stringify(widgets.myTradesTableBinaryExchangeNewColumns), {path: "/"});
            }
        });

        //defaultSettings btn on the exchange  table
        $('.tab-pane.fade.exchange .defaultSettings').on('click', function (event) {

            $('.tab-pane.fade.exchange .multiple-selection-content .item').addClass('selected');
            $.each(myTradesTableBinaryExchangeNewColumnsDefault, function (i, isVisible) {
                if (isVisible === true){
                    //hide header column
                    $(".tab-pane.fade.exchange .dataTable th:nth-child(" + i +")").show();
                    //hide rows
                    if(widgets.isMobileDevice()) {
                        $(".tab-pane.fade.exchange .dataTable div:nth-child("+ i +")").show();
                    } else {
                        $(".tab-pane.fade.exchange .dataTable td:nth-child("+ i +")").show();
                    }
                }
                else{
                    //hide header column
                    $(".tab-pane.fade.exchange .dataTable th:nth-child(" + i +")").hide();
                    //hide rows
                    if(widgets.isMobileDevice()) {
                        $(".tab-pane.fade.exchange .dataTable div:nth-child("+ i +")").hide();
                    } else {
                        $(".tab-pane.fade.exchange .dataTable td:nth-child("+ i +")").hide();
                    }
                    $(".tab-pane.fade.exchange .multiple-selection-content .item:nth-child("+ i +")").removeClass('selected');
                }
            });
            widgets.myTradesTableBinaryExchangeNewColumns = $.extend(true, {}, myTradesTableBinaryExchangeNewColumnsDefault);
            if($.cookie('myTradesTableBinaryExchangeNewColumns')){
                $.cookie('myTradesTableBinaryExchangeNewColumns',JSON.stringify(widgets.myTradesTableBinaryExchangeNewColumns));
            }else{
                $.cookie('myTradesTableBinaryExchangeNewColumns',JSON.stringify(widgets.myTradesTableBinaryExchangeNewColumns), {path: "/"});
            }
            $('.page-my-trades .ui-widget-overlay').hide();
        });

        //simpleForex table settings
        $('.tab-pane.fade.simpleForex .multiple-selection-content .item').on('click', function (event) {
            var columnIndex = $(this).attr('data-column');
            event.stopPropagation();

            if(columnIndex==1 || columnIndex==2 || columnIndex==12 ) return;
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
                if (widgets.myTradesTableSimpleForexNewColumns){
                    widgets.myTradesTableSimpleForexNewColumns[columnIndex] = false;
                }
                if(widgets.isMobileDevice()){
                    $('.tab-pane.fade.simpleForex .dataTable thead th:nth-child('+(columnIndex)+'), .tab-pane.fade.simpleForex .dataTable tbody div:nth-child('+(columnIndex)+')').hide();
                } else {
                    $('.tab-pane.fade.simpleForex .dataTable thead th:nth-child('+(columnIndex)+'), .tab-pane.fade.simpleForex .dataTable tbody td:nth-child('+(columnIndex)+')').hide();
                }
            } else {
                $(this).addClass('selected');
                if (widgets.myTradesTableSimpleForexNewColumns){
                    widgets.myTradesTableSimpleForexNewColumns[columnIndex] = true;
                }
                if(widgets.isMobileDevice()){
                    $('.tab-pane.fade.simpleForex .dataTable thead th:nth-child('+(columnIndex)+'), .tab-pane.fade.simpleForex .dataTable tbody div:nth-child('+(columnIndex)+')').show();
                } else {
                    $('.tab-pane.fade.simpleForex .dataTable thead th:nth-child('+(columnIndex)+'), .tab-pane.fade.simpleForex .dataTable tbody td:nth-child('+(columnIndex)+')').show();
                }
            };

            // set cookie
            if($.cookie('myTradesTableSimpleForexNewColumns')){
                $.cookie('myTradesTableSimpleForexNewColumns',JSON.stringify(widgets.myTradesTableSimpleForexNewColumns));
            }else{
                $.cookie('myTradesTableSimpleForexNewColumns',JSON.stringify(widgets.myTradesTableSimpleForexNewColumns), {path: "/"});
            }
        });

        //defaultSettings btn on the simpleForex  table
        $('.tab-pane.fade.simpleForex .defaultSettings').on('click', function (event) {

            $('.tab-pane.fade.simpleForex .multiple-selection-content .item').addClass('selected');
            $.each(myTradesTableSimpleForexNewColumnsDefault, function (i, isVisible) {
                if (isVisible === true){
                    //hide header column
                    $(".tab-pane.fade.simpleForex .dataTable th:nth-child(" + i +")").show();
                    //hide rows
                    if(widgets.isMobileDevice()){
                        $(".tab-pane.fade.simpleForex .dataTable div:nth-child("+ i +")").show();
                    } else {
                        $(".tab-pane.fade.simpleForex .dataTable td:nth-child("+ i +")").show();
                    }
                }
                else{
                    //hide header column
                    $(".tab-pane.fade.simpleForex .dataTable th:nth-child(" + i +")").hide();
                    //hide rows
                    if(widgets.isMobileDevice()){
                        $(".tab-pane.fade.simpleForex .dataTable div:nth-child("+ i +")").hide();
                    } else {
                        $(".tab-pane.fade.simpleForex .dataTable td:nth-child("+ i +")").hide();
                    }
                    $(".tab-pane.fade.simpleForex .multiple-selection-content .item:nth-child("+ i +")").removeClass('selected');
                }
            });
            widgets.myTradesTableSimpleForexNewColumns = $.extend(true, {}, myTradesTableSimpleForexNewColumnsDefault);
            if($.cookie('myTradesTableSimpleForexNewColumns')){
                $.cookie('myTradesTableSimpleForexNewColumns',JSON.stringify(widgets.myTradesTableSimpleForexNewColumns));
            }else{
                $.cookie('myTradesTableSimpleForexNewColumns',JSON.stringify(widgets.myTradesTableSimpleForexNewColumns), {path: "/"});
            }
            $('.page-my-trades .ui-widget-overlay').hide();
        });

        //simplex table settings
        $('.tab-pane.fade.simplex .multiple-selection-content .item').on('click', function (event) {
            var columnIndex = $(this).attr('data-column');
            event.stopPropagation();

            if(columnIndex==3 || columnIndex==4 || columnIndex==14 ) return;
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
                if (widgets.myTradesTableSimplexNewColumns){
                    widgets.myTradesTableSimplexNewColumns[columnIndex] = false;
                }
                if(widgets.isMobileDevice()){
                    $('.tab-pane.fade.simplex .dataTable thead th:nth-child('+(columnIndex)+'), .tab-pane.fade.simplex .dataTable tbody div:nth-child('+(columnIndex)+')').hide();
                } else {
                    $('.tab-pane.fade.simplex .dataTable thead th:nth-child('+(columnIndex)+'), .tab-pane.fade.simplex .dataTable tbody td:nth-child('+(columnIndex)+')').hide();
                }
            } else {
                $(this).addClass('selected');
                if (widgets.myTradesTableSimplexNewColumns){
                    widgets.myTradesTableSimplexNewColumns[columnIndex] = true;
                }
                if(widgets.isMobileDevice()){
                    $('.tab-pane.fade.simplex .dataTable thead th:nth-child('+(columnIndex)+'), .tab-pane.fade.simplex .dataTable tbody div:nth-child('+(columnIndex)+')').show();
                } else {
                    $('.tab-pane.fade.simplex .dataTable thead th:nth-child('+(columnIndex)+'), .tab-pane.fade.simplex .dataTable tbody td:nth-child('+(columnIndex)+')').show();
                }
            };

            // set cookie
            if($.cookie('myTradesTableSimplexNewColumns')){
                $.cookie('myTradesTableSimplexNewColumns',JSON.stringify(widgets.myTradesTableSimplexNewColumns));
            }else{
                $.cookie('myTradesTableSimplexNewColumns',JSON.stringify(widgets.myTradesTableSimplexNewColumns), {path: "/"});
            }
        });

        //defaultSettings btn on the simplex  table
        $('.tab-pane.fade.simplex .defaultSettings').on('click', function (event) {

            $('.tab-pane.fade.simplex .multiple-selection-content .item').addClass('selected');
            $.each(myTradesTableSimplexNewColumnsDefault, function (i, isVisible) {
                if (isVisible === true){
                    //hide header column
                    $(".tab-pane.fade.simplex .dataTable th:nth-child(" + i +")").show();
                    //hide rows
                    if(widgets.isMobileDevice()){
                        $(".tab-pane.fade.simplex .dataTable div:nth-child("+ i +")").show();
                    } else {
                        $(".tab-pane.fade.simplex .dataTable td:nth-child("+ i +")").show();
                    }

                }
                else{
                    //hide header column
                    $(".tab-pane.fade.simplex .dataTable th:nth-child(" + i +")").hide();
                    //hide rows
                    if(widgets.isMobileDevice()){
                        $(".tab-pane.fade.simplex .dataTable div:nth-child("+ i +")").hide();
                    } else {
                        $(".tab-pane.fade.simplex .dataTable td:nth-child("+ i +")").hide();
                    }
                    $(".tab-pane.fade.simplex .multiple-selection-content .item:nth-child("+ i +")").removeClass('selected');
                }
            });
            widgets.myTradesTableSimplexNewColumns = $.extend(true, {}, myTradesTableSimplexNewColumnsDefault);
            if($.cookie('myTradesTableSimplexNewColumns')){
                $.cookie('myTradesTableSimplexNewColumns',JSON.stringify(widgets.myTradesTableSimplexNewColumns));
            }else{
                $.cookie('myTradesTableSimplexNewColumns',JSON.stringify(widgets.myTradesTableSimplexNewColumns), {path: "/"});
            }
            $('.page-my-trades .ui-widget-overlay').hide();
        });

        $(window).on('myTradesTableIsLoaded', function () {
            $.each(widgets.myTradesTableNewBinaryColumns, function (i, isVisible) {
                if (isVisible === false){
                    $(".tab-pane.fade.binary .dataTable thead th:nth-child(" + i +")").hide();
                    $(widgets.isMobileDevice() ? ".tab-pane.fade.binary .dataTable tbody div:nth-child("+ i +")" : ".tab-pane.fade.binary .dataTable tbody td:nth-child("+ i +")").hide();
                } else {
                    $(".tab-pane.fade.binary .dataTable thead th:nth-child(" + i +")").show();
                    $(widgets.isMobileDevice() ? ".tab-pane.fade.binary .dataTable tbody div:nth-child("+ i +")" : ".tab-pane.fade.binary .dataTable tbody td:nth-child("+ i +")").show();
                }
            });


            if( $('.tab-pane.fade.binary .multiple-selection-content .item').length>0){
                $('.tab-pane.fade.binary .multiple-selection-content .item').each(function (el) {
                    if(widgets.myTradesTableNewBinaryColumns[el+1]===true){
                        $(this).addClass('selected');
                    }else if(widgets.myTradesTableNewBinaryColumns[el+1]===false){
                        $(this).removeClass('selected');
                    }
                });
            };

            $.each(widgets.myTradesTableBinaryExchangeNewColumns, function (i, isVisible) {
                if (isVisible === false){
                    $(".tab-pane.fade.exchange .dataTable thead th:nth-child(" + i +")").hide();
                    $(widgets.isMobileDevice() ? ".tab-pane.fade.exchange .dataTable tbody div:nth-child("+ i +")" : ".tab-pane.fade.exchange .dataTable tbody td:nth-child("+ i +")").hide();
                } else {
                    $(".tab-pane.fade.exchange .dataTable thead th:nth-child(" + i +")").show();
                    $(widgets.isMobileDevice() ? ".tab-pane.fade.exchange .dataTable tbody div:nth-child("+ i +")" : ".tab-pane.fade.exchange .dataTable tbody td:nth-child("+ i +")").show();
                }
            });


            if( $('.tab-pane.fade.exchange .multiple-selection-content .item').length>0){
                $('.tab-pane.fade.exchange .multiple-selection-content .item').each(function (el) {
                    if(widgets.myTradesTableBinaryExchangeNewColumns[el+1]===true){
                        $(this).addClass('selected');
                    }else if(widgets.myTradesTableBinaryExchangeNewColumns[el+1]===false){
                        $(this).removeClass('selected');
                    }
                });
            };

            $.each(widgets.myTradesTableSimpleForexNewColumns, function (i, isVisible) {
                if (isVisible === false){
                    $(".tab-pane.fade.simpleForex .dataTable thead th:nth-child(" + i +")").hide();
                    $(widgets.isMobileDevice() ? ".tab-pane.fade.simpleForex .dataTable tbody div:nth-child("+ i +")" : ".tab-pane.fade.simpleForex .dataTable tbody td:nth-child("+ i +")").hide();
                } else {
                    $(".tab-pane.fade.simpleForex .dataTable thead th:nth-child(" + i +")").show();
                    $(widgets.isMobileDevice() ? ".tab-pane.fade.simpleForex .dataTable tbody div:nth-child("+ i +")" : ".tab-pane.fade.simpleForex .dataTable tbody td:nth-child("+ i +")").show();
                }
            });


            if( $('.tab-pane.fade.simpleForex .multiple-selection-content .item').length>0){
                $('.tab-pane.fade.simpleForex .multiple-selection-content .item').each(function (el) {
                    if(widgets.myTradesTableSimpleForexNewColumns[el+1]===true){
                        $(this).addClass('selected');
                    }else if(widgets.myTradesTableSimpleForexNewColumns[el+1]===false){
                        $(this).removeClass('selected');
                    }
                });
            };

            $.each(widgets.myTradesTableSimplexNewColumns, function (i, isVisible) {
                if (isVisible === false){
                    $(".tab-pane.fade.simplex .dataTable thead th:nth-child(" + i +")").hide();
                    $(widgets.isMobileDevice() ? ".tab-pane.fade.simplex .dataTable tbody div:nth-child("+ i +")" : ".tab-pane.fade.simplex .dataTable tbody td:nth-child("+ i +")").hide();
                } else {
                    $(".tab-pane.fade.simplex .dataTable thead th:nth-child(" + i +")").show();
                    $(widgets.isMobileDevice() ? ".tab-pane.fade.simplex .dataTable tbody div:nth-child("+ i +")" : ".tab-pane.fade.simplex .dataTable tbody td:nth-child("+ i +")").show();
                }
            });


            if( $('.tab-pane.fade.simplex .multiple-selection-content .item').length>0){
                $('.tab-pane.fade.simplex .multiple-selection-content .item').each(function (el) {
                    if(widgets.myTradesTableSimplexNewColumns[el+1]===true){
                        $(this).addClass('selected');
                    }else if(widgets.myTradesTableSimplexNewColumns[el+1]===false){
                        $(this).removeClass('selected');
                    }
                });
            };
        });


        if($(document).width() < 768 && $(document).width() > 325 ){
            $(window).scroll(function(){
                if($(this).scrollTop() >= 50){
                    $( "#gamesMenu li" ).each(function () {
                        $(this).slideUp( "slow");
                    });
                } else {
                    $( "#gamesMenu li" ).each(function () {
                        $(this).show("slow");
                    });
                }
                if ($(".column-settings").hasClass('show')) {
                    $(".column-settings").removeClass('show');
                    $('.ui-widget-overlay').hide();
                }
            });
        }
        // for iphone 5
        if($(document).width() < 325 ){
            $(window).scroll(function(){
                if($(this).scrollTop() >= 100){
                    $( "#gamesMenu li" ).each(function () {
                        $(this).slideUp( "slow");
                    });
                } else {
                    $( "#gamesMenu li" ).each(function () {
                        $(this).show("slow");
                    });
                }
                if ($(".column-settings").hasClass('show')) {
                    $(".column-settings").removeClass('show');
                    $('.ui-widget-overlay').hide();
                }
            });
        }

        setTimeout(function () {
            myTradesTableNewWidget.elementsCache.simplexTable.find('[data-type="btn-reinvest"]').unbind('click').bind('click', function (e) {
                e.preventDefault();
                window.location = getPathLanguagePrefix() + getTraderoomLink(traderoomMode.easyForex) + "&reinvestorderid=" + $(this).attr('data-orderid');
            });
        }, 1000);
        
        setTimeout(function () {
            $('#myTradesSearchButton').click();
        }, 500);
    });
</script>

<div id="tol-myTrades-layout-9">
    <div class="container-fluid no-padding-all">
        <div class="row myTrades-header-part">
            <h1 class="entry-title" data-translate="userDataMyTrades"></h1>
            <div class="col-xs-12 col-sm-3" id="totalProfit" style="visibility: hidden">
                <label data-translate="Trades_Profit"></label>
                <span data-widget="myTradesTableNew" data-type="myTradesTotalProfit" class="form-control"></span>
            </div>
        </div>
        <div class="mobile-menu-container" style="">
            <div data-widget="myTradesTableNew" data-type="gamesMenu" id="gamesMenu-mobile-active-only" class="myTrades-table-navigation" style="display: block;">
                <ul class="nav">
                    <?php if (ENABLE_DIGITAL || ENABLE_RANGE || ENABLE_TURBOBINARY || ENABLE_TOUCH || ENABLE_ADVANCED) { ?>
                        <li class="myTrades-binary" data-type="tab" data-game="Binary"><a href="#tab1" data-toggle="tab">Binary</a></li>
                    <?php } if (ENABLE_BINARYEXCHANGE) { ?>
                        <li class="myTrades-exchange" data-type="tab" data-game="Exchange"><a href="#tab2" data-toggle="tab">Exchange</a></li>
                    <?php } if (ENABLE_FOREX) { ?>
                        <li class="myTrades-simpleForex" data-type="tab" data-game="SimpleForex"><a href="#tab3" data-toggle="tab">Simple Forex</a></li>
                    <?php } if (ENABLE_SIMPLEX) { ?>
                        <li class="myTrades-simplex" data-type="tab" data-game="EasyForex"><a href="#tab4" data-toggle="tab" data-translate="game-name-easy"></a></li>
                    <?php } if (ENABLE_FOREXHA) { ?>
                        <li class="myTrades-realForex" data-type="tab" data-game="RealForex"><a href="#tab5" data-toggle="tab" data-translate="game-name-real"></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="navigation-container">
            <div data-widget="myTradesTableNew" data-type="gamesMenu" id="gamesMenu" class="myTrades-table-navigation post" style="display: block;">
                <ul class="nav">
                    <?php if (ENABLE_DIGITAL || ENABLE_RANGE || ENABLE_TURBOBINARY || ENABLE_TOUCH || ENABLE_ADVANCED) { ?>
                        <li class="myTrades-binary" data-type="tab" data-game="Binary"><a href="#tab1" data-toggle="tab">Binary</a></li>
                    <?php } if (ENABLE_BINARYEXCHANGE) { ?>
                        <li class="myTrades-exchange" data-type="tab" data-game="Exchange"><a href="#tab2" data-toggle="tab">Exchange</a></li>
                    <?php } if (ENABLE_FOREX) { ?>
                        <li class="myTrades-simpleForex" data-type="tab" data-game="SimpleForex"><a href="#tab3" data-toggle="tab">Simple Forex</a></li>
                    <?php } if (ENABLE_SIMPLEX) { ?>
                        <li class="myTrades-simplex" data-type="tab" data-game="EasyForex"><a href="#tab4" data-toggle="tab" data-translate="game-name-easy"></a></li>
                    <?php } if (ENABLE_FOREXHA) { ?>
                        <li class="myTrades-realForex" data-type="tab" data-game="RealForex"><a href="#tab5" data-toggle="tab" data-translate="game-name-real"></a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="search-bar">
                <div class="row">
                    <div data-widget="myTradesTableNew" data-type="forexTabsWrapper" id="my-trades-tabs" class="col-sm-12" style="display: none;">
                        <div class="col-xs-4 col-sm-4">
                            <div data-widget="myTradesTableNew" data-type="forexTabs" data-action="results" class="mytrades-tabs active" data-translate="results"></div>
                        </div>
                        <div class="col-xs-4 col-sm-4">
                            <div data-widget="myTradesTableNew" data-type="forexTabs" data-action="orders" class="mytrades-tabs" data-translate="exchangeOrders"></div>
                        </div>
                        <div class="col-xs-4 col-sm-4">
                            <div data-widget="myTradesTableNew" data-type="forexTabs" data-action="closed" class="mytrades-tabs" data-translate="ClosedPositions"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div id="searchBarInfo">
                        <div class="form-group search-bar-el col-xs-12 col-sm-3">
                            <div data-translate="Trades_Total" class="search-label"></div>
                            <div data-widget="myTradesTotalTrades" class="form-control trades"></div>
                        </div>
                        <div class="form-group search-bar-el col-xs-12 col-sm-3">
                            <div data-translate="Trades_Profit" class="search-label"></div>
                            <div data-widget="myTradesProfit" class="form-control profit"></div>
                        </div>
                    </div>
                    <div id="summaryResults" data-widget="myTradesTableNew" data-type="summaryResults"></div>
                </div>
                <div class="row" id="searchBarControls">
                    <div class="form-group search-bar-el col-xs-12 col-sm-2 orders-wrapper">
                        <label data-translate="forexTable_status"></label>
                        <select id="orderStatus" class="form-control btn-default">
                            <option value="" data-translate="All"></option>
                            <option value="TradePending" data-translate="Pending"></option>
                            <option value="Filled" data-translate="Filled"></option>
                            <option value="TradeCancelled" data-translate="Cancelled"></option>
                            <option value="Modified" data-translate="Modified"></option>
                        </select>
                    </div>
                    <div class="form-group search-bar-el col-xs-12 col-sm-2 assets">
                        <label data-translate="TradeRoom_Assets"></label>
                        <?php echo do_shortcode('[myTradesOptions class="form-control btn-default form-control-md searchBarAssets"]'); ?>
                    </div>
                    <div class="form-group search-bar-el col-xs-6 col-sm-2 fromDate">
                        <label data-translate="MyOptions_From"></label>
                        <?php echo do_shortcode('[myTradesFromDate  data-past="30" class="form-control nopadding-right"]'); ?>
                    </div>
                    <div class="form-group search-bar-el col-xs-6 col-sm-2 toDate">
                        <label data-translate="MyOptions_To"></label>
                        <?php echo do_shortcode('[myTradesToDate class="form-control nopadding-right"]'); ?>
                    </div>
                    <div class="row chooseDateRowMobile"></div>
                    <div class="form-group search-bar-el col-xs-6 col-sm-2 gameFilter" style="display: none;">
                        <label data-translate="MyOptions_Type"></label>
                        <select data-widget="myTradesGameFilter" id="gameFilter" class="form-control btn-default form-control-md"></select>
                    </div>
                    <div class="form-group search-bar-el col-xs-6 col-sm-2 positionIdFilter" style="display: none;">
                        <label data-translate="positionId"></label>
                        <input data-widget="myTradesTableNew" data-type="positionIdFilter" type="text" id="positionIdFilter" class="form-control btn-default form-control-md" />
                    </div>
                    <div class="form-group search-bar-el col-xs-6 col-sm-2 orderId-container" style="display: none;">
                        <label data-translate="MyOptions_OrderID"></label>
                        <input data-widget="myTradesTableNew" data-type="orderId" type="text" id="orderId" class="form-control btn-default form-control-md" />
                    </div>
                    <div id="exportButtons-mobile"></div>
                    <?php echo do_shortcode('[myTradesSearchButtonNew translate="MyTrades_SearchBtn" class="btn btn-primary col-sm-2 col-xs-12"]'); ?>

                </div>
            </div>
        </div>
        <div class="main-container">
            <div class="tab-content">
                <!-- tab1 start -->
                <div class="tab-pane fade binary" id="tab1" >
                    <div class="myTrades-tables-content">
                        <div class="tabHolder">
                            <span class="column-settings taborders">
                                <div class="settings"></div>
                                <div class="contextmenu multiple-selection datatable-contextmenu _focusable leftCenter" tabindex="-1">
                                    <div class="multiple-selection-content">
                                        <div class="item item-datatable-contextmenu-orderID selected" data-column="1">
                                            <span data-translate="MyOptions_OrderID"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-created selected" data-column="2">
                                             <span data-translate="orderDate"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-name selected" data-column="3">
                                            <span data-translate="asset"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                         <div class="item item-datatable-contextmenu-action selected" data-column="4">
                                          <span data-translate="action"></span>
                                             <div class="list-checkbox"></div>
                                         </div>
                                         <div class="item item-datatable-contextmenu-rate selected" data-column="5">
                                           <span data-translate="MyOptions_TradeRate"></span>
                                           <div class="list-checkbox"></div>
                                         </div>
                                         <div class="item item-datatable-contextmenu-invested selected" data-column="6">
                                            <span data-translate="withdrawTableAmount"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                         <div class="item item-datatable-contextmenu-expired selected" data-column="7">
                                           <span data-translate="Expiry_Date"></span>
                                           <div class="list-checkbox"></div>
                                        </div>
                                         <div class="item item-datatable-contextmenu-expiryRate selected" data-column="8">
                                            <span data-translate="MyTrades_closeRate"></span>
                                            <div class="list-checkbox"></div>
                                         </div>
                                         <div class="item item-datatable-contextmenu-profit selected" data-column="9">
                                            <span data-translate="profit"></span>
                                           <div class="list-checkbox"></div>
                                        </div>
                                         <div class="btn-wrapper navigation">
                                           <div class="nav-button defaultSettings" data-translate="defaultSettings"></div>
                                         </div>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div role="tabpanel">
                            <div class="dataTable">
                                <table class="fake-table">
                                    <thead>
                                    <tr>
                                        <th data-column="id" data-sort="string" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="MyOptions_OrderID"></span></div>
                                        </th>
                                        <th data-column="created" data-sort="int" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="orderDate"></span></div>
                                        </th>
                                        <th data-column="name" data-sort="string" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="asset"></span></div>
                                        </th>
                                        <th data-column="action" data-sort="float" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="action"></span></div>
                                        </th>
                                        <th data-column="rate" data-sort="float" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="MyOptions_TradeRate"></span></div>
                                        </th>
                                        <th data-column="invested" data-sort="float" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="withdrawTableAmount"></span></div>
                                        </th>
                                        <th data-column="expired" data-sort="float" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="Expiry_Date"></span></div>
                                        </th>
                                        <th data-column="expiryRate" data-sort="float" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="MyTrades_closeRate"></span></div>
                                        </th>
                                        <th data-column="profit" data-sort="float" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="profit"></span></div>
                                        </th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="dataTable hasArrows">
                                <?php echo do_shortcode('[myTradesTableNewBinary]'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- tab1 end -->
                <!-- tab2 start -->
                <div class="tab-pane fade exchange" id="tab2" >
                    <div class="myTrades-tables-content">
                        <div class="tabHolder">
                            <span class="column-settings taborders">
                                <div class="settings"></div>
                                <div class="contextmenu multiple-selection datatable-contextmenu _focusable leftCenter" tabindex="-1">
                                    <div class="multiple-selection-content">
                                        <div class="item item-datatable-contextmenu-tradeId selected" data-column="1">
                                            <span data-translate="tradeID"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-timestamp selected" data-column="2">
                                            <span data-translate="mytrades-timestamp"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-asset selected" data-column="3">
                                            <span data-translate="exchangeOrders-asset"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-strike selected" data-column="4">
                                            <span data-translate="exchangeOrders-strike"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-expiry selected" data-column="5">
                                            <span data-translate="exchangeOrders-expiry"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-direction selected" data-column="6">
                                            <span data-translate="exchangeOrders-direction"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-lots selected" data-column="7">
                                            <span data-translate="exchangeOrders-lots"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-limitPrice selected" data-column="8">
                                            <span data-translate="exchangeOrders-price"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-rate selected" data-column="9">
                                            <span data-translate="rate"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-fee selected" data-column="10">
                                            <span data-translate="exchangeOrders-fee"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-pl selected" data-column="11">
                                             <span data-translate="exchangeOrders-pl"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="btn-wrapper navigation">
                                            <div class="nav-button defaultSettings" data-translate="defaultSettings"></div>
                                        </div>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div role="tabpanel">
                            <div class="dataTable">
                                <table class="fake-table">
                                    <thead>
                                    <tr>
                                        <th data-column="tradeId" data-sort="string" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="tradeID"></span></div>
                                        </th>
                                        <th data-column="timestamp" data-sort="int" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="mytrades-timestamp"></span></div>
                                        </th>
                                        <th data-column="asset" data-sort="string" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="exchangeOrders-asset"></span></div>
                                        </th>
                                        <th data-column="strike" data-sort="float" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="exchangeOrders-strike"></span></div>
                                        </th>
                                        <th data-column="expiry" data-sort="float" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="exchangeOrders-expiry"></span></div>
                                        </th>
                                        <th data-column="direction" data-sort="float" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="exchangeOrders-direction"></span></div>
                                        </th>
                                        <th data-column="lots" data-sort="float" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="exchangeOrders-lots"></span></div>
                                        </th>
                                        <th data-column="limitPrice" data-sort="float" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="exchangeOrders-price"></span></div>
                                        </th>
                                        <th data-column="rate" data-sort="float" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="rate"></span></div>
                                        </th>
                                        <th data-column="fee" data-sort="float" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="exchangeOrders-fee"></span></div>
                                        </th>
                                        <th data-column="pl" data-sort="float" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="exchangeOrders-pl"></span></div>
                                        </th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="dataTable hasArrows">
                                <?php echo do_shortcode('[myTradesTableBinaryExchangeNew]'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- tab2 end -->
                <!-- tab3 start -->
                <div class="tab-pane fade simpleForex" id="tab3" >
                    <div class="myTrades-tables-content">
                        <div class="tabHolder">
                            <span class="column-settings taborders">
                                <div class="settings"></div>
                                <div class="contextmenu multiple-selection datatable-contextmenu _focusable leftCenter" tabindex="-1">
                                    <div class="multiple-selection-content">
                                        <div class="item item-datatable-contextmenu-name selected" data-column="1">
                                            <span data-translate="asset"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-action selected" data-column="2">
                                            <span data-translate="type"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-created selected" data-column="3">
                                            <span data-translate="mytrades-openDate"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-rate selected" data-column="4">
                                            <span data-translate="tradeRate"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-invested selected" data-column="5">
                                            <span data-translate="invest_forex"></span>
                                             <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-leverage selected" data-column="6">
                                            <span data-translate="leverage"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-stopLoss" data-column="7">
                                            <span data-translate="stopLoss"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-takeProfit" data-column="8">
                                             <span data-translate="takeProfit"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-expiryRate selected" data-column="9">
                                            <span data-translate="MyTrades_closeRate"></span>
                                            <div class="list-checkbox"></div>
                                         </div>
                                         <div class="item item-datatable-contextmenu-expired selected" data-column="10">
                                            <span data-translate="forexTable_expired"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-swap" data-column="11">
                                            <span data-translate="forexSwap"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-profit selected" data-column="12">
                                            <span data-translate="profit"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="btn-wrapper navigation">
                                            <div class="nav-button defaultSettings" data-translate="defaultSettings"></div>
                                        </div>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div role="tabpanel">
                            <div class="dataTable">
                                <table class="fake-table">
                                    <thead>
                                    <tr>
                                        <th data-column="name" data-sort="string" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="asset"></span></div>
                                        </th>
                                        <th data-column="action" data-sort="int" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="type"></span></div>
                                        </th>
                                        <th data-column="created" data-sort="int" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="mytrades-openDate"></span></div>
                                        </th>
                                        <th data-column="rate" data-sort="float" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="tradeRate"></span></div>
                                        </th>
                                        <th data-column="invested" data-sort="float" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="invest_forex"></span></div>
                                        </th>
                                        <th data-column="leverage" data-sort="float" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="leverage"></span></div>
                                        </th>
                                        <th data-column="stopLoss" data-sort="float" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="stopLoss"></span></div>
                                        </th>
                                        <th data-column="takeProfit" data-sort="float" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="takeProfit"></span></div>
                                        </th>
                                        <th data-column="expiryRate" data-sort="float" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="MyTrades_closeRate"></span></div>
                                        </th>
                                        <th data-column="expired" data-sort="string" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="forexTable_expired"></span></div>
                                        </th>
                                        <th data-column="swap" data-sort="int" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="forexSwap"></span></div>
                                        </th>
                                        <th data-column="profit" data-sort="string" class="name canSort canResize canDrag">
                                            <div class="cellWrapper"><span class="content" data-translate="profit"></span></div>
                                        </th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="dataTable hasArrows">
                                <?php echo do_shortcode('[myTradesTableNewSimpleForex]'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- tab3 end -->
                <!-- tab4 start -->
                <div class="tab-pane fade simplex" id="tab4" >
                    <div class="myTrades-tables-content">
                        <div class="tabHolder">
			                <span class="column-settings taborders">
                                <div class="settings"></div>
                                <div class="contextmenu multiple-selection datatable-contextmenu _focusable leftCenter" tabindex="-1">
				                    <div class="multiple-selection-content">
						                <div class="item item-datatable-contextmenu-name selected" data-column="1">
							                <span data-translate="tradeID"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-name selected" data-column="2">
                                            <span data-translate="openTime"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-quantity selected" data-column="3">
                                            <span data-translate="asset"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-direction selected" data-column="4">
                                            <span data-translate="type"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-averagePrice selected" data-column="5">
                                            <span data-translate="openPrice"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-currentPrice selected" data-column="6">
                                            <span data-translate="investAmount"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-limitPrice selected" data-column="7">
                                            <span data-translate="risk"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-stopPrice" data-column="8">
                                            <span data-translate="stopLoss"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-margin" data-column="9">
                                            <span data-translate="takeProfit"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-swap selected" data-column="10">
                                            <span data-translate="closeTime"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-swap selected" data-column="11">
                                            <span data-translate="closePrice"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-created selected" data-column="12">
                                            <span data-translate="swap"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-created selected" data-column="13">
                                            <span data-translate="commissions"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-ppl" data-column="14">
                                            <span data-translate="return"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item item-datatable-contextmenu-ppl selected" data-column="15">
                                            <span data-translate="profit"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="btn-wrapper navigation">
                                            <div class="nav-button defaultSettings" data-translate="defaultSettings"></div>
                                        </div>
				                    </div>
				                </div>
			                </span>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div role="tabpanel">
                            <div class="dataTable">
                                <table class="fake-table">
                                    <thead>
                                        <tr>
                                            <th data-column="tradeID" data-sort="int" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="tradeID"></span></div>
                                            </th>
                                            <th data-column="openTime" data-sort="int" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="openTime"></span></div>
                                            </th>
                                            <th data-column="asset" data-sort="string" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="asset"></span></div>
                                            </th>
                                            <th data-column="type" data-sort="float" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="type"></span></div>
                                            </th>
                                            <th data-column="openPrice" data-sort="float" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="openPrice"></span></div>
                                            </th>
                                            <th data-column="investAmount" data-sort="float" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="investAmount"></span></div>
                                            </th>
                                            <th data-column="risk" data-sort="float" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="risk"></span></div>
                                            </th>
                                            <th data-column="stopLoss" data-sort="float" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="stopLoss"></span></div>
                                            </th>
                                            <th data-column="takeProfit" data-sort="float" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="takeProfit"></span></div>
                                            </th>
                                            <th data-column="closeTime" data-sort="float" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="closeTime"></span></div>
                                            </th>
                                            <th data-column="closePrice" data-sort="float" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="closePrice"></span></div>
                                            </th>
                                            <th data-column="swap" data-sort="float" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="swap"></span></div>
                                            </th>
                                            <th data-column="commissions" data-sort="float" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="commissions"></span></div>
                                            </th>
                                            <th data-column="return" data-sort="string" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="return"></span></div>
                                            </th>
                                            <th data-column="profit" data-sort="string" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" data-translate="profit"></span></div>
                                            </th>
                                            <th data-column="actions" data-sort="string" class="name canSort canResize canDrag">
                                                <div class="cellWrapper"><span class="content" ></span></div>
                                            </th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="dataTable hasArrows">
                                <?php echo do_shortcode('[myTradesTableNewSimpex]'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- tab4 end -->
                <!-- tab5 start -->
                <div class="tab-pane fade realForex" id="tab5" >
                    <div class="myTrades-tables-content">
                        <div class="tabHolder">
                            <span class="column-settings taborders" data-widget="myTradesTableNew" data-type="forexResultsColCtrl">
                                <div class="settings"></div>
                                <div class="contextmenu multiple-selection datatable-contextmenu _focusable leftCenter" tabindex="-1">
                                    <div class="multiple-selection-content">
                                        <div class="item selected" name="position">
                                            <span data-translate="positionId"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item selected" name="volume">
                                            <span data-translate="forexTable_quantity"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item selected" name="averageRate">
                                            <span data-translate="forexTable_rate"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item selected" name="closeRate">
                                            <span data-translate="MyTrades_closeRate"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item selected" name="description">
                                            <span data-translate="description"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item selected" name="date">
                                            <span data-translate="date"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="btn-wrapper navigation">
                                            <div class="nav-button defaultSettings" data-translate="defaultSettings"></div>
                                        </div>
				                    </div>
				                </div>
			                </span>

                            <span class="column-settings taborders" data-widget="myTradesTableNew" data-type="forexOrdersColCtrl" style="display: none">
                                <div class="settings"></div>
                                <div class="contextmenu multiple-selection datatable-contextmenu _focusable leftCenter" tabindex="-1">
                                    <div class="multiple-selection-content">
                                        <div class="item selected" name="orderId">
                                            <span data-translate="orderId"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item selected" name="position">
                                            <span data-translate="positionId"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item selected" name="volume">
                                            <span data-translate="quantity"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item selected" name="type">
                                            <span data-translate="type"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item selected" name="orderRate">
                                            <span data-translate="MyTrades_orderRate"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item selected" name="execRate">
                                            <span data-translate="MyTrades_execRate"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item selected" name="date">
                                            <span data-translate="forexTable_created_orders"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item selected" name="status">
                                            <span data-translate="forexTable_status"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item" name="copiedFrom">
                                            <span data-translate="copiedFrom"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="item selected" name="modDate">
                                            <span data-translate="forexTable_modDate"></span>
                                            <div class="list-checkbox"></div>
                                        </div>
                                        <div class="btn-wrapper navigation">
                                            <div class="nav-button defaultSettings" data-translate="defaultSettings"></div>
                                        </div>
				                    </div>
				                </div>
			                </span>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div role="tabpanel">
                            <div class="dataTable hasArrows">
                                <table data-widget="myTradesTableNew" data-type="forexResults" data-enableemptyrows="true" data-rowsperpage="10">
                                    <thead>
                                    <tr>
                                        <th data-column="position" class="position">
                                            <div class="cellWrapper" data-translate="positionId"></div>
                                        </th>
                                        <th data-column="name" class="name">
                                            <div class="cellWrapper" data-translate="forexTable_asset"></div>
                                        </th>
                                        <th data-column="volume" class="volume">
                                            <div class="cellWrapper" data-translate="quantity"></div>
                                        </th>
                                        <th data-column="direction" class="direction">
                                            <div class="cellWrapper" data-translate="forexTable_direction"></div>
                                        </th>
                                        <th data-column="averageRate" class="averageRate">
                                            <div class="cellWrapper" data-translate="forexTable_avrate"></div>
                                        </th>
                                        <th data-column="closeRate" class="closeRate">
                                            <div class="cellWrapper" data-translate="MyTrades_closeRate"></div>
                                        </th>
                                        <th data-column="result" class="result">
                                            <div class="cellWrapper" data-translate="forexTable_result"></div>
                                        </th>
                                        <th data-column="description" class="description">
                                            <div class="cellWrapper" data-translate="forexTable_description"></div>
                                        </th>
                                        <th data-column="date" class="date">
                                            <div class="cellWrapper" data-translate="date"></div>
                                        </th>
                                    </tr>
                                    </thead>
                                </table>

                                <table data-widget="myTradesTableNew" data-type="forexOrders" style="display: none;">
                                    <thead>
                                    <tr>
                                        <th data-column="orderId" class="orderId">
                                            <div class="cellWrapper" data-translate="orderId"></div>
                                        </th>
                                        <th data-column="position" class="position">
                                            <div class="cellWrapper" data-translate="positionId"></div>
                                        </th>
                                        <th data-column="name" class="name">
                                            <div class="cellWrapper" data-translate="forexTable_asset"></div>
                                        </th>
                                        <th data-column="volume" class="volume">
                                            <div class="cellWrapper" data-translate="quantity"></div>
                                        </th>
                                        <th data-column="direction" class="direction">
                                            <div class="cellWrapper" data-translate="forexTable_direction"></div>
                                        </th>
                                        <th data-column="type" class="type">
                                            <div class="cellWrapper" data-translate="type"></div>
                                        </th>
                                        <th data-column="orderRate" class="orderRate">
                                            <div class="cellWrapper" data-translate="MyTrades_orderRate"></div>
                                        </th>
                                        <th data-column="execRate" class="execRate">
                                            <div class="cellWrapper" data-translate="MyTrades_execRate"></div>
                                        </th>
                                        <th data-column="date" class="date">
                                            <div class="cellWrapper" data-translate="forexTable_created_orders"></div>
                                        </th>
                                        <th data-column="status" class="status">
                                            <div class="cellWrapper" data-translate="forexTable_status"></div>
                                        </th>
                                        <th data-column="copiedFrom" class="copiedFrom">
                                            <div class="cellWrapper" data-translate="copiedFrom"></div>
                                        </th>
                                        <th data-column="modDate" class="modDate">
                                            <div class="cellWrapper" data-translate="forexTable_modDate"></div>
                                        </th>
                                    </tr>
                                    </thead>
                                </table>

                                <table data-widget="myTradesTableNew" data-type="forexClosed" style="display: none;">
                                    <thead>
                                    <tr>
                                        <th data-column="position" class="position">
                                            <div class="cellWrapper" data-translate="positionId"></div>
                                        </th>
                                        <th data-column="name" class="name">
                                            <div class="cellWrapper" data-translate="forexTable_asset"></div>
                                        </th>
                                        <th data-column="date" class="date">
                                            <div class="cellWrapper" data-translate="option_entry_date"></div>
                                        </th>
                                        <th data-column="closeDate" class="closeDate">
                                            <div class="cellWrapper" data-translate="closeDate"></div>
                                        </th>
                                        <th data-column="result" class="result">
                                            <div class="cellWrapper" data-translate="forexTable_result"></div>
                                        </th>
                                        <th data-column="swap" class="result">
                                            <div class="cellWrapper" data-translate="swap"></div>
                                        </th>
                                        <th data-column="commission" class="result">
                                            <div class="cellWrapper" data-translate="easy-commission"></div>
                                        </th>
                                        <th data-column="profit" class="result">
                                            <div class="cellWrapper" data-translate="profit"></div>
                                        </th>
                                        <th data-column="comment" class="result">
                                            <div class="cellWrapper" data-translate="Comment"></div>
                                        </th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- tab5 end -->
                <div class="exportButtons desktop-only">
                    <div data-widget="myTradesExportButtonPDF" data-gametype="" class="export-container" style="display: none;">
                        <div class="pdfButton"> </div>
                        <div data-translate="export-pdf"></div>
                    </div>
                    <div data-widget="myTradesExportButtonXLS" data-gametype="" class="export-container" style="display: none;">
                        <div class="xlsButton"> </div>
                        <div data-translate="export-xls"></div>
                    </div>
                </div>
                <?php echo do_shortcode('[myTradesPagingNew]'); ?>
            </div>
        </div>
    </div>
</div>
<!-- <div class="ui-widget-overlay"></div> -->
</div>

<?php
    get_footer();
?>
