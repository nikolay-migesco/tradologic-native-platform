<?php
/**
 *
 * Template Name: Economic Calendar Template
 *
 */
get_header();
?>

<div id="content" role="main">
    <div class="col-sm-10 col-sm-offset-1">
        <?php load_template(ABSPATH . 'wp-content/themes/tol-parent/layouts/ecoCalendar.php'); ?>
    </div>	
</div>

<?php
get_footer();
?>