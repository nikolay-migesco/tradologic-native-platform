<?php
/**
 * Template Name: News page
 */

get_header();

?>
    <div id="content" role="main">
        <div class="container">
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <header class="entry-header">
                    <h1 class="entry-title"><?php the_title(); ?></h1>
                </header>
                <div class="entry-content well">
                     <?php dynamic_sidebar('tol-traderoom-rss'); ?>
                </div>
            </article>
        </div>
    </div>
<?php
    get_footer();
?>
