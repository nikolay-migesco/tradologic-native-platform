<?php
/**
 *
 * Template Name: Right Sidebar InnerPages
 *
 */
get_header();
?>


<div id="content" role="main">
     <div class="pageHeadTitle col-sm-offset-1 no-padding-all">
        <?php require_once(ABSPATH.'wp-content/themes/tol-parent/layouts/'.BREADCRUMB.'.php' ); ?>
    </div>
    <header class="entry-header col-sm-offset-1 no-padding-all">
        <h1 class="entry-title blue-heading"><?php the_title(); ?></h1>
    </header>
  

    <div class="">
        <div class="container-fluid no-padding sidebarRight">
            <div class="col-sm-10 col-sm-offset-1 no-padding-all">  
                <div class="col-sm-8 no-padding-all">
                    <div class="entry-content">
                        <?php the_content(); ?>
                    </div><!-- .entry-content -->
                </div>

                <div class="col-sm-4 nopadding">
                    <?php load_template(ABSPATH.'wp-content/themes/tol-parent/layouts/sidebar-right.php' ); ?>
                </div> 

            </div>
        </div>

    </div>


</div>

<?php
get_footer();
?>

