<?php
/**
 *
 * Template Name: Trader Points
 *
 */

get_header(); 
//wp_enqueue_style('leaderboard-layout');

?>

<div id="content" role="main">
    <div class="traderpoints-warapper">
        <div data-widget="traderPoints" class="row traderpoints-table" data-points="1">
  
            <div class="traderpoints-menu">
                <div data-widget="traderPoints" data-type="menu" data-target="currencies" data-translate="Currencies" class="traderpoints-menu-currencies active"></div>
                <div data-widget="traderPoints" data-type="menu" data-target="indices" data-translate="Indices" class="traderpoints-menu-indices"></div>
                <div data-widget="traderPoints" data-type="menu" data-target="stocks" data-translate="Stocks" class="traderpoints-menu-stocks"></div>
                <div data-widget="traderPoints" data-type="menu" data-target="commodities" data-translate="Commodities" class="traderpoints-menu-commodities"></div>
            </div>
            <div class="traderpoints-info" data-translate="traderPointsInfo"></div>
            <div class="assettablewrapper">
                <div data-widget="traderPoints" data-type="section" data-item="currencies" data-popup-details="click" class="col-md-12 currencies"></div>
                <div data-widget="traderPoints" data-type="section" data-item="indices" data-popup-details="hover" class="col-md-12 indices" style="display: none"></div>
                <div data-widget="traderPoints" data-type="section" data-item="stocks" data-popup-details="slideToggle" class="col-md-12 stocks" style="display: none"></div>
                <div data-widget="traderPoints" data-type="section" data-item="commodities" data-popup-details="slideToggle" class="col-md-12 widgetPlaceholder commodities" style="display: none"></div>
            </div>
        </div>

    </div>
</div>

<?php
    get_footer();
?>
