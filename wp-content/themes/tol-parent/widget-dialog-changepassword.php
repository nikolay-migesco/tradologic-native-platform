<?php //if(TolWidgets::isUserLogged()):?>
<div class="change-wrap" style="display:none;">
    <div class="box_title"><span>Изменить пароль</span></div>
    <div id="change_form">
        <form>
            <div data-widget="changePassword" data-type="error" style="display: none" class="error-message"></div>
            <div class="form_input">
                <input type="password" placeholder="Текущий пароль" data-widget="changePassword"
                       data-type="currentPassword" data-isRequired="true"/>
            </div>
            <div class="form_input">
                <input type="password" placeholder="Новый пароль" data-widget="changePassword"
                       data-type="newPassword" data-isRequired="true"/>
            </div>
            <div class="pass_length">
                <div data-widget="changePassword" data-type="passwordStrength" class="passwordStrength"></div>
            </div>
            <div class="form_input">
                <input type="password" placeholder="Повторите новый пароль" data-widget="changePassword"
                       data-type="confirmPassword" data-isRequired="true"/>
            </div>
            <div><input type="button" value="Изменить пароль" data-widget="changePassword" data-type="submit"/>
            </div>
            <div class="profile-link"><a href="#myprofile">Личные данные</a></div>
        </form>
    </div>
    <div id="close_popup5"><span class="change-close"></span></div>
</div>

<script type="text/javascript">
    if (/(^|;)\s*widgetSession=/.test(document.cookie)) {
        $('.change-wrap input, .change-wrap select').attr("data-widget", "changePassword");
    } else {
        $('.change-wrap input, .change-wrap select').removeAttr("data-widget");
    }
</script>
<?php //endif;?>
