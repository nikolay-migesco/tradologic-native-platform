<?php //if(TolWidgets::isUserLogged()):?>
<!--<script type="text/javascript" src="/wp-content/themes/xptheme/userDetails.js"></script>-->
<div class="changephone-wrap" style="display:none;">
    <form>
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td width="50%" id="profile_col1" valign="top">
                    <div class="box_title"><span>Номер QIWI-кошелька</span></div>
                    <div class="change_text">Номер телефона должен соответствовать номеру QIWI-кошелька. Если телефон
                        указан неверно, исправьте его.<br/><br/>Пример: 71234567890
                    </div>
                    <div class="form_input">
                        <input type="text" placeholder="Телефон" data-widget="userDetails" data-type="phone"
                               data-regexpMessage="Это поле обязательно для заполнения"/>
                    </div>
                    <div class="form_input" style="height:77px !important;position:relative;"><input type="button"
                                                                                                     value="Продолжить"
                                                                                                     data-widget="userDetails"
                                                                                                     data-type="submit"/>
                        <div id="preload" style="display:none;"><img
                                    src="/wp-content/themes/tol-parent/images/xp-theme/load.gif" width="30" height="30">
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </form>
    <!-- <div id="close_popup5"><span class="changephone-close"></span></div> -->
</div>

<script type="text/javascript">
    if (/(^|;)\s*widgetSession=/.test(document.cookie)) {
        $('.changephone-wrap input, .changephone-wrap select').attr("data-widget", "userDetails");
    } else {
        $('.changephone-wrap input, .changephone-wrap select').removeAttr("data-widget");
    }


    $(document).ready(function () {

        $('.changephone-wrap input[data-type="phone"]').live("keyup", function (e) {
            $('.profile-wrap input[data-type="phone"]').val($(this).val());
        });

        $('div.changephone-wrap input').live("change", function (e) {
            if ($(this).attr('data-type') == 'phone') {
                $(this).val($(this).val().replace(/[^0-9\(\)\[\]\{\}\-\+"'@\#\$%\^&<>\,\?=_~\\/\|;:\ ]/gim, ''));
            }
        });

        $('div.changephone-wrap input').live("keypress", function (e) {
            if ($(this).attr('data-type') == 'phone') {
                if (String.fromCharCode(e.which).match(/[^0-9\(\)\[\]\{\}\-\+"'@\#\$%\^&<>\,\?=_~\\/\|;:\ ]/gim)) {
                    e.preventDefault();
                    //$(this).val($(this).val().replace(/[^0-9\(\)\[\]\{\}\-\+"'@\#\$%\^&<>\,\?=_~\\/\|;:\ ]/gim, ''));
                }
            }
        });

        $('.changephone-wrap input[data-type="submit"]').click(function () {
            $('#preload').show();
            setTimeout(function () {
                if ($('div.myprofile-successmsg').text() != '') {
                    $('div.myprofile-successmsg').css('display', 'inline-block');
                    setTimeout(function () {
                        $('.login-popup-bg, .changephone-wrap, #preload').hide();
                        $('.login-popup-bg').css('opacity', '0.5');
                        $('input[data-widget="cashierDeposit"][data-type="submit"]').click();
                        $('input#depositBTN').hide();
                    }, 2000);
                }
                if ($('.changephone-wrap div.validationError').length > 0) {
                    $('div.myprofile-successmsg').hide();
                }
            }, 1500);
        });
    });
</script>
<?php //endif;?>
