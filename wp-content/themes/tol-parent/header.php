<?php

$isLoadingMigescoLayout = isMigescoLayout();
if (!$isLoadingMigescoLayout && USE_WP_HEAD) {
    wp_head();
}
if (!$isLoadingMigescoLayout) {
    load_template(dirname(__FILE__) . '/layouts/' . HEADER_LAYOUT . '.php');
}
if ($isLoadingMigescoLayout) {
    load_template(dirname(__FILE__) . '/layouts/header-' . MIGESCO_LAYOUT . '.php');
}

