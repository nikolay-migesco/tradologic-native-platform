<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package TOL-Parent
 */
defineMigescoLayout();

get_header(); ?>

    <div id="primary" class="site-content">
        <div id="content" role="main">
            <?php if ( have_posts() ) : ?>

                <?php /* Start the Loop */ ?>
                <?php while ( have_posts() ) : the_post(); ?>

                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <header class="entry-header">
                            <h1 class="entry-title"><?php the_title(); ?></h1>
                        </header>

                        <div class="entry-content">
                            <?php the_content(); ?>
                            <?php wp_link_pages( array( 'before' => '<div class="page-links">Pages:', 'after' => '</div>' ) ); ?>
                        </div><!-- .entry-content -->
                        <footer class="entry-meta">
                            <?php edit_post_link( 'Edit', '<span class="edit-link">', '</span>' ); ?>
                        </footer><!-- .entry-meta -->
                    </article><!-- #post -->

                <?php endwhile; ?>

            <?php else : ?>

                <article id="post-0" class="post no-results not-found">

                    <header class="entry-header">
                        <h1 class="entry-title">Nothing Found</h1>
                    </header>

                    <div class="entry-content">
                        <p>Apologies, but no results were found. </p>
                    </div><!-- .entry-content -->

                </article><!-- #post-0 -->

            <?php endif; // end have_posts() check ?>

        </div><!-- #content -->
    </div><!-- #primary -->

<?php get_footer(); ?>
