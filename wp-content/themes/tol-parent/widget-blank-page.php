<?php
include_once($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');
//include($_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/xptheme/simple_html_dom.php');

$page_alias = $_GET['alias'];
if ($page_alias) {
    ?>
    <head>

        <script type='text/javascript'
                src='/wp-content/themes/tol-parent/javascript/jquery.min.js'></script>
        <script type='text/javascript'
                src='/wp-content/themes/tol-parent/javascript/jquery.cookie.js'></script>
        <link rel='stylesheet' href='/wp-content/themes/tol-parent/blogs-n-pages-styles.css' type='text/css'
              media='all'/>
        <link rel='stylesheet' href='/wp-content/themes/tol-parent/style.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='/wp-content/themes/tol-parent/fonts.css' type='text/css' media='all'/>
        <link rel="stylesheet" href="/wp-content/themes/tol-parent/prior.css" type="text/css" media="all"/>
        <link rel="stylesheet"
              href="/wp-content/plugins/automatic-page-numbers-pagenavi/automatic-page-numbers-pagenavi.css"
              type="text/css"
              media="all"/>
        <link rel="stylesheet" type="text/css" id="bottom-css2"
              href="/wp-content/themes/tol-child/styles/bottom-light.css">

    </head>
    <body class="entry-content">
    <div class="scrollContent" style="overflow-y: scroll;height: 256px;">
        <style type="text/css">
            html {
                margin-top: -10px;
            }

            body {
                padding: 0 0 0 0;
                margin: 0;
                height: auto;
            }

            .m_nav_pan img {
                height: auto;
            }

            img {
                height: auto !important;
            }

            table#fxst-calendartable th {
                font-size: 11px !important;
                font-weight: bold !important;
                text-transform: uppercase !important;
            }

            .new_template li {
                padding-left: 20px;
                background: url(/wp-content/themes/tol-parent/images/posts/puls-rynka/li.png) no-repeat;
                font: 13px Arial;
                color: black;
                margin-bottom: 10px;
                list-style-type: none;
                margin-left: 0;
            }

            .new_template li span {
                display: inline-block;
                margin-top: -2px;
            }

            .entry-content p {
                margin-top: 0 !important;
            }

            .inner_page {
                width: 738px !important;
                margin: 0 auto !important;
            }

            div.subli span {
                position: relative !important;
                top: -2px !important;
                font: 13px Arial !important;
                color: black !important;
            }

            #mega_button a {
                display: inline-block !important;
                font: bold 45px pf_din_text_cond_probold !important;
                color: white !important;
                text-transform: uppercase !important;
                text-decoration: none !important;
                background-color: #69bb1b !important;
                height: 58px !important;
                width: 372px !important;
                text-align: center !important;
                margin-top: 13px !important;
                padding-top: 5px !important;
                margin-bottom: 10px !important;
                margin-left: 32px !important;
            }

            #mega_button a:hover {
                display: inline-block !important;
                font: bold 45px pf_din_text_cond_probold !important;
                color: white !important;
                text-transform: uppercase !important;
                text-decoration: none !important;
                background-color: #458E00 !important;
                height: 58px !important;
                width: 372px !important;
                text-align: center !important;
                margin-top: 13px !important;
                padding-top: 5px !important;
                margin-bottom: 10px !important;
                margin-left: 32px !important;
            }
        </style>
        <script>
            function popup_strategy(pageURL, title) {
                var left = (screen.width / 2) - (1000 / 2);
                var top = (screen.height / 2) - (700 / 2);
                var targetWin = window.open(pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width=1000, height=700, top=' + top + ', left=' + left);
            }
        </script>
        <script>
            jQuery(function ($) {
                $('.text_block strong').css('color', 'initial');
                $('#strategy_table td a').css('color', 'initial');
            });
        </script>
        <script>
            jQuery(function ($) {
                if ($.cookie("color-scheme") !== 'light') {
                    document.getElementById('bottom-css2').href = location.origin + '/wp-content/themes/tol-child/styles/bottom-dark.css';
                } else {
                    document.getElementById('bottom-css2').href = location.origin + '/wp-content/themes/tol-child/styles/bottom-light.css';
                }
            });
        </script>
        <!--
        <script type="text/javascript">
            $(function () {
                $('.scrollContent').height($(parent.window.document).find('.tradesTable').height());
            });
        </script>
        -->
        <?php
        $hasCookie = is_array($_COOKIE);
        $userDataExists = false;
        if ($hasCookie) {
            $userDataExists = array_key_exists('widgetUserData', $_COOKIE);
        }
        $cookie = null;
        if ($userDataExists) {
            $cookie = json_decode(stripslashes($_COOKIE['widgetUserData']));
        }
        $user_level = null;
        if (!empty($cookie)) {
            $user_level = $cookie->userClass;
        }
        if (
            strpos($page_alias, 'category/') === false
            && strpos($page_alias, 'tehnicheskiy-analiz') === false
            && strpos($page_alias, 'technical-analysis') === false
            && strpos($page_alias, 'plan-torgovli') === false
            && strpos($page_alias, 'trading-plan') === false
            && strpos($page_alias, 'ekonomicheskii-kalendar') === false
            && strpos($page_alias, 'economic-calendar') === false
            && strpos($page_alias, 'torgovie-idei') === false
            && strpos($page_alias, 'trading-ideas') === false
            && strpos($page_alias, 'tehnicheskiy-analiz') === false
            && strpos($page_alias, 'technical-analysis') === false) {
            if ($page_alias == 'strat') {
                $page_id = url_to_postid('стратегии');
                $content = get_post_field('post_content', $page_id, 'display');
                $content = apply_filters('the_content', $content);
                echo $content;
            } elseif ($page_alias == 'strat-en') {
                $page_id = url_to_postid('en/strategy');
                $content = get_post_field('post_content', $page_id, 'display');
                $content = apply_filters('the_content', $content);
                echo $content;
            } else {
                $page_id = url_to_postid($page_alias);
                $content = get_post_field('post_content', $page_id, 'display');
                $content = apply_filters('the_content', $content);
                echo $content;
            }
        } else {

            $user_level = 2;
            $isDeny = intval(filter_var($user_level, FILTER_SANITIZE_NUMBER_INT)) < 2;
            if ($page_alias == 'plan-torgovli' && $isDeny) {
                $page_id = url_to_postid('план-торговли');
                $content = get_post_field('post_content', $page_id, 'display');
                $content = apply_filters('the_content', $content);
                echo $content;
            } elseif ($page_alias == '/en/trading-plan' && $isDeny) {
                $page_id = url_to_postid('en/trading-plan');
                $content = get_post_field('post_content', $page_id, 'display');
                $content = apply_filters('the_content', $content);
                echo $content;
            } elseif ($page_alias == 'ekonomicheskii-kalendar' && $isDeny) {
                $page_id = url_to_postid('экономический-календарь');
                $content = get_post_field('post_content', $page_id, 'display');
                $content = apply_filters('the_content', $content);
                echo $content;
            } elseif ($page_alias == '/en/economic-calendar' && $isDeny) {
                $page_id = url_to_postid('en/economic-calendar');
                $content = get_post_field('post_content', $page_id, 'display');
                $content = apply_filters('the_content', $content);
                echo $content;
            } elseif ($page_alias == 'category/torgovie-idei' && $isDeny) {
                $page_id = url_to_postid('торговые-идеи');
                $content = get_post_field('post_content', $page_id, 'display');
                $content = apply_filters('the_content', $content);
                echo $content;
            } elseif ($page_alias == '/en/category/trading-ideas' && $isDeny) {
                $page_id = url_to_postid('en/trading-ideas');
                $content = get_post_field('post_content', $page_id, 'display');
                $content = apply_filters('the_content', $content);
                echo $content;
            } elseif ($page_alias == 'tehnicheskiy-analiz' && $isDeny) {
                $page_id = url_to_postid('технический-анализ');
                $content = get_post_field('post_content', $page_id, 'display');
                $content = apply_filters('the_content', $content);
                echo $content;
            } elseif ($page_alias == '/en/technical-analysis' && $isDeny) {
                $page_id = url_to_postid('en/technical-analysis-weekly-review');
                $content = get_post_field('post_content', $page_id, 'display');
                $content = apply_filters('the_content', $content);
                echo $content;
            } else {

                $paged = $_GET['news_page'];

                define('CATEGORY', 'category/');
                $position = strpos($page_alias, CATEGORY);
                $isCategory = $position !== false;
                $categorySlug = '';
                if ($isCategory) {
                    $categorySlug = substr($page_alias, $position + strlen(CATEGORY));
                }

                $tradingPlan = strpos($page_alias, 'plan-torgovli') !== false || strpos($categorySlug, 'trading-plan') !== false;
                $postsPerPage = 5;
                if ($tradingPlan) {
                    $postsPerPage = 1;
                }

                $letAddPopup = false;
                $latest_cat_post = null;
                $isCategory = !empty($categorySlug);
            if ($isCategory) {
                ?>
                <script type="text/javascript"
                        src="/wp-content/themes/tol-parent/javascript/posts/layout-processor.js"></script>
            <?php
            $letAddPopup = strpos($categorySlug, 'puls-rynka') !== false
                || strpos($categorySlug, 'market-pulse') !== false
                || strpos($categorySlug, 'torgovie-idei') !== false
                || strpos($categorySlug, 'trading-ideas') !== false;
            $latest_cat_post = new WP_Query(array('post_type' => 'post',
                'category_name' => $categorySlug,
                'post_status' => 'publish',
                'posts_per_page' => $postsPerPage,
                'paged' => $paged));
            }

            if ($letAddPopup&&false) {
            ?>
                <div style="margin:5px;">
                    <a onclick=
                       "popup_strategy('/wp-content/themes/xptheme-child/strategy-videos/6/index.html','strategy_6');"
                       style=
                       "display:inline-block;
                       height:25px;
                       cursor:pointer;
                       background:url('/wp-content/themes/tol-parent/images/posts/puls-rynka/play.jpg') no-repeat;
                       padding-left:30px;
                       color:black;
                       font:bold 13px/25px Arial;">
                        Как торговать по пульсу рынка. Стратегия торговли
                    </a>
                </div>
            <?php
            }
            $letOutput = !empty($latest_cat_post);
            if ($letOutput) {
                $letOutput = $latest_cat_post->have_posts();
            }
            if ($letOutput) {

            while ($latest_cat_post->have_posts()) {
            $latest_cat_post->the_post();
            $postId = get_the_ID();
            $postFields[$postId] = get_fields();
            ?>
                <article
                        class="post-format-standard hentry">
                    <header class="entry-header"><h1 class="entry-title"><?php the_title(); ?></h1></header>
                    <div class="entry-content" data-post-id="<?php echo $postId; ?>"><?php the_content(); ?></div>
                </article>
                <?php
            }
                ?>
                <script type="text/javascript">
                    var categoryPosts=<?php echo \json_encode($postFields) ?>;
                </script>
                <?php
                wp_reset_postdata();
            }

                if ($isCategory) {

                    $maxNumberOfPages = 10;
                    $pageNumberOfPageText = get_option('pagenavi_auto_page_translation') . " %u " . get_option('pagenavi_auto_of_translation') . " %u";
                    $previousPage = "&lt;";
                    $nextPage = "&gt;";
                    $isFirstLastNumbers = true;
                    $isThisFirstLastGap = true;
                    $firstGap = "...";
                    $lastGap = "...";

                    $total_number_of_pages = $latest_cat_post->max_num_pages; // total number of pages in category
                    if ($total_number_of_pages == 1) {
                        return null;
                    }  // only one page so no navigation is needed

                    $current_page_number = (!empty($paged)) ? $paged : 1; // current page where we are on

                    $min_page = $current_page_number - floor(intval($maxNumberOfPages) / 2); //The lowest page
                    $maxNumberOfPages = (intval($maxNumberOfPages) - 1);
                    if ($min_page < 1) $min_page = 1;
                    $max_page = $min_page + $maxNumberOfPages; // The highest page
                    if ($max_page > $total_number_of_pages) $max_page = $total_number_of_pages;
                    if ($max_page == $total_number_of_pages && $max_page > $maxNumberOfPages) $min_page = ($max_page - $maxNumberOfPages); // changes min_page if max is last page now

                    $pagingOutputString = "<ul class='page_navistyle'>"; //String splays "Page x of y, based on the settrings from translation"
                    $pagingOutputString .= sprintf("<li class='page_info'>" . $pageNumberOfPageText . "</li>", floor($current_page_number), floor($total_number_of_pages));

                    $callbackBase = $_SERVER['SCRIPT_NAME'] . '?alias=';
                    // displays link to previous page number
                    if ($current_page_number != 1)
                        $pagingOutputString .= sprintf("<li><a href='%s'>%s</a></li>", $callbackBase . $page_alias . '&news_page=' . ($current_page_number - 1), $previousPage);

                    // displays page 1 links and ellipses further when min page is more than 1
                    if ($min_page > 1) {
                        if ($isFirstLastNumbers) $pagingOutputString .= sprintf("<li class='first_last_page'><a href='%s'>%u</a>", $callbackBase . $page_alias . '&news_page=1', 1);
                        if ($isThisFirstLastGap) $pagingOutputString .= sprintf("<li class='space'>%s</li>", $firstGap);
                    }

                    // displays lowest to highest page output
                    for ($i = $min_page; $i <= $max_page; $i++)
                        $pagingOutputString .= ($current_page_number == $i) ?
                            sprintf("<li class='current'><span><a>%u</a></span></li>", $i) :
                            sprintf("<li %s><a href='%s'>%u</a></li>", ($current_page_number == $i) ? "class='after'" : '', $callbackBase . $page_alias . '&news_page=' . $i, $i);

                    // displays total page link and ellipses when max page is lower than the total page
                    if ($max_page < $total_number_of_pages) {
                        if ($isThisFirstLastGap) $pagingOutputString .= sprintf("<li class='space'>%s</li>", $lastGap);
                        if ($isFirstLastNumbers) $pagingOutputString .= sprintf("<li class='first_last_page'><a href='%s'>%u</a>", $callbackBase . $page_alias . '&news_page=' . ($total_number_of_pages), $total_number_of_pages);
                    }

                    // displays link to next page
                    if ($current_page_number != $total_number_of_pages)
                        $pagingOutputString .= sprintf("<li><a href='%s'>%s</a></li>", $callbackBase . $page_alias . '&news_page=' . ($current_page_number + 1), $nextPage);

                    $pagingOutputString .= "</ul>\n\n";

                    echo $pagingOutputString;
                }

            }
        }
        ?>
    </div>
    </body>
    <?php
}
?>

