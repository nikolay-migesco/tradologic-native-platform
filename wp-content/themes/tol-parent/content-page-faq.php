<?php
/**
 *
 * Template Name: Wilkins FAQ page
 *
 */
 get_header();
?>

<style>

.accordion-toggle[aria-expanded="false"] .box-icon .expand {
  display: block;
}
.accordion-toggle[aria-expanded="true"] .box-icon .expand {
  display: none;
}
.accordion-toggle[aria-expanded="false"] .box-icon .contract {
  display: none;
}
.accordion-toggle[aria-expanded="true"] .box-icon .contract {
  display: block;
}
.cms-page-content strong {
    font-weight: 800;
}
.cms-page-content h2:not(:first-child) {
    margin-top: 60px;
}
.breadcrumb {
    padding: 0;
    margin-bottom: 20px;
	line-height: 52px;
    list-style: none;
    background-color: transparent;
    max-width: 1150px;
    margin: auto;
}
.breadcrumbs__link {
    color: #fff !important;
}
.breadcrumbs__divider {
    margin: 0 10px;
    color: #f9a134;
    font-size: 13px;
    font-weight: 900;
}
.breadcrumb>.active {
    color: #fff;
}
.breadcrumb a{
   color: #fff !important;
}
.breadcrumbs__wrapper {
    height: 52px;
    background: #1a1d1f;
}
.page-content {
    padding-top: 140px;
    padding-bottom: 0;
    background-color: #111214;
}
.container.cfd-container {
	 padding-top: 30px;
}
.page-content.page-content-home {
    padding-top: 0
}

@media screen and (max-width: 767px) {
    .page-content {
        padding-top:50px;
        padding-bottom: 0
    }
}
.cfd-container {
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
}
.cfd-container, .cfd__main-text {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
}
.container.cfd-containe {
    height: 100%;
    margin: 0 auto;
    max-width: 1150px;
    position: relative;
    width: 100%;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
	padding: 0;
}
.sidebar {
    width: 230px;
	margin-right: 20px;
}
.sidebar__menu {
    background: #000;
    margin-bottom: 30px;
}
.sidebar__menu__header {
    display: block;
    height: 38px;
    line-height: 38px;
    padding-left: 30px;
    background: #1a1c1f;
    color: #f9a134;
    font-size: 15px;
    font-weight: 500;
	margin: 0;
}
.sidebar__menu__item {
    height: 38px;
    color: #fff;
    border-top: 1px solid #383d43;
    -webkit-transition: .1s;
    transition: .1s;
}
.sidebar__menu__item.active {
	color: #f9a134;
}
.sidebar__menu a {
    display: block;
    height: 38px;
    padding-left: 30px;
    line-height: 38px;
    font-size: 14px;
    text-decoration: none;
    color: inherit;
    font-family: Open Sans,Arial,Tahoma,sans-serif;
}
.cfd__main-text {
    font-family: Open Sans,Arial,Tahoma,sans-serif;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    max-width: 900px;
    width: 900px;
    padding: 6px;
    -webkit-box-flex: 1;
    -ms-flex-positive: 1;
    flex-grow: 1;
    -ms-flex-negative: 1;
    flex-shrink: 1;
}
h1 {
    font-size: 35px;
	margin: 0;
    margin-bottom: 30px;
    font-family: Open Sans,Arial,Tahoma,sans-serif;
    color: #f9a134;
	font-weight: 400;
}
h2 {
    font-size: 25px;
    color: #fff;
    margin: 0 0 25px 0;
	font-family: Open Sans,Arial,Tahoma,sans-serif;
	font-weight: 400;
}
.cms-page-content li, .cms-page-content p {
    margin-bottom: 15px;
    line-height: 22px;
    font-size: 15px;
    color: #fff;
	font-family: Open Sans,Arial,Tahoma,sans-serif;
}
ul, ol {
    list-style: none;
    padding-left: 0;
}
.sidebar__menu__item:hover {
    color: #f9a134;
}
.glossary-wrapper {
	background: #fff;
    border-radius: 6px;
    max-width: 880px;
    overflow: hidden;
    color: #000;
}
.glossary-wrapper .nav-tabs.clearfix {
	-webkit-box-orient: horizontal;
    -ms-flex-flow: row wrap;
    flex-flow: row wrap;
	display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-direction: normal;
	border: 0;
}
.glossary-wrapper .nav-tabs.clearfix li {
    width: 54px;
    cursor: pointer;
    font-size: 13px;
    font-weight: 700;
    color: #444;
    border-right: 1px solid #d3d3d3;
    border-bottom: 1px solid #d3d3d3;
    background: #f6f6f6;
	line-height: 42px;
}
.nav-tabs>li>a {
    margin-right: 0;
    line-height: 15px;
    border: 0;
    border-radius: 0;
    color: #444;
	padding: 12px 23px;
}
.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
    box-shadow: none;
    border: 0;
}
.glossary-wrapper .nav-tabs.clearfix li.active {
	background-color: #fff;
    border-bottom: none;
}
.glossary-wrapper ul  {
	    -webkit-box-orient: vertical;
    -ms-flex-direction: column;
    flex-direction: column;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    padding:  0;
	margin: 0;
}

.collapse-sub-menu {
	    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    max-width: 890px;
    min-height: 49px;
    padding: 0 14px 0 0;
    font-size: 14px;
    line-height: 22px;
    font-family: Noto Sans,serif;
    font-weight: 700;
    background-color: #282d30;
    border: none;
    color: #fff;
}
.box-icon {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    min-width: 50px;
    border-right: 1px solid #e5e5e5;
    min-height: 49px;
	float: left;
}
.collapse-sub-menu .panel-title {
    font-size: 24px;
    padding-top: 14px;
    padding-left: 10px;
}
.tab-content {
    padding: 0;
}
.panel-body {
    padding: 15px 15px 15px 60px;
    background-color: #191d20;
    color: #fff;
}
.panel.panel-default {
	border:0;
    max-width: 850px;
    width: 97%;
	min-height: 49px;
    list-style: none;
    margin-bottom: 7px;
    cursor: pointer;
}
.panel-default > .panel-heading {
    border-bottom-color: transparent;
    border-top-color: transparent;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    max-width: 890px;
    min-height: 49px;
    padding: 0 14px 0 0;
    font-size: 14px;
    line-height: 22px;
    font-family: Noto Sans,serif;
    font-weight: 700;
    background-color: #282d30 !important;
    border: none;
    color: #fff;
	border-radius: 0;
}
.panel-default h4 {
    display: inline-block;
    padding-left: 20px;
    padding-right: 5px;
    font-family: Open Sans,Arial,Tahoma,sans-serif;
    font-size: 14px;
	line-height: 49px;
	color: #fff;
	width: 100%;
}
.accordion-toggle {
	width: 100%;
}
.fa-icon {
    display: inline-block;
    fill: #fff;
	margin: auto;
}
@media(max-width: 1024px){
	.breadcrumb {
		padding: 0 3%;
	}
	.container.cfd-container {
		width: 100%;
	}
}
@media(width: 768px){
	.cfd__main-text {
		width: 490px;
	}
}
@media screen and (max-width: 767px){
	.cfd-container {
		-webkit-box-orient: vertical;
		-webkit-box-direction: reverse;
		-ms-flex-direction: column-reverse;
		flex-direction: column-reverse;
		-ms-flex-line-pack: center;
		align-content: center;
		padding-left: 15px;
	}
	.sidebar {
		width: 100%;
		min-width: 230px;
	}
	.cfd__main-text {
		width: 100%;
	}
}
.cms-page-content .panel-body p {
	margin-bottom: 0px;
}
</style>

<div id="content" role="main">
   <div class="page-content" style="background-color:#111214;">
      <div class="breadcrumbs__wrapper">
         <?php require_once(ABSPATH.'wp-content/themes/tol-parent/layouts/breadcrumb.php' ); ?>
      </div>
      <div class="container cfd-container">
         <div class="sidebar">
            <div class="sidebar__menu">
               <p class="sidebar__menu__header">About us</p>
               <ul>
                  <li class="sidebar__menu__item"><a href="/about-us/why-us" class="">Why Us</a></li>
                  <li class="sidebar__menu__item active"><a href="/about-us/faq-page" class="">FAQ</a></li>
                  <li class="sidebar__menu__item"><a href="/about-us/contact-us" class="">Contact Us</a></li>
               </ul>
            </div>
         </div>
         <div class="cfd__main-text">
            <h1>FAQ's</h1>
            <div class="cms-page-content">
               <div class="" >
			   <h2>Support</h2>
                  <div id="accordion-1">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-1" href="#collapse11" aria-expanded="false">
                              <h4 class="panel-title">What if I forgot my password?</h4>
                           </a>
                        </div>
                        <div id="collapse11" class="panel-collapse collapse">
                           <div class="panel-body">
                              <p>Under the login button, click on the link that says “forgot password?” You’ll be asked for your email address, and to enter a confirmation number that will appear on the web page. The support team will send a new password to the email address on file. You can also contact our customer support team.</p>
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-1" href="#collapse12" aria-expanded="false">
                              <h4 class="panel-title">How can I update my personal details?</h4>
                           </a>
                        </div>
                        <div id="collapse12" class="panel-collapse collapse" >
                           <div class="panel-body">
                              <p>Log in to your account, and visit the Personal Details section where you can make any necessary updates. You can also contact our customer support team by email with a detailed list of the updated information.</p>
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-1" href="#collapse13" aria-expanded="false">
                              <h4 class="panel-title">How can I get in touch with you?</h4>
                           </a>
                        </div>
                        <div id="collapse13" class="panel-collapse collapse" >
                           <div class="panel-body">
                              <p>We offer support 24 hours a day, five days per week via email, chat, or telephone. Please visit our contact page for more information.</p>
                           </div>
                        </div>
                     </div>
                  </div>
				  <h2>Platform</h2>
                  <div id="accordion-2">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-2" href="#collapse21" aria-expanded="false">
                              <h4 class="panel-title">Do I have to download any software to trade with you?</h4>
                           </a>
                        </div>
                        <div id="collapse21" class="panel-collapse collapse">
                           <div class="panel-body">
                              <p>We offer a web-based platform, which means you can log in with any compatible device using just an internet connection. There is no need to download any special software.</p>
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-2" href="#collapse22" aria-expanded="false">
                              <h4 class="panel-title">How can I view my trading history?</h4>
                           </a>
                        </div>
                        <div id="collapse22" class="panel-collapse collapse" >
                           <div class="panel-body">
                              <p>You have complete access to all of your records, including trades, deposits, and withdrawals, when you log in to the My Account section of our website. You can also contact us for assistance regarding your account.</p>
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-2" href="#collapse23" aria-expanded="false">
                              <h4 class="panel-title">Where can I get specific information on the prices of the assets you offer?</h4>
                           </a>
                        </div>
                        <div id="collapse23" class="panel-collapse collapse" >
                           <div class="panel-body">
                              <p>We include detailed pricing data in the order section on the platform.</p>
                           </div>
                        </div>
                     </div>
					<div class="panel panel-default">
                        <div class="panel-heading">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-2" href="#collapse24" aria-expanded="false">
                              <h4 class="panel-title">What is the default timezone for the trading platform?</h4>
                           </a>
                        </div>
                        <div id="collapse24" class="panel-collapse collapse" >
                           <div class="panel-body">
                              <p>The platform is set for GMT +0. However you can customize the time zone.</p>
                           </div>
                        </div>
                     </div>
					 	<div class="panel panel-default">
                        <div class="panel-heading">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-2" href="#collapse25" aria-expanded="false">
                              <h4 class="panel-title">How can I tell when asset prices are going up or down?</h4>
                           </a>
                        </div>
                        <div id="collapse25" class="panel-collapse collapse" >
                           <div class="panel-body">
                              <p>We have a color code to help you determine price directions. If the asset price is in green, the price is trending up. If the asset price is in red, the price is trending down.</p>
                           </div>
                        </div>
                     </div>
					 <div class="panel panel-default">
                        <div class="panel-heading">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-2" href="#collapse26" aria-expanded="false">
                              <h4 class="panel-title">How does Online Trading work?</h4>
                           </a>
                        </div>
                        <div id="collapse26" class="panel-collapse collapse" >
                           <div class="panel-body">
                              <p>Traders working with up and down options select whether an asset’s price will go up or down. If they make a correct prediction, they keep their investment and receive an additional predetermined percentage.</p>
                           </div>
                        </div>
                     </div>
					 <div class="panel panel-default">
                    <div class="panel-heading">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-2" href="#collapse27" aria-expanded="false">
                              <h4 class="panel-title">What is traded with Online Trading?</h4>
                           </a>
                        </div>
                        <div id="collapse27" class="panel-collapse collapse" >
                           <div class="panel-body">
                              <p>We offer a large list of assets from across several types, including currencies, stocks, indices, and commodities.</p>
                           </div>
                        </div>
                     </div>
					 <div class="panel panel-default">
                    <div class="panel-heading">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-2" href="#collapse28" aria-expanded="false">
                              <h4 class="panel-title">What is the most I can invest in one trade?</h4>
                           </a>
                        </div>
                        <div id="collapse28" class="panel-collapse collapse" >
                           <div class="panel-body">
                              <p>Our standard orders have a limit of 2500 units of your account’s base currency (USD/GBP/EUR). We may be able to create a specialized account if you need to be able to place larger trades. Please contact us with any questions.</p>
                           </div>
                        </div>
                     </div>
                  </div>
				  
				  
				  
				 <h2>Open Account</h2>
                  <div id="accordion-3">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-3" href="#collapse31" aria-expanded="false">
                              <h4 class="panel-title">How can I open an account?</h4>
                           </a>
                        </div>
                        <div id="collapse31" class="panel-collapse collapse">
                           <div class="panel-body">
                              <p>Please complete an online application. You will then receive a verification email containing a list of documents mandated to confirm your identity.</p>
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-3" href="#collapse32" aria-expanded="false">
                              <h4 class="panel-title">I’m having difficulties opening an account, what shall I do?</h4>
                           </a>
                        </div>
                        <div id="collapse32" class="panel-collapse collapse" >
                           <div class="panel-body">
                              <p>Please contact us by email, chat, or phone for assistance in opening your account.</p>
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-3" href="#collapse33" aria-expanded="false">
                              <h4 class="panel-title">Are their taxes on profits I make trading with Migesco?</h4>
                           </a>
                        </div>
                        <div id="collapse33" class="panel-collapse collapse" >
                           <div class="panel-body">
                              <p>Traders are responsible for complying with tax regulations in their jurisdiction. Please consult a qualified tax professional for more information.</p>
                           </div>
                        </div>
                     </div>
					<div class="panel panel-default">
                        <div class="panel-heading">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-3" href="#collapse34" aria-expanded="false">
                              <h4 class="panel-title">Is trading with Migesco secure?</h4>
                           </a>
                        </div>
                        <div id="collapse34" class="panel-collapse collapse" >
                           <div class="panel-body">
                              <p>We use SSL technology, the same safety measures adopted by the banking industry, to safeguard your funds and personal information.</p>
                           </div>
                        </div>
                     </div>
	
					 <div class="panel panel-default">
                        <div class="panel-heading">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-3" href="#collapse35" aria-expanded="false">
                              <h4 class="panel-title">Can I open an account before making a deposit?</h4>
                           </a>
                        </div>
                        <div id="collapse35" class="panel-collapse collapse" >
                           <div class="panel-body">
                              <p>You can activate your account prior to sending funds. However, you will need to make a deposit before you can trade.</p>
                           </div>
                        </div>
                     </div>
					 <div class="panel panel-default">
                    <div class="panel-heading">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-3" href="#collapse36" aria-expanded="false">
                              <h4 class="panel-title">What currencies can I use for trading?</h4>
                           </a>
                        </div>
                        <div id="collapse36" class="panel-collapse collapse" >
                           <div class="panel-body">
                              <p>We offer trading in USD, GBP, and EUR. Accounts cannot be converted to different currencies after they have been created.</p>
                           </div>
                        </div>
                     </div>
                  </div>
				  
				  <h2>Deposit</h2>
                  <div id="accordion-4">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-4" href="#collapse41" aria-expanded="false">
                              <h4 class="panel-title">What is the minimum deposit needed to trade on an account?</h4>
                           </a>
                        </div>
                        <div id="collapse41" class="panel-collapse collapse">
                           <div class="panel-body">
                              <p>The minimum deposit is 250 units of your account’s base currency (USD, GBP, EUR).</p>
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-4" href="#collapse42" aria-expanded="false">
                              <h4 class="panel-title">What is the minimum withdrawal amount?</h4>
                           </a>
                        </div>
                        <div id="collapse42" class="panel-collapse collapse" >
                           <div class="panel-body">
                              <p>The minimum for withdrawals is 50 units of your account’s base currency (USD, GBP, EUR).</p>
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-4" href="#collapse43" aria-expanded="false">
                              <h4 class="panel-title">What banking information is mandated for a withdrawal?</h4>
                           </a>
                        </div>
                        <div id="collapse43" class="panel-collapse collapse" >
                           <div class="panel-body">
                              <p>Before requesting a withdrawal, please make sure that you have supplied us with the name of the bank account holder, your account number, the bank’s SWIFT code, and the bank’s address. The SWIFT code is a unique identifier that allows banks to transfer funds between each other. Please contact your bank to confirm the correct SWIFT code.</p>
                           </div>
                        </div>
                     </div>
					<div class="panel panel-default">
                        <div class="panel-heading">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-4" href="#collapse44" aria-expanded="false">
                              <h4 class="panel-title">How can I cancel a withdrawal request?</h4>
                           </a>
                        </div>
                        <div id="collapse44" class="panel-collapse collapse" >
                           <div class="panel-body">
                              <p>Prior to our sending the funds to your bank, we can stop a withdrawal at your request. Please contact us via chat, phone, or email to cancel a pending withdrawal.</p>
                           </div>
                        </div>
                     </div>
	
					 <div class="panel panel-default">
                        <div class="panel-heading">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-4" href="#collapse45" aria-expanded="false">
                              <h4 class="panel-title">Can I send a wire transfer to fund my account?</h4>
                           </a>
                        </div>
                        <div id="collapse45" class="panel-collapse collapse" >
                           <div class="panel-body">
                              <p>We can accept wire transfers for amounts of 500 units of your account’s base currency (USD, GBP, EUR) or more. Wire transfers are usually processed within 3 to 5 business days. Please contact our customer service department for wire transfer instructions.</p>
                           </div>
                        </div>
                     </div>
					 <div class="panel panel-default">
                    <div class="panel-heading">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-4" href="#collapse46" aria-expanded="false">
                              <h4 class="panel-title">What methods are available for making deposits and withdrawals?</h4>
                           </a>
                        </div>
                        <div id="collapse46" class="panel-collapse collapse" >
                           <div class="panel-body">
                              <p>We offer deposits and withdrawals via credit or debit card, or by wire.</p>
                           </div>
                        </div>
                     </div>
					 <div class="panel panel-default">
                        <div class="panel-heading">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-4" href="#collapse47" aria-expanded="false">
                              <h4 class="panel-title">What steps are mandated to deposit or withdraw funds?</h4>
                           </a>
                        </div>
                        <div id="collapse47" class="panel-collapse collapse" >
                           <div class="panel-body">
                              <p>We can accept wire transfers for amounts of 500 units oIn order to fund your account or make a withdrawal, we will need your account to be activated. This requires government issued ID, proof of residency, and submission of a few forms related to the terms and conditions of your account. Please visit our page on account verification for additional information.</p>
                           </div>
                        </div>
                     </div>
                  </div>
				  
				  <h2>Trading</h2>
                  <div id="accordion-5">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-5" href="#collapse51" aria-expanded="false">
                              <h4 class="panel-title">Why are the rates constantly changing before I place my trade?</h4>
                           </a>
                        </div>
                        <div id="collapse51" class="panel-collapse collapse">
                           <div class="panel-body">
                              <p>Because we display rates in real time, you are seeing how the market movements affect asset prices.</p>
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-5" href="#collapse52" aria-expanded="false">
                              <h4 class="panel-title">Why can’t I place a trade?</h4>
                           </a>
                        </div>
                        <div id="collapse52" class="panel-collapse collapse" >
                           <div class="panel-body">
                              <p>There are two major reasons why you will not be able to trade on your account. First, check to see if trading is open for your asset. Most assets are closed for trading between 5pm EST on Friday and 5pm EST on Sunday. Some assets have specific trading hours which you can find in our asset list. Second, you should confirm that your account has sufficient funds for trading. Please contact us if you are unable to determine why trading is not available for your account.</p>
                           </div>
                        </div>
                     </div>
                  </div>
				  
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- #content -->

<?php get_footer(); ?>
