<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 */

defineMigescoLayout();

get_header(); ?>
<div style="margin:183px auto 0;width:980px;">
    <div id="primary" class="site-content">
        <div id="content" role="main">

            <article id="post-0" class="post error404 no-results not-found">
                <header class="entry-header">
                    <h1 class="entry-title">Страница не найдена</h1>
                </header>

                <div class="entry-content">
                    <p>Мы недавно изменили структуру нашего сайта, и возможно вы воспользовались устаревшей
                        ссылкой. </p>
                </div><!-- .entry-content -->
            </article><!-- #post-0 -->

        </div><!-- #content -->
    </div><!-- #primary -->
</div>
<?php get_footer(); ?>
