<?php
/**
 * 
 *  The template for displaying the footer.
 *
 */
?>

<?php
$isLoadingMigescoLayout = isMigescoLayout();
if (!$isLoadingMigescoLayout) {
    wp_footer();
    ?>
    <script type="text/javascript">
        document.write(ssxdom('b13075cb792e333de3ec360539d5bda2c3b20d3d'));
    </script>
    <?php
    load_template( dirname( __FILE__ ) . '/layouts/'.FOOTER_LAYOUT.'.php' );
}
if ($isLoadingMigescoLayout) {
    load_template(dirname(__FILE__) . '/layouts/footer-' . MIGESCO_LAYOUT . '.php');
}
