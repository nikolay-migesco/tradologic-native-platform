<!--<script type="text/javascript" src="/wp-content/themes/tol-parent/registration.js?1"></script>-->
<script type="text/javascript">
    var loadingRegistration;
    var regCount = 0;

    $(window).on('registrationOnSubmit', function (event, response) {
        if (regCount == 0) {
            $('.dialog-widget').attr('value', 'Открытие счета').attr('style', 'background-color:#69bb1b !important');
            $('.preload_registration').css('left', '30px');
            $('#form_reg .preload_registration').css('left', '45px');
        } else {
            $('.dialog-widget').attr('value', 'Подключение').attr('style', 'background-color:#ff7200 !important');
            $('.preload_registration').css('left', '35px');
            $('#form_reg .preload_registration').css('left', '53px');
        }
        $('.preload_registration').show();

        loadingRegistration = setTimeout(function () {
            $('.preload_registration').hide();
            if (regCount == 3) {
                $('.dialog-widget').attr('value', 'Ошибка подключения').attr('style', 'background-color:#DE0404 !important');
            } else {
                $('.dialog-widget').attr('value', 'Подключение').attr('style', 'background-color:#ff7200 !important');
                $('.preload_registration').css('left', '35px');
                $('#form_reg .preload_registration').css('left', '53px');
                regCount++;
            }
            writeRegistrationLog('reg-dialog-widget', 'reg_success', '', 'Registration Timeout [' + regCount + '] Tradologic (20 sec.)');
            console.log(regCount);
            if (regCount != 3) {
                $('#reg-dialog-widget').click();
            }
            if (regCount == 3) {
                $('.dialog-widget').attr('value', 'Ошибка подключения').attr('style', 'background-color:#DE0404 !important');
                setTimeout(function () {
                    $('.dialog-widget').attr('value', 'Открыть счет').attr('style', 'background-color:#69bb1b !important');
                }, 1000);
                regCount = 0;
            }
        }, 20000);
    });

    $(window).on('registrationCompleted', function (event, response) {
        clearTimeout(loadingRegistration);
        if (response.code == '400') {
            $('.preload_registration').hide();
            $('.dialog-widget').attr('value', 'Ошибка подключения').attr('style', 'background-color:#DE0404 !important');
            <?php if(prior_lang() == 'ru') { ?>
            setTimeout(function () {
                $('.appriseOuter').css('left', '469px');
                if ($('.appriseOuter .appriseInner').contents().get(0).nodeValue == 'Action is not allowed for this country.') {
                    $('.appriseOuter .appriseInner').contents().get(0).nodeValue = 'Компания не предоставляет сервис клиентам из вашего региона';
                }
            }, 100);
            <?php } else { ?>

            setTimeout(function () {
                $('<span>The company does not provide services to clients from your region</span>').insertBefore('.aButtons');
            }, 300);


            <?php } ?>
            setTimeout(function () {
                $('.dialog-widget').attr('value', 'Открыть счет').attr('style', 'background-color:#69bb1b !important');
            }, 60000);
        } else if (response.code == '201') {
            $('.dialog-widget').attr('value', 'Счет открыт');
            $('.preload_registration').css('left', '47px');
            $('.preload_registration img').css('width', '25px').css('height', '25px').attr('src', '/wp-content/themes/xptheme/img/check.png');
        }
    });
</script>
<style type="text/css">
    .preload_registration {
        position: absolute;
        top: 33px;
        left: 35px;
        width: 30px;
        height: 30px;
    }
</style>
<div class="reg-wrap" style="display:none;">
    <div class="box_title"><span>Открытие счета</span></div>
    <div id="reg_form">
        <form class="cloneReg">
            <div class="form_input">
                <input type="text" placeholder="Имя" data-widget="registration" data-type="firstName" data-isRequired="true"
                       data-minLength="1"/>
            </div>
            <div class="form_input">
                <input type="text" placeholder="Электронная почта" data-widget="registration" data-type="email"
                       data-regexpMessage="Неправильно заполнено поле Эл. почта" data-isRequired="true"/>
            </div>
            <div class="form_input">
                <input type="hidden" placeholder="Телефон" id="customer_phone2"/>
                <?php echo do_shortcode('[quickRegistrationPhone class="form-control"]'); ?>
            </div>
            <div class="form_input">
                <input type="password" placeholder="Пароль" data-widget="registration" data-type="password"
                       data-minLength="6" data-isRequired="true"
                       data-regexpMessage="Пароль должен содержать минимум 6 символов"/>
            </div>
            <div class="form_check">
                <input id="agree_term" type="checkbox" data-widget="registration" data-type="tos" data-isRequired="true"
                       value="true" checked="checked"/><label for="agree_term"><span>Я ознакомился с положениями и принимаю <br/>условия <a
                                href="/oficialnye-documenty?register=widget" target="_blank">официальных документов</a></span></label>
                <span data-widget="registration" data-type="error" style="display: none;" class="error-message"></span>
            </div>
            <div style="height:70px !important;position:relative;clear:both;">
                <input type="button" value="Открыть счет" data-widget="registration" data-type="submit"
                       class="dialog-widget" id="reg-dialog-widget"/>
                <div class="preload_registration" style="display:none;"><img
                            src="/wp-content/themes/tol-parent/images/xp-theme/load.gif" width="30" height="30"></div>
            </div>
            <div class="login-link"><span class="login-link-span">Уже есть торговый счет?</span><a
                        href="#login">Войти</a></div>
            <div style="display:none;">
                <input type="text" data-widget="registration" data-type="confirmPassword" data-isRequired="true"/>
                <input type="text" value="" data-widget="registration" data-type="lastName" data-minLength="1"
                       data-isRequired="true"/>
                <input type="text" data-widget="registration" data-type="languageCode" data-isRequired="true"
                       value="<?php echo (TolLang::getCurrentIso() == 'ru') ? 1049 : 1033; ?>">
                <input type="text" data-widget="registration" data-type="countryPhoneCode" value=""/>
                <input type="text" data-widget="registration" data-type="areaCode" value=""/>
                <select data-widget="registration" data-type="countryCode" data-isRequired="true"></select>
            </div>
        </form>
    </div>
    <div id="close_popup2"><span class="reg-close"></span></div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        $('#reg_form [data-type="lastName"], #form_reg [data-type="lastName"]').val("-");

        widgetMessage['tos'] = 'Необходимо согласиться с правилами';

        $('#reg_form input').live("change", function (e) {
            $(this).val($(this).val().trim());

            if ($(this).attr('data-type') === 'phone') {
                $(this).val($(this).val().replace(/[^0-9\(\)\[\]\{\}\-\+"'@\#\$%\^&<>\,\?=_~\\/\|;:\ ]/gim, ''));
            }
            if ($(this).attr('data-type') === 'email') {
                $(this).val($(this).val().replace(/[^a-zA-Z]+$/gim, ''));
            }

            if($(this).attr("data-type") === "firstName"){
                const firstName = $(this).val();
                $('#form_reg [data-type="firstName"]').val(firstName);
            }
        });

        $('#reg_form input').live("keypress", function (e) {
            if ($(this).attr('data-type') == 'phone') {
                if (String.fromCharCode(e.which).match(/[^0-9\(\)\[\]\{\}\-\+"'@\#\$%\^&<>\,\?=_~\\/\|;:\ ]/gim)) {
                    e.preventDefault();
                }
            }
            if (e.keyCode == 13) {
                var inputs = $(this).parents("form").eq(0).find(':input:visible');
                var idx = inputs.index(this);

                if (idx == inputs.length - 1) {
                    inputs[0].select()
                } else {
                    inputs[idx + 1].focus();
                    inputs[idx + 1].select();
                }
                return false;
            }
        });
        $('#reg_form input[data-type="password"]').change(function () {
            $('#reg_form input[data-type="confirmPassword"]').val($(this).val());
        });
    });

</script>
